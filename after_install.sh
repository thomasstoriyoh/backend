#!/bin/bash

#touch /var/www/html/app.storiyoh.com/storage/logs/laravel.log
#chmod 664 /var/www/html/app.storiyoh.com/storage/logs/laravel.log
#chown ec2-user:ec2-user /var/www/html/app.storiyoh.com/storage/logs/laravel.log

aws s3 cp s3://storiyoh-prod-conf/backend/v2/.env /var/www/html/app.storiyoh.com/.env
aws s3 cp s3://storiyoh-prod-conf/backend/oauth-public.key /var/www/html/app.storiyoh.com/storage/oauth-public.key
aws s3 cp s3://storiyoh-prod-conf/backend/oauth-private.key /var/www/html/app.storiyoh.com/storage/oauth-private.key
aws s3 cp s3://storiyoh-prod-conf/google/analytics/laravel-project-1211-5f36bc4a0ca3.json /var/www/html/app.storiyoh.com/storage/laravel-analytics/laravel-project-1211-5f36bc4a0ca3.json
aws s3 cp s3://storiyoh-prod-conf/google/firebase/storiyoh-916d2c131c81.json /var/www/html/app.storiyoh.com/storage/storiyoh-916d2c131c81.json

chown ec2-user:ec2-user /var/www/html/app.storiyoh.com/.env
chown ec2-user:ec2-user /var/www/html/app.storiyoh.com/storage/laravel-analytics/laravel-project-1211-5f36bc4a0ca3.json
chown ec2-user:ec2-user /var/www/html/app.storiyoh.com/storage/storiyoh-916d2c131c81.json

chown apache:apache /var/www/html/app.storiyoh.com/storage/oauth-private.key
chown apache:apache /var/www/html/app.storiyoh.com/storage/oauth-public.key

setfacl -Rdm u:nginx:rwx,u:apache:rwx /var/www/html/app.storiyoh.com/storage /var/www/html/app.storiyoh.com/bootstrap/cache/
setfacl -Rm u:nginx:rwx,u:apache:rwx /var/www/html/app.storiyoh.com/storage /var/www/html/app.storiyoh.com/bootstrap/cache/

chmod 600 /var/www/html/app.storiyoh.com/storage/oauth-private.key
chmod 600 /var/www/html/app.storiyoh.com/storage/oauth-public.key

chmod 440 /var/www/html/app.storiyoh.com/storage/laravel-analytics/laravel-project-1211-5f36bc4a0ca3.json
chmod 440 /var/www/html/app.storiyoh.com/storage/storiyoh-916d2c131c81.json

cd /var/www/html/app.storiyoh.com/
composer install

cp /var/www/html/app.storiyoh.com/packages/tamayo/laravel-scout-elastic/src/ElasticsearchEngine.php /var/www/html/app.storiyoh.com/vendor/tamayo/laravel-scout-elastic/src/ElasticsearchEngine.php

chown ec2-user:ec2-user /var/www/html/app.storiyoh.com/vendor/ -R

service supervisord restart

#chmod 664 /var/www/html/app.storiyoh.com/.env
#chown ec2-user:ec2-user /var/www/html/app.storiyoh.com/.env

#chmod 600 /var/www/html/app.storiyoh.com/storage/oauth-public.key
#chown apache:apache /var/www/html/app.storiyoh.com/storage/oauth-public.key

#chmod 600 /var/www/html/app.storiyoh.com/storage/oauth-private.key
#chown apache:apache /var/www/html/app.storiyoh.com/storage/oauth-private.key

#touch /var/www/html/storiyoh.com/storage/logs/laravel.log
#chmod 664 /var/www/html/storiyoh.com/storage/logs/laravel.log
#chown ec2-user:ec2-user /var/www/html/storiyoh.com/storage/logs/laravel.log

#aws s3 cp s3://storiyoh-prod-conf/frontend/.env /var/www/html/storiyoh.com/.env
#chmod 664 /var/www/html/storiyoh.com/.env
#chown ec2-user:ec2-user /var/www/html/storiyoh.com/.env

#cd /var/www/html/storiyoh.com/
#composer dump-autoload