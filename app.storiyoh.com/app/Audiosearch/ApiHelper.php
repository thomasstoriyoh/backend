<?php
namespace App\Audiosearch;

class ApiHelper
{
    /**
     * ApiHelper constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @return object
     */
    public function getCategories()
    {
        return $this->client->get('categories');
    }

    /**
     * @return object
     */
    public function getNetworks()
    {
        return $this->client->get('networks');
    }

    /**
     * @param int $from
     * @param int $size
     * @param string $sort
     * @param string $order
     * @return object
     */
    public function getShowsFrom($from, $size = 1000, $sort = 'id', $order = 'asc')
    {
        return $this->client->get('search/shows/*', [
            'size' => $size,
            'from' => 0,
            'sort_by' => $sort,
            'sort_order' => $order,
            'range[id]' => $from
        ]);
    }

    /**
     * @param int $from
     * @param int $size
     * @param string $sort
     * @param string $order
     * @return object
     */
    public function getEpisodesFrom($from, $size = 1000, $sort = 'id', $order = 'asc')
    {
        return $this->client->get('search/episodes/*', [
            'size' => $size,
            'from' => 0,
            'sort_by' => $sort,
            'sort_order' => $order,
            'range[id]' => $from
        ]);
    }
}