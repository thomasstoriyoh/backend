<?php
namespace App\Audiosearch;

use App\Models\Category;
use App\Models\Episode;
use App\Models\Show;

class DataProcessHelper
{
    /**
     * @param array $ids
     */
    public function setCategoryParentId($ids = [])
    {
        $categories = with(new Category())->whereIn('api_id', function ($q) use ($ids) {
            $q->select('api_parent_id')->from(with(new Category())->getTable())
                ->whereNull('parent_id')->whereNotNull('api_parent_id');
            if (! empty($ids)) {
                $q->whereIn('api_id', $ids);
            }
        })->get();

        foreach ($categories as $category) {
            with(new Category())->where('api_parent_id', $category->api_id)
                ->update(['parent_id' => $category->id]);
        }
    }

    /**
     * @param array $ids
     */
    public function setEpisodeShowId($ids = [])
    {
        $shows = with(new Show())->whereIn('api_id', function ($q) use ($ids) {
            $q->select('api_show_id')->from(with(new Episode())->getTable())
                ->whereNull('show_id');
            if (! empty($ids)) {
                $q->whereIn('api_show_id', $ids);
            }
        })->get();

        foreach ($shows as $show) {
            with(new Episode())->where('api_show_id', $show->api_id)
                ->update(['show_id' => $show->id]);
        }
    }
}