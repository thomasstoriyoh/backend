<?php
namespace App\Audiosearch;

class ManualQuota
{
    /**
     * @var int
     */
    protected $category;

    /**
     * @var int
     */
    protected $network;

    /**
     * @var int
     */
    protected $show;

    /**
     * @var int
     */
    protected $episode;

    /**
     * ManualQuota constructor.
     */
    public function __construct()
    {
        $quota = config('quota');

        foreach ($quota as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * @param int $network
     */
    public function setNetwork($network)
    {
        $this->network = $network;
    }

    /**
     * @return int
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param int $show
     */
    public function setShow($show)
    {
        $this->show = $show;
    }

    /**
     * @return int
     */
    public function getEpisode()
    {
        return $this->episode;
    }

    /**
     * @param int $episode
     */
    public function setEpisode($episode)
    {
        $this->episode = $episode;
    }
}