<?php
namespace App\Classes;

use App\Models\Banner;
use App\Models\Click;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Traits\Helper;
use Illuminate\Http\Request;
class Banners
{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    /**
     * Constructor
     *
     * @param $api_type
     * @param $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * This function is use for Banner listing
     *
     * @return void
     */
    public function banner_listing()
    {
        //$request = request();

        try
        {
            $banner = Banner::Published()->orderBy('order','asc')->get(['id', 'title', 'image', 'type', 'link']);

            $bannerArray = [];
            foreach ($banner as $item) {
                $bannerArray[] = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'image' => $item->getImage(1200, 300),
                    'type' => $item->type,
                    'link' => $item->link
                ];
            }

            return response()->api([
                    'status' => true,
                    'mesage' => '',
                    'data' => $bannerArray
                ]);
        }
        catch(\Exception $ex)
        {
            return response()->api([
                'status' => false,
                'message' => 'Something is went wrong. Please try again.',
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * This function is use for Banner Click
     *
     * @return void
     */
    public function banner_clicks_event(Request $request)
    {        
        try
        {
            $request = request();
            
            $os = $request->os;
            $browser = $request->browser;
            $browser_full_info = $request->browser_full_info;
            $currentdatetime = new Carbon();

            $banner_id = $request->banner_id;

            $banner = Banner::Published()->where('id', $banner_id)->first();

            $click = new Click();
            $click->banner_id = $banner_id;
            $click->banner_title = $banner->title;
            $click->os = $os;
            $click->browser = $browser;
            $click->browser_full_info = $browser_full_info;
            $click->date_time = $currentdatetime;
            $click->save();            
            
            return response()->api([
                'status' => true,
                'mesage' => '',
            ]);
        }
        catch(\Exception $ex)
        {
            return response()->api([
                'status' => false,
                'mesage' => '',
            ]);
        }
    }
}
