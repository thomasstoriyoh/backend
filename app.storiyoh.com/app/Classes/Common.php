<?php

namespace App\Classes;

use App\Models\SearchKeyword;
use App\Models\Show;
use Illuminate\Support\Facades\Validator;
use App\Models\Episode;
use App\Traits\Helper;
use Carbon\Carbon;
use App\Models\Chart;
use App\Models\PodcastDay;

class Common
{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    /**
     * Constructor
     *
     * @param [type] $api_type
     * @param [type] $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * Search Keyword
     *
     * @return void
     */
    public function search_keyword()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
                'q' => 'required',
                'type' => 'required'
            ], [
                'q.required' => config('config.common_message'),
                'type.required' => config('config.common_message')
            ]);

        if ($validator->fails()) {
            return response()->api([
                    'status' => false,
                    'message' => $validator->errors()->first()
                ]);
        }

        try{
            if ($request->type == 'Show') {
                if (! empty($request->q)) {
                    $showInfo = Show::find($request->q);
                    $data = SearchKeyword::where('type', 'Show')->where('show_id', $request->q)->first(['id', 'count']);
                    if ($data) {
                        $data->fill(['count' => ($data->count + 1)])->save();
                    } else {
                        SearchKeyword::create(['keyword' => strtolower($showInfo->title), 'show_id' => $showInfo->id, 'count' => 1, 'type' => 'Show']);
                    }
                }
            } else {
                if (! empty($request->q)) {
                    $data = SearchKeyword::where('type', 'Episode')->where(\DB::raw('lower(keyword)'), 'LIKE', '%' . strtolower($request->q) . '%')->first(['id', 'count']);
                    if ($data) {
                        $couter = $data->count + 1;
                        $data->fill(['count' => $couter])->save();
                    } else {
                        SearchKeyword::create(['keyword' => strtolower($request->q), 'count' => 1, 'type' => 'Episode']);
                    }
                }
            }
        } catch(\Exception $ex) {
            
        }
        

        return response()->api([
                'status' => true,
                'message' => ''
            ]);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function shortenURL()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
                'url' => 'required'
            ], [
                'url.required' => config('config.common_message')
            ]);

        if ($validator->fails()) {
            return response()->api([
                    'status' => false,
                    'message' => $validator->errors()->first()
                ]);
        }

        $url = \UrlShortener::shorten($request->url); // Uses default driver as per config settings

        return response()->api([
                'status' => true,
                'data' => [
                    'share_url' => $url,
                ]
            ]);
    }

    /**
     * Undocumented function
     *
     * @param string $type
     * @return void
     */
    public function trending($type = 'individual', $noofrecords = 10)
    {
        $date = \Carbon\Carbon::today()->subDays(10);
        $trendingData = SearchKeyword::orderBy('count', 'DESC')->where('updated_at', '>=', date($date))
                ->take($noofrecords)->get(['id', 'keyword', 'type', 'show_id', 'count']);

        //dump($trendingData->toArray());

        $data = [];
        foreach ($trendingData as $item) {
            $link = '#';
            $show_id = '';
            if ($item->type == 'Show') {
                $link = Helper::make_slug($item->keyword) . '/episodes/' . $item->show_id;
                $show_id = $item->show_id;
            } else {
                $link = strtolower($item->keyword);
            }
            $data[] = [
                    'id' => $item->id,
                    'title' => $item->keyword,
                    'search_keyword' => $item->keyword,
                    'type' => $item->type,
                    'link' => $link,
                    'show_id' => $show_id,
                    //'count' => $item->count,
                ];
        }

        if ($type == 'individual') {
            return $data;
        } else {
            return response()->api([
                    'status' => true,
                    'message' => '',
                    'data' => $data
                ]);
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function totalEpisode()
    {
        $request = request();

        try {
            $totalcount = Episode::published()->count();

            return response()->api([
                    'status' => true,
                    'message' => '',
                    'data' => [
                        'total' => Helper::shorten_count($totalcount),
                        'trending' => $this->trending('individual', 5)
                    ]
                ]);
        } catch (\Exception $e) {
            return response()->api([
                    'status' => false,
                    'message' => 'Something is went wrong. Please try again.',
                    'data' => [
                        'items' => []
                    ]
                ]);
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function home_page_content()
    {
        $request = request();

        try {
            $totalcount = Episode::published()->count();

            $date = Carbon::today();

            /** Treading Episode Data */
            $trendingEpisodeData = SearchKeyword::orderBy('count', 'DESC')
                    ->where('type', 'Episode')
                    ->where('updated_at', '>=', date($date->subDays(10)))
                    ->take(10)->get(['id', 'keyword', 'type', 'show_id', 'count']);
            $data['episode'] = [];
            foreach ($trendingEpisodeData as $item) {
                $link = '#';
                $show_id = '';
                $link = strtolower($item->keyword);
                $data['episode'][] = [
                        'id' => $item->id,
                        'title' => $item->keyword,
                        'search_keyword' => $item->keyword,
                        'type' => $item->type,
                        'link' => $link,
                        'show_id' => $show_id
                    ];
            }            

            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'total' => Helper::shorten_count($totalcount),
                    'trending_episode' => $data['episode']                    
                ]
            ]);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => 'Something is went wrong. Please try again.',
                'data' => [
                    'total' => 0,
                    'trending_episode' => []                    
                ]
            ]);
        }
    }

    /**
     * Trending Show function
     *
     * @return void
     */
    public function home_page_content_trending_show()
    {
        $request = request();

        try {
            $date = Carbon::today();            

            /** Treading Show Data */
            $trendingShowData = SearchKeyword::orderBy('count', 'DESC')
                    ->where('type', 'Show')
                    ->where('updated_at', '>=', date($date->subDays(10)))
                    ->take(6)->pluck('id')->all();//get(['id', 'keyword', 'type', 'show_id', 'count']);
            
            shuffle($trendingShowData);
            
            $data['show'] = [];            
            foreach ($trendingShowData as $dataItem) {
                $item = SearchKeyword::where('id', $dataItem)->first(['id', 'keyword', 'type', 'show_id', 'count']);
                
                $link = '#';
                $show_id = '';
                $link = Helper::make_slug($item->keyword) . '/episodes/' . $item->show_id;
                $show_id = $item->show_id;

                //we fetched show data
                $showData = Show::find($item->show_id, ['image']);

                $data['show'][] = [
                    'id' => $item->id,
                    'title' => $item->keyword,
                    'search_keyword' => $item->keyword,
                    'type' => $item->type,
                    'image' => $showData->getWSImage(200),
                    'link' => $link,
                    'show_id' => $show_id
                ];
            }

            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'trending_show' => $data['show']                        
                ]
            ]);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => 'Something is went wrong. Please try again.',
                'data' => [
                    'trending_show' => []                    
                ]
            ]);
        }
    }

    /**
     * Collection function
     *
     * @return void
     */
    public function home_page_content_collection()
    {
        $request = request();

        try {            
            /** Collection data */
            //->orderByRaw("RAND()")
            $collection_data = Chart::Published()->orderBy('order')
                    ->orderByRaw("RAND()")->take(9)->get(['id', 'title', 'image']);

            $collectionArray = [];
            foreach ($collection_data as $key => $item) {
                if ($key == 8) {
                    break;
                }
                $collectionArray[] = [
                        'id' => $item->id,
                        'title' => $item->title,
                        'image' => $item->getImage(85),
                        'url_slug' => Helper::make_slug(trim($item->title))
                    ];
            }
            
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'collection_data' => $collectionArray,
                    'collection_data_length' => $collection_data->count()                        
                ]
            ]);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => 'Something is went wrong. Please try again.',
                'data' => [
                    'collection_data' => [],
                    'collection_data_length' => []                    
                ]
            ]);
        }
    }
     /**
     * Podcast of the day function
     *
     * @return void
     */
    public function home_page_content_podcast_of_the_day()
    {
        $request = request();

        try {       
            /** Podcast of the day */
            $date = Carbon::today();
            $podcast_day = PodcastDay::Published()->where('date', date($date->parse()->format('Y-m-d')))->take(1)->first();
            $podcastDayArray = [];
            if ($podcast_day) {
                $podcastDayArray = [
                    'id' => $podcast_day->show->id,
                    'title' => trim(str_replace("\n", '', $podcast_day->show->title)),
                    'description' => str_limit($podcast_day->show->description, 200, '...'),
                    'image' => $podcast_day->show->getWSImage(230),
                    'url_slug' => Helper::make_slug(trim($podcast_day->show->title))
                ];
            } else {
                $podcast_day = PodcastDay::published()->where('date', '<', date($date->parse()->format('Y-m-d')))->take(1)->first();
                $podcastDayArray = [
                    'id' => $podcast_day->show->id,
                    'title' => trim(str_replace("\n", '', $podcast_day->show->title)),
                    'description' => str_limit($podcast_day->show->description, 200, '...'),
                    'image' => $podcast_day->show->getWSImage(230),
                    'url_slug' => Helper::make_slug(trim($podcast_day->show->title))
                ];   
            }
            
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'podcast_day_array' => $podcastDayArray                        
                ]
            ]);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => 'Something is went wrong. Please try again.',
                'data' => [
                    'podcast_day_array' => []                    
                ]
            ]);
        }
    }

    /**
     * Latest Shows function
     *
     * @return void
     */
    public function home_page_content_latest_shows()
    {
        $request = request();
        try {                        
            $latest_shows_array = [];
            $latest_shows = Show::published()->has('episodes')->take(6)->orderByDesc('updated_at')->get(['id', 'title', 'image']);
            foreach ($latest_shows as $key => $item) {
                $latest_shows_array[] = [
                    'id' => $item->id,
                    'title' => trim(str_replace("\n", '', $item->title)),
                    'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                    'url_slug' => Helper::make_slug(trim($item->title))
                ];
            }

            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'latest_shows' => $latest_shows_array
                ]
            ]);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => 'Something is went wrong. Please try again.',
                'data' => [
                    'latest_shows' => []
                ]
            ]);
        }
    }

    public function home_page_content_ssr()
    {
        $request = request();

        try {
            $totalcount = Episode::published()->count();

            $date = Carbon::today();

            /** Treading Episode Data */
            $trendingEpisodeData = SearchKeyword::orderBy('count', 'DESC')
                    ->where('type', 'Episode')
                    ->where('updated_at', '>=', date($date->subDays(10)))
                    ->take(10)->get(['id', 'keyword', 'type', 'show_id', 'count']);
            $data['episode'] = [];
            foreach ($trendingEpisodeData as $item) {
                $link = '#';
                $show_id = '';
                $link = strtolower($item->keyword);
                $data['episode'][] = [
                        'id' => $item->id,
                        'title' => $item->keyword,
                        'search_keyword' => $item->keyword,
                        'type' => $item->type,
                        'link' => $link,
                        'show_id' => $show_id
                    ];
            }

            /** Treading Show Data */
            $trendingShowData = SearchKeyword::orderBy('count', 'DESC')
                    ->where('type', 'Show')
                    ->where('updated_at', '>=', date($date->subDays(10)))
                    ->take(6)->get(['id', 'keyword', 'type', 'show_id', 'count']);
            $data['show'] = [];
            foreach ($trendingShowData as $item) {
                $link = '#';
                $show_id = '';
                $link = Helper::make_slug($item->keyword) . '/episodes/' . $item->show_id;
                $show_id = $item->show_id;

                //we fetched show data
                $showData = Show::find($item->show_id, ['image']);

                $data['show'][] = [
                        'id' => $item->id,
                        'title' => $item->keyword,
                        'search_keyword' => $item->keyword,
                        'type' => $item->type,
                        'image' => $showData->getWSImage(200),
                        'link' => $link,
                        'show_id' => $show_id
                    ];
            }

            /** Collection data */
            $collection_data = Chart::Published()->orderBy('order')->orderBy('id', 'DESC')
                    ->take(9)->get(['id', 'title', 'image']);

            $collectionArray = [];
            foreach ($collection_data as $key => $item) {
                if ($key == 8) {
                    break;
                }
                $collectionArray[] = [
                        'id' => $item->id,
                        'title' => $item->title,
                        'image' => $item->getImage(85),
                        'url_slug' => Helper::make_slug(trim($item->title))
                    ];
            }

            /** Podcast of the day */
            $podcast_day = PodcastDay::Published()->where('date', date($date->parse()->format('Y-m-d')))->take(1)->first();
            $podcastDayArray = [];
            if ($podcast_day) {
                $podcastDayArray = [
                        'id' => $podcast_day->show->id,
                        'title' => trim(str_replace("\n", '', $podcast_day->show->title)),
                        'description' => str_limit($podcast_day->show->description, 200, '...'),
                        'image' => $podcast_day->show->getWSImage(230),
                        'url_slug' => Helper::make_slug(trim($podcast_day->show->title))
                    ];
            }

            // $top = Show::with(['episodes' => function ($q) {
            //     $q->orderBy('episodes.created_at', 'DESC');
            //   }])->take(6)->get(); // or get() or whatever
            // dd($top->toArray());
            
            $latest_shows_array = [];
            $latest_shows = Show::published()->take(6)->orderByDesc('updated_at')->get(['id', 'title', 'image']);
            foreach ($latest_shows as $key => $item) {
                $latest_shows_array[] = [
                    'id' => $item->id,
                    'title' => trim(str_replace("\n", '', $item->title)),
                    'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                    'url_slug' => Helper::make_slug(trim($item->title))
                ];
            }

            return response()->api([
                    'status' => true,
                    'message' => '',
                    'data' => [
                        'total' => Helper::shorten_count($totalcount),
                        'trending_episode' => $data['episode'],
                        'trending_show' => $data['show'],
                        'collection_data' => $collectionArray,
                        'collection_data_length' => $collection_data->count(),
                        'podcast_day_array' => $podcastDayArray,
                        'latest_shows' => $latest_shows_array
                    ]
                ]);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => 'Something is went wrong. Please try again.',
                'data' => [
                    'total' => 0,
                    'trending_episode' => [],
                    'trending_show' => [],
                    'collection_data' => [],
                    'collection_data_length' => [],
                    'podcast_day_array' => [],
                    'latest_shows' => []
                ]
            ]);
        }
    }
}
