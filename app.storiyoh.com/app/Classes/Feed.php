<?php
namespace App\Classes;

use App\Models\Feed as FeedModel;
use Carbon\Carbon;

/**
 * Description of Feed
 *
 * @author bcm
 */
class Feed {
    
    // Follow User, Follow Board, Subscribe Show, Episode Like, New Board,
    // Board Update, Show Update, Episode Comment.    
    const FOLLOW_BOARD      = 'Follow Board';
    const FOLLOW_USER       = 'Follow User';
    const SUBSCRIBE_SHOW    = 'Subscribe Show';
    const EPISODE_LIKE      = 'Episode Like';
    const NEW_BOARD         = 'New Board';
    const BOARD_UPDATE      = 'Board Update';
    const SHOW_UPDATE       = 'Show Update';
    const EPISODE_COMMENT   = 'Episode Comment';
    const EPISODE_HEARD     = 'Episode Heard';
    const FOLLOW_SMARTLIST  = 'Follow Smartlist';

    /* New Code Added 01/09/2018 */
    /** Add POST Types */
    const NEW_SHOW_POST = 'New Show Post';
    const NEW_EPISODE_POST = 'New Episode Post';
    const NEW_PLAYLIST_POST = 'New Playlist Post';
    const NEW_SMARTPLAYLIST_POST = 'New Smart Playlist Post';
    const NEW_COLLECTION_POST = 'New Collection Post';

    /** Repost Types */
    const SHOW_REPOST = 'Show Repost';
    const EPISODE_REPOST = 'Episode Repost';
    const PLAYLIST_REPOST = 'Playlist Repost';
    const SMARTPLAYLIST_REPOST = 'Smart Playlist Repost';
    const COLLECTION_REPOST = 'Collection Repost';

    /** Like Types */
    const SHOW_POST_LIKE = 'Show Post Like';
    const EPISODE_POST_LIKE = 'Episode Post Like';
    const PLAYLIST_POST_LIKE = 'Playlist Post Like';
    const SMARTPLAYLIST_POST_LIKE = 'Smart Playlist Post Like';
    const COLLECTION_POST_LIKE = 'Collection Post Like';

    /** Comment Types */
    const SHOW_POST_COMMENT = 'Show Post Comment';
    const EPISODE_POST_COMMENT = 'Episode Post Comment';
    const PLAYLIST_POST_COMMENT = 'Playlist Post Comment';
    const SMARTPLAYLIST_POST_COMMENT = 'Smart Playlist Post Comment';
    const COLLECTION_POST_COMMENT = 'Collection Post Comment';

    /** Comment Types */
    const SHOW_POST_COMMENT_TAG = 'Show Post Comment Tag';
    const EPISODE_POST_COMMENT_TAG = 'Episode Post Comment Tag';
    const PLAYLIST_POST_COMMENT_TAG = 'Playlist Post Comment Tag';
    const SMARTPLAYLIST_POST_COMMENT_TAG = 'Smart Playlist Post Comment Tag';
    const COLLECTION_POST_COMMENT_TAG = 'Collection Post Comment Tag';

    /** Contributor */
    const PLAYLIST_CONTRIBUTOR = 'Playlist Contributor';
    const PLAYLIST_CONTRIBUTOR_EPISODE = 'Playlist Contributor Episode';
    
    /**
     *
     * @var Model
     */
    protected $data;
    
    /**
     *
     * @var int
     */
    protected $interval = 1; // Default 1 Day
    
    /**
     * Feed constructor.
     * @var $data Model
     */
    public function __construct($data) {
        $this->data = $data;
    }
    
    /**
     * 
     * @param array|string $content
     * @param string $type
    */    
    public function add($content, $type) 
    {
        $content = is_array($content) ? $content : [$content];
        
        if($type == self::FOLLOW_USER) {
            $activity = $this->getActivity($type);
            
            $content = array_merge(json_decode($activity->data), $content);
            
            return $this->update($activity, array_unique($content));
        }
        
        //return [$typeable, $typeable_id, $content, $content_type];
        $attributes = [            
            'data' => json_encode($content),
            'type' => $type
        ];
        
        return $this->data->activities()->create($attributes);
    }
    
    /**
     * 
     * @param FeedModel $feed
     * @param array $content
     * @return FeedModel
     */
    public function update($feed, $content) 
    {
        $attributes = ['data' => json_encode($content)];
        
        return $feed->fill($attributes)->save();
    }
    
    /**
     * 
     * @param string $type
     * @return FeedModel
     */
    protected function getActivity($type = self::FOLLOW_USER)
    {
        $from = Carbon::now()->subDays($this->interval);
        
        $activity = $this->data->activities()->whereType($type)
                ->where('updated_at', '>', $from)->latest()->first();
        
        return $activity ? : new FeedModel([
            'typeable_type' => "App\Models\\" . class_basename($this->data),
            'typeable_id' => $this->data->id,
            'data' => "[]",
            'type' => $type
        ]);
    }
    
    /**
     * 
     * @param int $interval
     * @return Feed
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;
        
        return $this;
    }
}
