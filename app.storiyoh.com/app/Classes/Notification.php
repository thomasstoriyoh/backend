<?php

namespace App\Classes;

/**
 * Description of Notification
 *
 * @author bcm
 */
class Notification
{
    const SHOW_UPDATE = 'Show Update';

    /**
     *
     * @var Model
     */
    protected $data;

    /**
     * Feed constructor.
     * @var $data Model
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     *
     * @param array|string $content
     * @param string $type
    */
    public function add($content, $type)
    {
        $content = is_array($content) ? $content : [$content];

        $attributes = [
            'data' => json_encode($content),
            'type' => $type
        ];

        return $this->data->activities_show()->create($attributes);
    }
}
