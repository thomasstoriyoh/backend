<?php
namespace App\ClassesV3;
//use Tzsk\ScrapePod\Facade\ScrapePod;
use App\Models\Show;
use App\Models\Episode;
use App\Models\EpisodeTag;
use App\Classes\Feed;
use Log;
use Carbon\Carbon;
use App\Traits\Helper;

class AddNewEpisode
{
    /**
     * Show id variable
     *
     * @var [type]
     */
    protected $show_id;

    /**
     * Episode Data variable
     *
     * @var [type]
     */
    protected $episodeData;

    /**
     * Constructor
     *
     * @param $api_type
     * @param $user
     */
    public function __construct($show_id, $episodeData)
    {
        $this->show_id = $show_id;
        $this->episodeData = $episodeData;
    }

    /**
     * This function is use fetching new episodes
     *
     * @return void
     */
    public function fetchingEpisodes() 
    {
        try {
            //Fetch Data from Feed URL
            //$response = ScrapePod::feed($item['feed_url']);

            $response = $this->episodeData;
            
            if (count($response['data']['episodes']) > 0) {

                $showData = Show::with('categories')->where("id", $this->show_id)->first();
                
                $no_of_shows = 0;
                //Added all episodes to tbl_episodes table
                $showIds = [];
                foreach ($response['data']['episodes'] as $data) {
                    try {
                        $no_of_shows++;
                        
                        //If same MP3 found than we will not added this episode
                        //and continue to next episode
                        $checkEpisodeMp3Data = Episode::where('show_id', $showData->id)->where('mp3', $data['mp3'])->count();
                        if ($checkEpisodeMp3Data) {
                            continue;
                        }

                        $uniqueID = md5($data['title'] . $showData->id . $data['size'] . $data['duration'] . $data['published_at']);

                        $episode = Episode::firstOrNew(['uuid' => $uniqueID]);
                        
                        $newOrOld = $episode->exists;
                        
                        $attributes = $this->getEpisodeAttributes($data, $showData);
                        $episode->fill($attributes)->save();                                                                        

                        $tags = $this->getTags($data);
                        $episode->tags()->sync($tags);

                        $categories = $showData->categories->pluck('id')->all();
                        $categories = array_filter($categories);
                        $episode->categories()->sync($categories);

                        if (!empty($attributes['show_id']) && !$newOrOld) {
                            $showIds[$attributes['show_id']][] = trim($episode->id);
                            $episode->searchable();
                        }                        
                    } catch (\Exception $e) {
                        Log::info($e->getMessage());
                        continue;
                    }
                }

                if (!empty($showIds)) {
                    $showData->fill([
                        'web_url' => @$response['data']['site'],
                        //'no_of_episode' => $no_of_shows,
                        //'content_length' => @$length ? $length : 0,
                        //'last_update_timestamp' => $timestamp > 0 ? Carbon::createFromTimestamp($timestamp) : NULL
                        //'status' => $no_of_shows > 0 ? 'Published' : 'Draft'
                    ])->save();
                    $showData->searchable();
                }

                if (!empty($showIds)) {
                    $subscribeUserData = $showData->subscribers()->wherePivot('notify', 1)->pluck('id')->all();
                    if (count($subscribeUserData) > 0) {
                        $this->makeActivity(array_filter($showIds));
                        //Insert Into Push Table
                        Helper::addPushNotification($showData, $showIds, $subscribeUserData);
                    }                                
                }                                
            }
        } catch (\Exception $e) {
            //$this->error($e->getMessage());
            Log::info($e->getMessage());         
        }                                
    }

    /**
     * @param array $showIds
     * @return void
    */
    protected function makeActivity($showIds)
    {
        $shows = Show::whereIn('id', array_keys($showIds))->get(['id', 'updated_at']);
        foreach ($shows as $show) {
            $show->activities()->create([
                'type' => Feed::SHOW_UPDATE,
                'data' => json_encode($showIds[$show->id])
            ]);
            $show->touch();
        }
    }

    /**
     * @param $data
     * @return array
    */
    protected function getEpisodeAttributes($data, $show)
    {
        $date_created = null;
        if (!empty(@$data['published_at'])) {
            $date_created = Carbon::parse(@$data['published_at'])->format('Y-m-d');
            if ($date_created == '1970-01-01') {
                $date_created = null;
            }
        }

        $date_added = null;
        if (!empty(@$data['published_at'])) {
            $date_added = Carbon::parse(@$data['published_at'])->format('Y-m-d H:i:s');
            if ($date_added == '1970-01-01 05:30:00') {
                $date_added = null;
            }
        }

        $duration = 0;
        if (@$data['duration']) {
            if ($data['duration'] > 0) {
                $duration = $data['duration'];
            }
        }

        $size = 0;
        if (@$data['size']) {
            if ($data['size'] > 0) {
                $size = $data['size'];
            }
        }
        

        $attributes = [
            'show_id' => @$show->id,
            'itunes_id' => @$show->itunes_id,
            'title' => empty($data['title']) ? '-' : $data['title'],
            'description' => @$data['description'],
            'image' => @$data['image'],
            'duration' => $duration,
            'size' => $size,
            'mp3' => @$data['mp3'],
            'link' => @$data['link'],
            'date_created' => $date_created,
            'date_added' => $date_added,
            'created_by' => 1,
            'updated_by' => 1
        ];

        return $attributes;
    }

    /**
     * @param $data
     * @return array
    */
    protected function getTags($data)
    {
        $tags = [];
        $keywordsData = array_filter($data['keywords']);
        if (count($keywordsData)) {
            foreach ($keywordsData as $tag) {
                if (strlen($tag) <= 191) {
                    $tag = EpisodeTag::firstOrCreate(['title' => $tag]);
                    $tags[] = $tag->id;
                } else {
                    continue;
                }
            }
        }

        return $tags;
    }
}