<?php

namespace App\ClassesV3;

use App\Models\Chart;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Traits\Helper;
use App\Traits\ResponseFormat;

class Collection
{
    use ResponseFormat;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    /**
     * Constructor
     *
     * @param $api_type
     * @param $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * This function is use for collection listing
     *
     * @return void
     */
    public function Collections()
    {
        $request = request();

        try {
            $query = Chart::Published()->orderBy('order')->orderBy('id', 'DESC');

            if ($request->tag) {
                $query->where('keywords', 'LIKE', '%' . $request->tag . '%');
            }

            $collections = $query->paginate(12, ['id', 'title', 'description', 'date', 'image']);

            $collectionArray = [];
            foreach ($collections as $item) {
                $collectionArray[] = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'date' => Carbon::parse($item->date)->format('jS M Y'),
                    'description' => str_limit($item->description, 100, '...'),
                    'image' => $item->getImage(200),
                    'url_slug' => Helper::make_slug(trim($item->title))
                ];
            }

            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'total' => $collections->total(),
                    'total_number_format' => number_format($collections->total()),
                    'per_page' => $collections->perPage(),
                    'pages' => ceil($collections->total() / $collections->perPage()),
                    'items' => $collectionArray
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.error"),
                    'data' => [
                        'items' => []
                    ]
                ]);
        }
    }

    /**
     * This function is use for collection show
     *
     * @return void
     */
    public function Show()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
                'collection_id' => 'required'
            ], [
                'collection_id.required' => config('language.' . $this->getLocale() . ".Collection.missing_parameter_id"),
            ]);

        if ($validator->fails()) {
            return response()->api([
                    'status' => false,
                    'message' => $validator->errors()->first()
                ]);
        }

        $collection = Chart::Published()->where('id', $request->collection_id)
                ->first(['id', 'title', 'description', 'date', 'keywords', 'source_url', 'image']);

        if (!$collection) {
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Common.no_collection_found_lbl"),
                'redirect' => 'yes',
                'data' => []
            ]);
        }

        $podcasts = $collection->chart_poadcast()->Published()->orderBy('chart_show.order')
                ->get(['id', 'title', 'image']);

        $subscribers = [];
        if ($this->api_type == 'api') {
            $subscribers = $this->userData->subscribers()->pluck('show_id')->all();
        }

        $podcastArray = [];
        foreach ($podcasts as $podcast) {
            $subscribe = false;
            if ($this->api_type == 'api') {
                $subscribe = @in_array($podcast->id, $subscribers) ? 'true' : 'false';
            }
            $podcastArray[] = [
                'id' => $podcast->id,
                'title' => $podcast->title,
                'image' => $podcast->getWSImage(150),
                'url_slug' => Helper::make_slug(trim($podcast->title)),
                'no_of_episode' => $podcast->episodes()->count(),
                'subscribe' => $subscribe
            ];
        }

        $tags = [];
        if (!empty($collection->keywords)) {
            $data = explode(',', $collection->keywords);
            $data = array_filter($data);
            foreach ($data as $item) {
                $tags[] = [
                    'id' => trim($item),
                    'title' => trim($item),
                ];
            }
        }

        //fetch trending data
        $trending_data = [];
        if ($this->api_type == 'web') {
            $trending_data = Helper::trending_data();
        }

        $collectionData = [
            'id' => $collection->id,
            'title' => $collection->title,
            'date' => Carbon::parse($collection->date)->format('jS M Y'),
            'description' => $collection->description,
            'source_url' => !empty($collection->source_url) ? $collection->source_url : '',
            'tags' => $tags,
            'collection_tags' => $collection->keywords,
            'image' => $collection->getImage(400),
            'url_slug' => config('config.live_url') . '/collections/' . $collection->id . '/' . Helper::make_slug(trim(@$collection->title)),
            'podcasts' => $podcastArray,
            'trending_data' => $trending_data
        ];

        return response()->api([
            'status' => true,
            'redirect' => 'no',
            'data' => $collectionData
        ]);
    }
    
    /**
     * Url Shortner for collection
     *
     * @return void
     */
    public function  url_shortner() {
        
        $request = request();
        
        $validator = Validator::make($request->all(), [
            'collection_id' => 'required'
        ], [
            'collection_id.required' => config('language.' . $this->getLocale() . ".Collection.missing_parameter_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        // Show Episode
        $collection = Chart::where('id', $request->collection_id)->first(['id', 'title']);

        if (!$collection) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_collection_found_lbl")
            ]);
        }
        

        return response()->api([
            'status' => true,
            'data' => [
                "share_url" => config('config.live_url') . '/collections/' . $collection->id . '/' . Helper::make_slug(trim(@$collection->title)),
            ]
        ]);
    }
}
