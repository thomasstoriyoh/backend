<?php

namespace App\ClassesV3;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class Firebase
{
    /**
     * Subscribed token from speciifc topic
     */
    public function subscribetoATopic($topicId, $registrationTokens) {

        $topic = config('config.firebase_topic_pre_text').$topicId;
        
        $acc = ServiceAccount::fromJsonFile(storage_path(config('config.firebase_config_file')));
        $firebase = (new Factory)->withServiceAccount($acc)->create();
        
        return $firebase->getMessaging()->subscribeToTopic($topic, $registrationTokens);        
    }

    /**
     * Unsubscribed token from speciifc topic
     */
    public function unSubscribefromTopic($topicId, $registrationTokens) {        
        
        $topic = config('config.firebase_topic_pre_text').$topicId;

        $acc = ServiceAccount::fromJsonFile(storage_path(config('config.firebase_config_file')));
        $firebase = (new Factory)->withServiceAccount($acc)->create();

        return $firebase->getMessaging()->unsubscribeFromTopic($topic, $registrationTokens);        
    }

    /**
     * Send push notification for topic with different data
     */
    public function sendApnandAndroidMessagetoTopic($data = []) {
        if (count($data) > 0) {
            $acc = ServiceAccount::fromJsonFile(storage_path(config('config.firebase_config_file')));
            $firebase = (new Factory)->withServiceAccount($acc)->create();

            return $firebase->getMessaging()->send($data);
        }
        return true;
    }
}