<?php

namespace App\ClassesV3;

use App\Models\Show;
use Illuminate\Support\Facades\Validator;
use App\Models\Episode;
use Carbon\Carbon;
use App\Traits\Helper;
use App\Models\User;
use App\Traits\ResponseFormat;

class Podcast2_2
{
    use ResponseFormat;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * This function is use for show listing
     *
     * @return void
     */
    public function Shows()
    {
        $request = request();

        try {
            //Elastic Search
            $query = Show::search('*')->where('status', 'published');

            if (! empty($request->country)) {
                $query->where('country', $request->country);
            }
            if (! empty($request->language)) {
                $query->where('language', $request->language);
            }
            if (! empty($request->filter_category)) {
                $query->where('category_ids', '"' . $request->filter_category . '"');
            }

            if ($request->sort_by == 'recency') {
                $query->orderBy('recency', 'DESC');
            }

            $noofrecords = config('config.pagination.show');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->no_paginate == 'Yes') {
                $all_shows = $query->get();
            } else {
                $all_shows = $query->paginate($noofrecords);
            }

            $subscribers = [];
            if ($this->api_type == 'api') {
                $subscribers = $this->userData->subscribers()->pluck('show_id')->all();
            }

            $data = [];
            foreach ($all_shows as $item) {
                $subscribe = false;
                if ($this->api_type == 'api') {
                    $subscribe = @in_array($item->id, $subscribers) ? 'true' : 'false';
                }
                $data[] = [
                    'id' => $item->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    //'category' => $item->categories()->pluck('title', 'id')->all(),
                    'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                    'no_of_episodes' => $item->episodes()->count(),
                    'no_of_subscribers' => $item->subscribers->count(),
                    'subscribe' => $subscribe
                ];
            }

            $message = '';
            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
            } else {
                return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_shows->total(),
                    'per_page' => $all_shows->perPage(),
                    'pages' => ceil($all_shows->total() / $all_shows->perPage()),
                    'items' => $data
                ]
            ]);
            }
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * This function is use for show search
     *
     * @return void
     */
    public function Search()
    {
        $request = request();

        try {
            //Elastic Search
            $query = Show::search($request->keyword)->where('status', 'published');

            if (! empty($request->country)) {
                $query->where('country', $request->country);
            }
            if (! empty($request->language)) {
                $query->where('language', $request->language);
            }
            if (! empty($request->filter_category)) {
                $query->where('category_ids', '"' . $request->filter_category . '"');
            }

            if ($request->sort_by == 'recency') {
                $query->orderBy('recency', 'DESC');
            }

            $noofrecords = config('config.pagination.show');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->no_paginate == 'Yes') {
                $all_shows = $query->where('status', 'published')->get();
            } else {
                $all_shows = $query->where('status', 'published')->paginate($noofrecords);
            }

            $subscribers = [];
            if ($this->api_type == 'api') {
                $subscribers = $this->userData->subscribers()->pluck('show_id')->all();
            }
            //dd($subscribers);

            $data = [];
            foreach ($all_shows as $show) {
                $subscribe = false;
                if ($this->api_type == 'api') {
                    $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';
                }
                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'url_slug' => Helper::make_slug($show->title),
                    //'category' => $show->categories()->pluck('title', 'id')->all(),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'no_of_episodes' => $show->episodes()->count(),
                    'no_of_subscribers' => $show->subscribers->count(),
                    'subscribe' => $subscribe,
                    'premium' => $show->content_type != 0 ? 'yes' : 'no'
                ];
            }

            $message = '';
            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'items' => $data,
                    ]
                ]);
            } else {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'total' => $all_shows->total(),
                        'per_page' => $all_shows->perPage(),
                        'pages' => ceil($all_shows->total() / $all_shows->perPage()),
                        'items' => $data,
                    ]
                ]);
            }
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * This function is use for show details page
     *
     * @return void
     */
    public function Show()
    {
        $request = request();
       
        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Show
        $show = Show::published()->where('id', $request->show_id)->withCount('subscribers')->with('categories')
            ->first(['id', 'title', 'image', 'language', 'artist_name', 'description', 'updated_at']);

        if (!$show) {            
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                'redirect' => 'yes',
                'data' => []
            ]);
        }

        $episodeArray = [];        
        // if ($this->api_type == 'web') {
        //     $categoryIds = $show->categories()->pluck('id')->all();
        //     $query = Show::published()->has('episodes')->withCount('episodes');
        //     $query->where(function ($query) use ($categoryIds) {
        //         $query->orWhereHas('categories', function ($q) use ($categoryIds) {
        //             $q->whereIn('id', $categoryIds);
        //         });
        //     });            
        // }

        $subscribe = false;
        if ($this->api_type == 'api' && $show) {
            $subscribers = $this->userData->subscribers()->pluck('show_id')->all();
            $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';
        }

        // Marketplace code start
        $show_language = !is_null($show->language) ? $show->language : "";
        $language = "";
        if (isset(config('config.languages')[$show_language])) {
            $language = config('config.languages')[$show_language];
        }

        /* No of Episode Count */
        $total_episode = 0;
        if ($show->content_type != 0) {
            $total_episode = \App\Models\EpisodeDetail::where('show_id', $show->id)->where('status', 'Published')->count();
        } else {
            $total_episode = $show->episodes()->count();
        }
        /* No of Episode Count */
                
        $purchase = "no";
        $country_id = 1;
        $currency_id = 1;
        $selling_currency = "INR";
        $selling_price = "";
        if ($show->content_type != 0) {
            //Ip2Location code start            
            $ip_address = $request->ip_address;
            $responseData = new V3_3\IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();
            if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
                //Check if marketplace available in user country
                $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                    ->where('country_alpha2_code', $location['data']['country_code'])
                    ->first(["id", "title", "country_alpha2_code", "currency_id"]);
                if ($marketplace_country) {
                    // $product_price = $show->premium_pricing()->where('country_id', $marketplace_country->id)
                    //     ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    // $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
                    $country_id = $marketplace_country->id;
                    $currency_id = $marketplace_country->currency_id;
                    $selling_currency = $marketplace_country->currency->title;                    
                }
            }
            $product_price = $show->premium_pricing()->where('country_id', $country_id)->where('currency_id', $currency_id)
                ->first(['selling_price']);
            $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";

            if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
                $purchase = "free";
            }
        }        
        // Marketplace code end



        //Fetch trending data
        $subscribersArray = [];       
        if ($this->api_type == 'web') {
            $more_episode_link = Helper::make_slug($show->title).'/episodes/'.$show->id;

            // $latest_episode = $show->episodes()->published()->orderBy('date_added', 'DESC')->limit(3)->get(['id', 'title','show_id', 'date_added', 'duration']);

            $query = \App\Models\Episode::where('show_id', $request->show_id);

            $orderIDs = [];
            if ($show->content_type != 0) {
                $orderIDs = \App\Models\EpisodeDetail::where('show_id', $request->show_id)->orderBy('order', 'asc')->pluck('episode_id')->all();
                $query->published();
            }
            if (count($orderIDs) > 0) {
                $orderIDs = implode(",", $orderIDs);
                $query->orderByRaw(\DB::raw("FIELD(id, " . $orderIDs . " )"));
            }

            $latest_episode = $query->orderBy('id', 'DESC')->limit(3)->get(['id', 'title', 'show_id', 'date_added', 'duration']);

            $latestEpisodeArray = [];
            $premium = $show->content_type != 0 ? 'yes' : 'no';
            if ($latest_episode) {
                foreach ($latest_episode as $key => $value) {
                    $selling_episode_price = "";
                    $episode_purchase = 'no';
                    //if (isset($marketplace_country)) {
                        if ($show->content_type != 0) {
                            $product_episodeprice = $value->premium_pricing()->where('country_id', $country_id)
                                ->where('currency_id', $currency_id)->first(['selling_price']);
                            $selling_episode_price = !empty($product_episodeprice->selling_price) ?  $product_episodeprice->selling_price : "";
                            if (in_array($selling_episode_price, ['0', '0.0', '0.00', '0.000'])) {
                                $episode_purchase = "free";
                            }
                        }
                    //}
                    $latestEpisodeArray[] = [
                        'id' => $value->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($value->title))),
                        'url_slug' =>Helper::make_slug($value->title),
                        'duration' => $value->getDurationText(),
                        'show_id' => $value->show->id,
                        'image' => !empty($value->show->image) ? $value->show->getWSImage(200) : asset('uploads/default/show.png'),
                        'last_updated' => Carbon::parse($value->date_added)->format('jS M Y'),
                        'premium' => $premium,
                        'selling_price' => $selling_episode_price,
                        'selling_currency' => $selling_currency,
                        'purchase' => $episode_purchase,                                
                    ];
                }
            }

            $subscribers = $show->subscribers()->verified()->take(10)->get(['id','image','first_name','last_name']);
             if ($subscribers) {
                foreach ($subscribers as $key => $value1) {
                    $subscribersArray[] = [
                        'key' => $key + 1,
                        //'id' => $value1->id,
                        'name' => trim($value1->first_name . ' ' . $value1->last_name),
                        'user_image' => !empty($value1->image) ? $value1->getImage(100) : asset('uploads/default/user.png'),
                    ];
                }
            }                        
        }

        $User_Data = [];
        $chart_arr = []; 

        $data = [
            'id' => $show->id,
            'show_name' => trim(str_replace("\n", '', html_entity_decode($show->title))),
            'show_image' => !empty($show->image) ? $show->getWSImage(400) : asset('uploads/default/show.png'),
            'show_slug' => Helper::make_slug(trim(@$show->title)),
            "share_url" => config('config.live_url')."/podcasts/".$show->id."/".Helper::make_slug(trim(@$show->title)),
            'category' => @implode(', ', @$show->categories()->pluck('title')->all()),
            'show_desc' => $this->api_type == 'api' ? strip_tags(trim($show->description)) : $show->getOriginal('description'),
            'artist_name' => @$show->artist_name,
            'network_name' => @$show->network->title,
            //'no_of_episodes' => $show->episodes()->published()->count(),
            'no_of_episodes' => $total_episode,
            'last_updated' => $show->updated_at->diffForHumans(),
            'subscribe' => $subscribe,
            'no_of_subscribers' => $show->subscribers()->verified()->count(),
            'subscribers' => $subscribersArray,
            'episodes' => $this->api_type == 'web' ? $latestEpisodeArray : $episodeArray,
            'more_episode_link' => $this->api_type == 'web' ? $more_episode_link : '',
            'subscribe_to_connection' => $User_Data,
            'chart_collection' => $chart_arr,

            'owner' => $show->content_type != 0 ? $show->user->first_name : '',
            'premium' => $show->content_type != 0 ? 'yes' : 'no',
            'standalone_show' => $show->content_type == 2 ? 'yes' : 'no',
            'show_language' => $language,
            'show_rating' => !is_null($show->rating) ? $show->rating : "",
            'selling_price' => $selling_price,
            'selling_currency' => $selling_currency,
            'purchase' => $purchase,
            'free_first' => @$latestEpisodeArray[0]['purchase'] == 'free' ? 'yes' : 'no',
            'btn_label' => is_null($show->btn_label) ? "" : $show->btn_label,
        ];

        return response()->api([
            'status' => true,
            'redirect' => 'no',
            'data' => $data
        ]);
    }    

    /**
     * This function is use for all show episodes
     *
     * @return void
     */
    public function Episodes()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $show = Show::where('id', $request->show_id)->first(['id', 'user_id', 'title', 'image', 'language', 'rating', 'content_type']);
        
        $purchase = "no";
        $country_id = 1;
        $currency_id = 1;
        $selling_currency = "INR";
        $selling_price = "";
        if ($show->content_type != 0) {
            //Ip2Location code start            
            $ip_address = $request->ip_address;
            //$ip_address = "183.87.113.164";
            $responseData = new V3_3\IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();
            if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
                //Check if marketplace available in user country
                $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                    ->where('country_alpha2_code', $location['data']['country_code'])
                    ->first(["id", "title", "country_alpha2_code", "currency_id"]);
                if ($marketplace_country) {
                    // $product_price = $show->premium_pricing()->where('country_id', $marketplace_country->id)
                    //     ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    // $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
                    $country_id = $marketplace_country->id;
                    $currency_id = $marketplace_country->currency_id;
                    $selling_currency = $marketplace_country->currency->title;
                }
            }
            $product_price = $show->premium_pricing()->where('country_id', $country_id)->where('currency_id', $currency_id)
                ->first(['selling_price']);
            $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";            
            if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
                $purchase = "free";
            }
        }

        //All Episodes
        $query = Episode::where('show_id', $request->show_id);

        $orderIDs = [];
        if ($show->content_type != 0) {
            $orderIDs = \App\Models\EpisodeDetail::where('show_id', $request->show_id)->orderBy('order', 'asc')->pluck('episode_id')->all();
            $query->published();
        }
        if (count($orderIDs) > 0) {
            $orderIDs = implode(",", $orderIDs);
            $query->orderByRaw(\DB::raw("FIELD(id, " . $orderIDs . " )"));
        } 

        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (! empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        //$query->groupBy('mp3');

        $query->orderBy('id', 'DESC');

        $all_show_episode = $query->paginate($noofrecords, ['id', 'title', 'duration', 'mp3', 'show_id', 'date_added', 'updated_at', 'date_created', 'listen_count', 'explicit']);

        $data = [];
        $premium = $show->content_type != 0 ? 'yes' : 'no';
        foreach ($all_show_episode as $item) {
            $selling_episode_price = "";
            $episode_purchase = 'no';
            //if (isset($marketplace_country)) {
                if ($show->content_type != 0) {
                    $product_episodeprice = $item->premium_pricing()->where('country_id', $country_id)
                        ->where('currency_id', $currency_id)->first(['selling_price']);
                    $selling_episode_price = !empty($product_episodeprice->selling_price) ?  $product_episodeprice->selling_price : "";
                    if (in_array($selling_episode_price, ['0', '0.0', '0.00', '0.000'])) {
                        $episode_purchase = "free";
                    }
                }
            //}

            $data[] = [
                'id' => $item->id,
                'show_id' => $item->show_id,
                'show_title' => html_entity_decode(@$item->show->title),
                "share_url" => config('config.live_url')."/episode/".$item->id."/".Helper::make_slug(trim(@$item->title)),
                'show_slug' => Helper::make_slug(trim(@$item->show->title)),
                'episode_title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'url_slug' => Helper::make_slug(trim(@$item->title)),
                'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                'audio_file' => $item->getAudioLink(),
                'duration' => $item->getDurationText(),
                'listen_count' => Helper::shorten_count($item->listen_count), 
                "explicit" => $item->show->explicit,
                'created_date' => \Carbon\Carbon::parse($item->date_added)->format('jS M Y'),
                'premium' => $premium,
                'selling_price' => $selling_episode_price,
                'selling_currency' => $selling_currency,
                'purchase' => $episode_purchase,
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
        }

        // Marketplace code start
        $show_language = !is_null($show->language) ? $show->language : "";
        $language = "";
        if (isset(config('config.languages')[$show_language])) {
            $language = config('config.languages')[$show_language];
        }        
        // Marketplace code end

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_show_episode->total(),
                'total_number_format' => number_format($all_show_episode->total()),
                'per_page' => $all_show_episode->perPage(),
                'pages' => ceil($all_show_episode->total() / $all_show_episode->perPage()),
                'show_title' => $show->title,
                'show_image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),                
                'items' => $data,
                'owner' => $show->content_type != 0 ? $show->user->first_name : '',
                'premium' => $show->content_type != 0 ? 'yes' : 'no',
                'standalone_show' => $show->content_type == 2 ? 'yes' : 'no',
                'show_language' => $language,
                'show_rating' => !is_null($show->rating) ? $show->rating : "",
                'selling_price' => $selling_price,
                'selling_currency' => $selling_currency,
                'purchase' => $purchase,
                'free_first' => @$data[0]['purchase'] == 'free' ? 'yes' : 'no',
                'btn_label' => ! is_null($item->show->btn_label) ? $item->show->btn_label :  '',
            ]
        ]);
    }

    /**
     * This function is use for test
     *
     * @return void
     */
    public function Episodes_Test()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('config.missing_parameter1').' show id'. config('config.missing_parameter2').' Podcast Episodes.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $show = Show::where('id', $request->show_id)->first(['title', 'image']);

        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (! empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        $max = $noofrecords;
        $page_no = $request->page ? $request->page : 1; 
        $start = ($page_no) ? ($page_no-1) * $max : 0;

        $query = \DB::table('episodes')->distinct('mp3')->select('mp3', 'date_created')
            ->where('show_id', $request->show_id);

        $all_show_episode_count_clone = clone $query;

        $all_show_episode_count = $all_show_episode_count_clone->get()->count();

        $query->skip($start)->take($max);
                
        $query->orderBy('date_created', 'DESC');

        $all_show_episode = $query->get();

        $data = [];
        foreach ($all_show_episode as $item_val) {
            $item = Episode::where('show_id', $request->show_id)
                ->where('mp3', $item_val->mp3)->first(['id', 'title', 'duration', 'mp3', 'show_id', 'date_added','updated_at', 'date_created']);
            $data[] = [
                'id' => $item->id,
                'show_id' => $item->show_id,
                'show_title' => html_entity_decode(@$item->show->title),
                "share_url" => config('config.live_url')."/episode/".$item->id."/".Helper::make_slug(trim(@$item->title)),
                'show_slug' => Helper::make_slug(trim(@$item->show->title)),
                'episode_title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'url_slug' => Helper::make_slug(trim(@$item->title)),
                'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                'audio_file' => $item->getAudioLink(),
                'duration' => $item->getDurationText(),
                'created_date' => \Carbon\Carbon::parse($item->date_added)->format('jS M Y')
            ];            
        }
        
        $message = '';
        if (count($data) == 0) {
            $message = 'No Episode Found.';
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_show_episode_count,
                'total_number_format' => number_format($all_show_episode_count),
                'per_page' => $max,
                'pages' => ceil($all_show_episode_count / $max),                
                'show_title' => $show->title,
                'show_image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for show autosearch
     *
     * @return void
     */
    public function AutoSearch()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'q' => 'required'
        ], [
            'q.required' => config('language.' . $this->getLocale() . ".Common.error")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $showData = Show::published()->where('title', 'LIKE', '%'. $request->q .'%')->has('episodes')
            ->take(5)->pluck('id', 'title')->all();

        $data = [];
        foreach($showData as $key => $item) {
            $data[$key] = $item."****".Helper::make_slug(trim($key));
        }
        
        /*
        * Results array
        */
        $results = array();
        /*
        * Autocomplete formatter
        */
        function autocomplete_format($results) {
            foreach ($results as $result) {
                echo $result[0] . '|' . $result[1] . "\n";
            }
        }
        /*
        * Search for term if it is given
        */
        if (! empty($request->q)) {
            $q = strtolower($request->q);
            if ($q) {
                foreach ($data as $key => $value) {
                    if (strpos(strtolower($key), $q) !== false) {
                        $results[] = array($key, $value);
                    }
                }
            }
        }
        /*
        * Output format
        */
        $output = 'autocomplete';
        if (! empty($request->output)) {
            $output = strtolower($request->output);
        }
        /*
        * Output results
        */
        if ($output === 'json') {
            echo json_encode($results);
        } else {
            echo autocomplete_format($results);
        }                
    }
}
