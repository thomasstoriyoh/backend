<?php

namespace App\ClassesV3;

use App\Models\Show;
use Illuminate\Support\Facades\Validator;
use App\Models\Episode;
use Carbon\Carbon;
use App\Traits\Helper;
use App\Models\User;
use App\Traits\ResponseFormat;

class Podcast4
{
    use ResponseFormat;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * This function is use for show listing
     *
     * @return void
     */
    public function Shows()
    {
        $request = request();

        try {
            //Elastic Search
            $query = Show::search('*')->where('status', 'published');

            if (! empty($request->country)) {
                $query->where('country', $request->country);
            }
            if (! empty($request->language)) {
                $query->where('language', $request->language);
            }
            if (! empty($request->filter_category)) {
                $query->where('category_ids', '"' . $request->filter_category . '"');
            }

            if ($request->sort_by == 'recency') {
                $query->orderBy('recency', 'DESC');
            }

            $noofrecords = config('config.pagination.show');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->no_paginate == 'Yes') {
                $all_shows = $query->get();
            } else {
                $all_shows = $query->paginate($noofrecords);
            }

            $subscribers = [];
            if ($this->api_type == 'api') {
                $subscribers = $this->userData->subscribers()->pluck('show_id')->all();
            }

            $data = [];
            foreach ($all_shows as $item) {
                $subscribe = false;
                if ($this->api_type == 'api') {
                    $subscribe = @in_array($item->id, $subscribers) ? 'true' : 'false';
                }
                $data[] = [
                    'id' => $item->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    //'category' => $item->categories()->pluck('title', 'id')->all(),
                    'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                    'no_of_episodes' => $item->episodes()->count(),
                    'no_of_subscribers' => 0,//$item->subscribers->count(),
                    'subscribe' => $subscribe
                ];
            }

            $message = '';
            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
            } else {
                return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_shows->total(),
                    'per_page' => $all_shows->perPage(),
                    'pages' => ceil($all_shows->total() / $all_shows->perPage()),
                    'items' => $data
                ]
            ]);
            }
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * This function is use for show search
     *
     * @return void
     */
    public function Search()
    {
        $request = request();

        try {
            //Elastic Search
            $query = Show::search($request->keyword)->where('status', 'published');

            if (! empty($request->country)) {
                $query->where('country', $request->country);
            }
            if (! empty($request->language)) {
                $query->where('language', $request->language);
            }
            if (! empty($request->filter_category)) {
                $query->where('category_ids', '"' . $request->filter_category . '"');
            }

            if ($request->sort_by == 'recency') {
                $query->orderBy('recency', 'DESC');
            }

            $noofrecords = config('config.pagination.show');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->no_paginate == 'Yes') {
                $all_shows = $query->where('status', 'published')->get();
            } else {
                $all_shows = $query->where('status', 'published')->paginate($noofrecords);
            }

            $subscribers = [];
            if ($this->api_type == 'api') {
                $subscribers = $this->userData->subscribers()->pluck('show_id')->all();
            }
            //dd($subscribers);

            $data = [];
            foreach ($all_shows as $show) {
                $subscribe = false;
                if ($this->api_type == 'api') {
                    $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';
                }
                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'desc' => $show->description,
                    'url_slug' => Helper::make_slug($show->title),
                    //'category' => $show->categories()->pluck('title', 'id')->all(),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'no_of_episodes' => 0,//$show->episodes()->count(),
                    'no_of_subscribers' => $show->subscribers->count(),
                    'subscribe' => $subscribe,
                    'premium' => $show->content_type != 0 ? 'yes' : 'no'
                ];
            }

            $message = '';
            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'items' => $data,
                    ]
                ]);
            } else {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'total' => $all_shows->total(),
                        'per_page' => $all_shows->perPage(),
                        'pages' => ceil($all_shows->total() / $all_shows->perPage()),
                        'items' => $data,
                    ]
                ]);
            }
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * This function is use for show details page
     *
     * @return void
     */
    public function Show()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Show
        $show = Show::published()->where('id', $request->show_id)->withCount('subscribers')
                    ->with('categories')->first(['id', 'title', 'image', 'language', 'artist_name', 'description', 'updated_at']);

        if (!$show) {
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                'redirect' => 'yes',
                'data' => []
            ]);
        }

        //All Show Episodes
        $all_show_episode = $show->episodes()->published()->orderBy('date_added', 'DESC')
            ->first(['id', 'title', 'duration', 'mp3', 'show_id', 'date_added', 'updated_at', 'date_created', 'listen_count', 'explicit']);

        $episodeArray = [];
        $like = [];
        if ($this->api_type == 'api' && $all_show_episode) {
            //$like = $all_show_episode->like()->where('user_id', $this->userData->id)->count();
        }
        //foreach ($all_show_episode as $item) {
        //dd($item);
        if ($all_show_episode) {
            $episodeArray[] = [
                'id' => $all_show_episode->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($all_show_episode->title))),
                'category' => @implode(', ', @$all_show_episode->categories()->pluck('title')->all()),
                'show_id' => $all_show_episode->show->id,
                'show_title' => trim(str_replace("\n", '', $all_show_episode->show->title)),
                'image' => !empty($all_show_episode->show->image) ? $all_show_episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                'duration' => $all_show_episode->getDurationText(),
                'audio_file' => $all_show_episode->getAudioLink(),
                'listen_count' => Helper::shorten_count($all_show_episode->listen_count),
                'explicit' => $all_show_episode->show->explicit,
                'last_updated' => Carbon::parse($all_show_episode->date_added)->format('jS M Y')
            ];
        }
        //}

        $similar_podcast = [];
        if ($this->api_type == 'web') {
            $categoryIds = $show->categories()->pluck('id')->all();

            $query = Show::published()->has('episodes')->withCount('episodes');
            $query->where(function ($query) use ($categoryIds) {
                $query->orWhereHas('categories', function ($q) use ($categoryIds) {
                    $q->whereIn('id', $categoryIds);
                });
            });

            $data = $query->where('id', '!=', $request->show_id)->where('language', $show->language)->take(5)
                ->orderBy('no_of_subscribers', 'desc')->get(['id', 'title', 'image']);
            foreach ($data as $item) {
                $similar_podcast[] = [
                    'id' => $item->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    'slug' => Helper::make_slug(trim(@$item->title)),
                    'image' => !empty($item->image) ? $item->getWSImage(400) : asset('uploads/default/show.png'),
                    //'image' => asset('uploads/default/show-banner.jpg'),
                    'total_episode' => $item->episodes_count . ' episodes',
                ];
            }
        }

        $subscribe = false;
        if ($this->api_type == 'api' && $show) {
            $subscribers = $this->userData->subscribers()->pluck('show_id')->all();
            $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';
        }

        //fetch trending data
        $trending_data = [];
        $subscribersArray = [];

        if ($this->api_type == 'web') {
            ##Added By Datta##
            $more_episode_link = Helper::make_slug($show->title).'/episodes/'.$show->id;
            $latest_episode = $show->episodes()->published()->orderBy('date_added', 'DESC')->limit(3)
            ->get(['id', 'title','show_id', 'date_added', 'duration']);

            $latestEpisodeArray = [];
            if ($latest_episode) {
                foreach ($latest_episode as $key => $value) {
                    $latestEpisodeArray[] = [
                        'id' => $value->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($value->title))),
                        'url_slug' =>Helper::make_slug($value->title),
                        'duration' => $value->getDurationText(),
                        'show_id' => $value->show->id,
                        'image' => !empty($value->show->image) ? $value->show->getWSImage(200) : asset('uploads/default/show.png'),
                        'last_updated' => Carbon::parse($value->date_added)->format('jS M Y')
                    ];
                }
            }

            $subscribers = $show->subscribers()->verified()->get(['id','image','first_name','last_name']);

             if ($subscribers) {
                foreach ($subscribers as $key => $value1) {
                    $subscribersArray[] = [
                        'key' => $key + 1,
                        'id' => $value1->id,
                        'name' => trim($value1->first_name . ' ' . $value1->last_name),
                        'user_image' => !empty($value1->image) ? $value1->getImage(100) : asset('uploads/default/user.png'),
                    ];
                }
            }
            ##end##
            // $trending_data = Helper::trending_data(); //remove in v3.3
        }

        //Nitesh Code Start
        $User_Data = [];
        $chart_arr = [];
        //Nitesh Code End

        $data = [
            'id' => $show->id,
            'show_name' => trim(str_replace("\n", '', html_entity_decode($show->title))),
            'show_image' => !empty($show->image) ? $show->getWSImage(400) : asset('uploads/default/show.png'),
            'show_slug' => Helper::make_slug(trim(@$show->title)),
            "share_url" => config('config.live_url')."/podcasts/".$show->id."/".Helper::make_slug(trim(@$show->title)),
            'category' => @implode(', ', @$show->categories()->pluck('title')->all()),
            'show_desc' => $this->api_type == 'api' ? strip_tags(trim($show->description)) : trim($show->description),
            'artist_name' => @$show->artist_name,
            'network_name' => @$show->network->title,
            'no_of_episodes' => $show->episodes()->published()->count(),
            'last_updated' => $show->updated_at->diffForHumans(),
            'subscribe' => $subscribe,
            'no_of_subscribers' => $show->subscribers()->verified()->count(),
            'subscribers' => $subscribersArray,
            'episodes' => $this->api_type == 'web' ? $latestEpisodeArray : $episodeArray,
            // 'similar_podcast' => $similar_podcast,
            // 'trending_data' => $trending_data,
            'more_episode_link' => $this->api_type == 'web' ? $more_episode_link : '',
            'subscribe_to_connection' => $User_Data,
            'chart_collection' => $chart_arr
        ];

        return response()->api([
            'status' => true,
            'redirect' => 'no',
            'data' => $data
        ]);
    }

    /**
     * This function is use for all show episodes
     *
     * @return void
     */
    public function Episodes()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $show = Show::where('id', $request->show_id)->first(['title', 'image']);

        //All Episodes
        $query = Episode::published()->where('show_id', $request->show_id);

        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (! empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        //$query->groupBy('mp3');

        $query->orderBy('date_added', 'DESC');

        $all_show_episode = $query->paginate($noofrecords, ['id', 'title', 'duration', 'mp3', 'show_id', 'date_added','updated_at', 'date_created', 'listen_count', 'explicit']);

        $data = [];
        foreach ($all_show_episode as $item) {
            $data[] = [
                'id' => $item->id,
                'show_id' => $item->show_id,
                'show_title' => html_entity_decode(@$item->show->title),
                "share_url" => config('config.live_url')."/episode/".$item->id."/".Helper::make_slug(trim(@$item->title)),
                'show_slug' => Helper::make_slug(trim(@$item->show->title)),
                'episode_title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'url_slug' => Helper::make_slug(trim(@$item->title)),
                'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                'audio_file' => $item->getAudioLink(),
                'duration' => $item->getDurationText(),
                'listen_count' => Helper::shorten_count($item->listen_count),
                "explicit" => $item->show->explicit,
                'created_date' => \Carbon\Carbon::parse($item->date_added)->format('jS M Y')
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_show_episode->total(),
                'total_number_format' => number_format($all_show_episode->total()),
                'per_page' => $all_show_episode->perPage(),
                'pages' => ceil($all_show_episode->total() / $all_show_episode->perPage()),
                'show_title' => $show->title,
                'show_image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for test
     *
     * @return void
     */
    public function Episodes_Test()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('config.missing_parameter1').' show id'. config('config.missing_parameter2').' Podcast Episodes.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $show = Show::where('id', $request->show_id)->first(['title', 'image']);

        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (! empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        $max = $noofrecords;
        $page_no = $request->page ? $request->page : 1;
        $start = ($page_no) ? ($page_no-1) * $max : 0;

        $query = \DB::table('episodes')->distinct('mp3')->select('mp3', 'date_created')
            ->where('show_id', $request->show_id);

        $all_show_episode_count_clone = clone $query;

        $all_show_episode_count = $all_show_episode_count_clone->get()->count();

        $query->skip($start)->take($max);

        $query->orderBy('date_created', 'DESC');

        $all_show_episode = $query->get();

        $data = [];
        foreach ($all_show_episode as $item_val) {
            $item = Episode::where('show_id', $request->show_id)
                ->where('mp3', $item_val->mp3)->first(['id', 'title', 'duration', 'mp3', 'show_id', 'date_added','updated_at', 'date_created']);
            $data[] = [
                'id' => $item->id,
                'show_id' => $item->show_id,
                'show_title' => html_entity_decode(@$item->show->title),
                "share_url" => config('config.live_url')."/episode/".$item->id."/".Helper::make_slug(trim(@$item->title)),
                'show_slug' => Helper::make_slug(trim(@$item->show->title)),
                'episode_title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'url_slug' => Helper::make_slug(trim(@$item->title)),
                'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                'audio_file' => $item->getAudioLink(),
                'duration' => $item->getDurationText(),
                'created_date' => \Carbon\Carbon::parse($item->date_added)->format('jS M Y')
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No Episode Found.';
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_show_episode_count,
                'total_number_format' => number_format($all_show_episode_count),
                'per_page' => $max,
                'pages' => ceil($all_show_episode_count / $max),
                'show_title' => $show->title,
                'show_image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for show autosearch
     *
     * @return void
     */
    public function AutoSearch()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'q' => 'required'
        ], [
            'q.required' => config('language.' . $this->getLocale() . ".Common.error")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $showData = Show::published()->where('title', 'LIKE', '%'. $request->q .'%')->has('episodes')
            ->take(5)->pluck('id', 'title')->all();

        $data = [];
        foreach($showData as $key => $item) {
            $data[$key] = $item."****".Helper::make_slug(trim($key));
        }

        /*
        * Results array
        */
        $results = array();
        /*
        * Autocomplete formatter
        */
        function autocomplete_format($results) {
            foreach ($results as $result) {
                echo $result[0] . '|' . $result[1] . "\n";
            }
        }
        /*
        * Search for term if it is given
        */
        if (! empty($request->q)) {
            $q = strtolower($request->q);
            if ($q) {
                foreach ($data as $key => $value) {
                    if (strpos(strtolower($key), $q) !== false) {
                        $results[] = array($key, $value);
                    }
                }
            }
        }
        /*
        * Output format
        */
        $output = 'autocomplete';
        if (! empty($request->output)) {
            $output = strtolower($request->output);
        }
        /*
        * Output results
        */
        if ($output === 'json') {
            echo json_encode($results);
        } else {
            echo autocomplete_format($results);
        }
    }
}
