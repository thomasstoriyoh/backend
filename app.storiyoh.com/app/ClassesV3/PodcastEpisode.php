<?php

namespace App\ClassesV3;

use App\Models\Episode;
use Illuminate\Support\Facades\Validator;
use App\Traits\Helper;
use Carbon\Carbon;
use App\Models\User;
use App\Traits\ResponseFormat;

class PodcastEpisode
{
    use ResponseFormat;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    public function __construct($api_type, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function Episodes()
    {
        $request = request();

        try {
            $query = Episode::search('*')->where('status', 'published')->where('mp3', 1);

            if (! empty($request->country)) {
                $query->where('country', $request->country);
            }
            if (! empty($request->language)) {
                $query->where('language', $request->language);
            }
            if (! empty($request->filter_category)) {
                $query->where('category_ids', '"' . $request->filter_category . '"');
            }

            if (! empty($request->filter_show)) {
                $query->where('show_id', $request->filter_show);
            }

            if ($request->sort_by == 'recency') {
                $query->orderBy('recency', 'DESC');
            }

            $noofrecords = config('config.pagination.episode');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->no_paginate == 'Yes') {
                $all_episodes = $query->get();
            } else {
                $all_episodes = $query->paginate($noofrecords);
            }

            $user_episode_like = [];
            if ($this->api_type == 'api') {
                $user_episode_like = $this->userData->like()->pluck('episode_id')->all();
            }

            $data = [];
            foreach ($all_episodes as $item) {
                $like = false;
                if ($this->api_type == 'api') {
                    $like = @in_array($item->id, $user_episode_like) ? 'true' : 'false';
                }
                $data[] = [
                    'id' => $item->id,
                    'show_id' => $item->show_id,
                    'show_title' => html_entity_decode(@$item->show->title),
                    'show_slug' => Helper::make_slug(trim(str_replace("\n", '', @$item->show->title))),
                    'episode_title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    'url_slug' => Helper::make_slug(trim(str_replace("\n", '', @$item->title))),
                    //"category" => $item->categories()->pluck('title', 'id')->all(),
                    'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                    'audio_file' => $item->getAudioLink(),
                    'duration' => $item->getDurationText(),
                    'like' => $like,
                    'created_date' => \Carbon\Carbon::parse($item->date_created)->format('jS M Y'),
                ];
            }

            $message = '';
            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'items' => $data,
                    ]
                ]);
            } else {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'total' => $all_episodes->total(),
                        'per_page' => $all_episodes->perPage(),
                        'pages' => ceil($all_episodes->total() / $all_episodes->perPage()),
                        'items' => $data
                    ]
                ]);
            }
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function Search()
    {
        $request = request();

        try {
            $query = Episode::search($request->keyword)->where('status', 'published')->where('mp3', 1);

            if (! empty($request->country)) {
                $query->where('country', $request->country);
            }
            if (! empty($request->language)) {
                $query->where('language', $request->language);
            }
            if (! empty($request->filter_category)) {
                $query->where('category_ids', '"' . $request->filter_category . '"');
            }
            if (! empty($request->filter_show)) {
                $query->where('show_id', $request->filter_show);
            }
            
            if ($request->sort_by == 'recency') {
                $query->orderBy('release_date', 'DESC');
            }

            $noofrecords = config('config.pagination.episode');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->no_paginate == 'Yes') {
                $all_episodes = $query->get();
            } else {
                $all_episodes = $query->paginate($noofrecords);
            }

            $user_episode_like = [];
            if ($this->api_type == 'api') {
                $user_episode_like = $this->userData->like()->pluck('episode_id')->all();
            }

            //dd($user_episode_like, $this->api_type, $this->userData);

            $data = [];
            foreach ($all_episodes as $item) { //dump($item->id);
                $like = false;
                if ($this->api_type == 'api') {
                    $like = @in_array($item->id, $user_episode_like) ? 'true' : 'false';
                }
                $data[] = [
                    'id' => $item->id,
                    'show_id' => $item->show_id,
                    'show_title' => html_entity_decode(@$item->show->title),
                    "share_url" => config('config.live_url')."/episode/".$item->id."/".Helper::make_slug(trim(str_replace("\n", '', @$item->title))),
                    'show_slug' => Helper::make_slug(trim(str_replace("\n", '', @$item->show->title))),
                    'episode_title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    'url_slug' => Helper::make_slug(trim(str_replace("\n", '', @$item->title))),
                    //"category" => $item->categories()->pluck('title', 'id')->all(),
                    'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                    //'image' => asset('uploads/default/episode-banner.jpg'),
                    'audio_file' => $item->getAudioLink(),
                    'duration' => $item->getDurationText(),
                    'created_date' => \Carbon\Carbon::parse($item->date_created)->format('jS M Y'),
                    'like' => $like,
                    'like_count' => $item->like()->count(),
                    'comment_count' => $item->comments()->count()
                ];
            }

            $message = '';
            $status = true;
            if (count($data) == 0) {
                $status = false;
                $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'items' => $data
                    ]
                ]);
            } else {

                $total = $all_episodes->total() > 10000 ? 10000 : $all_episodes->total();

                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'total' => $all_episodes->total(),
                        'total_number_format' => number_format($all_episodes->total()),
                        'per_page' => $all_episodes->perPage(),
                        'pages' => ceil($total / $all_episodes->perPage()),
                        'items' => $data
                    ]
                ]);
            }
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function Show()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        // Show Episode
        $episode = Episode::where('id', $request->episode_id)->withCount('like')
            ->first(['id', 'show_id', 'title', 'updated_at', 'description', 'image', 'duration', 'mp3', 'date_created', 'listen_count']);

        if (!$episode) {
            $data = [];
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl"),
                'redirect' => 'yes',
                'data' => $data
            ]);
        }

        $like = [];
        if ($this->api_type == 'api' && $episode) {
            $like = $episode->like()->where('user_id', $this->userData->id)->count();
        }
        
        $tags = $episode->tags()->pluck('title')->all();

        //fetch trending data
        $trending_data = [];
        if ($this->api_type == 'web') {
            $trending_data = Helper::trending_data();
        }

        //Code start here !!!
        $num_of_listens = $episode->listen_count;
        //dd($num_of_listens);
    
        $recent_listens_arr = [];
        if ($this->api_type == 'api' && $episode) {            
            // Recently listened to by 5
            $user = $this->userData;
            
            //$followingIds = $user->following()->verified()->pluck('id')->all();

            $followingIds = $user->following()->verified()->pluck('id')->all();
            //dd($followingIds);
            $user_ids = [];
            $create_data_array = [];
            if (count($followingIds) > 0) {
                $followingIds = implode(",", $followingIds);
                $recent_listens = \DB::select('select distinct(user_id), MAX(created_at) as created_at from `tbl_user_stream_data` where `episode_id` = ' . $episode->id . ' and `user_id` != ' . $user->id . ' and `discreet` = "N" and user_id IN(' . $followingIds . ') group by `user_id` order by `created_at` desc limit 10');
                if(count($recent_listens) == 0) {
                    $recent_listens = \DB::select('select distinct(user_id), MAX(created_at) as created_at from `tbl_user_stream_data` where `episode_id` = ' . $episode->id . ' and `user_id` != ' . $user->id . ' and `discreet` = "N" group by `user_id` order by `created_at` desc limit 10');    
                }
                foreach ($recent_listens as $recent_listen) {
                    $create_data_array[$recent_listen->user_id] = $recent_listen->created_at;
                    $user_ids[] = $recent_listen->user_id;
                }
            } else {
                $recent_listens = \DB::select('select distinct(user_id), MAX(created_at) as created_at from `tbl_user_stream_data` where `episode_id` = ' . $episode->id . ' and `user_id` != ' . $user->id . ' and `discreet` = "N" group by `user_id` order by `created_at` desc limit 10');                
                foreach ($recent_listens as $recent_listen) {
                    $create_data_array[$recent_listen->user_id] = $recent_listen->created_at;
                    $user_ids[] = $recent_listen->user_id;
                } 
            }
            
            if (count($user_ids) > 0) {
                $users = User::whereIn('id', $user_ids)->orderByRaw('FIELD(id,' . implode(',', $user_ids) . ') ASC')->get(['id', 'username', 'first_name', 'last_name', 'image']);                
                foreach ($users as $key => $user) {
                    array_push($recent_listens_arr, [
                        'username' => $user->username,
                        'name' => $user->first_name . ' ' . $user->last_name,
                        'image' => !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png'),
                        'created_at' => Carbon::createFromTimeStamp(strtotime($create_data_array[$user->id]))->diffForHumans()
                    ]);                    
                }
            }            
        }

        $data = [
            'id' => $episode->id,            
            'show_id' => trim(str_replace("\n", '', @$episode->show->id)),
            'show_name' => trim(str_replace("\n", '', html_entity_decode(@$episode->show->title))),
            "share_url" => config('config.live_url')."/episode/".$episode->id."/".Helper::make_slug(trim(str_replace("\n", '', @$episode->title))),
            'show_slug' => Helper::make_slug(trim(str_replace("\n", '', @$episode->show->title))),
            'show_category' => !empty($episode->show->categories) ? implode(',', $episode->show->categories()->pluck('title')->all()) : '',
            'show_image' => !empty($episode->show->image) ? $episode->show->getWSImage(400) : asset('uploads/default/show.png'),
            //'image' => asset('uploads/default/episode-banner.jpg'),
            'artist_name' => $episode->show->artist_name,
            'episode_name' => trim(str_replace("\n", '', html_entity_decode($episode->title))),
            'episode_desc' => $this->api_type == 'api' ? strip_tags(trim($episode->description)) : trim($episode->description),
            'episode_image' => $episode->image,
            'episode_tags' => count($tags) > 0 ? implode(",", $tags) : "",
            'last_updated' => \Carbon\Carbon::parse($episode->date_created)->format('jS M Y'),//$episode->show->updated_at->diffForHumans(),
            'duration' => $episode->getDurationText(),
            'audio_file' => $episode->getAudioLink(),
            'episode_like' => Helper::shorten_count($episode->like_count),
            'created_date' => \Carbon\Carbon::parse($episode->date_created)->format('jS M Y'),
            'like' => $like  ? 'true' : 'false',
            'like_count' => $episode->like()->count(),
            'comment_count' => $episode->comments()->count(),
            'trending_data' => $trending_data,
            'num_of_listens' => Helper::shorten_count($num_of_listens),
            'recent_listens_arr' => $recent_listens_arr
        ];

        return response()->api([
            'status' => true,
            'redirect' => 'no',
            'data' => $data
        ]);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function  url_shortner() {

        $request = request();
        
        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        // Show Episode
        $episode = Episode::where('id', $request->episode_id)->first(['id', 'title']);

        if (!$episode) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl")
            ]);
        }

        return response()->api([
            'status' => true,
            'data' => [
                "share_url" => config('config.live_url')."/episode/".$episode->id."/".Helper::make_slug(trim(str_replace("\n", '', @$episode->title))),
            ]
        ]);
    }       
}
