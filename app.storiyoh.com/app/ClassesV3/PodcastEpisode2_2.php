<?php

namespace App\ClassesV3;

use App\Models\Episode;
use Illuminate\Support\Facades\Validator;
use App\Traits\Helper;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use App\Traits\ResponseFormat;
class PodcastEpisode2_2
{
    use ResponseFormat;
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    public function __construct($api_type, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function Episodes()
    {
        $request = request();

        try {
            $query = Episode::search('*')->where('status', 'published')->where('mp3', 1);

            if (! empty($request->country)) {
                $query->where('country', $request->country);
            }
            if (! empty($request->language)) {
                $query->where('language', $request->language);
            }
            if (! empty($request->filter_category)) {
                $query->where('category_ids', '"' . $request->filter_category . '"');
            }

            if (! empty($request->filter_show)) {
                $query->where('show_id', $request->filter_show);
            }

            if ($request->sort_by == 'recency') {
                $query->orderBy('recency', 'DESC');
            }

            $noofrecords = config('config.pagination.episode');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->no_paginate == 'Yes') {
                $all_episodes = $query->get();
            } else {
                $all_episodes = $query->paginate($noofrecords);
            }

            $user_episode_like = [];
            if ($this->api_type == 'api') {
                //$user_episode_like = $this->userData->like()->pluck('episode_id')->all();
            }

            $data = [];
            foreach ($all_episodes as $item) {
                $like = false;
                if ($this->api_type == 'api') {
                    //$like = @in_array($values->id, $user_episode_like) ? 'true' : 'false';
                }
                $data[] = [
                    'id' => $item->id,
                    'show_id' => $item->show_id,
                    'show_title' => html_entity_decode(@$item->show->title),
                    'show_slug' => Helper::make_slug(trim(str_replace("\n", '', @$item->show->title))),
                    'episode_title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    'url_slug' => Helper::make_slug(trim(str_replace("\n", '', @$item->title))),
                    //"category" => $item->categories()->pluck('title', 'id')->all(),
                    'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                    'audio_file' => $item->getAudioLink(),
                    'duration' => $item->getDurationText(),
                    'like' => $like,
                    'listen_count' => Helper::shorten_count($item->listen_count),
                    'explicit' => $item->show->explicit,
                    'created_date' => \Carbon\Carbon::parse($item->date_created)->format('jS M Y'),
                ];
            }

            $message = '';
            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'items' => $data,
                    ]
                ]);
            } else {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'total' => $all_episodes->total(),
                        'per_page' => $all_episodes->perPage(),
                        'pages' => ceil($all_episodes->total() / $all_episodes->perPage()),
                        'items' => $data
                    ]
                ]);
            }
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function Search()
    {
        $request = request();

        try {
            $query = Episode::search($request->keyword)->where('status', 'published')->where('mp3', 1);

            if (! empty($request->country)) {
                $query->where('country', $request->country);
            }
            if (! empty($request->language)) {
                $query->where('language', $request->language);
            }
            if (! empty($request->filter_category)) {
                $query->where('category_ids', '"' . $request->filter_category . '"');
            }
            if (! empty($request->filter_show)) {
                $query->where('show_id', $request->filter_show);
            }

            if ($request->sort_by == 'recency') {
                $query->orderBy('release_date', 'DESC');
            }

            $noofrecords = config('config.pagination.episode');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->no_paginate == 'Yes') {
                $all_episodes = $query->get();
            } else {
                $all_episodes = $query->paginate($noofrecords);
            }

            $user_episode_like = [];
            if ($this->api_type == 'api') {
                //$user_episode_like = $this->userData->like()->pluck('episode_id')->all();
            }

            //dd($user_episode_like, $this->api_type, $this->userData);

            $data = [];
            foreach ($all_episodes as $item) { //dump($item->id);
                $like = false;
                if ($this->api_type == 'api') {
                    //$like = @in_array($item->id, $user_episode_like) ? 'true' : 'false';
                }

                $episodeDetails = \App\Models\EpisodeDetail::where('episode_id', $item->id)->count();                
                
                $data[] = [
                    'id' => $item->id,
                    'show_id' => $item->show_id,
                    'show_title' => html_entity_decode(@$item->show->title),
                    "share_url" => config('config.live_url')."/episode/".$item->id."/".Helper::make_slug(trim(str_replace("\n", '', @$item->title))),
                    'show_slug' => Helper::make_slug(trim(str_replace("\n", '', @$item->show->title))),
                    'episode_title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    'desc' => $item->description,
                    'url_slug' => Helper::make_slug(trim(str_replace("\n", '', @$item->title))),
                    //"category" => $item->categories()->pluck('title', 'id')->all(),
                    'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                    //'image' => asset('uploads/default/episode-banner.jpg'),
                    'audio_file' => $item->getAudioLink(),
                    'duration' => $item->getDurationText(),
                    'listen_count' => Helper::shorten_count($item->listen_count),
                    'explicit' => $item->show->explicit,
                    'created_date' => \Carbon\Carbon::parse($item->date_created)->format('jS M Y'),
                    'premium' => $episodeDetails > 0 ? "yes" : "no"
                ];
            }

            $message = '';
            $status = true;
            if (count($data) == 0) {
                $status = false;
                $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'items' => $data
                    ]
                ]);
            } else {

                $total = $all_episodes->total() > 10000 ? 10000 : $all_episodes->total();

                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'total' => $all_episodes->total(),
                        'total_number_format' => number_format($all_episodes->total()),
                        'per_page' => $all_episodes->perPage(),
                        'pages' => ceil($total / $all_episodes->perPage()),
                        'items' => $data
                    ]
                ]);
            }
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => [
                    'items' => []
                ]
            ]);
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function Show()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        // Show Episode
        // if (config('config.cache_status.episode_details')) {
        //     if (Cache::get('episode.details.'.$request->episode_id)) {
        //         $episode = Cache::get('episode.details.'.$request->episode_id);
        //     } else {
        //         $episode = Episode::where('id', $request->episode_id)->with('tags')
        //             ->first(['id', 'show_id', 'title', 'updated_at', 'description', 'image', 'duration', 'mp3', 'date_created', 'listen_count', 'explicit']);

        //         //This code is use for cache that query data
        //         $expiresAt = Carbon::now()->endOfDay()->addSecond();
        //         Cache::put('episode.details.'.$request->episode_id, $episode, $expiresAt);
        //     }
        // } else {
            $episode = Episode::where('id', $request->episode_id)->with('tags')
                ->first(['id', 'show_id', 'title', 'updated_at', 'description', 'image', 'duration', 'mp3', 'date_created', 'listen_count', 'explicit']);
        //}

        if (!$episode) {
            $data = [];
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl"),
                'redirect' => 'yes',
                'data' => $data
            ]);
        }

        $like = [];
        
        $tags = $episode->tags()->pluck('title')->all();

        //fetch trending data
        $trending_data = [];        

        //Code start here !!!
        $num_of_listens = $episode->listen_count;        

        $recent_listens_arr = [];

        // Marketplace code start
        $episodeInfo = $episode->episode_extra_info;
        $episode_rating = "";
        $episode_language = !is_null($episode->show->language) ? $episode->show->language : "";
        $premium = 'no';        
        $purchase = "no";
        $country_id = 1;
        $currency_id = 1;        
        $selling_currency = "INR";
        $selling_price = "";
        $show_image = !empty($episode->show->image) ? $episode->show->getWSImage(400) : asset('uploads/default/show.png');
        if (!empty($episodeInfo)) {
            $episode_language = $episodeInfo->language;
            $episode_rating = $episodeInfo->rating;
            $premium = 'yes';
            //Ip2Location code start            
            $ip_address = $request->ip_address;
            //$ip_address = "183.87.113.164";
            $responseData = new V3_3\IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();
            if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
                //Check if marketplace available in user country
                $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                    ->where('country_alpha2_code', $location['data']['country_code'])
                    ->first(["id", "title", "country_alpha2_code", "currency_id"]);
                if ($marketplace_country) {
                    // $product_ids = $episode->premium_pricing()->where('country_id', $marketplace_country->id)
                    //     ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    // $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";                    
                    $country_id = $marketplace_country->id;
                    $currency_id = $marketplace_country->currency_id;
                    $selling_currency = $marketplace_country->currency->title;
                }
            }
            $product_ids = $episode->premium_pricing()->where('country_id', $country_id)->where('currency_id', $currency_id)
                ->first(['selling_price']);
            $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
            if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
                $purchase = "free";
            }

            if (!is_null($episode->image)) {
                $show_image = $episode->getImage(200);
            }
        }

        /** Language Code */
        $language = $episode_language;
        if (isset(config('config.languages')[$language])) {
            $language = config('config.languages')[$language];
        }
        // Marketplace code end

        $data = [
            'id' => $episode->id,
            'show_id' => trim(str_replace("\n", '', @$episode->show->id)),
            'show_name' => trim(str_replace("\n", '', html_entity_decode(@$episode->show->title))),
            "share_url" => config('config.live_url')."/episode/".$episode->id."/".Helper::make_slug(trim(str_replace("\n", '', @$episode->title))),
            'show_slug' => Helper::make_slug(trim(str_replace("\n", '', @$episode->show->title))),
            'show_category' => !empty($episode->show->categories) ? implode(',', $episode->show->categories()->pluck('title')->all()) : '',
            'show_image' => $show_image, //!empty($episode->show->image) ? $episode->show->getWSImage(400) : asset('uploads/default/show.png'),
            'artist_name' => $episode->show->artist_name,
            'episode_name' => trim(str_replace("\n", '', html_entity_decode($episode->title))),
            'episode_desc' => $episode->getOriginal('description'),
            'episode_image' => $episode->image,
            'episode_tags' => count($tags) > 0 ? $tags : "",
            'last_updated' => \Carbon\Carbon::parse($episode->date_created)->format('jS M Y'),
            'duration' => $episode->getDurationText(),
            'audio_file' => $episode->getAudioLink(),
            'episode_like' => 0,
            'created_date' => \Carbon\Carbon::parse($episode->date_created)->format('jS M Y'),
            'trending_data' => $trending_data,
            'num_of_listens' => Helper::shorten_count($num_of_listens),
            'explicit' => @$episode->show->explicit,
            'embed_url' => config('config.live_url')."/player/".$episode->id."/embed",
            'recent_listens_arr' => $recent_listens_arr,
            'standalone_episode' => $episode->show->content_type == 2 ? 'yes' : 'no',

            'premium' => $premium,
            'episode_language' => $language,
            'episode_rating' => $episode_rating,
            'selling_price' => $selling_price,
            'selling_currency' => $selling_currency,
            'purchase' => $purchase,
        ];

        return response()->api([
            'status' => true,
            'redirect' => 'no',
            'data' => $data
        ]);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function  url_shortner() {

        $request = request();

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        // Show Episode
        $episode = Episode::where('id', $request->episode_id)->first(['id', 'title']);

        if (!$episode) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl")
            ]);
        }

        return response()->api([
            'status' => true,
            'data' => [
                "share_url" => config('config.live_url')."/episode/".$episode->id."/".Helper::make_slug(trim(str_replace("\n", '', @$episode->title))),
            ]
        ]);
    }

    ##Added By Datta
    /**
     * Episode listeners
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function EpisodeListeners()
    {
        $request = request();
        $validator = Validator::make($request->all(),
        [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError($validator->messages()->getMessages(),$validator->messages()->first());
        }

        try {
            $episode = Episode::published()->where('id', $request->episode_id)->first();

            if (!$episode) {
                return $this->sendEmptyResponse();
            }
            $data = $episode->user_stream_data()->orderBy('created_at','DESC')->distinct('id')->take(10)->get();

            $result = $data->map( function($item, $key){
                return [
                    'key' => $key + 1,
                    'id' => $item->id,
                    'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                    'name' => $item->getFullNameAttribute()
                ];
            });

            return $this->sendResponse($result);

        } catch(\Exception $ex){

            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
    }
}
