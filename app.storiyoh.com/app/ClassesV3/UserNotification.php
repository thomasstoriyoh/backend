<?php

namespace App\ClassesV3;

/**
 * Description of Push Notification
 *
 * @author bcm
 */
class UserNotification
{
    /** General Notification */
    const FOLLOW_BOARD = 'Follow Board';
    const FOLLOW_SMARTLIST = 'Follow Smartlist';
    const FOLLOW_USER = 'Follow User';
    const BOARD_UPDATE = 'Board Update';
    
    /** Likes Types */
    const SHOW_POST_LIKE = 'Show Post Like';
    const EPISODE_POST_LIKE = 'Episode Post Like';
    const PLAYLIST_POST_LIKE = 'Playlist Post Like';
    const SMARTPLAYLIST_POST_LIKE = 'Smart Playlist Post Like';
    const COLLECTION_POST_LIKE = 'Collection Post Like';

    /** Comment Types */
    const SHOW_POST_COMMENT = 'Show Post Comment';
    const EPISODE_POST_COMMENT = 'Episode Post Comment';
    const PLAYLIST_POST_COMMENT = 'Playlist Post Comment';
    const SMARTPLAYLIST_POST_COMMENT = 'Smart Playlist Post Comment';
    const COLLECTION_POST_COMMENT = 'Collection Post Comment';

    /** Comment Types */
    const SHOW_POST_COMMENT_TAG = 'Show Post Comment Tag';
    const EPISODE_POST_COMMENT_TAG = 'Episode Post Comment Tag';
    const PLAYLIST_POST_COMMENT_TAG = 'Playlist Post Comment Tag';
    const SMARTPLAYLIST_POST_COMMENT_TAG = 'Smart Playlist Post Comment Tag';
    const COLLECTION_POST_COMMENT_TAG = 'Collection Post Comment Tag';
    
    /**
     *
     * @var Model
     */
    protected $data;

    /**
     * Feed constructor.
     * @var $data Model
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     *
     * @param array|string $content
     * @param string $type
    */
    public function add($content, $type)
    {
        $content = is_array($content) ? $content : [$content];

        $attributes = [
            'data' => json_encode($content),
            'type' => $type
        ];

        return $this->data->activities_notification()->create($attributes);
    }
}
