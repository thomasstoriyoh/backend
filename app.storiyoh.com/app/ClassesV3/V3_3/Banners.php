<?php
namespace App\ClassesV3\V3_3;

use App\Models\Banner;
use App\Models\Click;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Banners
{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    /**
     * Constructor
     *
     * @param $api_type
     * @param $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * This function is use for Banner listing
     *
     * @return void
     */
    public function banner_listing()
    {
        $banner = Banner::Published()->orderBy('order','asc')->get(['id', 'title', 'image', 'type', 'link']);

        $result = $banner->map( function($banner) {
            return [
                'id' => $banner->id,
                'title' => $banner->title,
                // 'image' => $banner->getImage(1924, 924),
                'image' => $banner->getImage(1200, 600),
                'banner_type' => 'banner',
                'type' => $banner->type,
                'link' => $banner->link
            ];
        });
       
        return $result ;
       
    }

    /**
     * This function is use for Banner Click
     *
     * @return void
     */
    public function banner_clicks_event(Request $request)
    {        
       
        $request = request();
        
        $os = $request->os;
        $browser = $request->browser;
        $browser_full_info = $request->browser_full_info;
        $currentdatetime = new Carbon();
        $banner_id = $request->banner_id;

        $banner = Banner::Published()->where('id', $banner_id)->first();

        $click = new Click();
        $click->banner_id = $banner_id;
        $click->banner_title = $banner->title;
        $click->os = $os;
        $click->browser = $browser;
        $click->browser_full_info = $browser_full_info;
        $click->date_time = $currentdatetime;
        $click->save();            
            
        return true;     
        
    }

    /**
     * This function is use for Marketplace Promo Banner listing
     *
     * @return void
     */
    public function marketplace_banner_listing()
    {
        $banner = \App\Models\MarketplaceWebPromotion::Published()->orderBy('order', 'asc')->get(['id', 'title', 'type', 'content_id', 'image']);
        //$banner = \App\Models\MarketplacePromotion::Published()->orderBy('order', 'asc')->get(['id', 'title', 'type', 'content_id', 'image']);

        $result = $banner->map(function ($banner) {
            $url_slug = "";
            if ($banner->type == "Episode") {
                $url_slug = \App\Models\Episode::where('id', $banner->content_id)->pluck('title')->all();
                $url_slug = $url_slug[0];
            } else if ($banner->type == "Podcast") {
                $url_slug = \App\Models\Show::where('id', $banner->content_id)->pluck('title')->all();
                $url_slug = $url_slug[0];
            }
            return [
                'id' => $banner->id,
                'title' => $banner->title,
                //'image' => $banner->getImage(1200, 675),
                'image' => $banner->getImage(1170, 376),
                'type' => ($banner->type === "Standalone") ? "Episode" : $banner->type,
                'content_id' => $banner->content_id,
                'url_slug' => \App\Traits\Helper::make_slug($url_slug)
            ];
        });

        return $result;
    }
}
