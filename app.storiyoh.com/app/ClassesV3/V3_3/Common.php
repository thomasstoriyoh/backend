<?php

namespace App\ClassesV3\V3_3;

use App\Models\Show;
use App\Traits\Helper;
use Carbon\Carbon;
use App\Models\PodcastDay;
use App\Models\TrendingShow;

class Common
{
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    /**
     * Constructor
     *
     * @param [type] $api_type
     * @param [type] $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    /**
     * Trending Show function
     *
     * @return void
     */
    public function home_page_content_trending_show()
    {
        $request = request();

        $data = array();

        $trending = TrendingShow::limit(10)->orderBy('created_at','DESC')->get();
        foreach ($trending as $dataItem) {
            $item = Show::where('itunes_id', $dataItem->itunes_id)->first(['id', 'title','image']);
            
            $data[] = [
                'show_id' => $item->id,
                'title' => $item->title,
                'image' => $item->getWSImage(200),
                'url_slug'  => Helper::make_slug($item->title)
            ];
        }

        return $data;
    }


     /**
     * Podcast of the day function
     *
     * @return void
     */
    public function home_page_content_podcast_of_the_day()
    {       
        $date = Carbon::today();
        $podcast_day = PodcastDay::Published()->where('date', date($date->parse()->format('Y-m-d')))->take(1)->first();
        $podcastDayArray = [];
        if ($podcast_day) {
            $podcastDayArray = [
                'id' => $podcast_day->show->id,
                'title' => trim(str_replace("\n", '', $podcast_day->show->title)),
                'image' => $podcast_day->show->getWSImage(230),
                'banner_type' => 'podcast',
                'description' => str_limit($podcast_day->show->description, 200, '...'),
                'url_slug' => Helper::make_slug(trim($podcast_day->show->title))
            ];
        } else {
            $podcast_day = PodcastDay::published()->where('date', '<', date($date->parse()->format('Y-m-d')))->take(1)->first();
            $podcastDayArray = [
                'id' => $podcast_day->show->id,
                'title' => trim(str_replace("\n", '', $podcast_day->show->title)),
                'image' => $podcast_day->show->getWSImage(230),
                'banner_type' => 'podcast',
                'description' => str_limit($podcast_day->show->description, 200, '...'),
                'url_slug' => Helper::make_slug(trim($podcast_day->show->title))
            ];   
        }
        
        return  [$podcastDayArray];      
    }

    /**
     * Homepage Free Premium Episode
     * Function
     *
     * @return void
     */
    public function home_page_content_premium_free_episode()
    {
        $request = request();        

        $country_id = 1;
        $currency_id = 1;
        $selling_price = "";
        $data = [];
        $highlighted_episodes = \App\Models\HighlightedEpisode::published()->orderBy('order')->latest()->get(['id', 'episode_id']);

        $ip_address = $request->ip_address;
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();
        if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
            //Check if marketplace available in user country
            $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id"]);
            if (!is_null($marketplace_country)) {
                $country_id = $marketplace_country->id;
                $currency_id = $marketplace_country->currency_id;
            }
        } else {
            return $data;
        }
        foreach ($highlighted_episodes as $item) {
            if ($item->episode->status == "Published") {
                if (!empty($country_id) && !empty($currency_id)) {
                    $product_price = $item->episode->premium_pricing()->where('country_id', $country_id)
                        ->where('currency_id', $currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
                }
                $image = asset('uploads/default/show.png');
                if (!empty($item->episode->image)) {
                    $image = $item->episode->getImage(200);
                } else {
                    if (!empty($item->episode->show->image)) {
                        $image = $item->episode->show->getImage(200);
                    }
                }

                if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
                    $data[] = [
                        'id' => $item->episode->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                        'show_id' => $item->episode->show->id,
                        'show_title' => trim(str_replace("\n", '', html_entity_decode($item->episode->show->title))),
                        'show_url_slug' => Helper::make_slug($item->episode->show->title),
                        'image' => $image,
                        'imgAlt' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                        'desc' => str_limit(trim(str_replace("\n", '', html_entity_decode($item->episode->description))), 70),
                        'url_slug' => Helper::make_slug($item->episode->title),
                        'purchase' => 'free'
                    ];
                }
            }
        }

        return $data;
    }

    /**
     *  Get Single Signed Url for premium epsiode
     */
    public function getSourcePremiumFile()
    {
        $request = request();

        $ip_address = $request->ip_address;
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();
        $list = [];
        if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
            //Check if marketplace available in user country
            $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id"]);
            if (!is_null($marketplace_country)) {
                $country_id = $marketplace_country->id;
                $currency_id = $marketplace_country->currency_id;
            }

            try {

                $episode = \App\Models\Episode::find($request->id, ['id', 'show_id', 'title', 'image', 'mp3']);
                if (!$episode) {
                    return $list;
                }

                if (!empty($country_id) && !empty($currency_id)) {
                    $product_price = $episode->premium_pricing()->where('country_id', $country_id)
                        ->where('currency_id', $currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
                    if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
                        $response = "";
                        if (!is_null(env('DBLOCAL'))) {
                            $response = config('config.storiyoh_backend_url') . '/uploads/marketplace/' . $episode->getAudioLink();
                        } else {
                            if (!empty($episode->getAudioLink())) {
                                $response = $this->getSignedUrl(config('config.s3_marketplace_folder') . $episode->getAudioLink());
                            }
                        }
                        if (!empty($response)) {
                            $list[] =  [
                                'title' => $episode->title,
                                'artist' => !is_null($episode->show->artist_name) ? $episode->show->artist_name : "",
                                'src' => $response,
                                'pic' => !empty($episode->show) ? $episode->show->getWSImage(200) : asset('uploads/default/board.png'),
                            ];
                        }
                    }
                }

                return $list;
            } catch (\Exception $ex) {
                return $list;
            }
        } else {
            return $list;
        }
    }

    /**
     * This function is use for create signed url
     */
    protected function getSignedUrl($audio_file_name)
    {
        //Instantiate an Amazon S3 client.
        $s3Client = new \Aws\S3\S3Client([
            'version' => 'latest',
            'region'  => 'ap-south-1'
        ]);

        //Creating a presigned URL
        $cmd = $s3Client->getCommand('GetObject', [
            'Bucket' => config('config.s3_bucket_name'),
            'Key' => $audio_file_name,
            //'ResponseContentDisposition' => 'attachment'
        ]);

        $request = $s3Client->createPresignedRequest($cmd, '+5 minutes');

        // Get the actual presigned-url
        return (string) $request->getUri();
    }

    /**
     * This function is use for 
     * Episode cast and crew
     */
    public function episode_cast_crew()
    {
        $request = request();

        //Select Episode
        $episode = \App\Models\Episode::where('id', $request->episode_id)->first(['id', 'show_id']);

        if (!$episode) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl"),
                'data' => []
            ]);
        }

        $user_authors_array = [];
        $user_narrators_array = [];
        $user_producers_array = [];

        if ($episode->show->content_type != 0) {
            $all_user_authors_ids = $episode->authors()->pluck('user_id')->all();
            if (count($all_user_authors_ids) > 0) {
                $users = \App\Models\User::whereIn('id', $all_user_authors_ids)->approved()->verified()->whereNotNull('username')->where('username', '!=', '')->get(['id', 'first_name', 'username', 'image']);
                foreach ($users as $item) {
                    $user_authors_array[] = [
                        'name' => $item->notification_format_name,
                        'username' => $item->username,
                        'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png')
                    ];
                }
            }

            $all_user_narrators_ids = $episode->narrators()->pluck('user_id')->all();
            if (count($all_user_narrators_ids) > 0) {
                $users = \App\Models\User::whereIn('id', $all_user_narrators_ids)->approved()->verified()->whereNotNull('username')->where('username', '!=', '')->get(['id', 'first_name', 'username', 'image']);
                foreach ($users as $item) {
                    $user_narrators_array[] = [
                        'name' => $item->notification_format_name,
                        'username' => $item->username,
                        'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png')
                    ];
                }
            }

            $all_user_producers_ids = $episode->producers()->pluck('user_id')->all();
            if (count($all_user_producers_ids) > 0) {
                $users = \App\Models\User::whereIn('id', $all_user_producers_ids)->approved()->verified()->whereNotNull('username')->where('username', '!=', '')->get(['id', 'first_name', 'username', 'image']);
                foreach ($users as $item) {
                    $user_producers_array[] = [
                        'name' => $item->notification_format_name,
                        'username' => $item->username,
                        'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png')
                    ];
                }
            }
        }

        return [
            "user_authors_array" => $user_authors_array,
            "user_narrators_array" => $user_narrators_array,
            "user_producers_array" => $user_producers_array,
        ];
    }

    /**
     * This function is use for 
     * Show cast and crew
     */
    public function show_cast_crew()
    {
        $request = request();

        //Select Episode
        $show = \App\Models\Show::where('id', $request->show_id)->first(['id', 'content_type']);

        if (!$show) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                'data' => []
            ]);
        }

        $user_authors_array = [];
        $user_narrators_array = [];
        $user_producers_array = [];
        if ($show->content_type != 0) {
            //Getting all Episode list
            $all_episodes_ids = $show->episodes()->published()->pluck('id')->all();

            $all_user_authors_ids = \DB::table('episode_author')->whereIn('episode_id', $all_episodes_ids)->pluck('user_id')->all();
            if (count($all_user_authors_ids) > 0) {
                $users = \App\Models\User::whereIn('id', $all_user_authors_ids)->approved()->verified()->whereNotNull('username')->where('username', '!=', '')->get(['id', 'first_name', 'username', 'image']);
                foreach ($users as $item) {
                    $user_authors_array[] = [
                        'name' => $item->notification_format_name,
                        'username' => $item->username,
                        'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png')
                    ];
                }
            }

            $all_user_narrators_ids = \DB::table('episode_narrator')->whereIn('episode_id', $all_episodes_ids)->pluck('user_id')->all();
            if (count($all_user_narrators_ids) > 0) {
                $users = \App\Models\User::whereIn('id', $all_user_narrators_ids)->approved()->verified()->whereNotNull('username')->where('username', '!=', '')->get(['id', 'first_name', 'username', 'image']);
                foreach ($users as $item) {
                    $user_narrators_array[] = [
                        'name' => $item->notification_format_name,
                        'username' => $item->username,
                        'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png')
                    ];
                }
            }

            $all_user_producers_ids = \DB::table('episode_producer')->whereIn('episode_id', $all_episodes_ids)->pluck('user_id')->all();
            if (count($all_user_producers_ids) > 0) {
                $users = \App\Models\User::whereIn('id', $all_user_producers_ids)->approved()->verified()->whereNotNull('username')->where('username', '!=', '')->get(['id', 'first_name', 'username', 'image']);
                foreach ($users as $item) {
                    $user_producers_array[] = [
                        'name' => $item->notification_format_name,
                        'username' => $item->username,
                        'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png')
                    ];
                }
            }
        }

        return [
            "user_authors_array" => $user_authors_array,
            "user_narrators_array" => $user_narrators_array,
            "user_producers_array" => $user_producers_array,
        ];
    }

    /**
     * Marketplace Page
     * Homepage
     */
    public function marketplace()
    {

        $request = request();

        //Fetching mraketplace promotion banner
        $banner = new Banners('web');
        $bannerArray = $banner->marketplace_banner_listing();

        $show_data = [];
        $episode_data = [];
        $show_count = 0;
        $episode_count = 0;
        $selling_currency = "";

        //Ip2Location code start            
        $ip_address = $request->ip_address;
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();
        if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
            //Check if marketplace available in user country
            $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id"]);

            if ($marketplace_country) {
                $selling_currency = $marketplace_country->currency->title;

                $all_shows = Show::published()->where('content_type', 1)->whereNotNull('user_id')->has('episodes')
                    ->whereHas('premium_pricing', function ($qry) use ($marketplace_country) {
                        $qry->where('country_id', $marketplace_country->id);
                        $qry->where('currency_id', $marketplace_country->currency_id);
                    })
                    ->orderBy('created_at', 'DESC')->take(7)->get(['id', 'user_id', 'title', 'image', 'created_at']);

                $show_count = $all_shows->count();

                foreach ($all_shows as $key => $show) {
                    if ($key < 6) {
                        $show_data[] = [
                            'id' => $show->id,
                            'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                            'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                            'url_slug' => Helper::make_slug($show->title)
                        ];
                    }
                }

                //New New Episodes
                $all_episodes = \App\Models\EpisodeDetail::where('status', "Published")
                    ->whereHas('episode', function ($qry) use ($marketplace_country) {
                        $qry->whereHas('premium_pricing', function ($q) use ($marketplace_country) {
                            $q->where('country_id', $marketplace_country->id);
                            $q->where('currency_id', $marketplace_country->currency_id);
                        });
                    })
                    ->orderBy('order', 'ASC')->orderBy('created_at', 'DESC')
                    ->take(7)->get(['id', 'user_id', 'episode_id', 'content_type', 'created_at']);

                $episode_count = $all_episodes->count();

                foreach ($all_episodes as $key => $item) {
                    if ($key < 6) {
                        $product_price = $item->episode->premium_pricing()->where('country_id', $marketplace_country->id)
                            ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);

                        $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
                        $purchase = "no";
                        if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
                            $purchase = "free";
                        }

                        $episode_data[] = [
                            'id' => $item->episode->id,
                            'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                            'url_slug' => Helper::make_slug($item->episode->title),
                            'image' => !empty($item->episode->image) ? $item->getImage(200) : asset('uploads/default/show.png'),
                            'selling_price' => $selling_price,
                            'selling_currency' => $selling_currency,
                            'duration' => $item->episode->getDurationText(),
                            'created_date' => \Carbon\Carbon::parse($item->episode->date_created)->format('jS M Y'),
                            'premium' => 'yes',
                            'purchase' => $purchase
                        ];
                    }
                }
            }
        }

        $result = [
            'banner' => $bannerArray,
            'shows' => $show_data,
            'episodes' => $episode_data,
            'show_count' => $show_count,
            'episode_count' => $episode_count
        ];

        return $result;
    }

    /**
     * Marketplace Page
     * Homepage
     */
    public function marketplace_masthead()
    {

        $request = request();

        //Fetching mraketplace promotion banner
        $banner = new Banners('web');
        $bannerArray = $banner->marketplace_banner_listing();

        $result = [
            'banner' => $bannerArray
        ];

        return $result;
    }

    /**
     * Marketplace Page
     * Recently Added Shows
     */
    public function recently_added_shows()
    {
        $request = request();

        $noofrecords = 12;
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        $show_data = [];
        //Ip2Location code start            
        $ip_address = $request->ip_address;
        //$ip_address = "128.90.101.253";
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();

        //dd($location);

        $country_id = 1;
        $currency_id = 1;
        if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
            //Check if marketplace available in user country
            $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")
                ->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id"]);
            if ($marketplace_country) {
                $country_id = $marketplace_country->id;
                $currency_id = $marketplace_country->currency_id;
            }

            $all_shows = Show::published()->where('content_type', 1)->whereNotNull('user_id')->has('episodes')
                ->whereHas('premium_pricing', function ($qry) use ($country_id, $currency_id) {
                    $qry->where('country_id', $country_id);
                    $qry->where('currency_id', $currency_id);
                })
                ->orderBy('created_at', 'DESC')->paginate($noofrecords, ['id', 'user_id', 'title', 'image', 'created_at']);

            foreach ($all_shows as $key => $show) {
                $show_data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'url_slug' => Helper::make_slug($show->title)
                ];
            }

            return [
                'total' => $all_shows->total(),
                'per_page' => $all_shows->perPage(),
                'pages' => ceil($all_shows->total() / $all_shows->perPage()),
                'items' => $show_data,
            ];
        }

        return [
            'total' => 0,
            'per_page' => 0,
            'pages' => 1,
            'items' => [],
        ];
    }

    /**
     * Marketplace Page
     * Recently Added Episodes
     */
    public function recently_added_episodes()
    {
        $request = request();

        $noofrecords = 8;
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        //Ip2Location code start            
        $ip_address = $request->ip_address;
        //$ip_address = "128.90.101.253";
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();

        $country_id = 1;
        $currency_id = 1;
        $selling_currency = "INR";
        $selling_price = "";
        if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
            //Check if marketplace available in user country
            $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")
                ->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id"]);

            if ($marketplace_country) {
                $selling_currency = $marketplace_country->currency->title;
                $country_id = $marketplace_country->id;
                $currency_id = $marketplace_country->currency_id;
            }
        }

        //New New Episodes
        $all_episodes = \App\Models\EpisodeDetail::where('status', "Published")
            ->whereHas('episode', function ($qry) use ($country_id, $currency_id) {
                $qry->whereHas('premium_pricing', function ($q) use ($country_id, $currency_id) {
                    $q->where('country_id', $country_id);
                    $q->where('currency_id', $currency_id);
                });
            })->orderBy('order', 'ASC')->orderBy('created_at', 'DESC')
            ->paginate($noofrecords, ['id', 'user_id', 'episode_id', 'content_type', 'created_at']);

        $episode_data = [];
        foreach ($all_episodes as $key => $item) {
            $product_price = $item->episode->premium_pricing()->where('country_id', $marketplace_country->id)
                ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
            $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
            $purchase = "no";
            if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
                $purchase = "free";
            }

            $episode_data[] = [
                'id' => $item->episode->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                'url_slug' => Helper::make_slug($item->episode->title),
                'show_id' => trim(str_replace("\n", '', html_entity_decode($item->episode->show->id))),
                'show_title' => trim(str_replace("\n", '', html_entity_decode($item->episode->show->title))),                
                'image' => !empty($item->episode->image) ? $item->getImage(200) : asset('uploads/default/show.png'),
                'selling_price' => $selling_price,
                'selling_currency' => $selling_currency,
                'duration' => $item->episode->getDurationText(),
                'created_date' => \Carbon\Carbon::parse($item->episode->date_created)->format('jS M Y'),
                'premium' => 'yes',
                'purchase' => $purchase
            ];
        }

        if (count($episode_data) == 0) {
            return [
                'total' => 0,
                'per_page' => 0,
                'pages' => 1,
                'items' => [],
            ];
        }

        return [
            'total' => $all_episodes->total(),
            'per_page' => $all_episodes->perPage(),
            'pages' => ceil($all_episodes->total() / $all_episodes->perPage()),
            'items' => $episode_data,
        ];
    }

    /**
     * Marketplace Shots Banner
     * Recently Added Episodes
     */
    public function standalone_episodes_shots()
    {
        $request = request();

        $noofrecords = 5;
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        //Ip2Location code start            
        $ip_address = $request->ip_address;
        //$ip_address = "128.90.101.253";
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();

        $country_id = 1;
        $currency_id = 1;
        if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
            //Check if marketplace available in user country
            $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")
            ->where("admin_status", "Approved")
            ->where('country_alpha2_code', $location['data']['country_code'])
            ->first(["id", "title", "country_alpha2_code", "currency_id"]);

            if ($marketplace_country) {
                $country_id = $marketplace_country->id;
                $currency_id = $marketplace_country->currency_id;
            }
        }

        //New Episodes
        $all_episodes = \App\Models\EpisodeDetail::where('content_type', 2)->where('status', "Published")
        ->whereHas('episode', function ($qry) use ($country_id, $currency_id) {
            $qry->whereHas('premium_pricing', function ($q) use ($country_id, $currency_id) {
                $q->where('country_id', $country_id);
                $q->where('currency_id', $currency_id);
            });
        })->orderBy('order', 'ASC')->orderBy('created_at', 'DESC')
        ->paginate($noofrecords, ['id', 'episode_id', 'shots', 'created_at']);

        $episode_data = [];
        $count = 0;
        foreach ($all_episodes as $item) {
            if (!is_null($item->shots)) {
                $episode_data[] = [
                    'id' => $item->episode_id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                    'url_slug' => Helper::make_slug($item->episode->title),
                    'image' => !empty($item->shots) ? $item->getShotImage(1200, 600) : "",
                    'image2' => config('config.s3_url') . '/episodes/shots/' . $item->shots
                ];
                $count++;
            }
        }

        return [
            'total' => $count,
            'items' => $episode_data,
        ];
    }

    /**
     * Marketplace Page
     * Standalone Episodes
     */
    public function standalone_episodes()
    {
        $request = request();

        $noofrecords = 8;
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        //Ip2Location code start            
        $ip_address = $request->ip_address;
        //$ip_address = "128.90.101.253";
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();

        $country_id = 1;
        $currency_id = 1;
        $selling_currency = "INR";
        $selling_price = "";
        if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
            //Check if marketplace available in user country
            $marketplace_country = \App\Models\MarketplaceCountry::where("status", "Published")
            ->where("admin_status", "Approved")
            ->where('country_alpha2_code', $location['data']['country_code'])
            ->first(["id", "title", "country_alpha2_code", "currency_id"]);

            if ($marketplace_country) {
                $selling_currency = $marketplace_country->currency->title;
                $country_id = $marketplace_country->id;
                $currency_id = $marketplace_country->currency_id;
            }
        }

        //New New Episodes
        $all_episodes = \App\Models\EpisodeDetail::where('status', "Published")
        ->whereHas('episode', function ($qry) use ($country_id, $currency_id) {
            $qry->whereHas('premium_pricing', function ($q) use ($country_id, $currency_id) {
                $q->where('country_id', $country_id);
                $q->where('currency_id', $currency_id);
            });
        })->where('content_type', 2)->orderBy('order', 'ASC')->orderBy('created_at', 'DESC')
        ->paginate($noofrecords, ['id', 'user_id', 'episode_id', 'content_type', 'created_at']);

        $episode_data = [];
        foreach ($all_episodes as $key => $item) {
            $product_price = $item->episode->premium_pricing()->where('country_id', $marketplace_country->id)
                ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
            $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
            $purchase = "no";
            if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
                $purchase = "free";
            }

            $episode_data[] = [
                'id' => $item->episode->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                'url_slug' => Helper::make_slug($item->episode->title),
                'show_id' => trim(str_replace("\n", '', html_entity_decode($item->episode->show->id))),
                'show_title' => trim(str_replace("\n", '', html_entity_decode($item->episode->show->title))),
                'image' => !empty($item->episode->image) ? $item->getImage(200) : asset('uploads/default/show.png'),
                'selling_price' => $selling_price,
                'selling_currency' => $selling_currency,
                'duration' => $item->episode->getDurationText(),
                'created_date' => \Carbon\Carbon::parse($item->episode->date_created)->format('jS M Y'),
                'premium' => 'yes',
                'purchase' => $purchase
            ];
        }

        if (count($episode_data) == 0) {
            return [
                'total' => 0,
                'per_page' => 0,
                'pages' => 1,
                'items' => [],
            ];
        }

        return [
            'total' => $all_episodes->total(),
            'per_page' => $all_episodes->perPage(),
            'pages' => ceil($all_episodes->total() / $all_episodes->perPage()),
            'items' => $episode_data,
        ];
    }
}
