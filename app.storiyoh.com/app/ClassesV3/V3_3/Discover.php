<?php
namespace App\ClassesV3\V3_3;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Traits\Helper;
use App\Models\Show;
use App\Models\Chart;
use App\Models\PodcastDay;
use App\Models\Board;
use App\Models\SmartPlaylist;
use App\Models\TrendingShow;
use App\Models\Network;
use App\Models\DiscoverMasthead;
use App\Traits\ResponseFormat;
use Illuminate\Support\Facades\Cache;
use App\Models\Category;
use App\Models\PopularShow;
use App\Models\Episode;

class Discover
{
    use ResponseFormat;
    /**
     * api type
     */
    protected $api_type;

    /**
     * user data
     */
    protected $user;

    /**
     * Constructor
     *
     * @param $api_type
     * @param $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->user = $user;
    }

    /**
     * DIscover Masthead
     *
     * @param Type $request 
     * @return type
     * @throws conditon
     **/
    public function discover_masthead()
    {
        $request = request();   
        try {
            $date = Carbon::today();
            $list = DiscoverMasthead::Published()->orderBy('updated_at', 'DESC')->take(10)->get();
           
            $result = $list->map(function($item) {
                switch( $item->type ) {
                    case 'Podcast': 
                        $data = Show::find($item->content_id);
                        $image = $data->getWSImage(230);
                    break;
                    case 'Playlist' :
                        $data = Board::find($item->content_id);
                        $image = $data->getImage(230);
                    break;
                    case 'Smart Playlist' :
                        $data = SmartPlaylist::find($item->content_id);
                        $image = $data->getImage(230);
                    break;
                    case 'Episode' :
                        $data = Episode::find($item->content_id);
                        $image = $data->show->getWSImage(230);
                    break;
                    default:
                        $data = Chart::find($item->content_id);
                        $image = $data->getImage(230);
                }
               
                return [
                    'id' => $item->id,
                    'type' => $item->type,
                    'content_id' => $item->content_id,
                    'title' => trim(str_replace("\n", '', $data->title)),
                    'image' => $image,
                    'description' => str_limit($data->description, 200, '...'),
                    'url_slug' => Helper::make_slug(trim($data->title)),
                ];
            });

            return $this->sendResponse($result);
        } catch (\Exception $e) {
            return $this->sendExceptionError($e->getCode(), $e->getMessage());
        }
        
        
    }
    /**
     * Pod of the day
     *
     * @param Type $request 
     * @return type
     * @throws conditon
     **/
    public function pod_of_the_day()
    {
        $request = request();   
        try {
            $date = Carbon::today();
            $noofrecords = config('config.pagination.discover_pod_of_the_day');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }
           
            $list = PodcastDay::whereDate('date', '<=', $date)->Published()->orderBy('date', 'DESC')->distinct('id')->paginate($noofrecords);
            $subscribers = $this->api_type == 'web' ? 0 : $this->user->subscribers()->pluck('show_id')->all();
            $result = $list->map(function($item) use ( $subscribers) {
                $subscribe = @in_array($item->show->id, $subscribers) ? 'true' : 'false';
                return [
                    'id' => $item->show->id,
                    'title' => trim(str_replace("\n", '', $item->show->title)),
                    'image' => $item->show->getWSImage(230),
                    'description' => str_limit($item->show->description, 200, '...'),
                    'url_slug' => Helper::make_slug(trim($item->show->title)),
                    'subscribe' => $subscribe
                ];
            });

            $data = [
                'total' => $list->total(),
                'total_number_format' => number_format($list->total()),
                'per_page' =>(int) $list->perPage(),
                'pages' => ceil($list->total() / $list->perPage()),
                'items' => $result
            ];

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            return $this->sendExceptionError($e->getCode(), $e->getMessage());
        }
        
        
    }
    
    /**
     * Popular shows
     *
     * @param Type $request 
     * @return type
     * @throws conditon
     **/
    public function popular_shows()
    {
        $request = request();   
        try {
            $date = Carbon::today();
            $noofrecords = config('config.pagination.discover_popular_shows');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }
           
            $list = TrendingShow::orderBy('created_at','DESC')->paginate($noofrecords);
            $subscribers = $this->api_type == 'web' ? 0 : $this->user->subscribers()->pluck('show_id')->all();
            $result = $list->map(function($show) use ( $subscribers) {
                $item = Show::where('itunes_id', $show->itunes_id)->distinct('id')->first(['id', 'title','image']);
                $subscribe = @in_array($item->id, $subscribers) ? 'true' : 'false';
                return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'image' => $item->getWSImage(200),
                    'url_slug'  => Helper::make_slug($item->title),
                    'subscribe' => $subscribe
                ];
            });

            $data = [
                'total' => $list->total(),
                'total_number_format' => number_format($list->total()),
                'per_page' =>(int) $list->perPage(),
                'pages' => ceil($list->total() / $list->perPage()),
                'items' => $result
            ];

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            return $this->sendExceptionError($e->getCode(), $e->getMessage());
        }                
    }

   /**
     * Network list
     *
     * @param Type $request 
     * @return type
     * @throws conditon
     **/
    public function networks()
    {
        $request = request();   
        try {
            $noofrecords = config('config.pagination.discover_networks');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }
            $list = Network::published()->orderBy('updated_at', 'DESC')->paginate($noofrecords);

            $result = $list->map(function($item) {
                return [
                    'id' => $item->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/show.png'),
                    'url_slug'  => Helper::make_slug($item->title)
                ];
            });

            $data = [
                'total' => $list->total(),
                'total_number_format' => number_format($list->total()),
                'per_page' =>(int) $list->perPage(),
                'pages' => ceil($list->total() / $list->perPage()),
                'items' => $result
            ];

            return $this->sendResponse($data);
        
        } catch (\Exception $e) {
            return $this->sendExceptionError($e->getCode(), $e->getMessage());
        }
        
    }
    
    /**
     * Network shows list
     *
     * @param Type $request 
     * @return type
     * @throws conditon
     **/
    public function network_shows()
    {
        $request = request();  
        
        $validator = Validator::make($request->all(), 
        [
            'network_id' => 'required'
        ], [
            'network_id.required' => config('language.' . $this->getLocale() . ".Network.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $noofrecords = config('config.pagination.discover_network_shows');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }
            $networkData = Network::find($request->network_id);

            if (!$networkData) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_network_found_lbl")
                ]);
            }

            $query = $networkData->shows()->published();

            $total_query = clone $query;

            $pageNo = $request->page ? ($request->page - 1) : 0;
            $skip = (config('config.pagination.historical_recommended_show') * $pageNo) ;
        
            if($skip) {
                $query->skip($skip);
            }
            $network_shows = $query->orderBy('updated_at', 'DESC')->paginate($noofrecords);

            $subscribers = $this->api_type == 'web' ? 0 : $this->user->subscribers()->pluck('show_id')->all();

            $data = [];
            foreach ($network_shows as $show) {
                $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';            
                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),                
                    'url_slug' => str_slug(trim($show->title)),
                    'no_of_episodes' => 0,//$show->episodes()->published()->count(),
                    'subscribe' => $subscribe
                ];
            }

            $message = '';

            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
            }
        
            $list = [
                'total' => $network_shows->total(),
                'per_page' =>(int) $network_shows->perPage(),
                'pages' => ceil($network_shows->total() / $network_shows->perPage()),
                'items' => $data
            ];
            return $this->sendResponse($list);

        } catch (\Exception $e) {
            return $this->sendExceptionError($e->getCode(), $e->getMessage());
        }
        
    }

    /**
     * Categories list
     *
     * @param Type $request 
     * @return type
     * @throws conditon
     **/
    public function categories()
    {
        $request = request();   
        try {
            $noofrecords = config('config.pagination.discover_category');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }
            if (config('config.cache_status.show_categories')) {
                if (Cache::get('show.categories')) {
                    $categories = Cache::get('show.categories');
                } else {
                    //This code is use for cache that query data
                    $expiresAt = Carbon::now()->endOfDay()->addSecond();
                    $categories = Cache::remember('show.categories', $expiresAt, function () use ($noofrecords) {
                        return Category::published()->orderBy('title')->get();
                    });
                }
            } else {
                $categories = Category::published()->orderBy('title')->get();
            }
            $categories_data = [];
            foreach ($categories as $values) {
                $categories_data[] = [
                    'id' => $values->id,
                    'title' => $values->title,
                    'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/default.jpg'),
                    'url_slug' => Helper::make_slug($values->title)
                ];
            }   

            return $this->sendResponse($categories_data);
        } catch (\Exception $e) {
            return $this->sendExceptionError($e->getCode(), $e->getMessage());
        }
        
    }

    /**
     * Categories Shows list
     *
     * @param Type $request 
     * @return type
     * @throws conditon
     **/
    public function categories_shows()
    {
        $request = request();  
        $validator = Validator::make($request->all(), [
            'filter_category' => 'required',
        ], [
            'filter_category.required' => config('language.' . $this->getLocale() . ".Discover.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $noofrecords = config('config.pagination.discover_category_shows');
            if (! empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }
            $popular_shows = PopularShow::where('category_id', $request->filter_category)->first();
            
            if(!$popular_shows){
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl")
                ]);
            }
            
            $query = $popular_shows->shows()->published();
            
            if($this->api_type == 'api') {
                $subscribedShows = $this->user->subscribers()->pluck('id')->all();
                $query->whereNotIn('show_id', $subscribedShows);
            }

            $user_unsubscribed_shows = $query->orderBy('no_of_subscribers', 'DESC')->paginate($noofrecords, ['id', 'title', 'image', 'no_of_subscribers']);
            
            $data = [];
            foreach ($user_unsubscribed_shows as $show) {
                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'url_slug' => Helper::make_slug($show->title),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'no_of_episodes' => 0,//$show->episodes()->published()->count(),
                    'no_of_subscribers' => 0,//$show->subscribers->count(),
                    'subscribe' => 'false'
                ];
            }        
            $message = '';

            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
            }
            $list = [
                'total' => $user_unsubscribed_shows->total(),
                'per_page' =>(int) $user_unsubscribed_shows->perPage(),
                'pages' => ceil($user_unsubscribed_shows->total() / $user_unsubscribed_shows->perPage()),
                'items' => $data
            ];
              
            return $this->sendResponse($list);

        } catch (\Exception $e) {
            return $this->sendExceptionError($e->getCode(), $e->getMessage());
        }
        
    }
}
