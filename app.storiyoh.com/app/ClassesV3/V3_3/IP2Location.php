<?php
    namespace App\ClassesV3\V3_3;

    use Illuminate\Support\Facades\Validator;
    use DB;
    use App\Models\User;
    use App\Traits\ResponseFormat;
    use App\Models\Ip2locationDb3;

    class IP2Location
    {
        use ResponseFormat;

        /**
         * Undocumented variable
         *
         * @var [type]
         */
        protected $api_type;

        /**
         * Undocumented variable
         *
         * @var [type]
         */
        protected $user;

        /**
         * IP Address variable
         *
         * @var [type]
         */
        protected $ip_no;

        /**
         * Constructor
         *
         * @param $api_type
         * @param $user
         */
        public function __construct($api_type = null, $user = null, $ip_no = null)
        {
            $this->api_type = $api_type;
            $this->user = $user;
            $this->ip_no = $ip_no;
        }

        /**
         * Get Location from IP2Location
         *
         * @return array
         */
        public function get_location()
        {
            $request = request();
            
            /*
                // Get IP number from dotted IP address
                $ip_address = ! empty($this->ip_no) ? $this->ip_no : $request->ip();
                $ipno = $this->Dot2LongIP($ip_address);

                //Fetch location from ip number
                $country_region = DB::select(DB::raw("SELECT * FROM tbl_ip2location_db3 WHERE ? <= ip_to ORDER BY ip_to LIMIT 1"), array($ipno));            

                $data = [];            
                if(count($country_region) > 0) {
                    $data = [
                        "country_code" => $country_region[0]->country_code,
                        "country_name" => $country_region[0]->country_name,
                        "region_name" => $country_region[0]->region_name,
                        "city_name" => $country_region[0]->city_name,
                    ];
                }
            */

            try {
                // Get IP number from dotted IP address
                $ip_address = !empty($this->ip_no) ? $this->ip_no : $request->ip();

                $db = new \IP2Location\Database(database_path('ip2location/IP-COUNTRY-REGION-CITY.BIN'), \IP2Location\Database::FILE_IO);

                $country_region = $db->lookup($ip_address, \IP2Location\Database::ALL);

                $data = [];
                if ($country_region) {
                    $data = [
                        "country_code" => $country_region['countryCode'],
                        "country_name" => $country_region['countryName'],
                        "region_name" => $country_region['regionName'],
                        "city_name" => $country_region['cityName'],
                    ];
                } else {
                    $data = [
                        "country_code" => "-",
                        "country_name" => "-",
                        "region_name" => "-",
                        "city_name" => "-",
                    ];
                }
            } catch (\Exception $ex) {
                $data = [
                    "country_code" => "-",
                    "country_name" => "-",
                    "region_name" => "-",
                    "city_name" => "-",
                ];
            }

            return  ["data" => $data];
        }

        // Convert dotted IP address into IP number in long
        protected function Dot2LongIP ($IPaddr)
        {
            try {
                if ($IPaddr == "") {
                    return 0;
                } else {
                    $ips = explode(".", "$IPaddr");
                    return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
                }
            } catch(\Exception $ex){                
                return 0;
            }
        }
    }
?>