<?php

namespace App\ClassesV3\V3_3;

use App\Traits\ResponseFormatNew;
use Illuminate\Support\Facades\Validator;
use App\ClassesV3\V3_3\IP2Location;
use App\Models\MarketplaceCountry;
use App\Models\MarketplacePromotion;
use App\Models\Show;
use Carbon\Carbon;
use App\Models\EpisodeDetail;
use App\Models\Episode;
use App\Traits\Helper;
use Illuminate\Support\Facades\Cache;
use App\Models\User;
use DB;
use App\Traits\HelperV2;
use Illuminate\Http\Request;

use App\Models\ShowPurchase;
use App\Models\EpisodePurchase;
use App\Models\EpisodeResource; ///////new Code

class Marketplace
{
    use ResponseFormatNew, HelperV2;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $user;

    /**
     * Constructor
     *
     * @param $api_type
     * @param $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->user = $user;
    }

    /**
     * Check if marketplace available or not
     *
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function check_if_marketplace_available_for_country()
    {
        $request = request();

        try {
            //Ip2Location code start
            $ip_address = /*"183.87.113.164";*/ $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();

            if (empty($location['data']['country_code']) || $location['data']['country_code'] == "-") {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
                    'data' => [
                        "country_id" => 0,
                        "country_name" => "",
                        "currency_id" => 0,
                        "currency" => "",
                        "gatway" => ""
                    ]
                ]);
            }

            //Check if marketplace available in user country
            $marketplace_country = MarketplaceCountry::where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id", "status"]);
            //dd($marketplace_country->toArray());

            if (empty($marketplace_country)) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
                    'data' => [
                        "country_id" => 0,
                        "country_name" => "",
                        "currency_id" => 0,
                        "currency" => "",
                        "gatway" => ""
                    ]
                ]);
            }

            //Device detection code start
            //\Log::info($_SERVER['HTTP_USER_AGENT']);
            //$device_detection  = explode("/", $_SERVER['HTTP_USER_AGENT']);
           /*  if ($device_detection[0] == "okhttp" || $device_detection[0] == "PostmanRuntime") {
                //Do nothing                
            } else {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
                    'data' => [
                        'country_id' => $marketplace_country->id,
                        "country_name" => $marketplace_country->title,
                        'currency_id' => $marketplace_country->currency_id,
                        "currency" => !is_null($marketplace_country->currency_id) ? $marketplace_country->currency->title : ""
                    ]
                ]);
            } */
            //Device detection code end

            return response()->api([
                'status' => $marketplace_country->status == "Draft" ? false : true,
                'message' => $marketplace_country->status == "Draft" ? config('language.' . $this->getLocale() . ".Marketplace.country_missing") : "",
                'data' => [
                    'country_id' => $marketplace_country->id,
                    "country_name" => $marketplace_country->title,
                    'currency_id' => $marketplace_country->currency_id,
                    "currency" => !is_null($marketplace_country->currency_id) ? $marketplace_country->currency->title : "",
                    "gatway" => $marketplace_country->country_alpha2_code == "IN" ? "razorpay" : "paytabs",
                ]
            ]);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error"),
                'data' => [
                    "country_id" => 0,
                    "country_name" => "",
                    "currency_id" => 0,
                    "currency" => "",
                    "gatway" => ""
                ]
            ]);
        }
    }

    /**
     * Promotion Banner
     *
     * @param Request $request
     * @return mixed
     */
    public function promo_banner()
    {
        try {
            $promotions = MarketplacePromotion::adminpublished()->published()->orderBy('order')->get(['id', 'title', 'type', 'content_id', 'image']);

            $data = [];
            foreach ($promotions as $item) {
                $data[] = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'image' => !empty($item->image) ? $item->getImage(1200, 675) : '',
                    'type' => ($item->type === "Standalone") ? "Episode" : $item->type,
                    'content_id' => $item->content_id,
                ];
            }

            $response = ['items' => $data];

            return $this->sendResponse($response);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }

    /**
     * Dashboard New Series
     *
     * @param Request $request
     * @return mixed
     */
    public function new_series()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'country_id' => 'required',
            'currency_id' => 'required'
        ], [
            'country_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_id_lbl1"),
            'currency_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_id_lbl2"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $noofrecords = 10;
            if (!empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->paginate == "Yes") {
                $all_shows = Show::published()->where('content_type', 1)->whereNotNull('user_id')->has('episodes')
                    ->whereHas('premium_pricing', function ($qry) use ($request) {
                        $qry->where('country_id', $request->country_id);
                        $qry->where('currency_id', $request->currency_id);
                    })
                    ->orderBy('created_at', 'DESC')->paginate($noofrecords, ['id', 'user_id', 'title', 'image', 'created_at']);
            } else {
                $all_shows = Show::published()->where('content_type', 1)->whereNotNull('user_id')->has('episodes')
                    ->whereHas('premium_pricing', function ($qry) use ($request) {
                        $qry->where('country_id', $request->country_id);
                        $qry->where('currency_id', $request->currency_id);
                    })
                    ->orderBy('created_at', 'DESC')->take($noofrecords)->get(['id', 'user_id', 'title', 'image', 'created_at']);
            }

            $data = [];
            foreach ($all_shows as $show) {
                $product_ids = $show->premium_pricing()->where('country_id', $request->country_id)
                    ->where('currency_id', $request->currency_id)->first(['selling_price']);

                $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                //$android_product_id = ! empty($product_ids->android_product_id) ?  $product_ids->android_product_id : "";

                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'owner' => ($show->user_id == $this->user->id) ? "yes" : "no",
                    'premium' => "yes",
                    'selling_price' => $selling_price,
                    'purchase' => $this->check_if_item_purchase_by_user($show->user_id, $this->user->id, $show->id, 'series', null, $selling_price)
                    //'android_product_id' => $android_product_id
                    //'no_of_episodes' => 0,
                    //'no_of_subscribers' => 0,
                    //'subscribe' => false,
                    //'date' => Carbon::createFromFormat('Y-m-d H:i:s', $show->created_at)->format('jS M Y')
                ];
            }

            $response = [
                'total' => $request->paginate == "Yes" ? $all_shows->total() : $all_shows->count(),
                'per_page' => $request->paginate == "Yes" ? (int) $all_shows->perPage() : $noofrecords,
                'pages' => $request->paginate == "Yes" ? ceil($all_shows->total() / $all_shows->perPage()) : 1,
                'items' => $data
            ];

            return $this->sendResponse($response);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }

    /**
     * Dashboard Updated Series
     *
     * @param Request $request
     * @return mixed
     */
    public function updated_series()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'country_id' => 'required',
            'currency_id' => 'required'
        ], [
            'country_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_id_lbl1"),
            'currency_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_id_lbl2"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $noofrecords = 10;
            if (!empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->paginate == "Yes") {
                $all_shows = Show::published()->where('content_type', 1)->whereNotNull('user_id')->has('episodes')
                    ->whereHas('premium_pricing', function ($qry) use ($request) {
                        $qry->where('country_id', $request->country_id);
                        $qry->where('currency_id', $request->currency_id);
                    })
                    ->orderBy('updated_at', 'DESC')->paginate($noofrecords, ['id', 'user_id', 'title', 'image', 'updated_at']);
            } else {
                $all_shows = Show::published()->where('content_type', 1)->whereNotNull('user_id')->has('episodes')
                    ->whereHas('premium_pricing', function ($qry) use ($request) {
                        $qry->where('country_id', $request->country_id);
                        $qry->where('currency_id', $request->currency_id);
                    })
                    ->orderBy('updated_at', 'DESC')->get(['id', 'user_id', 'title', 'image', 'updated_at']);
            }

            $data = [];
            foreach ($all_shows as $show) {
                $product_ids = $show->premium_pricing()->where('country_id', $request->country_id)
                    ->where('currency_id', $request->currency_id)->first(['selling_price']);

                $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                //$android_product_id = ! empty($product_ids->android_product_id) ?  $product_ids->android_product_id : "";

                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'owner' => ($show->user_id == $this->user->id) ? "yes" : "no",
                    'premium' => "yes",
                    'selling_price' => $selling_price,
                    'purchase' => $this->check_if_item_purchase_by_user($show->user_id, $this->user->id, $show->id, 'series', null, $selling_price)
                    //'android_product_id' => $android_product_id
                    //'no_of_episodes' => 0,
                    //'no_of_subscribers' => 0,
                    //'subscribe' => false,
                    //'date' => Carbon::createFromFormat('Y-m-d H:i:s', $show->updated_at)->format('jS M Y')
                ];
            }

            $response = [
                'total' => $request->paginate == "Yes" ? $all_shows->total() : $all_shows->count(),
                'per_page' => $request->paginate == "Yes" ? (int) $all_shows->perPage() : $noofrecords,
                'pages' => $request->paginate == "Yes" ? ceil($all_shows->total() / $all_shows->perPage()) : 1,
                'items' => $data
            ];

            return $this->sendResponse($response);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }

    /**
     * Dashboard New Episodes
     *
     * @param Request $request
     * @return mixed
     */
    public function new_episodes()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'country_id' => 'required',
            'currency_id' => 'required'
        ], [
            'country_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_id_lbl1"),
            'currency_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_id_lbl2"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $noofrecords = 10;
            if (!empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            if ($request->paginate == "Yes") {
                $all_episodes = EpisodeDetail::where('status', "Published")
                    ->whereHas('episode', function ($qry) use ($request) {
                        $qry->whereHas('premium_pricing', function ($q) use ($request) {
                            $q->where('country_id', $request->country_id);
                            $q->where('currency_id', $request->currency_id);
                        });
                    })
                    ->orderBy('order', 'ASC')
                    ->orderBy('created_at', 'DESC')
                    ->paginate($noofrecords, ['id', 'user_id', 'episode_id', 'content_type', 'created_at']);
            } else {
                $all_episodes = EpisodeDetail::where('status', "Published")
                    ->whereHas('episode', function ($qry) use ($request) {
                        $qry->whereHas('premium_pricing', function ($q) use ($request) {
                            $q->where('country_id', $request->country_id);
                            $q->where('currency_id', $request->currency_id);
                        });
                    })
                    ->orderBy('order', 'ASC')
                    ->orderBy('created_at', 'DESC')
                    ->take($noofrecords)->get(['id', 'user_id', 'episode_id', 'content_type', 'created_at']);
            }

            $data = [];
            foreach ($all_episodes as $item) {
                $product_ids = $item->episode->premium_pricing()->where('country_id', $request->country_id)
                    ->where('currency_id', $request->currency_id)->first(['selling_price']);

                $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                $show_image = !empty($item->episode->show->image) ? $item->episode->show->getWSImage(200) : asset('uploads/default/show.png');
                if ($item->content_type == 2) {
                    $show_image = !empty($item->episode->image) ? $item->getImage(200) : asset('uploads/default/show.png');
                }

                $data[] = [
                    'id' => $item->episode->id,
                    'show_id' => @$item->episode->show->id,
                    'show_title' => html_entity_decode(@$item->episode->show->title),
                    'show_image' => $show_image,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                    'image' => !empty($item->episode->image) ? $item->getImage(200) : asset('uploads/default/show.png'),
                    'owner' => ($item->user_id == $this->user->id) ? "yes" : "no",
                    'premium' => "yes",
                    'standalone' => $item->content_type == 2 ? "yes" : "no",
                    'selling_price' => $selling_price,
                    'duration' => $item->episode->getDurationText(),
                    'created_date' => Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('jS M Y'),
                    'purchase' => $this->check_if_item_purchase_by_user($item->user_id, $this->user->id, $item->episode_id, 'episode', $item->episode->show->id, $selling_price),
                    "explicit" => $item->episode->show->explicit,
                    'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->episode->updated_at)->format('jS M Y'),
                ];
            }

            $response = [
                'total' => $request->paginate == "Yes" ? $all_episodes->total() : $all_episodes->count(),
                'per_page' => $request->paginate == "Yes" ? (int) $all_episodes->perPage() : $noofrecords,
                'pages' => $request->paginate == "Yes" ? ceil($all_episodes->total() / $all_episodes->perPage()) : 1,
                'items' => $data
            ];

            return $this->sendResponse($response);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }

    /**
     * This function is use for show details page
     */
    public function show_details()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Show
        $show = Show::where('id', $request->show_id)
            ->with('categories')->first([
                'id', 'user_id', 'title', 'image', 'language', 'artist_name',
                'description', 'no_of_subscribers', 'updated_at',
                'explicit', 'user_id', 'content_type', 'rating', 'country', 'status'
            ]);

        if (!$show) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                'data' => []
            ]);
        }

        //If premium content then update show status
        if($show->status == "Draft") {
            if($show->content_type == 0) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                    'data' => []
                ]);
            } else {
                //Update Show Status
                $show->fill(['status' => "Published"])->save();
                $show->unsearchable();
            }
        }

        $subscribe = false;
        $wishlist_status = 0;
        if ($this->api_type == 'api' && $show) {
            $subscribers = $this->user->subscribers()->pluck('show_id')->all();
            $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';

            $check = $this->user->wishlists()->where('content_id', $show->id)->where('content_type', "Series");
            $wishlist_status = $check->count();
        }

        $similar_podcast = [];
        $subscribersArray = [];
        $episodeArray = [];
        if ($this->api_type == 'web') {
            $categoryIds = $show->categories()->pluck('id')->all();
            $query = Show::published()->has('episodes')->withCount('episodes');
            $query->where(function ($query) use ($categoryIds) {
                $query->orWhereHas('categories', function ($q) use ($categoryIds) {
                    $q->whereIn('id', $categoryIds);
                });
            });

            $data = $query->where('id', '!=', $request->show_id)->where('language', $show->language)->take(5)
                ->orderBy('no_of_subscribers', 'desc')->get(['id', 'title', 'image']);
            foreach ($data as $item) {
                $similar_podcast[] = [
                    'id' => $item->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                    'slug' => Helper::make_slug(trim(@$item->title)),
                    'image' => !empty($item->image) ? $item->getWSImage(400) : asset('uploads/default/show.png'),
                    'total_episode' => $item->episodes_count . ' episodes',
                ];
            }

            //fetch latest episode data
            $more_episode_link = Helper::make_slug($show->title) . '/episodes/' . $show->id;
            $latest_episode = $show->episodes()->published()->orderBy('date_added', 'DESC')->limit(3)
                ->get(['id', 'title', 'show_id', 'date_added', 'duration']);

            $latestEpisodeArray = [];
            if ($latest_episode) {
                foreach ($latest_episode as $key => $value) {
                    $latestEpisodeArray[] = [
                        'id' => $value->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($value->title))),
                        'url_slug' => Helper::make_slug($value->title),
                        'duration' => $value->getDurationText(),
                        'show_id' => $value->show->id,
                        'image' => !empty($value->show->image) ? $value->show->getWSImage(200) : asset('uploads/default/show.png'),
                        'last_updated' => Carbon::parse($value->date_added)->format('jS M Y')
                    ];
                }
            }

            $subscribers = $show->subscribers()->verified()->get(['id', 'image', 'first_name', 'last_name']);
            if ($subscribers) {
                foreach ($subscribers as $key => $value1) {
                    $subscribersArray[] = [
                        'key' => $key + 1,
                        'id' => $value1->id,
                        'name' => trim($value1->first_name . ' ' . $value1->last_name),
                        'user_image' => !empty($value1->image) ? $value1->getImage(100) : asset('uploads/default/user.png'),
                    ];
                }
            }
        }

        //Check if this show is premium show
        $image_name = asset('uploads/default/show.png');
        $selling_price = "";
        $selling_currency = "";
        if ($show->content_type == 0) {
            $image_name = !empty($show->image) ? $show->getWSImage(400) : asset('uploads/default/show.png');
        } else {
            if ($show->content_type == 1) {
                //Ip2Location code start
                // $ip_address = "183.87.113.164";
                $ip_address = $request->ip_address;
                $responseData = new IP2Location('api', null, $ip_address);
                $location = $responseData->get_location();
                if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
                    //Check if marketplace available in user country
                    $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                        ->where('country_alpha2_code', $location['data']['country_code'])
                        ->first(["id", "title", "country_alpha2_code", "currency_id"]);
                    if ($marketplace_country) {
                        $product_ids = $show->premium_pricing()->where('country_id', $marketplace_country->id)
                            ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                        $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                        $selling_currency = $marketplace_country->country_alpha2_code;
                    }
                }
                $image_name = !empty($show->image) ? $show->getWSImage(400) : asset('uploads/default/show.png');
            } else if ($show->content_type == 2) {
                $epiosdeInfo = $show->episodes()->published()->orderBy('date_added', 'DESC')->first(['id', 'image']);
                $image_name = !empty($epiosdeInfo->image) ? $epiosdeInfo->getImage(400) : asset('uploads/default/show.png');
            }
        }

        //Check if existing user is owner or not
        $owner = "no";
        $seller_name = "";
        $seller_username = "";
        if (!is_null($show->user_id)) {
            if ($show->user_id == $this->user->id) {
                $owner = "yes";
            }

            $seller_name = $show->user->full_name;
            $seller_username = $show->user->username;
        }

        /** Language Code */
        $language = $show->language;
        if (isset(config('config.languages')[$language])) {
            $language = config('config.languages')[$language];
        }
        /** Language Code */

        /* No of Episode Count */
        $total_episode = 0;
        if ($show->content_type != 0) {
            $total_episode = EpisodeDetail::where('show_id', $show->id)->where('status', 'Published')->count();
        } else {
            $total_episode = $show->episodes()->count();
        }
        /* No of Episode Count */

        $data = [
            'id' => $show->id,
            'show_name' => trim(str_replace("\n", '', html_entity_decode($show->title))),
            'show_image' => $image_name,
            'show_slug' => Helper::make_slug(trim(@$show->title)),
            'category' => @implode(', ', @$show->categories()->pluck('title')->all()),
            'show_desc' => $this->api_type == 'api' ? strip_tags(trim($show->description)) : trim($show->description),
            'show_language' => @$language,//@$show->language,
            "show_country" => @$show->country,
            'explicit' => $show->explicit,
            'owner' => $owner,
            'user' => !is_null($show->user_id) ? $show->user->full_name : "",
            'username' => !is_null($show->user_id) ? $show->user->username : "",
            'premium' => $show->content_type == 0 ? "no" : "yes",
            'standalone' => $show->content_type == 2 ? "yes" : "no",
            'rating' => !is_null($show->rating) ? $show->rating : "",
            'selling_price' => $selling_price,
            'selling_currency' => $selling_currency,
            //'android_product_id' => $android_product_id,
            "share_url" => config('config.live_url') . "/podcasts/" . $show->id . "/" . Helper::make_slug(trim(@$show->title)),
            'artist_name' => is_null($show->artist_name) ? "" : $show->artist_name,
            'network_name' => is_null($show->network) ? "" : $show->network->title,
            'no_of_episodes' => $total_episode, //$show->episodes()->published()->count(),
            'last_updated' => !is_null($show->updated_at) ? @$show->updated_at->diffForHumans() : '',
            'subscribe' => $subscribe,
            'no_of_subscribers' => $show->subscribers()->verified()->count(),
            'subscribers' => $subscribersArray,
            'episodes' => $this->api_type == 'web' ? $latestEpisodeArray : $episodeArray,
            'more_episode_link' => $this->api_type == 'web' ? $more_episode_link : '',
            'purchase' => $this->check_if_item_purchase_by_user($show->user_id, $this->user->id, $show->id, 'series', null, $selling_price),
            'seller_name' => $seller_name,
            'seller_username' => $seller_username,
            'wishlist_status' => $wishlist_status == 0 ? "no" : "yes"
        ];

        return $this->sendResponse($data);
    }

    /** 
     * 
     * 
     */
    public function show_cast_crew()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'show_id' => 'required',
            'type' => 'required',
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl2"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Show
        //$show = Show::published()->where('id', $request->show_id)->first(['id', 'content_type']);
        $show = Show::where('id', $request->show_id)->first(['id', 'content_type']);

        if (!$show) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                'data' => []
            ]);
        }

        $all_episodes_ids = [];
        $user_array = [];
        if ($show->content_type != 0) {
            $all_episodes_ids = $show->episodes()->published()->pluck('id')->all();
            if (count($all_episodes_ids) > 0) {
                $all_user_ids = [];
                if ($request->type == "A") {
                    $all_user_ids = DB::table('episode_author')->whereIn('episode_id', $all_episodes_ids)->pluck('user_id')->all();
                } else if ($request->type == "N") {
                    $all_user_ids = DB::table('episode_narrator')->whereIn('episode_id', $all_episodes_ids)->pluck('user_id')->all();
                } else if ($request->type == "P") {
                    $all_user_ids = DB::table('episode_producer')->whereIn('episode_id', $all_episodes_ids)->pluck('user_id')->all();
                }
                if (count($all_user_ids) > 0) {

                    $user_id = $this->user->id;

                    $follow_status = 'false';
                    $user_follower = $this->user->following()->pluck('id')->all();

                    $users = User::whereIn('id', $all_user_ids)->approved()->verified()
                        ->whereNotNull('username')->where('username', '!=', '')
                        ->get(['id', 'first_name', 'username', 'image']);
                    foreach ($users as $item) {
                        $follow_status = @in_array($item->id, $user_follower) ? 'true' : 'false';
                        $user_array[] = [
                            'name' => $item->notification_format_name,
                            'username' => $item->username,
                            'owner' => $user_id == $item->id ? true : false,
                            'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                            'follow_status' => $follow_status,
                        ];
                    }
                }
            }
        }

        $message = '';
        if (count($user_array) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        $response = [
            'items' => $user_array
        ];

        return $this->sendResponse($response, $message);
    }

    /**
     * This function is use for getting show episodes
     */
    public function show_episodes()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Show
        // $show = Show::where('id', $request->show_id)->published()->first(['id', 'title', 'image', 'content_type']);
        $show = Show::where('id', $request->show_id)->first(['id', 'title', 'image', 'content_type']);

        if (!$show) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                'data' => []
            ]);
        }

        //all episodes
        $query = Episode::published()->where('show_id', $request->show_id);

        // New Code added for reorder
        $orderIDs = [];
        if ($show->content_type == 1) {
            $orderIDs = \App\Models\EpisodeDetail::where('show_id', $request->show_id)->orderBy('order', 'asc')->pluck('episode_id')->all();
        }
        if (count($orderIDs) > 0) {
            $orderIDs = implode(",", $orderIDs);
            $query->orderByRaw(\DB::raw("FIELD(id, " . $orderIDs . " )"));
        }
        // New Code added for reorder

        //No of record per Page
        $noofrecords = 10;
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        if ($request->paginate == "Yes") {
            $all_show_episode = $query->orderBy('date_added', 'DESC')->paginate($noofrecords, ['id', 'title', 'image', 'duration', 'mp3', 'show_id', 'date_added', 'updated_at', 'date_created', 'listen_count']);
        } else {
            $all_show_episode = $query->orderBy('date_added', 'DESC')->take($noofrecords)->get(['id', 'title', 'image', 'duration', 'mp3', 'show_id', 'date_added', 'updated_at', 'date_created', 'listen_count']);
        }

        $data = [];
        foreach ($all_show_episode as $item) {
            $owner = "no";
            $premium = "no";
            $standalone = "no";
            $selling_price = "";
            $selling_currency = "";
            $content_user_id = null;
            $episodeInfo = $item->episode_extra_info;
            if (!empty($episodeInfo)) {
                $image_name = !is_null($item->image) ? $item->getImage(200) : asset('uploads/default/show.png');
                $show_image_name = !is_null($item->show->image) ? $item->show->getImage(200) : asset('uploads/default/show.png');

                if ($episodeInfo->user_id == $this->user->id) {
                    $owner = "yes";
                }

                $premium = "yes";
                $standalone = $episodeInfo->content_type == 2 ? "yes" : "no";

                //Ip2Location code start
                // $ip_address = "183.87.113.164";
                $ip_address = $request->ip_address;
                $responseData = new IP2Location('api', null, $ip_address);
                $location = $responseData->get_location();
                if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
                    //Check if marketplace available in user country
                    $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                        ->where('country_alpha2_code', $location['data']['country_code'])
                        ->first(["id", "title", "country_alpha2_code", "currency_id"]);
                    if ($marketplace_country) {
                        $product_ids = $item->premium_pricing()->where('country_id', $marketplace_country->id)
                            ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                        $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                        $selling_currency = $marketplace_country->country_alpha2_code;
                    }
                }

                if ($episodeInfo->content_type == 2) {
                    $show_image_name = !is_null($item->image) ? $item->getImage(200) : asset('uploads/default/show.png');
                }

                $content_user_id = $episodeInfo->user_id;
            } else {
                $image_name = !is_null($item->show->image) ? $item->show->getImage(200) : asset('uploads/default/show.png');
                $show_image_name = !is_null($item->show->image) ? $item->show->getImage(200) : asset('uploads/default/show.png');
            }

            $data[] = [
                'id' => $item->id,
                'show_id' => html_entity_decode(@$item->show->id),
                'show_title' => html_entity_decode(@$item->show->title),
                'show_image' => $show_image_name,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'image' => $image_name,
                'owner' => $owner,
                'premium' => $premium,
                'standalone' => $standalone,
                'selling_price' => $selling_price,
                'audio_file' => $premium == "yes" ? "" : $item->getAudioLink(),
                'duration' => $item->getDurationText(),
                'created_date' => \Carbon\Carbon::parse($item->date_added)->format('jS M Y'),
                'purchase' => $this->check_if_item_purchase_by_user($content_user_id, $this->user->id, $item->id, 'episode', $item->show_id, $selling_price),
                "explicit" => $item->show->explicit,
                'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
            ];
        }
        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
        }

        $response = [
            'total' => $request->paginate == "Yes" ? $all_show_episode->total() : $all_show_episode->count(),
            'per_page' => $request->paginate == "Yes" ? (int) $all_show_episode->perPage() : $noofrecords,
            'pages' => $request->paginate == "Yes" ? ceil($all_show_episode->total() / $all_show_episode->perPage()) : 1,
            'items' => $data
        ];

        return $this->sendResponse($response, $message);
    }

    /**
     * This function is use for episode details
     */
    public function episode_details()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        // Show Episode
        /*if (config('config.cache_status.episode_details')) {
            if (Cache::get('episode.details.' . $request->episode_id)) {
                $episode = Cache::get('episode.details.' . $request->episode_id);
            } else {
                $episode = Episode::where('id', $request->episode_id)->with('tags')
                    ->first(['id', 'show_id', 'title', 'description', 'image', 'duration', 'mp3', 'date_created', 'listen_count', 'updated_at']);

                //This code is use for cache that query data
                $expiresAt = Carbon::now()->endOfDay()->addSecond();
                Cache::put('episode.details.' . $request->episode_id, $episode, $expiresAt);
            }
        } else {*/
            $episode = Episode::where('id', $request->episode_id)->with('tags')
                ->first(['id', 'show_id', 'title', 'description', 'image', 'duration', 'mp3', 'date_created', 'listen_count', 'updated_at']);
        //}

        if (!$episode) {
            $data = [];
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl"),
                'data' => $data
            ]);
        }

        $owner = "no";
        $premium = "no";
        $standalone = "no";
        //$android_product_id = "";
        $episode_language = !is_null($episode->show->language) ? $episode->show->language : "";
        $episode_rating = "";
        $episode_country = !is_null($episode->show->country) ? $episode->show->country : "";
        $content_user_id = null;
        $selling_price = "";
        $selling_currency = "";
        $seller_name = "";
        $seller_username = "";
        $resourcesFiles = []; ///////new Code
        $resourcesFlag = "no"; ///////new Code
        $resFileData = [];

        $episodeInfo = $episode->episode_extra_info;
        //dd($episodeInfo);

        if (!empty($episodeInfo)) {
            $image_name = !is_null($episode->image) ? $episode->getImage(200) : asset('uploads/default/show.png');
            $show_image_name = !is_null($episode->show->image) ? $episode->show->getImage(200) : asset('uploads/default/show.png');
            if ($episodeInfo->user_id == $this->user->id) {
                $owner = "yes";
            }

            $premium = "yes";
            $standalone = $episodeInfo->content_type == 2 ? "yes" : "no";

            ///////new Code
            $resourcesFiles = EpisodeResource::where('episode_id', $episode->id)->get(['id', 'res_title1', 'res_file1', 'res_title2', 'res_file2', 'res_title3', 'res_file3', 'res_title4', 'res_file4']);
            foreach ($resourcesFiles as $item) {
                if (!empty($item->res_file1) && !empty($item->res_title1)) {
                    $resFileData[] = [
                        "id" => 1,
                        "title" => $item->res_title1
                    ];
                }
                if (!empty($item->res_file2) && !empty($item->res_title2)) {
                    $resFileData[] = [
                        "id" => 2,
                        "title" => $item->res_title2
                    ];
                }
                if (!empty($item->res_file3) && !empty($item->res_title3)) {
                    $resFileData[] = [
                        "id" => 3,
                        "title" => $item->res_title3
                    ];
                }
                if (!empty($item->res_file4) && !empty($item->res_title4)) {
                    $resFileData[] = [
                        "id" => 4,
                        "title" => $item->res_title4
                    ];
                }
            }

            $resourcesFlag = is_null($episodeInfo->res_description) ? "no" : "yes";

            //Ip2Location code start
            // $ip_address = "183.87.113.164";
            $ip_address = $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();
            if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
                //Check if marketplace available in user country
                $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                    ->where('country_alpha2_code', $location['data']['country_code'])
                    ->first(["id", "title", "country_alpha2_code", "currency_id"]);
                if ($marketplace_country) {
                    $product_ids = $episode->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                    $selling_currency = $marketplace_country->country_alpha2_code;
                }
            }

            if ($episodeInfo->content_type == 2) {
                $show_image_name = !is_null($episode->image) ? $episode->getImage(200) : asset('uploads/default/show.png');
            }

            $episode_language = $episodeInfo->language;
            $episode_rating = $episodeInfo->rating;
            $content_user_id = $episodeInfo->user_id;

            $seller_name = $episodeInfo->user->full_name;
            $seller_username = $episodeInfo->user->username;
        } else {
            $image_name = !is_null($episode->image) ? $episode->show->getImage(200) : asset('uploads/default/show.png');
            $show_image_name = !is_null($episode->show->image) ? $episode->show->getImage(200) : asset('uploads/default/show.png');
        }

        $tags = $episode->tags()->pluck('title')->all();

        $wishlist_status = 0;
        if ($this->api_type == 'api' && $episode) {
            $check = $this->user->wishlists()->where('content_id', $episode->id)->where('content_type', "Episode");
            $wishlist_status = $check->count();
        }

        //getting episode category data
        $episode_category = $episode->show->categories()->pluck('title', 'id')->all();
        $catData = [];
        $category_listing = "";
        foreach ($episode_category as $key => $item) {
            $catData[] = [
                'id' => trim($key),
                'title' => trim($item),
            ];
            $category_listing .= $item . ",";
        }
        $category_listing = trim($category_listing, ",");

        /** Language Code */
        $language = $episode_language;
        if (isset(config('config.languages')[$language])) {
            $language = config('config.languages')[$language];
        }
        /** Language Code */

        $data = [
            'id' => $episode->id,
            'show_id' => trim(str_replace("\n", '', @$episode->show->id)),
            'show_name' => trim(str_replace("\n", '', html_entity_decode(@$episode->show->title))),
            "share_url" => config('config.live_url') . "/episode/" . $episode->id . "/" . Helper::make_slug(trim(str_replace("\n", '', @$episode->title))),
            'show_slug' => Helper::make_slug(trim(str_replace("\n", '', @$episode->show->title))),
            'show_category' => $category_listing,
            'episode_category' => $catData,
            'show_image' => $show_image_name,
            'episode_name' => trim(str_replace("\n", '', html_entity_decode($episode->title))),
            'episode_desc' => $this->api_type == 'api' ? strip_tags(trim($episode->description)) : trim($episode->description),
            'episode_image' => $image_name,
            'episode_language' => $language,//$episode_language,
            'episode_rating' => $episode_rating,
            'episode_country' => $episode_country,
            'episode_tags' => count($tags) > 0 ? implode(",", $tags) : "",
            'duration' => $episode->getDurationText(),
            'audio_file' => $episode->getAudioLink(),
            'owner' => $owner,
            'premium' => $premium,
            'standalone' => $standalone,
            'selling_price' => $selling_price,
            'selling_currency' => $selling_currency,
            'last_updated' => Carbon::parse($episode->updated_at)->format('jS M Y'),
            'num_of_listens' => Helper::shorten_count($episode->listen_count),
            'embed_url' => config('config.live_url') . "/player/" . $episode->id . "/embed",
            'purchase' => $this->check_if_item_purchase_by_user($content_user_id, $this->user->id, $episode->id, 'episode', $episode->show->id, $selling_price),
            'seller_name' => $seller_name,
            'seller_username' => $seller_username,
            "explicit" => $episode->show->explicit,
            'wishlist_status' => $wishlist_status == 0 ? "no" : "yes",
            'resourcesFiles' => $resFileData, ///////new Code
            'resourcesFlag' => $resourcesFlag ///////new Code
        ];

        return $this->sendResponse($data);
    }

    /** 
     * This is function is use for cast and crew
     * 
     */
    public function episode_cast_crew()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'type' => 'required',
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl2"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Episode
        $episode = Episode::where('id', $request->episode_id)->first(['id']);

        if (!$episode) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl"),
                'data' => []
            ]);
        }

        $user_array = [];
        if (!is_null($episode->episode_extra_info)) {
            if ($episode->episode_extra_info->content_type != 0) {
                $all_user_ids = [];
                if ($request->type == "A") {
                    $all_user_ids = $episode->authors()->pluck('user_id')->all();
                } elseif ($request->type == "N") {
                    $all_user_ids = $episode->narrators()->pluck('user_id')->all();
                } elseif ($request->type == "P") {
                    $all_user_ids = $episode->producers()->pluck('user_id')->all();
                }
                if (count($all_user_ids) > 0) {
                    $user_id = $this->user->id;

                    $follow_status = 'false';
                    $user_follower = $this->user->following()->pluck('id')->all();

                    $users = User::whereIn('id', $all_user_ids)->approved()->verified()
                        ->whereNotNull('username')->where('username', '!=', '')
                        ->get(['id', 'first_name', 'username', 'image']);
                    foreach ($users as $item) {
                        $follow_status = @in_array($item->id, $user_follower) ? 'true' : 'false';
                        $user_array[] = [
                            'name' => $item->notification_format_name,
                            'username' => $item->username,
                            'owner' => $user_id == $item->id ? true : false,
                            'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                            'follow_status' => $follow_status,
                        ];
                    }
                }
            }
        }

        $message = '';
        if (count($user_array) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        $response = [
            'items' => $user_array
        ];

        return $this->sendResponse($response, $message);
    }

    /**
     * This function is create temporary url for episode
     * 
     */
    public function single_signedin_url()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Episode Info
        $episode = Episode::where('id', $request->episode_id)->where('status', 'Published')
            ->first(['id', 'show_id', 'mp3']);

        if (!$episode) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl"),
                'data' => []
            ]);
        }

        $episodeInfo = $episode->episode_extra_info;

        if (!empty($episodeInfo)) {
            $purchase_status = $this->check_if_item_purchase_by_user($episodeInfo->user_id, $this->user->id, $episode->id, 'episode', $episode->show->id);

            //dd($episodeInfo->user_id, $this->user->id);

            if ($purchase_status == 'yes') { // If user purchase status is yes than we will create single signin url for episode                

                $audio_file_name = config('config.s3_marketplace_folder') . $episode->mp3;

                $response = $this->getSignedUrl($audio_file_name);

                return $this->sendResponse($response);
            } else { // if If user purchase status is no than we will send error message
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.not_purchase"),
                    'data' => []
                ]);
            }
        } else {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Marketplace.not_a_premium_episode"),
                'data' => []
            ]);
        }
    }

    /**
     * This function is use for seller profile
     * 
     */
    public function seller_profile()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Marketplace.username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('username', $request->username)->where('verified', "Verified")->where('admin_status', 'Approved')->first();
        if (is_null($user)) {
            $response = ['status' => false, 'message' => config('language.' . $this->getLocale() . ".Marketplace.seller_not_found_lbl"), 'data' => []];
            return $this->sendErrorResponse($response, 200);
        }

        if (!is_null($user->sellerInfo)) {
            $imageData = !is_null($user->sellerInfo->image) ? $user->sellerInfo->getImage(100) : asset('uploads/default/user.png');
            $data[] = [
                'id' => $user->sellerInfo->id,
                'name' => $user->sellerInfo->name,
                'email' => !is_null($user->sellerInfo->email) ? $user->sellerInfo->email : "",
                'image' => $imageData,
                'contact_no' => !is_null($user->sellerInfo->contact_no) ? $user->sellerInfo->contact_no : "",
                'overview' => !is_null($user->sellerInfo->overview) ? $user->sellerInfo->overview : "",
                'url' => !is_null($user->sellerInfo->url) ? $user->sellerInfo->url : "",
                'media_link_fb' => !is_null($user->sellerInfo->media_link_fb) ? $user->sellerInfo->media_link_fb : "",
                'media_link_twitter' => !is_null($user->sellerInfo->media_link_twitter) ? $user->sellerInfo->media_link_twitter : "",
                'media_link_linkedin' => !is_null($user->sellerInfo->media_link_linkedin) ? $user->sellerInfo->media_link_linkedin : "",
                'media_link_instagram' => !is_null($user->sellerInfo->media_link_instagram) ? $user->sellerInfo->media_link_instagram : "",
            ];
            return $this->sendResponse($data);
        } else {
            $response = ['status' => false, 'message' => config('language.' . $this->getLocale() . ".Marketplace.seller_not_found_lbl"), 'data' => []];
            return $this->sendErrorResponse($response, 200);
        }
    }

    /**
     * This function is use for seller shows
     * 
     */
    public function seller_series()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Marketplace.username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('username', $request->username)->where('verified', "Verified")->where('admin_status', 'Approved')->first(['id']);
        if (is_null($user)) {
            $response = ['status' => false, 'message' => config('language.' . $this->getLocale() . ".Marketplace.seller_not_found_lbl"), 'data' => []];
            return $this->sendErrorResponse($response, 200);
        }

        try {
            $noofrecords = 10;
            if (!empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            $ip_address = $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();
            if (empty($location['data']['country_code']) || $location['data']['country_code'] == "-") {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing")
                ]);
            }
            $marketplace_country = "";
            if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
                //Check if marketplace available in user country
                $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                    ->where('country_alpha2_code', $location['data']['country_code'])
                    ->first(["id", "title", "country_alpha2_code", "currency_id"]);
            }

            if ($request->paginate == "Yes") {
                $all_shows = Show::published()->where('user_id', $user->id)->where('content_type', 1)->has('episodes')
                    ->whereHas('premium_pricing', function ($qry) use ($marketplace_country) {
                        $qry->where('country_id', $marketplace_country->id);
                        $qry->where('currency_id', $marketplace_country->currency_id);
                    })
                    ->orderBy('created_at', 'DESC')->paginate($noofrecords, ['id', 'user_id', 'title', 'image', 'created_at']);
            } else {
                $all_shows = Show::published()->where('user_id', $user->id)->where('content_type', 1)->has('episodes')
                    ->whereHas('premium_pricing', function ($qry) use ($marketplace_country) {
                        $qry->where('country_id', $marketplace_country->id);
                        $qry->where('currency_id', $marketplace_country->currency_id);
                    })
                    ->orderBy('created_at', 'DESC')->take($noofrecords)->get(['id', 'user_id', 'title', 'image', 'created_at']);
            }

            $data = [];
            foreach ($all_shows as $show) {
                $selling_price = "";
                if (!empty($request->country_id) && !empty($request->currency_id)) {
                    $product_ids = $show->premium_pricing()->where('country_id', $request->country_id)
                        ->where('currency_id', $request->currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                }
                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'owner' => ($show->user_id == $this->user->id) ? "yes" : "no",
                    'premium' => "yes",
                    'selling_price' => $selling_price,
                    'purchase' => $this->check_if_item_purchase_by_user($show->user_id, $this->user->id, $show->id, 'series', null, $selling_price)
                ];
            }

            $response = [
                'total' => $request->paginate == "Yes" ? $all_shows->total() : $all_shows->count(),
                'per_page' => $request->paginate == "Yes" ? (int) $all_shows->perPage() : $noofrecords,
                'pages' => $request->paginate == "Yes" ? ceil($all_shows->total() / $all_shows->perPage()) : 1,
                'items' => $data
            ];

            return $this->sendResponse($response);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }

    /**
     * This function is use for seller episodes
     * 
     */
    public function seller_episodes()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Marketplace.username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('username', $request->username)->where('verified', "Verified")->where('admin_status', 'Approved')->first(['id']);
        if (is_null($user)) {
            $response = ['status' => false, 'message' => config('language.' . $this->getLocale() . ".Marketplace.seller_not_found_lbl"), 'data' => []];
            return $this->sendErrorResponse($response, 200);
        }

        try {
            $noofrecords = 10;
            if (!empty($request->noofrecords)) {
                $noofrecords = $request->noofrecords;
            }

            $ip_address = $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();
            if (empty($location['data']['country_code']) || $location['data']['country_code'] == "-") {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing")
                ]);
            }
            $marketplace_country = "";
            if (!empty($location['data']['country_code']) && $location['data']['country_code'] != "-") {
                //Check if marketplace available in user country
                $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                    ->where('country_alpha2_code', $location['data']['country_code'])
                    ->first(["id", "title", "country_alpha2_code", "currency_id"]);
            }

            if ($request->paginate == "Yes") {
                $all_episodes = EpisodeDetail::where('user_id', $user->id)->where('status', "Published")
                    ->where('content_type', 2)->whereHas('episode', function ($qry) use ($marketplace_country) {
                        $qry->whereHas('premium_pricing', function ($q) use ($marketplace_country) {
                            $q->where('country_id', $marketplace_country->id);
                            $q->where('currency_id', $marketplace_country->currency_id);
                        });
                    })
                    ->orderBy('created_at', 'DESC')
                    ->paginate($noofrecords, ['id', 'user_id', 'episode_id', 'content_type', 'created_at']);
            } else {
                $all_episodes = EpisodeDetail::where('user_id', $user->id)->where('status', "Published")
                    ->where('content_type', 2)->whereHas('episode', function ($qry) use ($marketplace_country) {
                        $qry->whereHas('premium_pricing', function ($q) use ($marketplace_country) {
                            $q->where('country_id', $marketplace_country->id);
                            $q->where('currency_id', $marketplace_country->currency_id);
                        });
                    })
                    ->orderBy('created_at', 'DESC')
                    ->take($noofrecords)->get(['id', 'user_id', 'episode_id', 'content_type', 'created_at']);
            }

            $data = [];
            foreach ($all_episodes as $item) {
                $selling_price = "";
                if (!empty($request->country_id) && !empty($request->currency_id)) {
                    $product_ids = $item->episode->premium_pricing()->where('country_id', $request->country_id)
                        ->where('currency_id', $request->currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                }

                $data[] = [
                    'id' => $item->episode->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                    'image' => !empty($item->episode->image) ? $item->getImage(200) : asset('uploads/default/show.png'),
                    'owner' => ($item->user_id == $this->user->id) ? "yes" : "no",
                    'premium' => "yes",
                    'standalone' => $item->content_type == 2 ? "yes" : "no",
                    'selling_price' => $selling_price,
                    'duration' => $item->episode->getDurationText(),
                    'created_date' => Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('jS M Y'),
                    'purchase' => $this->check_if_item_purchase_by_user($item->user_id, $this->user->id, $item->episode_id, 'episode', $item->episode->show->id, $selling_price)
                ];
            }

            $response = [
                'total' => $request->paginate == "Yes" ? $all_episodes->total() : $all_episodes->count(),
                'per_page' => $request->paginate == "Yes" ? (int) $all_episodes->perPage() : $noofrecords,
                'pages' => $request->paginate == "Yes" ? ceil($all_episodes->total() / $all_episodes->perPage()) : 1,
                'items' => $data
            ];

            $total_count = $request->paginate == "Yes" ? $all_episodes->total() : $all_episodes->count();
            $message = "";
            if ($total_count == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
            }

            return $this->sendResponse($response, $message);
        } catch (\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }

    /**
     * This function is use for user already
     * purchase this item or not
     * 
     */
    public function check_recipient_gift_status()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'content_type' => 'required',
            'email' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl1"),
            'content_type.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl2"),
            'email.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_email_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $recipient = User::where('email', $request->email)->first(['id', 'username', 'verified', 'admin_status']);

        if (is_null($recipient)) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Marketplace.recipient_not_found_lbl")
            ]);
        }

        if ($recipient->verified == 'Disabled') {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
            ]);
        }

        if (!$recipient->isVerified()) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Auth.login_account_verified_lbl")
            ]);
        }

        if (!$recipient->isApproved()) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_approved_lbl")
            ]);
        }

        if ($this->user->id == $recipient->id) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Marketplace.recipient_self_lbl")
            ]);
        }

        if ($request->content_type == "Series") {
            $purchased_status = ShowPurchase::where('show_id', $request->content_id)->where('user_id', $recipient->id)->count();
            if ($purchased_status) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.recipient_already_purchased_lbl"),
                ]);
            } else {
                return response()->api([
                    'status' => true,
                    'recipientId' => $recipient->id,
                    'message' => "",
                ]);
            }
        } else if ($request->content_type == "Episode") {

            $episode_data = Episode::find($request->content_id, ['show_id']);

            $purchased_show_status = ShowPurchase::where('show_id', $episode_data->show_id)->where('user_id', $recipient->id)->count();
            if ($purchased_show_status) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.recipient_already_purchased_lbl"),
                ]);
            }

            $purchased_status = EpisodePurchase::where('episode_id', $request->content_id)->where('user_id', $recipient->id)->count();
            //dd($request->content_id, $recipient->id, $purchased_status);
            if ($purchased_status) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.recipient_already_purchased_lbl"),
                ]);
            } else {
                return response()->api([
                    'status' => true,
                    'recipientId' => $recipient->id,
                    'message' => "",
                ]);
            }
        }

        return response()->api([
            'status' => false,
            'message' => config('language.' . $this->getLocale() . ".Common.error")
        ]);
    }

    /**
     * This function is create temporary url for episode
     * 
     */
    public function single_signedin_url_resaurces()
    {
        $request = request();

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'resaurces_id' => 'required',
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl"),
            'resaurces_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Episode Info
        $episode = Episode::where('id', $request->episode_id)->where('status', 'Published')->first(['id', 'show_id', 'mp3']);

        if (!$episode) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl"),
                'data' => []
            ]);
        }

        if ($request->resaurces_id == 1) {
            $resaucesData = EpisodeResource::where('episode_id', $episode->id)->first(['id', 'res_file1']);
        } else if ($request->resaurces_id == 2) {
            $resaucesData = EpisodeResource::where('episode_id', $episode->id)->first(['id', 'res_file2']);
        } else if ($request->resaurces_id == 3) {
            $resaucesData = EpisodeResource::where('episode_id', $episode->id)->first(['id', 'res_file3']);
        } else if ($request->resaurces_id == 4) {
            $resaucesData = EpisodeResource::where('episode_id', $episode->id)->first(['id', 'res_file4']);
        }
        $resauces_file_name = "";
        if (!empty($resaucesData)) {
            if (!is_null(env('DBLOCAL'))) {
                if ($request->resaurces_id == 1) {
                    $response = config('config.storiyoh_backend_url') . '/uploads/marketplace_resources/' . $resaucesData->res_file1;
                } else if ($request->resaurces_id == 2) {
                    $response = config('config.storiyoh_backend_url') . '/uploads/marketplace_resources/' . $resaucesData->res_file2;
                } else if ($request->resaurces_id == 3) {
                    $response = config('config.storiyoh_backend_url') . '/uploads/marketplace_resources/' . $resaucesData->res_file3;
                } else if ($request->resaurces_id == 4) {
                    $response = config('config.storiyoh_backend_url') . '/uploads/marketplace_resources/' . $resaucesData->res_file4;
                }
            } else {
                if ($request->resaurces_id == 1) {
                    if (!is_null($resaucesData->res_file1)) {
                        $resauces_file_name = config('config.s3_marketplace_resources_folder') . $resaucesData->res_file1;
                    }
                } else if ($request->resaurces_id == 2) {
                    if (!is_null($resaucesData->res_file2)) {
                        $resauces_file_name = config('config.s3_marketplace_resources_folder') . $resaucesData->res_file2;
                    }
                } else if ($request->resaurces_id == 3) {
                    if (!is_null($resaucesData->res_file3)) {
                        $resauces_file_name = config('config.s3_marketplace_resources_folder') . $resaucesData->res_file3;
                    }
                } else if ($request->resaurces_id == 4) {
                    if (!is_null($resaucesData->res_file4)) {
                        $resauces_file_name = config('config.s3_marketplace_resources_folder') . $resaucesData->res_file4;
                    }
                }
                $response = "";
                if (!empty($resauces_file_name)) {
                    $response = $this->getSignedUrl($resauces_file_name);
                }
            }
            return $this->sendResponse($response);
        } else {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_content_found_lbl"),
                'data' => []
            ]);
        }
    }
}
