<?php

namespace App\ClassesV3\V3_3;

use Illuminate\Support\Facades\Validator;
use App\Traits\Helper;
use Carbon\Carbon;
use DB;
use App\Models\Board;
use App\Models\User;
use App\Traits\ResponseFormat;

class Playlist
{
    use ResponseFormat;
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $user;

    /**
     * Constructor
     *
     * @param [type] $api_type
     * @param [type] $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->user = $user;
    }

   /**
     * Trending Playlist
     *
     * @return void
     */
    public function home_page_content_trending_playlist()
    {
        $request = request();     
        
        $data = array();
        $list = DB::table('board_user as bu')
                ->leftJoin('boards as b', 'b.id','=','bu.board_id')
                ->select('bu.board_id',DB::raw('count(*) as total'))
                ->where('b.private','N')
                ->whereBetween('bu.created_at',[Carbon::now()->subDay(56), Carbon::now()])
                ->groupBy('bu.board_id')
                ->orderBy('total','DESC')
                ->limit(4)
                ->get();
        if($list->isEmpty()) {
            $list = Board::where('private', 'N')->has('episodes')
                ->orderBy('updated_at', 'desc')
                ->select('id as board_id')
                ->limit(3)
                ->get();    
        }        
        foreach ($list as $key => $value) {
            $board = Board::find($value->board_id);    

            $data[] = [
                'id'    =>$board->id,
                'title' =>$board->title,
                'image' =>$board->getImage(200),
                'name'  =>$board->user->getFullNameAttribute(),
                'episode_count'=>$board->episodes->count(),
                'url_slug' => Helper::make_slug($board->title) 
            ];  
        }
        
        return $data;
    }

    /**
     * Playlist
     *
     * @return void
     */
    public function playlist()
    {
        $request = request();     
        $data = array();

        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (! empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }
       
        $list = Board::where('private', 'N')->has('episodes')
        ->orderBy('updated_at', 'desc')
        ->select('id as board_id')
        ->paginate($noofrecords);

        return $this->format($list);
    }
    /**
     * Trending Playlist
     *
     * @return void
     */
    public function trending_playlist()
    {
        $request = request();     
        $data = array();
        
        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (! empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }
        $list = DB::table('board_user as bu')
                ->leftJoin('boards as b', 'b.id','=','bu.board_id')
                ->select('bu.board_id',DB::raw('count(*) as total'))
                ->where('b.private','N')
                ->whereBetween('bu.created_at',[Carbon::now()->subDay(56), Carbon::now()])
                ->groupBy('bu.board_id')
                ->orderBy('total','DESC')
                ->paginate($noofrecords);
       
        return $this->format($list);
        
    }

    /**
     * Trending Playlist Detail
     *
     * @return void
     */
    public function trending_playlist_detail()
    {
        $request = request();     

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl")
            ]);
        }
        $board_follow_status = 0;
        $user_follow_status= 0;
        $user_id = $this->api_type == 'web' ? 0 : $this->user->id;
        if($this->api_type == 'api') {
            $board_follow_status = $this->user->following_boards()->where('board_user.board_id', $board->id)->count();
            $user_follow_status = $this->user->following()->where('user_follow.following_id', $board->user_id)->count();
        }

        $contributor_array = [];

        if (!is_null($board->contributors)) {
            $con_array = json_decode($board->contributors, 1);
            $con_array = array_reverse($con_array);
            $con_array = array_slice($con_array, 0, 5);
            $contributor_lists = User::whereIn('id', $con_array)->get(['id', 'first_name', 'username', 'image']);
            foreach ($contributor_lists as $key => $contributor_list) {
                $contributor_array[] = [
                'name' => $contributor_list->first_name,
                'username' => $contributor_list->username,
                'user_image' => $contributor_list->image ? $contributor_list->getImage(100) : asset('uploads/default/user.png')
                ];
            }
        }

        $data = [
            'id' => $board->id,
            'name' => $board->user->notification_format_name,
            'username' => $board->user->username,
            'board_name' => $board->title,
            'description' => $board->description,
            'private' => $board->private,
            'shared' => $board->shared,
            'share_url' => config('config.live_url')."/playlists/".$board->id."/".Helper::make_slug(trim(@$board->title)),
            'total_contributors' => is_null($board->contributors) ? 0 : count(json_decode($board->contributors, 1)),
            'contributors' => $contributor_array,
            'board_owner' => $user_id == $board->user_id ? 1 : 0, //api default 0
            'image' => !empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
            'user_image' => $board->user->image ? $board->user->getImage(100) : asset('uploads/default/user.png'),
            //'followers_count' => $user_follow_status == 0 ? $board->followers_count : $board->followers_count -1 ,
            'followers_count' => $board->followers->count(),
            'follow_status' => $board_follow_status,
            'user_follow_status' => $user_follow_status, //api
            'no_of_episodes' => $board->episodes()->count(),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $board->updated_at)->format('jS M Y')
        ];

        return $this->sendResponse($data) ;
    }

    /**
     * Trending Playlist Episodes
     *
     * @return void
     */
    public function trending_playlist_episodes()
    {
        $request = request();     
        $validator = Validator::make($request->all(), 
        [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl")
            ]);
        }
        $playlist = [
            'id' => $board->id,
            'title' => $board->title,
            'image' =>!empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
            'private' => $board->private,
            'shared' => $board->shared,
            'followers_count' => $board->followers_count
        ];
        //All Board Episodes
        $all_board_episode = $board->episodes()->orderBy('id', 'DESC')->paginate(10, ['id', 'title', 'duration', 'mp3', 'show_id', 'listen_count', 'explicit', 'date_added']);

        $data = [];
        $user_id = isset($this->user->id) ? $this->user->id : 0 ; 
        foreach ($all_board_episode as $item) {
            $show_delete_button = 'no';
            if ($board->user_id == $user_id) {
                $show_delete_button = 'yes';
            } else if ($item->pivot->contributor_id == $user_id) {
                $show_delete_button = 'yes';
            }

            $owner_name = "";
            $owner_username = "";
            if($board->shared == "Y") {
                if ($item->pivot->contributor_id == 0) {
                    $owner_name = $board->user->first_name;
                    $owner_username = $board->user->username;
                } else {
                    if ($item->pivot->contributor_id == $user_id) {
                        $owner_name = $this->user->first_name;
                        $owner_username = $this->user->username;
                    } else {
                        $user_info = User::find($item->pivot->contributor_id, ['first_name', 'username']);
                        $owner_name = $user_info->first_name;
                        $owner_username = $user_info->username;
                    }
                }
            }            

            $data[] = [
                'id' => $item->id,
                'title' => html_entity_decode($item->title),
                'url_slug' => Helper::make_slug($item->title),
                'duration' => $item->getDurationText(),
                'audio_file' => $item->getAudioLink(),
                'show_id' => $item->show_id,
                'show_title' => !empty($item->show) ? html_entity_decode($item->show->title) : 'No Show',
                'episode_image' => !empty($item->show) ? $item->show->getWSImage(200) : asset('uploads/default/board.png'),
                'listen_count' => Helper::shorten_count($item->listen_count),
                'explicit' => $item->show->explicit,
                'show_delete_button' => $show_delete_button,
                'shared' => $board->shared,
                'owner_name' => $owner_name,
                'owner_username' => $owner_username,
                'last_updated' => Carbon::parse($item->date_added)->format('jS M Y')                     
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
        }

        $result = [
            'total' => $all_board_episode->total(),
            'per_page' => $all_board_episode->perPage(),
            'pages' => ceil($all_board_episode->total() / $all_board_episode->perPage()),
            'items' => $data,
            'playlist' => $playlist
        ];

        return $this->sendResponse($result);
    }

    /**
     * Trending Playlist Followers
     *
     * @return void
     */
    public function trending_playlist_follwers()
    {
        $request = request();     
        $validator = Validator::make($request->all(), 
        [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();
        $message = '';
        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl")
            ]);
        }
        $playlist = [
            'id' => $board->id,
            'title' => $board->title,
            'image' =>!empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
            'private' => $board->private,
            'shared' => $board->shared,
            'followers_count' => $board->followers_count
        ];
        $user_id = isset($this->user->id) ? $this->user->id : 0 ;
        $all_users = $board->followers()->with('categories')->where('verified', 'Verified')
            ->where('admin_status', 'Approved')
            ->where('id', '!=', $user_id)
            ->withCount(['follower as follower_status_count' => function ($qry) use ($user_id) {
                $qry->where('id', $user_id);
            }])
            ->paginate(10, ['id', 'first_name', 'last_name', 'username', 'image']);


        $data = [];
        foreach ($all_users as $item) {
            $data[] = [
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follow_status' => $item->follower_status_count
            ];
        }

        $result = [
                'total' => $all_users->total(),
                'per_page' => $all_users->perPage(),
                'pages' => ceil($all_users->total() / $all_users->perPage()),
                'items' => $data,
                'playlist' => $playlist
        ];
        return $this->sendResponse($result);
    }


    /**
     * playlist and trending playlist format data
     *
     * @param Type $list
     * @return type
     * @throws conditon
     **/
    public function format($list)
    {
        $items = $list->map(function($item) {

            $board = Board::find($item->board_id);    
 
             return [
                 'id'    =>$board->id,
                 'title' =>$board->title,
                 'image' =>$board->getImage(200),
                 'name'  =>$board->user->getFullNameAttribute(),
                 'username' =>$board->user->username,
                 'episode_count'=>$board->episodes->count() ,
                 'follower_count' =>$board->followers->count(),
                 'author' => $board->user->getFullNameAttribute(),
                 'url_slug' => Helper::make_slug($board->title),
                 'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $board->updated_at)->format('jS M Y'),
             ]; 
         });
         
        //  if (count($items) == 0) {
        //      return $this->sendEmptyResponse(config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"));
        //  }
         $data = [
             'total' => $list->total(),
             'total_number_format' => number_format($list->total()),
             'per_page' =>(int) $list->perPage(),
             'pages' => ceil($list->total() / $list->perPage()),
             'items' => $items
         ];
         return $this->sendResponse($data) ;
    }
}
