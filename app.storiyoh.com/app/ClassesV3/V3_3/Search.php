<?php

namespace App\ClassesV3\V3_3;

use App\Models\Show;
use Illuminate\Support\Facades\Validator;
use App\Traits\Helper;
use Carbon\Carbon;
use App\Models\Board;
use App\Models\SmartPlaylist;
use App\Models\User;
use App\Traits\ResponseFormat;

class Search
{
    use ResponseFormat;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $userData;

    /**
     * Constructor
     *
     * @param [type] $api_type
     * @param [type] $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->userData = $user;
    }

    public function auto_search() {
        $request = request();
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => config('language.' . $this->getLocale() . ".Common.search_keyword_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $showData = Show::published()->where('content_type', 0)->where('title', 'LIKE', '%' . $request->keyword . '%')->has('episodes')
            ->take(10)->get(['id', 'title', 'image']);

        $data = [];
        foreach ($showData as $show) {
            $data[] = [
                'id' => $show->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'url_slug' => Helper::make_slug($show->title),
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'items' => $data,
            ]
        ]);
    }

    public function playlist_search() {
        $request = request();
        $user = $this->userData;

        $query = Board::where('private', 'N')
            ->withCount(['episodes', 'followers'])->has('episodes');

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('title', 'LIKE', '%' . $searchText . '%');

                //Search in username
                $query->orWhereHas('user', function ($q) use ($searchText) {
                    $q->where('username', 'LIKE', '%' . $searchText . '%');
                });
            });
        }

        if ($request->sort_by == 'last_updated') {
            $query->orderBy('updated_at', 'Desc');
        } else {
            $query->orderBy('title', 'ASC');
        }

        if (!empty($request->discover)) {
            $query->whereDoesntHave('followers', function ($q) use ($user) {
                $q->where('user_id', $user->id);
            });
        }

        $noofrecords = config('config.pagination.board');
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }
        //echo $query->toSql();
        if ($request->no_paginate == 'Yes') {
            $all_boards = $query->get(['id', 'user_id', 'title', 'description', 'image', 'private', 'episodes_count', 'updated_at', 'shared', 'contributors']);
        } else {
            $all_boards = $query->paginate($noofrecords, ['id', 'user_id', 'title', 'description', 'image', 'private', 'episodes_count', 'updated_at', 'shared', 'contributors']);
        }

        $follow_status = 0;
        $user_id = $this->api_type == 'web' ? 0 : $user->id;
        $user_boards = $this->api_type == 'api' ? $user->following_boards()->pluck('id')->all() : 0 ;

        $data = [];
        foreach ($all_boards as $item) {
            $follow_status = 0;
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 1;
            }
            $contributor_array = [];
            if (!is_null($item->contributors)) {
                $con_array = json_decode($item->contributors, 1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();
                foreach ($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }
            }

            $data[] = [
            'id' => $item->id,
            'board_name' => $item->title,
            'desc' => $item->description,
            'name' => @$item->user->notification_format_name,
            'url_slug' => Helper::make_slug($item->title),
            'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
            'private' => $item->private,
            'shared' => $item->shared,
            'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors, 1)),
            'contributors' => $contributor_array,
            'board_owner' => $user_id == $item->user_id ? 1 : 0,
            'user_follow_status' => $follow_status,
            'username' => $item->user->username,
            'user_photo' => !empty($item->user->image) ? $item->user->getImage(100) : asset('uploads/default/user.png'),
            'no_of_episodes' => $item->episodes_count,
            'no_of_follower' => Helper::shorten_count($item->followers_count),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_boards->total(),
                    'per_page' => $all_boards->perPage(),
                    'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                    'items' => $data
                ]
            ]);
        }
    }

    public function smartplaylist_search() {
        $request = request();
        $user = $this->userData;

        $query = SmartPlaylist::where('private', 'N');
        $user_id = $this->api_type == 'web' ? 0 : $this->userData->id;
        $query->whereDoesntHave('followers', function ($q) use ($user_id) {
            $q->where('user_id', $user_id);
        });

        $boardData = [];
        foreach ($query->get(['id', 'updated_at']) as $item) {
            $ShowArray = 0;
            foreach ($item->shows()->get(['shows.updated_at']) as $show) {
                if (strtotime($show->updated_at) > $ShowArray) {
                    $ShowArray = strtotime($show->updated_at);
                }
            }
            if ($ShowArray != 0) {
                if (strtotime($item->updated_at) > $ShowArray) {
                    $boardData[$item->id] = strtotime($item->updated_at);
                } else {
                    $boardData[$item->id] = $ShowArray;
                }
            }
        }

        $data = [];
        if (count($boardData) > 0) {
            arsort($boardData);

            $final_ids = array_keys($boardData);

            $query->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC');

            //Search Query
            if (! empty($request->keyword)) {
                $searchText = urldecode(trim($request->keyword));
                $query->where(function ($query) use ($searchText) {
                    $query->orWhere('title', 'LIKE', '%' . $searchText . '%');

                    //Search in username
                    $query->orWhereHas('user', function ($q) use ($searchText) {
                        $q->where('username', 'LIKE', '%' . $searchText . '%');
                    });
                });
            }

            $all_boards = $query->withCount('shows', 'followers')->paginate(10, ['id', 'user_id', 'title', 'description', 'image', 'private', 'updated_at']);

            //$follow_status = 0;
            //$user_boards = $user->following_smart_playlist()->pluck('id')->all();

            foreach ($all_boards as $item) {
                $data[] = [
                    'id' => $item->id,
                    'name' => @$item->user->notification_format_name,
                    'username' => @$item->user->username,
                    'board_name' => $item->title,
                    'url_slug' => Helper::make_slug($item->title),
                    'description' => $item->description,
                    'private' => $item->private,
                    'board_owner' => $user_id == $item->user_id ? 1 : 0,
                    'user_follow_status' => 0,
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                    'user_image' => @$item->user->image ? @$item->user->getImage(100) : '',
                    'no_of_follower' => Helper::shorten_count($item->followers_count),
                    'no_of_episodes' => $item->shows_count,
                    'timestamp' => strtotime($item->updated_at),
                    'last_updated' => @Carbon::createFromTimestamp($boardData[$item->id])->format('jS M Y'),
                ];
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_boards->total(),
                'per_page' => $all_boards->perPage(),
                'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                'items' => $data
            ]
        ]);
    }

    public function user_search () {
        $request = request();

        $user_id = $this->api_type == 'web' ? 0 : $this->userData->id;

        $user = $this->api_type == 'api' ? $this->userData : new User ;

        //Fetch all Users
        $query = User::approved()->verified()
        ->whereNotNull('username')
        ->where('username', '!=', '')
        ->where('id', '!=', $user_id)
        ->withCount(['follower', 'follower as follower_status_count' => function ($qry) use ($user_id) {
            $qry->where('id', $user_id);
        }])
        ->with(['categories' => function ($q) use ($user) {
            $q->whereNotIn('id', $user->categories()->pluck('id')->all());
        }]);

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('first_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('username', 'LIKE', '%' . $searchText . '%');
            });
        }

        //Sort By
        if ($request->sort_by == 'last_updated') {
            $query->orderBy('updated_at', 'Desc');
        } else {
            $query->orderBy('first_name', 'ASC');
        }

        // No of Records
        $noofrecords = config('config.pagination.user');
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        if ($request->no_paginate == 'Yes') {
            $users = $query->get(['id', 'first_name', 'last_name', 'username', 'image']);
        } else {
            $users = $query->paginate($noofrecords, ['id', 'first_name', 'last_name', 'username', 'image']);
        }

        $data = [];
        foreach ($users as $item) {
            $data[] = [
                //"id" => $item->id,
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => $item->categories()->pluck('title', 'id')->all(),
                'also_interested_category_implode' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follower_count' => $item->follower_count,
                'follow_status' => $item->follower_status_count
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'data' => [
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => $users->total(),
                    'per_page' => $users->perPage(),
                    'pages' => ceil($users->total() / $users->perPage()),
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        }
    }
}
