<?php

namespace App\ClassesV3\V3_3;

use Illuminate\Support\Facades\Validator;
use App\Traits\Helper;
use Carbon\Carbon;
use DB;
use App\Models\SmartPlaylist as SPlaylist;
use App\Traits\ResponseFormat;

class SmartPlaylist
{
    use ResponseFormat;
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $api_type;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $user;

    /**
     * Constructor
     *
     * @param [type] $api_type
     * @param [type] $user
     */
    public function __construct($api_type = null, $user = null)
    {
        $this->api_type = $api_type;
        $this->user = $user;
    }

    /**
     * Trending Smart Playlist
     *
     * @return void
     */
    public function home_page_content_trending_smart_playlist()
    {
        $request = request();     
        $data = array();
        $list = DB::table('smart_playlist_user as spu')
                ->leftJoin('smart_playlists as sp', 'sp.id','=','spu.smart_playlist_id')
                ->select('spu.smart_playlist_id',DB::raw('count(*) as total'))
                ->where('sp.private','N')
                ->whereBetween('spu.created_at',[Carbon::now()->subDay(56), Carbon::now()])
                ->groupBy('spu.smart_playlist_id')
                ->orderBy('total','DESC')
                ->limit(4)
                ->get();
        if($list->isEmpty()) {
            $list = SPlaylist::where('private', 'N')->has('shows')
            ->select('id as smart_playlist_id')
            ->orderBy('updated_at', 'desc')
            ->limit(3)
            ->get(); 
        }        
        foreach ($list as $key => $value) {
            $sp= SPlaylist::find($value->smart_playlist_id);    

            $data[] = [
                'id'    =>$sp->id,
                'title' =>$sp->title,
                'image' =>$sp->getImage(200),
                'name'  =>$sp->user->getFullNameAttribute(),
                'episode_count'=>$sp->shows->count() ,
                'url_slug' => Helper::make_slug($sp->title) 
            ]; 
        }
        return $data;
    }
  
    /**
     * Smart Playlist List
     *
     * @return void
     */
    public function smartplaylist()
    {
        $request = request();     
        $data = array();
        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (! empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }
        $list = SPlaylist::where('private', 'N')->has('shows')
                ->select('id as smart_playlist_id')
                ->orderBy('updated_at', 'desc')
                ->paginate($noofrecords);
        return $this->format($list);

    }

    /**
     * Trending Smart Playlist List
     *
     * @return void
     */
    public function trending_smartplaylist()
    {
        $request = request();     
        $data = array();
        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (! empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }
        $list = DB::table('smart_playlist_user as spu')
                ->leftJoin('smart_playlists as sp', 'sp.id','=','spu.smart_playlist_id')
                ->select('spu.smart_playlist_id',DB::raw('count(*) as total'))
                ->where('sp.private','N')
                ->whereBetween('spu.created_at',[Carbon::now()->subDay(56), Carbon::now()])
                ->groupBy('spu.smart_playlist_id')
                ->orderBy('total','DESC')
                ->paginate($noofrecords);

        return $this->format($list);
    }

    /**
     * Trending Smart Playlist Detail
     *
     * @return void
     */
    public function trending_smartplaylist_detail()
    {
        $request = request();     
        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SPlaylist::where('id', $request->board_id)->withCount('shows', 'followers')->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }
        $board_follow_status = 0;
        $user_follow_status= 0;
        $user_id = $this->api_type == 'web' ? 0 : $this->user->id;
        if($this->api_type == 'api') {
            $board_follow_status = $this->user->following_smart_playlist()->where('smart_playlist_user.smart_playlist_id', $smart_playlist->id)->count();
            $user_follow_status = $this->user->following()->where('user_follow.following_id', $smart_playlist->user_id)->count();
        }
        $data = [
            'id' => $smart_playlist->id,
            'name' => $smart_playlist->user->notification_format_name,
            'username' => $smart_playlist->user->username,
            'board_name' => $smart_playlist->title,
            'description' => $smart_playlist->description,
            'private' => $smart_playlist->private,
            'board_owner' => $user_id == $smart_playlist->user_id ? 1 : 0,
            'share_url' => config('config.live_url')."/smartplaylists/".$smart_playlist->id."/".Helper::make_slug(trim(@$smart_playlist->title)),
            'image' => !empty($smart_playlist->image) ? $smart_playlist->getImage(200) : asset('uploads/default/board.png'),
            'user_image' => $smart_playlist->user->image ? $smart_playlist->user->getImage(100) : asset('uploads/default/user.png'),
            //'followers_count' => $user_follow_status == 0 ? $smart_playlist->followers_count : $smart_playlist->followers_count -1 ,
            'followers_count' => $smart_playlist->following_user->count(),
            'follow_status' => $board_follow_status,
            'user_follow_status' => $user_follow_status,
            'no_of_episodes' => $smart_playlist->shows_count,
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $smart_playlist->updated_at)->format('jS M Y')
        ];

       return $this->sendResponse($data);
    }

    /**
     * Trending Smart Playlist Episodes
     *
     * @return void
     */
    public function trending_smartplaylist_episodes()
    {
        $request = request();     
        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SPlaylist::where('id', $request->board_id)->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }
        $splaylist = [
            'id' => $smart_playlist->id,
            'title' => $smart_playlist->title,
            'image' =>!empty($smart_playlist->image) ? $smart_playlist->getImage(200) : asset('uploads/default/board.png'),
            'private' => $smart_playlist->private,
            'shared' => $smart_playlist->shared,
            'followers_count' => $smart_playlist->followers_count
        ];
        //All Smart playlist Episodes
        $all_board_shows_temp = $smart_playlist->shows()->published()->orderBy('id', 'DESC')->get(['id', 'title']);
        
        $tempId = [];
        foreach($all_board_shows_temp as $temp) {
            $item = $temp->episodes()->orderBy('id', 'DESC')->first(['id', 'title', 'duration', 'mp3', 'show_id', 'updated_at']);
            $tempId[$temp->id] = strtotime($item->updated_at);
        }

        arsort($tempId);

        $final_ids = array_keys($tempId);
                
        if ($request->no_paginate == 'Yes') {
            $all_board_shows = $smart_playlist->shows()->published()->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC')
                ->get(['id', 'title']);
        } else {
            $all_board_shows = $smart_playlist->shows()->published()->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC')
                ->paginate(10, ['id', 'title']);
        }
        
        $data = [];
        foreach ($all_board_shows as $show) {
            $item = $show->episodes()->orderBy('id', 'DESC')->first(['id', 'title', 'duration', 'mp3', 'show_id', 'created_at', 'listen_count', 'explicit', 'date_added']);
            if (!empty($item)) {
                $data[] = [
                    'id' => $item->id,
                    'title' => html_entity_decode($item->title),
                    'url_slug' => Helper::make_slug($item->title),
                    'duration' => $item->getDurationText(),
                    'audio_file' => $item->getAudioLink(),
                    'show_id' => $item->show_id,
                    'show_title' => !empty($item->show) ? html_entity_decode($item->show->title) : 'No Show',
                    'episode_image' => !empty($item->show) ? $item->show->getWSImage(200) : asset('uploads/default/board.png'),
                    'listen_count' => Helper::shorten_count($item->listen_count), 
                    "explicit" => $item->show->explicit,
                    'last_updated' => Carbon::parse($item->date_added)->format('jS M Y')
                ];
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            $data = [   'total' => count($data), 
                        'per_page' => 10,
                        'pages' => ceil(count($data) / 10),
                        'items' => $data,
                        'splaylist' => $splaylist
            ];
            
        } else {
            $data = [   'total' => $all_board_shows->total(),
                        'per_page' => $all_board_shows->perPage(),
                        'pages' => ceil($all_board_shows->total() / $all_board_shows->perPage()),
                        'items' => $data,
                        'splaylist' => $splaylist
            ];
        }

        return $this->sendResponse($data);
    }

    /**
     * Trending Smart Playlist Followers
     *
     * @return void
     */
    public function trending_smartplaylist_follwers()
    {
        $request = request();     
        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SPlaylist::where('id', $request->board_id)->first();

        $message = '';
        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }
        $user_id = isset($this->user->id) ? $this->user->id : 0 ;
       
        $all_users = $smart_playlist->following_user()->with('categories')->where('verified', 'Verified')
            ->where('id', '!=', $user_id)
            ->where('admin_status', 'Approved')
            ->withCount(['follower as follower_status_count' => function ($qry) use ($user_id) {
                $qry->where('id', $user_id);
            }])->paginate(10, ['id', 'first_name', 'last_name', 'username', 'image']);

        $data = [];
        foreach ($all_users as $item) {
            $data[] = [
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follow_status' => $item->follower_status_count
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }
        $data = [
            'total' => $all_users->total(),
            'per_page' => $all_users->perPage(),
            'pages' => ceil($all_users->total() / $all_users->perPage()),
            'items' => $data
        ];

        return $this->sendResponse($data);
    }

    /**
     * Smart playlist and trending smart playlist format data
     *
     * @param Type $list
     * @return type
     * @throws conditon
     **/
    public function format($list)
    {
        $items = $list->map(function($item) {
            $sp= SPlaylist::find($item->smart_playlist_id);
             return [
                'id'    =>$sp->id,
                'title' =>$sp->title,
                'image' =>$sp->getImage(200),
                'name'  =>$sp->user->getFullNameAttribute(),
                'username' =>$sp->user->username,
                'episode_count'=>$sp->shows->count() ,
                'follower_count' =>$sp->following_user->count(),
                'author' => $sp->user->getFullNameAttribute(),
                'url_slug' => Helper::make_slug($sp->title),
                'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $sp->updated_at)->format('jS M Y'), 
             ]; 
         });
        // if (count($items) == 0) {
        //     return $this->sendEmptyResponse(config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl"));
        // }
        $data = [
            'total' => $list->total(),
            'total_number_format' => number_format($list->total()),
            'per_page' =>(int) $list->perPage(),
            'pages' => ceil($list->total() / $list->perPage()),
            'items' => $items
        ];

        return $this->sendResponse($data) ;
    }
}
