<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\Models\ActiveShow;
use App\Models\ShowActiveHourly;
use App\Models\ShowActiveDaily;
use App\Models\ShowActiveDailyOnce;
use App\Models\Show;
use App\Models\Episode;
use Illuminate\Support\Facades\Storage;

class ActiveBucket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'active:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Added into current Active Bucket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$lastRecord = DB::table('search_count')->where('type', 'ActBucket')->first();
        $created_at = Carbon::now();

        $show_active = ActiveShow::whereNull('cron_run_at')->where('no_of_episode', '!=', 1)->take(200)->orderBy('id', 'ASC')->get();
        
        //dd($show_active->toArray());

        foreach ($show_active as $item) {
            try {
                $showData = Show::find($item->show_id, ['id', 'itunes_id', 'feed_url', 'content_length']);

                $all_episodes = $showData->episodes()->whereNotNull('date_created')
                    ->orderBy('date_added', 'DESC')->take(6)
                    ->get(['id', 'title', 'show_id', 'date_created', 'date_added']);

                //dd($all_episodes->toArray());

                $diff = 0;
                $total_episode = $all_episodes->count() - 1;
                
                if ($all_episodes->count() == 2) {
                    $date1 = Carbon::parse($all_episodes[0]['date_added']);
                    $date2 = Carbon::parse($all_episodes[1]['date_added']);

                    $diff = $date1->diffInHours($date2);
                } elseif ($all_episodes->count() == 3) {
                    $date1 = Carbon::parse($all_episodes[0]['date_added']);
                    $date2 = Carbon::parse($all_episodes[1]['date_added']);
                    $date3 = Carbon::parse($all_episodes[2]['date_added']);

                    $diff1 = $date1->diffInHours($date2);
                    $diff2 = $date2->diffInHours($date3);
                    $diff = $diff1 + $diff2;
                } elseif ($all_episodes->count() == 4) {
                    $date1 = Carbon::parse($all_episodes[0]['date_added']);
                    $date2 = Carbon::parse($all_episodes[1]['date_added']);
                    $date3 = Carbon::parse($all_episodes[2]['date_added']);
                    $date4 = Carbon::parse($all_episodes[3]['date_added']);

                    $diff1 = $date1->diffInHours($date2);
                    $diff2 = $date2->diffInHours($date3);
                    $diff3 = $date3->diffInHours($date4);
                    $diff = $diff1 + $diff2 + $diff3;
                } elseif ($all_episodes->count() == 5) {
                    $date1 = Carbon::parse($all_episodes[0]['date_added']);
                    $date2 = Carbon::parse($all_episodes[1]['date_added']);
                    $date3 = Carbon::parse($all_episodes[2]['date_added']);
                    $date4 = Carbon::parse($all_episodes[3]['date_added']);
                    $date5 = Carbon::parse($all_episodes[4]['date_added']);

                    $diff1 = $date1->diffInHours($date2);
                    $diff2 = $date2->diffInHours($date3);
                    $diff3 = $date3->diffInHours($date4);
                    $diff4 = $date4->diffInHours($date5);
                    $diff = $diff1 + $diff2 + $diff3 + $diff4;
                } elseif ($all_episodes->count() == 6) {
                    $date1 = Carbon::parse($all_episodes[0]['date_added']);
                    $date2 = Carbon::parse($all_episodes[1]['date_added']);
                    $date3 = Carbon::parse($all_episodes[2]['date_added']);
                    $date4 = Carbon::parse($all_episodes[3]['date_added']);
                    $date5 = Carbon::parse($all_episodes[4]['date_added']);
                    $date6 = Carbon::parse($all_episodes[5]['date_added']);

                    $diff1 = $date1->diffInHours($date2);
                    $diff2 = $date2->diffInHours($date3);
                    $diff3 = $date3->diffInHours($date4);
                    $diff4 = $date4->diffInHours($date5);
                    $diff5 = $date5->diffInHours($date6);

                    $diff = $diff1 + $diff2 + $diff3 + $diff4 + $diff5;
                }

                //dump($all_episodes->count());                

                if ($all_episodes->count() > 1) {

                    $final_def = ($diff/$total_episode);

                    $has_hub = 0;
                    $hub_url = null;

                    /*try {
                        $str = file_get_contents($item->feed_url);
                        $output_array = [];

                        $strmatch = preg_match('/rel="hub" (.+?) \/>/', $str, $output_array);
                        if (count($output_array) == 0) {
                            $strmatch = preg_match('/rel="hub" (.+?)\/>/', $str, $output_array);
                        }
                        
                        if (count($output_array) > 0) {
                            $href = explode('href=', $output_array[1]);
                            $href = array_filter($href);
                            $final_href = trim($href[1], '""');
                            //$final_href = trim($final_href, '"');
                            $final_href = trim($final_href, '/');

                            $has_hub = 1;
                            $hub_url = $final_href;
                        }
                    } catch(\Exception $ex) {

                    } */                  

                    $arrayData = [
                        'show_id' => $showData->id,
                        'itunes_id' => $showData->itunes_id,
                        'feed_url' => $showData->feed_url,
                        'content_length' => $showData->content_length,
                        'date_between' => $final_def,
                        'cron_run_at' => Carbon::now()->format('Y-m-d'),
                        'has_hub' => $has_hub,
                        'hub_url' => $hub_url,
                    ];

                    //dump($arrayData);
                                        
                    $bucket_type = 0;
                    if ($final_def <= 6) { // Added 6 hrs bucket
                        $responseActive = ShowActiveHourly::firstOrNew(['show_id' => $showData->id]);
                        $responseActive->fill($arrayData)->save();
                        $bucket_type = 1;

                        //Create JSON file for new show
                        $showJsonData = Show::with(['episodes' => function($qry) {
                            $qry->where('status', 'Published');
                            $qry->whereNotNull('mp3');
                        }])->find($showData->id, ['id', 'content_length', 'last_update_timestamp']);
            
                        $episodeDataArray = $showJsonData->episodes->pluck('mp3')->all();
                        
                        $tm = $showJsonData->last_update_timestamp ? strtotime($showJsonData->last_update_timestamp) : 0;
                        $episodeData = count($episodeDataArray) > 0 ? $episodeDataArray : [];
                        
                        $epiArray = [];
                        if (count($episodeDataArray) > 0) {
                            foreach ($episodeData as $epi) {
                                $epiArray[] = strtolower($epi);
                            }
                        }
            
                        $dataArray = [
                            'show_id' => $showData->id, 
                            'itunes_id' => $showData->itunes_id, 
                            'feed_url' => $showData->feed_url,
                            'bucket_type' => 'hourly',
                            'content_length' => $showJsonData->content_length,
                            'last_update_timestamp' => $tm,
                            'episodes' => $epiArray
                        ];
                        
                        dump($dataArray);
                        if (count($dataArray) > 0) {
                            $dataJson = json_encode($dataArray);
                            Storage::disk('local')->put('JSON/hourly/'.$showJsonData->id.'.json', $dataJson);
                        }
                    } elseif ($final_def > 6 && $final_def <= 168) { // Added 12 hrs bucket
                        $responseData = ShowActiveDaily::firstOrNew(['show_id' => $showData->id]);
                        $responseData->fill($arrayData)->save();
                        $bucket_type = 2;

                        //Create JSON file for new show
                        $showJsonData = Show::with(['episodes' => function($qry) {
                            $qry->where('status', 'Published');
                            $qry->whereNotNull('mp3');
                        }])->find($showData->id, ['id', 'content_length', 'last_update_timestamp']);
            
                        $episodeDataArray = $showJsonData->episodes->pluck('mp3')->all();
                        
                        $tm = $showJsonData->last_update_timestamp ? strtotime($showJsonData->last_update_timestamp) : 0;
                        $episodeData = count($episodeDataArray) > 0 ? $episodeDataArray : [];
                        
                        $epiArray = [];
                        if (count($episodeDataArray) > 0) {
                            foreach ($episodeData as $epi) {
                                $epiArray[] = strtolower($epi);
                            }
                        }
            
                        $dataArray = [
                            'show_id' => $showData->id, 
                            'itunes_id' => $showData->itunes_id, 
                            'feed_url' => $showData->feed_url,
                            'bucket_type' => 'active_daily',
                            'content_length' => $showJsonData->content_length,
                            'last_update_timestamp' => $tm,
                            'episodes' => $epiArray
                        ];
                        
                        dump($dataArray);
                        if (count($dataArray) > 0) {
                            $dataJson = json_encode($dataArray);
                            Storage::disk('local')->put('JSON/active_daily/'.$showJsonData->id.'.json', $dataJson);
                        }
                    } else {
                        $responseData = ShowActiveDailyOnce::firstOrNew(['show_id' => $showData->id]);
                        $responseData->fill($arrayData)->save();

                        //Create JSON file for new show
                        $showJsonData = Show::with(['episodes' => function($qry) {
                            $qry->where('status', 'Published');
                            $qry->whereNotNull('mp3');
                        }])->find($showData->id, ['id', 'content_length', 'last_update_timestamp']);
            
                        $episodeDataArray = $showJsonData->episodes->pluck('mp3')->all();
                        
                        $tm = $showJsonData->last_update_timestamp ? strtotime($showJsonData->last_update_timestamp) : 0;
                        $episodeData = count($episodeDataArray) > 0 ? $episodeDataArray : [];
                        
                        $epiArray = [];
                        if (count($episodeDataArray) > 0) {
                            foreach ($episodeData as $epi) {
                                $epiArray[] = strtolower($epi);
                            }
                        }
            
                        $dataArray = [
                            'show_id' => $showData->id, 
                            'itunes_id' => $showData->itunes_id, 
                            'feed_url' => $showData->feed_url,
                            'bucket_type' => 'active_daily_once',
                            'content_length' => $showJsonData->content_length,
                            'last_update_timestamp' => $tm,
                            'episodes' => $epiArray
                        ];
                        
                        dump($dataArray);
                        if (count($dataArray) > 0) {
                            $dataJson = json_encode($dataArray);
                            Storage::disk('local')->put('JSON/active_daily_once/'.$showJsonData->id.'.json', $dataJson);
                        }
                    }

                    //if ($bucket_type > 0) { //Update Bucket Type
                        $item->fill([
                            'which_bucket' => $bucket_type, 
                            'date_between' => $final_def,
                            'cron_run_at' => Carbon::now()->format('Y-m-d'),
                            'content_length' => $showData->content_length,
                        ])->save();
                    //}
                } else {
                    $item->fill([
                        'no_of_episode' => $all_episodes->count()                     
                    ])->save();
                }
            } catch (\Exception $ex) {
                Log::info('Show - ' . $item->show_id);
                continue;
            }

            //$lastRecord = $item->id;
        }

        // $items_array = [
        //     'from_count' => $lastRecord,
        //     'created_at' => $created_at,
        //     'updated_at' => Carbon::now()
        // ];

        // DB::table('search_count')->where('type', 'ActBucket')->update($items_array);
    }
}
