<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\ShowActiveHourly;
use Tzsk\ScrapePod\Facade\ScrapePod;
use App\Models\Show;
use App\Models\Episode;
use App\Models\EpisodeTag;
use App\Classes\Feed;
use Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ActiveBucketHourly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'active:hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all shows in 4 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastrecord = DB::table('search_count')->where('type', 'ActHourly')->first();
        
        $active_shows = ShowActiveHourly::where('id', '>', $lastrecord->from_count)
            ->take(1000)->orderBy('id', 'ASC')->get();

        $lastRecordId = $lastrecord->from_count;
        
        $created_at = Carbon::now();

        foreach ($active_shows as $item) {
            try {
                $process = new Process('curl -s '.$item->feed_url.' | wc -c');
                $process->run();
                
                // executes after the command finishes                            
                if (! $process->isSuccessful()) {
                    throw new ProcessFailedException($process);                    
                }
                
                $length = "0";
                $length = trim($process->getOutput());
                
                /*
                dump($item->feed_url);
                dump("Show Id ===".$item->show_id); 
                
                echo $length;
                echo "<br>";
                echo strlen($length);
                echo "<br>";

                echo $item->content_length;
                echo "<br>";
                echo strlen($item->content_length);
                echo "<br>";
                if($length == $item->content_length) {
                    echo "Same";
                }*/
                
                if ($length == "0" || $length == 0 || ($length != $item->content_length)){
                    try {
                        $showData = Show::find($item->show_id);

                        //Fetch Data from Feed URL
                        $response = ScrapePod::feed($item->feed_url);

                        $bar = $this->output->createProgressBar(count($response['data']['episodes']));

                        $this->info('Fetching Episodes...');

                        if (count($response['data']['episodes']) > 0) {
                            //if (count($response['data']['episodes']) > $showData->no_of_episode) {
                            $no_of_shows = 0;
                            //Added all episodes to tbl_episodes table
                            foreach ($response['data']['episodes'] as $data) {
                                try {
                                    $uniqueID = md5($data['title'] . $showData->id . $data['size'] . $data['duration'] . $data['published_at']);

                                    $episode = Episode::firstOrNew(['uuid' => $uniqueID]);

                                    $newOrOld = $episode->exists;

                                    $attributes = $this->getEpisodeAttributes($data, $showData);
                                    $episode->fill($attributes)->save();

                                    $tags = $this->getTags($data);
                                    $episode->tags()->sync($tags);

                                    $categories = $showData->categories()->pluck('id')->all();
                                    $categories = array_filter($categories);
                                    $episode->categories()->sync($categories);

                                    if (!empty($attributes['show_id']) && !$newOrOld) {
                                        $showIds[$attributes['show_id']][] = trim($episode->id);
                                    }

                                    $bar->advance();

                                    if (!empty($attributes['show_id']) && !$newOrOld) {
                                        $episode->searchable();
                                    }

                                    $no_of_shows++;
                                } catch (\Exception $e) {
                                    $this->error($e->getMessage());
                                    Log::info($e->getMessage());
                                    Log::info('Process Show Itunes');
                                    $episode->unsearchable();
                                    continue;
                                }
                            }

                            if(strlen($length) > 50) {
                                $length = 0;
                            }
                            $showData->fill([
                                'web_url' => @$response['data']['site'],
                                'no_of_episode' => $no_of_shows, //@count($response['data']['episodes']),
                                'content_length' => @$length ? $length : 0
                            ])->save();

                            $showData->searchable();

                            $bar->finish();
                            
                            if (! empty($showIds)) {
                                //$this->makeActivity(array_filter($showIds));
                            }

                            $this->info("\nEpisodes Fetch Finished...");                            
                            //}
                        }

                        //Update timestamps for tbl_active_show table
                        $item->fill([
                            'cron_run_at' => Carbon::now()->format('Y-m-d'),
                            'content_length' => @$length ? $length : 0
                        ])->save();
                    } catch (\Exception $e) {
                        $this->error($e->getMessage());
                        Log::info($e->getMessage());

                        //Update timestamps for tbl_active_show table
                        $item->fill(['cron_run_at' => Carbon::now()->format('Y-m-d')])->save();
                        continue;
                    }
                }
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                continue;
            }

            $lastRecordId = $item->id;
        }

        $updated_at = Carbon::now();

        $items_array = [
            'from_count' => $lastRecordId,
            'created_at' => $created_at,
            'updated_at' => $updated_at
        ];

        DB::table('search_count')->where('type', 'ActHourly')->update($items_array);
    }

    /**
     * @param array $showIds
     * @return void
    */
    protected function makeActivity($showIds)
    {
        $shows = Show::whereIn('id', array_keys($showIds))->get(['id', 'updated_at']);
        foreach ($shows as $show) {
            $show->activities()->create([
                'type' => Feed::SHOW_UPDATE,
                'data' => json_encode($showIds[$show->id])
            ]);
            $show->touch();
        }
    }

    /**
     * @param $data
     * @return array
    */
    protected function getEpisodeAttributes($data, $show)
    {
        $date_created = null;
        if (!empty(@$data['published_at'])) {
            $date_created = Carbon::parse(@$data['published_at'])->format('Y-m-d');
            if ($date_created == '1970-01-01') {
                $date_created = null;
            }
        }

        $date_added = null;
        if (!empty(@$data['published_at'])) {
            $date_added = Carbon::parse(@$data['published_at'])->format('Y-m-d H:i:s');
            if ($date_added == '1970-01-01 05:30:00') {
                $date_added = null;
            }
        }

        $duration = 0;
        if (@$data['duration']) {
            if ($data['duration'] > 0) {
                $duration = $data['duration'];
            }
        }

        $size = 0;
        if (@$data['size']) {
            if ($data['size'] > 0) {
                $size = $data['size'];
            }
        }

        $attributes = [
            'show_id' => @$show->id,
            'itunes_id' => @$show->itunes_id,
            'title' => empty($data['title']) ? '-' : $data['title'],
            'description' => @$data['description'],
            'image' => @$data['image'],
            'duration' => $duration,
            'size' => $size,
            'mp3' => @$data['mp3'],
            'link' => @$data['link'],
            'date_created' => $date_created,
            'date_added' => $date_added,
            'created_by' => 1,
            'updated_by' => 1
        ];

        return $attributes;
    }

    /**
     * @param $data
     * @return array
    */
    protected function getTags($data)
    {
        $tags = [];
        $keywordsData = array_filter($data['keywords']);
        if (count($keywordsData)) {
            foreach ($keywordsData as $tag) {
                if (strlen($tag) <= 191) {
                    $tag = EpisodeTag::firstOrCreate(['title' => $tag]);
                    $tags[] = $tag->id;
                } else {
                    continue;
                }
            }
        }

        return $tags;
    }
}
