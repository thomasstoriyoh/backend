<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Feed;
use App\Models\PushSyncTimestamp;
use App\Models\Show;
use App\Jobs\CategoryEpisodePushNotificationJob;
use App\Models\PushId;
use App\Models\User;

class CategoryEpisodePushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:category_episode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification to intrested categories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date_time = \Carbon\Carbon::now();
        
        $last_timestamp = PushSyncTimestamp::whereType('Category')
            ->first(['id', 'last_feed_id', 'updated_at']);

        $user_feeds = Feed::where('typeable_type', 'App\Models\Show')
            ->where('type', \App\Classes\Feed::SHOW_UPDATE)
            ->where('updated_at', '>=', $last_timestamp->updated_at)
            ->get(['typeable_id', 'data']);

        $shows = Show::whereIn('id', $user_feeds->pluck('typeable_id'))
            ->with(['categories.users' => function($q) use ($user_feeds) {
                $q->where('verified', 'Verified')
                    ->where('admin_status', 'Approved')
                    ->wherePivot('notify', 1);
            }, 'subscribers'])->get(['id', 'title']);

        $push_array = []; 
        foreach($shows as $show) {
            foreach($show->categories as $cat) {
                foreach($cat->users as $user) {
                    if (! $show->subscribers->pluck('id')->contains($user->id)) {
                        $episodes = $user_feeds->where('typeable_id', $show->id)
                            ->pluck('data')->map(function($item) {
                                return json_decode($item);
                            })->flatten()->all();
                        
                        $push_array[$user->id][$show->id]['episodes'] = $episodes;
                        $push_array[$user->id][$show->id]['categories'] = $show->categories()->pluck('id')->all();
                    }
                }
            }
        }

        if(count($push_array) > 0) {            
            foreach ($push_array as $key => $user) {
                $tokens = PushId::active()
                    ->where('user_id', $key)
                    ->pluck('token')->all();

                if($tokens) {
                    $episode_count = [];
                    $show_categories = [];
                    $values = array_values($user);
                    foreach($values as $value) {
                        $episode_count[] = array_flatten($value['episodes']);
                        $show_categories[] = array_flatten($value['categories']);
                    }

                    $episode_count = array_unique(array_filter(array_flatten($episode_count)));
                    $show_categories = array_unique(array_filter(array_flatten($show_categories)));

                    $user_info = User::find($key);
                    $user_categories = $user_info->categories()->pluck('id')->all();
                    $user_show_category = array_intersect($user_categories, $show_categories);

                    $message1 = count($user) == 1 ? " Show has " : " Shows have ";
                    $message2 = count($episode_count) == 1 ? " new episode available." : " new episodes available.";
                    $mesage = count($user).$message1.count($episode_count).$message2;

                    $title = "Storiyoh Update";
                    $description = $mesage;
                    $icon = "";
                    $data = [
                        'title' => "Storiyoh Update",
                        'type' => "CNE",
                        'desc' => $mesage,
                        'ids' => json_encode($episode_count),
                        'category_ids' => json_encode(array_values($user_show_category)),
                        'tray_icon' => '',
                        'current_timestamp' => strtotime($current_date_time)
                    ];

                    //Dispatch Queue to schedule
                    dispatch(new CategoryEpisodePushNotificationJob($tokens, $title, $description, $icon, $data));
                }
            }
            //Update tbl_push_sync_timestamp table timestamp
            $last_timestamp->forceFill(['updated_at' => $current_date_time])->save();
        }
    }
}
