<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Feed;
use App\Models\PushSyncTimestamp;
use App\Models\Show;
use App\Jobs\EpisodePushNotificationJob;
use App\Models\PushId;

class EpisodePushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:episode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification to subscribe show';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date_time = \Carbon\Carbon::now();
        
        $last_timestamp = PushSyncTimestamp::whereType('Show')
            ->first(['id', 'last_feed_id', 'updated_at']);

        $user_feeds = Feed::where('typeable_type', 'App\Models\Show')
            ->where('type', \App\Classes\Feed::SHOW_UPDATE)
            ->where('updated_at', '>=', $last_timestamp->updated_at)
            ->get(['typeable_id', 'data']);

        $shows = Show::whereIn('id', $user_feeds->pluck('typeable_id'))
            ->with(['subscribers' => function($q) {
                $q->where('verified', 'Verified')
                  ->where('admin_status', 'Approved');
                $q->wherePivot('notify', 1);
            }])->get(['id', 'title']);

        $push_array = [];
        foreach ($shows as $show) {
            foreach ($show->subscribers as $sub) {
                $episodes = $user_feeds->where('typeable_id', $show->id)
                    ->pluck('data')->map(function($item) {
                        return json_decode($item);
                })->flatten()->all();
                $push_array[$sub->id][$show->id] = $episodes;
            }
        }

        if(count($push_array) > 0) {            
            foreach ($push_array as $key => $user) {
                $tokens = PushId::active()
                    ->where('user_id', $key)
                    ->pluck('token')->all();

                if($tokens) {

                    $episode_count = array_unique(array_flatten(array_values($user)));
                    $message1 = count($user) == 1 ? " Show has " : " Shows have ";
                    $message2 = count($episode_count) == 1 ? " new episode available." : " new episodes available.";
                    $mesage = count($user).$message1.count($episode_count).$message2;

                    $title = "Storiyoh Update";
                    $description = $mesage;
                    $icon = "";
                    $data = [
                        'title' => "Storiyoh Update",
                        'type' => "SNE",
                        'desc' => $mesage,
                        'ids' => json_encode($episode_count),
                        'tray_icon' => '',
                        'current_timestamp' => strtotime($current_date_time)
                    ];

                    //Dispatch Queue to schedule
                    dispatch(new EpisodePushNotificationJob($tokens, $title, $description, $icon, $data));
                }
            }
            //Update tbl_push_sync_timestamp table timestamp
            $last_timestamp->forceFill(['updated_at' => $current_date_time])->save();
        }
    }
}
