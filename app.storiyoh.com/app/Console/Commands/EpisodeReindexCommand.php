<?php

namespace App\Console\Commands;

//use App\Models\Episode;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Log;
use App\Models\Episode;

class EpisodeReindexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:episode_reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexes all episodes to elasticsearch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    //933810
    //938187
    public function handle()
    {
        //$this->info('Indexing all episodes. Might take a while...');
        $elasticsearch = DB::table('search_count')->where('type', 'Episode')->first();

        // $items = Episode::where('id','>',$elasticsearch->from_count)
        //     ->where('status', 'Published')->orderBy('id', 'ASC')->take(8000)->get();

        $items = Episode::where('id', '>', $elasticsearch->from_count)
            ->where('status', 'Published')->orderBy('id', 'ASC')->take(2500)->get();

        $from_count = $elasticsearch->from_count;

        $created_at = Carbon::now();

        if (sizeof($items)) {
            foreach ($items as $item) {
                dump($item->id);
                $item->searchable();

                // PHPUnit-style feedback
                /*$this->output->write('.');
                $this->output->write($item->id);*/
                $from_count = $item->id;
            }

            $updated_at = Carbon::now();

            $items_array = [
                'from_count' => $from_count,
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ];

            DB::table('search_count')->where('type', 'Episode')->update($items_array);
            //$this->info("\nDone!");

            //$this->info('Start: ' . $elasticsearch->from_count);
            //$this->info('End: ' . $from_count);

            Log::info('Start: ' . $elasticsearch->from_count);
            Log::info('End: ' . $from_count);
            Log::info('Done');
        }
    }
}
