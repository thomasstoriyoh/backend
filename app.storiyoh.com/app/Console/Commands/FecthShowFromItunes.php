<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ShowResponse;
use App\Models\ShowItuneId;
use Tzsk\ScrapePod\Facade\ScrapePod;
use App\Models\Show;
use App\Models\Country;
use App\Models\Category;
use App\Traits\Helper;
use Log;

class FecthShowFromItunes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'itunes:ids';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'We get all information from Itunes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $items = ShowItuneId::where('processed', 0)->take(10)->get();

        //$bar = $this->output->createProgressBar(count($items));

        $this->info("\nShow Processing Start...");

        foreach ($items as $item) {
            $response = ScrapePod::original()->find($item->itune_id);                   
            try {                                
                $value = $response['data']->results;
                dump("Ituen Id : ".@$value[0]->collectionId);
                $processed = 2;
                if (count($value) > 0) {

                    //Create Image on S3 Bucket Server
                    $imageData = Helper::createImage($value[0]->collectionId, @$value[0]->artworkUrl600);

                    $data = [
                        'itunes_id' => $value[0]->collectionId,
                        'title' => @$value[0]->collectionName ? @$value[0]->collectionName : "No Title",
                        'full_image' => @$value[0]->artworkUrl600,
                        'image' => $imageData,
                        'feed_url' => @$value[0]->feedUrl,
                        'artist_name' => @$value[0]->artistName,
                        'collection_name' => @$value[0]->collectionName,
                        'collection_censored_name' => @$value[0]->collectionCensoredName,
                        'country' => @$value[0]->country,
                        'status' => @$value[0]->feedUrl ? "Published" : "Draft",
                        'created_by' => 1,
                        'updated_by' => 1
                    ];

                    try {
                        //Insert into tbl_shows table
                        $show = Show::firstOrNew(['itunes_id' => $value[0]->collectionId]);
                        $show->fill($data)->save();

                        //dump($value[0]->collectionId, $show->id, strtoupper($value[0]->country));

                        // If country not found we will create new entry
                        try {
                            $countryData = Country::firstOrNew(['countries_iso_code_3' => strtoupper($value[0]->country)]);
                            $countryData->fill(['countries_iso_code_3' => strtoupper($value[0]->country)])->save();
                        } catch (\Exception $e) {
                            Log::info('Country not found.' . $value[0]->collectionId);
                        }

                        try {
                            //Insert Categories
                            $categories = $this->getCategories($value[0]->genres);

                            //Sync Categories with Shows
                            $categories = array_filter($categories);
                            $show->categories()->sync($categories);                            
                        } catch (\Exception $e) {
                            Log::info('Genre not found.' . $value[0]->collectionId);
                        }

                        //Also insert into tbl_show_responses table
                        try {
                            $response = ShowResponse::firstOrNew(['id' => $value[0]->collectionId]);
                            $response->fill(['data' => json_encode($value[0])])->save();
                        } catch (\Exception $e) {
                            $item->update(['processed' => 2, 'processed_at' => @date('Y-m-d H:i:s')]);
                            Log::info('Error Show Response.' . $value[0]->collectionId);
                        }
                    }
                    catch (\Exception $e) {
                        $item->update(['processed' => 2, 'processed_at' => @date('Y-m-d H:i:s')]);
                        Log::info('Error Show.' . $value[0]->collectionId);
                    }

                    $processed = 1;
                }
                
                dump("Show Id : ".@$show->id);

                $item->update(['processed' => $processed, 'processed_at' => @date('Y-m-d H:i:s')]);
            } catch (Exception $ex) {
                $this->error($item->id . ' - ' . $e->getMessage());
                Log::info($e->getMessage());
                Log::info('Fecth Show from Itunes');
                continue;
            }
            //$bar->advance();
        }

        //$bar->finish();

        $this->info("\nShow Processing Finished...");
    }

    /**
     *
     * @param type $data
     * @param type $parent_id
     * @return type
     */
    protected function getCategories($data, $parent_id = null)
    {
        $categories = [];
        if (count($data) > 0) {
            foreach ($data as $apiCategory) {
                $category = Category::firstOrNew(['title' => trim($apiCategory)]);
                //$category->fill(['parent_id' => $parent_id, 'title' => trim($apiCategory)])->save();
                $category->fill(['parent_id' => 0, 'title' => trim($apiCategory)])->save();
                $categories = array_merge($categories, [$category->id]);

                //$children = $this->getCategories($apiCategory['children'], $category->id);
                //$categories = array_merge($categories, $children);
            }
        }

        return $categories;
    }
}
