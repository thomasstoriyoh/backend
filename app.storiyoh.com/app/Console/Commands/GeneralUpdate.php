<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Show;
use App\Models\ActiveShow;
use App\Models\ShowLanguage;
use App\Models\Country;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Models\Episode;
use Illuminate\Support\Facades\DB;
use App\Models\ShowResponse;
use App\Models\Category;
use App\Models\ShowActiveHourly;
use App\Models\ShowActiveDaily;
use App\Models\ShowActiveDailyOnce;
use Illuminate\Support\Facades\Storage;
use App\Models\Chart;

use App\Models\User;
use App\Jobs\UserSubscribedFirstTime;

class GeneralUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'general:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Script1 Start');

        $userListing = User::whereNull('username')->get();
        foreach($userListing as $item) {

            $uname = uniqid();        
            $uname .= $this->getRandomNumber(10);
            $uname = "tmp_".$uname;

            //$item->update(['username' => $uname]);

            dump($uname, $item->id);
        }
        
        // $lastRecord = DB::table('search_count')->where('type', 'General')->first();        
        // $created_at = Carbon::now();

        // $users = User::where('id', '>', $lastRecord->from_count)->where('verified', 'Verified')
        //     ->where('admin_status', 'Approved')->where('topic_sync', 0)->take(1)->orderBy('id', 'ASC')->get();
        
        // foreach($users as $item) {
            
        //     //If topic_sync is 0 then we will call sync all topic for specific user
        //     if($item->topic_sync == 0) {
        //         dump("inside===".$item->id);
        //         dispatch(new UserSubscribedFirstTime($item->id))->onQueue('default');
        //     }

        //     $items_array = [
        //         'from_count' => $item->id,
        //         'created_at' => $created_at,
        //         'updated_at' => Carbon::now()
        //     ];

        //     DB::table('search_count')->where('type', 'General')->update($items_array);
        // }

        Log::info('Script End');
        exit;
        
        // $backdate = \Carbon\Carbon::now()->subMonths(3);
        // //dump($backdate);
    
        // $data = ShowActiveDaily::where('id', '>', 0)->take(10000)->get();
        // $count = 0;
        // foreach($data as $item) {
        //     dump($item->id);
        //     $query = Show::where('id', $item->show_id);
        //     $query->whereHas('episodes', function ($q) use ($backdate) {
        //         $q->where('date_created', '>=', $backdate->format('Y-m-d'));
        //     });
        //     $all_shows = $query->orderBy('id', 'ASC')->first();        
        //     //dump($all_shows);
        //     if(!$all_shows) {
        //         $item->delete();
        //         $count++;
        //     }            
        // }
        // echo $count;
        // exit;

        // $data = ShowActiveDailyOnce::where('has_hub', 2)->whereNull('hub_url')->skip(0)->take(500)->get();
        // foreach($data as $item) {
        //     try {
        //         $str = file_get_contents($item->feed_url);

        //         $strmatch = preg_match('/rel="hub" (.+?) \/>/', $str, $output_array);

        //         if (count($output_array) == 0) {
        //             $strmatch = preg_match('/rel="hub" (.+?)\/>/', $str, $output_array);
        //         }

        //         if (count($output_array)) {
        //             $href = explode('href=', $output_array[1]);
        //             $href = array_filter($href);
        //             $final_href = trim($href[1], '""');
        //             //$final_href = trim($final_href, '"');
        //             $final_href = trim($final_href, '/');

        //             $item->fill(['hub_url' => $final_href])->save();
        //         }

        //         dump($output_array);
        //     } catch(\Exception $ex) {
        //         dump($ex->getMessage());
        //         continue;
        //     }
        // }

        // exit;

        // $lastRecord = DB::table('search_count')->where('type', 'Hub1')->first();        
        
        // $created_at = Carbon::now();
        
        // $all_data = ShowActiveHourly::where('id', '>', $lastRecord->from_count)->take(200)->get();
        // dd($all_data);
        
        // foreach ($all_data as $item) {
        //     try {
        //         $data = file_get_contents($item->feed_url);
        //         if (strpos($data, 'rel="hub"') !== false) {
        //             $array_data = [
        //                 'has_hub' => 1,
        //                 //"hub_url" => @implode(",", $data['hub'])
        //             ];
        //             $item->fill($array_data)->save();
        //         }
        //         $lastRecord = $item->id;
        //     } catch(\Exception $ex) {
        //         Log::info('Error ==='. $item->id);
        //         $array_data = [
        //             'has_hub' => 2
        //         ];
        //         $item->fill($array_data)->save();
        //         $lastRecord = $item->id;
        //         continue;
        //     }
        // }
        
        // $items_array = [
        //     'from_count' => $lastRecord,
        //     'created_at' => $created_at,
        //     'updated_at' => Carbon::now()
        // ];
    
        // DB::table('search_count')->where('type', 'Hub1')->update($items_array);
        // Log::info('Script1 End');
        // exit;

        /*$lastRecord = DB::table('search_count')->where('type', 'Hub1')->first();        
        $created_at = Carbon::now();

        $all_data = ShowActiveHourly::where('id', '>', $lastRecord->from_count)->take(10)->get();
                
        foreach($all_data as $item) {
            Log::info('Current Id==='. $item->id); 
            dump('Current Id ==='. $item->id); 

            $curl = curl_init("https://websub.rocks/publisher/discover");    
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, ["topic" => $item->feed_url]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($curl, CURLOPT_VERBOSE, 1);
            $result = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            Log::info('Code==='. $code); 
            dump($code);
            try{
                $data = json_decode($result, true);
                Log::info('Hub==='. $data['hub']); 
                dump($data['hub']);
                if($data['hub']) {
                    $array_data = [
                        "has_hub" => 1, 
                        "hub_url" => @implode(",", $data['hub'])
                    ]; 
                    $item->fill($array_data)->save();
                }
            } catch(\Exception $ex) {
                Log::info('Error'. $ex->getMessage()); 
            }
            $lastRecord = $item->id;
        }

        $items_array = [
            'from_count' => $lastRecord,
            'created_at' => $created_at,
            'updated_at' => Carbon::now()
        ];

        DB::table('search_count')->where('type', 'Hub1')->update($items_array);*/

        /*$lastRecord = DB::table('search_count')->where('type', 'Show')->first();

        $created_at = Carbon::now();

        $show_active = Show::with('episodes')->where('id', '>', $lastRecord->from_count)->take(1000)->orderBy('id', 'ASC')->get(['id']);
        //$show_active = Show::with('episodes')->where('id', 27867)->take(5)->orderBy('id', 'ASC')->get(['id']);

        $showArray = [];
        $episodeIdsArray = [];
        foreach ($show_active as $item) {
            if ($item->episodes()->count() > 0) {
                $showArray[$item->id] = [];
                $allPodcastEpisodeData = $item->episodes()->where('status', 'Published')->orderBy('created_at', 'DESC')->get();
                foreach ($allPodcastEpisodeData as $episode_item) {
                    //dump($episode_item->show_id, $episode_item->mp3, $episode_item->created_at);
                    if ($episode_item->mp3 == '') {
                        //unpublished episode and remove from elasticsearch
                        $episode_item->fill(['status' => 'Draft'])->save();
                        $episode_item->unsearchable();
                    } else {
                        if (array_key_exists(strtolower($episode_item->mp3), $showArray[$item->id])) {
                            $episodeIdsArray[] = $episode_item->id;
                        } else {
                            $mp3_title = strtolower($episode_item->mp3);
                            $showArray[$item->id][$mp3_title] = $episode_item->id;
                        }
                    }
                }
                //dump($item->id, $showArray[$item->id]);
            }
            $lastRecord = $item->id;
        }

        //delete or draft episodes and also remove from elasticsearch
        foreach ($episodeIdsArray as $episodeDraftData) {
            $epiData = Episode::find($episodeDraftData);
            $epiData->fill(['status' => 'Duplicate'])->save();
            $epiData->unsearchable();
            //dump('Duplicate data found');
            //dump($epiData->toArray());
        }

        //dd($episodeIdsArray);

        $items_array = [
            'from_count' => $lastRecord,
            'created_at' => $created_at,
            'updated_at' => Carbon::now()
        ];

        DB::table('search_count')->where('type', 'Show')->update($items_array);
        */        

        /* This code is use for sitemap.xml
        $skip = 0;
        $max = 25000;
        $end = $skip + $max;
        $shows = Show::published()->skip($skip)->take($max)->orderBy('id', 'ASC')
            ->get(['id', 'title', 'updated_at']);

        //$shows = Chart::published()->orderBy('id', 'ASC')->get(['id', 'title', 'updated_at']);

        $data = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
                xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
                xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';

        foreach ($shows as $key => $route) {
            dump($route->id);
            $modified_date = $route->updated_at;
            $podcast_url = config('config.live_url')."/collections/".$route->id."/".\App\Traits\Helper::make_slug(trim(@$route->title));
            $params = [
                'url' => url($podcast_url),
                'added' => time(),
                'modified' => Carbon::parse($modified_date)->format('c'),
                'priority' => '0.5',
                'frequency' => 'monthly'
            ];

            $data .= '<url>
                <loc>'. $params['url'] .'</loc>
                <lastmod>'. $params['modified'] .'</lastmod>
                <changefreq>'. $params['frequency'] .'</changefreq>
                <priority>'. $params['priority'] .'</priority>
            </url>';
        }

        $data .= '</urlset>';

        Storage::disk('public')->put('sitemap/sitemap-shows-'.$skip.'-'.$end.'.xml', $data);

        //Storage::disk('public')->put('sitemap/sitemap-collections'.'.xml', $data);

        This code is use for sitemap.xml
        */

        // This code use for draft podcast if no episode found

        /*$lastRecord = DB::table('search_count')->where('type', 'Show')->first();

        $created_at = Carbon::now();

        $show_active = Show::withCount('episodes')->where('id', '>', $lastRecord->from_count)
            ->take(15)->orderBy('id', 'ASC')->get();

        foreach($show_active as $item) {
            dump($item->id);
            dump($item->episodes_count);

            if($item->episodes_count > 0) {
                //$item->fill(['no_of_episode' => $item->episodes_count, 'status' => 'Published'])->save();
                //$item->searchable();
            } else {
                //$item->fill(['no_of_episode' => $item->episodes_count, 'status' => 'Draft'])->save();
                //$item->unsearchable();
            }

            $lastRecord = $item->id;
        }

        $items_array = [
            'from_count' => $lastRecord,
            'created_at' => $created_at,
            'updated_at' => Carbon::now()
        ];

        DB::table('search_count')->where('type', 'Show')->update($items_array);
        */

        // $lastRecord = DB::table('search_count')->where('type', 'ActHourly')->first();

        // $created_at = Carbon::now();

        // $show_active = ActiveShow::where('id', '>', $lastRecord->from_count)
        //     ->where('which_bucket', 0)->take(5000)->orderBy('id', 'ASC')->get();

        // dd($show_active->toARray());
        // foreach($show_active as $item) {
        //     dump($item->id);
        //     $arrayData = [
        //         'show_id' => $item->show_id,
        //         'itunes_id' => $item->itunes_id,
        //         'feed_url' => $item->feed_url,
        //         'cron_run_at' => $item->cron_run_at,
        //         'content_length' => $item->content_length,
        //         'date_between' => $item->date_between
        //     ];

        //     $responseActive = ShowActiveDailyOnce::firstOrNew(['show_id' => $item->show_id]);
        //     $responseActive->fill($arrayData)->save();

        //     $lastRecord = $item->id;
        // }

        // $items_array = [
        //     'from_count' => $lastRecord,
        //     'created_at' => $created_at,
        //     'updated_at' => Carbon::now()
        // ];

        // DB::table('search_count')->where('type', 'ActHourly')->update($items_array);

        // $backdate = \Carbon\Carbon::now()->subMonths(config('config.active_show_interval'));
        // $query = Show::where('id', '>', 0);
        // $query->whereHas('episodes', function ($q) use ($backdate) {
        //     $q->where('date_created', '>=', $backdate->format('Y-m-d'));
        // });
        // $all_shows = $query->orderBy('id', 'ASC')->get(['id', 'itunes_id', 'feed_url']);
        // ///dd($all_shows->toArray());

        // foreach ($all_shows as $show) {
        //     $responseActive = ActiveShow::firstOrNew(['show_id' => $show->id]);
        //     $responseActive->fill([
        //         'show_id' => $show->id,
        //         'itunes_id' => $show->itunes_id,
        //         'feed_url' => $show->feed_url,
        //         'cron_run_at' => Carbon::now()
        //     ])->save();
        // }

        //This is use for all language create
        // $all_shows = Show::whereNotNull('language')->groupBy('language')->get(['language']);
        // foreach ($all_shows as $item) {
        //     $lang = ShowLanguage::firstOrNew(['short_code' => trim($item['language'])]);
        //     $lang->fill(['short_code' => trim($item['language']), 'created_by' => 1, 'updated_by' => 1])->save();
        // }

        // $showData = Show::all();
        // foreach ($showData as $item) {
        //     if ($item->country) {
        //         $country = Country::firstOrNew(['countries_iso_code_3' => strtoupper($item->country)]);
        //         $country->fill(['countries_iso_code_3', strtoupper($item->country)])->save();
        //     }
        // }

        // $file = public_path('uploads/csv/languages.csv');

        // $handle = fopen($file, 'r');

        // $i = 0;
        // while(!feof($handle)) {
        //     $model = fgetcsv($handle);
        //     if(is_array($model)) {

        //         $lang = ShowLanguage::firstOrNew(['short_code' => strtolower(trim($model[1]))]);
        //         $lang->fill(['title' => trim($model[0])])->save();
        //     }
        // }
        // fclose($handle);

        // Create Elasticsearch Index
        // $lastRecord = DB::table('search_count')->where('type', 'Episode')->first();
        // $all_episodes = Episode::where('id', '>', $lastRecord->from_count)->take(20000)->orderBy('id', 'ASC')->get();
        // $created_at = Carbon::now();

        // foreach ($all_episodes as $item) {
        //     //try {
        //     $item->searchable();
        //     //Log::info('Els Episode ID : ' . $item->id);
        //     dump($item->id);
        //     // } catch (\Exception $e) {
        //     //     Log::info('Error Episode ID : ' . $item->id);
        //     //     //continue;
        //     // }
        //     $lastRecord = $item->id;
        // }

        // $items_array = [
        //     'from_count' => $lastRecord,
        //     'created_at' => $created_at,
        //     'updated_at' => Carbon::now()
        // ];

        // DB::table('search_count')->where('type', 'Episode')->update($items_array);

        /*$lastRecord = DB::table('search_count')->where('type', 'ActBucket')->first();

        $created_at = Carbon::now();

        $backdate = Carbon::now()->subMonths(config('config.active_show_interval'));

        $query = Show::has('episodes')->where('id', '>', $lastRecord->from_count);

        $all_shows = $query->take(1000)->orderBy('id', 'ASC')
            ->get(['id', 'itunes_id', 'feed_url', 'content_length']);

        foreach ($all_shows as $show) {

            $all_episodes = $show->episodes()->whereNotNull('date_created')
                ->where('date_created', '>=', $backdate->format('Y-m-d'))
                ->count();

            if($all_episodes > 0) {
                $responseActive = ActiveShow::firstOrNew(['show_id' => $show->id]);
                if (!$responseActive->exists) {
                    $responseActive->fill([
                        'show_id' => $show->id,
                        'itunes_id' => $show->itunes_id,
                        'feed_url' => $show->feed_url,
                        'cron_run_at' => Carbon::now()->format('Y-m-d'),
                        'content_length' => $show->content_length
                    ])->save();
                }
            }

            $lastRecord = $show->id;
        }

        $items_array = [
            'from_count' => $lastRecord,
            'created_at' => $created_at,
            'updated_at' => Carbon::now()
        ];

        DB::table('search_count')->where('type', 'ActBucket')->update($items_array);*/

        // $lastRecord = DB::table('search_count')->where('type', 'ActBucket')->first();
        // $created_at = Carbon::now();

        // $show_active = ActiveShow::where('id', '>', $lastRecord->from_count)
        //     ->take(1000)->orderBy('id', 'ASC')->get();

        // foreach ($show_active as $item) {
        //     try {
        //         $showData = Show::find($item->show_id, ['id', 'itunes_id', 'feed_url', 'content_length']);

        //         $all_episodes = $showData->episodes()->whereNotNull('date_created')
        //             ->orderBy('date_added', 'DESC')->take(6)
        //             ->get(['id', 'title', 'show_id', 'date_created', 'date_added']);

        //         //dump($all_episodes->toArray());

        //         $diff = 0;
        //         $total_episode = $all_episodes->count() - 1;

        //         if ($all_episodes->count() == 2) {
        //             $date1 = Carbon::parse($all_episodes[0]['date_added']);
        //             $date2 = Carbon::parse($all_episodes[1]['date_added']);

        //             $diff = $date1->diffInHours($date2);
        //         } elseif ($all_episodes->count() == 3) {
        //             $date1 = Carbon::parse($all_episodes[0]['date_added']);
        //             $date2 = Carbon::parse($all_episodes[1]['date_added']);
        //             $date3 = Carbon::parse($all_episodes[2]['date_added']);

        //             $diff1 = $date1->diffInHours($date2);
        //             $diff2 = $date2->diffInHours($date3);
        //             $diff = $diff1 + $diff2;
        //         } elseif ($all_episodes->count() == 4) {
        //             $date1 = Carbon::parse($all_episodes[0]['date_added']);
        //             $date2 = Carbon::parse($all_episodes[1]['date_added']);
        //             $date3 = Carbon::parse($all_episodes[2]['date_added']);
        //             $date4 = Carbon::parse($all_episodes[3]['date_added']);

        //             $diff1 = $date1->diffInHours($date2);
        //             $diff2 = $date2->diffInHours($date3);
        //             $diff3 = $date3->diffInHours($date4);
        //             $diff = $diff1 + $diff2 + $diff3;
        //         } elseif ($all_episodes->count() == 5) {
        //             $date1 = Carbon::parse($all_episodes[0]['date_added']);
        //             $date2 = Carbon::parse($all_episodes[1]['date_added']);
        //             $date3 = Carbon::parse($all_episodes[2]['date_added']);
        //             $date4 = Carbon::parse($all_episodes[3]['date_added']);
        //             $date5 = Carbon::parse($all_episodes[4]['date_added']);

        //             $diff1 = $date1->diffInHours($date2);
        //             $diff2 = $date2->diffInHours($date3);
        //             $diff3 = $date3->diffInHours($date4);
        //             $diff4 = $date4->diffInHours($date5);
        //             $diff = $diff1 + $diff2 + $diff3 + $diff4;
        //         } elseif ($all_episodes->count() == 6) {
        //             $date1 = Carbon::parse($all_episodes[0]['date_added']);
        //             $date2 = Carbon::parse($all_episodes[1]['date_added']);
        //             $date3 = Carbon::parse($all_episodes[2]['date_added']);
        //             $date4 = Carbon::parse($all_episodes[3]['date_added']);
        //             $date5 = Carbon::parse($all_episodes[4]['date_added']);
        //             $date6 = Carbon::parse($all_episodes[5]['date_added']);

        //             $diff1 = $date1->diffInHours($date2);
        //             // echo $diff1 . ' Hour';
        //             // echo "\n";

        //             $diff2 = $date2->diffInHours($date3);
        //             // echo $diff2 . ' Hour';
        //             // echo "\n";

        //             $diff3 = $date3->diffInHours($date4);
        //             // echo $diff3 . ' Hour';
        //             // echo "\n";

        //             $diff4 = $date4->diffInHours($date5);
        //             // echo $diff4 . ' Hour';
        //             // echo "\n";

        //             $diff5 = $date5->diffInHours($date6);
        //             // echo $diff5 . ' Hour';
        //             // echo "\n";

        //             $diff = $diff1 + $diff2 + $diff3 + $diff4 + $diff5;
        //         }

        //         //dump($diff);
        //         //dump("$diff/$total_episode");
        //         //dump($diff / $total_episode);

        //         if ($all_episodes->count() > 1) {

        //             $final_def = ($diff/$total_episode);

        //             $arrayData = [
        //                 'show_id' => $showData->id,
        //                 'itunes_id' => $showData->itunes_id,
        //                 'feed_url' => $showData->feed_url,
        //                 'content_length' => $showData->content_length,
        //                 'date_between' => $final_def,
        //                 'cron_run_at' => Carbon::now()->format('Y-m-d')
        //             ];

        //             //dd($final_def);
        //             $bucket_type = 0;
        //             if ($final_def <= 6) { // Added 6 hrs bucket
        //                 $responseActive = ShowActiveHourly::firstOrNew(['show_id' => $showData->id]);
        //                 $responseActive->fill($arrayData)->save();
        //                 $bucket_type = 1;
        //             } elseif ($final_def > 6 && $final_def <= 168) { // Added 12 hrs bucket
        //                 $responseData = ShowActiveDaily::firstOrNew(['show_id' => $showData->id]);
        //                 $responseData->fill($arrayData)->save();
        //                 $bucket_type = 2;
        //             }

        //             //if ($bucket_type > 0) { //Update Bucket Type
        //                 $item->fill(['which_bucket' => $bucket_type, 'date_between' => $final_def])->save();
        //             //}
        //         }
        //     } catch (\Exception $ex) {
        //         Log::info('Show - ' . $item->show_id);
        //         continue;
        //     }

        //     $lastRecord = $item->id;
        // }

        // $items_array = [
        //     'from_count' => $lastRecord,
        //     'created_at' => $created_at,
        //     'updated_at' => Carbon::now()
        // ];

        // DB::table('search_count')->where('type', 'ActBucket')->update($items_array);

        /*
        $shows = Show::where('id', '>', $lastRecord->from_count)
            ->take(10)->orderBy('id', 'ASC')->get(['id', 'itunes_id']);
        foreach ($shows as $item) {
            try {
                $showRes = ShowResponse::where('id', $item->itunes_id)->first();
                $data = json_decode($showRes['data'], 1);

                $categories = [];
                if (count($data['genres']) > 0) {
                    foreach ($data['genres'] as $apiCategory) {
                        $category = Category::firstOrNew(['title' => trim($apiCategory)]);
                        $category->fill(['parent_id' => 0, 'title' => trim($apiCategory)])->save();
                        $categories = array_merge($categories, [$category->id]);
                    }
                }
            } catch (Exception $ex) {
                Log::info('Category Error : Show Id : ' . $item->id);
                continue;
            }

            //dump($categories);

            $item->categories()->sync(array_filter($categories));

            $lastRecord = $item->id;

            dump($item->id);
        }*/
    }
}
