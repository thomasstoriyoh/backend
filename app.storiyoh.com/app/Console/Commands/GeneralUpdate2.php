<?php

namespace App\Console\Commands;

use App\Models\Show;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class GeneralUpdate2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'general:queue2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //\Log::info('Elastic Search Script Start');

        $lastRecord = DB::table('search_count')->where('type', 'Show')->first();
        $created_at = Carbon::now();
        //dd($lastRecord);

        $all_data = Show::where('status', 'Published')->where('id', '>', $lastRecord->from_count)->where('content_type', 0)->take(5000)->get();
        //dd($all_data->toArray());

        foreach ($all_data as $item) {
            //dump("Id=".$item->id);
            try {
                $item->searchable();
                $lastRecord = $item->id;
            } catch(\Exception $ex) {
                \Log::info('Error ==='. $item->id);
                $lastRecord = $item->id;
                continue;
            }
        }

        $items_array = [
            'from_count' => $lastRecord,
            'created_at' => $created_at,
            'updated_at' => Carbon::now()
        ];

        DB::table('search_count')->where('type', 'Show')->update($items_array);

        //\Log::info('Elastic Search Script End');
    }
}
