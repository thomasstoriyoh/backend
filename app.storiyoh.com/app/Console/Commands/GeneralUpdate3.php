<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\ShowActiveDailyOnce;

class GeneralUpdate3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'general:queue3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Script3 Start');
        
        $lastRecord = DB::table('search_count')->where('type', 'Hub3')->first();        
        $created_at = Carbon::now();

        $all_data = ShowActiveDailyOnce::where('id', '>', $lastRecord->from_count)->take(200)->get();
                
        foreach ($all_data as $item) {
            try {
                $data = file_get_contents($item->feed_url);
                if (strpos($data, 'rel="hub"') !== false) {
                    $array_data = [
                        'has_hub' => 1,
                        //"hub_url" => @implode(",", $data['hub'])
                    ];
                    $item->fill($array_data)->save();
                }
                $lastRecord = $item->id;
            } catch(\Exception $ex) {
                Log::info('Error ==='. $item->id);
                $array_data = [
                    'has_hub' => 2
                ];
                $item->fill($array_data)->save();
                $lastRecord = $item->id;
                continue;
            }
        }

        $items_array = [
            'from_count' => $lastRecord,
            'created_at' => $created_at,
            'updated_at' => Carbon::now()
        ];

        DB::table('search_count')->where('type', 'Hub3')->update($items_array);

        Log::info('Script3 End');
    }
}
