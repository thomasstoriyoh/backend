<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use App\Models\ItunesTopPodcast as TopPodcast;
use App\Models\ShowItuneId;

class ItunesTopPodcast extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'itunes:toppodcast';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetched top podcast from iTunes and insert into our Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Update all processed to 0
        TopPodcast::where('status', 'Published')->update(['processed' => false]);

        $top_podcast = TopPodcast::where('status', 'Published')->get();        
        //dd($top_podcast->toArray());
        foreach($top_podcast as $item) {
            //dump("Title ===". $item->title);
            //dump("Id ===". $item->id);
            //Log::info($item->title . ' rss feed start');
            try{
                $data = file_get_contents($item->rss_url);            
                $data_Array = json_decode($data);
                if (!empty($data_Array->feed->results)) {
                    foreach ($data_Array->feed->results as $itemData) {
                        ShowItuneId::firstOrCreate(['itune_id' => $itemData->id]);                    
                    }
                }
                
                //Update processed status
                $item->update(["processed" => true]);
            } catch(\Exception $ex) {
                //Log::info("Error :: ". $ex->getMessage());
                continue;
            }
            //Log::info($item->title . ' rss feed end');           
        }                
    }
}
