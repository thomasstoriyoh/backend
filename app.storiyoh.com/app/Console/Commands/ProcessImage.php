<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Models\Show;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProcessImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will be create new show image.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastRecord = DB::table('search_count')->where('type', 'Image')->first();

        $all_shows = Show::where('id', '>', $lastRecord->from_count)->take(300)->orderBy('id', 'ASC')->get();

        //dd($all_shows);

        $created_at = Carbon::now();

        foreach ($all_shows as $item) {
            if (!empty($item->itunes_id) && !empty($item->full_image)) {
                try {
                    // open an image file
                    // $img = Image::make($item->full_image)->encode('jpg');
                    // $file = public_path('uploads/show_assets/' . $item->itunes_id . '.jpg');
                    // finally we save the image as a new file
                    // $img->save($file);

                    $img = Image::make($item->full_image)->encode('jpg');
                    $image_name = $item->itunes_id.".jpg";
                    $t = Storage::disk('s3')->put("shows/".$image_name, file_get_contents($img));

                    $item->fill(['image' => $item->itunes_id . '.jpg'])->save();
                } catch (\Exception $e) {
                    Log::info('Image Error : ' . $item->itunes_id);
                    continue;
                }
            }
            $lastRecord = $item->id;
            //dump($lastRecord);
            //Log::info($lastRecord);
        }

        $items_array = [
            'from_count' => $lastRecord,
            'created_at' => $created_at,
            'updated_at' => Carbon::now()
        ];

        DB::table('search_count')->where('type', 'Image')->update($items_array);
    }
}
