<?php

namespace App\Console\Commands;

use App\Models\Episode;
use App\Models\EpisodeResponse;
use App\Models\EpisodeTag;
use App\Models\Show;
use App\Classes\Feed;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Log;

class ProcessItunesEpisodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:itune_episodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process the fresh episodes that has been cached in the temporary show response database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Processing Episodes...');

            $items = EpisodeResponse::take(50)->get();

            //$bar = $this->output->createProgressBar(count($items));
            $showIds = [];
            foreach ($items as $item) {
                $data = json_decode($item->data);

                try {
                    //Fetched Show Data from table
                    $showData = Show::where('id', $item->show_id)->first();

                    if ($showData) {
                        $uniqueID = md5($data->title . $item->show_id . $data->size . $data->duration);

                        $episode = Episode::firstOrNew(['uuid' => $uniqueID]);

                        $newOrOld = $episode->exists;

                        $attributes = $this->getEpisodeAttributes($data, $showData);

                        $episode->fill($attributes)->save();

                        $tags = $this->getTags($data);
                        $episode->tags()->sync($tags);

                        $categories = $showData->categories()->pluck('id')->all();
                        $episode->categories()->sync($categories);

                        if (!empty($attributes['show_id']) && !$newOrOld) {
                            $showIds[$attributes['show_id']][] = trim($episode->id);
                        }
                    }

                    //Delete from tbl_episode_response
                    //$itemDelete = EpisodeResponse::findOrFail($item->id);
                    $episode->searchable();
                    $item->delete();
                } catch (\Exception $e) {
                    $this->error($e->getMessage());
                    Log::info($e->getMessage());
                    Log::info('Process Itunes Episodes Itunes Id===' . $item->show_id);
                    $episode->unsearchable();
                    continue;
                }

                //$bar->advance();
            }

            $this->makeActivity(array_filter($showIds));

            //$bar->finish();
            $this->info("\nEpisode Processing Finished...");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            Log::info($e->getMessage());
            Log::info('Process Itunes Episodes2');
        }
    }

    /**
     * @param array $showIds
     * @return void
    */
    protected function makeActivity($showIds)
    {
        $shows = Show::whereIn('id', array_keys($showIds))->get(['id', 'updated_at']);
        foreach ($shows as $show) {
            $show->activities()->create([
                'type' => Feed::SHOW_UPDATE,
                'data' => json_encode($showIds[$show->id])
            ]);
            $show->touch();
        }
    }

    /**
     * @param $data
     * @return array
    */
    protected function getEpisodeAttributes($data, $show)
    {
        $date_created = null;
        if (!empty(@$data->published_at)) {
            $date_created = Carbon::parse(@$data->published_at)->format('Y-m-d');
            if ($date_created == '1970-01-01') {
                $date_created = null;
            }
        }

        $date_added = null;
        if (!empty(@$data->published_at)) {
            $date_added = Carbon::parse(@$data->published_at)->format('Y-m-d H:i:s');
            if ($date_added == '1970-01-01 05:30:00') {
                $date_added = null;
            }
        }

        $duration = 0;
        if (@$data->duration) {
            if ($data->duration > 0) {
                $duration = $data->duration;
            }
        }

        $size = 0;
        if (@$data->size) {
            if ($data->size > 0) {
                $size = $data->size;
            }
        }

        $attributes = [
            'show_id' => @$show->id,
            'itunes_id' => @$show->itunes_id,
            'title' => empty($data->title) ? '-' : $data->title,
            'description' => @$data->description,
            'image' => @$data->image,
            'duration' => $duration,
            'size' => $size,
            'mp3' => @$data->mp3,
            'link' => @$data->link,
            'date_created' => $date_created,
            'date_added' => $date_added,
            'created_by' => 1,
            'updated_by' => 1
        ];

        return $attributes;
    }

    /**
     * @param $data
     * @return array
    */
    protected function getTags($data)
    {
        $tags = [];
        $keywordsData = array_filter($data->keywords);
        if (count($keywordsData)) {
            foreach ($keywordsData as $tag) {
                if (strlen($tag) <= 191) {
                    $tag = EpisodeTag::firstOrCreate(['title' => $tag]);
                    $tags[] = $tag->id;
                } else {
                    continue;
                }
            }
        }

        return $tags;
    }
}
