<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Show;
use App\Models\ShowResponse;
use Tzsk\ScrapePod\Facade\ScrapePod;
use App\Models\EpisodeResponse;
use App\Models\ShowLanguage;
use App\Models\Episode;
use App\Models\EpisodeTag;
use Carbon\Carbon;
use App\Models\ActiveShow;
use Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ProcessShowItunes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:ituneshows';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Episodes into our Temporary Database from Itunes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shows = ShowResponse::where('processed', 0)->take(100)->get();
        //dump($shows->toArray());
        //Log::info('Start');
        foreach ($shows as $item) {
            try {
                $showData = Show::where('itunes_id', $item->id)->first();

                //echo $showData->id;
                //exit;
                
                //Found Feed URl Length Code Start
                //$process = new Process('curl -s '.$showData->feed_url.' | wc -c');
                //$process->run();
                
                // executes after the command finishes
                // if (!$process->isSuccessful()) {
                //     throw new ProcessFailedException($process);
                // }

                $length = 0;
                //$length = trim($process->getOutput());
                //dump($length);
                // if($length) {
                //     if(strlen($length) > 50) {
                //         $length = 0;
                //     }
                // }
                //dd($length);
                //Found Feed URl Length Code End

                $processed = 2;

                if ($showData) {
                    //Fetch Data from Feed URL
                    $response = ScrapePod::feed($showData->feed_url);
                    //dump($response['data']);

                    $bar = $this->output->createProgressBar(count($response['data']['episodes']));

                    $this->info('Fetching Episodes...');
                    $no_of_shows = 0;
                    if (count($response['data']['episodes']) > 0) {
                        if (count($response['data']['episodes']) > $showData->no_of_episode) {
                            //Added all episodes to tbl_episodes table
                            $showIds = [];
                            foreach ($response['data']['episodes'] as $data) {
                                //dd($data);
                                try {                                    
                                    //If same MP3 found than we will not added this episode
                                    //and continue to next episode
                                    $checkEpisodeMp3Data = Episode::where('show_id', $showData->id)->where('mp3', $data['mp3'])->count();
                                    if($checkEpisodeMp3Data) {
                                        continue;
                                    }

                                    $uniqueID = md5($data['title'] . $showData->id . $data['size'] . $data['duration'] . $data['published_at']);

                                    $checkEpisodeData = Episode::where('uuid', $uniqueID)->count();

                                    if (!$checkEpisodeData) {
                                        $episode = Episode::firstOrNew(['uuid' => $uniqueID]);

                                        $newOrOld = $episode->exists;

                                        $attributes = $this->getEpisodeAttributes($data, $showData);
                                        $episode->fill($attributes)->save();

                                        $tags = $this->getTags($data);
                                        $episode->tags()->sync($tags);

                                        $categories = $showData->categories()->pluck('id')->all();
                                        $categories = array_filter($categories);
                                        $episode->categories()->sync($categories);

                                        if (!empty($attributes['show_id']) && !$newOrOld) {
                                            $showIds[$attributes['show_id']][] = trim($episode->id);
                                        }

                                        //EpisodeResponse::create(['show_id' => $showData->id, 'data' => json_encode($data)]);
                                        $bar->advance();

                                        $episode->searchable();
                                    }

                                    $no_of_shows++;
                                                                        
                                } catch (\Exception $e) {
                                    $this->error($e->getMessage());
                                    Log::info($e->getMessage());
                                    Log::info('Process Show Itunes');
                                    //$episode->unsearchable();
                                    continue;
                                }
                            }
                            
                            //Check if any episode added in last 12 month
                            try {
                                $backdate = Carbon::now()->subMonths(config('config.active_show_interval'));
                                //dump($backdate);
                                $episode_count = $showData->episodes()->where('date_created', '>=', $backdate->format('Y-m-d'))->count();
                                if ($episode_count > 1 && !empty($showData->feed_url)) {
                                    $responseActive = ActiveShow::firstOrNew(['show_id' => $showData->id]);
                                    $responseActive->fill([
                                        'show_id' => $showData->id,
                                        'itunes_id' => $showData->itunes_id,
                                        'feed_url' => $showData->feed_url,
                                        'content_length' => @$length ? $length : 0,
                                        'cron_run_at' => NULL
                                    ])->save();
                                }
                            } catch (\Exception $e) {
                                Log::info('Error : Active Bucket - ' . $showData->id);
                            }
                        }
                    }
                    if (!empty($showIds)) {
                        $showData->fill([
                            'language' => strtolower(trim(@$response['data']['language'])),
                            'description' => @$response['data']['description'] ? @$response['data']['description'] : @$response['data']['summary'],
                            'web_url' => @$response['data']['site'],
                            'no_of_episode' => $no_of_shows, //@count($response['data']['episodes']),
                            //'content_length' => @$length ? $length : 0,
                            'status' => 'Published'
                        ])->save();

                        try {
                            $showData->fill([
                            'content_length' => @$length ? $length : 0
                        ])->save();
                        } catch (\Exception $ex) {
                            Log::info('Length Error - ' . $showData->id);
                        }

                        $showData->searchable();
                    }

                    //dd("no of shows==".$no_of_shows);
                                                            
                    // $lang = ShowLanguage::firstOrNew(['short_code' => strtolower(trim(@$response['data']['language']))]);
                    // $lang->fill([
                    //     'short_code' => strtolower(trim(@$response['data']['language'])),
                    //     'created_by' => 1,
                    //     'updated_by' => 1
                    // ])->save();
                    
                    $processed = 1;

                    $bar->finish();
                    //$this->makeActivity(array_filter($showIds));
                }

                $item->update(['processed' => $processed, 'processed_at' => @date('Y-m-d H:i:s')]);

                $this->info("\nEpisodes Fetch Finished...");
            } catch (\Exception $e) {
                $this->error($e->getMessage());
                Log::info($e->getMessage());
                //Log::info('Process Show Itunes Id === ' . $item->id);

                //$showData->fill(['status' => 'Draft'])->save();
                //$showData->unsearchable();

                $item->update(['processed' => 2, 'processed_at' => @date('Y-m-d H:i:s')]);
                continue;
            }
        }
        //Log::info('End');
    }

    /**
     * @param array $showIds
     * @return void
    */
    protected function makeActivity($showIds)
    {
        $shows = Show::whereIn('id', array_keys($showIds))->get(['id', 'updated_at']);
        foreach ($shows as $show) {
            $show->activities()->create([
                'type' => Feed::SHOW_UPDATE,
                'data' => json_encode($showIds[$show->id])
            ]);
            $show->touch();
        }
    }

    /**
     * @param $data
     * @return array
    */
    protected function getEpisodeAttributes($data, $show)
    {
        $date_created = null;
        if (!empty(@$data['published_at'])) {
            $date_created = Carbon::parse(@$data['published_at'])->format('Y-m-d');
            if ($date_created == '1970-01-01') {
                $date_created = null;
            }
        }

        $date_added = null;
        if (!empty(@$data['published_at'])) {
            $date_added = Carbon::parse(@$data['published_at'])->format('Y-m-d H:i:s');
            if ($date_added == '1970-01-01 05:30:00') {
                $date_added = null;
            }
        }

        $duration = 0;
        if (@$data['duration']) {
            if ($data['duration'] > 0) {
                $duration = $data['duration'];
            }
        }

        $size = 0;
        if (@$data['size']) {
            if ($data['size'] > 0) {
                $size = $data['size'];
            }
        }

        $attributes = [
            'show_id' => @$show->id,
            'itunes_id' => @$show->itunes_id,
            'title' => empty($data['title']) ? '-' : $data['title'],
            'description' => @$data['description'],
            'image' => @$data['image'],
            'duration' => $duration,
            'size' => $size,
            'mp3' => @$data['mp3'],
            'link' => @$data['link'],
            'date_created' => $date_created,
            'date_added' => $date_added,
            'created_by' => 1,
            'updated_by' => 1
        ];

        return $attributes;
    }

    /**
     * @param $data
     * @return array
    */
    protected function getTags($data)
    {
        $tags = [];
        $keywordsData = array_filter($data['keywords']);
        if (count($keywordsData)) {
            foreach ($keywordsData as $tag) {
                if (strlen($tag) <= 191) {
                    $tag = EpisodeTag::firstOrCreate(['title' => $tag]);
                    $tags[] = $tag->id;
                } else {
                    continue;
                }
            }
        }

        return $tags;
    }
}
