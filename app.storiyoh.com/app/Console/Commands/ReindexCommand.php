<?php

namespace App\Console\Commands;

use App\Models\Episode;
use Carbon\Carbon;
use Elasticsearch\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Log;
class ReindexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexes all articles to elasticsearch';

    protected $search;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $search)
        {
            parent::__construct();
            $this->search = $search;
        }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     
    public function handle()
    {
        //$this->info('Indexing all episodes. Might take a while...');

        $elasticsearch = DB::table('search_count')->first();
        
        $items = Episode::where('id','>',$elasticsearch->from_count)->orderBy('id')->take(10000)->get();

        $from_count = $elasticsearch->from_count;
        
        $created_at = Carbon::now(); 

        if(sizeof($items))
        {
            foreach ($items as $item)
            {
                $this->search->index([
                    'index' => $item->getSearchIndex(),
                    'type' => $item->getSearchType(),
                    'id' => $item->id,
                    'body' => $item->toSearchArray(),
                ]);

                // PHPUnit-style feedback
                /*$this->output->write('.');
                $this->output->write($item->id);*/
                $from_count = $item->id;
            }
        }

        $updated_at = Carbon::now(); 

        $items_array = ["from_count" => $from_count,'created_at' => $created_at,'updated_at'=> $updated_at];


        DB::table('search_count')->where('id',1)->update($items_array);
        //$this->info("\nDone!");
        Log::info('Start: '.$elasticsearch->from_count);
        Log::info('End: '.$from_count);

        Log::info('Done');
    }
   

}
