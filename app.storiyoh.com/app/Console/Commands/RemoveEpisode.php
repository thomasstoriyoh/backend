<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ShowNotification;
use App\Models\Show;
use App\Models\Episode;
use App\Models\Feed;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class RemoveEpisode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:duplicate_episode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Show Id
     */
    protected $itemId = 115613;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {        
        try {
            $show_active = Show::where('id', $this->itemId)->orderBy('id', 'ASC')->get(['id']);
            Log::info("Remove Episode");

            $showArray = [];
            $episodeIdsArray = [];

            $lastRecord = DB::table('search_count')->where('type', 'Remove')->first();        
            $created_at = Carbon::now();

            foreach ($show_active as $item) {
                if ($item->episodes()->count() > 0) {
                    $showArray[$item->id] = [];
                    $allPodcastEpisodeData = $item->episodes()->where('id', '<', $lastRecord->from_count)
                        ->where('status', 'Published')->whereNotNull('date_added')
                        ->orderBy('id', 'DESC')->take(2000)->get();
                    foreach ($allPodcastEpisodeData as $episode_item) {
                        //dump($episode_item->id);
                        if (! is_null($episode_item->date_added)) {
                            if (array_key_exists(strtotime(Carbon::parse($episode_item->date_added)->format("Y-m-d H:i")), $showArray[$item->id])) {
                                $episodeIdsArray[] = $episode_item->id;
                            } else {
                                $date_added = strtotime(Carbon::parse($episode_item->date_added)->format("Y-m-d H:i"));
                                $showArray[$item->id][$date_added] = $episode_item->id;
                            }
                        }

                        $items_array = [
                            'from_count' => $episode_item->id,
                            'created_at' => $created_at,
                            'updated_at' => Carbon::now()
                        ];
                    
                        DB::table('search_count')->where('type', 'Remove')->update($items_array);
                    }
                }
            }
            
            //delete or draft episodes and also remove from elasticsearch
            foreach ($episodeIdsArray as $episodeDraftData) {
                
                $epiData = Episode::find($episodeDraftData);
                $epiData->fill(['status' => 'Duplicate'])->save();
                $epiData->unsearchable();

                //also remove from show_notification table
                $showNotificationData = ShowNotification::where('typeable_id', $this->itemId)
                    ->where('data', 'LIKE', '%"' . $episodeDraftData . '"%');
                if ($showNotificationData->count()) {
                    $feedData = $showNotificationData->get();
                    foreach ($feedData as $item) {
                        $dataArray = json_decode($item->data);
                        $arr = array_diff($dataArray, [$episodeDraftData]);
                        if (count($arr) == 0) {
                            $item->delete();
                        } else {
                            $item->timestamps = false;
                            $item->fill(['data' => json_encode(array_flatten($arr))])->save();
                        }
                    }
                }

                //Remove episode id for listen from feed
                $feedNotificationData = Feed::where('type', "Episode Heard")
                    ->where('data', 'LIKE', '%"' . $episodeDraftData . '"%');
                if ($feedNotificationData->count()) {
                    $feedListenData = $feedNotificationData->get();
                    foreach ($feedListenData as $itemListenData) {
                        $itemListenData->delete();
                    }
                }
            }
        } catch(\Exception $ex) {
            \Log::info($ex->getMessage());
        }
    }
}
