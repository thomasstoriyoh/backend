<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use App\Traits\Helper;
use Log;

use App\Models\Show;
use App\Models\Chart;
use App\Models\Episode;
use App\Models\Subscriber;
use App\Models\User;
use App\Models\CronNewsletter;
use App\Models\TestSubscriber;
use App\Models\Newsletter;

class SendNewsletterCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Newsletter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cronnewsletter = CronNewsletter::where('status', 0)->first();

        if($cronnewsletter)
        {
            $cronnewsletter->update(['status' => 1]);

            $newsletter = Newsletter::where('id',$cronnewsletter->newsletter_id)->first();

            $trending_podcasts = Show::whereIn('id',$newsletter->trending_podcasts)->get();
            $featured_collections = Chart::whereIn('id',$newsletter->featured_collections)->with('chart_poadcast')->get();
            $featured_episodes = Episode::whereIn('id',$newsletter->featured_episodes)->get();   
            $essentials = Show::whereIn('id',$newsletter->essentials)->get();
            $podcast_of_the_week = Show::where('id',$newsletter->podcast_of_the_week)->first(); 

            $subscribers = [];
            if($cronnewsletter->user_type == 'subscriber') {
                $users = Subscriber::where('subscription_status',1)->get();
            } elseif ($cronnewsletter->user_type == 'user') {
                $users = User::where('verified','Verified')->where('admin_status','Approved')->where('subscription_status', 1)->get();
            } elseif ($cronnewsletter->user_type == 'test') {
                $users = TestSubscriber::where('subscription_status',1)->get();
            } elseif ($cronnewsletter->user_type == 'both') {
                $users = User::where('verified','Verified')->where('admin_status','Approved')->where('subscription_status', 1)->get();
                $subscribers = Subscriber::where('subscription_status',1)->get();
            }

            $filename = null;
            
            if($subscribers)
            {                
                //dd($subscribers->toArray());
                //dump("Subscriber");
                foreach ($subscribers as $key => $user) {
                    $mailData =   [
                        'file_path' => 'emails.newsletter',
                        'from_email' => $cronnewsletter->from_email,
                        'from_name' => $cronnewsletter->from_name,
                        'to_email' => $user['email'],
                        'to_name' => $user['email'],
                        'subject' => $cronnewsletter->subject,
                        'filename' => $filename,
                        'data' => ['newsletter' => $newsletter, 
                        'user' => $user, 
                        'trending_podcasts' => $trending_podcasts, 
                        'featured_collections' => $featured_collections, 
                        'featured_episodes' => $featured_episodes, 
                        'essentials' => $essentials, 
                        'podcast_of_the_week' => $podcast_of_the_week,
                        'newsletter_subject' => $cronnewsletter->subject,
                        ],
                    ];

                    Helper::sendAllEmail($mailData);
                }
            }

            //dd($users->toArray());
            foreach ($users as $key => $user) {
                //dump($user->id);                
                $mailData =   [
                    'file_path' => 'emails.newsletter',
                    'from_email' => $cronnewsletter->from_email,
                    'from_name' => $cronnewsletter->from_name,
                    'to_email' => $user['email'],
                    'to_name' => $user['email'],
                    'subject' => $cronnewsletter->subject,
                    'filename' => $filename,
                    'data' => ['newsletter' => $newsletter, 
                    'user' => $user, 
                    'trending_podcasts' => $trending_podcasts, 
                    'featured_collections' => $featured_collections, 
                    'featured_episodes' => $featured_episodes, 
                    'essentials' => $essentials, 
                    'podcast_of_the_week' => $podcast_of_the_week,
                    'newsletter_subject' => $cronnewsletter->subject,
                    ],
                ];
                Helper::sendAllEmail($mailData);
                //dump("Mail Sent!");
            }    
        }
    }
}


        