<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ShowPushNotification as PushNotification;
use Log;

use App\Jobs\FirebaseTopicPushNotification;

class ShowPushNotificationTesting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'show:push_notification_testing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification to subscribe show';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Push Notification Sent');

        $shows = PushNotification::where('id', 1276)->get();

        foreach ($shows as $item) {
            //remove from table
            //$item->delete();
            //$item->fill(['status' => 1])->save();

            $userIds = json_decode($item->userIds);
            $data = json_decode($item->data);

            dump($item->show_id, $userIds, $data);
                        
            //dispatch(new FirebaseTopicPushNotification($item->show_id, $userIds, $data))->onQueue('push_notification');
        }
    }
}
