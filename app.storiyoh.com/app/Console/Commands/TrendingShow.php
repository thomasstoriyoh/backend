<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TrendingShow as TrendingShowModel;
use App\Models\Show;
class TrendingShow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trending:show';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch latest shows from india rss url';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $data = file_get_contents('https://rss.itunes.apple.com/api/v1/in/podcasts/top-podcasts/all/50/explicit.json');            
            $data_Array = json_decode($data);
            $shows = array();
            if (!empty($data_Array->feed->results)) {
                TrendingShowModel::truncate();
                foreach ($data_Array->feed->results as $key => $itemData) {
                    $exists = Show::where('itunes_id' , $itemData->id)->published()->exists(); // check show exists
                   
                    if($exists) {
                       $up = TrendingShowModel::firstOrCreate(['id' => $key+1]);
                       $up->itunes_id = $itemData->id;
                       $up->save();
                    }                    
                }
            }          
            //\Log::info("Trending show updated");
        } catch(\Exception $ex) {
            //\Log::info("Error :: ". $ex->getMessage());
        }
    }
}
