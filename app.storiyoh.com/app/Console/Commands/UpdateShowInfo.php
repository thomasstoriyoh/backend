<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ActiveShow;
use App\Jobs\UpdateShowInfo as UpdateShowInfoJob;

class UpdateShowInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'show:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $show_active = ActiveShow::where('id', '>', 0)->take(1)->get();
        foreach ($show_active as $item) {
            dump('Id===' . $item->id);
            dump('Show Id===' . $item->show_id);
            dispatch(new UpdateShowInfoJob($item->show_id, $item->itunes_id, $item->feed_url));
        }
    }
}
