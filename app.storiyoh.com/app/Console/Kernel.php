<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /****************************************************************/
        $schedule->command('itunes:ids')->everyFiveMinutes()->onOneServer();
        $schedule->command('process:ituneshows')->everyTenMinutes()->onOneServer();
        $schedule->command('show:push_notification')->everyMinute()->onOneServer();
        $schedule->command('send:newsletter')->everyFiveMinutes()->onOneServer();
        $schedule->command('itunes:toppodcast')->twiceDaily(0, 12)->onOneServer();
        $schedule->command('horizon:snapshot')->everyFiveMinutes()->onOneServer();
        $schedule->command('trending:show')->twiceDaily(3, 15)->onOneServer();
        //$schedule->command('general:queue2')->everyFiveMinutes()->onOneServer();
        /*****************************************************************/

        //$schedule->command('remove:duplicate_episode')->everyFiveMinutes()->onOneServer();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
