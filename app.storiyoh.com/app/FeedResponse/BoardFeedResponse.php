<?php
namespace App\FeedResponse;

use App\Models\Episode;
use App\Traits\Helper;

/**
 * Description of BoardFeedResponse
 *
 * @author bcm
 */
class BoardFeedResponse extends FeedResponseable {
    
    public function response() {        
        $episode = Episode::withCount('like', 'comments')->whereIn('id', json_decode($this->feed->data))->first(['id', 'title', 'show_id', 'description', 'mp3', 'duration']);
        if($episode) {
            $user_episode_like = $this->current_user->like()->pluck('episode_id')->all();
            $like = @in_array($episode->id, $user_episode_like) ? 'true' : 'false';
            return [
                'type' => $this->feed->type,
                'board_id' => $this->feed->typeable->id,
                'board_title' => $this->feed->typeable->title,
                "board_image" => !empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(200) : asset('uploads/default/board.png'),
                "no_follower" => $this->feed->typeable->followers()->count(),
                'display_text' => "has a new episode",
                'show_id' => @$episode->show->id,
                'show_name' => @$episode->show->title,
                'episode_id' => $episode->id,
                'episode_name' => $episode->title,
                'episode_desc' => strip_tags($episode->description),
                'episode_audio' => $episode->getAudioLink(),                
                "episode_image" => !empty($episode->show->image) ? $episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                "duration" => $episode->getDurationText(),
                "episode_like" => Helper::shorten_count($episode->like_count),
                'episode_comment' => Helper::shorten_count($episode->comments_count),
                'like' => $like,
                'last_update' => $this->feed->updated_at->diffForHumans(),
            ];
        }
    }    
}
