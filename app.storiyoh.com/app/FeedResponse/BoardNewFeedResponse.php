<?php
namespace App\FeedResponse;

use App\Models\Board;

/**
 * Description of BoardNewFeedResponse
 *
 * @author bcm
 */
class BoardNewFeedResponse extends FeedResponseable 
{
    /**
     * 
     * @return type
     */
    public function response() {
        
        $board = Board::whereIn('id', json_decode($this->feed->data))->first(['id', 'title']);
        
        if($board) {
            return [
                'type' => $this->feed->type,
                'name' => @$this->feed->typeable->full_name,
                'username' => @$this->feed->typeable->username,
                "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                'display_text' => "has created a new board",
                'board_id' => $board->id,
                'board_name' => $board->title,
                'last_update' => $this->feed->updated_at->diffForHumans()
            ];
        }
    }    
}
