<?php

namespace App\FeedResponse;

use App\Models\Episode;
use App\Traits\Helper;

/**
 * Description of EpisodeHeardFeedResponse
 *
 * @author bcm
 */
class EpisodeHeardFeedResponse extends FeedResponseable 
{
    /**
     * 
     * @return type
     */
    public function response() {
     
        $episode = Episode::withCount('like', 'comments')->whereIn('id', json_decode($this->feed->data))->first(['id', 'title', 'show_id', 'description', 'mp3', 'duration']);
        if($episode) {
            $user_episode_like = $this->current_user->like()->pluck('episode_id')->all();
            $like = false;
            $like = @in_array($episode->id, $user_episode_like) ? 'true' : 'false'; 
            return [
                'type' => $this->feed->type,
                'name' => @$this->feed->typeable->full_name,
                'username' => @$this->feed->typeable->username,
                "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                'display_text' => "listened to",
                'show_id' => @$episode->show->id,
                'show_name' => trim(str_replace("\n", "", html_entity_decode(@$episode->show->title))),
                'episode_id' => $episode->id,
                'episode_name' => trim(str_replace("\n", "", html_entity_decode($episode->title))),
                'episode_desc' => strip_tags($episode->description),
                'episode_audio' => $episode->getAudioLink(),
                'episode_like' => Helper::shorten_count($episode->like_count),
                'episode_comment' => Helper::shorten_count($episode->comments_count),
                'episode_image' => !empty($episode->show->image) ? $episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                'duration' => $episode->getDurationText(),
                'last_update' => $this->feed->updated_at->diffForHumans(),
                'like' => $like,
            ];
        }
    }

}
