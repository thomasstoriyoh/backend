<?php
namespace App\FeedResponse;

use App\Models\Feed;
use App\Models\User;

abstract class FeedResponseable 
{
    protected $feed;

    protected $current_user;
    
    public function __construct(Feed $feed, User $current_user) 
    {
        $this->feed = $feed;
        $this->current_user = $current_user;
    }
    
    abstract public function response();
}
