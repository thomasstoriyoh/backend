<?php
namespace App\FeedResponse;

use App\Models\Show;

/**
 * Description of SubscribeFeedResponse
 *
 * @author bcm
 */
class ShowFeedResponse extends FeedResponseable 
{
    /**
     * 
     * @return array
     */
    public function response() 
    {
        $showInfo = Show::whereIn('id', json_decode($this->feed->data))->first(['id', 'title']);
        if($showInfo) {
            return [
                'type' => $this->feed->type,
                'name' => @$this->feed->typeable->full_name,
                'username' => @$this->feed->typeable->username,
                "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                'display_text' => "is subscribe new show",
                'show_id' => $showInfo->id,
                'show_name' => trim(str_replace("\n", "", html_entity_decode($showInfo->title))),
                'last_update' => $this->feed->updated_at->diffForHumans()
            ];
        }                        
    }       
}
