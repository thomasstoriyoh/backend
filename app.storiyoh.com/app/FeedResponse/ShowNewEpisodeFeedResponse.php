<?php
namespace App\FeedResponse;

use App\Models\Episode;
use App\Traits\Helper;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class ShowNewEpisodeFeedResponse extends FeedResponseable
{
    //put your code here
    //put your code here
    public function response() {
        
        $episodes = Episode::withCount('like', 'comments')->whereIn('id', json_decode($this->feed->data))->whereNotNull('show_id')->get(['id', 'title', 'show_id', 'mp3', 'duration']);
        $user_episode_like = $this->current_user->like()->pluck('episode_id')->all();
        if($episodes) {
            return [
                'type' => $this->feed->type,
                'show_id' => @$episodes->first()->show->id,
                'show_name' => trim(str_replace("\n", "", html_entity_decode(@$episodes->first()->show->title))),
                'show_image' => @$episodes->first()->show->image ? $episodes->first()->show->getWSImage(200) : asset('uploads/default/show.png'),
                'episode_data' => $this->episode_list($episodes, $user_episode_like)
            ];
        }        
    }

    protected function episode_list($episodes, $user_episode_like)
    {
        $data = []; 
        foreach($episodes as $episode)
        {            
            $like = false;
            $like = @in_array($episode->id, $user_episode_like) ? 'true' : 'false';
            $data[] = [
                'episode_id' => $episode->id,
                'episode_name' => html_entity_decode($episode->title),
                'episode_like' => Helper::shorten_count($episode->like_count),
                'episode_comment' => Helper::shorten_count($episode->comments_count),
                'episode_image' => !empty($episode->show->image) ? $episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                'episode_audio' => $episode->getAudioLink(),
                'duration' => $episode->getDurationText(),
                'display_text' => "has a new episode",                
                'last_update' => $this->feed->updated_at->diffForHumans(),
                'like' => $like
            ];
        }

        return $data;
    }
}
