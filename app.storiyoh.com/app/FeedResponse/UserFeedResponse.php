<?php
namespace App\FeedResponse;

use App\Models\User;

class UserFeedResponse extends FeedResponseable
{
    /**
     * 
     * @return array
     */
    public function response() 
    {
        //dd($this->feed->type);
        return [
            'type' => $this->feed->type,
            'name' => @$this->feed->typeable->full_name,
            'username' => @$this->feed->typeable->username,
            "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
            "follower_count" => count(json_decode($this->feed->data)),
            'display_text' => "is following ".count(json_decode($this->feed->data))." new users",
            //'display_text' => count(json_decode($this->feed->data)) > 1 ? "started following <username> & ". count(json_decode($this->feed->data)-1). " others" : "started following <username>",
            'follwed' => $this->followed_user($this->feed->data),
            'last_update' => $this->feed->updated_at->diffForHumans()
        ];
    }
    
    /**
     * 
     * @param type $data
     * @return array
     */
    private function followed_user($data) {
        $data = [];
        //$userData = User::whereIn('id', json_decode($this->feed->data))->get(['id', 'first_name', 'last_name', 'username', 'image'])->all();
        $user_data_ids = json_decode($this->feed->data);
        if(count($user_data_ids) > 6) {
            $reverse_data = array_reverse($user_data_ids);
            $user_data_ids = array_slice($reverse_data, 0, 6, true);
        }
        $userData = User::whereIn('id', $user_data_ids)->get(['id', 'first_name', 'last_name', 'username', 'image'])->all();
        foreach($userData as $item) {
            $data[] = [
                "name" => $item->full_name,
                "username" => $item->username,
                "user_photo" => ! empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
            ];
        }
        return $data;
    }

}
