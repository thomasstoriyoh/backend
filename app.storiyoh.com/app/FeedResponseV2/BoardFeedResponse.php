<?php
namespace App\FeedResponseV2;

use App\Models\Episode;
use App\Traits\Helper;

/**
 * Description of BoardFeedResponse
 *
 * @author bcm
 */
class BoardFeedResponse extends FeedResponseableV2 {
    
    public function response() {        
        $episode = Episode::whereIn('id', json_decode($this->feed->data))->first(['id', 'title', 'show_id', 'description', 'mp3', 'duration', 'listen_count', 'explicit']);
        if($episode) {
            return [
                'type' => $this->feed->type,
                'board_id' => $this->feed->typeable->id,
                'board_title' => $this->feed->typeable->title,
                "board_image" => !empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(200) : asset('uploads/default/board.png'),
                "no_follower" => $this->feed->typeable->followers()->count(),
                'display_text' => "has a new episode",
                'show_id' => @$episode->show->id,
                'show_name' => @$episode->show->title,
                'episode_id' => $episode->id,
                'episode_name' => $episode->title,
                'episode_desc' => strip_tags($episode->description),
                'episode_audio' => $episode->getAudioLink(),                
                "episode_image" => !empty($episode->show->image) ? $episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                "duration" => $episode->getDurationText(),
                'listen_count' => Helper::shorten_count($episode->listen_count), 
                "explicit" => $episode->show->explicit,
                'last_update' => $this->feed->updated_at->diffForHumans()
            ];
        }
    }    
}
