<?php
namespace App\FeedResponseV2;

use App\Models\Board;
use App\Traits\Helper;
use App\Classes\Feed;
use App\Models\PostShow;
use App\Models\PostEpisode;
use App\Models\PostPlaylist;
use App\Models\PostSmartPlaylist;
use App\Models\PostCollection;

/**
 * Description of BoardFollowResponse
 *
 * @author bcm
 */
class RePostResponse extends FeedResponseableV2 {

    public function response() { 

        if($this->feed->type == Feed::SHOW_REPOST) {
            try {
                $content = PostShow::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'show_id', 'post', 'likes_count', 'comment_count']);
                return [
                    'type' => $this->feed->type,
                    'content_id' => $content->id,
                    'name' => @$this->feed->typeable->full_name,
                    'username' => @$this->feed->typeable->username,
                    "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'display_text' => "shared a post to show",                    
                    'show_id' => $content->show->id,
                    'show_name' => trim(str_replace("\n", "", html_entity_decode($content->show->title))),
                    'show_categories' => @implode(", ", $content->show->categories()->pluck('title')->all()),                
                    'show_image' => !empty($content->show->image) ? $content->show->getWSImage(200) : asset('uploads/default/show.png'),
                    'post' => $content->post,
                    'last_update' => $this->feed->updated_at->diffForHumans(),
                    'no_of_subscribers' => Helper::shorten_count(0),
                    'no_of_likes' => Helper::shorten_count($content->likes_count),
                    'no_of_comments' => Helper::shorten_count($content->comment_count)
                ];
            } catch(\Exception $ex) {}            
        } else if($this->feed->type == Feed::EPISODE_REPOST) {
            try {
                $content = PostEpisode::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'episode_id', 'post', 'likes_count', 'comment_count']);
                return [
                    'type' => $this->feed->type,
                    'content_id' => $content->id,
                    'name' => @$this->feed->typeable->full_name,
                    'username' => @$this->feed->typeable->username,
                    "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'display_text' => "shared a post to episode",
                    'episode_id' => $content->episode_id,
                    'episode_name' => html_entity_decode($content->episode->title),
                    'show_id' => $content->episode->show->id,
                    'show_name' => trim(str_replace("\n", "", html_entity_decode($content->episode->show->title))),
                    'show_image' => !empty($content->episode->show->image) ? $content->episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                    'post' => $content->post,
                    'last_update' => $this->feed->updated_at->diffForHumans(),
                    'no_of_likes' => Helper::shorten_count($content->likes_count),
                    'no_of_comments' => Helper::shorten_count($content->comment_count),
                    'listen_count' => Helper::shorten_count($content->episode->listen_count),
                ];
            } catch(\Exception $ex) {}            
        }  else if($this->feed->type == Feed::PLAYLIST_REPOST) {
            try {
                $content = PostPlaylist::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'playlist_id', 'post', 'likes_count', 'comment_count']);
                return [
                    'type' => $this->feed->type,
                    'content_id' => $content->id,
                    'name' => @$this->feed->typeable->full_name,
                    'username' => @$this->feed->typeable->username,
                    "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'display_text' => "shared a post to playlist",
                    'board_id' => $content->playlist->id,
                    'board_name' => trim(str_replace("\n", "", html_entity_decode($content->playlist->title))),
                    'board_image' => !empty($content->playlist->image) ? $content->playlist->getImage(200) : asset('uploads/default/board.png'),
                    'post' => $content->post,
                    'last_update' => $this->feed->updated_at->diffForHumans(),
                    'no_of_likes' => Helper::shorten_count($content->likes_count),
                    'no_of_comments' => Helper::shorten_count($content->comment_count),
                    'no_of_followers' => Helper::shorten_count(0),
                ];
            } catch(\Exception $ex) {}            
        } else if($this->feed->type == Feed::SMARTPLAYLIST_REPOST) {
            try {
                $content = PostSmartPlaylist::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'smart_playlist_id', 'post', 'likes_count', 'comment_count']);
                return [
                    'type' => $this->feed->type,
                    'content_id' => $content->id,
                    'name' => @$this->feed->typeable->full_name,
                    'username' => @$this->feed->typeable->username,
                    "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'display_text' => "shared a post to smart playlist",
                    'board_id' => $content->smart_playlist->id,
                    'board_name' => trim(str_replace("\n", "", html_entity_decode($content->smart_playlist->title))),
                    'board_image' => !empty($content->smart_playlist->image) ? $content->smart_playlist->getImage(200) : asset('uploads/default/board.png'),
                    'post' => $content->post,
                    'last_update' => $this->feed->updated_at->diffForHumans(),
                    'no_of_likes' => Helper::shorten_count($content->likes_count),
                    'no_of_comments' => Helper::shorten_count($content->comment_count),
                    'no_of_followers' => Helper::shorten_count(0),
                ];
            } catch(\Exception $ex) {}            
        } else if($this->feed->type == Feed::COLLECTION_REPOST) {
            try {
                $content = PostCollection::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'collection_id', 'post', 'likes_count', 'comment_count']);
                return [
                    'type' => $this->feed->type,
                    'content_id' => $content->id,
                    'name' => @$this->feed->typeable->full_name,
                    'username' => @$this->feed->typeable->username,
                    "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'display_text' => "shared a post to collection",
                    'collection_id' => $content->collection->id,
                    'collection_name' => trim(str_replace("\n", "", html_entity_decode($content->collection->title))),
                    'collection_image' => !empty($content->collection->image) ? $content->collection->getImage(200) : asset('uploads/default/board.png'),
                    'post' => $content->post,
                    'last_update' => $this->feed->updated_at->diffForHumans(),
                    'no_of_likes' => Helper::shorten_count($content->likes_count),
                    'no_of_comments' => Helper::shorten_count($content->comment_count)
                ];
            } catch(\Exception $ex) {}            
        }
    }
}
