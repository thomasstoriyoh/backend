<?php

namespace App\FeedResponseV2;

use App\Models\Show;
use App\Traits\Helper;

/**
 * Description of SubscribeFeedResponse
 *
 * @author bcm
 */
class ShowFeedResponse extends FeedResponseableV2
{
    /**
     *
     * @return array
     */
    public function response()
    {
        $showInfo = Show::whereIn('id', json_decode($this->feed->data))->withCount('subscribers')->first(['id', 'title', 'image', 'description']);
        if ($showInfo) {
            $subscribers = $this->current_user->subscribers()->pluck('show_id')->all();
            $follow_status = 0;
            if (@in_array($showInfo->id, $subscribers)) {
                $follow_status = 1;
            }

            return [
                'type' => $this->feed->type,
                'name' => @$this->feed->typeable->full_name,
                'username' => @$this->feed->typeable->username,
                'user_photo' => !empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                'display_text' => 'subscribed to a new show',
                'show_id' => $showInfo->id,
                'show_name' => trim(str_replace("\n", '', html_entity_decode($showInfo->title))),
                'show_description' => $showInfo->description,
                'show_image' => !empty($showInfo->image) ? $showInfo->getWSImage(200) : asset('uploads/default/show.png'),
                'last_update' => $this->feed->updated_at->diffForHumans(),
                'user_follow_status' => $follow_status,
                'no_of_subscribers' => $showInfo->subscribers_count,
                'share_url' => config('config.live_url') . '/podcasts/' . $showInfo->id . '/' . Helper::make_slug(trim(@$showInfo->title)),
            ];
        }
    }
}
