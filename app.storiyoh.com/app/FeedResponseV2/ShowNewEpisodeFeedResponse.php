<?php
namespace App\FeedResponseV2;

use App\Models\Episode;
use App\Traits\Helper;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class ShowNewEpisodeFeedResponse extends FeedResponseableV2
{
    //put your code here    
    public function response() {        
        $episodes = Episode::whereIn('id', json_decode($this->feed->data))->whereNotNull('show_id')->get(['id', 'title', 'show_id', 'mp3', 'duration', 'listen_count', 'explicit']);
        if($episodes) {
            return [
                'type' => $this->feed->type,
                'show_id' => @$episodes->first()->show->id,
                'show_name' => trim(str_replace("\n", "", html_entity_decode(@$episodes->first()->show->title))),
                'show_image' => @$episodes->first()->show->image ? $episodes->first()->show->getWSImage(200) : asset('uploads/default/show.png'),
                'episode_data' => $this->episode_list($episodes)
            ];
        }        
    }

    protected function episode_list($episodes)
    {
        $data = []; 
        foreach($episodes as $episode)
        {            
            $data[] = [
                'episode_id' => $episode->id,
                'episode_name' => html_entity_decode($episode->title),
                'episode_image' => !empty($episode->show->image) ? $episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                'episode_audio' => $episode->getAudioLink(),
                'duration' => $episode->getDurationText(),
                'display_text' => "has a new episode",
                'listen_count' => Helper::shorten_count($episode->listen_count),
                "explicit" => $episode->show->explicit,
                'last_update' => $this->feed->updated_at->diffForHumans()                
            ];
        }

        return $data;
    }
}
