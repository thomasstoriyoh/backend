<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Module;
use App\Models\ActiveShow;
use App\Traits\CommonModuleMethods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Show;
use App\Models\ShowActiveHourly;
use App\Models\ShowActiveDaily;
use App\Models\ShowActiveDailyOnce;

class ActiveShowController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.active-show';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request)
    {
        $query = ActiveShow::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function ($query) use ($request) {
//                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('itunes_id', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (!empty($request->custom_filter)) {
            $query->where(function ($query) use ($request) {
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode('.', $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('created_at', 'desc')->take($request->length)->skip($request->start);
//        foreach ($request->order as $order) {
//            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
//        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'show_id' => $item->show($item->show_id)[0],
                'image' => empty($item->show($item->show_id)[1]) ? '' :
                '<img src="' . $item->show($item->show_id)[1] . '" width="100px" style="box-shadow:0 10px 16px 0 #CCC">',
                'created_at' => \Carbon\Carbon::parse($item->created_at)->format('d/m/Y')
            ];
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ActiveShow $active_show
     * @return \Illuminate\Http\Response
     */
    public function create(ActiveShow $active_show)
    {
        return $this->view('form', compact('active_show'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['show_id']);

        $show_info = Show::find($attributes['show_id'], ['id', 'itunes_id', 'feed_url']);

        if (empty($show_info->feed_url)) {
            return redirect()->back()->withInput($request->all())->withErrors([
                'show_id' => 'Feed url not found for this show.'
            ]);
        }

        $responseActive = ActiveShow::firstOrNew(['show_id' => $show_info->id]);
        $responseActive->fill([
            'show_id' => $show_info->id, 
            'itunes_id' => $show_info->itunes_id, 
            'feed_url' => $show_info->feed_url,
            'cron_run_at' => NULL
        ])->save();

        return redirect()->action('Admin\ActiveShowController@index')
            ->with('success', 'Faq active_show Created Successfully.');
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function search_movies(Request $request)
    {
        $query = Show::Published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No show found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = ActiveShow::findOrFail($id);
        if($item) {
            //Remove from Show active hourly table
            $ShowItem = ShowActiveHourly::where("show_id", $item->show_id)->first();
            if($ShowItem) {                
                $ShowItem->delete();
            }
            
            //Remove from Show active daily table
            $ShowItem2 = ShowActiveDaily::where("show_id", $item->show_id)->first();            
            if($ShowItem2) {
                $ShowItem2->delete();
            }

            //Remove from Show active daily once table
            $ShowItem3 = ShowActiveDailyOnce::where("show_id", $item->show_id)->first();
            if($ShowItem3) {
                $ShowItem3->delete();
            }
        }
        $item->delete();

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages()
    {
        return [
        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules()
    {
        return [
            'show_id' => ['required'],
        ];
    }
}
