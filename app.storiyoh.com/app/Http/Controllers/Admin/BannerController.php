<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Module;
use App\Models\Banner;
use App\Traits\CommonModuleMethods;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Validator;


class BannerController extends Controller
{
    use CommonModuleMethods;


    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.banner';

    /**
     * @var string
     */
    protected $upload_folder_name = "uploads/banner";

    /**
     * Helper for cover image dimension.
     *
     * @var array
     */
    protected $coverDimension = [
        ['width' => 1200, 'height' => 600]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Banner::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
//                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'image' => empty($item->image) ? '' :
                    '<img src="'. $item->getImage(640, 480) .'" width="100px">',
                'status' => $item->status,
                'admin_status' => $item->admin_status,
            ];
            if(Auth::guard('admin')->user()->can('edit.banner')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\BannerController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }


    public function create(Banner $banner)
    {
        return $this->view('form', compact('banner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {   //dd($request->all());
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['title', 'link', 'type']);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        if ($request->has('images')) {
            $attributes['image'] = $request->images[0];

            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/banner/' . $filename);            
            $image_name = "banner/" . $filename;
            $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

            //delete image from local
            unlink(public_path('uploads/banner/' . $filename));
            unlink(public_path('uploads/banner/thumbs/1200_600_' . $filename));
        }

        Banner::create($attributes);

        return redirect()->action('Admin\BannerController@index')
            ->with('success', 'Banner Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Banner $banner
     * @return mixed
     */

    public function edit(Banner $banner)
    {
        return $this->view('form', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Banner $banner
     * @return \Illuminate\Http\Response
     * @internal param Banner $banner
     */

    public function update(Request $request, Banner $banner)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($banner), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title', 'link', 'type']);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        if ($request->has('images')) {
            
            $attributes['image'] = $request->images[0];

            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/banner/' . $filename);            
            $image_name = "banner/" . $filename;
            $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

            //delete image from local
            unlink(public_path('uploads/banner/' . $filename));
            unlink(public_path('uploads/banner/thumbs/1200_600_' . $filename));

            //delete old image
            // if(! empty($banner->image)) {
            //     if (Storage::disk('s3')->exists('banner/' . $banner->image)) {
            //         Storage::disk('s3')->delete('banner/' . $banner->image);
            //     }
            // }
        }

        $banner->fill($attributes)->save();        
    
        return redirect()->action('Admin\BannerController@index')
            ->with('success', 'Banner Edited Successfully.');
    }

    /**
     * Update the image crop.
     *
     * @param $image
     * @param $chords
     * @return bool
     */

    protected function updateCropImage($image, $chords) {
        Image::make('uploads/banner/' . $image)->crop((int) $chords->w, (int) $chords->h, (int) $chords->x, (int) $chords->y)
            ->fit((int) $chords->width, (int) $chords->height)
            ->save('uploads/banner/thumbs/'. $chords->width ."_" . $chords->height . "_" . $image);
        return true;
    }

    
    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            'link' => ['required'],
            'type' => ['required'],
            'images' => ['required']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required', 'min:3'],
            'link' => ['required'],
            'type' => ['required'],
        ];
    }

    

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $request->resource == 'Image' ? $this->imageUpload($request) : $this->fileUpload($request);
    }

    /**
     * For uploading images...
     *
     * @param Request $request
     * @return array
     */
    protected function imageUpload(Request $request) {
        $files = $request->file;

        $name_array = explode(".", $request->name);
        array_pop($name_array);
        $name = implode(".", $name_array);

        $uploaded_files = [];
        $this->forceDimension = [];

        if (empty($request->$name)) {
            $this->forceDimension = $this->coverDimension;
        }else{
            $chords = (object) $request->$name;
            $this->crop_chords = $chords;
        }

        if (is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleImageWithChords($file, $filename, $this->upload_folder_name, "thumbs");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleImageWithChords($files, $filename, $this->upload_folder_name, "thumbs");
        }

        return $uploaded_files;
    }

    /**
     * For uploading files...
     *
     * @param Request $request
     * @return array
     */
    protected function fileUpload(Request $request) {
        $files = $request->file;

        $uploaded_files = [];
        if(is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleFile($file, $filename, "uploads/banner/files");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleFile($files, $filename, "uploads/banner/files");
        }

        return $uploaded_files;
    }

     /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */

     /**
     * Delete image page.
     *
     * @param Request $request
     * @param Banner $banner
     * @return array
     */
    public function deleteImage(Request $request, Banner $banner) {
        switch ($request->type) {
            case 'Cover':
                if (Storage::disk('s3')->exists('banner/' . $banner->image)) {
                    Storage::disk('s3')->delete('banner/' . $banner->image);
                }

                $banner->image = "";
                $banner->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined image type to delete.'];
                break;

        }
    }

    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = Banner::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Banner::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = Banner::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = Banner::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = Banner::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = Banner::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */	
    public function destroy($id)
    {
        $item = Banner::findOrFail($id);

        if (!empty($item->image)) {
            if (Storage::disk('s3')->exists('banner/' . $item->image)) {
                Storage::disk('s3')->delete('banner/' . $item->image);
            }                        
        }

        $item->delete();
        
        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
}
