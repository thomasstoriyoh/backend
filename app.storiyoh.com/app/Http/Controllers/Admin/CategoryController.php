<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Module;
use App\Models\Faqcategory;
use App\Traits\CommonModuleMethods;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class CategoryController extends Controller
{
    use CommonModuleMethods;


    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.faqs-category';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }
    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Faqcategory::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
//                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'status' => $item->status,
                'admin_status' => $item->admin_status,
            ];
            if(Auth::guard('admin')->user()->can('edit.category')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\CategoryController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Faqcategory $category
     * @return \Illuminate\Http\Response
     */
    public function create(Faqcategory $category)
    {
        return $this->view('form', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        $attributes = $request->only(['title']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
       
        Faqcategory::create($attributes);

        return redirect()->action('Admin\CategoryController@index')
            ->with('success', 'Faq category Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Faqcategory $category
     * @return mixed
     */
    public function edit(Faqcategory $category)
    {
        return $this->view('form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Faqcategory $category
     * @return \Illuminate\Http\Response
     * @internal param Faqcategory $category
     */
    public function update(Request $request, Faqcategory $category)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($category), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        $category->fill($attributes);
        $category->save();

        return redirect()->action('Admin\CategoryController@index')
            ->with('success', 'Faq category Edited Successfully.');
    }
    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = Faqcategory::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Faqcategory::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = Faqcategory::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = Faqcategory::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = Faqcategory::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = Faqcategory::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = Faqcategory::findOrFail($id);
        $item->delete();

        /*$this->forceDimension = $this->coverDimension;
        $this->deleteImageOrFile($item->image, $this->upload_folder_name, "thumbs");*/

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required', 'min:3'],
        ];
    }
}
