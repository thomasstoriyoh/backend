<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\Chart;
use App\Traits\CommonModuleMethods;
use Auth;
use Validator;
use Intervention\Image\Facades\Image;
use Tzsk\Collage\Facade\Collage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\Show;

class ChartController extends Controller
{
    use CommonModuleMethods;
        
    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.chart';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Chart::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('keywords', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'image' => empty($item->image) ? '' :
                    '<img src="' . $item->getImage(150) . '" width="150px" style="box-shadow:0 10px 16px 0 #CCC">',
                'date' => Carbon::parse($item->date)->format('d/m/Y'),
                'featured' => $item->featured ? "Yes" : "No", 
                'status' => $item->status
            ];
            if(Auth::guard('admin')->user()->can('edit.charts')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\ChartController@edit', $item->id) .'">Edit</a></li>
                            <li><a href="'. action('Admin\ChartController@reorder_podcast', $item->id) .'">Reorder Podcast</a></li>
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Chart $charts
     * @return \Illuminate\Http\Response
     */
    public function create(Chart $chart)
    {
        $tag_options = [];
        $podcastArray = [];
        
        return $this->view('form', compact('chart', 'tag_options', 'podcastArray'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        if(! $request->has('show_id') && ! $request->has('data_csv')) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'Please select atleast one podcast.']);
        }
        
        $attributes = $request->only(['title', 'date', 'source_url', 'description']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        $tag_list = [];
        if($request->has('keywords')) {
            foreach ($request->keywords as $item) {
                if (strstr($item, '*#$*#$')) {
                    $tag_list[] = ltrim($item, '*#$*#$');
                } else {
                    $tag_list[] = $item;
                }
            }
            $attributes['keywords'] = implode(",", $tag_list);            
        }

        $result = Chart::create($attributes);

        //If csv found
        $itunesIds = [];
        $csv_flag = true;
        $shows_id = [];
        if($request->has('data_csv')) {
            $csv_flag = false;
            $file = public_path('uploads/charts/' . $request->data_csv[0]);
            $all_values = [];
            $handle = fopen($file, 'r');            
            while (!feof($handle)) {
                $model = fgetcsv($handle);
                if (is_array($model)) {
                    $itunesIds[] = $model[0];
                }
            }
            array_filter($itunesIds);

            if(count($itunesIds) > 0) {
                $sync_array = [];
                foreach ($itunesIds as $key => $item) {
                    $showData = Show::where('itunes_id', $item)->first(['id']);
                    if ($showData) {                        
                        $sync_array[] = ['show_id' => $showData->id];
                        $shows_id[] = $showData->id;
                    }
                }
                $result->chart_poadcast()->sync($sync_array);

                // Update Collection Count
                $show_data = $result->chart_poadcast;
                foreach ($show_data as $show) {                    
                    Show::where('id', $show->id)->update(['collection_count' => \DB::raw('collection_count + 1')]);                    
                }
            }
            unlink(public_path('uploads/charts/' . $request->data_csv[0]));
        }

        if($request->has('show_id') && $csv_flag) {
            
            $shows_id = $request->show_id;
            $result->chart_poadcast()->sync($request->show_id);

            // Update Collection Count
            $show_data = $result->chart_poadcast;
            foreach ($show_data as $show) {                
                Show::where('id', $show->id)->update(['collection_count' => \DB::raw('collection_count + 1')]);                
            }
        }

        $filename = $this->generateCollageImage($shows_id, null);
        
        if(! empty($filename)) {
            $result->fill(['image' => $filename])->save();
        }

        return redirect()->action('Admin\ChartController@index')
            ->with('success', 'Chart Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Chart $Chart
     * @return mixed
     */
    public function edit(Chart $chart)
    {
        $keywordArray = explode(",", $chart->keywords);             
        $keywordArray = array_flatten($keywordArray);
        $tag_options = array_combine($keywordArray, $keywordArray);

        $podcastArray = $chart->chart_poadcast()->pluck('title', 'id')->all();

        return $this->view('form', compact('chart', 'tag_options', 'podcastArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Chart $Chart
     * @return \Illuminate\Http\Response
     * @internal param Chart $Chart
     */
    public function update(Request $request, Chart $chart)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($chart), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        if(! $request->has('show_id') && ! $request->has('data_csv')) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'Please select atleast one podcast.']);
        }

        $attributes = $request->only(['title', 'date', 'source_url', 'description']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        $tag_list = [];
        if($request->has('keywords')) {
            foreach ($request->keywords as $item) {
                if (strstr($item, '*#$*#$')) {
                    $tag_list[] = ltrim($item, '*#$*#$');
                } else {
                    $tag_list[] = $item;
                }
            }
            $attributes['keywords'] = implode(",", $tag_list);            
        }
        
        $chart->fill($attributes)->save();

        //If csv found
        $csv_flag = true;
        $itunesIds = [];
        $shows_id = [];
        if($request->has('data_csv')) {
            $csv_flag = false;
            $file = public_path('uploads/charts/' . $request->data_csv[0]);
            $all_values = [];
            $handle = fopen($file, 'r');            
            while (!feof($handle)) {
                $model = fgetcsv($handle);
                if (is_array($model)) {
                    $itunesIds[] = $model[0];
                }
            }

            array_filter($itunesIds);

            if(count($itunesIds) > 0) {
                // $sync_array = [];
                // foreach ($itunesIds as $key => $item) {
                //     $showData = Show::where('itunes_id', $item)->first(['id']);
                //     if ($showData) {
                //         $sync_array[] = ['show_id' => $showData->id];
                //         $shows_id[] = $showData->id;
                //     }
                // }
                // $chart->chart_poadcast()->sync($sync_array); 
                
                $sync_array = [];
                foreach ($itunesIds as $key => $item) {
                    $showData = Show::where('itunes_id', $item)->first(['id']);
                    if ($showData) {
                        $sync_array[] = ['show_id' => $showData->id];
                        $shows_id[] = $showData->id;
                    }
                }
                
                $chart_show_arr = $chart->chart_poadcast->pluck('id')->all();
                
                //Now Sync
                $chart->chart_poadcast()->sync($sync_array);

                $removeArr = array_diff($chart_show_arr, $shows_id);
                $addArr = array_diff($shows_id, $chart_show_arr);
                // dd($removeArr, $addArr);
                
                // Update Collection Count
                foreach ($addArr as $add) {                    
                    Show::where('id', $add)->update(['collection_count' => \DB::raw('collection_count + 1')]);   
                }

                // Update Collection Count
                foreach ($removeArr as $remove) {
                    Show::where('id', $remove)->update(['collection_count' => \DB::raw('collection_count - 1')]); 
                }
            }

            unlink(public_path('uploads/charts/' . $request->data_csv[0]));
        }

        if($request->has('show_id') && $csv_flag) {            

            $chart_show_arr = $chart->chart_poadcast->pluck('id')->all();
            
            //$chart->chart_poadcast()->sync($request->show_id);
            $shows_id = $request->show_id;
            $shows_id = array_map('intval', $shows_id);            

            $removeArr = array_diff($chart_show_arr, $shows_id);
            $addArr = array_diff($shows_id, $chart_show_arr);
            // dd($removeArr, $addArr);
            foreach ($addArr as $add) {
                // Update Collection Count
                Show::where('id', $add)->update(['collection_count' => \DB::raw('collection_count + 1')]);   
            }

            foreach ($removeArr as $remove) {
                // Update Collection Count
                Show::where('id', $remove)->update(['collection_count' => \DB::raw('collection_count - 1')]); 
            }
            
            $chart->chart_poadcast()->sync($request->show_id);
            $shows_id = $request->show_id;
        }
        
        $filename = $this->generateCollageImage($shows_id, $chart);

        if(! empty($filename)) {
            $chart->fill(['image' => $filename])->save();
        }
        
        return redirect()->action('Admin\ChartController@index')
            ->with('success', 'Chart Edited Successfully.');
    }

    /**
     * Fetured action.
     * 
     * @param Request $request
     * @return array
     */
    public function featured(Request $request) {
        
        $total = Chart::featured()->count();
        $featured_count = Chart::featured()->whereNotIn('id', $request->ids)->count();
        if (($featured_count + count($request->ids)) > 10) {
            return ['status' => false, 
                'message' => 'You cannot feature more than 10 items. You already have ' . $total 
                    . ' items in your featured list.'];
        }
        
        foreach ($request->ids as $id) {
            $item = Chart::findOrFail($id);
            $item->setEventType('Featured');
            $item->featured = true;
            $item->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Unfetured action.
     * 
     * @param Request $request
     * @return array
     */
    public function unfeatured(Request $request) {        
        foreach ($request->ids as $id) {
            $item = Chart::findOrFail($id);
            $item->setEventType('Unfeatured');
            $item->featured = false;
            $item->save();
        }
        
        return ['status' => true];
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $this->fileUpload($request);
    }

    /**
     * Product document upload.
     *
     * @param Request $request
     * @return array
     */
    protected function fileUpload(Request $request) {
        $files = $request->file;
        
        $uploaded_files = [];
        if(is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleFile($file, $filename, "uploads/charts");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleFile($files, $filename, "uploads/charts");
        }
        
        return $uploaded_files;
    }
    
    /**
     * Generate Collage Image
     * @param type $episodes
     * @return string
     */
    protected function generateCollageImage($podcast, $chart)
    {
        if (count($podcast) > 0) {
            $show_images = Show::whereIn('id', $podcast)->take(4)->pluck('image')->all();

            $images = [];
            foreach ($show_images as $epp_image) {
                $images[] = config('config.s3_url') . '/shows/' . $epp_image;
            }

            if (count($images) > 1) {
                $images = array_pad($images, 4, Image::canvas(400, 400, '#C0C0C0'));
            }

            if (count($images)) {
                $imageArray = [];
                foreach ($images as $image) {
                    try {
                        $imageArray[] = Image::make($image);
                    } catch (\Exception $e) {
                        continue;
                    }
                }

                if (count($imageArray) > 0) {
                    
                    if (!empty($chart) && !empty($chart->image)) {
                        if (Storage::disk('s3')->exists('charts/' . $chart->image)) {
                            Storage::disk('s3')->delete('charts/' . $chart->image);
                        }                        
                    }
                    
                    $collage = Collage::make(400, 400)->from($imageArray);
                    $filename = date('YmdHis') . rand(100000, 999999) . '.png';
                    $collage->save('uploads/charts/' . $filename, 100);

                    Storage::disk('s3')->put('charts/' . $filename, file_get_contents(asset('uploads/charts/' . $filename)));

                    unlink(public_path('uploads/charts/' . $filename));
                    
                    return $filename;
                }

                return '';
            }
        }

        return '';
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function search_keyword(Request $request)
    {
        $keywordData = Chart::where('keywords', 'LIKE', '%' . $request->q . '%')
            ->take(20)->get(['keywords']);

        $finalArrayData = [];
        if ($keywordData->count() == 0) {
            $finalArrayData[] = ['id' => $request->q, 'text' => $request->q];
        }

        $arrayData = [];
        foreach ($keywordData as $item) {
            $explodeDat = @explode(",", $item->keywords);            
            $arrayData[] = array_flatten($explodeDat);
        }

        $arrayData = array_filter(array_flatten($arrayData));
        $arrayData = array_unique($arrayData);
        
        foreach($arrayData as $item) {
            $finalArrayData[] = ['id' => $item, 'text' => $item];
        }

        $results['results'] = $finalArrayData;

        return $results;
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {
        foreach ($request->ids as $id) {
            $item = Chart::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Chart::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }    

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = Chart::published()->orderBy('order')->latest()->get(['id', 'title']);
        return $this->view('reorder', compact('items'));
    }

    /**
     * Reorder Podcast resources
     *
     * @param Chart $chart
     * @return void
     */
    public function reorder_podcast(Chart $chart) {
        $items = $chart->chart_poadcast()->orderBy('chart_show.order')->get(['id', 'title']);
        return $this->view('reorder_podcast', compact('items', 'chart'));
    }
    
    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = Chart::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postPodcastReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        $chartData = Chart::findOrFail($request->chartId);
        foreach ($sequence as $order => $id) {            
            $chart_reorder = $chartData->chart_poadcast()->find($id);
            if ($chart_reorder) {
                $chart_reorder->pivot->order = $order+1;
                $chart_reorder->pivot->save();
            }                        
        }

        $chartData->setEventType('Login');
        if($order == 0) {
            $chartData->setEventType('Rearranged');
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = Chart::findOrFail($id);        

        if (!empty($item->image)) {
            if (Storage::disk('s3')->exists('charts/' . $item->image)) {
                Storage::disk('s3')->delete('charts/' . $item->image);
            }                        
        }

        $item->delete();

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            'date' => ['required'],
            'keywords' => ['required']            
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required', 'min:3'],
            'date' => ['required'],
            'keywords' => ['required']
        ];
    }
}
