<?php

namespace App\Http\Controllers\Admin\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;
use App\Models\Admin\Admin;
use App\Models\Admin\Module;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Using the common module methods.
     */
    use CommonModuleMethods;
    
    /**
     * The view root directory.
     * 
     * @var string
     */
    protected $viewRoot = 'admin.core.admin';

    protected $role_list = [
        'A' => 'Content Manager',
        'S' => 'Sales Team',
        'O' => 'Operations Team'
    ];

    /**
     * Resource constructor.
     */
    public function __construct() {
        $this->middleware('auth.admin:admin');
    }
    
    /**
     * Resource listing page.
     * 
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController('AdminController')->first();

        return $this->view('index', compact('module_details'));
    }
    
    /**
     * Ajax json response of the resource.
     * 
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        
        $query = Admin::whereIn('type', ['A', 'S', 'O'])->take($request->length)->skip($request->start);
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query) use ($request) {
                $query->orWhere('first_name', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('last_name', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('email', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('username', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }
        
        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }
        
        foreach ($request->order as $order) {
            if ($request->columns[$order['column']]['data'] == 'order') {
                continue;
            }
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $admins = $query->where('id', '!=', Auth::guard('admin')->user()->id)->get();
        
        $data = [];
        foreach ($admins as $key => $admin) {
            $row = [
                'id' => $admin->id,
                'order' => $key + 1,
                'first_name' => $admin->first_name,
                'last_name' => $admin->last_name,
                'email' => $admin->email,
                'username' => $admin->username,
                'last_login' => is_null($admin->last_login) ? 'Never' : $admin->last_login->diffForHumans(),
                'status' => $admin->status,
                'admin_status' => $admin->admin_status,
            ];

            if(Auth::guard('admin')->user()->can('edit.admin')){
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\Core\AdminController@edit', $admin->id) .'"><i class="glyph-icon icon-edit"></i> Edit</a></li>
                        </ul>
                    </div>
                </div>';
            }

            $data[] = $row;
        }
        
        return [
            'recordsTotal' => Admin::whereIn('type', ['A', 'S', 'O'])->count(),
            'recordsFiltered' => Admin::whereIn('type', ['A', 'S', 'O'])->count(),
            'data' => $data
        ];
    }

    /**
     * View the profile.
     *
     * @return mixed
     */
    public function profile() {
        $admin = Auth::guard('admin')->user();

        return $this->view('profile', compact('admin'));
    }


    public function postProfile(Request $request) {
        $admin = Auth::guard('admin')->user();
        $this->validate($request, [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'email' => ['required', 'unique:admins,email,'.$admin->id],
            'username' => ['required'],
            'old_password' => ['required_with:password'],
            'password' => ['required_with:old_password', 'confirmed']
        ]);

        $attributes = $request->only(['first_name', 'last_name', 'email']);
        if ($request->has('password') && $request->has('old_password')) {
            if (! Hash::check($request->old_password, $admin->password)) {
                return redirect()->back()->withErrors(['password' => 'Old password did not match with existing password in the database.']);
            }

            $attributes['password'] = Hash::make($request->password);
        }

        $admin->fill($attributes);

        $admin->save();

        return redirect()->back()->with(['success' => 'Profile updated successfully.']);
    }
    
    /**
     * Resourece create page.
     * 
     * @param Admin $admin
     * @return mixed
     */
    public function create(Admin $admin) {
        $modules = Module::published()->whereType('Module')->whereModuleId(0)->get();
        $configs = Module::published()->whereType('Configure')->whereModuleId(0)->get();
        $reports = Module::published()->whereType('Report')->whereModuleId(0)->get();
        $role_list = $this->role_list;
        return $this->view('form', compact('admin', 'modules', 'configs', 'reports', 'role_list'));
    }
    
    /**
     * Store resource.
     * 
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request) {
        $validator = Validator::make(
                $request->all(), 
                $this->storeValidationRules(), 
                $this->validationMessages()
            );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $attributes = $request->only(['first_name', 'last_name', 'email', 'username', 'type']);
        $attributes['password'] = Hash::make($request->password);
        $attributes['status'] = $request->status ? 'Active' : 'Inactive';
        $attributes['type'] = "A";
        //dd($attributes);
        $admin = Admin::create($attributes);

        $permissions = $this->getParsedPermissions($request);
        if (!empty($permissions)) {
            $admin->addPermissions($permissions);
        }

        return redirect()->action('Admin\Core\AdminController@index')
                ->with('success', 'CMS User Created Successfully.');
    }
    
    /**
     * Edit resource page.
     * 
     * @param Admin $admin
     * @return mixed
     */
    public function edit(Admin $admin) {
        $modules = Module::published()->whereType('Module')->whereModuleId(0)->get();
        $configs = Module::published()->whereType('Configure')->whereModuleId(0)->get();
        $reports = Module::published()->whereType('Report')->whereModuleId(0)->get();
        $role_list = $this->role_list;

        return $this->view('form', compact('admin', 'modules', 'configs', 'reports', 'role_list'));
    }
    
    /**
     * Update resourece action.
     * 
     * @param Request $request
     * @param Admin $admin
     * @return mixed
     */
    public function update(Request $request, Admin $admin) {
        $validator = Validator::make(
                $request->all(), 
                $this->updateValidationRules($admin->id), 
                $this->validationMessages()
            );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $admin->fill($request->only(['first_name', 'last_name', 'email', 'username']));
        if ($request->has('password')) {
            $admin->password = Hash::make($request->password);
        }
        $admin->status = $request->status ? 'Active' : 'Inactive';
        $admin->save();

        $permissions = $this->getParsedPermissions($request);
        $admin->syncPermissions($permissions);

        return redirect()->action('Admin\Core\AdminController@index')
                ->with('success', 'CMS User updated successfully.');
    }
    
    /**
     * Publish Resource.
     * 
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {
        
        foreach ($request->ids as $id) {
            $admin = Admin::findOrFail($id);
            $admin->status = 'Active';
            $admin->setEventType('Activated');
            $admin->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Publish Resource.
     * 
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        
        foreach ($request->ids as $id) {
            $admin = Admin::findOrFail($id);
            $this->approveModule($admin);
        }
        
        return ['status' => true];
    }
    
    /**
     * Publish Resource.
     * 
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {
        
        foreach ($request->ids as $id) {
            $admin = Admin::findOrFail($id);
            $this->unapproveModule($admin);
        }
        
        return ['status' => true];
    }
    
    /**
     * Draft Resource.
     * 
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        
        foreach ($request->ids as $id) {
            $admin = Admin::findOrFail($id);
            $admin->status = 'Inactive';
            $admin->setEventType('Deactivated');
            $admin->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Delete resource.
     * 
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {
        
        foreach ($request->ids as $id) {
            $this->destroy($id);
        }
        
        return ['status' => true];
    }
    
    /**
     * Destroy resource.
     * 
     * @param $id
     * @return boolean
     */
    public function destroy($id) {
        $admin = Admin::findOrFail($id);
        $admin->setEventType('Deleted');
        $admin->delete();
        
        return true;
    }

    /**
     * Validation messages.
     * 
     * @return array
     */
    public function validationMessages() {
        return [];
    }
    
    /**
     * Validation for store.
     * 
     * @return array
     */
    public function storeValidationRules() {
        return [
            'first_name' => ['required', 'min:3', 'max:20'],
            'email' => ['required', 'email', 'unique:admins'],
            'password' => ['required', 'confirmed', 'min:6', 'max:20'],
            'username' => ['required', 'min:4', 'max:20', 'unique:admins'],
        ];
    }
    
    /**
     * Parse the permissions from the request.
     *
     * @param $request
     * @return array
     */
    protected function getParsedPermissions($request) {
        $permissions = [];
        if ($request->has('read')) {
            foreach ($request->read as $access_id) {
                $module = Module::whereId($access_id)->first(['slug']);
                $permissions[] = 'read.' . $module->slug;
            }
        }

        if ($request->has('create')) {
            foreach ($request->create as $access_id) {
                $module = Module::whereId($access_id)->first(['slug']);
                $permissions[] = 'create.' . $module->slug;
            }
        }

        if ($request->has('update')) {
            foreach ($request->update as $access_id) {
                $module = Module::whereId($access_id)->first(['slug']);
                $permissions[] = 'update.' . $module->slug;
            }
        }

        if ($request->has('delete')) {
            foreach ($request->delete as $access_id) {
                $module = Module::whereId($access_id)->first(['slug']);
                $permissions[] = 'delete.' . $module->slug;
            }
        }

        return $permissions;
    }
    
    /**
     * Update validator.
     * 
     * @param $id
     * @return array
     */
    public function updateValidationRules($id) {
        return [
            'first_name' => ['required', 'min:3', 'max:20'],
            'email' => ['required', 'email', 'unique:admins,email,'.$id],
            'username' => ['required', 'min:4', 'max:20', 'unique:admins,id,'.$id],
        ];
    }

}
