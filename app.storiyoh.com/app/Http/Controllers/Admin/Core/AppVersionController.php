<?php

namespace App\Http\Controllers\Admin\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\AppVersion;
use App\Traits\CommonModuleMethods;
use Intervention\Image\Facades\Image;
use Auth;
use Validator;
use Cache;
use App\Jobs\AppVersionJob;

class AppVersionController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.core.app_version';
    
    /**
     * @var string
     */
    protected $upload_folder_name = "uploads/version";

    /**
     * Helper for cover image dimension.
     *
     * @var array
     */
    protected $coverDimension = [
        ['width' => 640, 'height' => 480]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();
        
        $app_version_count = AppVersion::all();
        $count_value = ! empty($app_version_count) ? $app_version_count->count() : 0;
        
        return $this->view('index', compact('module_details', 'count_value'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = AppVersion::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('ios_version', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('android_version', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            if($request->columns[$order['column']]['data'] == "order") {
                continue;
            }
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'platform' => strtoupper($item->platform),
                'version' => $item->version,
                'force_update' => $item->force_update                
            ];
            if(Auth::guard('admin')->user()->can('edit.app_version')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\Core\AppVersionController@edit', $item->id) .'">Edit</a></li>
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param AppVersion $app_version
     * @return \Illuminate\Http\Response
     */
    public function create(AppVersion $app_version) {
        return $this->view('form', compact('app_version'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
//        if ($validator->fails()) {
//            return redirect()->back()->withInput($request->all())->withErrors($validator);
//        }
//        
//        if ($request->has('images')) {
//            $attributes['image'] = $request->images[0];
//        }
//        
//        $attributes = $request->all();
//        
//        //$attributes['status'] = 'Published';
//        //$attributes['admin_status'] = 'Approved';
//        
//        AppVersion::create($attributes);

        return redirect()->action('Admin\Core\AppVersionController@index')
            ->with('success', 'Version Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AppVersion $app_version
     * @return mixed
     */
    public function edit(AppVersion $app_version) {        
        return $this->view('form', compact('app_version'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param AppVersion $app_version
     * @return \Illuminate\Http\Response
     * @internal param AppVersion $app_version
     */
    public function update(Request $request, AppVersion $app_version)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($app_version), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->all();
        
        //$attributes['status'] = 'Published';
        //$attributes['admin_status'] = 'Approved';
        
        if ($request->has('images')) {
            $attributes['image'] = $request->images[0];
        }
        
        if ($request->has('edit_chords')) {
            foreach ($request->edit_chords as $edit_chord) {
                $this->updateCropImage($app_version->image, json_decode($edit_chord));
            }
        }

        $app_version->fill($attributes);
        $app_version->save();

        return redirect()->action('Admin\Core\AppVersionController@index')
            ->with('success', 'Version Edited Successfully.');
    }

    /**
     * Update the image crop.
     *
     * @param $image
     * @param $chords
     * @return bool
     */
    protected function updateCropImage($image, $chords) {
        Image::make('uploads/version/' . $image)->crop((int) $chords->w, (int) $chords->h, (int) $chords->x, (int) $chords->y)
            ->fit((int) $chords->width, (int) $chords->height)
            ->save('uploads/version/thumbs/'. $chords->width ."_" . $chords->height . "_" . $image);
        return true;
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $request->resource == 'Image' ? $this->imageUpload($request) : $this->fileUpload($request);
    }

    /**
     * For uploading images...
     *
     * @param Request $request
     * @return array
     */
    protected function imageUpload(Request $request) {
        $files = $request->file;

        $name_array = explode(".", $request->name);
        array_pop($name_array);
        $name = implode(".", $name_array);

        $uploaded_files = [];
        $this->forceDimension = [];

        if (empty($request->$name)) {
            $this->forceDimension = $this->coverDimension;
        }else{
            $chords = (object) $request->$name;
            $this->crop_chords = $chords;
        }

        if (is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleImageWithChords($file, $filename, $this->upload_folder_name, "thumbs");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleImageWithChords($files, $filename, $this->upload_folder_name, "thumbs");
        }

        return $uploaded_files;
    }

    /**
     * For uploading files...
     *
     * @param Request $request
     * @return array
     */
    protected function fileUpload(Request $request) {
        $files = $request->file;

        $uploaded_files = [];
        if(is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleFile($file, $filename, "uploads/version/files");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleFile($files, $filename, "uploads/version/files");
        }

        return $uploaded_files;
    }

    /**
     * Delete image page.
     *
     * @param Request $request
     * @param Version $version
     * @return array
     */
    public function deleteImage(Request $request, AppVersion $app_version) {
        switch ($request->type) {
            case 'Cover':
                $this->forceDimension = $this->coverDimension;
                $this->deleteImageOrFile($app_version->image, $this->upload_folder_name, "thumbs");
                $app_version->image = "";
                $app_version->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined image type to delete.'];
                break;

        }
    }
    
    /**
     * 
     * @param Request $request
     * @return type
     */
    public function notification(Request $request) {
        
        if(count($request->ids) > 1) {
           return ['status' => false, "message" => "Notification must be processed one at a time"]; 
        }
        
        $version = AppVersion::where('id', $request->ids)->first();
        
        if (Cache::get('ProcessRunning.App.Push'.$version->id)) {
            return ['status' => false, 'message' => "Push already in progress."];
        }

        //Cache::forever('ProcessRunning.App.Push'.$version->id, 'Yes');
        
        dispatch(new AppVersionJob($version->id));
                        
        return ['status' => true];
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'version' => ['required'],
            'platform' => ['required'],
            'force_update' => ['required'],
            'title' => ['required'],
            'description' => ['required'],
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'version' => ['required'],
            //'platform' => ['required'],
            'force_update' => ['required'],
            'title' => ['required'],
            'description' => ['required'],
        ];
    }
}
