<?php

namespace App\Http\Controllers\Admin\Core;

use App\Models\Admin\Language;
use App\Models\Admin\ModuleEntryView;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Admin\Module;
use App\Models\Admin\Log;

class DashboardController extends Controller
{
    /**
     * Resource constructor.
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    $dashboardModule = $this->getModuleData();
	
	    $logs_data = Log::latest()->take(10)->get();

		$module_view_data = $this->getModuleViewData();

	    return view('admin.index', compact('dashboardModule', 'logs_data', 'module_view_data'));
	}
     	 
	/**
	 * Get the module doughnut chart data.
	 *
	 * @return mixed
	*/
	private function getModuleData()
	{
	    $modules = Module::published()->where('table_name', '!=', '')
            ->whereType('Module')->whereNotIn('slug', ['state', 'city', 'area'])
            ->get(['id', 'title', 'model']);

	    $module_data = [];
	    foreach ($modules as $module) {
            $item = 'App\Models\\' . $module->model;
			if (!empty($item::$routeRegistered)) {
                $module_data[$module->title] = app()->make('App\Models\\' . $module->model)->count();
            }
	    }
	    arsort($module_data);
	
	    $spliceArray = array_splice($module_data, 0, 4);
		$finalArray[] = array("Module", "Total");
		foreach($spliceArray as $k=>$v){
			$finalArray[] = array($k, $v);
		}
		return $finalArray;
	}

    protected function getModuleViewData() {
        $widgets = Module::published()->whereType('Module')->whereWidget(true)->get();

        $data = [];
        foreach ($widgets as $widget) {
            $views = ModuleEntryView::whereModuleId($widget->id)
                ->whereBetween('view_date', [session('ga_start_date_for_analytics'), session('ga_end_date_for_analytics')])
                ->selectRaw('view_date, SUM(`count`) AS `views`')->groupBy('view_date')->get();
            $data[$widget->title] = $views->toArray();
        }

        foreach ($data as $key => $item) {
            foreach ($item as $k => $i) {
                $data[$key][$k] = [Carbon::createFromFormat('Y-m-d', $i['view_date'])->format('jS M'), (int) $i['views']];
            }
        }

        return array_filter($data);
    }
    
	/**
	 * Update date range for Google Analytics.
	 *
	 * @return mixed
	*/
	public function ajaxIndex(Request $request)
	{
		session()->put('ga_start_date_for_analytics', $request->start);
		session()->put('ga_end_date_for_analytics', $request->end);
		
		return ['status'=>true];
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $locale
     * @return \Illuminate\Http\Response
     */
    public function show($locale)
    {
        $language = Language::whereLocale($locale)->first();
		if ($language) {
            session()->put('current_language', $language->locale);
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}