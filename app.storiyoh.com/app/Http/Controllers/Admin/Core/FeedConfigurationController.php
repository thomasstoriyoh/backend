<?php

namespace App\Http\Controllers\Admin\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\FeedConfiguration;
use App\Traits\CommonModuleMethods;
use Intervention\Image\Facades\Image;
use Auth;
use Validator;

class FeedConfigurationController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.core.feed_configuration';
    
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();        
        $feed_configuration = FeedConfiguration::first();
        //dd($feed_configuration, $module_details);
        return $this->view('index', compact('module_details', 'feed_configuration'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = FeedConfiguration::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                //$query->orWhere('ios_version', 'LIKE', '%'. $request->search['value'] .'%');
                //$query->orWhere('android_version', 'LIKE', '%'. $request->search['value'] .'%');
                //$query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            if($request->columns[$order['column']]['data'] == "order") {
                continue;
            }
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'trending' => $item->trending
            ];
            if(Auth::guard('admin')->user()->can('edit.app_version')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\Core\FeedConfigurationController@edit', $item->id) .'">Edit</a></li>
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param FeedConfiguration $feed_configuration
     * @return \Illuminate\Http\Response
     */
    public function create(FeedConfiguration $feed_configuration) {
        return $this->view('form', compact('app_version'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->all();
        
        $feed_configuration = FeedConfiguration::first();
        $feed_configuration->fill($attributes);
        $feed_configuration->save();

        return redirect()->action('Admin\Core\FeedConfigurationController@index')
            ->with('success', 'Feed Configuration Edited Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FeedConfiguration $feed_configuration
     * @return mixed
     */
    public function edit(FeedConfiguration $feed_configuration) {        
        return $this->view('form', compact('app_version'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param FeedConfiguration $feed_configuration
     * @return \Illuminate\Http\Response
     * @internal param FeedConfiguration $feed_configuration
     */
    public function update(Request $request, FeedConfiguration $feed_configuration)
    {
        return redirect()->action('Admin\Core\FeedConfigurationController@index')
            ->with('success', 'Version Edited Successfully.');
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'trending_shows' => ['required'],
            'trending_boards' => ['required'],
            'trending_episodes' => ['required']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'trending_shows' => ['required'],
            'trending_boards' => ['required'],
            'trending_episodes' => ['required']
        ];
    }
}
