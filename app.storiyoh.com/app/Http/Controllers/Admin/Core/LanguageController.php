<?php

namespace App\Http\Controllers\Admin\Core;

use App\Models\Admin\Language;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LanguageController extends Controller
{
    use CommonModuleMethods;
    
    protected $viewRoot = "admin.core.language";

    /**
     * Language Listing page.
     *
     * @return \App\Traits\type
     */
    public function index() {
        $language = Language::whereLocale('en')->first();
        return $this->view('index', compact('language'));
    }

    /**
     * Item listing json.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Language::take($request->length)->skip($request->start);
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $query->orderBy('order');
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $languages = $query->latest()->get();

        $data = [];
        foreach ($languages as $key => $language) {
            $row = [
                'id' => $language->id,
                'order' => $key + 1,
                'title' => $language->title,
                'locale' => $language->locale,
                'status' => $language->status,
                'default' => $language->default ? 'Yes' : 'No',
            ];

            if(Auth::guard('admin')->user()->can('edit.language')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li>
                                <a href="'. action('Admin\Core\LanguageController@edit', $language->id) .'">
                                    <i class="glyph-icon icon-edit"></i> Edit
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>';
            }

            $data[] = $row;
        }

        return [
            'recordsTotal' => Language::count(),
            'recordsFiltered' => Language::count(),
            'data' => $data
        ];
    }

    /**
     * Language create page.
     *
     * @param Language $language
     * @return mixed
     */
    public function create(Language $language) {
        return $this->view('form', compact('language'));
    }

    /**
     * Store resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'locale' => 'required|unique:languages'
        ]);

        $attributes = $request->only(['title', 'locale']);
        $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';
        Language::create($attributes);

        if ($request->get('locale') != "en") {
            Storage::disk('app')->makeDirectory('resources/lang/' . $request->get('locale') . '/sections');

            $all_files = Storage::disk('app')->allFiles('resources/lang/en');
            foreach ($all_files as $file) {
                $new_file = str_replace('/en/', '/'. $request->get('locale') .'/', $file);
                Storage::disk('app')->copy($file, $new_file);
            }
        }

        return redirect()->action('Admin\Core\LanguageController@index')
            ->with('success', 'Language Created Successfully.');
    }

    /**
     * Edit each language.
     *
     * @param Language $language
     * @return mixed
     */
    public function edit(Language $language) {
        if ($language->locale == 'en') {
            return redirect()->back()->with('error', 'English cannot be updated.');
        }
        return $this->view('form', compact('language'));
    }

    /**
     * Update the language.
     *
     * @param Request $request
     * @param Language $language
     * @return mixed
     */
    public function update(Request $request, Language $language) {
        $this->validate($request, [
            'title' => 'required',
            'locale' => 'required|unique:languages,locale,'.$language->id
        ]);

        $attributes = $request->only(['title']);
        $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';
        $language->fill($attributes);

        $language->save();

        return redirect()->action('Admin\Core\LanguageController@index')->with('success', $language->title . ' Edited Successfully.');
    }

    /**
     * View the language details.
     *
     * @param Language $language
     * @return mixed
     */
    public function show(Language $language) {
        $sections = $this->formatSections(Storage::disk('app')->allFiles('resources/lang/'. $language->locale . '/sections'));
        return $this->view('show', compact('language', 'sections'));
    }

    /**
     * Create section.
     *
     * @return mixed
     */
    public function createSection() {
        return $this->view('section_form');
    }

    /**
     * Store Section.
     *
     * @param Request $request
     * @return mixed
     */
    public function storeSection(Request $request) {
        $all_languages = Language::all();
        foreach ($all_languages as $language) {
            if (Storage::disk('app')->has('resources/lang/' . $language->locale . '/sections/' . $request->slug . '.php')) {
                continue;
            }
            $content = $this->getContentFromArray([]);
            Storage::disk('app')->put('resources/lang/' . $language->locale . '/sections/' . $request->slug . '.php', $content);
        }
        return redirect()->action('Admin\Core\LanguageController@show', 1)->with('success', 'Section Created Successfully');
    }

    /**
     * Word listing.
     *
     * @param $section
     * @return \App\Traits\type
     */
    public function words($section) {
        $words = require app_path('../resources/lang/en/sections/'.$section.'.php');
        $languages = Language::all();
        return $this->view('words', compact('words', 'section', 'languages'));
    }

    /**
     * Store the word.
     *
     * @param Request $request
     * @param $section
     * @return mixed
     */
    public function storeWord(Request $request, $section) {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required'
        ], [
            'title' => ['required' => 'Please enter a word.'],
        ]);

        $words = require app_path('../resources/lang/en/sections/'.$section.'.php');
        $words[$request->slug] = $request->title;
        $content = $this->getContentFromArray($words);
        Storage::disk('app')->put('resources/lang/en/sections/' . $section . '.php', $content);

        $all_languages = Language::all();
        foreach ($all_languages as $language) {
            if ($language->locale == 'en') {
                continue;
            }
            $other_words = require app_path('../resources/lang/'.$language->locale.'/sections/'.$section.'.php');
            $other_words[$request->slug] = "";
            $other_content = $this->getContentFromArray($other_words);

            Storage::disk('app')->put('resources/lang/' . $language->locale . '/sections/' . $section . '.php', $other_content);
        }

        return redirect()->action('Admin\Core\LanguageController@words', $section)
            ->with('success', 'Word added successfully.')
            ->with('slug', $request->slug)->with('title', $request->title);
    }

    /**
     * Save the translated words.
     *
     * @param Request $request
     * @return array
     */
    public function caption(Request $request) {
        $words = require app_path('../resources/lang/'.$request->get('locale').'/sections/'.$request->get('name').'.php');
        $words[$request->get('pk')] = $request->get('value');

        $content = $this->getContentFromArray($words);
        Storage::disk('app')->put('resources/lang/' . $request->get('locale') . '/sections/' . $request->get('name') . '.php', $content);

        return ['status' => true];
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = Language::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Language::findOrFail($id);
            if($item->locale == 'en') {
                continue;
            }
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Make Default Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $key => $id) {
            if ($key > 0) {
                break;
            }
            with(new Language)->update(['default' => 0]);

            $item = Language::findOrFail($id);
            $item->default = true;
            $item->save();
        }

        return ['status' => true];
    }

    /**
     * Delete section.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {
        $languages = Language::all();

        foreach ($request->ids as $item) {
            $file = $item . ".php";

            foreach ($languages as $language) {
                Storage::disk('app')->delete('resources/lang/'.$language->locale.'/sections/'.$file);
            }
        }
        
        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = Language::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = Language::findOrFail($id);
            $items->order = $order + 1;
            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete words from sections.
     *
     * @param Request $request
     * @param $section
     * @return array
     */
    public function deleteImage(Request $request, $section) {
        $languages = Language::all();

        foreach ($languages as $language) {
            $words = require app_path('../resources/lang/'.$language->locale.'/sections/'.$section.'.php');
            foreach ($request->slugs as $slug) {
                unset($words[$slug]);
            }
            $content = $this->getContentFromArray($words);
            Storage::disk('app')->put('resources/lang/' . $language->locale . '/sections/' . $section . '.php', $content);
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = Language::findOrFail($id);
        if ($item->locale == 'en') {
            return true;
        }
        if ($item->default) {
            Language::whereLocale('en')->update(['default' => true]);
        }

        $item->delete();

        Storage::disk('app')->deleteDirectory('resources/lang/'.$item->locale);

        return true;
    }

    /**
     * Format the section names.
     *
     * @param $allFiles
     * @return array
     */
    private function formatSections($allFiles)
    {
        $sections = [];
        foreach ($allFiles as $file) {
            $file_array = explode("/", $file);
            $section_slug = rtrim(end($file_array), '.php');
            $section_title = ucwords(str_replace('-', ' ', $section_slug));
            $sections[$section_slug] = $section_title;
        }

        return $sections;
    }

    /**
     * Get array in a file format.
     *
     * @param $words
     * @return mixed
     */
    private function getContentFromArray($words)
    {
        ob_start();
        echo '<?php ' . PHP_EOL;
        echo 'return ';
        var_export($words);
        echo ';';
        $content = ob_get_clean();

        return $content;
    }
}
