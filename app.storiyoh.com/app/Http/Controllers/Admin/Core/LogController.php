<?php

namespace App\Http\Controllers\Admin\Core;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;
use App\Models\Admin\Log;

class LogController extends Controller
{
	use CommonModuleMethods;
    
    protected $viewRoot = 'admin.core.user_log';
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->view('index');
    }
	
	/**
     * Resource Listing Ajax Page.
     * 
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request)
    {
        $query = Log::take($request->length)->skip($request->start);
        
        $logs = $query->latest()->get();

        $data = [];
        foreach ($logs as $key => $log) {
            $data[] = [
                'id' => $log->id,
                'order' => $key + 1,
                'title' => $log->module->title,
                'user' => $log->user->first_name." ".$log->user->last_name,
                'item' => $log->title,
                'action' => $log->type,
                'date' => $log->created_at->format("jS M Y"),
            ];            
        }
        
        return [
            'recordsTotal' => Log::count(),
            'recordsFiltered' => Log::count(),
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
