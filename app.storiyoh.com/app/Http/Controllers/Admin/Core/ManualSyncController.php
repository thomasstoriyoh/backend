<?php

namespace App\Http\Controllers\Admin\Core;

use App\Jobs\ManualSyncJob;
use App\Models\ManualSync;
use App\Traits\CommonModuleMethods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManualSyncController extends Controller
{
    use CommonModuleMethods;

    /**
     * @var string
     */
    protected $viewRoot = 'admin.core.manual-sync';

    /**
     * @var array
     */
    protected $models = [
        'Category', 'Network', 'Show', 'Episode'
    ];

    /**
     * @var array
     */
    protected $commands = [
        "Category" => "sync:categories",
        "Network" => "sync:networks",
        "Show" => "sync:shows",
        "Episode" => "sync:episodes",
    ];

    /**
     * @return mixed
     */
    public function index()
    {
        $items = collect($this->models)->map(function ($item) {
            return $this->getModelInstance($item);
        });

        return $this->view('index', compact('items'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request)
    {
        $validator = $this->getValidatorObject($request);
        if ($validator->fails()) {
            return ['status' => false, 'message' => $validator->getMessageBag()->first()];
        }

        $item = $this->getModelInstance($request->type);

        if ($item->availableQuota() < 1) {
            return ['status' => false, 'message' => $request->type . " sync quota is already full."];
        }

        if ($item->isInProgress()) {
            return ['status' => false, 'message' => $request->type . " already in progress."];
        }

        dispatch(new ManualSyncJob(class_basename($item)));

        $this->deductQuota($item);

        return ['status' => true, 'message' => $request->type . ' sync started Successfully.'];
    }

    /**
     * @return array
     */
    public function publish()
    {
        $status = collect($this->models)->map(function ($item) {
            return $this->getModelInstance($item)->isInProgress();
        });
        $counts = collect($this->models)->map(function ($item) {
            return $this->getModelInstance($item)->availableQuota();
        });

        $quotas = collect($this->models)->combine($counts);
        $phases = collect($this->models)->combine($status);

        return ['status' => true, 'data' => compact('phases', 'quotas')];
    }

    /**
     * @param string
     * @return mixed
     */
    protected function getModelInstance($type)
    {
        $name = 'App\Models\\' . $type;

        return new $name;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorObject(Request $request)
    {
        return validator($request->all(), [
            'type' => 'required|in:' . implode(",", $this->models)
        ]);
    }

    /**
     * @param $item
     */
    protected function deductQuota($item)
    {
        $obj = ManualSync::firstOrNew([
            'type' => class_basename($item),
            'sync_date' => @date('Y-m-d')
        ]);

        $obj->fill(['sync_count' => intval($obj->sync_count) + 1])->save();

        return $item->availableQuota();
    }
}
