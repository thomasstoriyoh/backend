<?php

namespace App\Http\Controllers\Admin\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\MarketplaceCountry;
use App\Traits\CommonModuleMethods;
use Auth;
use Validator;
use App\Models\MarketplaceCurrency;
use App\Models\Ip2LocationCountry;

class MarketplaceCountryController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.core.marketplace_country';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();        
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = MarketplaceCountry::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');                
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            if($request->columns[$order['column']]['data'] == "order") {
                continue;
            }
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'currency_id' => empty($item->currency_id) ? 'Not Assigned' : $item->currency->title,
                'markup_price' => $item->markup_price,
                'commission_price' => $item->commission_price,
                'tax_name' => $item->tax_name,
                'tax_price' => $item->tax_price,
                'status' => $item->status,
            ];
            if(Auth::guard('admin')->user()->can('edit.marketplace-currencies')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\Core\MarketplaceCountryController@edit', $item->id) .'">Edit</a></li>
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param MarketplaceCountry $marketplace-currencies
     * @return \Illuminate\Http\Response
     */
    public function create(MarketplaceCountry $marketplace_country) {
        $currency_list = MarketplaceCurrency::published()->approved()->pluck('title', 'id')->all();
        
        $already_taken_country = MarketplaceCountry::pluck('country_alpha2_code')->all();        
        $countries = Ip2LocationCountry::whereNotIn('country_alpha2_code', $already_taken_country)->get(['country_name', 'country_alpha2_code']);
        $country_list = [];
        foreach($countries as $item) {
            $country_list[$item->country_name."###".$item->country_alpha2_code] = $item->country_name;
        }
                
        return $this->view('form', compact('marketplace_country', 'currency_list', 'country_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['title', 'currency_id', 'markup_price', 'commission_price', 'tax_name', 'tax_price']);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        list($country_name, $country_short) = explode("###", $request->title);
        
        $attributes['title'] = $country_name;
        $attributes['country_alpha2_code'] = $country_short;

        MarketplaceCountry::create($attributes);

        return redirect()->action('Admin\Core\MarketplaceCountryController@index')
            ->with('success', 'Country Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MarketplaceCountry $marketplace_country
     * @return mixed
     */
    public function edit(MarketplaceCountry $marketplace_country) { 

        $currency_list = MarketplaceCurrency::published()->approved()->pluck('title', 'id')->all();
        $country_list = [];

        //$already_taken_country = MarketplaceCountry::where('country_alpha2_code', '!=', $marketplace_country->country_alpha2_code)->pluck('country_alpha2_code')->all();
        //$countries = Ip2LocationCountry::whereNotIn('country_alpha2_code', $already_taken_country)->get(['country_name', 'country_alpha2_code']);        
        // foreach($countries as $item) {
        //     $country_list[$item->country_name."###".$item->country_alpha2_code] = $item->country_name;
        // }
        return $this->view('form', compact('marketplace_country', 'currency_list', 'country_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MarketplaceCountry $marketplace_country
     * @return \Illuminate\Http\Response
     * @internal param MarketplaceCountry $marketplace_country
     */
    public function update(Request $request, MarketplaceCountry $marketplace_country)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($marketplace_country), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        //dd($request->all());
        $attributes = $request->only(['title', 'currency_id', 'markup_price', 'commission_price', 'tax_name', 'tax_price']);

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        $marketplace_country->fill($attributes);
        $marketplace_country->save();

        return redirect()->action('Admin\Core\MarketplaceCountryController@index')
            ->with('success', 'Country Edited Successfully.');
    }
    
    /**
     * Published action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = MarketplaceCountry::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = MarketplaceCountry::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = MarketplaceCountry::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = MarketplaceCountry::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */	
    public function destroy($id)
    {
        $item = MarketplaceCountry::findOrFail($id);        

        $item->delete();
        
        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            //'title' => ['required']
        ];
    }
}
