<?php

namespace App\Http\Controllers\Admin\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Traits\CommonModuleMethods;
use Auth;
use Validator;
use App\Models\MarketplaceCurrency;
use App\Models\MarketplaceCountry;

class MarketplaceCurrencyController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.core.marketplace_currency';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();        
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = MarketplaceCurrency::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');                
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            if($request->columns[$order['column']]['data'] == "order") {
                continue;
            }
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'status' => $item->status,
            ];
            if(Auth::guard('admin')->user()->can('edit.marketplace-currencies')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\Core\MarketplaceCurrencyController@edit', $item->id) .'">Edit</a></li>
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param MarketplaceCurrency $marketplace-currencies
     * @return \Illuminate\Http\Response
     */
    public function create(MarketplaceCurrency $marketplace_currency) {
        return $this->view('form', compact('marketplace_currency'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['title']);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        MarketplaceCurrency::create($attributes);

        return redirect()->action('Admin\Core\MarketplaceCurrencyController@index')
            ->with('success', 'Currency Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MarketplaceCurrency $marketplace_currency
     * @return mixed
     */
    public function edit(MarketplaceCurrency $marketplace_currency) { 
        return $this->view('form', compact('marketplace_currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MarketplaceCurrency $marketplace_currency
     * @return \Illuminate\Http\Response
     * @internal param MarketplaceCurrency $marketplace_currency
     */
    public function update(Request $request, MarketplaceCurrency $marketplace_currency)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($marketplace_currency), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['title']);

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        $marketplace_currency->fill($attributes);
        $marketplace_currency->save();

        return redirect()->action('Admin\Core\MarketplaceCurrencyController@index')
            ->with('success', 'Currency Edited Successfully.');
    }
    
    /**
     * Published action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = MarketplaceCurrency::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = MarketplaceCurrency::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = MarketplaceCurrency::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = MarketplaceCurrency::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */	
    public function destroy($id)
    {
        $item = MarketplaceCurrency::findOrFail($id);
        
        $check_country = MarketplaceCountry::where('currency_id', $id);
        
        if($check_country->count() > 0) {
            $check_country->update(['currency_id' => 0]);
        }

        $item->delete();
        
        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required']
        ];
    }
}
