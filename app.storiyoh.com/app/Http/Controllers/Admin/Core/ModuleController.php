<?php

namespace App\Http\Controllers\Admin\Core;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;
use App\Models\Admin\Module;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;

class ModuleController extends Controller
{
    use CommonModuleMethods;
    
    protected $viewRoot = 'admin.core.module';
    
    public function __construct() {
        $this->middleware('auth.admin:admin');
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::guard('admin')->user()->type != "SA") {
            return redirect("/catalyst");
        }
        $modules = Module::paginate(15);
        return $this->view('index', compact('modules'));
    }
    
    /**
     * Settings Page for maker cheker.
     */
    public function settings() {
        $modules = Module::published()
                ->where(function($query) {
                    $query->whereType('Module')->orWhere('slug', 'admin');
                })
                ->where('has_maker_checker', true)
                ->where('controller', '!=', '')
                ->orderBy('order')->latest()
                ->get();
        return $this->view('settings', compact('modules'));
    }

    /**
     * Save the changed settings.
     *
     * @param Request $request
     * @return mixed
     */
    public function postSettings(Request $request) {
        $modules = Module::published()
                ->where(function($query) {
                    $query->whereType('Module')->orWhere('slug', 'admin');
                })
                ->where('controller', '!=', '')
                ->get(['id', 'model', 'slug', 'type']);
        
        $maker_checker_on = array_filter($request->maker_checker);
        foreach ($modules as $module) {
            if (in_array($module->id, $maker_checker_on)) {
                $module->maker_checker = 'On';
            } else {
                $module->maker_checker = 'Off';

                $model = "App\\Models\\" . $module->model;
                if ($module->slug == 'admin') {
                    $model = "App\\Models\\Admin\\" . $module->model;
                }
                
                app($model)->update(['admin_status' => 'Approved']);
            }

            $module->widget = false;
            if ($request->has('widget_modules')) {
                if(in_array($module->id, $request->widget_modules)) {
                    $module->widget = true;
                }
            }

            $module->save();
        }
        return redirect()->back()->with('success', 'Settings updated successfully.');
    }

    /**
     * Resource Listing Ajax Page.
     * 
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request)
    {
        $query = Module::take($request->length)->skip($request->start);
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $query->orderBy('order');
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $modules = $query->latest()->get();

        $data = [];
        foreach ($modules as $key => $module) {
            //dump($module);
            $data[] = [
                'id' => $module->id,
                'order' => $key + 1,
                'title' => $module->title,
                'slug' => $module->slug,
                'parent' => empty($module->parent->title) ? 'Self' : $module->parent->title,
                'model' => $module->model ? $module->model : 'None',
                'controller' => $module->controller ? $module->controller : 'None',
                'status' => $module->status,
                'options' => '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\Core\ModuleController@edit', $module->id) .'"><i class="glyph-icon icon-edit"></i> Edit</a></li>
                        </ul>
                    </div>
                </div>',
            ];            
        }
        
        return [
            'recordsTotal' => Module::count(),
            'recordsFiltered' => Module::count(),
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Module $module)
    {
        $modules = Module::all();
        return $this->view('form', compact('module', 'modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->all();
        $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';
        $attributes['nested'] = $request->has('nested') ? 1 : 0;
        // $attributes['maker_checker'] = $request->has_maker_checker ? 'On' : 'Off';
        $attributes['maker_checker'] = 'Off';
        
        //dd($attributes);
        
        Module::create($attributes);
                
        if ($request->nested) {
            Artisan::call('make:migration', [
                "name" => "create_" . $request->table_name . "_table",
                "--create" => $request->table_name
            ]);
            Artisan::call('make:model', [
                "name" => "Models/" . $request->model
            ]);
            Artisan::call('make:controller', [
                "name" => "Admin/" . $request->controller,
            ]);
        }
        
        return redirect()->action('Admin\Core\ModuleController@index')
                ->with('success', 'Module Created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        $modules = Module::all();
        return $this->view("form", compact("module", "modules"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($module), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->all();
        $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';
        
        $module->fill($attributes);
        $module->save();
        
        return redirect()->action('Admin\Core\ModuleController@index')
                ->with('success', 'Module Edited Successfully.');
    }
    
    /**
     * Publish action.
     * 
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {
        
        foreach ($request->ids as $id) {
            $module = Module::findOrFail($id);
            $module->status = "Published";
            $module->save();
        }
        
        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        
        foreach ($request->ids as $id) {
            $module = Module::findOrFail($id);
            $module->status = "Draft";
            $module->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Reorder resources.
     * 
     */
    public function reorder() {
        $modules = Module::published()->whereModuleId(0)->orderBy('order')->latest()->get(['id', 'title', 'module_id']);
        
        return $this->view('reorder', compact('modules'));
    }
    
    /**
     * Save the ordered sequence in database.
     * 
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $module = Module::findOrFail($id);
            $module->order = $order + 1;
            $module->save();
        }
        
        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {
        
        //return $request->all();
        
        foreach ($request->ids as $id) {
            $this->destroy($id);
        }
        
        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy($id)
    {
        $module = Module::findOrFail($id);
        
        if ($module->child->count() > 0) {
            foreach ($module->child as $child) {
                $child->module_id = $module->module_id;
                $child->save();
            }
        }
        
        $module->delete();
        
        return ['status' => true];
    }

    /**
     * This is the validation messges.
     * 
     */
    public function validatorMessages() {
        return [
            
        ];
    }

    /**
     * Store action rules.
     */
    public function storeActionRules() {
        return [
            'title' => ['required', 'unique:modules', 'min:3', 'max:50', 'regex:/^[a-zA-z\s\&\/]+$/'],
            'slug' => ['required', 'unique:modules', 'min:3', 'max:50', 'regex:/^[a-zA-z\-]+$/'],
            'type' => ['required'],
            'table_name' => ['required_with:nested', 'unique:modules'],
            'model' => ['required_with:nested', 'unique:modules'],
            'controller' => ['required_with:nested', 'unique:modules'],
        ];
    }

    /**
     * Update action rules.
     */
    public function updateActionRules($module) {
        return [
            'title' => ['required', 'unique:modules,id,' . $module->id, 'min:3', 'max:50', 'regex:/^[a-zA-z\s\&\/]+$/'],
            'slug' => ['required', 'unique:modules,id,' . $module->id, 'min:3', 'max:50', 'regex:/^[a-zA-z\-]+$/'],
            'type' => ['required'],
        ];
    }

}
