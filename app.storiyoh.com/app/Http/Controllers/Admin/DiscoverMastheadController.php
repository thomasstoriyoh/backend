<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;

use App\Traits\CommonModuleMethods;
use Auth;
use Intervention\Image\Facades\Image;
use Validator;

use Carbon\Carbon;

use App\Models\DiscoverMasthead;

use App\Models\Episode;
use App\Models\Playlist;
use App\Models\SmartPlaylist;
use App\Models\Chart;
use App\Models\Show;

class DiscoverMastheadController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.discover_masthead';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
    	$module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = DiscoverMasthead::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
//                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        // foreach ($request->order as $order) {
        //     $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        // }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'status' => $item->status,
                
            ];
            if(Auth::guard('admin')->user()->can('edit.discover-masthead')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\DiscoverMastheadController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }


    public function create(DiscoverMasthead $discovermasthead)
    {
        $podcastArray = [];
        $episode = [];
        $playlist = [];
        $smartplaylist = [];
        $collection = [];
        return $this->view('form', compact('discovermasthead', 'podcastArray', 'episode', 'playlist', 'smartplaylist', 'collection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {   
        // dd($request->all());
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title']);

        // extra
        $content_type = $request->type;
        if ($content_type == "Podcast") {
            if ($request->episode_id != '') {
                $attributes['type'] = "Episode";
                $attributes['content_id'] = $request->episode_id;
            }
            else {
                $attributes['type'] = $content_type;
                $attributes['content_id'] = $request->show_id;
            }
        }

        elseif ($content_type == "Playlist") {
            $attributes['type'] = $content_type;
            $attributes['content_id'] = $request->playlist_id;
        }

        elseif ($content_type == "Smart Playlist") {
            $attributes['type'] = $content_type;
            $attributes['content_id'] = $request->smart_playlist_id;
        }

        elseif ($content_type == "Collection") {
            $attributes['type'] = $content_type;
            $attributes['content_id'] = $request->collection_id;
        }

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        // dd($attributes);

        DiscoverMasthead::create($attributes);
        return redirect()->action('Admin\DiscoverMastheadController@index')
            ->with('success', 'Discover Masthead Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Banner $banner
     * @return mixed
     */

    public function edit(DiscoverMasthead $discovermasthead)
    {
        // dd($discovermasthead);

        $podcastArray = [];
        $episode = [];
        $playlist = [];
        $smartplaylist = [];
        $collection = [];


        if ($discovermasthead->type == 'Podcast') {
            $show = Show::where('id', $discovermasthead->content_id)->first();
            $podcastArray = [$show->id => $show->title];
        }

        elseif ($discovermasthead->type == 'Playlist') {
            $query = Playlist::where('id', $discovermasthead->content_id)->first();
            $playlist = [$query->id => $query->title];
        }

        elseif ($discovermasthead->type == 'Smart Playlist') {
            $query = SmartPlaylist::where('id', $discovermasthead->content_id)->first();
            $smartplaylist = [$query->id => $query->title];
        }

        elseif ($discovermasthead->type == 'Collection') {
            $query = Chart::where('id', $discovermasthead->content_id)->first();
            $collection = [$query->id => $query->title];
        }

        elseif ($discovermasthead->type == 'Episode') {
           $episode = Episode::where('id', $discovermasthead->content_id)->first();
           $show = Show::where('id', $episode->show_id)->first();
           $podcastArray = [$show->id => $show->title];
           $episode = [$episode->id => $episode->title];
           $discovermasthead['type'] = 'Podcast';
        }

        // dd($discovermasthead->type);

        // dd(key($episode));
        // if ($factoid->episode_id != 0) {
        //     $episodedata = Episode::where('id', $factoid->episode_id)->first();
        //     $show = Show::where('id', $episodedata->show_id)->first();
        //     $podcastArray = [$show->id => $show->title];
        //     $episode = [$episodedata->id => $episodedata->title];
        // }

        return $this->view('form', compact('discovermasthead', 'podcastArray', 'episode', 'playlist', 'smartplaylist', 'collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Banner $ExplicitContent
     * @return \Illuminate\Http\Response
     * @internal param ExplicitContent $ExplicitContent
     */

    public function update(Request $request, DiscoverMasthead $discovermasthead)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), $this->storeActionRules($discovermasthead), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['title']);

        $content_type = $request->type;
        if ($content_type == "Podcast") {
            if ($request->episode_id != '') {
                $attributes['type'] = "Episode";
                $attributes['content_id'] = $request->episode_id;
            }
            else {
                $attributes['type'] = $content_type;
                $attributes['content_id'] = $request->show_id;
            }
        }

        elseif ($content_type == "Playlist") {
            $attributes['type'] = $content_type;
            $attributes['content_id'] = $request->playlist_id;
        }

        elseif ($content_type == "Smart Playlist") {
            $attributes['type'] = $content_type;
            $attributes['content_id'] = $request->smart_playlist_id;
        }

        elseif ($content_type == "Collection") {
            $attributes['type'] = $content_type;
            $attributes['content_id'] = $request->collection_id;
        }

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        $discovermasthead->fill($attributes);
        $discovermasthead->save();
        
        return redirect()->action('Admin\DiscoverMastheadController@index')
            ->with('success', 'Discover Masthead Edited Successfully.');
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

     /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            // 'date_appear' => ['required']
        ];
    }

    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = DiscoverMasthead::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = DiscoverMasthead::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */

    public function delete(Request $request)
    {
    	foreach ($request->ids as $id) {
            $this->destroy($id);
        }
        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */	
    public function destroy($id)
    {
        $item = DiscoverMasthead::findOrFail($id);
        $item->delete();

        return true;
    }

    
    // search shows
    public function search_movies(Request $request)
    {
        $query = Show::Published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            $query->orWhere('itunes_id', 'LIKE', '%'. $request->q .'%');
        });

        $showData = $query->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No show found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    //search episode
    public function search_episode(Request $request)
    {
        $query = Episode::Where('show_id', $request->select_type)->Published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            $query->orWhere('itunes_id', 'LIKE', '%'. $request->q .'%');
        });

        $showData = $query->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Episode found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }
    //Search playlist
    public function search_playlist(Request $request)
    {
        $query = Playlist::where('private', 'N');

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            // $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Playlist found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    // search_smart_playlist
    public function search_smart_playlist(Request $request)
    {
        $query = SmartPlaylist::where('private', 'N');

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            // $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Smart Playlist found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    // search_collection
    public function search_collection(Request $request)
    {
        $query = Chart::published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            // $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No collection found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }
}
