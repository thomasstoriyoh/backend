<?php

namespace App\Http\Controllers\Admin\Documentation;

use App\Models\Admin\DocumentationCategory;
use App\Traits\CommonModuleMethods;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DocumentationCategoryController extends Controller
{
    /**
     * Common module methods to use.
     *
     */
    use CommonModuleMethods;

    /**
     * Root for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.documentation.category';

    /**
     * Listing page.
     *
     * @return \App\Traits\type
     */
    public function index() {
        return $this->view('index');
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return type
     */
    public function ajaxIndex(Request $request)
    {
        $query = DocumentationCategory::take($request->length)->skip($request->start);
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $query->orderBy('order');
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $data[] = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'slug' => $item->slug,
                'parent' => empty($item->parent->title) ? 'Root' : $item->parent->title,
                'status' => $item->status,
                'options' => '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\Documentation\DocumentationCategoryController@edit', $item->id) .'"><i class="glyph-icon icon-edit"></i> Edit</a></li>
                            </ul>
                    </div>
                </div>',
            ];
        }

        return [
            'recordsTotal' => DocumentationCategory::count(),
            'recordsFiltered' => DocumentationCategory::count(),
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param DocumentationCategory $category
     * @return \Illuminate\Http\Response
     */
    public function create(DocumentationCategory $category)
    {
        $categories = DocumentationCategory::lists('title', 'id')->toArray();
        return $this->view('form', compact('category', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->all();
        $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';
        DocumentationCategory::create($attributes);

        return redirect()->action('Admin\Documentation\DocumentationCategoryController@index')
            ->with('success', 'Documentation Category Created Successfully.');
    }

    /**
     * Show the form for editing a resource.
     *
     * @param DocumentationCategory $category
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentationCategory $category)
    {
        $categories = DocumentationCategory::where('id', '!=', $category->id)->lists('title', 'id')->toArray();
        return $this->view('form', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param DocumentationCategory $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentationCategory $category)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($category), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->all();
        $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';

        $category->fill($attributes);
        $category->save();

        return redirect()->action('Admin\Documentation\DocumentationCategoryController@index')
            ->with('success', 'Documentation Category Edited Successfully.');
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = DocumentationCategory::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = DocumentationCategory::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = DocumentationCategory::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = DocumentationCategory::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = DocumentationCategory::findOrFail($id);
        $item->delete();

        if ($item->child->count() > 0) {
            $item->child()->update(['documentation_category_id' => $item->documentation_category_id]);
        }

        return true;
    }

    /**
     * Validation rules for storing.
     *
     * @return array
     */
    private function storeActionRules()
    {
        return [
            'title' => ['required'],
            'slug' => ['required', 'unique:documentation_categories']
        ];
    }

    /**
     * Validation messages if any.
     *
     * @return array
     */
    private function validatorMessages()
    {
        return [];
    }

    /**
     * Validation rules for updating.
     *
     * @param $category
     * @return array
     */
    private function updateActionRules($category)
    {
        return [
            'title' => ['required'],
            'slug' => ['required', 'unique:documentation_categories,slug,'.$category->id]
        ];
    }

}
