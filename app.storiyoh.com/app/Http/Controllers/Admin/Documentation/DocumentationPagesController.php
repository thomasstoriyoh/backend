<?php

namespace App\Http\Controllers\Admin\Documentation;

use App\Models\Admin\DocumentationCategory;
use App\Models\Admin\DocumentationPages;
use App\Traits\CommonModuleMethods;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use PhpParser\Comment\Doc;

class DocumentationPagesController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.documentation.pages';

    /**
     * The index page.
     *
     * @return View
     */
    public function index() {
        return $this->view('index');
    }

    /**
     * Resource details page.
     *
     * @param DocumentationPages $pages
     * @return View
     * @internal param Blog $blog
     */
    public function show(DocumentationPages $pages) {
        return $this->view('show', compact('pages'));
    }

    /**
     * Json for datatable.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = DocumentationPages::take($request->length)->skip($request->start);
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $query->orderBy('order');
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'category' => $item->category ? $item->category->title : 'None',
                'status' => $item->status,
            ];
            if (Auth::guard('admin')->user()->can(['edit.documentation-pages', 'view.documentation-pages'], 'OR')) {
                $options = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\Documentation\DocumentationPagesController@edit', $item->id) .'">
                                <i class="glyph-icon icon-edit"></i> Edit</a></li>
                        </ul>
                    </div>
                </div>';
                $row['options'] = $options;
            }

            $data[] = $row;
        }

        return [
            'recordsTotal' => DocumentationPages::count(),
            'recordsFiltered' => DocumentationPages::count(),
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param DocumentationPages $pages
     * @return \Illuminate\Http\Response
     * @internal param Blog $blog
     */
    public function create(DocumentationPages $pages)
    {
        $category_list = DocumentationCategory::published()->lists('title', 'id')->toArray();
        return $this->view('form', compact('pages', 'category_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title', 'documentation_category_id']);
        $attributes['description'] = $this->parseEditorBody($request->description);
        $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';

        if (! empty($request->file('attachment_file'))) {
            $file = $request->file('attachment_file');
            $filename = date('YmdHis').rand(100000, 999999) . '.' . $file->getClientOriginalExtension();
            $file->move('uploads/documentation', $filename);

            $attributes['attachment'] = $filename;
        }

        DocumentationPages::create($attributes);

        return redirect()->action('Admin\Documentation\DocumentationPagesController@index')
            ->with('success', 'Documentation Page Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param DocumentationPages $pages
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentationPages $pages)
    {
        $category_list = DocumentationCategory::published()->lists('title', 'id')->toArray();
        return $this->view('form', compact('pages', 'category_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param DocumentationPages $pages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentationPages $pages)
    {

        $validator = Validator::make($request->all(), $this->updateActionRules($pages), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title', 'documentation_category_id']);
        $attributes['description'] = $this->parseEditorBody($request->description);
        $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';

        if (! empty($request->file('attachment_file'))) {
            $file = $request->file('attachment_file');
            $filename = date('YmdHis').rand(100000, 999999) . '.' . $file->getClientOriginalExtension();
            $file->move('uploads/documentation', $filename);

            $attributes['attachment'] = $filename;

            $this->deleteImageOrFile($pages->attachment, 'uploads/documentation');
        }

        $pages->fill($attributes);
        $pages->save();

        return redirect()->action('Admin\Documentation\DocumentationPagesController@index')
            ->with('success', 'Documentation Page Edited Successfully.');
    }

    /**
     * Delete image page.
     *
     * @param Request $request
     * @param DocumentationPages $pages
     * @return array
     */
    public function deleteImage(Request $request, DocumentationPages $pages) {
        switch ($request->type) {
            case 'File':
                // File Deleting code goes here.
                $this->deleteImageOrFile($pages->attachment, 'uploads/documentation');
                $pages->attachment = "";
                $pages->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined file type to delete.'];
                break;

        }
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = DocumentationPages::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = DocumentationPages::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = DocumentationPages::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = DocumentationPages::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = DocumentationPages::findOrFail($id);
        $item->delete();

        $this->deleteImageOrFile($item->attachment, 'uploads/documentation');
        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required'],
            'documentation_category_id' => ['required'],
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required'],
            'documentation_category_id' => ['required'],
        ];
    }
}
