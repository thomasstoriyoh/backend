<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Module;

use App\Traits\CommonModuleMethods;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Validator;

use App\Models\ExplicitContent;


class ExplicitContentController extends Controller
{
    use CommonModuleMethods;


    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.explicit-content';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
    	//dd(json_decode($this->Update_json()));
        $module_details = Module::whereController(class_basename(self::class))->first();
        // update josn
        //$this->Update_json();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = ExplicitContent::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
//                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        // foreach ($request->order as $order) {
        //     $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        // }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'status' => $item->status,
                
            ];
            if(Auth::guard('admin')->user()->can('edit.explicit_content')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\ExplicitContentController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }


    public function create(ExplicitContent $explicit_content)
    {
        return $this->view('form', compact('explicit_content'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {   //dd($request->all());
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        $attributes = $request->only(['title']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        ExplicitContent::create($attributes);
        
        // update josn
        $this->Update_json();

        return redirect()->action('Admin\ExplicitContentController@index')
            ->with('success', 'Explicit Content Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Banner $banner
     * @return mixed
     */

    public function edit(ExplicitContent $explicit_content)
    {
        return $this->view('form', compact('explicit_content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Banner $ExplicitContent
     * @return \Illuminate\Http\Response
     * @internal param ExplicitContent $ExplicitContent
     */

    public function update(Request $request, ExplicitContent $explicit_content)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules($explicit_content), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        $explicit_content->fill($attributes);
        $explicit_content->save();
        
        //update josn
        $this->Update_json();

        return redirect()->action('Admin\ExplicitContentController@index')
            ->with('success', 'Explicit Content Edited Successfully.');
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

     /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3']
        ];
    }

    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = ExplicitContent::findOrFail($id);
            $this->publishModule($item);
        }

         // update josn
        $this->Update_json();

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = ExplicitContent::findOrFail($id);
            $this->unpublishModule($item);
        }

         // update josn
        $this->Update_json();

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */

    public function delete(Request $request)
    {
    	foreach ($request->ids as $id) {
            $this->destroy($id);
        }
         // update josn
        $this->Update_json();
        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */	
    public function destroy($id)
    {
        $item = ExplicitContent::findOrFail($id);
        $item->delete();

        return true;
    }

    // Udpate json file
    public function Update_json()
    {
    	$query = ExplicitContent::select('*')->published()->pluck('title');
        $json_data = json_encode($query);
        
        $myFile = public_path("/uploads/explicit_content.json");
        file_put_contents($myFile, $json_data);

    	// $myFile = storage_path('app/JSON/explicit_content.json');
    	// file_put_contents($myFile, $json_data);
    }
}
