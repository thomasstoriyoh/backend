<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;

use App\Traits\CommonModuleMethods;
use Auth;
use Intervention\Image\Facades\Image;
use Validator;

use Carbon\Carbon;

use App\Models\Factoid;
use App\Models\Show;
use App\Models\Episode;

class FactoidsController extends Controller
{
    use CommonModuleMethods;


    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.factoids';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Factoid::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
//                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        // foreach ($request->order as $order) {
        //     $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        // }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'date_appear' => Carbon::parse($item->date_appear)->format('d/m/Y'),
                'status' => $item->status,
                
            ];
            if(Auth::guard('admin')->user()->can('edit.factoids')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\FactoidsController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }


    public function create(Factoid $factoid)
    {
        $podcastArray = [];
        $episode = [];
        return $this->view('form', compact('factoid', 'podcastArray', 'episode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {   
        // dd($request->all());
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $query = Factoid::where('date_appear', $request->date_appear)->first();
        if ($query) {
            return redirect()->back()->with('error', 'Factoid can not be added same Date')->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title', 'episode_id', 'date_appear']);

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        Factoid::create($attributes);
        return redirect()->action('Admin\FactoidsController@index')
            ->with('success', 'Factoids Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Banner $banner
     * @return mixed
     */

    public function edit(Factoid $factoid, Request $request)
    {
        // dd($factoid->episode_id);
        $podcastArray = [];
        $episode = [];

        if ($factoid->episode_id != 0) {
            $episodedata = Episode::where('id', $factoid->episode_id)->first();
            $show = Show::where('id', $episodedata->show_id)->first();
            $podcastArray = [$show->id => $show->title];
            $episode = [$episodedata->id => $episodedata->title];
        }

        return $this->view('form', compact('factoid', 'podcastArray', 'episode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Banner $ExplicitContent
     * @return \Illuminate\Http\Response
     * @internal param ExplicitContent $ExplicitContent
     */

    public function update(Request $request, Factoid $factoid)
    {
        // dd($request->episode_id);
        $validator = Validator::make($request->all(), $this->storeActionRules($factoid), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $query = Factoid::where('date_appear', $request->date_appear)->where('id', '!=',$factoid->id)->first();
        if ($query) {
            return redirect()->back()->with('error', 'Factoid can not be added same Date')->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title', 'date_appear']);

        if ($request->episode_id == '') {
            $attributes['episode_id'] = 0;
        }
        else{
            $attributes['episode_id'] = $request->episode_id;
        }

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        $factoid->fill($attributes);
        $factoid->save();
        
        return redirect()->action('Admin\FactoidsController@index')
            ->with('success', 'Factoids Edited Successfully.');
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

     /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            'date_appear' => ['required']
        ];
    }

    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = Factoid::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Factoid::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */

    public function delete(Request $request)
    {
        foreach ($request->ids as $id) {
            $this->destroy($id);
        }
        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */ 
    public function destroy($id)
    {
        $item = Factoid::findOrFail($id);
        $item->delete();

        return true;
    }

    
    // search shows
    public function search_movies(Request $request)
    {
        $query = Show::Published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            $query->orWhere('itunes_id', 'LIKE', '%'. $request->q .'%');
        });

        $showData = $query->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No show found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    //search episode
    public function search_episode(Request $request)
    {
        $query = Episode::Where('show_id', $request->select_type)->Published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            $query->orWhere('itunes_id', 'LIKE', '%'. $request->q .'%');
        });

        $showData = $query->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Episode found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }
}
