<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;

use App\Traits\CommonModuleMethods;
use Auth;
use Validator;

use App\Models\HighlightedEpisode;
use App\Models\Episode;
use App\Models\Show;
use App\Models\EpisodeDetail;

class HighlightedEpisodeController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.highlighted_episodes';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request)
    {
        $query = HighlightedEpisode::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function ($query) use ($request) {
                $query->orWhere('title', 'LIKE', '%' . $request->search['value'] . '%');
                $query->orWhere('status', 'LIKE', '%' . $request->search['value'] . '%');
            });
        }

        if (!empty($request->custom_filter)) {
            $query->where(function ($query) use ($request) {
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();
        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->episode->title,
                'type' => $item->episode->episode_extra_info->content_type == 1 ? "Series Episode" : "Standalone Episode",
                'status' => $item->status,

            ];
            // if (Auth::guard('admin')->user()->can('edit.promotion')) {
            //     $row['options'] = '<div class="dropdown">
            //         <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
            //             <span class="glyph-icon icon-align-justify"></span>
            //             <i class="glyph-icon icon-angle-down"></i>
            //         </a>
            //         <div class="dropdown-menu float-right">
            //                 <ul class="reset-ul mrg5B">
            //                     <li><a href="' . action('Admin\HighlightedEpisodeController@edit', $item->id) . '">Edit</a></li>
            //                 </ul>
            //         </div>
            //     </div>';
            // }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * 
     */
    public function create(HighlightedEpisode $highlighted_episode)
    {
        $podcastArray = $episode = $standalone = [];
        return $this->view('form', compact('highlighted_episode', 'podcastArray', 'episode', 'standalone'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $content_type = $request->type;
        if ($content_type == "Podcast") {
            $attributes['episode_id'] = $request->episode_id;
        } elseif ($content_type == "Standalone") {
            $attributes['episode_id'] = $request->standaloneepisode_id;
        }

        $attributes['status'] = !empty($request->status) ? 'Published' : 'Draft';

        //$attributes['created_by'] = Auth::guard('admin')->user()->id;
        //$attributes['updated_by'] = Auth::guard('admin')->user()->id;

        HighlightedEpisode::create($attributes);

        return redirect()->action('Admin\HighlightedEpisodeController@index')
            ->with('success', 'Episode Added Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param HighlightedEpisode $highlighted_episode
     * @return mixed
     */
    public function edit(HighlightedEpisode $highlighted_episode)
    {
        // $podcastArray = $episode = $standalone = [];
        // if ($highlighted_episode->type == 'Podcast') {
        //     $show = Show::where('id', $highlighted_episode->content_id)->first();
        //     $podcastArray = [$show->id => $show->title];
        // } elseif ($highlighted_episode->type == 'Episode') {
        //     $episode = Episode::where('id', $highlighted_episode->content_id)->first();
        //     $show = Show::where('id', $episode->show_id)->first();
        //     $podcastArray = [$show->id => $show->title];
        //     $episode = [$episode->id => $episode->title];
        //     $highlighted_episode['type'] = 'Podcast';
        // } elseif ($highlighted_episode->type == 'Standalone') {
        //     $query = EpisodeDetail::where('episode_id', $highlighted_episode->content_id)->first();
        //     $standalone = [$query->episode_id => $query->title];
        // }

        // return $this->view('form', compact('highlighted_episode', 'podcastArray', 'episode', 'standalone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param HighlightedEpisode $highlighted_episode
     * @return \Illuminate\Http\Response
     * @internal param ExplicitContent $ExplicitContent
     */

    public function update(Request $request, HighlightedEpisode $highlighted_episode)
    {
        // $validator = Validator::make($request->all(), $this->storeActionRules($highlighted_episode), $this->validatorMessages());
        // if ($validator->fails()) {
        //     return redirect()->back()->withInput($request->all())->withErrors($validator);
        // }

        // $attributes = $request->only(['title']);

        // $content_type = $request->type;
        // if ($content_type == "Podcast") {
        //     if ($request->episode_id != '') {
        //         $attributes['type'] = "Episode";
        //         $attributes['content_id'] = $request->episode_id;
        //     } else {
        //         $attributes['type'] = $content_type;
        //         $attributes['content_id'] = $request->show_id;
        //     }
        // } elseif ($content_type == "Standalone") {
        //     $attributes['type'] = $content_type;
        //     $attributes['content_id'] = $request->standaloneepisode_id;
        // }

        // $attributes['status'] = !empty($request->status) ? 'Published' : 'Draft';

        // $attributes['image'] = !empty($request->images) ? $request->images[0] : null;

        // $highlighted_episode->fill($attributes);

        // $highlighted_episode->save();

        // return redirect()->action('Admin\HighlightedEpisodeController@index')
        //     ->with('success', 'Promotion Edited Successfully.');
    }   

    /**
     * This function is use for getting
     */
    public function search_movies(Request $request)
    {
        $showData = Show::whereIn('content_type', config('config.show_content_type'))
            ->where('title', 'LIKE', '%' . $request->q . '%')->published()->get(['title', 'id']);

        $arrayData = [];
        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No show found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    /**
     * This function is use for getting episode
     */
    public function search_episode(Request $request)
    {
        $episode_data = Episode::where('show_id', $request->select_type)
            ->where('title', 'LIKE', '%' . $request->q . '%')->published()->get(['title', 'id']);

        $arrayData = [];
        if ($episode_data->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Episode found.'];
        }

        foreach ($episode_data as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    /**
     * This function is getting statndalone episode
     */
    public function search_standalone_episode(Request $request)
    {
        $episode_data = EpisodeDetail::Where('title', 'LIKE', '%' . $request->q . '%')
            ->where('content_type', 2)
            ->where('status', "Published")->get(['id', 'title', 'episode_id']);

        $arrayData = [];
        if ($episode_data->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Episode found.'];
        }

        foreach ($episode_data as $item) {
            $arrayData[] = ['id' => $item->episode_id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages()
    {
        return [];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules()
    {
        return [
            'type' => ['required']
        ];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder()
    {
        $items = HighlightedEpisode::published()->orderBy('order')->latest()->get(['id', 'episode_id']);
        $data = [];
        foreach($items as $item) {
            $data[] = [
                'id' => $item->id,
                'title' => $item->episode->title
            ];
        }

        $items = $data;        

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request)
    {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = HighlightedEpisode::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if ($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    public function publish(Request $request)
    {

        foreach ($request->ids as $id) {
            $item = HighlightedEpisode::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request)
    {
        foreach ($request->ids as $id) {
            $item = HighlightedEpisode::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */

    public function delete(Request $request)
    {
        foreach ($request->ids as $id) {
            $this->destroy($id);
        }
        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = HighlightedEpisode::findOrFail($id);        

        $item->delete();

        return true;
    }
}
