<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\ItunesTopPodcast;
use App\Traits\CommonModuleMethods;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\Country;

class ItunesTopPodcastController extends Controller
{
    use CommonModuleMethods;
    
    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.itunes-top-podcast';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        
        $query = ItunesTopPodcast::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query) use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }
        
        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();        
        
        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            if($request->columns[$order['column']]['data'] == 'order') {
                continue;
            }
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        
        $items = $query->orderBy('title', 'ASC')->get();

        
        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'status' => $item->status
            ];
            if(Auth::guard('admin')->user()->can('edit.podcast-of-the-day')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\ItunesTopPodcastController@edit', $item->id) .'">Edit</a></li>                            
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ItunesTopPodcast $itunes_top_podcast
     * @return \Illuminate\Http\Response
     */
    public function create(ItunesTopPodcast $itunes_top_podcast)
    {
        $alreadyCountry = ItunesTopPodcast::pluck('title')->all();
        $countryArray = Country::whereNotIn('title', $alreadyCountry)->orderBy('title', 'ASC')->pluck('title', 'title')->all();        
        return $this->view('form', compact('itunes_top_podcast', 'countryArray'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['title', 'rss_url']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        $result = ItunesTopPodcast::create($attributes);

        return redirect()->action('Admin\ItunesTopPodcastController@index')
            ->with('success', 'Itunes Top Podcast Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ItunesTopPodcast $itunes_top_podcast
     * @return mixed
     */
    public function edit(ItunesTopPodcast $itunes_top_podcast)
    {        
        $countryArray = Country::orderBy('title', 'ASC')->pluck('title', 'title')->all(); 
        return $this->view('form', compact('itunes_top_podcast', 'countryArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ItunesTopPodcast $itunes_top_podcast
     * @return \Illuminate\Http\Response
     * @internal param ItunesTopPodcast $itunes_top_podcast
     */
    public function update(Request $request, ItunesTopPodcast $itunes_top_podcast)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($itunes_top_podcast), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title', 'rss_url']);        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        $itunes_top_podcast->fill($attributes)->save();        
                        
        return redirect()->action('Admin\ItunesTopPodcastController@index')
            ->with('success', 'Itunes Top Podcast Edited Successfully.');
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {
        foreach ($request->ids as $id) {
            $item = ItunesTopPodcast::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = ItunesTopPodcast::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }       

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = ItunesTopPodcast::findOrFail($id);
        $item->delete();

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required'],
            'rss_url' => ['required'],
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required'],
            'rss_url' => ['required'],
        ];
    }
}
