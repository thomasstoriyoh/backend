<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;

use App\Traits\CommonModuleMethods;
use Auth;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Support\Facades\Storage;

use App\Models\MarketplacePromotion;
use App\Models\Episode;
use App\Models\Show;
use App\Models\EpisodeDetail;

class MarketplacePromotionController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.marketplace_promotions';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
    	$module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = MarketplacePromotion::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();
        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'image' => ! empty($item->image) ? '<img src="'.$item->getImage(100).'" />' : "No Image",
                'type' => $item->type,
                'status' => $item->status,
                
            ];
            if(Auth::guard('admin')->user()->can('edit.promotion')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\MarketplacePromotionController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * 
     */
    public function create(MarketplacePromotion $marketplace_promotion) {
        $podcastArray = $episode = $standalone = [];        
        return $this->view('form', compact('marketplace_promotion', 'podcastArray', 'episode', 'standalone'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title']);

        $content_type = $request->type;
        if ($content_type == "Podcast") {
            if ($request->episode_id != '') {
                $attributes['type'] = "Episode";
                $attributes['content_id'] = $request->episode_id;
            } else {
                $attributes['type'] = $content_type;
                $attributes['content_id'] = $request->show_id;
            }
        } elseif ($content_type == "Standalone") {
            $attributes['type'] = $content_type;
            $attributes['content_id'] = $request->standaloneepisode_id;
        }

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        
        $attributes['image'] = !empty($request->images) ? $request->images[0] : null;

        MarketplacePromotion::create($attributes);

        return redirect()->action('Admin\MarketplacePromotionController@index')
            ->with('success', 'Promotion Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MarketplacePromotion $marketplace_promotion
     * @return mixed
     */
    public function edit(MarketplacePromotion $marketplace_promotion)
    {
        $podcastArray = $episode = $standalone = [];        
        if ($marketplace_promotion->type == 'Podcast') {
            $show = Show::where('id', $marketplace_promotion->content_id)->first();
            $podcastArray = [$show->id => $show->title];
        } elseif ($marketplace_promotion->type == 'Episode') {
           $episode = Episode::where('id', $marketplace_promotion->content_id)->first();
           $show = Show::where('id', $episode->show_id)->first();
           $podcastArray = [$show->id => $show->title];
           $episode = [$episode->id => $episode->title];
           $marketplace_promotion['type'] = 'Podcast';
        } elseif ($marketplace_promotion->type == 'Standalone') {
            $query = EpisodeDetail::where('episode_id', $marketplace_promotion->content_id)->first();
            $standalone = [$query->episode_id => $query->title];
        }

        return $this->view('form', compact('marketplace_promotion', 'podcastArray', 'episode', 'standalone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MarketplacePromotion $marketplace_promotion
     * @return \Illuminate\Http\Response
     * @internal param ExplicitContent $ExplicitContent
     */

    public function update(Request $request, MarketplacePromotion $marketplace_promotion)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules($marketplace_promotion), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['title']);

        $content_type = $request->type;
        if ($content_type == "Podcast") {
            if ($request->episode_id != '') {
                $attributes['type'] = "Episode";
                $attributes['content_id'] = $request->episode_id;
            }
            else {
                $attributes['type'] = $content_type;
                $attributes['content_id'] = $request->show_id;
            }
        } elseif ($content_type == "Standalone") {
            $attributes['type'] = $content_type;
            $attributes['content_id'] = $request->standaloneepisode_id;
        }

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        if (!empty($request->images)) {
            $attributes['image'] = $request->images[0];
        }
        
        $marketplace_promotion->fill($attributes);

        $marketplace_promotion->save();
        
        return redirect()->action('Admin\MarketplacePromotionController@index')
            ->with('success', 'Promotion Edited Successfully.');
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return CommonModuleMethods::imageUploadonS3($request, 'marketplace_promo');
    }            

    /**
     * Delete image page.
     *
     * @param Request $request
     * @param Banner $banner
     * @return array
     */
    public function deleteImage(Request $request, MarketplacePromotion $marketplace_promotion) {
        switch ($request->type) {
            case 'Cover':
                if (Storage::disk('s3')->exists('marketplace_promo/' . $marketplace_promotion->image)) {
                    Storage::disk('s3')->delete('marketplace_promo/' . $marketplace_promotion->image);
                }

                $marketplace_promotion->image = "";
                $marketplace_promotion->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined image type to delete.'];
                break;

        }
    }

    /**
     * This function is use for getting
     */
    public function search_movies(Request $request)
    {
        $showData = Show::where('content_type', 1)->where('title', 'LIKE', '%'. $request->q .'%')
            ->published()->get(['title', 'id']);
        
        $arrayData = [];
        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No show found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    /**
     * This function is use for getting episode
     */
    public function search_episode(Request $request)
    {
        $episode_data = Episode::where('show_id', $request->select_type)
            ->where('title', 'LIKE', '%'. $request->q .'%')->get(['title', 'id']);

        $arrayData = [];
        if ($episode_data->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Episode found.'];
        }

        foreach ($episode_data as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    /**
     * This function is getting statndalone episode
     */
    public function search_standalone_episode(Request $request)
    {
        $episode_data = EpisodeDetail::Where('title', 'LIKE', '%'. $request->q .'%')
            ->where('content_type', 2)->where('status', "Published")->get(['id', 'title', 'episode_id']);

        $arrayData = [];
        if ($episode_data->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Episode found.'];
        }

        foreach ($episode_data as $item) {
            $arrayData[] = ['id' => $item->episode_id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

     /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            'type' => ['required']
        ];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = MarketplacePromotion::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = MarketplacePromotion::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = MarketplacePromotion::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = MarketplacePromotion::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */

    public function delete(Request $request)
    {
    	foreach ($request->ids as $id) {
            $this->destroy($id);
        }
        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */	
    public function destroy($id)
    {
        $item = MarketplacePromotion::findOrFail($id);

        if (!empty($item->image)) {
            if (Storage::disk('s3')->exists('marketplace_promo/' . $item->image)) {
                Storage::disk('s3')->delete('marketplace_promo/' . $item->image);
            }                        
        }

        $item->delete();

        return true;
    }
}
