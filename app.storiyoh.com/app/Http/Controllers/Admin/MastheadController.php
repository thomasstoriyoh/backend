<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Module;
use App\Models\Masthead;
use App\Traits\CommonModuleMethods;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Validator;

use App\Models\Show;
use App\Models\Episode;
use App\Models\Playlist;
use App\Models\SmartPlaylist;
use App\Models\Chart;
use Illuminate\Support\Facades\Storage;

class MastheadController extends Controller
{
    use CommonModuleMethods;


    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.masthead';

    /**
     * @var string
     */
    protected $upload_folder_name = "uploads/masthead";

    /**
     * Helper for cover image dimension.
     *
     * @var array
     */
    protected $coverDimension = [
        ['width' => 414, 'height' => 210]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Masthead::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
//                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'image' => empty($item->image) ? '' :
                    '<img src="'. $item->getImage(320, 210) .'" width="100px">',
                'status' => $item->status,
                'type' => $item->type,
                'content_type' => $item->content_type,
            ];
            if(Auth::guard('admin')->user()->can('edit.masthead')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\MastheadController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Masthead $masthead
     * @return \Illuminate\Http\Response
     */
    public function create(Masthead $masthead)
    {
        $podcastArray = [];
        $episode = [];
        $playlist = [];
        $smartplaylist = [];
        $collection = [];
        return $this->view('form', compact('masthead', 'podcastArray', 'episode', 'playlist', 'smartplaylist', 'collection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title', 'link', 'type']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        $attributes['premium'] = 0;

        if ($request->type == "External") {
            $attributes['content_type'] = null;
            $attributes['content_id'] = 0;
        } elseif ($request->type == "App Page") {
            $attributes['link'] = null;
            // extra
            $content_type = $request->content_type;
            if ($content_type == "Podcast") {
                if ($request->episode_id != '') {
                    $attributes['content_type'] = "Episode";
                    $attributes['content_id'] = $request->episode_id;
                    //check if this is premium content
                    $check = \App\Models\EpisodeDetail::where('episode_id', $request->episode_id)->first(['id', 'content_type']);
                    if (!is_null($check)) {
                        $attributes['premium'] = 1;
                    }
                } else {
                    $attributes['content_type'] = $content_type;
                    $attributes['content_id'] = $request->show_id;
                    //check if this is premium content
                    $check = Show::where('id', $request->show_id)->first(['id', 'content_type']);
                    if ($check->content_type != 0) {
                        $attributes['premium'] = 1;
                    }
                }
            } else if ($content_type == "Playlist") {
                $attributes['content_type'] = $content_type;
                $attributes['content_id'] = $request->playlist_id;
            } else if ($content_type == "Smart Playlist") {
                $attributes['content_type'] = $content_type;
                $attributes['content_id'] = $request->smart_playlist_id;
            } else if ($content_type == "Collection") {
                $attributes['content_type'] = $content_type;
                $attributes['content_id'] = $request->collection_id;
            }
        }
        
        if ($request->has('images')) {
            $attributes['image'] = $request->images[0];

            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/masthead/' . $filename);            
            $image_name = "masthead/" . $filename;
            $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

            //delete image from local
            unlink(public_path('uploads/masthead/' . $filename));
            unlink(public_path('uploads/masthead/thumbs/414_210_' . $filename));
        }

        Masthead::create($attributes);

        return redirect()->action('Admin\MastheadController@index')
            ->with('success', 'Masthead Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Masthead $masthead
     * @return mixed
     */
    public function edit(Masthead $masthead)
    {
         $podcastArray = [];
         $episode = [];
         $playlist = [];
         $smartplaylist = [];
         $collection = [];
          
         if ($masthead->content_type == 'Podcast') {
            $show = Show::where('id', $masthead->content_id)->first();
            $podcastArray = [$show->id => $show->title];
        }
        else if ($masthead->content_type == 'Episode') {
            $episode = Episode::where('id', $masthead->content_id)->first();
            $show = Show::where('id', $episode->show_id)->first();
            $podcastArray = [$show->id => $show->title];
            $episode = [$episode->id => $episode->title];
            $masthead['content_type'] = 'Podcast';
        }
        else if ($masthead->content_type == 'Playlist') {
            $query = Playlist::where('id', $masthead->content_id)->first();
            $playlist = [$query->id => $query->title];
        }
        else if ($masthead->content_type == 'Smart Playlist') {
            $query = SmartPlaylist::where('id', $masthead->content_id)->first();
            $smartplaylist = [$query->id => $query->title];
        }
        else if ($masthead->content_type == 'Collection') {
            $query = Chart::where('id', $masthead->content_id)->first();
            $collection = [$query->id => $query->title];
        }
 
        return $this->view('form', compact('masthead', 'podcastArray', 'episode', 'playlist', 'smartplaylist', 'collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Masthead $masthead
     * @return \Illuminate\Http\Response
     * @internal param Masthead $masthead
     */
    public function update(Request $request, Masthead $masthead)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($masthead), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title', 'link', 'type']);

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        $attributes['premium'] = 0;

        if ($request->type == "External") {
            $attributes['content_type'] = null;
            $attributes['content_id'] = 0;
        } else if ($request->type == "App Page") {
            $attributes['link'] = null;
            // extra
            $content_type = $request->content_type;
            if ($content_type == "Podcast") {
                if ($request->episode_id != '') {
                    $attributes['content_type'] = "Episode";
                    $attributes['content_id'] = $request->episode_id;
                    //check if this is premium content
                    $check = \App\Models\EpisodeDetail::where('episode_id', $request->episode_id)->first(['id', 'content_type']);
                    if (!is_null($check)) {
                        $attributes['premium'] = 1;
                    }
                } else {
                    $attributes['content_type'] = $content_type;
                    $attributes['content_id'] = $request->show_id;
                    //check if this is premium content
                    $check = Show::where('id', $request->show_id)->first(['id', 'content_type']);
                    if ($check->content_type != 0) {
                        $attributes['premium'] = 1;
                    }
                }
            } else if ($content_type == "Playlist") {
                $attributes['content_type'] = $content_type;
                $attributes['content_id'] = $request->playlist_id;
            } else if ($content_type == "Smart Playlist") {
                $attributes['content_type'] = $content_type;
                $attributes['content_id'] = $request->smart_playlist_id;
            } else if ($content_type == "Collection") {
                $attributes['content_type'] = $content_type;
                $attributes['content_id'] = $request->collection_id;
            }
        }

        if ($request->has('images')) {
            
            $attributes['image'] = $request->images[0];

            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/masthead/' . $filename);            
            $image_name = "masthead/" . $filename;
            $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

            //delete image from local
            unlink(public_path('uploads/masthead/' . $filename));
            unlink(public_path('uploads/masthead/thumbs/414_210_' . $filename));

            //delete old image
            // if(! empty($masthead->image)) {
            //     if (Storage::disk('s3')->exists('masthead/' . $masthead->image)) {
            //         Storage::disk('s3')->delete('masthead/' . $masthead->image);
            //     }
            // }
        } 

        $masthead->fill($attributes)->save();        

        return redirect()->action('Admin\MastheadController@index')
            ->with('success', 'Masthead Edited Successfully.');
    }

    /**
     * Update the image crop.
     *
     * @param $image
     * @param $chords
     * @return bool
     */
    protected function updateCropImage($image, $chords) {
        Image::make('uploads/masthead/' . $image)->crop((int) $chords->w, (int) $chords->h, (int) $chords->x, (int) $chords->y)
            ->fit((int) $chords->width, (int) $chords->height)
            ->save('uploads/masthead/thumbs/'. $chords->width ."_" . $chords->height . "_" . $image);
        return true;
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $request->resource == 'Image' ? $this->imageUpload($request) : $this->fileUpload($request);
    }

    /**
     * For uploading images...
     *
     * @param Request $request
     * @return array
     */
    protected function imageUpload(Request $request) {
        $files = $request->file;

        $name_array = explode(".", $request->name);
        array_pop($name_array);
        $name = implode(".", $name_array);

        $uploaded_files = [];
        $this->forceDimension = [];

        if (empty($request->$name)) {
            $this->forceDimension = $this->coverDimension;
        }else{
            $chords = (object) $request->$name;
            $this->crop_chords = $chords;
        }

        if (is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleImageWithChords($file, $filename, $this->upload_folder_name, "thumbs");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleImageWithChords($files, $filename, $this->upload_folder_name, "thumbs");
        }

        return $uploaded_files;
    }

    /**
     * For uploading files...
     *
     * @param Request $request
     * @return array
     */
    protected function fileUpload(Request $request) {
        $files = $request->file;

        $uploaded_files = [];
        if(is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleFile($file, $filename, "uploads/masthead/files");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleFile($files, $filename, "uploads/masthead/files");
        }

        return $uploaded_files;
    }

    /**
     * Delete image page.
     *
     * @param Request $request
     * @param Masthead $masthead
     * @return array
     */
    public function deleteImage(Request $request, Masthead $masthead) {
        switch ($request->type) {
            case 'Cover':
                if (Storage::disk('s3')->exists('masthead/' . $masthead->image)) {
                    Storage::disk('s3')->delete('masthead/' . $masthead->image);
                }
                $masthead->image = "";
                $masthead->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined image type to delete.'];
                break;

        }
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = Masthead::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Masthead::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = Masthead::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = Masthead::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = Masthead::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = Masthead::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = Masthead::findOrFail($id);

        if (!empty($item->image)) {
            if (Storage::disk('s3')->exists('masthead/' . $item->image)) {
                Storage::disk('s3')->delete('masthead/' . $item->image);
            }                        
        }

        $item->delete();

        // $this->forceDimension = $this->coverDimension;
        // $this->deleteImageOrFile($item->image, $this->upload_folder_name, "thumbs");

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required'],
            // 'link' => ['required'],
            'type' => ['required'],
            'images' => ['required']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required'],
            //'link' => ['required'],
            'type' => ['required'],
        ];
    }

    //search episode
    public function search_episode(Request $request)
    {
        $query = Episode::Where('show_id', $request->select_type)->Published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            // $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No episode found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    //Search playlist
    public function search_playlist(Request $request)
    {
        $query = Playlist::where('private', 'N');

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            // $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Playlist found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    // search_smart_playlist
    public function search_smart_playlist(Request $request)
    {
        $query = SmartPlaylist::where('private', 'N');

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            // $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No Smart Playlist found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    // search_collection
    public function search_collection(Request $request)
    {
        $query = Chart::published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            // $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No collection found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }
}
