<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\Network;
use App\Traits\CommonModuleMethods;
use Intervention\Image\Facades\Image;
use Validator;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\Show;

class NetworkController extends Controller
{
    use CommonModuleMethods;

    /**
     * @var string
     */
    protected $upload_folder_name = "uploads/networks";
    
    /**
     * Helper for cover image dimension.
     *
     * @var array
     */
    protected $coverDimension = [
        ['width' => 600, 'height' => 600]
    ];
    
    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.network';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Network::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->withCount('shows')->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'image' => empty($item->image) ? '' :
                    '<img src="' . $item->getImage(150) . '" width="150px" style="box-shadow:0 10px 16px 0 #CCC">',
                'featured' => $item->featured ? "Yes" : "No", 
                'number_of_podcast' => $item->shows_count,
                'status' => $item->status
            ];
            if(Auth::guard('admin')->user()->can('edit.networks')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\NetworkController@edit', $item->id) .'">Edit</a></li>
                            <li><a href="'. action('Admin\NetworkController@addedPodcast', $item->id) .'">Add Podcasts</a></li>
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Network $network
     * @return \Illuminate\Http\Response
     */
    public function create(Network $network)
    {
        return $this->view('form', compact('network'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['title']);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        if ($request->has('images')) {

            $attributes['image'] = $request->images[0];

            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/networks/' . $filename);            
            $image_name = "networks/" . $filename;
            $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

            //delete image from local
            unlink(public_path('uploads/networks/' . $filename));
            unlink(public_path('uploads/networks/thumbs/600_600_' . $filename));
        }
        
        $result = Network::create($attributes);

        return redirect()->action('Admin\NetworkController@index')
            ->with('success', 'Network Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Network $network
     * @return mixed
     */
    public function edit(Network $network)
    {
        return $this->view('form', compact('network'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Network $network
     * @return \Illuminate\Http\Response
     * @internal param Network $network
     */
    public function update(Request $request, Network $network)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($network), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title']);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        if ($request->has('images')) {
            
            $attributes['image'] = $request->images[0];

            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/networks/' . $filename);            
            $image_name = "networks/" . $filename;
            $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

            //delete image from local
            unlink(public_path('uploads/networks/' . $filename));
            unlink(public_path('uploads/networks/thumbs/600_600_' . $filename));

            //delete old image
            // if(! empty($network->image)) {
            //     if (Storage::disk('s3')->exists('networks/' . $network->image)) {
            //         Storage::disk('s3')->delete('networks/' . $network->image);
            //     }
            // }
        }        
        
        $network->fill($attributes)->save();
        
        return redirect()->action('Admin\NetworkController@index')
            ->with('success', 'Network Edited Successfully.');
    }
    
    /**
     * 
     * view page for added to podcast
     * @param Network $network
     * @return void
     */
    public function addedPodcast(Network $network)
    {
        $podcastArray = $network->shows()->pluck('title', 'id')->all();
        return $this->view('podcast_form', compact('network', 'podcastArray'));
    }

    /**
     * update network after submit form
     *
     * @param Request $request
     * @param Network $network
     * @return void
     */
    public function postUpdatePodcast(Request $request, Network $network) {

        if(! $request->has('show_id') && ! $request->has('data_csv')) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'Please select atleast one podcast.']);
        }

        //If csv found
        $showIds = [];
        $csv_flag = true;
        if($request->has('data_csv')) {
            $csv_flag = false;
            $file = public_path('uploads/networks/' . $request->data_csv[0]);
            $all_values = [];
            $handle = fopen($file, 'r');            
            while (!feof($handle)) {
                $model = fgetcsv($handle);
                if (is_array($model)) {
                    $showIds[] = $model[0];
                }
            }
            array_filter($showIds);

            if(count($showIds) > 0) {
                $sync_array = [];
                foreach ($showIds as $key => $item) {
                    $showData = Show::where('itunes_id', $item)->first(['id', 'title']);
                    if ($showData) {
                        $showData->update(['network_id' => $network->id]);                        
                    }
                }
            }
            unlink(public_path('uploads/networks/' . $request->data_csv[0]));
        }
        
        if ($request->has('show_id') && $csv_flag) {
            $oldIds = $network->shows()->pluck('id')->all();
            $neyIds = $request->show_id;
            $diff = array_diff($oldIds, $neyIds);
            
            foreach ($request->show_id as $key => $show_id) {
                $showData = Show::find($show_id);
                if($showData) {
                    $showData->update(['network_id' => $network->id]);
                }
            }

            if(count($diff) > 0) {
                Show::whereIn('id', $diff)->update(['network_id' => 0]);                
            }
        }
               
        return redirect()->action('Admin\NetworkController@index')
            ->with('success', 'Podcasts added successfully.');
    }

    /**
     * Fetured action.
     * 
     * @param Request $request
     * @return array
     */
    public function featured(Request $request) {
        
        $total = Network::featured()->count();
        $featured_count = Network::featured()->whereNotIn('id', $request->ids)->count();
        if (($featured_count + count($request->ids)) > 10) {
            return ['status' => false, 
                'message' => 'You cannot feature more than 10 items. You already have ' . $total 
                    . ' items in your featured list.'];
        }
        
        foreach ($request->ids as $id) {
            $item = Network::findOrFail($id);
            $item->setEventType('Featured');
            $item->featured = true;
            $item->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Unfetured action.
     * 
     * @param Request $request
     * @return array
     */
    public function unfeatured(Request $request) {        
        foreach ($request->ids as $id) {
            $item = Network::findOrFail($id);
            $item->setEventType('Unfeatured');
            $item->featured = false;
            $item->save();
        }
        
        return ['status' => true];
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $request->resource == 'Image' ? $this->imageUpload($request) : $this->fileUpload($request);
    }

    /**
     * For uploading images...
     *
     * @param Request $request
     * @return array
     */
    protected function imageUpload(Request $request) {
        
        $files = $request->file;

        $name_array = explode(".", $request->name);
        array_pop($name_array);
        $name = implode(".", $name_array);

        $uploaded_files = [];
        $this->forceDimension = [];

        if (empty($request->$name)) {
            $this->forceDimension = $this->coverDimension;
        }else{
            $chords = (object) $request->$name;
            $this->crop_chords = $chords;
        }

        if (is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleImageWithChords($file, $filename, $this->upload_folder_name, "thumbs");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleImageWithChords($files, $filename, $this->upload_folder_name, "thumbs");
        }

        return $uploaded_files;
    }

    /**
     * CSV upload.
     *
     * @param Request $request
     * @return array
     */
    protected function fileUpload(Request $request) {
        
        $files = $request->file;
        
        $uploaded_files = [];
        if(is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleFile($file, $filename, "uploads/networks");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleFile($files, $filename, "uploads/networks");
        }
        
        return $uploaded_files;
    }

     /**
     * Delete image page.
     *
     * @param Request $request
     * @param Masthead $masthead
     * @return array
     */
    public function deleteImage(Request $request, Network $network) {
        switch ($request->type) {
            case 'Cover':                
                if (Storage::disk('s3')->exists('networks/' . $network->image)) {
                    Storage::disk('s3')->delete('networks/' . $network->image);
                }

                $network->image = "";
                $network->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined image type to delete.'];
                break;
        }
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {
        foreach ($request->ids as $id) {
            $item = Network::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Network::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }    

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = Network::published()->orderBy('order')->latest()->get(['id', 'title']);
        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = Network::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }    
       
    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = Network::findOrFail($id);        

        if (!empty($item->image)) {
            if (Storage::disk('s3')->exists('networks/' . $item->image)) {
                Storage::disk('s3')->delete('networks/' . $item->image);
            }                        
        }

        $item->delete();

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3']      
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required', 'min:3']            
        ];
    }
}
