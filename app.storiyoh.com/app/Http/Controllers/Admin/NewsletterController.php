<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Traits\CommonModuleMethods;
use App\Models\Admin\Module;
use App\Models\Newsletter;
use App\Models\Show;
use App\Models\Chart;
use App\Models\Episode;
use App\Models\Subscriber;
use App\Models\User;
use App\Jobs\SubscribeNewsJob;

use App\Models\CronNewsletter;

use Validator;
use Auth;
use App\Traits\Helper;
use App\Models\TestSubscriber;

class NewsletterController extends Controller
{
	use CommonModuleMethods;


    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.newsletter';

    /**
     * @var string
     */
    protected $upload_folder_name = "uploads/newsletter";

    /**
     * Helper for cover image dimension.
     *
     * @var array
     */
    protected $coverDimension = [
        ['width' => 640, 'height' => 480],
        ['width' => 320, 'height' => 150]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }


    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Newsletter::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('newsletter_title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'newsletter_title' => $item->newsletter_title,
                'image' => empty($item->image) ? '' :
                    '<img src="'. $item->getImage(320, 150) .'" width="100px">',
                'status' => $item->status,
                'admin_status' => $item->admin_status,
            ];
            if(Auth::guard('admin')->user()->can('edit.newsletter')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\NewsletterController@edit', $item->id) .'">Edit</a></li>
                                <li><a href="'. action('Admin\NewsletterController@notify', $item->id) .'">Notify</a></li>
                                <li><a href="'. action('Admin\NewsletterController@testnotify', $item->id) .'">Test Notify</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Newsletter $newsletter)
    {
        $podcastArray = [];
        $chartsArray = [];
        $episodesArray = [];
        $showArray = [];
        $essentialsArray = [];
        $podcastweekArray = [];

        return $this->view('form', compact('newsletter','podcastArray','chartsArray','episodesArray','showArray','essentialsArray','podcastweekArray'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->all();
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        if ($request->has('images')) {
            $attributes['image'] = $request->images[0];
        }
        
        Newsletter::create($attributes);

        return redirect()->action('Admin\NewsletterController@index')
            ->with('success', 'Newsletter Created Successfully.');
    }



    /**
     * Display the specified resource.
     *
     * @param  \Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show(Newsletter $newsletter)
    {
        $newsletter['trending_podcasts'] = Show::whereIn('id',$newsletter->trending_podcasts)->get();
        $newsletter['featured_collections'] = Chart::whereIn('id',$newsletter->featured_collections)->get();
        $newsletter['featured_episodes'] = Episode::whereIn('id',$newsletter->featured_episodes)->get();   
        $newsletter['essentials'] = Show::whereIn('id',$newsletter->essentials)->get();
        $newsletter['podcast_of_the_week'] = Show::where('id',$newsletter->podcast_of_the_week)->first();

        return view('emails.newsletter');
        //return $this->view('show',compact('newsletter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function edit(Newsletter $newsletter)
    {

        $podcastArray = Show::whereIn('id',$newsletter->trending_podcasts)->pluck('title','id')->toArray();
        $chartsArray = Chart::whereIn('id',$newsletter->featured_collections)->pluck('title','id')->toArray();
        $episodesArray = Episode::whereIn('id',$newsletter->featured_episodes)->pluck('title','id')->toArray();
        $essentialsArray = Show::whereIn('id',$newsletter->essentials)->pluck('title','id')->toArray();
        $podcastweekArray = Show::where('id',$newsletter->podcast_of_the_week)->pluck('title','id')->toArray();

        $showArray = [];

        return $this->view('form', compact('newsletter','podcastArray','chartsArray','episodesArray','showArray','essentialsArray','podcastweekArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Newsletter $newsletter)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($newsletter), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->all();

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        if ($request->has('images')) {
            $attributes['image'] = $request->images[0];
        }

        $newsletter->fill($attributes);
        $newsletter->save();


        if ($request->has('edit_chords')) {
            foreach ($request->edit_chords as $edit_chord) {
                $this->updateCropImage($newsletter->image, json_decode($edit_chord));
            }
        }

        return redirect()->action('Admin\NewsletterController@index')
            ->with('success', 'Newsletter Edited Successfully.');
    }


        /**
     * Update the image crop.
     *
     * @param $image
     * @param $chords
     * @return bool
     */
    protected function updateCropImage($image, $chords) {
        Image::make('uploads/newsletter/' . $image)->crop((int) $chords->w, (int) $chords->h, (int) $chords->x, (int) $chords->y)
            ->fit((int) $chords->width, (int) $chords->height)
            ->save('uploads/newsletter/thumbs/'. $chords->width ."_" . $chords->height . "_" . $image);
        return true;
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $request->resource == 'Image' ? $this->imageUpload($request) : $this->fileUpload($request);
    }

    /**
     * For uploading images...
     *
     * @param Request $request
     * @return array
     */
    protected function imageUpload(Request $request) {
        $files = $request->file;

        $name_array = explode(".", $request->name);
        array_pop($name_array);
        $name = implode(".", $name_array);

        $uploaded_files = [];
        $this->forceDimension = [];

        if (empty($request->$name)) {
            $this->forceDimension = $this->coverDimension;
        }else{
            $chords = (object) $request->$name;
            $this->crop_chords = $chords;
        }

        if (is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleImageWithChords($file, $filename, $this->upload_folder_name, "thumbs");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleImageWithChords($files, $filename, $this->upload_folder_name, "thumbs");
        }

        return $uploaded_files;
    }

    /**
     * For uploading files...
     *
     * @param Request $request
     * @return array
     */
    protected function fileUpload(Request $request) {
        $files = $request->file;

        $uploaded_files = [];
        if(is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleFile($file, $filename, "uploads/newsletter/files");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleFile($files, $filename, "uploads/newsletter/files");
        }

        return $uploaded_files;
    }

    /**
     * Delete image page.
     *
     * @param Request $request
     * @param Newsletter $newsletter
     * @return array
     */
    public function deleteImage(Request $request, Newsletter $newsletter) {
        switch ($request->type) {
            case 'Cover':
                $this->forceDimension = $this->coverDimension;
                $this->deleteImageOrFile($newsletter->image, $this->upload_folder_name, "thumbs");
                $newsletter->image = "";
                $newsletter->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined image type to delete.'];
                break;

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function notify(Newsletter $newsletter)
    {
        return $this->view('notifyform', compact('newsletter'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function postnotify(Request $request, Newsletter $newsletter)
    {
        $data  = $request->all();

        $attribute = new CronNewsletter;
        $attribute['newsletter_id'] = $newsletter->id;
        $attribute['user_type'] = $request->user_type;
        $attribute['newsletter_type'] = 'Live';
        $attribute['subject'] = $request->subject;
        $attribute['from_name'] = $request->from_name;
        $attribute['from_email'] = $request->from_email;
        $attribute['status'] = '0';
        $attribute->save();
       
        return redirect()->action('Admin\NewsletterController@index')
            ->with('success', 'Newsletter Send Successfully.');

    }


        /**
     * Display the specified resource.
     *
     * @param  \Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function testnotify(Newsletter $newsletter)
    {
        return $this->view('testnotifyform', compact('newsletter'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function posttestnotify(Request $request, Newsletter $newsletter)
    {
        $data  = $request->all();

        $attribute = new CronNewsletter;
        $attribute['newsletter_id'] = $newsletter->id;
        $attribute['user_type'] = $request->user_type;
        $attribute['newsletter_type'] = 'Test';
        $attribute['subject'] = $request->subject;
        $attribute['from_name'] = $request->from_name;
        $attribute['from_email'] = $request->from_email;
        $attribute['status'] = '0';
        $attribute->save();
       
        return redirect()->action('Admin\NewsletterController@index')
            ->with('success', 'Newsletter Send Successfully.');

    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = Newsletter::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Newsletter::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = Newsletter::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = Newsletter::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = Newsletter::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = Newsletter::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

   /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = Newsletter::findOrFail($id);
        $item->delete();

        $this->forceDimension = $this->coverDimension;
        $this->deleteImageOrFile($item->image, $this->upload_folder_name, "thumbs");

        return true;
    }





    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            'description' => ['required'],
        ];
    }


    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required', 'min:3'],
            'description' => ['required'],
        ];
    }


    /**
     *
     * @param Request $request
     * @return type
     */
    public function search_movies(Request $request)
    {
        $query = Show::Published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No show found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }


    /**
     *
     * @param Request $request
     * @return type
     */
    public function search_charts(Request $request)
    {
        $query = Chart::Published();

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%'. $request->q .'%');
            //$query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No chart found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }


    /**
     *
     * @param Request $request
     * @return type
     */
    public function search_episodes(Request $request)
    {
        $query = Episode::Published();

        $query->where(function ($query) use ($request) {
            $query->whereIn('show_id', $request->showid);
        });

        $showData = $query->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No episode found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    
}
