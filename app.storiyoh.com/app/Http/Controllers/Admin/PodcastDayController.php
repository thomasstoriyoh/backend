<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\PodcastDay;
use App\Traits\CommonModuleMethods;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\Show;

class PodcastDayController extends Controller
{
    use CommonModuleMethods;
    
    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.podcast-day';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        
        $query = PodcastDay::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query) use ($request) {
                //$query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                //$query->orWhere('keywords', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }
        
        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();        
        
        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            if($request->columns[$order['column']]['data'] == 'order') {
                continue;
            }
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        
        $items = $query->latest()->get();

        

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'image' => empty($item->show->image) ? '' :
                    '<img src="' . $item->show->getImage(150) . '" width="150px" style="box-shadow:0 10px 16px 0 #CCC">',
                'date' => Carbon::parse($item->date)->format('d/m/Y'),
                'status' => $item->status
            ];
            if(Auth::guard('admin')->user()->can('edit.podcast-of-the-day')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="'. action('Admin\PodcastDayController@edit', $item->id) .'">Edit</a></li>                            
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PodcastDay $podcast_of_the_day
     * @return \Illuminate\Http\Response
     */
    public function create(PodcastDay $podcast_of_the_day)
    {
        $podcastArray = [];
        return $this->view('form', compact('podcast_of_the_day', 'podcastArray'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['show_id', 'date']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        $podcast_title = Show::find($request->show_id, ['title']);
        $attributes['title'] = $podcast_title->title;        

        $result = PodcastDay::create($attributes);

        return redirect()->action('Admin\PodcastDayController@index')
            ->with('success', 'Podcast of the Day Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PodcastDay $podcast_of_the_day
     * @return mixed
     */
    public function edit(PodcastDay $podcast_of_the_day)
    {        
        $podcastArray = $podcast_of_the_day->show()->pluck('title', 'id')->all();   
        return $this->view('form', compact('podcast_of_the_day', 'podcastArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param PodcastDay $podcast_of_the_day
     * @return \Illuminate\Http\Response
     * @internal param PodcastDay $podcast_of_the_day
     */
    public function update(Request $request, PodcastDay $podcast_of_the_day)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($podcast_of_the_day), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['show_id', 'date']);        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        $podcast_of_the_day->fill($attributes)->save();

        $podcast_title = Show::find($request->show_id, ['title']);
        $podcast_of_the_day->fill(['title' => $podcast_title->title])->save();
                        
        return redirect()->action('Admin\PodcastDayController@index')
            ->with('success', 'Podcast of the Day Edited Successfully.');
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {
        foreach ($request->ids as $id) {
            $item = PodcastDay::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = PodcastDay::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }       

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = PodcastDay::findOrFail($id);
        $item->delete();

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'show_id' => ['required'],
            'date' => ['required', 'unique:podcast_of_the_day']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'show_id' => ['required'],
            'date' => ['required', 'unique:podcast_of_the_day,id,' . $item->id],
        ];
    }
}
