<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Module;
use App\Models\PopularShow;
use App\Traits\CommonModuleMethods;
use Auth;
use Validator;
use App\Models\Show;
use App\Models\Category;

class PopularShowController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.popular-shows';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request)
    {
        $query = PopularShow::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function ($query) use ($request) {
                //$query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                //$query->orWhere('keywords', 'LIKE', '%'. $request->search['value'] .'%');
                //$query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (!empty($request->custom_filter)) {
            $query->where(function ($query) use ($request) {
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode('.', $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            if ($request->columns[$order['column']]['data'] == 'order') {
                continue;
            }
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }

        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'no_of_podcast' => $item->shows()->count()
            ];
            if (Auth::guard('admin')->user()->can('edit.popular-show')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="' . action('Admin\PopularShowController@edit', $item->id) . '">Edit</a></li>            
                            <li><a href="' . action('Admin\PopularShowController@reorder_podcast', $item->id) . '">Reorder Podcast</a></li>
                        </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PopularShow $popular_show
     * @return \Illuminate\Http\Response
     */
    public function create(PopularShow $popular_show)
    {
        $podcastArray = [];
        $all_cat = PopularShow::pluck('category_id')->all();

        $category_list_array = Category::published()->whereNotIn('id', $all_cat)
            ->orderBy('title', 'ASC')->pluck('title', 'id')->toArray();

        $category_list = ['' => 'Select...'] + $category_list_array;

        return $this->view('form', compact('popular_show', 'podcastArray', 'category_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        if(! $request->has('show_id') && ! $request->has('data_csv')) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'Please select atleast one podcast.']);
        }

        $attributes = $request->only(['show_id', 'category_id']);

        $podcast_title = Category::find($request->category_id, ['title']);
        $attributes['title'] = $podcast_title->title;        
        
        $result = PopularShow::create($attributes);

        //If csv found
        $showIds = [];
        $csv_flag = true;
        if($request->has('data_csv')) {
            $csv_flag = false;
            $file = public_path('uploads/popular_shows/' . $request->data_csv[0]);
            $all_values = [];
            $handle = fopen($file, 'r');            
            while (!feof($handle)) {
                $model = fgetcsv($handle);
                if (is_array($model)) {
                    $showIds[] = $model[0];
                }
            }
            array_filter($showIds);

            if(count($showIds) > 0) {
                $sync_array = [];
                foreach ($showIds as $key => $item) {
                    $showData = Show::where('itunes_id', $item)->first(['id']);
                    if ($showData) {
                        $sync_array[] = [
                            'show_id' => $showData->id,
                            'category_id' => $request->category_id,
                            'sequence' => ($key + 1) . '.' . $result->id
                        ];
                    }
                }
                $result->shows()->sync($sync_array);            
            }
            unlink(public_path('uploads/popular_shows/' . $request->data_csv[0]));
        }
        
        if ($request->has('show_id') && $csv_flag) {
            $sync_array = [];
            foreach ($request->show_id as $key => $show_id) {
                $sync_array[] = [
                    'show_id' => $show_id,
                    'category_id' => $request->category_id,
                    'sequence' => ($key + 1) . '.' . $result->id
                ];
            }
            $result->shows()->sync($sync_array);
        }
                        
        return redirect()->action('Admin\PopularShowController@index')
            ->with('success', 'Popular Show Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PopularShow $popular_show
     * @return mixed
     */
    public function edit(PopularShow $popular_show)
    {
        $category_list = Category::where('id', $popular_show->category_id)
            ->pluck('title', 'id')->toArray();
        $podcastArray = $popular_show->shows()->orderBy('sequence')->pluck('title', 'id')->all();

        return $this->view('form', compact('popular_show', 'category_list', 'podcastArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param PopularShow $popular_show
     * @return \Illuminate\Http\Response
     * @internal param PopularShow $popular_show
     */
    public function update(Request $request, PopularShow $popular_show)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($popular_show), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        if(! $request->has('show_id') && ! $request->has('data_csv')) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'Please select atleast one podcast.']);
        }

        $attributes = $request->only(['show_id']);        

        //If csv found
        $csv_flag = true;
        $showIds = [];
        if($request->has('data_csv')) {
            $csv_flag = false;
            $file = public_path('uploads/popular_shows/' . $request->data_csv[0]);
            $all_values = [];
            $handle = fopen($file, 'r');            
            while (!feof($handle)) {
                $model = fgetcsv($handle);
                if (is_array($model)) {
                    $showIds[] = $model[0];
                }
            }

            array_filter($showIds);

            if(count($showIds) > 0) {
                $sync_array = [];
                foreach ($showIds as $key => $item) {
                    $showData = Show::where('itunes_id', $item)->first(['id']);
                    if ($showData) {
                        $sync_array[] = [
                            'show_id' => $showData->id,
                            'category_id' => $popular_show->category_id,
                            'sequence' => ($key + 1) . '.' . $popular_show->id
                        ];
                    }
                }
                $popular_show->shows()->sync($sync_array);            
            }

            unlink(public_path('uploads/popular_shows/' . $request->data_csv[0]));
        }

        //dd($request->all());

        //$podcast_title = Category::find($request->category_id, ['title']);
        //$attributes['title'] = $podcast_title->title;
        //$popular_show->fill($attributes)->save();
        
        if ($request->has('show_id') && $csv_flag) {
            $sync_array = [];
            foreach ($request->show_id as $key => $show_id) {
                $sync_array[] = [
                    'show_id' => $show_id,
                    'category_id' => $popular_show->category_id,
                    'sequence' => ($key + 1) . '.' . $popular_show->id
                ];
            }
            $popular_show->shows()->sync($sync_array);
        }
        
        return redirect()->action('Admin\PopularShowController@index')
            ->with('success', 'Popular Show Edited Successfully.');
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $this->fileUpload($request);
    }

    /**
     * Product document upload.
     *
     * @param Request $request
     * @return array
     */
    protected function fileUpload(Request $request) {
        $files = $request->file;
        
        $uploaded_files = [];
        if(is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleFile($file, $filename, "uploads/popular_shows");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleFile($files, $filename, "uploads/popular_shows");
        }
        
        return $uploaded_files;
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function search_movies(Request $request)
    {
        $query = Show::Published();

        // if($request->has('cat_id')) {
        //     $query->whereHas('categories', function ($q) use ($request) {
        //         $q->where('category_id', $request->cat_id);
        //     });
        // }

        $query->where(function ($query) use ($request) {
            $query->orWhere('title', 'LIKE', '%' . $request->q . '%');
            $query->orWhere('itunes_id', $request->q);
        });

        $showData = $query->take(20)->get(['title', 'id']);
        $arrayData = [];

        if ($showData->count() == 0) {
            $arrayData[] = ['id' => 0, 'text' => 'No show found.'];
        }

        foreach ($showData as $item) {
            $arrayData[] = ['id' => $item->id, 'text' => $item->title];
        }

        $results['results'] = $arrayData;

        return $results;
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder()
    {
        $items = PopularShow::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Reorder Podcast resources
     *
     * @param Chart $chart
     * @return void
     */
    public function reorder_podcast(PopularShow $popular_show)
    {
        $items = $popular_show->shows()->orderBy('show_popular_show.sequence')->get(['id', 'title']);

        return $this->view('reorder_podcast', compact('items', 'popular_show'));
    }

    /**
      * Save the ordered sequence in database.
      *
      * @param Request $request
      * @return array
      */
    public function postPodcastReorder(Request $request)
    {
        $sequence = $this->parseOrder($request->order);
        $popularShowData = PopularShow::findOrFail($request->popularShowId);

        foreach ($sequence as $order => $id) {
            $show_reorder = $popularShowData->shows()->find($id);
            if ($show_reorder) {
                $show_reorder->pivot->sequence = ($order + 1) . '.' . $popularShowData->id;
                $show_reorder->pivot->save();
            }
        }

        $popularShowData->setEventType('Login');
        if ($order == 0) {
            $popularShowData->setEventType('Rearranged');
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = PopularShow::findOrFail($id);
        $item->delete();

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages()
    {
        return [
        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules()
    {
        return [
            //'show_id' => ['required'],
            'category_id' => ['required', 'unique:popular_shows']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item)
    {
        return [
            //'show_id' => ['required']
        ];
    }
}
