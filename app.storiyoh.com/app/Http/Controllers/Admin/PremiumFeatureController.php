<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Module;
use App\Models\PremiumFeature;
use App\Traits\CommonModuleMethods;
use Auth;
use Validator;
use App\Models\PremiumFeatureHistoric;
use Carbon\Carbon;

class PremiumFeatureController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.premium_feature';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = PremiumFeature::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('feature_name', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();
        
        $data = [];
        foreach ($items as $key => $item) {
            $availabilityTxt = "";
            if($item->availability_ios) {
                $availabilityTxt = "IOS";
            }
            if($item->availability_android) {
                $availabilityTxt .= ", Android";
            }
            if($item->availability_web) {
                $availabilityTxt .= ", Web";
            }
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->feature_name,
                'availability' => trim($availabilityTxt, ","),
                'status' => $item->status,
                'admin_status' => $item->admin_status,
            ];
            if(Auth::guard('admin')->user()->can('edit.premium_feature')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\PremiumFeatureController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }


    public function create(PremiumFeature $premium_feature)
    {
        return $this->view('form', compact('premium_feature'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->except(['_token']);        
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        if(is_null($request->availability_ios) && is_null($request->availability_android) && is_null($request->availability_web)) {
            return redirect()->back()->withInput($request->all())->withErrors(["Please Select Alleast One Availabile Platform."]);
        }

        if(! is_null($request->availability_ios)) {
            if (is_null($request->apple_product_id)) {
                return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Apple Product ID."]);
            }
        } else {
            $attributes['apple_product_id'] = NULL;
        }

        if(! is_null($request->availability_android)) {
            if (is_null($request->price_in_usd)) {
                return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Android Price in USD"]);
            }
        } else {
            $attributes['price_in_inr'] = NULL;
            $attributes['price_in_usd'] = NULL;
            $attributes['price_in_aed'] = NULL;
        }

        if(! is_null($request->availability_web)) {
            if (is_null($request->web_price_in_usd)) {
                return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Web Price in USD"]);
            }
        } else {
            $attributes['web_price_in_inr'] = NULL;
            $attributes['web_price_in_usd'] = NULL;
            $attributes['web_price_in_aed'] = NULL;
        }

        //dd($request->all(), $request->availability_ios, $request->availability_android, $request->availability_web);
        
        // if(is_null($request->availability)) {
        //     return redirect()->back()->withInput($request->all())->withErrors(["Please Select Alleast One Availabile Platform."]);
        // }

        // if($request->availability == 'iphone') {
        //     $attributes['google_product_id'] = NULL;
        //     if(is_null($request->apple_product_id)) {
        //         return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Apple Product ID."]);
        //     }
        // } else {
        //     $attributes['apple_product_id'] = NULL;
        //     $attributes['google_product_id'] = NULL;
        //     //if (is_null($request->price_in_inr) && is_null($request->price_in_usd) && is_null($request->price_in_aed)) {
        //     if (is_null($request->price_in_usd)) {
        //         return redirect()->back()->withInput($request->all())->withErrors(["Please enter price in USD."]);
        //     }
        // }
        // if($request->availability == 'android') {
        //     $attributes['apple_product_id'] = NULL;
        //     if(is_null($request->google_product_id)) {
        //         return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Google Product ID."]);
        //     }
        // } else {
        //     $attributes['apple_product_id'] = NULL;
        //     $attributes['google_product_id'] = NULL;
        // }

        
        // if (is_null($request->price_in_inr) && is_null($request->price_in_usd) && is_null($request->price_in_aed)) {
        //     return redirect()->back()->withInput($request->all())->withErrors(["Please enter atleast one price."]);
        // }

        $premium_f_id = PremiumFeature::create($attributes);

        //Insert into premium_feature_historics table
        PremiumFeatureHistoric::create([
            "premium_feature_id" => $premium_f_id->id,
            "update_date" => Carbon::now(),
            "apple_product_id" => $attributes['apple_product_id'],
            //"google_product_id" => $attributes['google_product_id'],
            "price_in_inr" => $attributes['price_in_inr'],
            "price_in_usd" => $attributes['price_in_usd'],
            "price_in_aed" => $attributes['price_in_aed'],
            "web_price_in_inr" => $attributes['web_price_in_inr'],
            "web_price_in_usd" => $attributes['web_price_in_usd'],
            "web_price_in_aed" => $attributes['web_price_in_aed'],
            "created_by" => Auth::guard('admin')->user()->id,
            "updated_by" => Auth::guard('admin')->user()->id,
        ]);

        return redirect()->action('Admin\PremiumFeatureController@index')
            ->with('success', 'Premium Feature Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PremiumFeature $premium_feature
     * @return mixed
     */

    public function edit(PremiumFeature $premium_feature)
    {
        return $this->view('form', compact('premium_feature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param PremiumFeature $premium_feature
     * @return \Illuminate\Http\Response
     * @internal param PremiumFeature $premium_feature
     */

    public function update(Request $request, PremiumFeature $premium_feature)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($premium_feature), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->except(['_token']);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft'; 

        //dd($request->all());
        
        if(is_null($request->availability_ios) && is_null($request->availability_android) && is_null($request->availability_web)) {
            return redirect()->back()->withInput($request->all())->withErrors(["Please Select Alleast One Availabile Platform."]);
        }

        if(! is_null($request->availability_ios)) {
            if (is_null($request->apple_product_id)) {
                return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Apple Product ID."]);
            }
        } else {
            $attributes['apple_product_id'] = NULL;
        }

        if(! is_null($request->availability_android)) {
            if (is_null($request->price_in_usd)) {
                return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Android Price in USD"]);
            }
        } else {
            $attributes['price_in_inr'] = NULL;
            $attributes['price_in_usd'] = NULL;
            $attributes['price_in_aed'] = NULL;
        }

        if(! is_null($request->availability_web)) {
            if (is_null($request->web_price_in_usd)) {
                return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Web Price in USD"]);
            }
        } else {
            $attributes['web_price_in_inr'] = NULL;
            $attributes['web_price_in_usd'] = NULL;
            $attributes['web_price_in_aed'] = NULL;
        }

        $attributes['availability_ios'] = ! is_null($request->availability_ios) ? 1 : 0;
        $attributes['availability_android'] = ! is_null($request->availability_android) ? 1 : 0;
        $attributes['availability_web'] = ! is_null($request->availability_web) ? 1 : 0;

        // if($request->availability == 'iphone') {
        //     $attributes['google_product_id'] = NULL;
        //     if(is_null($request->apple_product_id)) {
        //         return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Apple Product ID."]);
        //     }
        // } else  {
        //     $attributes['apple_product_id'] = NULL;
        //     $attributes['google_product_id'] = NULL;
        //     //if (is_null($request->price_in_inr) && is_null($request->price_in_usd) && is_null($request->price_in_aed)) {
        //     if (is_null($request->price_in_usd)) {
        //         return redirect()->back()->withInput($request->all())->withErrors(["Please enter price in USD."]);
        //     }
        // }
        
        // if($request->availability == 'android') {
        //     $attributes['apple_product_id'] = NULL;
        //     if(is_null($request->google_product_id)) {
        //         return redirect()->back()->withInput($request->all())->withErrors(["Please Enter Google Product ID."]);
        //     }
        // } else {
        //     $attributes['apple_product_id'] = NULL;
        //     $attributes['google_product_id'] = NULL;
        // }        
        
        // if (is_null($request->price_in_inr) && is_null($request->price_in_usd) && is_null($request->price_in_aed)) {
        //     return redirect()->back()->withInput($request->all())->withErrors(["Please enter atleast one price."]);
        // }

        //dd($attributes);
        $premium_feature->fill($attributes)->save();
        
        //Insert into premium_feature_historics table
        PremiumFeatureHistoric::create([
            "premium_feature_id" => $premium_feature->id,
            "update_date" => Carbon::now(),
            "apple_product_id" => $attributes['apple_product_id'],
            //"google_product_id" => $attributes['google_product_id'],
            "price_in_inr" => $attributes['price_in_inr'],
            "price_in_usd" => $attributes['price_in_usd'],
            "price_in_aed" => $attributes['price_in_aed'],
            "web_price_in_inr" => $attributes['web_price_in_inr'],
            "web_price_in_usd" => $attributes['web_price_in_usd'],
            "web_price_in_aed" => $attributes['web_price_in_aed'],
            "created_by" => Auth::guard('admin')->user()->id,
            "updated_by" => Auth::guard('admin')->user()->id,
        ]);
    
        return redirect()->action('Admin\PremiumFeatureController@index')
            ->with('success', 'Ppremium Feature Edited Successfully.');
    }
    
    /**
     * Published action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = PremiumFeature::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = PremiumFeature::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = PremiumFeature::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = PremiumFeature::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = PremiumFeature::published()->orderBy('order')->latest()->get(['id', 'feature_name', 'availability']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = PremiumFeature::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */	
    public function destroy($id)
    {
        //$item = PremiumFeature::findOrFail($id);
        //$item->delete();
        
        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'feature_name' => ['required', 'min:3'],
            'feature_description' => ['required'],            
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'feature_name' => ['required', 'min:3'],
            'feature_description' => ['required']
        ];
    }
}
