<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\PushNotification;
use App\Traits\CommonModuleMethods;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\TargetGroup;
use App\Models\Country;
use App\Jobs\PushNotificationJob;
use Cache;

class PushNotificationController extends Controller
{
    use CommonModuleMethods;
    
    protected $last_active = [
        "" => "Select...", 
        '3' => 'Last 3 Days', 
        '7' => 'Last 7 Days', 
        '15' => "Last 15 Days", 
        '30' => 'Last 1 Month'
    ];
    
    protected $os_array = [
        "" => "Both", 
        "android" => "Android", 
        "ios" => "iOS"
    ];
    
    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.push-notification';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $module_details = Module::whereController(class_basename(__CLASS__))->first();
        $target_list = TargetGroup::get(['id', 'title']);
        return $this->view('index', compact('module_details', 'target_list'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = PushNotification::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

//        if (! empty($request->custom_filter)) {
//            $query->where(function($query) use ($request){
//                foreach ($request->custom_filter as $filter) {
//                    list($field, $value) = explode(".", $filter);
//                    $query->orWhere($field, $value);
//                }
//            });
//        }
        
        if(!empty($request->custom_filter)) {
            $custom_filters = [];
            foreach ($request->custom_filter as $filter) {
                list($field, $value) = explode(".", $filter);
                $custom_filters[$field][] = $value;
            }

            $query->where(function($query) use ($custom_filters) {
                foreach ($custom_filters as $key => $custom_filter) {
                    $query->whereIn($key, $custom_filter);
                }
            });
        }

        $query->orderBy('order');
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $total_query = clone $query;
        $query->take($request->length)->skip($request->start);
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {            
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'destination' => $item->destination,
                'target_group_id' => is_null($item->target_group_id) ? " - " : @$item->target_group->title,
                'app_page' => @$item->app_page,
                'status' => $item->status,
                'admin_status' => $item->admin_status,
            ];
            if(Auth::guard('admin')->user()->can('edit.push-notification')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\PushNotificationController@edit', $item->id) .'"><i class="glyph-icon icon-edit"></i> Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $total_query->count(),
            'recordsFiltered' => $total_query->count(),
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PushNotification $push_notification
     * @return \Illuminate\Http\Response
     */
    public function create(PushNotification $push_notification) {        
        $target_list = TargetGroup::pluck('title', 'id')->all();
        $notification_type = ["" => "Select..", "URL"=> "URL", "App" => "App Page"];
        $ape_feed_type = ["Network"=> "Network", "Recommendations" => "Recommendations"];
        $ape_page_type = ["Search"=> "Search", "Feed" => "Feed"];
        $last_active = $this->last_active;
        $os_array = $this->os_array;
        $country_array = Country::orderBy('title', 'ASC')->pluck('title', 'id')->all();
        
        return $this->view('form', compact('push_notification', 'target_list', 'notification_type', 
                'ape_feed_type', 'ape_page_type', 'os_array', 'last_active', 'country_array'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //dd($request->all());
        
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->all();
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        if($request->destination == "URL") {
            $attributes['app_page'] = "";
            $attributes['feed_page'] = "";
            $attributes['search_query'] = "";
        } else {        
            if($request->app_page == "Search") {
                $attributes['feed_page'] = "";
                $attributes['url'] = "";
            } else if($request->app_page == "Feed") {
                $attributes['search_query'] = "";
                $attributes['url'] = "";
            }
        }
                
        PushNotification::create($attributes);

        return redirect()->action('Admin\PushNotificationController@index')
            ->with('success', 'Push Notification Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PushNotification $push_notification
     * @return \App\Traits\type
     */
    public function edit(PushNotification $push_notification) {   
        //dd($push_notification);
        $last_active = $this->last_active;
        $os_array = $this->os_array;
        $country_array = Country::orderBy('title', 'ASC')->pluck('title', 'id')->all();
        $target_list = TargetGroup::pluck('title', 'id')->all();
        $notification_type = ["" => "Select..", "URL"=> "URL", "App" => "App Page"];
        $ape_feed_type = ["Network"=> "Network", "Recommendations" => "Recommendations"];
        $ape_page_type = ["Search"=> "Search", "Feed" => "Feed"];
        return $this->view('form', compact('push_notification', 'target_list', 'notification_type', 
            'ape_feed_type', 'ape_page_type', 'last_active', 'os_array', 'country_array'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param PushNotification $push_notification
     * @return \Illuminate\Http\Response
     * @internal param PushNotification $push_notification
     */
    public function update(Request $request, PushNotification $push_notification)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($push_notification), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->all();
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
//        if($request->destination == "URL") {
//            $attributes['app_page'] = "";
//            $attributes['feed_page'] = "";
//            $attributes['search_query'] = "";
//        }
//        
//        if($request->app_page == "Search") {
//            $attributes['feed_page'] = "";
//            $attributes['url'] = "";
//        } else if($request->app_page == "Feed") {
//            $attributes['search_query'] = "";
//            $attributes['url'] = "";
//        }
        
        if($request->destination == "URL") {
            $attributes['app_page'] = "";
            $attributes['feed_page'] = "";
            $attributes['search_query'] = "";
        } else {        
            if($request->app_page == "Search") {
                $attributes['feed_page'] = "";
                $attributes['url'] = "";
            } else if($request->app_page == "Feed") {
                $attributes['search_query'] = "";
                $attributes['url'] = "";
            }
        }

        $push_notification->fill($attributes);
        $push_notification->save();

        return redirect()->action('Admin\PushNotificationController@index')
            ->with('success', 'Push Notification Edited Successfully.');
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = PushNotification::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = PushNotification::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = PushNotification::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }
    
    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = PushNotification::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = PushNotification::findOrFail($id);
        $item->delete();

        return true;
    }
    
    /**
     * 
     * @param Request $request
     * @return type
     */
    public function notification(Request $request) {
        
        if(count($request->ids) > 1) {
           return ['status' => false, "message" => "Notification must be processed one at a time"]; 
        }
        
        $notification = PushNotification::published()->approved()
                ->where('id', $request->ids)->first();
        
        if(empty($notification)) {
            return ['status' => false, "message" => "Notification must be Published/Approved"];
        }

        if (Cache::get('ProcessRunning.Push'.$notification->id)) {
            return ['status' => false, 'message' => "Push already in progress."];
        }

        Cache::forever('ProcessRunning.Push'.$notification->id, 'Yes');
        
        dispatch(new PushNotificationJob($notification->id));
                        
        return ['status' => true];
    }
    
    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            'message' => ['required'],
            'destination' => ['required']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required'],
            'message' => ['required'],
            'destination' => ['required']
        ];
    }
}