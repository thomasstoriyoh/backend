<?php

namespace App\Http\Controllers\Admin\Reports\Google;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;
use DateTime;
use Illuminate\Support\Collection;
use Carbon\Carbon;

use Analytics;
use Spatie\Analytics\Period;

class GoogleAnalyticsBehaviorData extends Controller
{
	use CommonModuleMethods;
	
    protected $viewRoot = 'admin.reports.google';
 
    public function index() {
    	$data = $this->getBehaviorData(Carbon::createFromFormat("Y-m-d", session('ga_start_date_for_analytics')), Carbon::createFromFormat("Y-m-d", session('ga_end_date_for_analytics')), 'ga:pageviews,ga:uniquePageviews', ["dimensions"=>'ga:date,ga:pagePath', "sort"=>"ga:date"]);
		$fotmatedView = $this->formatDataTable($data->toArray());
		$pageView = $fotmatedView[0];
		$dateView = $fotmatedView[1];
		
        return $this->view('behavior_data', compact("pageView", "dateView"));
    }
	
	/**
     * Call the Method for Device Brand and Model info
     *
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $metrics
     * @param array $others
     * @return mixed
     */
    public function getBehaviorData(DateTime $startDate, DateTime $endDate, $metrics, $others = array(), $groupBy = 'date')
    {
        $visitorData = [];
        $answer = Analytics::performQuery(Period::create($startDate, $endDate), $metrics, $others);
		
        if (is_null($answer->rows)) {
            return new Collection([]);
        }
		foreach ($answer->rows as $dateRow) {
            $visitorData[] = [$groupBy => Carbon::createFromFormat(($groupBy == 'yearMonth' ? 'Ym' : 'Ymd'), $dateRow[0]), 
            'page'=>$dateRow[1],
            'pageview'=>$dateRow[2],
            'uniquepageview'=>$dateRow[3]
            ];
        }

        return new Collection($visitorData);
    }
	
	protected function formatDataTable($data)
	{
		$views = $unique = $row = $row2 = [];
		foreach($data as $item)
		{
			@$views[$item['page']][] += $item['pageview'];
			@$unique[$item['page']][] += $item['uniquepageview'];
		}
		
		foreach($views as $key=>$i)
		{
			$row[] = array($key,array_sum($i),array_sum($unique[$key]));
		}
		
		foreach($data as $j)
		{
			if($j['page'] != "(not set)")
			$row2[] = array($j['date']->format("d/m/Y"), $j['page'], $j['pageview']);
		}
		return array($row, $row2);
	}
}
