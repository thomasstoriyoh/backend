<?php

namespace App\Http\Controllers\Admin\Reports\Google;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;
use DateTime;
use Illuminate\Support\Collection;
use Carbon\Carbon;

use Analytics;
use Spatie\Analytics\Period;

class GoogleAnalyticsDeviceData extends Controller
{
	use CommonModuleMethods;
	
    protected $viewRoot = 'admin.reports.google';

    public function index() {
    	$data = $this->getDeviceOSData(Carbon::createFromFormat("Y-m-d", session('ga_start_date_for_analytics')), Carbon::createFromFormat("Y-m-d", session('ga_end_date_for_analytics')), 'ga:visits', ["dimensions"=>'ga:date,ga:operatingsystem,ga:operatingsystemversion', "sort"=>"ga:date"]);
		$data2 = $this->getDeviceBrandModelData(Carbon::createFromFormat("Y-m-d", session('ga_start_date_for_analytics')), Carbon::createFromFormat("Y-m-d", session('ga_end_date_for_analytics')), 'ga:visits', ["dimensions"=>'ga:date,ga:mobiledevicebranding,ga:mobiledevicemodel', "sort"=>"ga:date"]);
		
		$analyticsOSData = $this->formatAnalyticsOSData($data->toArray());
		$analyticsOSBrandModelData = $this->formatAnalyticsOSBrandingData($data2->toArray());

		$osData = $analyticsOSData[0];
		$osVersionData = $analyticsOSData[1];
		$brandData = $analyticsOSBrandModelData[0];
		$modelData = $analyticsOSBrandModelData[1];
		
		return $this->view('device_data', compact("osData", "osVersionData", "brandData", "modelData"));
    }

	/**
     * Call the Method for Device OS and OS Version info
     *
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $metrics
     * @param array $others
     * @return mixed
     */
    protected function getDeviceOSData(DateTime $startDate, DateTime $endDate, $metrics, $others = array(), $groupBy = 'date')
    {
        $visitorData = [];
        $answer = Analytics::performQuery(Period::create($startDate, $endDate), $metrics, $others);

        if (is_null($answer->rows)) {
            return new Collection([]);
        }
		foreach ($answer->rows as $dateRow) {
            $visitorData[] = [$groupBy => Carbon::createFromFormat(($groupBy == 'yearMonth' ? 'Ym' : 'Ymd'), $dateRow[0]), 
            'os'=>$dateRow[1],
            'osversion'=>$dateRow[2],
            'visits'=>$dateRow[3]
            ];
        }

        return new Collection($visitorData);
    }
	
	/**
     * Call the Method for Device Brand and Model info
     *
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $metrics
     * @param array $others
     * @return mixed
     */
    protected function getDeviceBrandModelData(DateTime $startDate, DateTime $endDate, $metrics, $others = array(), $groupBy = 'date')
    {
        $visitorData = [];
        $answer = Analytics::performQuery(Period::create($startDate, $endDate), $metrics, $others);
		//dd($answer);
        if (is_null($answer->rows)) {
            return new Collection([]);
        }
		foreach ($answer->rows as $dateRow) {
            $visitorData[] = [$groupBy => Carbon::createFromFormat(($groupBy == 'yearMonth' ? 'Ym' : 'Ymd'), $dateRow[0]), 
            'brand'=>$dateRow[1],
            'model'=>$dateRow[2],
            'visits'=>$dateRow[3]
            ];
        }

        return new Collection($visitorData);
    }
	
	protected function formatAnalyticsOSData($data)
	{
		$group = $os = $osv = [];
		foreach($data as $item) {
			@$group[$item['os']][$item['osversion']] += $item['visits'];
		}
		// OS code start
		$os[] = array("OS", "Total Views");
		foreach($group as $key=>$item) {
			@$os[] = array($key, array_sum($item));
		}
		
		// OS version code start
		$osv[] = array("OS Version", "Total Views");
		foreach($group as $key=>$item) {
			foreach($item as $i=>$j) {
				@$osv[] = array($key." ".$i, $j);
			}
		}
		
		return [$os,$osv];
	}
	
	protected function formatAnalyticsOSBrandingData($data)
	{
		$groupBM = $osB = $osM = [];
		foreach($data as $item) {
			@$groupBM[$item['brand']][$item['model']] += $item['visits'];
		}
		// Brand code start
		$osB[] = array("Brand", "Total Views");
		foreach($groupBM as $key=>$item) {
			@$osB[] = array($key, array_sum($item));
		}
		
		// Model code start
		$osM[] = array("Model", "Total Views");
		foreach($groupBM as $key=>$item) {
			foreach($item as $i=>$j) {
				@$osM[] = array($key." ".$i, $j);
			}
		}
		
		return [$osB,$osM];
	}
}