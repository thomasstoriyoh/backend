<?php

namespace App\Http\Controllers\Admin\Reports\Google;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;
use DateTime;
use Illuminate\Support\Collection;
use Carbon\Carbon;

use Analytics;
use Spatie\Analytics\Period;

class GoogleAnalyticsGeoData extends Controller
{
	use CommonModuleMethods;
	
    protected $viewRoot = 'admin.reports.google';

    public function index(Request $request) {
    	
		$geolocation = ['continent', 'subcontinent', 'country', 'region', 'city', 'networkdomain'];
    	$place = 'continent';
    	if ($request->has('type')) {
    		$place = $geolocation[array_search($request->type, $geolocation)+1];
    	}

		$options = ["dimensions"=>'ga:date,ga:'.$place, "sort"=>"ga:date"];
		
		if($request->has("type") && $request->has("value")){
			$options['filters'] = "ga:{$request->type}==".rawurlencode($request->value);
		}

    	$data = $this->getGEOData(Carbon::createFromFormat("Y-m-d", session('ga_start_date_for_analytics')), Carbon::createFromFormat("Y-m-d", session('ga_end_date_for_analytics')), 'ga:visits', $options);
        $analyticsData = $this->formatAnalyticsData($data->toArray());

        return $this->view('geo_data', [
        'analyticsData' => $analyticsData,
        'place_type' => $place,
        'value' => $request->value,
        ]);
    }

	/**
     * Call the Method for Device Brand and Model info
     *
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $metrics
     * @param array $others
     * @return mixed
     */
    protected function getGEOData(DateTime $startDate, DateTime $endDate, $metrics, $others = array(), $groupBy = 'date')
    {
        $visitorData = [];
        $answer = Analytics::performQuery(Period::create($startDate, $endDate), $metrics, $others);
		
        if (is_null($answer->rows)) {
            return new Collection([]);
        }
		
		foreach ($answer->rows as $dateRow) {
            $visitorData[] = [$groupBy => Carbon::createFromFormat(($groupBy == 'yearMonth' ? 'Ym' : 'Ymd'), $dateRow[0]), 
            'title'=>$dateRow[1],
            'visits'=>$dateRow[2]
            ];
        }

        return new Collection($visitorData);
    }

	/**
	 * Format the given data.
	 *
	 * @param $data
	 * @return array
	 */
	protected function formatAnalyticsData($data)
	{
		$group = $row = [];
		$total = 0;
		foreach($data as $item)
		{
			@$group[$item['title']] += $item['visits'];
			$total += $item['visits'];
		}
		
		foreach($group as $key=>$i)
		{
			$row[] = array($key, $i);
		}
		return $row;
	}
}
