<?php

namespace App\Http\Controllers\Admin\Reports\Google;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;
use DateTime;
use Illuminate\Support\Collection;
use Carbon\Carbon;

use Analytics;
use Spatie\Analytics\Period;

class GoogleAnalyticsSessionData extends Controller
{
	use CommonModuleMethods;
	
    protected $viewRoot = 'admin.reports.google';

    public function index() {
    	$data = $this->getSessionDataForPeriod(Carbon::createFromFormat("Y-m-d", session('ga_start_date_for_analytics')), Carbon::createFromFormat("Y-m-d", session('ga_end_date_for_analytics')), 'ga:percentNewSessions,ga:bounces,ga:bounceRate,ga:sessionDuration,ga:avgSessionDuration', ["dimensions"=>'ga:date', "sort"=>"ga:date"]);
        
        $percentNewSessionsData = $this->formatAnalyticsDatawithRound($data, 'percentNewSessions'); //%
		$avgSessionDurationData = $this->formatAnalyticsDatawithRound($data, 'avgSessionDuration');
		
		$bouncesData = $this->formatAnalyticsData($data, 'bounces');
		$bounceRateData = $this->formatAnalyticsDatawithRound($data, 'bounceRate');
		
        return $this->view('session_data', compact("percentNewSessionsData", "avgSessionDurationData", "bouncesData", "bounceRateData"));
    }
	
	/**
     * Call the Method for seesion info
     *
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $metrics
     * @param array $others
     * @return mixed
     */     
    protected function getSessionDataForPeriod(DateTime $startDate, DateTime $endDate, $metrics, $others = array(), $groupBy = 'date')
    {
        $visitorData = [];
        $answer = Analytics::performQuery(Period::create($startDate, $endDate), $metrics, $others);
		
        if (is_null($answer->rows)) {
            return new Collection([]);
        }
		
        foreach ($answer->rows as $dateRow) {
            $visitorData[] = [$groupBy => Carbon::createFromFormat(($groupBy == 'yearMonth' ? 'Ym' : 'Ymd'), $dateRow[0]), 
            'percentNewSessions'=>$dateRow[1], 
            'bounces'=>$dateRow[2], 
            'bounceRate'=>$dateRow[3], 
            'sessionDuration'=>$dateRow[4], 
            'avgSessionDuration'=>$dateRow[5]];
        }
        return new Collection($visitorData);
    }
	
	/*
	 * Format Analytics Data
	*/
	protected  function formatAnalyticsDatawithRound($data, $type)
	{
		$formatedArr = array();
		foreach($data as $item)
		{
			$formatedArr[] = array($item['date']->format("jS M"), round($item[$type], 2));
		}
		return $formatedArr;
	}
	protected  function formatAnalyticsData($data, $type)
	{
		$formatedArr = array();
		foreach($data as $item)
		{
			$formatedArr[] = array($item['date']->format("jS M"), (int) $item[$type]);
		}
		return $formatedArr;
	}
}