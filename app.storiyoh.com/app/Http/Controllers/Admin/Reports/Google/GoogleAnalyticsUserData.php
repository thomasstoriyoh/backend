<?php
namespace App\Http\Controllers\Admin\Reports\Google;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\CommonModuleMethods;

use DateTime;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Analytics;
use Spatie\Analytics\Period;
class GoogleAnalyticsUserData extends Controller
{
	use CommonModuleMethods;

    protected $viewRoot = 'admin.reports.google';

    public function index() {
		$data = $this->getUserDataForPeriod(Carbon::createFromFormat("Y-m-d", session('ga_start_date_for_analytics')), Carbon::createFromFormat("Y-m-d", session('ga_end_date_for_analytics')), 'ga:pageviews,ga:sessions,ga:users,ga:newUsers', ["dimensions"=>'ga:date', "sort"=>"ga:date"]);

		$analyticsTotalPageviewsData = $this->formatAnalyticsData($data, 'pageViews');
		$analyticsActiveUsersData = $this->formatAnalyticsData($data, 'users');
		$analyticsNewUsersData = $this->formatAnalyticsData($data, 'newUsers');

		$newuser = $this->totaNewlUser($data);
		$totalsession = $this->totalSessions($data);

		$returninguser = $totalsession - $newuser;

		$newVisitorData[] = ['New Visitor', 'Returning Visitor'];

        if($newuser || $returninguser){
            $newVisitorData[] = ["New Visitor", (int) $newuser];
			$newVisitorData[] = ["Returning Visitor", (int) $returninguser];
        }

    	return $this->view('user_data', compact("analyticsTotalPageviewsData", "analyticsActiveUsersData", "analyticsNewUsersData", "newVisitorData"));
    }

	/**
     * Call the query method for the user info
     *
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $metrics
     * @param array $others
     * @return mixed
     */
    protected function getUserDataForPeriod(DateTime $startDate, DateTime $endDate, $metrics, $others = array(), $groupBy = 'date')
    {
        $visitorData = [];
        $answer = Analytics::performQuery(Period::create($startDate, $endDate), $metrics, $others);

        if (is_null($answer->rows)) {
            return new Collection([]);
        }
		foreach ($answer->rows as $dateRow) {
            $visitorData[] = [$groupBy => Carbon::createFromFormat(($groupBy == 'yearMonth' ? 'Ym' : 'Ymd'), $dateRow[0]),
            'pageViews'=>$dateRow[1],
            'sessions'=>$dateRow[2],
            'users'=>$dateRow[3],
            'newUsers'=>$dateRow[4]
            ];
        }
        return new Collection($visitorData);
    }

	/*
	 * Format Analytics Data
	*/
	protected  function formatAnalyticsData($data, $type)
	{
		$formatedArr = array();
		foreach($data as $item)
		{
			$formatedArr[] = array($item['date']->format("jS M"), (int) $item[$type]);
		}
		return $formatedArr;
	}

	/*
	 * Total New Users Code
	*/
	protected function totaNewlUser($data)
	{
		$totalnewuser = 0;
		foreach($data as $item)
		{
			$totalnewuser = $totalnewuser + $item['newUsers'];
		}
		return $totalnewuser;
	}

	/*
	 * Total Sessions Code
	*/
	protected function totalSessions($data)
	{
		$totalsessions = 0;
		foreach($data as $item)
		{
			$totalsessions = $totalsessions + $item['sessions'];
		}
		return $totalsessions;
	}
}
