<?php

namespace App\Http\Controllers\Admin\Reports\Member;

use App\Models\AreaHead;
use App\Models\BrandCompany;
use App\Models\LicenceType;
use App\Models\Order;
use App\Models\Outlet;
use App\Models\OutletType;
use App\Models\Product;
use App\Models\RegionalHead;
use App\Models\SalesExecutive;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use function Psy\sh;

class SalesReportController extends Controller
{
    protected $report_types = [
        'Overall Sale',
        'Commission',
        'Service Charge',
//        'New Users',
//        'Total Users'
    ];

    protected $service_charge = 3;
    protected $commission = 15;

    public function index()
    {
        $companies = BrandCompany::lists('company', 'id');
        $regional_heads = $this->getRegionalHeadOptions();
        $licence_types = LicenceType::published()
            ->whereIn('id', OutletType::published()->lists('licence_type_id')->all())
            ->lists('title', 'id')->all();
        $outlet_types = $this->getOutletTypeOptions();
        $report_type = array_combine($this->report_types, $this->report_types);

        return view('admin.reports.member.sales',
            compact('companies', 'regional_heads', 'licence_types', 'outlet_types', 'report_type'));
    }

    public function ajaxIndex(Request $request)
    {
        $params = $this->getRequestParams($request);
        $query = $this->getOrderQuery($params);
        $orders = $query->get(['id', 'outlet_id', 'distributor_id', 'total_cost', 'order_date']);


        list($overall_sale, $overall_case) = $this->getOverallChart($orders, $params);
        list($company_sale, $company_case) = $this->getCompanyChart($orders, $params);
        list($category_sale, $category_case) = $this->getCategoryChart($orders, $params);
        list($product_sale, $product_case) = $this->getProductChart($orders, $params);
        list($distributor_sale, $distributor_case) = $this->getDistributorChart($orders, $params);
        list($licence_sale, $licence_case) = $this->getLicenceTypeChart($orders, $params);
        list($type_sale, $type_case) = $this->getOutletTypeChart($orders, $params);
        list($outlet_sale, $outlet_case) = $this->getOutletChart($orders, $params);


        return compact('overall_sale', 'overall_case', 'company_sale', 'company_case',
            'category_sale', 'category_case', 'product_sale', 'product_case', 'distributor_sale',
            'distributor_case', 'licence_sale', 'licence_case', 'type_sale', 'type_case',
            'outlet_sale', 'outlet_case');
    }

    protected function getCompanyChart($orders, $params)
    {
        $by_date_sale = [];
        $by_date_case = [];
        foreach ($orders as $order) {
            $gr = Carbon::createFromFormat('Y-m-d', $order->order_date)->format('M Y');
            foreach ($order->order_products as $order_product) {
                if (@$order_product->product->brand &&
                    in_array(@$order_product->product->brand->id, $params->company_ids)) {
                        @$by_date_sale[strtoupper($gr)]
                            [$order_product->product->brand->company] += $this->getValue($order_product, $params);
                        if ($order_product->bundle == 'Case') {
                            @$by_date_case[strtoupper($gr)]
                                [$order_product->product->brand->company] += $order_product->quantity;
                        } else {
                            @$by_date_case[strtoupper($gr)]
                                [$order_product->product->brand->company] += 0;
                        }
                }
            }
        }


        $sale = $this->getStackedData($by_date_sale);

        $case = $this->getStackedData($by_date_case);

        return [$sale, $case];
    }

    protected function getCategoryChart($orders, $params)
    {
        $by_date_sale = [];
        $by_date_case = [];
        foreach ($orders as $order) {
            $gr = Carbon::createFromFormat('Y-m-d', $order->order_date)->format('M Y');
            foreach ($order->order_products as $order_product) {
                if (@$order_product->product->category &&
                    in_array(@$order_product->product->category->id, $params->category_ids)) {
                        @$by_date_sale[strtoupper($gr)]
                            [$order_product->product->category->title] += $this->getValue($order_product, $params);
                        if ($order_product->bundle == 'Case') {
                            @$by_date_case[strtoupper($gr)]
                                [$order_product->product->category->title] += $order_product->quantity;
                        } else {
                            @$by_date_case[strtoupper($gr)]
                                [$order_product->product->category->title] += 0;
                        }
                }
            }
        }


        $sale = $this->getStackedData($by_date_sale);

        $case = $this->getStackedData($by_date_case);

        return [$sale, $case];
    }

    protected function getProductChart($orders, $params)
    {
        $by_date_sale = [];
        $by_date_case = [];
        foreach ($orders as $order) {
            $gr = Carbon::createFromFormat('Y-m-d', $order->order_date)->format('M Y');
            foreach ($order->order_products as $order_product) {
                if (@$order_product->product &&
                    in_array(@$order_product->product->id, $params->product_ids)) {
                    @$by_date_sale[strtoupper($gr)]
                    [$order_product->product->title] += $this->getValue($order_product, $params);
                    if ($order_product->bundle == 'Case') {
                        @$by_date_case[strtoupper($gr)]
                        [$order_product->product->title] += $order_product->quantity;
                    } else {
                        @$by_date_case[strtoupper($gr)]
                        [$order_product->product->title] += 0;
                    }
                }
            }
        }


        $sale = $this->getStackedData($by_date_sale);

        $case = $this->getStackedData($by_date_case);

        return [$sale, $case];
    }

    protected function getDistributorChart($orders, $params)
    {
        $by_date_sale = [];
        $by_date_case = [];
        foreach ($orders as $order) {
            $gr = Carbon::createFromFormat('Y-m-d', $order->order_date)->format('M Y');
            if (@$order->distributor &&
                in_array(@$order->distributor->id, $params->distributor_ids)) {
                foreach ($order->order_products as $order_product) {
                    @$by_date_sale[strtoupper($gr)]
                    [$order->distributor->company] += $this->getValue($order_product, $params);
                    if ($order_product->bundle == 'Case') {
                        @$by_date_case[strtoupper($gr)]
                        [$order->distributor->company] += $order_product->quantity;
                    } else {
                        @$by_date_case[strtoupper($gr)]
                        [$order->distributor->company] += 0;
                    }
                }
            }
        }


        $sale = $this->getStackedData($by_date_sale);

        $case = $this->getStackedData($by_date_case);

        return [$sale, $case];
    }

    protected function getLicenceTypeChart($orders, $params)
    {
        $by_date_sale = [];
        $by_date_case = [];
        foreach ($orders as $order) {
            $gr = Carbon::createFromFormat('Y-m-d', $order->order_date)->format('M Y');
            if (@$order->outlet->licence_type &&
                in_array(@$order->outlet->licence_type->id, $params->licence_type_ids)) {
                foreach ($order->order_products as $order_product) {
                    @$by_date_sale[strtoupper($gr)]
                    [$order->outlet->licence_type->title] += $this->getValue($order_product, $params);
                    if ($order_product->bundle == 'Case') {
                        @$by_date_case[strtoupper($gr)]
                        [$order->outlet->licence_type->title] += $order_product->quantity;
                    } else {
                        @$by_date_case[strtoupper($gr)]
                        [$order->outlet->licence_type->title] += 0;
                    }
                }
            }
        }


        $sale = $this->getStackedData($by_date_sale);

        $case = $this->getStackedData($by_date_case);

        return [$sale, $case];
    }

    protected function getOutletTypeChart($orders, $params)
    {
        $by_date_sale = [];
        $by_date_case = [];
        foreach ($orders as $order) {
            $gr = Carbon::createFromFormat('Y-m-d', $order->order_date)->format('M Y');
            if (@$order->outlet->outlet_type &&
                in_array(@$order->outlet->outlet_type->id, $params->outlet_type_ids)) {
                foreach ($order->order_products as $order_product) {
                    @$by_date_sale[strtoupper($gr)]
                    [$order->outlet->outlet_type->title] += $this->getValue($order_product, $params);
                    if ($order_product->bundle == 'Case') {
                        @$by_date_case[strtoupper($gr)]
                        [$order->outlet->outlet_type->title] += $order_product->quantity;
                    } else {
                        @$by_date_case[strtoupper($gr)]
                        [$order->outlet->outlet_type->title] += 0;
                    }
                }
            }
        }


        $sale = $this->getStackedData($by_date_sale);

        $case = $this->getStackedData($by_date_case);

        return [$sale, $case];
    }

    protected function getOutletChart($orders, $params)
    {
        $by_date_sale = [];
        $by_date_case = [];
        foreach ($orders as $order) {
            $gr = Carbon::createFromFormat('Y-m-d', $order->order_date)->format('M Y');
            if (@$order->outlet &&
                in_array(@$order->outlet->id, $params->outlet_ids)) {
                foreach ($order->order_products as $order_product) {
                    @$by_date_sale[strtoupper($gr)]
                    [$order->outlet->company] += $this->getValue($order_product, $params);
                    if ($order_product->bundle == 'Case') {
                        @$by_date_case[strtoupper($gr)]
                        [$order->outlet->company] += $order_product->quantity;
                    } else {
                        @$by_date_case[strtoupper($gr)]
                        [$order->outlet->company] += 0;
                    }
                }
            }
        }


        $sale = $this->getStackedData($by_date_sale);

        $case = $this->getStackedData($by_date_case);

        return [$sale, $case];
    }

    protected function getOverallChart($orders, $params)
    {
        $by_date_sale = [];
        $by_date_case = [];
        foreach ($orders as $order) {
            $gr = Carbon::createFromFormat('Y-m-d', $order->order_date)->format('M Y');
            foreach ($order->order_products as $order_product) {
                @$by_date_sale[strtoupper($gr)] += $this->getValue($order_product, $params);
                if($order_product->bundle == 'Case') {
                    @$by_date_case[strtoupper($gr)] += $order_product->quantity;
                } else {
                    @$by_date_case[strtoupper($gr)] += 0;
                }
            }
        }

        $sale = [["Month", "Sale", ["role" => "annotation"]]];
        foreach ($by_date_sale as $key => $item) {
            $sale[] = [$key, ceil($item), $this->kFormat($item)];
        }

        $case = [["Month", "Sale", ["role" => "annotation"]]];
        foreach ($by_date_case as $key => $item) {
            $case[] = [$key, ceil($item), $this->kFormat($item)];
        }

        return [$sale, $case];
    }

    protected function kFormat($num) {
        if ($num < 1000) {
            return ceil($num);
        } elseif ($num < 1000000) {
            return round($num/1000, 1) . 'K';
        } elseif ($num < 1000000000) {
            return round($num/1000000, 1) . 'M';
        } else {
            return round($num/1000000000, 1) . 'B';
        }
    }

    protected function getOutletIds($params)
    {
        if (! empty($params->outlet_ids)) {
            return $params->outlet_ids;
        }

        if (empty($params->sales_ids) &&
            empty($params->area_head_ids) &&
            empty($params->regional_head_ids) &&
            empty($params->licence_type_ids) &&
            empty($params->outlet_type_ids)
        ) {
            return [];
        }

        $query = Outlet::select('id');
        if (! empty($params->sales_ids)) {
            $city_ids = [];
            $city_list = SalesExecutive::whereIn('id', $params->sales_ids)->lists('city_ids');
            foreach ($city_list as $city) {
                $city_ids = array_merge($city_ids, json_decode($city));
            }
            $query->whereIn('city_id', $city_ids);
        } else if (! empty($params->area_head_ids)) {
            $district_ids = [];
            $district_list = AreaHead::whereIn('id', $params->area_head_ids)->lists('district_ids');
            foreach ($district_list as $district) {
                $district_ids = array_merge($district_ids, json_decode($district));
            }
            $query->whereIn('district_id', $district_ids);
        } else if (! empty($params->regional_head_ids)) {
            $state_ids = RegionalHead::whereIn('id', $params->regional_head_ids)
                ->lists('state_id')->all();
            $query->whereIn('state_id', $state_ids);
        }

        if (! empty($params->licence_type_ids)) {
            $query->whereIn('licence_type_id', $params->licence_type_ids);
        }
        if (! empty($params->outlet_type_ids)) {
            $query->whereIn('outlet_type_id', $params->outlet_type_ids);
        }

        return $query->lists('id')->all();

    }

    protected function getProductIds($params) {
        if (! empty($params->product_ids)) {
            return $params->product_ids;
        } else if (! empty($params->category_ids)) {
            return Product::whereIn('product_category_id', $params->category_ids)
                ->lists('id')->all();
        }

        return [];
    }

    protected function getStackedData($values)
    {
        $sHeader = ["Month"];
        foreach ($values as $month => $items) {
            foreach ($items as $company => $item) {
                if (! in_array($company, $sHeader)) {
                    $sHeader[] = $company;
                }
            }
        }

        $sale = [];
        foreach ($values as $month => $items) {
            $row = [$month];
            foreach ($sHeader as $h) {
                if ($h == "Month") {
                    continue;
                }
                $row[] = empty($items[$h]) ? 0 : ceil($items[$h]);
            }

            $sale[] = $row;
        }

        array_unshift($sale, $sHeader);

        return $sale;
    }

    // 15 / 3

    protected function getValue($op, $params)
    {
        switch ($params->report_type) {
            case "Overall Sale":
                return $op->total;

            case "Commission":
                return $this->getCommission($op->total);

            case "Service Charge":
                return $this->getServiceCharge($op->total);

            default:
                return $op->total;
        }
    }

    protected function getServiceCharge($value)
    {
        return ($value * $this->service_charge) / 100;
    }

    protected function getCommission($value)
    {
        $sc = $this->getServiceCharge($value);

        return ($sc * $this->commission) / 100;
    }

    /**
     * @return array
     */
    protected function getRegionalHeadOptions()
    {
        $rh = RegionalHead::all(['id', 'brand_company_id', 'first_name', 'last_name']);
        $regional_heads = [];
        foreach ($rh as $item) {
            $regional_heads[$item->brand_company_id][] = [
                'id' => $item->id,
                'full_name' => $item->full_name
            ];
        }

        return $regional_heads;
    }

    /**
     * @return array
     */
    protected function getOutletTypeOptions()
    {
        $outlet_types = [];
        $ot = OutletType::published()->get(['id', 'licence_type_id', 'title']);
        foreach ($ot as $item) {
            $outlet_types[$item->licence_type_id][] = [
                'id' => $item->id,
                'title' => $item->title
            ];
        }
        return $outlet_types;
    }

    protected function getRequestParams(Request $request)
    {
        return (object) [
            'start_date' => $request->has('start_date') ? $request->start_date :
                @date("Y-m-d", strtotime("-6 Months")),
            'end_date' => $request->has('end_date') ? $request->end_date : @date('Y-m-d'),
            'company_ids' => $request->has('company_ids') ? $request->company_ids : [],
            'regional_head_ids' => $request->has('regional_head_ids') ? $request->regional_head_ids : [],
            'area_head_ids' => $request->has('area_head_ids') ? $request->area_head_ids : [],
            'sales_ids' => $request->has('sales_ids') ? $request->sales_ids : [],
            'category_ids' => $request->has('category_ids') ? $request->category_ids : [],
            'product_ids' => $request->has('product_ids') ? $request->product_ids : [],
            'distributor_ids' => $request->has('distributor_ids') ? $request->distributor_ids : [],
            'licence_type_ids' => $request->has('licence_type_ids') ? $request->licence_type_ids : [],
            'outlet_type_ids' => $request->has('outlet_type_ids') ? $request->outlet_type_ids : [],
            'outlet_ids' => $request->has('outlet_ids') ? $request->outlet_ids : [],
            'report_type' => $request->has('report_type') ? $request->report_type : $this->report_types[0],
        ];
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function getOrderQuery($params)
    {
        $query = Order::whereIn('status', ['Accepted', 'Placed', 'Out For Delivery', 'Delivered'])
            ->whereBetween('order_date', [$params->start_date, $params->end_date]);
        $outlet_ids = $this->getOutletIds($params);

        if (!empty($outlet_ids)) {
            $query->whereIn('outlet_id', $outlet_ids);
        }
        if (!empty($params->distributor_ids)) {
            $query->whereIn('distributor_id', $params->distributor_ids);
        }

        $product_ids = $this->getProductIds($params);
        if (!empty($params->company_ids) OR !empty($product_ids)) {
            $query->whereHas('order_products', function ($q) use ($params, $product_ids) {
                $q->whereHas('product', function ($qry) use ($params) {
                    if (!empty($params->company_ids)) {
                        $qry->whereIn('brand_company_id', $params->company_ids);
                    }
                    if (! empty($product_ids)) {
                        $qry->whereIn('id', $product_ids);
                    }
                });

            });
        }
        return $query;
    }

}
