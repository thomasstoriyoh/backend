<?php

namespace App\Http\Controllers\Admin\Reports\Member;

use Illuminate\Http\Request;

use App\Models\Admin\Module;
use App\Models\User;
use App\Traits\CommonModuleMethods;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class UserReportController extends Controller
{

    use CommonModuleMethods;

    protected $viewRoot = "admin.reports.member";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $module_details = Module::whereController(class_basename(self::class))->first();
        $start_date = Carbon::now()->subDays(7)->format('Y-m-d');
        $end_date = Carbon::now()->format('Y-m-d');
        return $this->view('index', compact('module_details','start_date','end_date'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = User::select('*');

        $advanced_filters = explode('&',$request->advanced_filters);

        foreach ($advanced_filters as $filter_name => $filter_value) {
            $arr_fliter_value = explode('=', $filter_value);
            switch ($arr_fliter_value[0]) 
            { 
                case 'source':
                    if ($arr_fliter_value[1] != 'All') {
                        $query->orWhere('provider', '=', $arr_fliter_value[1]);
                    }
                    break;
                case 'daterange':
                    if (!empty($arr_fliter_value[1])) 
                    { 
                        $daterange = explode('+to+',$arr_fliter_value[1]);
                        $query = $query->where('created_at', '>=', $daterange[0] . " 00:00:00")
                            ->where('created_at', '<=', $daterange[1] . " 23:59:59");
                    }
                break;                
                case 'verified':
                    if ($arr_fliter_value[1] != 'All') { 
                        $query->Where('verified', $arr_fliter_value[1]);
                    }
                break;    
                
                case 'user_type':                
                    if ($arr_fliter_value[1] != 'All') {                        
                        $query->Where('user_type', $arr_fliter_value[1]);
                    }
                break;

                default:
                # code...
                break;
            }
            
        }
        
        $count = $query->count();

        $query->orderBy('app_last_access_time', 'DESC')->take($request->length)->skip($request->start);
        // foreach ($request->order as $order) {
        //     $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        // }

        $items = $query->latest()->get();        

        $data = [];
        foreach ($items as $key => $item) {
           
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'full_name' => $item->first_name." ".$item->last_name,
                //'last_name' => $item->last_name,
                'user_name' => $item->username,
                'email' => $item->email,
                'provider' => $item->provider,
                'user_type' => $item->user_type == 0 ? "Guest" : "Registered",
                'verified' => $item->verified,
                'admin_status' => $item->admin_status == 'Approved' ? 'Active':'Inactive',
                'last_login' => is_null($item->app_last_access_time) ? "-" : Carbon::parse($item->app_last_access_time)->format("d/m/Y H:i"),
            ];
            if(Auth::guard('admin')->user()->can('edit.users')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\UserController@show', $item->id) .'" target="_blank"><i class="glyph-icon icon-external-link"></i>  View</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function download_csv(Request $request)
    {
        //dd($request->all());
        $items = $this->getReportData($request);        
        $fields = $this->getReportFields($request);

        if(sizeof($items) < 1)
        {
            return redirect()->action('Admin\Reports\Member\UserReportController@index')
                ->with('error', 'No data available for report.');
        }        

        //dd($request->all());
        $data = $this->getFormattedReportData($fields, $items);

        $filename = "User Report Generated On - " . @date('Y-m-d H:i:s') . ".csv";
        
        ob_start();
        $handle = fopen("php://output", "w");
        foreach ($data as $key => $item) {
            if ($key == 0) {
                fputcsv($handle, array_keys($item));
            }
            fputcsv($handle, array_values($item));
        }
        fclose($handle);
        
        return \Response::make(ob_get_clean())
            ->header('Content-Type', 'application/csv')
            ->header('Content-Disposition', 'attachment; filename="'.$filename.'"');
    }

    /**
     * 
     */
    private function getReportData($request)
    {
        $query = User::select('*');        

        $advanced_filters = explode('&',$request->advanced_filters);

        foreach ($advanced_filters as $filter_name => $filter_value) {
            $arr_fliter_value = explode('=', $filter_value);
            switch ($arr_fliter_value[0]) 
            { 
                case 'source':
                    if ($arr_fliter_value[1] != 'All') { 
                        $query->orWhere('provider', '=', $arr_fliter_value[1]);
                    }
                    break;
                case 'daterange':
                    if (!empty($arr_fliter_value[1])) 
                    { 
                        $daterange = explode('+to+',$arr_fliter_value[1]);
                        $query = $query->where('created_at', '>=', $daterange[0] . " 00:00:00")
                            ->where('created_at', '<=', $daterange[1] . " 23:59:59");
                    }
                    break;
                case 'verified':
                    if ($arr_fliter_value[1] != 'All') { 
                        $query->Where('verified', $arr_fliter_value[1]);
                    }
                    break;
                
                case 'user_type':
                    if ($arr_fliter_value[1] != 'All') { 
                        $query->Where('user_type', $arr_fliter_value[1]);
                    }
                    break;
                        
                default:
                # code...
                break;
            }            
        }

        if(is_null($request->advanced_filters)) {
            $query->where('verified', 'Verified');
        }
        
        return $query->orderBy('app_last_access_time', "DESC")->orderBy('id', "DESC")->get();
    }

    /**
     * 
     */
    private function getReportFields($request)
    {
        $fields = [            
            'Full Name' => 'full_name',
            'Username' => 'username',
            'Email' => 'email',
            'Registration Type' => 'user_type',
            'Provider' => 'provider',
            'Active Status' => 'verified',
            'Registration' => 'created_at',
            'Last Active' => 'app_last_access_time',
        ];
        
        return $fields;
    }
    
    /**
     * Formated user data
     */
    private function getFormattedReportData($fields, $items)
    {
       $data = [];
        foreach ($items as $item) {
            $row = [];
            foreach ($fields as $title => $field) {
                $row[$title] = $item->$field;
                if ($field == 'full_name') {
                    $row[$title] = $item->first_name." ".$item->last_name;
                }
                if ($row[$title] instanceof Carbon) {
                    $row[$title] = $item->$field->format('d/m/Y H:i');
                }                
                if ($field == 'app_last_access_time') {
                    if (! is_null($item->$field)) {
                        $row[$title] = Carbon::parse($item->$field)->format('d/m/Y H:i');
                    } else {
                        $row[$title] = "Not Found";
                    }
                }
                if ($field == 'user_type') {
                    $row[$title] = $item->user_type == 0 ? "Guest" : "Registered";
                }               
            }
            $data[] = $row;
        }
        
        return $data;
    }

    /**
     * Download user count
     */
    public function downloadUserCsv(Request $request)
    {
        if(is_null($request->start_date) || is_null($request->end_date)){
            return redirect()->action('Admin\Reports\Member\UserReportController@index')->with('error', 'No data available for report.');
        }

        list($year1, $month1, $date1) = explode("-", $request->start_date);
        list($year2, $month2, $date2) = explode("-", $request->end_date);

        $final_csv_list = [
        [
            'Month',
            'Registered Users', 
            'Not Verified', 
            'Verified Users (without username)', 
            'Activated Users', 
            'Direct Activations', 
            'Google Activations', 
            'Facebook Activations', 
            'Twitter Activations'
        ]];

        $date_array = [];
        if(($month1 == $month2) && ($year1 == $year2)) {
            $date_array[0] = [$request->start_date." 00:00:01", $request->end_date." 23:59:59"];                        
        } else {
            $start    = (new \DateTime($request->start_date))->modify('first day of this month');
            $end      = (new \DateTime($request->end_date))->modify('first day of next month');
            $interval = \DateInterval::createFromDateString('1 month');
            $period   = new \DatePeriod($start, $interval, $end);
            
            $counter = 0;
            foreach ($period as $key => $dt) {             
                $counter++;
            }
            
            foreach ($period as $key => $dt) {
                $date_array[$key][] = $dt->format("Y-m-d"). " 00:00:01";                
                if($key == ($counter-1)) {
                    $date_array[$key][] = $request->end_date. " 23:59:59";
                } else {
                    $month_end_date = (new \DateTime($dt->format("Y-m-d")))->modify('last day of this month');
                    $date_array[$key][] = $month_end_date->format("Y-m-d"). " 23:59:59";;                    
                }                
            }
        }

        foreach($date_array as $value) {
            $start_date = $value[0];
            $end_date = $value[1];
    
            $total_registred_user = User::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->where('verified', '!=', 'Disabled')->count();
    
            //Not Verified
            $not_verified_users = User::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->where('verified', 'Unverified')->count();
        
            //Verified but not created username
            $verified_but_not_created_username = User::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->where('verified', 'Verified')->whereNull('username')->count();
    
            //Verified and created username
            $verified_and_created_username = User::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->where('verified', 'Verified')->whereNotNull('username')->count();
    
            //Verified, created username and Direct
            $verified_created_username_and_direct = User::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->where('verified', 'Verified')->whereNotNull('username')->where('provider', 'Direct')->count();
    
            //Verified, created username and Google
            $verified_created_username_and_google = User::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->where('verified', 'Verified')->whereNotNull('username')->where('provider', 'Google')->count();
        
            //Verified, created username and Facebook
            $verified_created_username_and_facebook = User::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->where('verified', 'Verified')->whereNotNull('username')->where('provider', 'Facebook')->count();
        
            //Verified, created username and Twitter
            $verified_created_username_and_twitter = User::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->where('verified', 'Verified')->whereNotNull('username')->where('provider', 'Twitter')->count();
    
            $final_csv_list[] =  [
                Carbon::parse($start_date)->format('F Y'),
                $total_registred_user, 
                $not_verified_users, 
                $verified_but_not_created_username, 
                $verified_and_created_username, 
                $verified_created_username_and_direct, 
                $verified_created_username_and_google, 
                $verified_created_username_and_facebook,
                $verified_created_username_and_twitter
            ];
        }        
        
        ob_start();
        $handle = fopen("php://output", "w");
        foreach ($final_csv_list as $line){
            fputcsv($handle, $line);
        }
            
        fclose($handle);

        $filename = "User Report Generated On - " . @date('Y-m-d H:i:s') . ".csv";
        
        return \Response::make(ob_get_clean())
            ->header('Content-Type', 'application/csv')
            ->header('Content-Disposition', 'attachment; filename="'.$filename.'"');
    }
}
