<?php

namespace App\Http\Controllers\Admin\Reports\Sales;

use Illuminate\Http\Request;

use App\Models\Admin\Module;
use App\Traits\CommonModuleMethods;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Order;

class SalesReportController extends Controller
{
    use CommonModuleMethods;

    protected $viewRoot = "admin.reports.sales";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $module_details = Module::whereController(class_basename(self::class))->first();
        $start_date = Carbon::now()->subDays(30)->format('Y-m-d');
        $end_date = Carbon::now()->format('Y-m-d');
        return $this->view('index', compact('module_details','start_date','end_date'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Order::select('*');

        $advanced_filters = explode('&',$request->advanced_filters);        

        foreach ($advanced_filters as $filter_name => $filter_value) {
            $arr_fliter_value = explode('=', $filter_value);
            switch ($arr_fliter_value[0]) 
            { 
                // case 'source':
                //     if ($arr_fliter_value[1] != 'All') {
                //         $query->orWhere('provider', '=', $arr_fliter_value[1]);
                //     }
                //     break;
                case 'daterange':
                    if (! empty($arr_fliter_value[1])) { 
                        $daterange = explode('+to+',$arr_fliter_value[1]);
                        $query = $query->where('invoice_date', '>=', $daterange[0] . " 00:00:00")
                            ->where('invoice_date', '<=', $daterange[1] . " 23:59:59");
                    } else {
                        $query = $query->where('invoice_date', '>=', Carbon::today()->subDays(30)->toDateString() . " 00:00:00")
                            ->where('invoice_date', '<=', Carbon::today()->toDateString() . " 23:59:59");
                    }
                break;                

                default:
                # code...
                break;
            }    
        }

        $query->where('status', 'success')->where('gateway', '!=', 'Free');
        
        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        // foreach ($request->order as $order) {
        //     $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        // }

        $items = $query->latest()->get();        

        $data = [];
        foreach ($items as $key => $item) {

            $name = "";
            $email = "";
            if (is_null($item->giftBy)) {
                $name = $item->user->first_name;
                $email = $item->user->email;
                $userId = $item->user_id;
            } else {
                $name = $item->sender->first_name;
                $email = $item->sender->email;
                $userId = $item->giftBy;
            }

            $type = $item->product_type == 1 ? "Series" : "Episode";
            $product = $item->product_type == 1 ? $item->show->title : $item->episode->title;
           
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'full_name' => $name,
                'email' => $email,
                'invoice_number' => $item->invoice_number,
                'invoice_date' => Carbon::parse($item->invoice_date)->format('d/m/Y g:ia'),
                'product' => $product,
                'product_type' => $type,
                'currency' => $item->currency,
                'invoice_total' => $item->invoice_total                
            ];
            if(Auth::guard('admin')->user()->can('edit.users')) {
                // $row['options'] = '<div class="dropdown">
                //     <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                //         <span class="glyph-icon icon-align-justify"></span>
                //         <i class="glyph-icon icon-angle-down"></i>
                //     </a>
                //     <div class="dropdown-menu float-right">
                //         <ul class="reset-ul mrg5B">
                //             <li><a href="'. action('Admin\UserController@show', $userId) .'" target="_blank"><i class="glyph-icon icon-external-link"></i>  View User</a></li>
                //         </ul>
                //     </div>
                // </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function download_sales_csv(Request $request)
    {
        $items = $this->getReportData($request);
        $fields = $this->getReportFields($request);

        if(sizeof($items) < 1)
        {
            return redirect()->action('Admin\Reports\Sales\SalesReportController@index')
                ->with('error', 'No data available for report.');
        }        
        
        $data = $this->getFormattedReportData($fields, $items);

        $filename = "Sales Report Generated On - " . @date('Y-m-d H:i:s') . ".csv";
        
        ob_start();
        $handle = fopen("php://output", "w");
        foreach ($data as $key => $item) {
            if ($key == 0) {
                fputcsv($handle, array_keys($item));
            }
            fputcsv($handle, array_values($item));
        }
        fclose($handle);
        
        return \Response::make(ob_get_clean())
            ->header('Content-Type', 'application/csv')
            ->header('Content-Disposition', 'attachment; filename="'.$filename.'"');
    }

    /**
     * 
     */
    private function getReportData($request)
    {
        $query = Order::select('*');        

        $advanced_filters = explode('&',$request->advanced_filters);
        $date_range = false;
        
        foreach ($advanced_filters as $filter_name => $filter_value) {
            $arr_fliter_value = explode('=', $filter_value);
            switch ($arr_fliter_value[0]) 
            {             
                // case 'source':
                //     if ($arr_fliter_value[1] != 'All') { 
                //         $query->orWhere('provider', '=', $arr_fliter_value[1]);
                //     }
                //     break;
                case 'daterange':
                    if (! empty($arr_fliter_value[1])) 
                    { 
                        $daterange = explode('+to+',$arr_fliter_value[1]);
                        $query = $query->where('invoice_date', '>=', $daterange[0] . " 00:00:00")
                            ->where('invoice_date', '<=', $daterange[1] . " 23:59:59");
                    }
                    $date_range = true;
                    // else {
                    //     $query = $query->where('invoice_date', '>=', Carbon::today()->subDays(15)->toDateString() . " 00:00:00")
                    //         ->where('invoice_date', '<=', Carbon::today()->toDateString() . " 23:59:59");
                    // }
                    break;                
                        
                default:
                # code...
                break;
            }            
        }

        if($date_range == false) {
            $query->where('invoice_date', '>=', Carbon::today()->subDays(30)->toDateString() . " 00:00:00")
                ->where('invoice_date', '<=', Carbon::today()->toDateString() . " 23:59:59");
        }

        $query->where('status', 'success')->where('gateway', '!=', 'Free');        
        
        return $query->orderBy('invoice_date', "DESC")->get();
    }

    /**
     * 
     */
    private function getReportFields($request)
    {
        $fields = [
            "Name" => "name",
            "Email" => "email", 
            "Billing City" => "billing_city", 
            "Billing State" => "billing_state", 
            "Billing Country" => "billing_country", 
            "Invoice Number" => "invoice_number",
            "Invoice Date" => "invoice_date", 
            "Title" => "product_id", 
            "Type" => "product_type", 
            "Currency" => "currency", 
            "Total" => "invoice_total", 
            "Gateway" => "gateway"
        ];
        
        return $fields;
    }
    
    /**
     * Formated user data
     */
    private function getFormattedReportData($fields, $items)
    {
       $data = [];
        foreach ($items as $item) {
            $row = [];
            foreach ($fields as $title => $field) {
                $row[$title] = $item->$field;
                if ($field == 'name') {
                    if (is_null($item->giftBy)) {
                        $name = $item->user->first_name;                        
                    } else {
                        $name = $item->sender->first_name;                        
                    }
                    $row[$title] = $name;
                }

                if ($field == 'email') {
                    if (is_null($item->giftBy)) {                        
                        $email = $item->user->email;
                    } else {
                        $email = $item->user->email;
                    }
                    $row[$title] = $email;
                }
                
                if ($field == 'product_id') {
                    $type = $item->product_type == 1 ? "Series" : "Episode";
                    $product = $item->product_type == 1 ? $item->show->title : $item->episode->title;
                    $row[$title] = $product;
                }

                if ($field == 'product_type') {
                    $type = $item->product_type == 1 ? "Series" : "Episode";
                    $row[$title] = $type;
                }

                if ($field == 'invoice_date') {
                    $row[$title] = Carbon::parse($item->invoice_date)->format('d/m/Y g:ia');
                }                               
            }
            $data[] = $row;
        }
        
        return $data;
    }
}
