<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Traits\CommonModuleMethods;

use Intervention\Image\Facades\Image;
use Validator;

use Carbon\Carbon;

use App\Models\ShowCategory;
use Auth;
use Illuminate\Support\Facades\Storage;

class ShowCategoryController extends Controller
{
    use CommonModuleMethods;


    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.show_category';

    /**
     * @var string
     */
    protected $upload_folder_name = "uploads/showcategory";

    /**
     * Helper for cover image dimension.
     *
     * @var array
     */
    protected $coverDimension = [
        ['width' => 100, 'height' => 100]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = ShowCategory::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
//                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'image' => empty($item->image) ? '' :
                    '<img src="'. $item->getImage(640, 480) .'" width="100px">',
                'status' => $item->status,
                'admin_status' => $item->admin_status,
            ];
            if(Auth::guard('admin')->user()->can('edit.show-category')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\ShowCategoryController@edit', $item->id) .'">Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Masthead $masthead
     * @return \Illuminate\Http\Response
     */
    public function create(ShowCategory $showcategory)
    {
        return $this->view('form', compact('showcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        $attributes = $request->only(['title']);
        
        // $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';
        $attributes['status'] = $request->status ? 'Published' : 'Draft';
        

        if ($request->has('images')) {
            $attributes['image'] = $request->images[0];
            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/showcategory/' . $filename);            
            $image_name = "showcategory/" . $filename;
            $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

            //delete image from local
            unlink(public_path('uploads/showcategory/' . $filename));
            unlink(public_path('uploads/showcategory/thumbs/100_100_' . $filename));            
        }

        ShowCategory::create($attributes);

        return redirect()->action('Admin\ShowCategoryController@index')
            ->with('success', 'Category Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Masthead $masthead
     * @return mixed
     */
    public function edit(ShowCategory $showcategory)
    {
        // dd($masthead);
        
        return $this->view('form', compact('showcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Masthead $masthead
     * @return \Illuminate\Http\Response
     * @internal param Masthead $masthead
     */
    public function update(Request $request, ShowCategory $showcategory)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), $this->updateActionRules($showcategory), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->only(['title']);
        
        // $attributes['status'] = $request->has('status') ? 'Published' : 'Draft';
        $attributes['status'] = $request->status ? 'Published' : 'Draft';

        
        if ($request->has('images')) {
            $attributes['image'] = $request->images[0];
            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/showcategory/' . $filename);            
            $image_name = "showcategory/" . $filename;
            $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

            //delete image from local
            unlink(public_path('uploads/showcategory/' . $filename));
            unlink(public_path('uploads/showcategory/thumbs/100_100_' . $filename));
        }

        $showcategory->fill($attributes);
        $showcategory->save();


        if ($request->has('edit_chords')) {
            foreach ($request->edit_chords as $edit_chord) {
                $this->updateCropImage($showcategory->image, json_decode($edit_chord));
            }
        }

        return redirect()->action('Admin\ShowCategoryController@index')
            ->with('success', 'Category Created Successfully.');
    }

    /**
     * Update the image crop.
     *
     * @param $image
     * @param $chords
     * @return bool
     */
    protected function updateCropImage($image, $chords) {
        Image::make('uploads/showcategory/' . $image)->crop((int) $chords->w, (int) $chords->h, (int) $chords->x, (int) $chords->y)
            ->fit((int) $chords->width, (int) $chords->height)
            ->save('uploads/showcategory/thumbs/'. $chords->width ."_" . $chords->height . "_" . $image);
        return true;
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $request->resource == 'Image' ? $this->imageUpload($request) : $this->fileUpload($request);
    }

    /**
     * For uploading images...
     *
     * @param Request $request
     * @return array
     */
    protected function imageUpload(Request $request) {
        $files = $request->file;

        $name_array = explode(".", $request->name);
        array_pop($name_array);
        $name = implode(".", $name_array);

        $uploaded_files = [];
        $this->forceDimension = [];

        if (empty($request->$name)) {
            $this->forceDimension = $this->coverDimension;
        }else{
            $chords = (object) $request->$name;
            $this->crop_chords = $chords;
        }

        if (is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleImageWithChords($file, $filename, $this->upload_folder_name, "thumbs");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleImageWithChords($files, $filename, $this->upload_folder_name, "thumbs");
        }

        return $uploaded_files;
    }

    /**
     * For uploading files...
     *
     * @param Request $request
     * @return array
     */
    protected function fileUpload(Request $request) {
        $files = $request->file;

        $uploaded_files = [];
        if(is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleFile($file, $filename, "uploads/showcategory/files");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleFile($files, $filename, "uploads/showcategory/files");
        }

        return $uploaded_files;
    }

    /**
     * Delete image page.
     *
     * @param Request $request
     * @param Masthead $masthead
     * @return array
     */
    public function deleteImage(Request $request, ShowCategory $showcategory) {
        switch ($request->type) {
            case 'Cover':
                if (Storage::disk('s3')->exists('showcategory/' . $showcategory->image)) {
                    Storage::disk('s3')->delete('showcategory/' . $showcategory->image);
                }
                
                $showcategory->image = "";
                $showcategory->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined image type to delete.'];
                break;

        }
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = ShowCategory::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = ShowCategory::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = ShowCategory::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = ShowCategory::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = ShowCategory::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = ShowCategory::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = ShowCategory::findOrFail($id);
        $item->delete();

        if (!empty($item->image)) {
            if (Storage::disk('s3')->exists('showcategory/' . $item->image)) {
                Storage::disk('s3')->delete('showcategory/' . $item->image);
            }                        
        }

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            'images' => ['required']
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required', 'min:3'],
        ];
    }
}
