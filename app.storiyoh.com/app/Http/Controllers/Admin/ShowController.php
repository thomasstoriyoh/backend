<?php

namespace App\Http\Controllers\Admin;

use App\Models\Show;
use App\Traits\CommonModuleMethods;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Tzsk\ScrapePod\Facade\ScrapePod;
use App\Models\ShowResponse;
use App\Models\ActiveShow;
use App\Jobs\ManualProcessEpisodeJob;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Jobs\UploadShowIdJob;
use App\Models\Country;
use App\Traits\Helper;
use Illuminate\Support\Facades\Log;
use App\Models\Episode;
use App\Models\Network;
use App\Models\ShowLanguage;
use Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Jobs\RemoveDuplicateEpisode;
use App\Models\ShowActiveHourly;
use App\Models\ShowActiveDaily;
use App\Models\ShowActiveDailyOnce;

use App\Jobs\ManualProcessEpisodeJobViaLocal;

class ShowController extends Controller
{
    use CommonModuleMethods;

    /**
     * @var string
     */
    protected $upload_folder_name = 'uploads/show_assets';
    
    /**
     * Helper for cover image dimension.
     *
     * @var array
     */
    protected $coverDimension = [
        ['width' => 600, 'height' => 600]
    ];

    /**
     * @var string
     */
    protected $viewRoot = 'admin.modules.show';

    /**
     * @return mixed
     */
    public function index()
    {
        //dd(Auth::guard('admin')->user()->id);
        $category_list = Category::orderBy('order')->orderBy('title')->get(['id', 'title']);
        //dd($category_list);
        return $this->view('index', compact('category_list'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request)
    {
        $query = Show::select('*');
        $query->where('content_type', '!=', 2);

        if (strlen($request->search['value']) > 1) {
            $query->where(function ($query) use ($request) {
                $query->orWhere('title', 'LIKE', '%' . $request->search['value'] . '%');
            });
        }

        if (!empty($request->custom_filter)) {
            $query->where(function ($query) use ($request) {
                list($field, $value) = explode('.', $request->custom_filter);
                if ($field == 'category_id') {
                    $query->orWhereHas('categories', function ($q) use ($value) {
                        $q->where('category_id', $value);
                    });
                } else {
                    if ($field == 'content_type') {
                        if ($value == 0) {
                            $query->orWhere('content_type', 0);
                        } else {
                            $query->orWhereIn('content_type', [1, 3]);
                        }
                    } else {
                        $query->orWhere($field, $value);
                    }
                }
            });
        }
        $query->orderBy('id', 'DESC');

        $request['page'] = 1;
        if ($request->start) {
            $request['page'] = ceil($request->start / $request->length) + 1;
        }

        //dd($query->toSql());
        $items = $query->paginate($request->length);

        $data = [];
        foreach ($items as $key => $item) {
            $image = !empty($item->image) ? $item->getImage(100) : asset('uploads/default/premium_show.jpg');
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'show_id' => $item->id,
                'title' => $item->title,
                'image' => '<img src="' . $image . '" width="100px" style="box-shadow:0 10px 16px 0 #CCC">',
                'featured' => !empty($item->featured) ? 'Yes' : 'No',
                'active' => !empty($item->active) ? 'Yes' : 'No',
                'status' => $item->status,
                'categories' => $item->categories()->pluck('title')->implode(', '),
                'episodes' => $item->episodes()->published()->count(),
                'content_type' => $item->content_type == 0 ? "No" : "Yes"
            ];
            $options = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="' . action('Admin\ShowController@show', $item->id) . '">View</a></li>';

            if (Auth::guard('admin')->user()->can('edit.show')) {
                if ($item->content_type == 0) {
                    if ($item->itunes_id == Null) {
                        $options .= '<li><a href="' . action('Admin\ShowController@edit', $item->id) . '">Update Show</a></li>';
                    }
                    if ($item->itunes_id) {
                        $options .= '<li><a href="javascript:void(0)" onclick="update_show(\'' . $item->id . '\',\'' . $item->itunes_id . '\')">Update Show</a></li>';
                    }
                    $options .= '<li><a href="javascript:void(0)" onclick="fetch_episode(\'' . $item->id . '\')">Fetch Episodes</a></li>';
                    $options .= '<li><a href="javascript:void(0)" onclick="remove_duplicate_episode(\'' . $item->id . '\')">Remove Duplicates</a></li>';
                }
            }

            $options .= '</ul>
                    </div>
                </div>';
            $row['options'] = $options;

            $data[] = $row;
        }

        return [
            'recordsTotal' => $items->total(),
            'recordsFiltered' => $items->total(),
            'data' => $data
        ];
    }
    /*public function ajaxIndex(Request $request)
    {        
        if (strlen($request->search['value']) > 1) {
            $query = Show::search($request->search['value']);
            if (!empty($request->custom_filter)) {                
                list($field, $value) = explode('.', $request->custom_filter);                
                if ($field == 'featured') {
                    $query = Show::select('*');
                    $query->orWhere($field, $value);
                } else {
                    if ($field == 'category_id') {
                        $query->where('category_ids', '"' . $value . '"');
                    }
                    if ($field == 'status') {
                        $query->where('status', strtolower($value));
                    }
                }
            }   
        } else {
            $query = Show::select('*');
            if (!empty($request->custom_filter)) {
                $query->where(function ($query) use ($request) {
                    list($field, $value) = explode('.', $request->custom_filter);
                    if ($field == 'category_id') {
                        $query->orWhereHas('categories', function ($q) use ($value) {
                            $q->where('category_id', $value);
                        });
                    } else {
                        $query->orWhere($field, $value);
                    }
                });
            }
            $query->orderBy('id', 'DESC');           
        }        
        
        $request['page'] = 1;
        if($request->start) {
            $request['page'] = ceil($request->start/$request->length) + 1;
        }
        
        $items = $query->paginate($request->length);

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'image' => empty($item->image) ? '' :
                    '<img src="' . $item->getImage(100) . '" width="100px" style="box-shadow:0 10px 16px 0 #CCC">',
                'featured' => !empty($item->featured) ? 'Yes' : 'No',
                'active' => !empty($item->active) ? 'Yes' : 'No',
                'status' => $item->status,
                'categories' => $item->categories()->pluck('title')->implode(', '),
                'episodes' => $item->episodes()->published()->count(),
                'content_type' => $item->content_type == 0 ? "No" : "Yes"
            ];
            $options = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li><a href="' . action('Admin\ShowController@show', $item->id) . '">View</a></li>';

            if (Auth::guard('admin')->user()->can('edit.show')) {
                if($item->itunes_id == Null) {
                    $options .= '<li><a href="' . action('Admin\ShowController@edit', $item->id) . '">Update Show</a></li>';
                }
                if ($item->itunes_id) {
                    $options .= '<li><a href="javascript:void(0)" onclick="update_show(\'' . $item->id . '\',\'' . $item->itunes_id . '\')">Update Show</a></li>';
                }
                $options .= '<li><a href="javascript:void(0)" onclick="fetch_episode(\'' . $item->id . '\')">Fetch Episodes</a></li>';                
                $options .= '<li><a href="javascript:void(0)" onclick="remove_duplicate_episode(\'' . $item->id . '\')">Remove Duplicates</a></li>';
            }

            $options .= '</ul>
                    </div>
                </div>';
            $row['options'] = $options;

            $data[] = $row;
        }

        return [
            'recordsTotal' => $items->total(),
            'recordsFiltered' => $items->total(),
            'data' => $data
        ];
    }*/

    /**
     * @param Show $show
     * @return mixed
     */
    public function show(Show $show)
    {
        return $this->view('show', compact('show'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function publish(Request $request)
    {
        foreach ($request->ids as $id) {
            $item = Show::findOrFail($id);
            $this->publishModule($item);
            
            if ($item->content_type == 0) {
                $item->searchable();
            }

            // $episodes = Episode::where('show_id', $id)->get();
            // foreach($episodes as $episode) {
            //     try {
            //         $episode->fill(['status' => 'Published'])->save();
            //         $episode->searchable();
            //     } catch(\Exception $e) {
                    
            //     }                
            // }
        }

        return ['status' => true];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function draft(Request $request)
    {
        foreach ($request->ids as $id) {
            $item = Show::findOrFail($id);
            $this->unpublishModule($item);

            $item->unsearchable();

            $episodes = Episode::where('show_id', $id)->get();
            foreach($episodes as $episode) {
                try {
                    $episode->fill(['status' => 'Draft'])->save();
                    $episode->unsearchable();
                } catch (\Exception $e) {
                }
            }
        }

        return ['status' => true];
    }

    /////////////////// 08112017 ////////////////////////////////

    /**
     * Show the form for creating a new resource.
     *
     * @param Show $show
     * @return \Illuminate\Http\Response
     */
    public function search_podcast_data(Show $show)
    {
        return $this->view('search-form', compact('show'));
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function searchPodcast(Request $request)
    {
        if ($request->has('search_key')) {
            session()->forget('Itunes.Search');

            list($keyword, $ituneId) = explode('##', $request->search_key);

            if (!empty($ituneId)) { //Search with itunes ID
                $response = ScrapePod::original()->find($ituneId);//"936914258");
            } elseif (!empty($keyword)) {
                $response = ScrapePod::original()->search($keyword);
            }

            session()->put('Itunes.Search', $response);
        }

        return ['status' => true, 'data' => $this->view('search')->render()];
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function store(Request $request)
    {
        //dd($request->all(), session()->get('Itunes.Search'));
        $attributes = $request->all();

        if (empty($attributes['ituneIDs'])) {
            return redirect()->back()->withInput($request->all())->withErrors(['error' => 'Please select atleast one podcast.']);
        }

        if (session()->get('Itunes.Search')) {
            foreach (session()->get('Itunes.Search')['data']->results as $key => $value) {
                if (@in_array($value->collectionId, $attributes['ituneIDs'])) {
                    $active = 'No';
                    if (@in_array($value->collectionId, $attributes['active'])) {
                        $active = 'Yes';
                    }

                    //Found Feed URl Length Code Start
                    $content_length = 0;
                    /*if ($value->feedUrl) {                        
                        $process = new Process('curl -s ' . $value->feedUrl . ' | wc -c');
                        $process->run();

                        // executes after the command finishes
                        if (!$process->isSuccessful()) {
                            throw new ProcessFailedException($process);
                        }
                        
                        $content_length = $process->getOutput();
                        //dump($content_length);
                        if ($content_length) {
                            if (strlen($content_length) > 50) {
                                $content_length = 0;
                            }
                        }
                    }*/
                    //Found Feed URl Length Code End

                    //Create Image on S3 Bucket Server
                    $imageData = Helper::createPodcastImage($value->collectionId, @$value->artworkUrl600);
                    
                    $data = [
                        'itunes_id' => $value->collectionId,
                        'title' => $value->collectionName,
                        'full_image' => @$value->artworkUrl600,
                        'image' => $imageData,
                        'feed_url' => @$value->feedUrl,
                        'artist_name' => @$value->artistName,
                        'collection_name' => @$value->collectionName,
                        'collection_censored_name' => @$value->collectionCensoredName,
                        'country' => @$value->country,
                        //'content_length' => @$content_length ? $content_length : 0,
                        'content_length' => 0,
                        'created_by' => Auth::guard('admin')->user()->id,
                        'updated_by' => Auth::guard('admin')->user()->id
                    ];
                    
                    //Insert into tbl_shows table
                    $show = Show::firstOrNew(['itunes_id' => $value->collectionId]);
                    //$newOrOld = $show->exists;
                    $show->fill($data)->save();                    

                    // If country not found we will create new entry
                    try {
                        $countryData = Country::firstOrNew(['countries_iso_code_3' => strtoupper($value->country)]);
                        $countryData->fill(['countries_iso_code_3' => strtoupper($value->country)])->save();
                    } catch (\Exception $e) {
                    }

                    try {
                        //Insert Categories
                        $categories = $this->getCategories($value->genres);

                        //Sync Categories with Shows
                        $categories = array_filter($categories);
                        $show->categories()->sync($categories);
                    } catch (\Exception $e) {
                    }

                    $show->searchable();

                    //Also insert into tbl_show_responses table
                    $response = ShowResponse::firstOrNew(['id' => $value->collectionId]);
                    $response->fill(['processed' => 1, 'processed_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'), 'data' => json_encode($value)])->save();

                    //Also added into tbl_show_active
                    if (@in_array($value->collectionId, $attributes['active'])) {
                        $responseActive = ActiveShow::firstOrNew(['show_id' => $show->id]);                        
                        $responseActive->fill([
                            'show_id' => $show->id, 
                            'itunes_id' => @$value->collectionId, 
                            'feed_url' => @$value->feedUrl, 
                            //'content_length' => @$content_length ? $content_length : 0,
                            'content_length' => 0,
                            'cron_run_at' => Carbon::now()->format('Y-m-d')
                        ])->save();
                    }

                    //Insert into Job Table
                    //if(! $newOrOld) {
                    $userInfo = Auth::guard('admin')->user() ? Auth::guard('admin')->user()->id : 1;
                    dispatch(new ManualProcessEpisodeJob($show->id, 'new', $userInfo));
                    //}
                }
            }

            //Unset Session
            session()->forget('Itunes.Search');

            return redirect()->action('Admin\ShowController@index')
                ->with('success', 'Show(s) Created Successfully.');
        } else {
            return redirect()->back()->withInput($request->all())
                ->withErrors(['erroe' => 'Please select atleast one podcast.']);
        }
    }

    /**
     *
     * @param Request $request
     */
    public function update_shows (Request $request)
    {
        //Cache::forget('manual.shows.' . $request->show_id);

        if (Cache::get('manual.shows.' . $request->show_id)) {
            return ['status' => false, 'message' => 'We are still updating show. Please try again after some time.'];
        }

        $expiresAt = Carbon::now()->addMinutes(5);

        //Cache::put('manual.shows.' . $request->show_id, 1, $expiresAt);

        $response = ScrapePod::original()->find($request->itune_id);

        //dd($response);

        try {
            $value = $response['data']->results;
            $userInfo = Auth::guard('admin')->user() ? Auth::guard('admin')->user()->id : 1;
            if (count($value) > 0) {                                
                $data = [
                    'title' => $value[0]->collectionName,
                    'full_image' => @$value[0]->artworkUrl600,
                    'feed_url' => @$value[0]->feedUrl,
                    'artist_name' => @$value[0]->artistName,
                    'collection_name' => @$value[0]->collectionName,
                    'collection_censored_name' => @$value[0]->collectionCensoredName,
                    'country' => @$value[0]->country,
                    'created_by' => $userInfo,
                    'updated_by' => $userInfo
                ];

                //Insert into tbl_shows table
                $show = Show::find($request->show_id);

                //Create or Update Image
                // $imageData = Helper::createPodcastImage($request->itune_id, @$value[0]->artworkUrl600);
                // if (!empty($imageData)) {
                //     if (Storage::disk('s3')->exists('shows/' . $show->image)) {
                //         Storage::disk('s3')->delete('shows/' . $show->image);
                //     }
                //     $data['image'] = $imageData;
                // }                

                $responseData = ScrapePod::original()->feed($show->feed_url);

                $data['description'] = $show->description;
                if(! empty($responseData['data'])) {
                    if(! empty($responseData['data']['description'])) {
                        $data['description'] = @$responseData['data']['description'];
                    } else if(! empty($responseData['data']['summary'])) {
                        $data['description'] = @$responseData['data']['summary'];
                    }
                }
                
                //Create Image from Episode
                if (!empty($responseData['data'])) {
                    
                    $episode_image = $responseData['data']['image'];

                    $episode_image = Image::make($episode_image)->fit(600, 600, function ($img) {
                        $img->upsize();
                    });

                    $image_name = date('YmdHis') . rand(100000, 999999) . '.jpg';

                    $file = public_path('uploads/show_assets/' . $image_name);

                    // finally we save the image as a new file
                    $episode_image->save($file);

                    Storage::disk('s3')->put("shows/".$image_name, file_get_contents($file)); 

                    if (Storage::disk('s3')->exists('shows/' . $image_name)) {
                        if (Storage::disk('s3')->exists('shows/' . $show->image)) {
                            Storage::disk('s3')->delete('shows/' . $show->image);
                        }
                    }

                    //Delete from local storage
                    if (file_exists($file)) {
                        unlink($file);
                    }

                    $data['image'] = $image_name;
                }

                $show->fill($data)->save();

                // If country not found we will create new entry
                try {
                    $countryData = Country::firstOrNew(['countries_iso_code_3' => strtoupper($value[0]->country)]);
                    $countryData->fill(['countries_iso_code_3' => strtoupper($value[0]->country)])->save();
                } catch (\Exception $e) {
                    Log::info('Country not found.' . $value[0]->collectionId);
                }

                try {
                    //Insert Categories
                    $categories = $this->getCategories($value[0]->genres);

                    //Sync Categories with Shows
                    $categories = array_filter($categories);
                    $show->categories()->sync($categories);
                } catch (\Exception $e) {
                    Log::info('Genre not found.' . $value[0]->collectionId);
                }

                //Also insert into tbl_show_responses table
                $response = ShowResponse::firstOrNew(['id' => $value[0]->collectionId]);
                $response->fill(['data' => json_encode($value[0])])->save();

                Cache::forget('manual.shows.' . $request->show_id);

                $show->searchable();
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            Log::info('Fecth Show from Itunes');
            $show->fill(['status' => 'Draft'])->save();
            $show->unsearchable();
        }

        return ['status' => true, 'message' => 'We are updating Show info.'];
    }

    /**
     *
     * @param type $data
     * @param type $parent_id
     * @return type
     */
    protected function getCategories ($data, $parent_id = null)
    {
        $categories = [];
        if (count($data) > 0) {
            foreach ($data as $apiCategory) {
                $category = Category::firstOrNew(['title' => trim($apiCategory)]);
                $category->fill(['parent_id' => $parent_id, 'title' => trim($apiCategory)])->save();

                $categories = array_merge($categories, [$category->id]);

                //$children = $this->getCategories($apiCategory['children'], $category->id);
                //$categories = array_merge($categories, $children);
            }
        }

        return $categories;
    }

    /**
     *
     * @param Request $request
     */
    public function fetch_episodes(Request $request)
    {
        if (Cache::get('manual.episode.' . $request->show_id)) {
            return ['status' => false, 'message' => 'We are still fetching all episodes from this show. Please try again after some time.'];
        }

        $expiresAt = Carbon::now()->addMinutes(5);

        Cache::put('manual.episode.' . $request->show_id, 1, $expiresAt);

        //$response = ScrapePod::original()->find($request->show_id);

        //Remove from show_active_hourly table
        $ShowItem = ShowActiveHourly::where("show_id", $request->show_id)->first();
        if($ShowItem) {                
            $ShowItem->delete();
        }
        
        //Remove from show_active_daily table
        $ShowItem2 = ShowActiveDaily::where("show_id", $request->show_id)->first();            
        if($ShowItem2) {
            $ShowItem2->delete();
        }

        //Remove from show_active_daily_once table
        $ShowItem3 = ShowActiveDailyOnce::where("show_id", $request->show_id)->first();
        if($ShowItem3) {
            $ShowItem3->delete();
        }

        $userInfo = Auth::guard('admin')->user() ? Auth::guard('admin')->user()->id : 1;
        dispatch(new ManualProcessEpisodeJob($request->show_id, 'manual', $userInfo));

        return ['status' => true, 'message' => 'We are fetching all episodes from this show.'];
    }

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    // public function upload(Request $request)
    // {
    //     return $this->csvUpload($request);
    // }

    /**
     * Upload from csv file
     * @param Request $request
     * @return array
     */
    protected function csvUpload($request)
    {
        $file = $request->file->getPathname();
        $all_values = [];
        $handle = fopen($file, 'r');
        while (!feof($handle)) {
            $model = fgetcsv($handle);
            if (is_array($model)) {
                // if (!empty($model[0])) {
                //     $array = explode('/', $model[0]);
                //     $itunesId = explode('id', $array[count($array) - 1]);
                //     if (!empty($itunesId)) {
                //         $all_values[] = $itunesId[count($itunesId) - 1];
                //     }
                // }
                if (!empty($model[0])) {
                    //$array = explode('/', $model[1]);
                    $itunesId = trim($model[0]);//explode('id', $array[count($array) - 1]);
                    if (!empty($itunesId)) {
                        $all_values[] = $itunesId;//$itunesId[count($itunesId) - 1];
                    }
                }
            }
        }
        fclose($handle);

        //dd($all_values);

        if (count($all_values) > 0) {
            dispatch(new UploadShowIdJob($all_values));
        }

        return [['status' => true]];
    }

    ######################################### 02-05-2018 ################################################

    /**
     * Upload files for resource.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        return $request->resource == 'Image' ? $this->imageUpload($request) : $this->csvUpload($request);
    }

    /**
     * For uploading images...
     *
     * @param Request $request
     * @return array
     */
    protected function imageUpload(Request $request) {
        
        $files = $request->file;

        $name_array = explode(".", $request->name);
        array_pop($name_array);
        $name = implode(".", $name_array);

        $uploaded_files = [];
        $this->forceDimension = [];

        if (empty($request->$name)) {
            $this->forceDimension = $this->coverDimension;
        }else{
            $chords = (object) $request->$name;
            $this->crop_chords = $chords;
        }

        if (is_array($files)) {
            foreach ($files as $file) {
                $filename = date('YmdHis').rand(100000, 999999);
                $uploaded_files[] = $this->uploadModuleImageWithChords($file, $filename, $this->upload_folder_name, "thumbs");
            }
        }else{
            $filename = date('YmdHis').rand(100000, 999999);
            $uploaded_files[] = $this->uploadModuleImageWithChords($files, $filename, $this->upload_folder_name, "thumbs");
        }

        return $uploaded_files;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Show $show
     * @return \Illuminate\Http\Response
     */
    public function create(Show $show)
    {
        $network_list = Network::published()->orderBy('title', 'ASC')->pluck('title', 'id')->all();
        $language_list = ShowLanguage::where('status', 'Published')
            ->whereNotNull('title')->orderBy('title', 'ASC')->pluck('title', 'short_code')->all();

        $country_list = Country::orderBy('title', 'ASC')->pluck('title', 'countries_iso_code_3')->all();

        $categories = Category::orderBy('title', 'ASC')->pluck('title', 'id')->all();

        return $this->view('form', compact('show', 'categories', 'network_list', 'language_list', 'country_list'));
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function add_show(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        if (! $request->has('category_id')) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'Please select atleast one category.']);
        }

        //Now check if this feed url already exits
        $feed_url_count = Show::where('feed_url', $request->feed_url)->count();
        if($feed_url_count) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'This feed url already exist in our database.']);
        }

        $attributes = $request->only([
            'title', 'collection_name', 'collection_censored_name', 'artist_name', 'network_id', 
            'language', 'country', 'description', 'feed_url', 'web_url'
        ]);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';        

        if ($request->has('images')) {
            
            $attributes['image'] = $request->images[0];

            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/show_assets/' . $filename); 
            if (file_exists($full_path)) {
                $image_name = 'shows/' . $filename;
                $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));

                //delete image from local
                unlink(public_path('uploads/show_assets/' . $filename));
                unlink(public_path('uploads/show_assets/thumbs/600_600_' . $filename));
            }
        }

        $result = Show::create($attributes);

        //added into categories table
        $sync_array = $request->category_id;
        $result->categories()->sync($sync_array);

        //Now Added into Elasticsearch
        if($attributes['status'] == "Published") {
            $result->searchable();
        }
        
        //Fetching Podcast episodes        
        $expiresAt = Carbon::now()->addMinutes(5);
        
        Cache::put('manual.episode.' . $result->id, 1, $expiresAt);

        $userInfo = Auth::guard('admin')->user() ? Auth::guard('admin')->user()->id : 1;

        dispatch(new ManualProcessEpisodeJob($result->id, 'manual', $userInfo));
        
        return redirect()->action('Admin\ShowController@index')
            ->with('success', 'Podcast Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Show $show
     * @return mixed
     */
    public function edit(Show $show)
    {
        $network_list = Network::published()->orderBy('title', 'ASC')->pluck('title', 'id')->all();
        $language_list = ShowLanguage::where('status', 'Published')
            ->whereNotNull('title')->orderBy('title', 'ASC')->pluck('title', 'short_code')->all();

        $country_list = Country::orderBy('title', 'ASC')->pluck('title', 'countries_iso_code_3')->all();

        $categories = Category::orderBy('title', 'ASC')->pluck('title', 'id')->all();

        return $this->view('form', compact('show', 'categories', 'network_list', 'language_list', 'country_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Show $show
     * @return \Illuminate\Http\Response
     * @internal param Masthead $masthead
     */
    public function update(Request $request, Show $show)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($show), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        if (! $request->has('category_id')) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'Please select atleast one category.']);
        }

        //Now check if this feed url already exits
        $feed_url_count = Show::where('feed_url', $request->feed_url)->where('id', '!=', $show->id)->count();
        if($feed_url_count) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'This feed url already exist in our database.']);
        }

        $attributes = $request->only([
            'title', 'collection_name', 'collection_censored_name', 'artist_name', 'network_id', 
            'language', 'country', 'description', 'feed_url', 'web_url'
        ]);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        if ($request->has('images')) {
            
            $attributes['image'] = $request->images[0];

            //upload image to s3 bucket
            $filename = $request->images[0];
            $full_path = public_path('uploads/show_assets/' . $filename);            
            
            if (file_exists($full_path)) {
                $image_name = "shows/" . $filename;
                $file_full_path = Storage::disk('s3')->put($image_name, file_get_contents($full_path));
                
                //delete image from local
                unlink(public_path('uploads/show_assets/' . $filename));
                unlink(public_path('uploads/show_assets/thumbs/600_600_' . $filename));
            }            
        }

        //Update Show info
        $show->fill($attributes);
        $show->save();
        
        //added into categories table
        $sync_array = $request->category_id;
        $show->categories()->sync($sync_array);

        //Now Update Elasticsearch
        if($show->status == "Published") {
            $show->searchable();
        } 

        return redirect()->action('Admin\ShowController@index')
            ->with('success', 'Show Edited Successfully.');
    }

    /**
     * Delete image page.
     *
     * @param Request $request
     * @param Masthead $masthead
     * @return array
     */
    public function deleteImage(Request $request, Show $show) {
        switch ($request->type) {
            case 'Cover':

                if (Storage::disk('s3')->exists('shows/' . $show->image)) {
                    Storage::disk('s3')->delete('shows/' . $show->image);
                }
                
                $show->image = "";
                $show->save();

                return ['status' => true];
                break;

            default :
                return ['status' => false, 'message' => 'Undefined image type to delete.'];
                break;
        }
    }

    /**
     * Fetured action.
     * 
     * @param Request $request
     * @return array
     */
    public function featured(Request $request) {
        $total = Show::featured()->count();
        $featured_count = Show::featured()->whereNotIn('id', $request->ids)->count();
        if (($featured_count + count($request->ids)) > 5) {
            return ['status' => false, 
                'message' => 'You cannot feature more than 5 item(s). You already have ' . $total 
                    . ' item(s) in your featured list.'];
        }
        
        foreach ($request->ids as $id) {
            $item = Show::findOrFail($id);
            $item->setEventType('Featured');
            $item->featured = true;
            $item->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Unfetured action.
     * 
     * @param Request $request
     * @return array
     */
    public function unfeatured(Request $request) {        
        foreach ($request->ids as $id) {
            $item = Show::findOrFail($id);
            $item->setEventType('Unfeatured');
            $item->featured = false;
            $item->save();
        }
        
        return ['status' => true];
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

     /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
            'category_id' => ['required'],
            'feed_url' => ['required'],
            'collection_name' => ['required'],
            'collection_censored_name' => ['required'],
            'artist_name' => ['required'],
            'country' => ['required'],
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'title' => ['required', 'min:3'],
            'category_id' => ['required'],
            'feed_url' => ['required'],
            'collection_name' => ['required'],
            'collection_censored_name' => ['required'],
            'artist_name' => ['required'],
            'country' => ['required'],            
        ];
    }

    /**
     *
     * @param Request $request
     */
    public function remove_duplicate_episode(Request $request)
    {
        //Check if this job is still running
        if (\Cache::get('duplicate.episode.' . $request->show_id)) {
            return ['status' => false, 'message' => 'We are still removing duplicate episodes from this show. Please try again after some time.'];
        }

        //Set cache for 5 minutes
        $expiresAt = Carbon::now()->addMinutes(5);
        \Cache::put('duplicate.episode.' . $request->show_id, 1, $expiresAt);

        //Removing all duplicate episodes from show
        dispatch(new RemoveDuplicateEpisode($request->show_id));

        return ['status' => true, 'message' => 'We are removing all duplicate episodes from this show.'];
    }

    /**
     *
     * @param Request $request
     */
    public function fetch_episodes_via_local(Request $request)
    {
        $show_data = Show::find($request->show_id, ['itunes_id']);
        $filepath = public_path($show_data->itunes_id);

        //dd($filepath);
        if (Cache::get('manual.episode.' . $request->show_id)) {
            return ['status' => false, 'message' => 'We are still fetching all episodes from this show. Please try again after some time.'];
        }

        $expiresAt = Carbon::now()->addMinutes(5);

        Cache::put('manual.episode.' . $request->show_id, 1, $expiresAt);
        
        //Remove from show_active_hourly table
        $ShowItem = ShowActiveHourly::where("show_id", $request->show_id)->first();
        if($ShowItem) {                
            $ShowItem->delete();
        }
        
        //Remove from show_active_daily table
        $ShowItem2 = ShowActiveDaily::where("show_id", $request->show_id)->first();            
        if($ShowItem2) {
            $ShowItem2->delete();
        }

        //Remove from show_active_daily_once table
        $ShowItem3 = ShowActiveDailyOnce::where("show_id", $request->show_id)->first();
        if($ShowItem3) {
            $ShowItem3->delete();
        }

        $userInfo = Auth::guard('admin')->user() ? Auth::guard('admin')->user()->id : 1;
        dispatch(new ManualProcessEpisodeJobViaLocal($request->show_id, 'manual', $userInfo, $filepath));

        return ['status' => true, 'message' => 'We are fetching all episodes from this show.'];
    }
}
