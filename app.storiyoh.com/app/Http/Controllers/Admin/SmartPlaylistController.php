<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Module;
use App\Models\SmartList;
use App\Traits\CommonModuleMethods;
use Validator;

class SmartPlaylistController extends Controller
{
    use CommonModuleMethods;
    
    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.smart-playlist';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();        
        return $this->view('index', compact('module_details'));
    }
    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = SmartList::select('*');
        $query->where('private', 'N');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
    //                $query->orWhere('slug', 'LIKE', '%'. $request->search['value'] .'%');
                //$query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            if($request->columns[$order['column']]['data'] == 'order') {
                continue;
            }

            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->has('shows')->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'no_of_shows' => $item->shows->count(),
                'user_id' => $item->user->full_name,
                'image' => empty($item->image) ? '' : 
                    '<img src="' . $item->getImage(100) . '" width="100px" style="box-shadow:0 10px 16px 0 #CCC">',
                'featured' => !empty($item->featured) ? 'Yes' : 'No',
                'created_at' => \Carbon\Carbon::parse($item->created_at)->format('d/m/Y')
            ];
            // if(Auth::guard('admin')->user()->can('edit.faq')) {
            //     $row['options'] = '<div class="dropdown">
            //         <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
            //             <span class="glyph-icon icon-align-justify"></span>
            //             <i class="glyph-icon icon-angle-down"></i>
            //         </a>
            //         <div class="dropdown-menu float-right">
            //                 <ul class="reset-ul mrg5B">
            //                     <li><a href="'. action('Admin\PlaylistController@edit', $item->id) .'">Edit</a></li>
            //                 </ul>
            //         </div>
            //     </div>';
            // }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    } 

    /**
     * Fetured action.
     * 
     * @param Request $request
     * @return array
     */
    public function featured(Request $request) {
        $total = SmartList::featured()->count();
        $featured_count = SmartList::featured()->whereNotIn('id', $request->ids)->count();
        if (($featured_count + count($request->ids)) > 5) {
            return ['status' => false, 
                'message' => 'You cannot feature more than 5 item(s). You already have ' . $total 
                    . ' item(s) in your featured list.'];
        }
        
        foreach ($request->ids as $id) {
            $item = SmartList::findOrFail($id);
            //$item->setEventType('Featured');
            $item->featured = true;
            $item->save();
        }
        
        return ['status' => true];
    }

    /**
     * Unfetured action.
     * 
     * @param Request $request
     * @return array
     */
    public function unfeatured(Request $request) {        
        foreach ($request->ids as $id) {
            $item = SmartList::findOrFail($id);
            //$item->setEventType('Unfeatured');
            $item->featured = false;
            $item->save();
        }
        
        return ['status' => true];
    }
}
