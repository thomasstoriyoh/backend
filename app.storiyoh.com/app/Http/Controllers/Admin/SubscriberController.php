<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Models\Subscriber;
use App\Traits\CommonModuleMethods;
use Intervention\Image\Facades\Image;
use Validator;
use Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\EventDispatcher\Tests\Debug\EventSubscriber;

class SubscriberController extends Controller
{
    use CommonModuleMethods;
        
    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.subscriber';    

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index() {
        $module_details = Module::whereController(class_basename(self::class))->first();
        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = Subscriber::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('email', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

        if (! empty($request->custom_filter)) {
            $query->where(function($query) use ($request){
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode(".", $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            //$query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'email' => $item->email,
                'subscription_status' => $item->subscription_status ? "Active" : "Inactive"
            ];
            
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Subscriber $Subscriber
     * @return \Illuminate\Http\Response
     */
    public function create(Subscriber $subscriber)
    {
        return $this->view('form', compact('subscriber'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        
        $attributes = $request->only(['email']);
          
        $checkEmail = Subscriber::where('email', $request->email)->count();

        if($checkEmail) {
            return redirect()->back()->withInput($request->all())->withErrors(["error" => 'This email is already exist.']);
        }

        $attributes['source'] = "web";

        $result = Subscriber::create($attributes);

        return redirect()->action('Admin\SubscriberController@index')
            ->with('success', 'Subscriber Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Subscriber $subscriber
     * @return mixed
     */
    public function edit(Subscriber $subscriber)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Subscriber $subscriber
     * @return \Illuminate\Http\Response
     * @internal param Subscriber $subscriber
     */
    public function update(Request $request, Subscriber $subscriber)
    {
        
    }
    
    /**
     * Active action.
     * 
     * @param Request $request
     * @return array
     */
    public function active(Request $request) {        
        foreach ($request->ids as $id) {
            $item = Subscriber::findOrFail($id);
            $item->setEventType('Active');
            $item->subscription_status = 1;
            $item->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Inactive action.
     * 
     * @param Request $request
     * @return array
     */
    public function inactive(Request $request) {        
        foreach ($request->ids as $id) {
            $item = Subscriber::findOrFail($id);
            $item->setEventType('Inactive');
            $item->subscription_status = 0;
            $item->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {
        foreach ($request->ids as $id) {
            $item = Subscriber::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = Subscriber::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }        
        
    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {

        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = Subscriber::findOrFail($id);
        $item->delete();

        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'email' => ['required', 'email']      
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return [
            'email' => ['required', 'email']            
        ];
    }
}
