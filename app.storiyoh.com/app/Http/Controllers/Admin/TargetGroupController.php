<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Module;
use App\Traits\CommonModuleMethods;
use Illuminate\Support\Facades\Auth;

use App\Models\TargetGroup;
use Illuminate\Support\Facades\Validator;
use App\Models\Country;

class TargetGroupController extends Controller
{
    use CommonModuleMethods;
    
    protected $last_active = [
        "" => "Select...", 
        '3' => 'Last 3 Days', 
        '7' => 'Last 7 Days', 
        '15' => "Last 15 Days", 
        '30' => 'Last 1 Month'
    ];
    
    protected $os_array = [
        "" => "Both", 
        "android" => "Android", 
        "ios" => "iOS"
    ];
    
    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.target-group';
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $module_details = Module::whereController('TargetGroupController')->first();
        return $this->view('index', compact('module_details'));
    }
    
    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request) {
        $query = TargetGroup::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function($query)use ($request) {
                $query->orWhere('title', 'LIKE', '%'. $request->search['value'] .'%');
                $query->orWhere('status', 'LIKE', '%'. $request->search['value'] .'%');
            });
        }

//        if (! empty($request->custom_filter)) {
//            $query->where(function($query) use ($request){
//                foreach ($request->custom_filter as $filter) {
//                    list($field, $value) = explode(".", $filter);
//                    $query->orWhere($field, $value);
//                }
//            });
//        }
        
        if(!empty($request->custom_filter)) {
            $custom_filters = [];
            foreach ($request->custom_filter as $filter) {
                list($field, $value) = explode(".", $filter);
                $custom_filters[$field][] = $value;
            }

            $query->where(function($query) use ($custom_filters) {
                foreach ($custom_filters as $key => $custom_filter) {
                    $query->whereIn($key, $custom_filter);
                }
            });
        }

        $query->orderBy('order');
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        
        $total_query = clone $query;
        $query->take($request->length)->skip($request->start);
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            
            if(count(json_decode($item->country))) {
                $country_name = Country::whereIn('id', json_decode($item->country))->pluck('title')->all();
                $country_name = @implode(", ", $country_name);
            } else {
                $country_name = "No Country";
            }
            
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'title' => $item->title,
                'country' => $country_name,
                'os' => $this->os_array[$item->os],
                'last_active' => $item->last_active ? $this->last_active[$item->last_active] : "-",
                'status' => $item->status,
                'admin_status' => $item->admin_status,
            ];
            if(Auth::guard('admin')->user()->can('edit.target-group')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="'. action('Admin\TargetGroupController@edit', $item->id) .'"><i class="glyph-icon icon-edit"></i> Edit</a></li>
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $total_query->count(),
            'recordsFiltered' => $total_query->count(),
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param TargetGroup $vehicle-type
     * @return \Illuminate\Http\Response
     */
    public function create(TargetGroup $target_group) {
        
        $country_array = [];                
        $country_array = Country::orderBy('title', 'ASC')->pluck('title', 'id')->all();
        
        $os_array = $this->os_array;
        $last_active = $this->last_active;
        
        return $this->view('form', compact('target_group', 'country_array', 'os_array', 'last_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $validator = Validator::make($request->all(), $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            if ($request->ajax()) {
                return ['status' => false, 'message' => "At least one of the fields are required. "];
            }
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }
        $attributes = $request->all(['title']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        
        $attributes['country'] = $request->has('location') ? json_encode($request->location) : "[]";
        
        $target_group = TargetGroup::create($attributes);
        
        if ($request->ajax()) {
            return ['status' => true, 'data' => $target_group->toArray()];
        }
        
        return redirect()->action('Admin\TargetGroupController@index')
            ->with('success', 'Target Group Created Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TargetGroup $target_group
     * @return \Illuminate\Http\Response
     * @internal param TargetGroup $module
     */
    public function edit(TargetGroup $target_group) {
        
        $country_array = Country::orderBy('title', 'ASC')->pluck('title', 'id')->all();        
        $os_array = $this->os_array;
        $last_active = $this->last_active;
        
        return $this->view('form', compact('target_group', 
            'country_array', 'os_array', 'last_active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param TargetGroup $target_group
     * @return \Illuminate\Http\Response
     * @internal param TargetGroup $module
     */
    public function update(Request $request, TargetGroup $target_group) {
        
        $validator = Validator::make($request->all(), $this->updateActionRules($target_group), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->all(['title']);
        
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';
        $attributes['country'] = $request->has('location') ? json_encode($request->location) : "[]";
        //dd($attributes);
        $target_group->fill($attributes)->save();
        
        return redirect()->action('Admin\TargetGroupController@index')
            ->with('success', 'Target Group Edited Successfully.');
    }
    
    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request) {

        foreach ($request->ids as $id) {
            $item = TargetGroup::findOrFail($id);
            $this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request) {
        foreach ($request->ids as $id) {
            $item = TargetGroup::findOrFail($id);
            $this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request) {
        foreach ($request->ids as $id) {
            $item = TargetGroup::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request) {

        foreach ($request->ids as $id) {
            $item = TargetGroup::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder() {
        $items = TargetGroup::published()->approved()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request) {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = TargetGroup::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request) {
        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        $item = TargetGroup::findOrFail($id);
        $item->delete();
        return true;
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages() {
        return [

        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules() {
        return [
            'title' => ['required', 'min:3'],
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item) {
        return $this->storeActionRules();
    }
}
