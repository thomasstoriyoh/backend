<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Module;
use App\Models\User;
use App\Models\Country;
use App\Traits\CommonModuleMethods;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use App\Models\PushId;
use App\Models\Last_synch;
use Carbon\Carbon;

class UserController extends Controller
{
    use CommonModuleMethods;

    /**
     * Root directory for all the view files.
     *
     * @var string
     */
    protected $viewRoot = 'admin.modules.users';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $module_details = Module::whereController(class_basename(self::class))->first();

        return $this->view('index', compact('module_details'));
    }

    /**
     * Resource Listing Ajax Page.
     *
     * @param Request $request
     * @return array
     */
    public function ajaxIndex(Request $request)
    {
        $query = User::select('*');
        if (strlen($request->search['value']) > 1) {
            $query->where(function ($query) use ($request) {
                $query->orWhere('first_name', 'LIKE', '%' . $request->search['value'] . '%');
                $query->orWhere('username', 'LIKE', '%' . $request->search['value'] . '%');
                $query->orWhere('email', 'LIKE', '%' . $request->search['value'] . '%');
                //$query->orWhere('status', 'LIKE', '%' . $request->search['value'] . '%');
            });
        }

        if (!empty($request->custom_filter)) {
            $query->where(function ($query) use ($request) {
                foreach ($request->custom_filter as $filter) {
                    list($field, $value) = explode('.', $filter);
                    $query->orWhere($field, $value);
                }
            });
        }

        $count = $query->count();

        $query->orderBy('order')->take($request->length)->skip($request->start);
        foreach ($request->order as $order) {
            $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
        }
        
        $items = $query->latest()->get();

        $data = [];
        foreach ($items as $key => $item) {
            // $platform = $item->pushIds()->pluck('platform')->all();
            // $platformData = 'None';
            // if (count($platform) > 0) {
            //     $platform = array_unique($platform);
            //     $platformData = @implode(', ', $platform);
            // }
            $row = [
                'id' => $item->id,
                'order' => $key + 1,
                'full_name' => $item->first_name . ' ' . $item->last_name,
                'user_name' => $item->username,
                'email' => $item->email,
                'provider' => $item->provider,
                'platform' => $item->registered_platform,
                'type' => $item->user_type == 0 ? "Guest" : "Registered",
                'verified' => $item->verified
            ];
            if (Auth::guard('admin')->user()->can('edit.users')) {
                $row['options'] = '<div class="dropdown">
                    <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                        <span class="glyph-icon icon-align-justify"></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                            <ul class="reset-ul mrg5B">
                                <li><a href="' . action('Admin\UserController@show', $item->id) . '"><i class="glyph-icon icon-external-link"></i> View</a></li>
                                
                            </ul>
                    </div>
                </div>';
            }
            $data[] = $row;
        }

        return [
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];
    }

    // <li><a href="'. action('Admin\UserController@edit', $item->id) .'">Edit</a></li>

    /**
     * Show the form for creating a new resource.
     *
     * @param Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        return $this->view('form', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();

        $validator = Validator::make($attributes, $this->storeActionRules(), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        User::create($attributes);

        return redirect()->action('Admin\UserController@index')
            ->with('success', 'User Created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $this->view('show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return mixed
     */
    public function edit(User $user)
    {
        $country_list = Country::pluck('title', 'id');

        return $this->view('form', compact('user', 'country_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Faq $faq
     * @return \Illuminate\Http\Response
     * @internal param Faq $faq
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), $this->updateActionRules($user), $this->validatorMessages());
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }

        $attributes = $request->except(['password']);
        $attributes['status'] = ! empty($request->status) ? 'Published' : 'Draft';

        if ($request->has('password')) {
            $attributes['password'] = bcrypt($request->password);
        }

        $user->fill($attributes);
        $user->save();

        return redirect()->action('Admin\UserController@index')
            ->with('success', 'User Edited Successfully.');
    }

    /**
     * Download the formatted CSV.
     *
     * @return $this
     */
    public function downloadCsv(){
        ob_start();

        $all_itmes = User::orderBy('id', 'DESC')->get();
        $final_csv_list = [["Name", "Email", "Username", "Provider", "Platform", "Status"]];
        foreach($all_itmes as $item)
        {
            $status = $item->verified == "Verified" ? "Active" : "Inactive";
            $platform = $item->pushIds()->pluck('platform')->all();
            $platformData = 'None';
            if (count($platform) > 0) {
                $platform = array_unique($platform);
                $platformData = @implode(', ', $platform);
            }
            $user_name = $item->username ? $item->username : "No Username";

            $final_csv_list[] =  [
                $item->full_name, 
                $item->email, 
                $user_name, 
                $item->provider, 
                $platformData, 
                $status, 
            ];
        }

        //dd($final_csv_list);

        $file = fopen("php://output","w");

        foreach ($final_csv_list as $line){
            fputcsv($file, $line);
        }
        fclose($file);

        $filename = "User List - " . @date('Y-m-d H:i') . ".csv";
        return \Response::make(ob_get_clean())
            ->header('Content-Type', 'application/csv')
            ->header('Content-Disposition', 'attachment; filename="'.$filename.'"');
    }

    /**
     * Publish action.
     *
     * @param Request $request
     * @return array
     */
    public function publish(Request $request)
    {
        foreach ($request->ids as $id) {
            $item = User::findOrFail($id);
            $item->fill(['verified' => 'Verified'])->save();
            //$this->publishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Draft action.
     *
     * @param Request $request
     * @return array
     */
    public function draft(Request $request)
    {
        foreach ($request->ids as $id) {
            $item = User::findOrFail($id);
            if($item->verified == 'Verified') {
                $item->fill(['verified' => 'Disabled', 'api_token' => NULL])->save();

                //it is also all user playlist will be private
                $all_board = $item->boards;
                foreach($all_board as $board_item) {
                    $board_item->fill(['private' => 'Y'])->save();
                }

                //it is also all user smart playlist will be private
                $all_smartlist = $item->smart_playlists;
                //dd($all_smartlist->toArray());
                foreach($all_smartlist as $smart_item) {
                    $smart_item->fill(['private' => 'Y'])->save();
                }                
            }            
            //$this->unpublishModule($item);
        }

        return ['status' => true];
    }

    /**
     * Approved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function approve(Request $request)
    {
        foreach ($request->ids as $id) {
            $item = User::findOrFail($id);
            $this->approveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Unapproved Resource.
     *
     * @param Request $request
     * @return array
     */
    public function unapprove(Request $request)
    {
        foreach ($request->ids as $id) {
            $item = User::findOrFail($id);
            $this->unapproveModule($item);
        }

        return ['status' => true];
    }

    /**
     * Reorder resources.
     *
     */
    public function reorder()
    {
        $items = User::published()->orderBy('order')->latest()->get(['id', 'title']);

        return $this->view('reorder', compact('items'));
    }

    /**
     * Save the ordered sequence in database.
     *
     * @param Request $request
     * @return array
     */
    public function postReorder(Request $request)
    {
        $sequence = $this->parseOrder($request->order);
        foreach ($sequence as $order => $id) {
            $items = User::findOrFail($id);
            $items->order = $order + 1;

            $items->setEventType('Login');
            if ($order == 0) {
                $items->setEventType('Rearranged');
            }

            $items->save();
        }

        return ['status' => true];
    }

    /**
     * Delete action.
     *
     * @param Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        foreach ($request->ids as $id) {
            $this->destroy($id);
        }

        return ['status' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function destroy($id)
    {
        //$item = User::findOrFail($id);
        //$item->delete();

        // $directory = public_path() . '/uploads/users/' . $id;
        // if (file_exists($directory)) {
        //     \File::deleteDirectory($directory);
        // }

        //$this->forceDimension = $this->coverDimension;
        //$this->deleteImageOrFile($item->image, $this->upload_folder_name, "thumbs");

        return true;
    }

    //syncdata
    public function syncdata()
    {
        $Last_synch = Last_synch::where('type', 'both')->first();
        //$Last_synch_android_date = $Last_synch->updated_at;

        $query = PushId::whereNotNull('user_id');
        if($Last_synch) {
            $Last_synch_android_date = $Last_synch->updated_at;
            $query->whereDate('created_at', '>=', $Last_synch_android_date);
        }
        
        $PushId = $query->get();

        //$PushId = PushId::all();
        //$PushId = PushId::whereDate('created_at', '>=', $Last_synch_android_date)->get();
        $arrData = array();
        foreach ($PushId as $PushIds) {
            if ($PushIds->user->subscription_status == 1) {
                array_push($arrData, array(
                    'user_id'=> $PushIds->user_id,
                    'full_name' => $PushIds->user->first_name." ".$PushIds->user->last_name,
                    'email_id' => $PushIds->user->email,
                    'type' => $PushIds->platform,
                    'status' => $PushIds->status
                ));
            }
            
        }
        
        //All User list
        foreach ($arrData as $arrDatas) {
            
            $arrDatas['user_id'];
            $email = $arrDatas['email_id'];            
            
            $apiKey = config('config.mailchimp_key');
            $listID = config('config.bothlistId'); // all user list id 

            // MailChimp API URL
            $memberID = md5(strtolower($email));
            $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
            
            // member information
            $json = json_encode([
                'email_address' => $email,
                'status' => 'subscribed',
                'merge_fields' => [
                    'FULLNAME' => $arrDatas['full_name'],
                    'TYPE' => $arrDatas['type'],                    
                ]
            ]);
            
            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            //return $result;
        }

        // Update Last synch table for android column
        $mytime = Carbon::now();
        $current_date_time = $mytime->toDateTimeString();
        if(empty ($Last_synch)) {
            Last_synch::create(['type' => 'both']);
        } else {
            Last_synch::where('type', 'both')->update(['updated_at' => $current_date_time]);
        }

        return ['status' => true, 'message' => 'Sync data successfully uploaded.'];
        //return "Sync Data Successfully !!!";
    }

    //Android
    public function syncdata_android()
    {
        $Last_synch = Last_synch::where('type', 'android')->first();
        $query = PushId::whereNotNull('user_id')->where('platform', 'android');
        if($Last_synch) {
            $Last_synch_android_date = $Last_synch->updated_at;
            $query->whereDate('created_at', '>=', $Last_synch_android_date);
        }
        
        $PushId = $query->get();

        //$Last_synch_android_date = $Last_synch->updated_at;
        //$PushId = PushId::where('platform', 'android')->whereDate('created_at', '>=', $Last_synch_android_date)->get();

        $arrData = array();
        foreach ($PushId as $PushIds) {
            if ($PushIds->user->subscription_status == 1) {
                array_push($arrData, array(
                    'user_id'=> $PushIds->user_id,
                    'full_name' => $PushIds->user->first_name." ".$PushIds->user->last_name,
                    'email_id' => $PushIds->user->email,
                    'type' => $PushIds->platform,
                    'status' => $PushIds->status
                ));
            }
        }
        
        foreach ($arrData as $arrDatas)
        {
            $arrDatas['user_id'];
            $email = $arrDatas['email_id'];

            $apiKey = config('config.mailchimp_key');                
            $AndroidListID = config('config.androidlistId');

            // MailChimp API URL
            $memberID = md5(strtolower($email));
            $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $AndroidListID . '/members/' . $memberID;
            
            // member information
            $json = json_encode([
                'email_address' => $email,
                'status' => 'subscribed',
                'merge_fields' => [
                    'FULLNAME' => $arrDatas['full_name'],
                    'TYPE' => $arrDatas['type']                        
                ]
            ]);
            
            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);            
        }

        // Update Last synch table for android column
        $mytime = Carbon::now();
        $current_date_time = $mytime->toDateTimeString();
        if(empty ($Last_synch)) {
            Last_synch::create(['type' => 'android']);
        } else {
            Last_synch::where('type', 'android')->update(['updated_at' => $current_date_time]);
        }

        return ['status' => true, 'message' => 'Sync android data successfully uploaded.'];
        //return "Sync Android user Successfully !!!";
    }

    //Ios
    public function syncdata_ios()
    {
        $Last_synch = Last_synch::where('type', 'ios')->first();
        $query = PushId::whereNotNull('user_id')->where('platform', 'iphone');
        if($Last_synch) {
            $Last_synch_android_date = $Last_synch->updated_at;
            $query->whereDate('created_at', '>=', $Last_synch_android_date);
        }
        
        $PushId = $query->get();

        $arrData = array();
        foreach ($PushId as $PushIds) {
            if ($PushIds->user->subscription_status == 1) {
                array_push($arrData, array(
                    'user_id'=> $PushIds->user_id,
                    'full_name' => $PushIds->user->first_name." ".$PushIds->user->last_name,
                    'email_id' => $PushIds->user->email,
                    'type' => $PushIds->platform,
                    'status' => $PushIds->status
                ));
            }
        }

        foreach ($arrData as $arrDatas)
        {
            //Ios Users
            // if ($arrDatas['type'] == 'ios')
            // {
                $arrDatas['user_id'];
                $email = $arrDatas['email_id'];

                $apiKey = config('config.mailchimp_key');                                
                $IOSListID = config('config.ioslistId');

                // MailChimp API URL
                $memberID = md5(strtolower($email));
                $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
                $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $IOSListID . '/members/' . $memberID;
                
                // member information
                $json = json_encode([
                    'email_address' => $email,
                    'status' => 'subscribed',
                    'merge_fields' => [
                        'FULLNAME' => $arrDatas['full_name'],
                        'TYPE' => $arrDatas['type'],
                        
                    ]
                ]);
                
                // send a HTTP POST request with curl
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);                
            //}            
        } 

        // Update Last synch table for android column
        $mytime = Carbon::now();
        $current_date_time = $mytime->toDateTimeString();
        if(empty ($Last_synch)) {
            Last_synch::create(['type' => 'ios']);
        } else {
            Last_synch::where('type', 'ios')->update(['updated_at' => $current_date_time]);
        }

        return ['status' => true, 'message' => 'Sync IOS data successfully uploaded.'];
        //return "Sync Ios user Successfully !!!";
    }

    /**
     * This is the validation messages.
     *
     * @return array
     */
    protected function validatorMessages()
    {
        return [
        ];
    }

    /**
     * Store action rules.
     *
     * @return array
     */
    protected function storeActionRules()
    {
        return [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'username' => ['required'],

            /*'address' => ['required'],
            'landmark' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'country' => ['required'],
            'zipcode' => ['required'],*/
        ];
    }

    /**
     * Update action rules.
     *
     * @return array
     */
    protected function updateActionRules($item)
    {
        return [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'username' => ['required'],
            /*'address' => ['required'],
            'landmark' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'country' => ['required'],
            'zipcode' => ['required'],*/
            'password' => ['confirmed', 'max:255'],
            'email' => ['required', 'unique:users,email,' . $item->id, 'max:255']
        ];
    }

    /**
     * Enable Seller
     * 
     * @param Request $request
     * @return array
     */
    public function enabled_seller(Request $request) {        
        foreach ($request->ids as $id) {
            $item = User::findOrFail($id);
            $item->seller = true;
            $item->save();
        }
        
        return ['status' => true];
    }
    
    /**
     * Disable Seller
     * 
     * @param Request $request
     * @return array
     */
    public function disabled_seller(Request $request) {        
        foreach ($request->ids as $id) {
            $item = User::findOrFail($id);
            $item->seller = false;
            $item->save();
        }
        
        return ['status' => true];
    }
}
