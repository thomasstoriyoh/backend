<?php
namespace App\Http\Controllers\Api\Common;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Traits\ResponseFormat;
use App\Models\Cart;
use App\ClassesV3\V3_3\IP2Location;
use App\Models\MarketplaceCountry;
use App\Models\Show;
use App\Models\Episode;

class CartController extends Controller
{
    use ResponseFormat;

    public function index(Request $request){

        $user = Auth::guard('api')->user();
        
        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }        
        $carts = Cart::where('user_id', $user->id)->get();
        

        //Ip2Location code start            
        $ip_address = $request->ip_address;
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();

        // if (empty($location['data']['country_code']) || $location['data']['country_code'] == "-") {
        //     return response()->api([
        //         'status' => false,
        //         'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
        //         'data'    => [
        //             'order_id' => "",
        //             'currency' => "",
        //             'amount' => 0
        //         ]
        //     ]);
        // }

        //Check if marketplace available in user country
        $marketplace_country = MarketplaceCountry::where("status", "Published")
            ->where("admin_status", "Approved")
            ->where('country_alpha2_code', $location['data']['country_code'])
            ->first(["id", "title", "country_alpha2_code", "currency_id", "tax_name", "tax_price"]);

        if ($marketplace_country) {
            // $product_price = $show->premium_pricing()->where('country_id', $marketplace_country->id)
            //     ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
            // $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
            $country_id = $marketplace_country->id;
            $currency_id = $marketplace_country->currency_id;
            $selling_currency = $marketplace_country->currency->title;                    
        }

        
        $items = array();
        foreach($carts as $item){
            $content_id = $item->content_id;                        

            switch($item->type){
                case 'show':
                    $show = Show::find($content_id);
                    $product_price = $show->premium_pricing()->where('country_id', $country_id)->where('currency_id', $currency_id)
                    ->first(['selling_price']);
                    $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";
                    $title = $show->title;
                    $image = !empty($show->image) ? $show->getWSImage(400) : asset('uploads/default/show.png');
                break;

                case 'episode':
                    $episode = Episode::find($content_id);
                    $product_price = $episode->premium_pricing()->where('country_id', $country_id)->where('currency_id', $currency_id)
                    ->first(['selling_price']);
                    $selling_price = !empty($product_price->selling_price) ?  $product_price->selling_price : "";                     
                    $image = !empty($episode->show->image) ? $episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                    break;
                    
            }       

            $items[] = [
                'content_id' => $content_id,
                'title' => $title,
                'price' => $selling_price, 
                'image' => $image
            ];
        }

        return $carts; 

    }

    public function create(Request $request){

        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }        
        
        $validatedData = $request->validate([
            'content_id' => 'required|int',
            'type' => 'required',
        ]);
        
        $cart = Cart::addItem($user, $request);    
        
        if($cart){
            $response = [
                'status' => true,                
                'message' => config('language.' . $this->getLocale() . ".Cart.item_added")
            ];
            return response()->json($response);
        }

        $response = [
            'status' => false,                
            'message' => config('language.' . $this->getLocale() . ".Cart.action_failed")
        ];
        return response()->json($response);
    }

    public function destroy(Request $request){

        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }        

        $validatedData = $request->validate([
            'content_id' => 'required|int',
            'type' => 'required',
        ]);
        

        $cart = Cart::where([
            'user_id' => $user->id, 
            'content_id' => $request->input('content_id'),
            'type' => $request->input('type')
        ])->first();

        if($cart){
            $cart->delete();
            $response = [
                'status' => true,                
                'message' => config('language.' . $this->getLocale() . ".Cart.item_removed")
            ];
            return response()->json($response);    
        }

        $response = [
            'status' => false,                
            'message' => config('language.' . $this->getLocale() . ".Cart.item_not_found")
        ];
        return response()->json($response);    
        
    }


    public function removeAll(){
     
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }        

        $carts = Cart::where([
            'user_id' => $user->id, 
        ]);

        if($carts->count() > 0){
            $carts->delete();
            $response = [
                'status' => true,                
                'message' => config('language.' . $this->getLocale() . ".Cart.all_items_removed")
            ];
            return response()->json($response);    
        }

        $response = [
            'status' => false,                
            'message' => config('language.' . $this->getLocale() . ".Cart.cart_empty")
        ];
        return response()->json($response);    
        
    }

}