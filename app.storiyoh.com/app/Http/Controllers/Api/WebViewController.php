<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Faqcategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Classes\Podcast;
use App\Classes\PodcastEpisode;
use App\Classes\Common;
use App\Classes\Collection;
use App\Classes\Banners;
use App\Models\Episode;
use App\Models\EpisodeDetail;

class WebViewController extends Controller
{
    /**
     * FAQ Category.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function faqs_category()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            return response()->api(['data' => [
                'status' => false,
                'message' => 'Unauthorized Access.'
            ]]);
        }

        $data = Faqcategory::published()->approved()
            ->orderBy('order')->latest()->get(['id', 'title']);

        return response()->api([
            'status' => true,
            'message' => '',
            'data' => [
                'items' => $data
            ]
        ]);
    }

    /**
     * FAQ Listing.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function faqs(Request $request, $view_mode = null)
    {
        $faq = Faqcategory::published()->approved()
            ->orderBy('order')->latest()->pluck('title', 'id')->all();

        return view('api.web.faqs', compact('faq', 'view_mode'));
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function faqs_details(Request $request, $catId, $view_mode = null)
    {
        $faqCategory = Faqcategory::find($catId, ['id', 'title']);

        $data = $faqCategory->faqs()->published()->approved()
            ->orderBy('order')->latest()->get(['id', 'title', 'answer']);

        return view('api.web.faqs_details', compact('faqCategory', 'data', 'view_mode'));
    }

    public function faqs_with_categories(Request $request)
    {
        $categoryData = Faqcategory::published()->approved()
            ->orderBy('order')->latest()->get(['id', 'title']);

        $categoryDataInfo = [];
        foreach ($categoryData as $category) {
            $data = Faq::published()->approved()
                ->where('faq_categories_id', $category->id)
                ->orderBy('order')->latest()->get(['id', 'title', 'answer']);
            $categoryDataInfo[] = ['id' => $category->id, 'title' => $category->title, 'faqs' => $data];
        }

        return response()->api([
            'status' => true,
            'message' => '',
            'data' => [
                'items' => $categoryDataInfo
            ]
        ]);
    }

    /**
     * About Us.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about_us($view_mode = null)
    {
        return view('api.web.about_us', compact('view_mode'));
    }

    /**
     * Disclaimer.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function disclaimer($view_mode = null)
    {
        return view('api.web.disclaimer', compact('view_mode'));
    }

    /**
     * Terms & Condition.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function terms_of_use($view_mode = null)
    {
        return view('api.web.terms_of_use', compact('view_mode'));
    }

    /**
     * Privacy Policy.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privacy_policy($view_mode = null)
    {
        return view('api.web.privacy_policy', compact('view_mode'));
    }

    /**
     * Attributions.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function attributions($view_mode = null)
    {
        return view('api.web.attributions', compact('view_mode'));
    }

    /**
     * Copyright Policy.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function copyright_policy($view_mode = null)
    {
        return view('api.web.copyright_policy', compact('view_mode'));
    }

    /**
     * Community Guidelines.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function community_guidelines($view_mode = null)
    {
        return view('api.web.community_guidelines', compact('view_mode'));
    }

    /**
     * Shows Listing page View.
     *
     * @param Request $request
     * @return mixed
     */
    public function shows(Request $request)
    {
        $showData = new Podcast();

        return $showData->Shows();
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function show_search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => 'Please provide search keyword.'
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $showData = new Podcast();

        return $showData->Search();
    }

    /**
     * ALl Podcast Episodes
     *
     * @param Request $request
     * @return void
     */
    public function episodes(Request $request)
    {
        $episodeData = new PodcastEpisode('web');

        return $episodeData->Episodes();
    }

    /**
     * Episode Search
     *
     * @param Request $request
     * @return void
     */
    public function episode_search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => 'Please provide search keyword.'
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $episodeData = new PodcastEpisode('web');

        return $episodeData->Search();
    }

    /**
     * Episode Details function
     *
     * @param Request $request
     * @return void
     */
    public function episode_show(Request $request)
    {
        $episodeData = new PodcastEpisode('web');

        return $episodeData->Show();
    }

    /**
     * Podcast Details function
     *
     * @param Request $request
     * @return void
     */
    public function show(Request $request)
    {
        $showData = new Podcast('web');

        return $showData->Show();
    }

    /**
     * Podcast Episodes function
     *
     * @param Request $request
     * @return void
     */
    public function show_episodes(Request $request)
    {
        $showData = new Podcast('web');

        return $showData->Episodes();
    }

    /**
     * Total Episode function
     *
     * @param Request $request
     * @return void
     */
    public function total_episodes(Request $request)
    {
        $trendingData = new Common('web');

        return $trendingData->totalEpisode();
    }

    /**
     * Trending function
     *
     * @param Request $request
     * @return void
     */
    public function trending(Request $request)
    {
        $trendingData = new Common('web');

        return $trendingData->trending();
    }

    /**
     * Auto Search function
     *
     * @param Request $request
     * @return void
     */
    public function auto_search(Request $request)
    {
        $showData = new Podcast('web');

        return $showData->AutoSearch();
    }

    /**
     * Keyword function
     *
     * @param Request $request
     * @return void
     */
    public function keyword_search(Request $request)
    {
        $commonData = new Common('web');

        return $commonData->search_keyword();
    }

    /**
     * Shorten Url
     *
     * @param Request $request
     * @return void
     */
    public function shortenURL(Request $request)
    {
        $commonData = new Common('web');

        return $commonData->shortenURL();
    }

    /**
     * Homepage COntent function
     *
     * @param Request $request
     * @return void
     */
    public function home_page_content(Request $request)
    {
        $home_data = new Common('web');

        return $home_data->home_page_content();
    }

    /**
     * Collection Listing
     *
     * @param Request $request
     * @return void
     */
    public function collection(Request $request)
    {
        $collectionData = new Collection('web');

        return $collectionData->Collections();
    }

    /**
     * Collection Show
     *
     * @param Request $request
     * @return void
     */
    public function collection_show(Request $request)
    {
        $collectionData = new Collection('web');

        return $collectionData->Show();
    }

    /**
     * Homepage Content Trending Show function
     *
     * @param Request $request
     * @return void
     */
    public function home_page_content_trending_show(Request $request)
    {
        $home_data = new Common('web');

        return $home_data->home_page_content_trending_show();
    }

    /**
     * Homepage Content Collection function
     *
     * @param Request $request
     * @return void
     */
    public function home_page_content_collection(Request $request)
    {
        $home_data = new Common('web');

        return $home_data->home_page_content_collection();
    }

    /**
     * Homepage Content Podcast of the Day function
     *
     * @param Request $request
     * @return void
     */
    public function home_page_content_podcast_of_the_day(Request $request)
    {
        $home_data = new Common('web');

        return $home_data->home_page_content_podcast_of_the_day();
    }

    /**
     * Homepage Content Latest Shows function
     *
     * @param Request $request
     * @return void
     */
    public function home_page_content_latest_shows(Request $request)
    {
        $home_data = new Common('web');

        return $home_data->home_page_content_latest_shows();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function home_page_content_ssr() {
        $home_data = new Common('web');
        return $home_data->home_page_content_ssr();
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function show_episodes_test(Request $request)
    {
        $showData = new Podcast('web');

        return $showData->Episodes_Test();
    }

    /**
     * Twitter Callback.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function twitter_callback()
    {
        return view('api.web.twitter');
    }

    /**
     * Banner Listing function
     *
     * @return void
     */
    public function banner()
    {
        $bannerData = new Banners('web');
        return $bannerData->banner_listing();
    }

    /**
     * Banner Click function
     *
     * @param Request $request
     * @return void
     */
    public function banner_clicks(Request $request)
    {
        $banner_clicks = new Banners('web');
        return $banner_clicks->banner_clicks_event($request);
    }

    public function test_log() {
        \Log::info("My Test");
    }

    /**
     * Webview function for
     *
     * @param Request $request
     * @param [type] $epi_id
     * @return void
     */
    public function episode_details(Request $request, $epi_id, $view_mode = null)
    {
        $episodeData = Episode::where('id', $epi_id)->first(['id', 'show_id','description']);
        $artist_name = $episodeData->show->artist_name;
        //$episode = $episodeData->description;
        $episode = $episodeData->getOriginal('description');

        return view('api.web.episode_details', compact('episode', 'artist_name', 'view_mode'));
    }

    /**
     * Webview function for
     *
     * @param Request $request
     * @param [type] $epi_id
     * @return void
     */
    public function episode_resources_details(Request $request, $epi_id, $view_mode = null)
    {
        $episodeData = EpisodeDetail::where('episode_id', $epi_id)->first(['id', 'res_description']);

        $episode = "";
        if (!is_null($episodeData)) {
            $episode = $episodeData->res_description;
        }

        return view('api.web.episode_resaurces_details', compact('episode', 'view_mode'));
    }

    /**
     * Webview function for
     *
     * @param Request $request
     * @param [type] $epi_id
     * @return void
     */
    public function show_details(Request $request, $show_id, $view_mode = null)
    {
        $showData = \App\Models\Show::where('id', $show_id)->first(['id', 'description']);
        $show = $showData->getOriginal('description');

        return view('api.web.show_details', compact('show', 'view_mode'));
    }
}
