<?php

namespace App\Http\Controllers\Api\business\V1\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Traits\GeneratePassportToken;
use App\Traits\PassportToken;
use Laravel\Socialite\Facades\Socialite;
use App\Traits\ResponseFormatBusiness;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use GeneratePassportToken, PassportToken, ResponseFormatBusiness;

    /**
     * This function is use for login
     * @param Request $request
     * @return mixed values
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ], [
            'username.required' => "Please enter your username.",
            'password.required' => "Please enter your password.",
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'navigate' => 'login',
                'seller' => 0,
                'message' => $validator->errors()->first()
            ];

            return response()->business_api($response);
        }
        //echo $request->username; exit;
        $user = User::where('username', $request->username)->orWhere('email', $request->username)->first([
            'id', 'first_name', 'image', 'email', 'username', 'password', 'city', 'state',
            'country', 'seller', 'verified', 'admin_status', 'encryption_key'
        ]);
        //exit($user);
        if (!$user) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'login',
                'seller' => 0,
                'message' => "Invalid username . "
            ]);
        }

        if (!Hash::check($request->password, $user->password)) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'login',
                'seller' => 0,
                'message' => "Invalid password."
            ]);
        }

        if ($user->verified == 'Disabled') {
            return response()->business_api([
                'status' => false,
                'navigate' => 'login',
                'seller' => 0,
                'message' => "Your account is disabled by the admin. Please contact support team."
            ]);
        }

        if (!$user->isVerified()) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'verify_email',
                'seller' => 0,
                'email' => $user->email,
                'message' => "Your account is not verified. Please verify your account."
            ]);
        }

        if (!$user->isApproved()) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'login',
                'seller' => 0,
                'message' => "Your account is de-activated by the admin. Please contact support team."
            ]);
        }

        if ($user->seller == 0) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'login',
                'seller' => 0,
                'message' => "Sorry, you are not a seller."
            ]);
        }
        //dd($user);
        //Update last Login type
        $user->update(['last_login_type' => "Direct"]);
        
        /* Creating Profile Access Token */
        $access_token = $this->generateAccessToken($request, $request->username, $request->password);
        //print_r($access_token); die();
        if (!array_key_exists('access_token', $access_token)) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'login',
                'seller' => 0,
                'message' => "No access token found."
            ]);
        }

        //Update Encyption Key
        $encryption_key = "";
        if (is_null($user->encryption_key)) {
            $encryption_key = md5(str_random(100));
            $user->update(['encryption_key' => $encryption_key]);
        } else {
            $encryption_key = $user->encryption_key;
        }
        //$encryption_key = md5(str_random(100));
        //$user->update(['encryption_key' => $encryption_key]);

        $imageData = !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png');

        $city = $user->city ? $user->city : '';
        $state = $user->state ? $user->state : '';
        $country = $user->country ? $user->country : '';

        $seller = $user->sellerInfo()->count();

        if($seller == 1) {
            $imageData = ! is_null($user->sellerInfo->image) ? $user->sellerInfo->getImage(100) : asset('uploads/default/user.png');
        }

        $data = collect($user)
            ->put('city', $city)
            ->put('state', $state)
            ->put('country', $country)
            ->put('full_name', $user->full_name)
            ->put('seller', $seller)
            ->put('image', $imageData)
            ->put('access_token', @$access_token['access_token'])
            ->put('refresh_token', @$access_token['refresh_token'])
            ->put('expires_in', @$access_token['expires_in'])
            ->put('encryption_key', $encryption_key);

        return response()->business_api([
            'status' => true,
            'navigate' => 'profile',
            'seller' => $user->seller,
            'data' => $data
        ]);
    }

    /**
     * Use for check social login for V2
     *
     * @param Request $request
     * @return void
     */
    public function provider_login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider' => 'required',
            'provider_id' => 'required',
            'token' => 'required',
        ], [
            'provider.required' => "Please send provider.",
            'provider_id.required' => "Please send provider id.",
            'token.required' => "Please send social token.",
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'navigate' => 'register',
                'seller' => 0,
                'message' => $validator->errors()->first()
            ];

            return response()->business_api($response);
        }

        //New Social Code Start
        try {
            if (strtolower($request->provider) == 'twitter') {
                if (!$request->secret) {
                    $response = [
                        'status' => false,
                        'navigate' => 'register',
                        'seller' => 0,
                        'message' => "Please send secret token."
                    ];

                    return response()->business_api($response);
                }
                $social_user = Socialite::driver('twitter')->userFromTokenAndSecret($request->token, $request->secret);
            } else {
                if (strtolower($request->provider) == 'google') {
                    $accessTokenResponse = Socialite::driver('google')->getAccessTokenResponse($request->token);
                    $accessToken = $accessTokenResponse['access_token'];
                } else {
                    $accessToken = $request->token;
                }
                $social_user = Socialite::driver(strtolower($request->provider))->userFromToken($accessToken);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => false,
                'navigate' => 'login',
                'seller' => 0,
                'message' => "Malformed access token."
            ];

            return response()->business_api($response);
        }

        if(! $social_user->email) {
            $response = [
                'status' => false,
                'navigate' => 'register',
                'seller' => 0,
                'message' => "We don't found your email address."
            ];

            return response()->business_api($response);
        }

        $user = User::where('email', $social_user->email)->first();
        //New Social Code End

        if ($user) {
            if ($user->verified == 'Disabled') {
                return response()->business_api([
                    'status' => false,
                    'navigate' => 'login',
                    'seller' => 0,
                    'message' => "Your account is disabled by the admin. Please contact support team."
                ]);
            }
            if (!$user->isVerified()) {
                return response()->business_api([
                    'status' => false,
                    'navigate' => 'verify_email',
                    'seller' => 0,
                    'email' => $user->email,
                    'message' => "Your account is not verified. Please verify your account."
                ]);
            }

            if (!$user->isApproved()) {
                return response()->business_api([
                    'status' => false,
                    'navigate' => 'login',
                    'seller' => 0,
                    'message' => "Your account is de-activated by the admin. Please contact support team."
                ]);
            }

            //update last login type
            $random_password = str_random(60);
            $user->update(['last_login_type' => 'Social', 'password_social' => Hash::make($random_password)]);

            /* Creating Social Profile Access Token */
            $access_token = $this->generateAccessToken($request, $user->email, $random_password);

            if (!array_key_exists('access_token', $access_token)) {
                return response()->business_api([
                    'status' => false,
                    'navigate' => 'login',
                    'seller' => 0,
                    'message' => "No access token found."
                ]);
            }

            $data = [
                'id' => @$user->id,
                'username' => $user->username,
                'full_name' => $user->full_name,
                'image' => !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png'),
                'city' => $user->city ? $user->city : '',
                'state' => $user->state ? $user->state : '',
                'country' => $user->country ? $user->country : '',
                'access_token' => @$access_token['access_token'],
                'refresh_token' => @$access_token['refresh_token'],
                'expires_in' => @$access_token['expires_in']
            ];

            $user = User::find($user->id, ['id', 'first_name', 'email', 'username', 'image', 'password', 'city', 'state', 'country', 'seller']);

            $imageData = !empty($user->image) ? $user->getImage(100) : '';

            $city = $user->city ? $user->city : '';
            $state = $user->state ? $user->state : '';
            $country = $user->country ? $user->country : '';

            $data = collect($user)
                ->put('full_name', $user->full_name)
                ->put('city', $city)
                ->put('state', $state)
                ->put('country', $country)
                ->put('image', $imageData)
                ->put('access_token', @$access_token['access_token'])
                ->put('refresh_token', @$access_token['refresh_token'])
                ->put('expires_in', @$access_token['expires_in']);

            return response()->business_api([
               'status' => true,
               'navigate' => 'profile',
               'seller' => $user->seller,
               'data' => $data,
            ]);
        } else {
            return response()->business_api([
                'status' => false,
                'navigate' => 'register',
                'seller' => 0,
                'message' => "User not recognized."
            ]);
        }
    }

    /*
     * This function is use for creating
     * new access token
     *
     * return tokens
    */
    public function refresh_token(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'refresh_token' => 'required'
        ], [
            'refresh_token.required' => "Please send refresh token."
        ]);
        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'login',
                'registered' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $access_token = $this->refreshAccessToken($request);

        if (array_key_exists('error', $access_token)) {
            return response()->business_api(['status' => false, 'code' => 401, 'message' => $access_token['message'], 'data' => []]);
        }

        $data = [
            'access_token' => @$access_token['access_token'],
            'refresh_token' => @$access_token['refresh_token'],
            'expires_in' => @$access_token['expires_in']
        ];

        return response()->business_api(['status' => true, 'code' => 200, 'message' => '', 'data' => $data]);
    }

    /**
     * Update Encryption Key
     */
    public function update_encryption_key (Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Update Encyption Key
        $encryption_key = md5(str_random(100));
        $user->update(['encryption_key' => $encryption_key]);

        $data = ['encryption_key' => $encryption_key];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }
}
