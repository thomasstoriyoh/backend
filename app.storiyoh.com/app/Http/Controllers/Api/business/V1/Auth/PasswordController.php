<?php

namespace App\Http\Controllers\Api\business\V1\Auth;

use App\Packages\OTP\OTP;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Traits\ResponseFormatBusiness;

class PasswordController extends Controller
{
    use ResponseFormatBusiness;

    public function forgot(Request $request)
    {
        $validator = Validator::make($request->all(), 
        [
            'email' => 'required',
        ], [
            'email.required' => "Please enter email address.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'forgot_password',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'forgot_password',
                'message' => "User not found with this credential."
            ]);
        }

        if (!$user->isApproved()) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'login',
                'message' => "Your account is de-activated by the admin. Please contact support team."
            ]);
        }

        if (!$user->isVerified()) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'verify_email',
                'message' => "User email is not verified."
            ]);
        }

        try {
            $this->sendForgetEmailOtp($user);
        } catch (\Exception $ex) {
        }

        return response()->business_api([
            'status' => true,
            'message' => "We have sent a email otp to your email id.",
            'navigate' => "forgot_password_otp",
            'data' => ['email' => $request->email]
        ]);
    }

    /**
     * This function is send otp to the given email
     * @param User $user
     */
    protected function sendForgetEmailOtp($user)
    {
        $otp = OTP::generate($user->email);

        $mailData = [
                'file_path' => 'auth.email.forgot-password',
                'from_email' => config('config.register.sender_email'),
                'from_name' => config('config.register.sender_name'),
                'to_email' => trim($user->email),
                'to_name' => $user->full_name,
                'subject' => ' Forgot Password OTP',
                'filename' => null,
                'data' => [
                    'user_data' => $user,
                    'otp' => $otp
                ]
            ];

        \App\Traits\Helper::sendAllEmail($mailData);

        return $otp;
    }

    /**
     * This function is use for verify email otp
     * @param User $user
     */
    public function verifyForgotEmailOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'otp' => 'required'
        ], [
            'email.required' => "Please enter email address.",
            'otp.required' => "The otp field is required.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'forgot_password_otp',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (OTP::verify($request->otp, $user->email)) {
                return response()->business_api([
                    'status' => true,
                    'email' => $user->email,
                    'navigate' => 'forgot_password_reset',
                    'message' => ''
                ]);
            } else {
                return response()->business_api([
                    'status' => false,
                    'navigate' => 'forgot_password_email_otp',
                    'message' => "Invalid OTP. Please try again."
                ]);
            }
        }
    }

    /**
     * Update Password function
     *
     * @param Request $request
     * @return void
     */
    public function reset_password(Request $request)
    {
        $validator = Validator::make($request->all(), 
        [
            'otp' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed'
        ], [
            'otp.required' => "The otp field is required.",
            'email.required' => "Please enter email address.",
            'password.required' => "Please enter password.",
            'password.confirmed' => "Password and confirm password do not match.", 
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'forgot_password_reset',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->business_api([
                'status' => false,
                'navigate' => 'forgot_password',
                'message' => "User not found with this credential."               
            ]);
        }

        if (OTP::verify($request->otp, $user->email)) {

            $new_password = Hash::make($request->password);
            $user->update(['password' => $new_password, 'password_social' => $new_password]);
            
            return response()->business_api([
                'status' => true,
                'navigate' => 'login',
                'message' => "Your pasword has been change successfully." 
            ]);            
        } else {
            return response()->business_api([
                'status' => false,
                'message' => "Invalid OTP. Please try again."
            ]);
        }    
    }
}
