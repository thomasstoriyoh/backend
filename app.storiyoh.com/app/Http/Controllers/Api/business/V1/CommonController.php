<?php

namespace App\Http\Controllers\Api\business\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ip2LocationCountry;
use App\Models\Category;
use App\Models\ShowLanguage;
use Illuminate\Support\Facades\Auth;
use App\Traits\ResponseFormatBusiness;
use App\Models\EpisodeTag;
use App\Traits\HelperV2;
use App\Models\EpisodeDetail;

class CommonController extends Controller
{
    use ResponseFormatBusiness;

    /**
     *
     * @param Request $request
     * @return type
     */
    public function country(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (! $user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $country_list = Ip2LocationCountry::orderBy('country_name', 'ASC')->get(['country_name', 'country_alpha3_code']);

        $data = [];
        foreach ($country_list as $item) {
            $data[] = [
                //'id' => $item->id,
                'id' => $item->country_alpha3_code,
                'name' => $item->country_name                
            ];
        }
        
        return response()->business_api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for get all categories and
     * also auto complete
     *
     * @param Request $request
     */
    public function categories(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $query = Category::published()->whereDoesntHave('children');

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('title', 'LIKE', '%' . $searchText . '%');
            });

            $query->take(20);
        }

        $query->orderBy('title', 'ASC');

        $all_category = $query->get(['id', 'title']);

        $data = [];
        foreach ($all_category as $item) {
            $data[] = [
                'id' => $item->id,
                'name' => $item->title,                
            ];
        }

        return response()->business_api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function getting all language
     * @param Request $request
     * @return type
     */
    public function languages(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }
        
        $languages = ShowLanguage::where('status', 'Published')
            ->whereNotNull('title')->orderBy('title', 'ASC')->get(['id', 'title', 'short_code']);

        $data = [];
        foreach ($languages as $item) {
            $data[] = [
                'id' => $item->short_code,
                'name' => $item->title                
            ];
        }

        return response()->business_api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for get all tags and
     * also auto complete
     *
     * @param Request $request
     */
    public function tags(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $query = EpisodeTag::select('id', 'title');

        //Search Query
        //$request->keyword = "a";
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('title', 'LIKE', '%' . $searchText . '%');
            });

            $query->take(20);
        }

        $query->take(25)->orderBy('title', 'ASC');

        $all_category = $query->get(['id', 'title']);

        $data = [];
        foreach ($all_category as $item) {
            $data[] = [
                'id' => $item->id,
                'name' => trim($item->title, '"')
            ];
        }

        return response()->business_api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function getting all language
     * @param Request $request
     * @return type
     */
    public function ratings(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }
        
        $data = [
            ['name' => 'G', 'id' => 'G'],
            ['name' => 'PG', 'id' => 'PG'],
            ['name' => 'PG-13', 'id' => 'PG-13'],
            ['name' => 'R', 'id' => 'R'],
            ['name' => 'NC-17', 'id' => 'NC-17'],
        ]; 
        
        return response()->business_api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for get all episode tags and
     * also auto complete
     *
     * @param Request $request
     */
    public function episode_tags(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $premium_episodes = EpisodeDetail::orderBy('id', 'DESC')->take(10)->get(['id', 'episode_id']);
        $data = [];
        foreach ($premium_episodes as $premium_episode) {
            $tags = $premium_episode->episode->tags()->pluck('title', 'id')->all();
            foreach ($tags as $key => $value) {
                $data[$key] = [
                    'id' => $key,
                    'name' => trim($value, '"')
                ];
            }
        }
        $data = array_values($data);

        return response()->business_api([
            'status' => true,
            'data' => $data
        ]);
    }
}
