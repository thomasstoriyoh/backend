<?php

namespace App\Http\Controllers\Api\business\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Traits\ResponseFormatBusiness;

//use App\ClassesV3\V3_3\IP2Location;
use App\Models\Seller;
use App\Models\User;
use App\Models\MarketplaceCountry;
use App\Traits\HelperV2;

class MyAccountController extends Controller
{
    use ResponseFormatBusiness;    

    /**
     * My Account page view.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $data = [];
        if (! is_null($user->sellerInfo)) {
            $imageData = ! is_null($user->sellerInfo->image) ? $user->sellerInfo->getImage(100) : asset('uploads/default/user.png');
            $data[] = [
                'name' => $user->sellerInfo->name,
                'email' => $user->sellerInfo->email,
                'image' => $imageData,
                'contact_no' => $user->sellerInfo->contact_no,
                'overview' => $user->sellerInfo->overview,
                'url' => $user->sellerInfo->url,
                'media_link_fb' => $user->sellerInfo->media_link_fb,
                'media_link_twitter' => $user->sellerInfo->media_link_twitter,
                'media_link_linkedin' => $user->sellerInfo->media_link_linkedin,
                'media_link_instagram' => $user->sellerInfo->media_link_instagram,
            ];
        } else {
            $imageData = asset('uploads/default/user.png');
            $data[] = [
                'name' => $user->full_name,
                'email' => $user->email,
                'image' => $imageData,
                'contact_no' => "",
                'overview' => "",
                'url' => "",
                'media_link_fb' => "",
                'media_link_twitter' => "",
                'media_link_linkedin' => "",
                'media_link_instagram' => "",
            ];
        }

        return response()->business_api([
            'status' => true,
            'data' => $data
        ]);
    }    

    /**
     * My Account edit profile page.
     *
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'overview' => 'required',
            //'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',            
        ], [
            //'first_name.required' => config('language.' . $this->getLocale() . ".Account.update_firstname_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }        

        $seller = Seller::updateOrCreate(['user_id' => $user->id], [
            "user_id" => $user->id,
            "name" => $request->name,
            "email" => $request->email,            
            "contact_no" => $request->contact_no,
            "overview" => $request->overview,
            "url" => $request->url,
            "media_link_fb" => $request->media_link_fb,
            "media_link_twitter" => $request->media_link_twitter,
            "media_link_linkedin" => $request->media_link_linkedin,
            "media_link_instagram" => $request->media_link_instagram
        ]);

        $old_image = $seller->image;
        $filename = '';
        if ($request->image instanceof UploadedFile) {
            $allowedfileExtension = ['jpeg', 'png' , 'jpg', 'gif'];
            $check = in_array($request->image->getClientOriginalExtension(), $allowedfileExtension);
            if ($check) {
                $filename = @date('YmdHis') . str_random(6) . '.' . $request->image->getClientOriginalExtension();
            
                //$directory = public_path('uploads/sellers/');
                //$request->image->move($directory, $filename);
            
                $image_name = 'sellers/' . $filename;
                Storage::disk('s3')->put($image_name, file_get_contents($request->image));

                if (Storage::disk('s3')->exists('sellers/' . $old_image)) {
                    Storage::disk('s3')->delete('sellers/' . $old_image);
                }

                $seller->update(['image' => $filename]);
            }
        }        

        $imageData = ! is_null($seller->image) ? $seller->getImage(100) : asset('uploads/default/user.png');

        return response()->business_api([
            'status' => true,
            'message' => 'Your profile has been updated successfully.',
            'data' => [
                'image' => $imageData
            ]
        ]);
    }    

    /**
     * My Account edit profile page.
     *
     * @param Request $request
     * @return mixed
     */
    public function update_image(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ], []);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $attributes = [];

        $old_image = $user->image;
        $filename = '';
        if ($request->image instanceof UploadedFile) {
            $filename = @date('YmdHis') . str_random(6) . '.' . $request->image->getClientOriginalExtension();

            $image_name = 'users/' . $user->id . '/' . $filename;
            
            Storage::disk('s3')->put($image_name, file_get_contents($request->image));

            $attributes['image'] = $filename;

            if (Storage::disk('s3')->exists('users/' . $user->id . '/' . $old_image)) {
                Storage::disk('s3')->delete('users/' . $user->id . '/' . $old_image);
                //@unlink($directory . '/' . $old_image);
            }
        } else {
            return response()->business_api([
                'status' => false,
                'message' => 'Please upload image.'
            ]);
        }

        //Update user info
        $user->forceFill($attributes)->save();

        return response()->business_api([
            'status' => true,
            'message' => 'Your profile image has been updated successfully.',
            'data' => [
                'image' => $user->image ? $user->getImage(100) : asset('uploads/default/user.png'),
                'full_image' => $user->image . '----' . $filename
            ]
        ]);
    }    

    /**
    * @param $upload
    * @param $directory
    * @param $filename
    */
    protected function resizeUploadedImage($upload, $directory, $filename)
    {
        $image = Image::make($upload);
        $image->fit(300, 300)->save($directory . '/' . $filename, 75);
    }

    /**
     * This function is use for user listing
     * @param Request $request
     * @return type
     */
    public function users(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        //Fetch all Users
        // $query = User::approved()->verified()->whereNotNull('username')->where('username', '!=', '')->where('id', '!=', $user->id);
        $query = User::approved()->verified()->whereNotNull('username')->where('username', '!=', '');

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('first_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('username', 'LIKE', '%' . $searchText . '%');
            });
        }

        //Sort By
        if ($request->sort_by == 'last_updated') {
            $query->orderBy('updated_at', 'Desc');
        } else {
            $query->orderBy('first_name', 'ASC');
        }      
        
        $users = $query->get(['id', 'first_name', 'last_name', 'email', 'username', 'image']);

        // if ($request->no_paginate == 'Yes') {
        //     $users = $query->get(['id', 'first_name', 'last_name', 'username', 'image']);
        // } else {
        //     $users = $query->paginate(20, ['id', 'first_name', 'last_name', 'username', 'image']);
        // }

        // $users = $user->following()->where('verified', 'Verified')
        //     ->where('admin_status', 'Approved')
        //     ->orderBy('user_follow.updated_at', 'DESC')
        //     ->get(['id', 'first_name', 'last_name', 'email', 'username', 'image']);

        $data = [];
        foreach ($users as $item) {
            $data[] = [
               "id" => $item->id,
               'name' => $item->notification_format_name . " - " . $item->email              
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No User Found.';
        }
        
        //if ($request->no_paginate == 'Yes') {
            return response()->business_api([
                'status' => true,
                'message' => $message,
                'data' => $data
            ]);
        // } else {
        //     return response()->business_api([
        //         'status' => true,
        //         'data' => [
        //             'total' => $users->total(),
        //             'per_page' => $users->perPage(),
        //             'pages' => ceil($users->total() / $users->perPage()),
        //             'items' => $data,
        //             'message' => $message
        //         ]
        //     ]);
        // }
    }

    /**
     * This function is use for logout session
     * @param Request $request
     * @return type
     */
    public function logout(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => '401:1 Token authentication error.'
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        try {
            //Revoke Access Token
            $request->user('api')->token()->revoke();

            //Revoke Refresh Token
            \DB::table('oauth_refresh_tokens')->where('access_token_id', $request->user('api')->token()->id)
                ->update(['revoked' => true]);

            //reset encryption key
            //$user->encryption_key = '';
            //$user->save();

        } catch(\Exception $ex) {

        }

        return response()->business_api([
            'status' => true,
            'message' => ""
        ]);
    }

    /**
     * This function is use for logout session
     * @param Request $request
     * @return type
     */
    public function getMarketplaceCountryList(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => '401:1 Token authentication error.'
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        // $country_list = MarketplaceCountry::where('status', 'Published')->where('admin_status', 'Approved')->whereNotNull('currency_id')
        //     ->get(['id', 'title', 'country_alpha2_code', 'currency_id', 'markup_price', 'commission_price', 'tax_name', 'tax_price']);

        $country_list = MarketplaceCountry::whereNotNull('currency_id')
            ->get(['id', 'title', 'country_alpha2_code', 'currency_id', 'markup_price', 'commission_price', 'tax_name', 'tax_price']);
        
        $data = [];
        foreach ($country_list as $item) {
            $data[] = [
                "id" => $item->id,
                'title' => $item->title,
                'country_alpha2_code' => $item->country_alpha2_code,
                'currency_id' => $item->currency_id,
                'currency_title' => $item->currency->title,
                'markup_price' => $item->markup_price,
                'commission_price' => $item->commission_price,
                'tax_name' => $item->tax_name,
                'tax_price' => $item->tax_price,
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No Country Found.';
        }

        return response()->business_api([
            'status' => true,
            'message' => $message,
            'data' => $data
        ]);

    }

    /**
     * This function is use for user listing
     * @param Request $request
     * @return type
     */
    public function searchUsers(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        //Fetch all Users
        $query = User::approved()->verified()->whereNotNull('username')->where('username', '!=', '');

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('first_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('email', 'LIKE', '%' . $searchText . '%');
            });
        }              
        
        $users = $query->take(50)->orderBy('first_name', 'ASC')
            ->get(['id', 'first_name', 'last_name', 'email', 'username', 'image']);        

        $data = [];
        foreach ($users as $item) {
            $data[] = [
               "id" => $item->id,
               'name' => $item->notification_format_name . " - " . $item->email              
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No User Found.';
        }
        
        return response()->business_api([
            'status' => true,
            'message' => $message,
            'data' => $data
        ]);        
    }
}
