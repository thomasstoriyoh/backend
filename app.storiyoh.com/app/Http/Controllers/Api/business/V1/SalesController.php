<?php

namespace App\Http\Controllers\Api\business\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Episode;
use App\Models\EpisodeDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Traits\ResponseFormatBusiness;
use App\Traits\HelperV2;
use App\Models\Order;
use App\Models\Show;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SalesController extends Controller
{
    use ResponseFormatBusiness;

    /**
     * This is function use for sales graph test
     */
    public function sales_graph_testing(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        //getting user premium podcast and episode
        $podcastIds = $user->premium_shows()->where('content_type', 1)->pluck('id')->all();
        $episodeIds = $user->premium_episode->pluck('episode_id')->all();

        if (count($podcastIds) == 0 && count($episodeIds) == 0) {
            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => [
                    'total' => 0,
                    'totalP' => 0,
                    'totalE' => 0,
                    'data' => []
                ]
            ]);
        }

        $interval = 7;
        if (!is_null($request->interval)) {
            $interval = $request->interval;
        }
        $start_date = Carbon::now()->subDays($interval);
        $end_date = Carbon::now();

        $query = Order::where('status', 'success')->where('updated_at', '>=', $start_date)
            ->where('updated_at', '<=', $end_date);

        if (count($podcastIds) > 0 || count($episodeIds) > 0) {
            $query->where(function ($q) use ($podcastIds, $episodeIds) {
                if (count($podcastIds) > 0) {
                    $q->orWhere('product_type', 1)->whereIn('product_id', $podcastIds);
                }
                if (count($episodeIds) > 0) {
                    $q->orWhere('product_type', '!=', 1)->whereIn('product_id', $episodeIds);
                }
            });
        }

        $orders = $query->get(['id', 'product_type', 'product_id', 'invoice_total', 'updated_at']);

        $total = 0;
        $product_type1 = [];
        $product_type2 = [];
        $final_data = [];
        $final_date_data = [];
        $final_data2 = [];
        $final_data3 = [];
        foreach ($orders as $order) {
            $month = Carbon::parse($order->updated_at)->format('Y-m-d');
            $month = strtotime($month);
            if (!in_array($month, $final_data)) {
                $final_data[] = $month;

                $final_date_data[] = date('d/m/Y', $month);
            }
            if ($order->product_type == 1) {
                $product_type1[$month][] = $order->invoice_total;
            } elseif ($order->product_type == 2 ||  $order->product_type == 3) {
                $product_type2[$month][] = $order->invoice_total;
            }
            $total += $order->invoice_total;
        }

        foreach ($product_type1 as $I1) {
            $total1 = 0;
            foreach ($I1 as $ii) {
                $total1 += $ii;
            }
            $final_data2[] = $total1;
        }

        foreach ($product_type2 as $J1) {
            $total2 = 0;
            foreach ($J1 as $jj) {
                $total2 += $jj;
            }
            $final_data3[] = $total2;
        }
        //dd($final_data, $final_data2, $final_data3, $final_date_data, $total, count($podcastIds), count($episodeIds));
        $data = [
            'labels' => $final_date_data,
            'datasets' => [
                [
                    'label' => 'Series',
                    'backgroundColor' => '#36a2eb',
                    'fill' => true,
                    'data' => $final_data2
                ],
                [
                    'label' => 'Episode',
                    'backgroundColor' => 'olive',
                    'fill' => true,
                    'data' => $final_data3
                ]
            ]
        ];

        return response()->business_api([
            'status' => true,
            'message' => "",
            'data' => [
                'total' => $this->shorten_count($total),
                'totalP' => $this->shorten_count(count($podcastIds)),
                'totalE' => $this->shorten_count(count($episodeIds)),
                'data' => $data
            ]
        ]);
    }

    /**
     * This is function use for sales graph
     */
    public function sales_graph(Request $request) {
       
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }
        
        //getting user premium podcast and episode
        $podcastIds = $user->premium_shows()->where('content_type', 1)->pluck('id')->all();
        $episodeIds = $user->premium_episode->pluck('episode_id')->all();

        if (count($podcastIds) == 0 && count($episodeIds) == 0) {
            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => [
                    'total' => 0,
                    'totalP' => 0,
                    'totalE' => 0,
                    'data' => []
                ]
            ]);
        }

        $interval = 7;
        if(! is_null($request->interval)) {
            $interval = $request->interval;
        }
        $start_date = Carbon::now()->subDays($interval);
        $end_date = Carbon::now();
        
        $query = Order::where('status', 'success')->where('updated_at', '>=', $start_date)
            ->where('updated_at', '<=', $end_date);
        
        if (count($podcastIds) > 0 || count($episodeIds) > 0) {
            $query->where(function ($q) use ($podcastIds, $episodeIds) {
                if (count($podcastIds) > 0) {
                    $q->orWhere('product_type', 1)->whereIn('product_id', $podcastIds);
                }
                if (count($episodeIds) > 0) {
                    $q->orWhere('product_type', '!=', 1)->whereIn('product_id', $episodeIds);
                }
            });
        }
        
        $orders = $query->get(['id', 'product_type', 'product_id', 'invoice_total', 'updated_at']);        
        
        $total = 0;
        $product_type1 = [];
        $product_type2 = [];
        $final_data = [];
        $final_date_data = [];
        $final_data2 = [];
        $final_data3 = [];
        foreach($orders as $order) {
            $month = Carbon::parse($order->updated_at)->format('Y-m-d');
            $month = strtotime($month);
            if (! in_array($month, $final_data)) {
                $final_data[] = $month;                

                $final_date_data[] = date('d/m/Y', $month);
            }            
            if ($order->product_type == 1) {
                $product_type1[$month][] = $order->invoice_total;                                    
            } elseif ($order->product_type == 2 ||  $order->product_type == 3) {
                $product_type2[$month][] = $order->invoice_total;
            }            
            $total += $order->invoice_total;            
        }
        
        foreach($product_type1 as $I1) {
            $total1 = 0;
            foreach($I1 as $ii) {
                $total1 += $ii;                
            }
            $final_data2[] = $total1;            
        }

        foreach($product_type2 as $J1) {
            $total2 = 0;
            foreach($J1 as $jj) {
                $total2 += $jj;                
            }
            $final_data3[] = $total2;
        }
        //dd($final_data, $final_data2, $final_data3, $final_date_data, $total, count($podcastIds), count($episodeIds));
        $data = [
            'labels' => $final_date_data,
            'datasets' => [
                [
                    'label'=> 'Series',
                    'backgroundColor'=> '#36a2eb',
                    'fill'=> true,
                    'data' => $final_data2
                ],
                [
                    'label'=> 'Episode',
                    'backgroundColor'=> 'olive',
                    'fill'=> true,
                    'data' => $final_data3
                ]
            ]
        ];

        return response()->business_api([
            'status' => true,
            'message' => "",
            'data' => [
                'total' => $this->shorten_count($total),
                'totalP' => $this->shorten_count(count($podcastIds)),
                'totalE' => $this->shorten_count(count($episodeIds)),
                'data' => $data
            ]
        ]);
    }

    /**
     * This function is getting top selling items
     */
    public function top_selling(Request $request) {
       
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        //getting user premium podcast and episode
        $podcastIds = $user->premium_shows()->where('content_type', 1)->pluck('id')->all();        
        $episodeIds = $user->premium_episode->pluck('episode_id')->all();

        if (count($podcastIds) == 0 && count($episodeIds) == 0) {
            return response()->business_api([
                'status' => true,
                'message' => "",
                'total' => 0,
                'data' => []
            ]);
        }

        $data = [];
        if(count($podcastIds) > 0) {
            $show_selling = DB::table('show_purchases')->whereIn('show_id', $podcastIds)
                ->select(\DB::raw('show_id, COUNT(show_id) AS count_series'))
                ->orderBy(\DB::raw('count_series'), 'desc')->groupBy(\DB::raw('show_id'))->get();            
            foreach($show_selling as $item) {
                $show = Show::find($item->show_id, ['id', 'title', 'image']);
                $data[] = [
                    'id' => $item->show_id,
                    'title' => $show->title,                    
                    'imgUrl' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/premium_show.jpg'),
                    'imgAlt' => $show->title,
                    'type' => 'S',                    
                    'total' => $item->count_series,
                    'date' => "5th Apr 2019"
                ];
            }
        }        

        if(count($episodeIds) > 0) {
            $episode_selling = DB::table('episode_purchases')->whereIn('episode_id', $episodeIds)
                ->select(\DB::raw('episode_id, COUNT(episode_id) AS count_episode'))
                ->orderBy(\DB::raw('count_episode'), 'desc')->groupBy(\DB::raw('episode_id'))->get(5);
            foreach($episode_selling as $item) {
                $episode = Episode::find($item->episode_id, ['id', 'title', 'image']);
                $data[] = [
                    'id' => $item->episode_id,
                    'title' => $episode->title,
                    'imgUrl' => !empty($episode->image) ? $episode->getImage(200) : asset('uploads/default/premium_show.jpg'),
                    'imgAlt' => $episode->title,
                    'type' => 'E',
                    'total' => $item->count_episode,
                    'date' => "5th Apr 2019"
                ];
            }
        }
        
        $finalData = [];
        $collection_data = collect([]);
        if(count($data) > 0) {
            $collection_data = collect($data)->sortByDesc('total');            
            $finalData = $collection_data->forPage($request->page, 5);
        }  
        
        return response()->business_api([
            'status' => true,
            'message' => "",
            'total' => count($finalData),
            'data' => $finalData
        ]);
    }

    /***
     * This function is getting top download episode
     */
    public function top_download(Request $request) {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        //getting user premium podcast and episode
        $episodeIds = $user->premium_episode->pluck('episode_id')->all();

        if (count($episodeIds) == 0) {
            return response()->business_api([
                'status' => true,
                'message' => "",
                'total' => 0,
                'data' => []
            ]);
        }

        $data = [];
        if(count($episodeIds) > 0) {
            $episode_selling = DB::table('user_download_data')->whereIn('episode_id', $episodeIds)
                ->select(\DB::raw('episode_id, COUNT(episode_id) AS count_episode'))
                ->orderBy(\DB::raw('count_episode'), 'desc')->groupBy(\DB::raw('episode_id'))->take(5)->get();
            foreach($episode_selling as $item) {
                $episode = Episode::find($item->episode_id, ['id', 'title', 'image']);
                $data[] = [
                    'id' => $item->episode_id,
                    'title' => $episode->title,
                    'imgUrl' => !empty($episode->image) ? $episode->getImage(200) : asset('uploads/default/premium_show.jpg'),
                    'imgAlt' => $episode->title,
                    'type' => 'E',
                    'total' => $item->count_episode,
                    'date' => "5th Apr 2019"
                ];
            }
        }
        
        return response()->business_api([
            'status' => true,
            'message' => "",
            'total' => count($data),
            'data' => $data
        ]);
    }

    /**
     * This function is getting top listened episode
     */
    public function top_listened(Request $request) {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        //getting user premium podcast and episode
        $episodeIds = $user->premium_episode->pluck('episode_id')->all();

        if (count($episodeIds) == 0) {
            return response()->business_api([
                'status' => true,
                'message' => "",
                'total' => 0,
                'data' => []
            ]);
        }

        $data = [];
        if(count($episodeIds) > 0) {
            $episode_selling = DB::table('user_stream_data')->whereIn('episode_id', $episodeIds)
                ->select(\DB::raw('episode_id, COUNT(episode_id) AS count_episode'))
                ->orderBy(\DB::raw('count_episode'), 'desc')->groupBy(\DB::raw('episode_id'))->take(5)->get();
            foreach($episode_selling as $item) {
                $episode = Episode::find($item->episode_id, ['id', 'title', 'image']);
                $data[] = [
                    'id' => $item->episode_id,
                    'title' => $episode->title,
                    'imgUrl' => !empty($episode->image) ? $episode->getImage(200) : asset('uploads/default/premium_show.jpg'),
                    'imgAlt' => $episode->title,
                    'type' => 'E',
                    'total' => $item->count_episode,
                    'date' => "5th Apr 2019"
                ];
            }
        }
                
        return response()->business_api([
            'status' => true,
            'message' => "",
            'total' => count($data),
            'data' => $data
        ]);
    }

    /**
     * This function is getting recent uplaod episode
     */
    public function recent_upload(Request $request) {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);        
        if(! $seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $episodes = EpisodeDetail::where('user_id', $user->id)->take(5)->orderByDesc('id')->get(['episode_id']);        
        if (! $episodes) {
            return response()->business_api([
                'status' => true,
                'message' => "",
                'total' => 0,
                'data' => []
            ]);
        }

        $data = [];        
        foreach($episodes as $item) {
            $episode = Episode::find($item->episode_id, ['id', 'title', 'image', 'created_at']);
            $data[] = [
                'id' => $item->episode_id,
                'title' => $episode->title,
                'imgUrl' => !empty($episode->image) ? $episode->getImage(200) : asset('uploads/default/premium_show.jpg'),
                'imgAlt' => $episode->title,
                'type' => 'E',
                'total' => $item->count_episode,
                'date' => Carbon::parse($episode->created_at)->format("jS M Y")
            ];
        }
        
        return response()->business_api([
            'status' => true,
            'message' => "",
            'total' => count($data),
            'data' => $data
        ]);
    }
}
