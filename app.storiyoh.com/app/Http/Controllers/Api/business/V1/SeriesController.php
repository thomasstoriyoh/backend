<?php

namespace App\Http\Controllers\Api\business\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Traits\ResponseFormatBusiness;
use App\Models\Show;
use App\Models\PremiumPricingShow;
use App\Models\PremiumPricingShowsHistory;
use App\Models\Episode;
use Carbon\Carbon;
use App\Models\EpisodeDetail;
use App\Models\PremiumPricingEpisode;
use App\Models\PremiumPricingEpisodesHistory;
use App\Models\EpisodeTemp;
use App\Models\EpisodeAudioFileUpload;
use Illuminate\Support\Facades\Storage;
use App\Models\MarketplaceCountry;
use LaravelMP3;
use App\Models\Ip2LocationCountry;
use App\Models\ShowLanguage;
use Aws\S3\S3Client;
use App\Traits\HelperV2;
use App\Models\EpisodeResource;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SeriesController extends Controller
{
    use ResponseFormatBusiness;

    /**
     * This function is use for my shows on dashboard
     * 
     */
    public function mySeriesDashboard(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        try {
            //Fetching Premium shows listing
            $qry = Show::where('content_type', 1)->where('user_id', $user->id);
            $series_total = $qry->count();

            $all_shows = $qry->orderBy('created_at', 'DESC')
                ->take(4)->get(['id', 'title', 'description', 'image', 'status', 'created_at']);

            $show_data = [];
            foreach ($all_shows as $show) {
                //$pricing_count = $show->premium_pricing()->count();
                $status = "";
                // if($pricing_count == 0) {
                //     $status = "incompete";
                // } else {
                if (strtolower($show->status) == "published") {
                    $status = "publish";
                } else {
                    $status = "draft";
                }
                //}
                $show_data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'description' => str_limit($show->description, 200, " ..."),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/premium_show.jpg'),
                    'status' => $status,
                    //'listing_type' => $pricing_count == 0 ? true : false,
                    'episode_count' => $show->episodes()->count(),
                ];
            }

            //Fetching Premium standalone episode listing
            $qry = EpisodeDetail::where('user_id', $user->id)->where('content_type', 2);

            $episode_total = $qry->count();

            $all_episodes = $qry->orderBy('created_at', 'DESC')->take(4)->get(['id', 'episode_id', 'intro', 'created_at']);

            $data_episode = [];
            foreach ($all_episodes as $item) {
                //$pricing_count = $item->episode->premium_pricing()->count();
                $status = "";
                /*if($pricing_count == 0) {
                    $status = "incompete";
                } else {*/
                if (strtolower($item->episode->status) == "published") {
                    $status = "publish";
                } else {
                    $status = "draft";
                }
                //}
                $data_episode[] = [
                    'id' => $item->episode->id,
                    'show_id' => @$item->episode->show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                    'description' => $item->intro,
                    'image' => !empty($item->episode->image) ? $item->getImage(200) : asset('uploads/default/premium_show.jpg'),
                    'status' => $status,
                    //'listing_type' => $pricing_count == 0 ? true : false,
                ];
            }

            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => [
                    'series_listing' => $show_data,
                    'series_total' => $series_total,
                    'episode_listing' => $data_episode,
                    'episode_total' => $episode_total
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * This function is use for getting series
     * 
     */
    public function mySeries(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        try {
            //Fetching Premium shows listing
            $qry = Show::where('content_type', 1)->where('user_id', $user->id);

            $total_shows = $qry->count();

            if (!empty($request->status)) {
                if ($request->status != 'all') {
                    $qry->where('status', ucfirst($request->status));
                }
            }

            $all_shows = $qry->orderBy('created_at', 'DESC')
                ->paginate(8, ['id', 'title', 'description', 'image', 'status', 'created_at']);
            $data = [];
            foreach ($all_shows as $show) {
                //$pricing_count = $show->premium_pricing()->count();
                $status = "";
                // if($pricing_count == 0) {
                //     $status = "incompete";
                // } else {
                if (strtolower($show->status) == "published") {
                    $status = "publish";
                } else {
                    $status = "draft";
                }
                //}
                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'description' => str_limit($show->description, 200, " ..."),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/premium_show.jpg'),
                    'status' => $status,
                    //'listing_type' => $pricing_count == 0 ? true : false,
                    'episode_count' => $show->episodes()->count(),
                ];
            }

            $message = "";
            if (count($data) == 0) {
                $message = "No Series found.";
            }

            return response()->business_api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_shows->total(),
                    'per_page' => $all_shows->perPage(),
                    'pages' => ceil($all_shows->total() / $all_shows->perPage()),
                    'items' => $data,
                    'total_shows' => $total_shows
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * This function is use for getting series
     * 
     */
    public function mySeriesListing(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        try {

            //Fetching Premium shows listing
            $all_shows = Show::where('content_type', 1)->where('user_id', $user->id)
                ->orderBy('created_at', 'DESC')->get(['id', 'title']);

            $data = [];
            foreach ($all_shows as $show) {
                $data[] = [
                    'id' => $show->id,
                    'name' => trim(str_replace("\n", '', html_entity_decode($show->title)))
                ];
            }

            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * This function is use for adding new series
     * 
     */
    public function addSeries(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validationArray = [
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'language' => 'required',
            'rating' => 'required',
            'pricing' => 'required',
            'explicit' => 'required',
            'country' => 'required',
            'status' => 'required',
        ];

        $validator = Validator::make($request->all(), $validationArray, [
            'title.required' => "Please enter series title.",
            'description.required' => "Please enter series description.",
            'category_id.required' => "Please select series category.",
            'language.required' => "Please select series language.",
            'rating.required' => "Please enter series rating.",
            'pricing.required' => "Please enter series pricing.",
            'explicit.required' => "Please enter explicit type.",
            'country.required' => "Please select country.",
            'status.required' => "Please select status.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $attributes = [
                'title' => $request->title,
                'user_id' => $user->id,
                'description' => !empty($request->description) ? $request->description : null,
                'country' => $request->country,
                'language' => $request->language,
                'web_url' => $request->web_url,
                'full_image' => null,
                'image' => !empty($request->image) ? $request->image : null,
                'rating' => $request->rating,
                'status' => $request->status,
                'content_type' => 1,
                'explicit' => $request->explicit,
                'btn_label' => !empty($request->btn_label) ? $request->btn_label : null, //btn label added
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ];

            //Create Series
            if (!is_null(env('DBLOCAL'))) {
                $showId = Show::insertGetId($attributes);
                $series = Show::find($showId);
            } else {
                $series = Show::create($attributes);
            }

            if (!empty($request->category_id)) {
                $categoryArr = explode(",", trim($request->category_id));
                $categoryArr = array_filter($categoryArr);
                $categoryArr = array_unique($categoryArr);
                $series->categories()->sync($categoryArr);
            }

            //This code for elastic search
            if (is_null(env('DBLOCAL'))) {
                if ($request->status == "Published") {
                    //$series->searchable();
                    $series->unsearchable();
                } elseif ($request->status == "Draft") {
                    $series->unsearchable();
                }
            }

            if (!empty($request->pricing)) {
                $show_pricing = json_decode($request->pricing, 1);
                foreach ($show_pricing as $item) {
                    $attributes = [
                        "show_id" => $series->id,
                        "country_id" => $item['country_id'],
                        "currency_id" => $item['currency_id'],
                        "apple_product_id" => !empty($item['apple_product_id']) ? $item['apple_product_id'] : null,
                        "android_product_id" => !empty($item['android_product_id']) ? $item['android_product_id'] : null,
                        "web_price" => !empty($item['web_price']) ? $item['web_price'] : 0,
                        "price" => !empty($item['price']) ? $item['price'] : 0,
                        "markup_price" => !empty($item['markup_price']) ? $item['markup_price'] : 0,
                        "commission_price" => !empty($item['commission_price']) ? $item['commission_price'] : 0,
                        "selling_price" => !empty($item['selling_price']) ? $item['selling_price'] : 0,
                    ];
                    PremiumPricingShow::create($attributes);
                }
            }

            return response()->business_api([
                'status' => true,
                'message' => "Series has been successfully added."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return response()->business_api([
            'status' => false,
            'message' => "Something is went wrong, Please try again after some time."
        ]);
    }

    /**
     * This function is use for edit series
     */
    public function editSeries(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validationArray = [
            's_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'language' => 'required',
            'rating' => 'required',
            'explicit' => 'required',
            'country' => 'required',
        ];

        $validator = Validator::make($request->all(), $validationArray, [
            's_id.required' => '6000:1 - id missing for series',
            'title.required' => "Please enter series title.",
            'description.required' => "Please enter series description.",
            'category_id.required' => "Please select series category.",
            'language.required' => "Please select series language.",
            'rating.required' => "Please enter series rating.",
            'explicit.required' => "Please enter explicit type.",
            'country.required' => "Please select country."
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Series
            $series = Show::where('id', $request->s_id)->where('user_id', $user->id)->first();

            if (!$series) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Series Found."
                ]);
            }

            $attributes = [
                'title' => $request->title,
                'description' => !empty($request->description) ? $request->description : null,
                'country' => $request->country,
                'language' => $request->language,
                'web_url' => $request->web_url,
                'full_image' => null,
                'image' => !empty($request->image) ? $request->image : null,
                'rating' => $request->rating,
                'explicit' => $request->explicit,
                'updated_by' => $user->id,
                'btn_label' => !empty($request->btn_label) ? $request->btn_label : null, //btn label added
            ];

            //Update Series
            if (is_null(env('DBLOCAL'))) {
                $series->fill($attributes)->save();
            }

            if (!empty($request->category_id)) {
                $categoryArr = explode(",", trim($request->category_id));
                $categoryArr = array_filter($categoryArr);
                $categoryArr = array_unique($categoryArr);
                $series->categories()->sync($categoryArr);
            }

            //This code for elastic search
            if (is_null(env('DBLOCAL'))) {
                if ($request->status == "Published") {
                    //$series->searchable();
                    $series->unsearchable();
                } else {
                    $series->unsearchable();
                }
            }
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return response()->business_api([
            'status' => true,
            'message' => "Successfully updated."
        ]);
    }

    /**
     * This function is use for getting series price
     */
    public function getSeriesPricing(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            's_id' => 'required'
        ], [
            's_id.required' => '6000:1 - id missing for series'
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Series
            $series = Show::where('id', $request->s_id)->where('user_id', $user->id)
                ->first(['id', 'title']);

            if (!$series) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Series Found."
                ]);
            }

            //Getting Published Country
            //$marketplace_country = \App\Models\MarketplaceCountry::where('status', "Published")->pluck('id')->all();

            //Getting pricing
            $premium_price = PremiumPricingShow::where('show_id', $series->id)
                //->whereIn('country_id', $marketplace_country)
                ->get(['id', 'country_id', 'currency_id', 'apple_product_id', 'android_product_id', 'web_price',
                'price', 'markup_price', 'commission_price', 'selling_price'
            ]);

            $data = [];
            foreach ($premium_price as $item) {
                $data[] = [
                    "id" => $item->id,
                    "country_id" => $item->country_id,
                    "currency_id" => $item->currency_id,
                    "price" => $item->price,
                    "markup_price" => $item->markup_price,
                    "selling_price" => $item->selling_price
                ];
            }

            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => $data,
                'series' => $series->title,
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * Update Series Pricing
     */
    public function updateSeriesPricing(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            's_id' => 'required',
            'pricing' => 'required',
        ], [
            's_id.required' => '6000:1 - id missing for series',
            'pricing.required' => "Please enter series pricing."
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Getting Series
        $series = Show::where('id', $request->s_id)->where('user_id', $user->id)->first();

        if (!$series) {
            return response()->business_api([
                'status' => false,
                'message' => "No Series Found."
            ]);
        }

        try {
            if (!empty($request->pricing)) {
                $oldpricedataIds = PremiumPricingShow::where('show_id', $series->id)->pluck('id')->all();
                $show_pricing = json_decode($request->pricing, 1);
                $insertedArray = [];
                foreach ($show_pricing as $item) {
                    $checkContentPrice = PremiumPricingShow::where('show_id', $series->id)->where('country_id', $item['country_id'])->where('currency_id', $item['currency_id'])->first();
                    if (empty($checkContentPrice)) {
                        $attributes = [
                            "show_id" => $series->id,
                            "country_id" => $item['country_id'],
                            "currency_id" => $item['currency_id'],
                            "apple_product_id" => !empty($item['apple_product_id']) ? $item['apple_product_id'] : null,
                            "android_product_id" => !empty($item['android_product_id']) ? $item['android_product_id'] : null,
                            "web_price" => !empty($item['web_price']) ? $item['web_price'] : 0,
                            "price" => !empty($item['price']) ? $item['price'] : 0,
                            "markup_price" => !empty($item['markup_price']) ? $item['markup_price'] : 0,
                            "commission_price" => !empty($item['commission_price']) ? $item['commission_price'] : 0,
                            "selling_price" => !empty($item['selling_price']) ? $item['selling_price'] : 0
                        ];
                        PremiumPricingShow::create($attributes);
                    } else {
                        //Insert into history table
                        $createdHistoryArrayData = [
                            "premium_pricing_id" => $checkContentPrice->id,
                            "show_id" => $checkContentPrice->show_id,
                            "country_id" => $checkContentPrice->country_id,
                            "currency_id" => $checkContentPrice->currency_id,
                            "apple_product_id" => $checkContentPrice->apple_product_id,
                            "android_product_id" => $checkContentPrice->android_product_id,
                            "web_price" => $checkContentPrice->web_price,
                            "price" => $checkContentPrice->price,
                            "markup_price" => $checkContentPrice->markup_price,
                            "commission_price" => $checkContentPrice->commission_price,
                            "selling_price" => $checkContentPrice->selling_price
                        ];
                        PremiumPricingShowsHistory::create($createdHistoryArrayData);

                        //Update series pricing table
                        //$attributes = ["web_price" => ! empty($item['web_price']) ? $item['web_price'] : 0];  
                        $attributes = $item;
                        $attributes['apple_product_id'] = !empty($item['apple_product_id']) ? $item['apple_product_id'] : null;
                        $attributes['android_product_id'] = !empty($item['android_product_id']) ? $item['android_product_id'] : null;
                        $attributes['web_price'] = !empty($item['web_price']) ? $item['web_price'] : 0;

                        $checkContentPrice->fill($attributes)->save();

                        $insertedArray[] = $checkContentPrice->id;
                    }
                }

                $removeIds = [];
                if (count($oldpricedataIds) > 0) {
                    $removeIds = array_diff($oldpricedataIds, $insertedArray);
                }

                //Delete data from pricing table
                if (count($removeIds) > 0) {
                    $removeData = PremiumPricingShow::whereIn('id', $removeIds);
                    if ($removeData->count() > 0) {
                        $removeData->delete();
                    }
                }
            }

            return response()->business_api([
                'status' => true,
                'message' => "Series price has been updated."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * This function is use for view series
     */
    public function viewSeries(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            's_id' => 'required'
        ], [
            's_id.required' => '6000:1 - id missing for series'
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Series
            $series = Show::where('id', $request->s_id)->where('user_id', $user->id)
                ->first(['id', 'title', 'description', 'image', 'language', 'rating', 'country', 'explicit', 'web_url', 'btn_label']);

            if (!$series) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Series Found."
                ]);
            }

            //Getting categories
            $categories = $series->categories->pluck('title', 'id')->all();
            //dd($categories);
            $categoryData = [];
            foreach ($categories as $key => $value) {
                $categoryData[] = [
                    'id' => $key,
                    'name' => $value
                ];
            }

            //Getting pricing
            // $premium_price = PremiumPricingShow::where('show_id', $series->id)->get([
            //     'id', 'country_id', 'currency_id', 'apple_product_id', 'android_product_id', 'web_price', 
            //     'price', 'markup_price', 'commission_price', 'selling_price'
            // ]);            

            $full_image = !is_null($series->image) ? $series->getSeriesImage(200) : '';
            $image_status = !is_null($series->image) ? 1 : 0;
            $imgsize = '';
            $imgext = '';
            $img_name = '';
            if ($series->image) {
                if (!is_null(env('DBLOCAL'))) {
                    if (file_exists(public_path('uploads/shows/' . $series->image))) {
                        $imgsize = filesize(public_path('uploads/shows/' . $series->image));
                        $path_parts = pathinfo(public_path('uploads/shows/' . $series->image));
                        $imgext = 'image/' . $path_parts['extension'];
                        $img_name = $path_parts['filename'];
                    } else {
                        $image_status = 0;
                    }
                } else {
                    if (Storage::disk('s3')->exists('shows/' . $series->image)) {
                        $imgsize = Storage::disk('s3')->size('shows/' . $series->image);
                        $path_parts = pathinfo($series->getSeriesImage());
                        $imgext = 'image/' . $path_parts['extension'];
                        $img_name = $path_parts['filename'];
                    } else {
                        $image_status = 0;
                    }
                }
            }

            //Fetching Country Data
            $countryData = [];
            if (!is_null($series->country)) {
                $countrySql = Ip2LocationCountry::where('country_alpha3_code', $series->country)
                    ->first(['country_name', 'country_alpha3_code']);
                if (!empty($countrySql)) {
                    $countryData[] = [
                        'id' => $countrySql->country_alpha3_code,
                        'name' => $countrySql->country_name
                    ];
                }
            }

            //Fetching Language Data
            $languageData = [];
            if (!is_null($series->language)) {
                $languageSql = ShowLanguage::where('short_code', $series->language)
                    ->first(['short_code', 'title']);
                if (!empty($languageSql)) {
                    $languageData[] = [
                        'id' => $languageSql->short_code,
                        'name' => $languageSql->title
                    ];
                }
            }

            $data = collect($series)
                ->put('description', $series->getOriginal('description'))
                ->put('img_size', $imgsize)
                ->put('img_ext', $imgext)
                ->put('img_name', $img_name)
                ->put('full_image', $full_image)
                ->put('image_status', $image_status)
                ->put('categories', $categoryData)
                ->put('country', $countryData)
                ->put('language', $languageData);
            //->put("pricing", $premium_price);

            //dd($data);
            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * Published Series
     * 
     */
    public function publishedSeries(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            's_id' => 'required'
        ], [
            's_id.required' => '6000:1 - id missing for series',
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Series
            $series = Show::where('id', $request->s_id)->where('user_id', $user->id)->first(['id', 'title', 'description', 'image', 'status', 'created_at']);

            if (!$series) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Series Found."
                ]);
            }

            $show_data = [];
            //$show_data[] = ['status' => "publish"];
            if ($series->status == "Draft") {
                if (is_null(env('DBLOCAL'))) {
                    //Update Series status
                    $series->status = 'Published';
                    $series->save();

                    //This series added to elastic search
                    //$series->searchable();
                    $series->unsearchable();

                    $show_data[] = ['status' => "publish"];

                    return response()->business_api([
                        'status' => true,
                        'message' => "Series published successfully.",
                        'data' => $show_data
                    ]);
                }
                return response()->business_api([
                    'status' => true,
                    'message' => "Series published successfully.",
                    'data' => $show_data
                ]);
            }
            return response()->business_api([
                'status' => true,
                'message' => "Series published successfully.",
                'data' => $show_data
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * Unpublished Series
     * 
     */
    public function unpublishedSeries(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            's_id' => 'required'
        ], [
            's_id.required' => '6000:1 - id missing for series',
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Series
            $series = Show::where('id', $request->s_id)->where('user_id', $user->id)
                ->first(['id', 'title', 'description', 'image', 'status', 'created_at']);

            if (!$series) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Series Found."
                ]);
            }

            $show_data = [];
            //$show_data[] = ['status' => "draft"];
            if ($series->status == "Published") {
                if (is_null(env('DBLOCAL'))) {
                    //Also unpublished all series episodes
                    $episodes = Episode::where('show_id', $series->id)->where('status', 'Published')->get();
                    foreach ($episodes as $episode) {
                        try {
                            //Update in episode details also
                            $episode_details = EpisodeDetail::where('episode_id', $episode->id)->first();
                            if (!empty($episode_details)) {
                                $episode_details->status = 'Draft';
                                $episode_details->save();
                            }

                            $episode->status = 'Draft';
                            $episode->save();

                            //This episode remove from elastic search
                            $episode->unsearchable();
                        } catch (\Exception $e) { }
                    }

                    //Update Series status
                    $series->status = 'Draft';
                    $series->save();

                    //This series remove from elastic search
                    $series->unsearchable();

                    $show_data[] = ['status' => "draft"];

                    return response()->business_api([
                        'status' => true,
                        'message' => "Series drafted successfully.",
                        'data' => $show_data
                    ]);
                }
                return response()->business_api([
                    'status' => true,
                    'message' => "Series drafted successfully.",
                    'data' => $show_data
                ]);
            }
            return response()->business_api([
                'status' => true,
                'message' => "Series drafted successfully.",
                'data' => $show_data
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * All Series Episodes Listing
     */
    public function viewSeriesEpisodes(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            's_id' => 'required'
        ], [
            's_id.required' => '6000:1 - id missing for series'
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Getting Series
        $series = Show::where('id', $request->s_id)->where('user_id', $user->id)->first();

        if (!$series) {
            return response()->business_api([
                'status' => false,
                'message' => "No Series Found."
            ]);
        }

        $episode_data = $series->episodes()->get(['id', 'title', 'status', 'created_at']);
        $finalData = [];
        foreach ($episode_data as $item) {
            $finalData[] = [
                'id' => $item->id,
                'title' => $item->title,
                'status' => 'compete',
                'type' => 'episode',
                'updateinfo' => "Update Info",
                'updateprice' => "Update Pricing",
                'delist' => $item->status == "Draft" ? "Publish Episode" : "Delist Episode",
                'createDate' => strtotime($item->created_at),
            ];
        }

        $temp_data = EpisodeAudioFileUpload::where('show_id', $series->id)
            ->where('user_id', $user->id)->whereNull('episode_id')
            ->where('status', 'Incomplete')->get(['id', 'status', 'created_at']);
        foreach ($temp_data as $key => $item) {
            $finalData[] = [
                'id' => $item->id,
                'title' => "Episode " . ($key + 1),
                'status' => 'incompete',
                'type' => 'temp',
                'updateinfo' => "Complete Data",
                'updateprice' => "",
                'delist' => "Delete File",
                'createDate' => strtotime($item->created_at)
            ];
        }

        $request->page = $request->page ? $request->page : 1;

        $collection_data = collect([]);
        $responseData = [];
        if (count($finalData) > 0) {
            $collection_data = collect($finalData)->sortByDesc('createDate');
            $finalData = $collection_data->forPage($request->page, 10);
            $counter = 1;
            foreach ($finalData as $key => $item) {
                $responseData[] = [
                    'id' => $item['id'],
                    'title' => $item['status'] != "incompete" ? $item['title'] : "Episode " . $counter,
                    'status' => $item['status'],
                    'type' => $item['type'],
                    'updateinfo' => $item['updateinfo'],
                    'updateprice' => $item['updateprice'],
                    'delist' => $item['delist'],
                    'createDate' => $item['createDate'],
                ];
                if ($item['status'] == "incompete") {
                    $counter++;
                }
            }
        }

        $message = "";
        $status = true;
        if (count($responseData) == 0) {
            $status = true;
            $message = "No Episode Found";
        }
        return response()->business_api([
            'status' => $status,
            'message' => $message,
            'data' => [
                'total' => count($collection_data),
                'per_page' => 10,
                'pages' => ceil(count($collection_data) / 10),
                'items' => $responseData,
                'series_title' => $series->title
            ]
        ]);
    }

    /**
     * Upload audio files to temperory table
     */
    public function file_upload(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'file' => 'required',
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        try {
            if ($request->hasFile('file')) {
                $allowedfileExtension = ['mp3', 'mp4'];
                $extension = $request->file->getClientOriginalExtension();                
                $check = in_array($extension, $allowedfileExtension);
                $attributes = [];
                if ($check) {
                    $filename = @date('YmdHis') . str_random(6) . '.' . $extension;
                    $upload_file_name = "marketplace/" . $filename;

                    $file_title = null;
                    $duration = null;
                    try {
                        $file_title = @LaravelMP3::getTitle($request->file);
                        $file_title = is_null($file_title) ? null : $file_title[0];                        
                        $original_name = $request->file->getClientOriginalName();

                        $duration = @LaravelMP3::getDuration($request->file);

                        if (!is_null($duration)) {
                            $durationArray = explode(':', (string) $duration);
                            if (count($durationArray) > 1) {
                                sscanf($duration, '%d:%d:%d', $hours, $minutes, $seconds);
                                $duration = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
                            }
                        }
                    } catch (\Exception $ex) { }

                    //Upload file on S3 Bucket
                    if (!is_null(env('DBLOCAL'))) {
                        $directory = public_path('uploads/marketplace/');
                        $request->file->move($directory, $filename);
                    } else {
                        Storage::disk('s3B')->put($upload_file_name, file_get_contents($request->file));
                    }

                    $attributes[] = [
                        "user_id" => $user->id,
                        "show_id" => !empty($request->s_id) ? $request->s_id : null,
                        "title" => $file_title,
                        "duration" => $duration,
                        "mp3" => $filename,
                        "file_original_name" => $original_name,
                        "status" => "Incomplete",
                        "created_at" => now(),
                        "updated_at" => now(),
                    ];
                    if (count($attributes) > 0) {
                        //Insert audio data in table
                        EpisodeAudioFileUpload::insert($attributes);
                    }
                    return [
                        'status' => true,
                        'code' => 200,
                        'message' => "Files uploaded successfully."
                    ];
                } else {
                    return [
                        'status' => false,
                        'code' => 200,
                        'message' => "Only Mp3 file allowed.."
                    ];
                }
            } else {
                return [
                    'status' => false,
                    'code' => 200,
                    'message' => 'Sorry Only Upload mp3 or mp4 files.'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => false,
                'code' => 200,
                'message' => "Something is went wrong, Please try again after some time."
            ];
        }
    }

    /**
     * Fetched Audio FIle
     */
    public function episodeAudioData(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'audio_upload_id' => 'required',
            'type' => 'required',
        ], []);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];

            return response()->business_api($response);
        }

        $qry = EpisodeAudioFileUpload::where('id', $request->audio_upload_id);
        if ($request->type == 'series') {
            $qry->where('show_id', $request->s_id);
        }

        $audio_temp_data = $qry->first(['mp3', 'user_id', 'status', 'title', 'duration', 'file_original_name']);

        if (!$audio_temp_data) {
            return response()->business_api([
                'status' => false,
                'message' => "No Audio File Found."
            ]);
        }

        if ($audio_temp_data->user_id != $user->id) {
            return response()->business_api([
                'status' => false,
                'message' => "You can not edit this form."
            ]);
        }

        if ($audio_temp_data->status == "Published") {
            return response()->business_api([
                'status' => false,
                'message' => "You can not update this file."
            ]);
        }
        $data = [
            'title' => $audio_temp_data->title,
            'duration' => (int) ceil($audio_temp_data->duration / 60),
            'mp3' => $audio_temp_data->mp3,
            'file_original_name' => $audio_temp_data->file_original_name,
        ];        

        return response()->business_api([
            'status' => true,
            'message' => "",
            'data' => $data

        ]);
    }

    /**
     * Adding new episode in series
     */
    public function addSeriesEpisode(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'audio_upload_id' => 'required',
            'title' => 'required',
            //'intro' => 'required', 
            'description' => 'required',
            'aired_date' => 'required',
            'pricing' => 'required',
            'status' => 'required'
        ], [
            'title.required' => "Please enter episode title.",
            //'intro.required' => "Please enter episode intro.",
            'description.required' => "Please enter episode description.",
            'aired_date.required' => "Please select episode aired date.",
            'pricing.required' => "Please enter episode pricing.",
            'status.required' => "Please select status.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //\Log::info(json_encode($request->all));        

        try {
            //check if existing user can update episode info or not
            $checkEpisodeUpdate = EpisodeAudioFileUpload::where('id', $request->audio_upload_id)
                ->where('user_id', $user->id)->first();
            if (empty($checkEpisodeUpdate)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not proceed..."
                ]);
            }

            if (is_null($checkEpisodeUpdate->show_id)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not proceed..."
                ]);
            }

            if ($checkEpisodeUpdate->status != "Incomplete" || !is_null($checkEpisodeUpdate->episode_id)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not proceed with this episode..."
                ]);
            }

            $attributes = [
                'audio_upload_id' => $request->audio_upload_id,
                'show_id' => $checkEpisodeUpdate->show_id,
                'title' => $request->title,
                //'intro' => ! empty($request->intro) ? $request->intro : null,
                'description' => !empty($request->description) ? $request->description : null,
                'res_description' => !empty($request->res_description) ? $request->res_description : null,
                'image' => !empty($request->image) ? $request->image : null,
                'mp3' => $checkEpisodeUpdate->mp3,
                'file_original_name' => $checkEpisodeUpdate->file_original_name,
                'duration' => !empty($request->duration) ? $request->duration * 60 : 0,
                'authors' => !empty($request->authors) ? $request->authors : null,
                'narrators' => !empty($request->narrators) ? $request->narrators : null,
                'producers' => !empty($request->producers) ? $request->producers : null,
                'aired_date' => $request->aired_date,
                'tags' => !empty($request->tags) ? $request->tags : null,
                'pricing' => !empty($request->pricing) ? $request->pricing : '{}',
                'language' => $request->language,
                'status' => $request->status
            ];

            $temp_data = EpisodeTemp::updateOrCreate([
                'user_id' => $user->id,
                'audio_upload_id' => $request->audio_upload_id
            ], $attributes);

            // \DB::table("episode_audio_file_uploads")->where('id', $request->audio_upload_id)
            //     ->update(['status' => "Draft"]);

            //Update tbl_episode_resources
            $foundEpisodeResource = EpisodeResource::where('temp_id', $request->audio_upload_id)->first();
            if (!is_null($foundEpisodeResource)) {
                $attributes_resource = [
                    "res_title1" => $request->res_title1,
                    "res_title2" => $request->res_title2,
                    "res_title3" => $request->res_title3,
                    "res_title4" => $request->res_title4,
                ];
                EpisodeResource::updateOrCreate(['temp_id' => $request->audio_upload_id], $attributes_resource);
            }

            //Published Episode
            $this->publishedEpisode($temp_data, $user, 'series');

            return response()->business_api([
                'status' => true,
                'message' => "Successfully added."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return response()->business_api([
            'status' => false,
            'message' => "Something is went wrong, Please try again after some time."
        ]);
    }

    /**
     * Adding / Updating new standalone episode
     */
    /*public function addStandaloneEpisode(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'audio_upload_id' => 'required',
            'title' => 'required',
            //'intro' => 'required',
            'description' => 'required',
            'country' => 'required',
            'language' => 'required',
            'category_id' => 'required',
            'rating' => 'required',
            'aired_date' => 'required',
            'pricing' => 'required',
            'status' => 'required'
        ], [
            'title.required' => "Please enter episode title.",
            //'intro.required' => "Please enter episode intro.",
            'description.required' => "Please enter episode description.",
            'country.required' => "Please select episode country.",
            'language.required' => "Please select episode language.",
            'category_id.required' => "Please select atleast one category.",
            'rating.required' => "Please select episode rating.",
            'aired_date.required' => "Please select episode aired date.",
            'pricing.required' => "Please enter episode pricing.",
            'status.required' => "Please select status.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //check if existing user can update episode info or not
        $checkEpisodeUpdate = EpisodeAudioFileUpload::where('id', $request->audio_upload_id)
            ->where('user_id', $user->id)->first();
        if (empty($checkEpisodeUpdate)) {
            return response()->business_api([
                'status' => false,
                'message' => "You can not proceed..."
            ]);
        }

        if ($checkEpisodeUpdate->status != "Incomplete" || !is_null($checkEpisodeUpdate->episode_id)) {
            return response()->business_api([
                'status' => false,
                'message' => "You can not proceed with this episode..."
            ]);
        }

        try {
            $attributes = [
                'audio_upload_id' => $request->audio_upload_id,
                'title' => $request->title,
                //'intro' => ! empty($request->intro) ? $request->intro : null,
                'description' => !empty($request->description) ? $request->description : null,
                'res_description' => !empty($request->res_description) ? $request->res_description : null,
                'country' => !empty($request->country) ? $request->country : null,
                'language' => !empty($request->language) ? $request->language : null,
                'category_id' => !empty($request->category_id) ? $request->category_id : null,
                'rating' => !empty($request->rating) ? $request->rating : null,
                'image' => !empty($request->image) ? $request->image : null,
                'mp3' => $checkEpisodeUpdate->mp3,
                'file_original_name' => $checkEpisodeUpdate->file_original_name,
                'duration' => !empty($request->duration) ? $request->duration * 60 : 0,
                'authors' => !empty($request->authors) ? $request->authors : null,
                'narrators' => !empty($request->narrators) ? $request->narrators : null,
                'producers' => !empty($request->producers) ? $request->producers : null,
                'aired_date' => $request->aired_date,
                'tags' => !empty($request->tags) ? $request->tags : null,
                'pricing' => !empty($request->pricing) ? $request->pricing : '{}',
                'status' => $request->status
            ];

            $temp_data = EpisodeTemp::updateOrCreate([
                'user_id' => $user->id,
                'audio_upload_id' => $request->audio_upload_id
            ], $attributes);

            // \DB::table("episode_audio_file_uploads")->where('id', $request->audio_upload_id)
            //     ->update(['status' => "Draft"]); 

            //Update tbl_episode_resources
            $foundEpisodeResource = EpisodeResource::where('temp_id', $request->audio_upload_id)->first();
            if (!is_null($foundEpisodeResource)) {
                $attributes_resource = [
                    "res_title1" => $request->res_title1,
                    "res_title2" => $request->res_title2,
                    "res_title3" => $request->res_title3,
                    "res_title4" => $request->res_title4,
                ];
                EpisodeResource::updateOrCreate(['temp_id' => $request->audio_upload_id], $attributes_resource);
            }

            //Published Episode
            $this->publishedEpisode($temp_data, $user, 'standalone');

            return response()->business_api([
                'status' => true,
                'message' => "Successfully added."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return response()->business_api([
            'status' => false,
            'message' => "Something is went wrong, Please try again after some time."
        ]);
    }*/

    public function addStandaloneEpisode(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'audio_upload_id' => 'required',
            'title' => 'required',
            //'intro' => 'required',
            'description' => 'required',
            'country' => 'required',
            'language' => 'required',
            'category_id' => 'required',
            'rating' => 'required',
            'aired_date' => 'required',
            'pricing' => 'required',
            'status' => 'required'
        ], [
            'title.required' => "Please enter episode title.",
            //'intro.required' => "Please enter episode intro.",
            'description.required' => "Please enter episode description.",
            'country.required' => "Please select episode country.",
            'language.required' => "Please select episode language.",
            'category_id.required' => "Please select atleast one category.",
            'rating.required' => "Please select episode rating.",
            'aired_date.required' => "Please select episode aired date.",
            'pricing.required' => "Please enter episode pricing.",
            'status.required' => "Please select status.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //check if existing user can update episode info or not
        $checkEpisodeUpdate = EpisodeAudioFileUpload::where('id', $request->audio_upload_id)
            ->where('user_id', $user->id)->first();
        if (empty($checkEpisodeUpdate)) {
            return response()->business_api([
                'status' => false,
                'message' => "You can not proceed..."
            ]);
        }

        if ($checkEpisodeUpdate->status != "Incomplete" || !is_null($checkEpisodeUpdate->episode_id)) {
            return response()->business_api([
                'status' => false,
                'message' => "You can not proceed with this episode..."
            ]);
        }

        try {
            $attributes = [
                'audio_upload_id' => $request->audio_upload_id,
                'title' => $request->title,
                //'intro' => ! empty($request->intro) ? $request->intro : null,
                'description' => !empty($request->description) ? $request->description : null,
                'res_description' => !empty($request->res_description) ? $request->res_description : null,
                'country' => !empty($request->country) ? $request->country : null,
                'language' => !empty($request->language) ? $request->language : null,
                'category_id' => !empty($request->category_id) ? $request->category_id : null,
                'rating' => !empty($request->rating) ? $request->rating : null,
                'image' => !empty($request->image) ? $request->image : null,
                'shots' => !empty($request->shot) ? $request->shot : null,
                'mp3' => $checkEpisodeUpdate->mp3,
                'file_original_name' => $checkEpisodeUpdate->file_original_name,
                'duration' => !empty($request->duration) ? $request->duration * 60 : 0,
                'authors' => !empty($request->authors) ? $request->authors : null,
                'narrators' => !empty($request->narrators) ? $request->narrators : null,
                'producers' => !empty($request->producers) ? $request->producers : null,
                'aired_date' => $request->aired_date,
                'tags' => !empty($request->tags) ? $request->tags : null,
                'pricing' => !empty($request->pricing) ? $request->pricing : '{}',
                'status' => $request->status
            ];

            $temp_data = EpisodeTemp::updateOrCreate([
                'user_id' => $user->id,
                'audio_upload_id' => $request->audio_upload_id
            ], $attributes);

            // \DB::table("episode_audio_file_uploads")->where('id', $request->audio_upload_id)
            //     ->update(['status' => "Draft"]); 

            //Update tbl_episode_resources
            $foundEpisodeResource = EpisodeResource::where('temp_id', $request->audio_upload_id)->first();
            if (!is_null($foundEpisodeResource)) {
                $attributes_resource = [
                    "res_title1" => $request->res_title1,
                    "res_title2" => $request->res_title2,
                    "res_title3" => $request->res_title3,
                    "res_title4" => $request->res_title4,
                ];
                EpisodeResource::updateOrCreate(['temp_id' => $request->audio_upload_id], $attributes_resource);
            }

            //Published Episode
            $this->publishedEpisode($temp_data, $user, 'standalone');

            return response()->business_api([
                'status' => true,
                'message' => "Successfully added."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return response()->business_api([
            'status' => false,
            'message' => "Something is went wrong, Please try again after some time."
        ]);
    }

    /**
     * Create new show for standalone episode
     */
    protected function createStandaloneSeries($temp_data, $user)
    {

        // create new show and insert show id into table
        $show_attributes = [
            'title' => $temp_data->title,
            'user_id' => $user->id,
            'description' => !empty($temp_data->description) ? $temp_data->description : null,
            'country' => !empty($temp_data->country) ? $temp_data->country : null,
            'language' => !empty($temp_data->language) ? $temp_data->language : null,
            'rating' => !empty($temp_data->rating) ? $temp_data->rating : null,
            'status' => "Published",
            'content_type' => 2,
            'created_by' => $user->id,
            'updated_by' => $user->id
        ];

        //Create Series
        if (!is_null(env('DBLOCAL'))) {
            $seriesId = Show::insertGetId($show_attributes);
            $series = Show::find($seriesId);
        } else {
            $series = Show::create($show_attributes);
        }

        if (!empty($temp_data->category_id)) {
            $categoryArr = explode(",", trim($temp_data->category_id));
            $categoryArr = array_filter($categoryArr);
            $categoryArr = array_unique($categoryArr);
            $series->categories()->sync($categoryArr);
        }

        if (is_null(env('DBLOCAL'))) {
            //This code for elastic search
            //$series->searchable();
            $series->unsearchable();
        }

        return $series;
    }

    /**
     * Push Episode content to Episode table
     */
    /*protected function publishedEpisode($temp_data, $user, $type)
    {
        try {
            $uuid = uniqid();
            $uuid .= $this->getRandomNumber(10);

            $date_created = Carbon::parse($temp_data->aired_date)->format('Y-m-d');

            //first we have to create new show for this standalone episode
            $show_id = null;
            $content_type = 1;
            if ($type == "standalone") {
                $series_data = $this->createStandaloneSeries($temp_data, $user);
                $show_id = $series_data->id;
                $content_type = 2;
            } else {
                $show_id = $temp_data->show_id;
            }

            $temp_status = $temp_data->status;

            $attributes = [
                'uuid' => $uuid,
                'title' => $temp_data->title,
                'show_id' => $show_id,
                'description' => $temp_data->description,
                'image' => $temp_data->image,
                'mp3' => $temp_data->mp3,
                'duration' => $temp_data->duration,
                'date_created' => $date_created,
                'date_added' => $temp_data->aired_date,
                'status' => $temp_status,
                'content_type' => $content_type, // 0 - Free, 1 - Premium Series, 2 - Premium Standalone
                'created_by' => $user->id,
                'updated_by' => $user->id
            ];

            if (!is_null(env('DBLOCAL'))) {
                $episodeId = Episode::insertGetId($attributes);
                $episode = Episode::find($episodeId);
            } else {
                $episode = Episode::create($attributes);
            }

            //Also Update status in tbl_episode_audio_file_uploads table
            \DB::table("episode_audio_file_uploads")->where('id', $temp_data->audio_upload_id)
                ->update(['status' => "Published", 'show_id' => $show_id, 'episode_id' => $episode->id]);

            //Also Update status in tbl_episode_audio_file_uploads table
            \DB::table("episode_temp")->where('audio_upload_id', $temp_data->audio_upload_id)
                ->update(['status' => "PushtoEpisode", 'show_id' => $show_id]);

            //Adding category
            $categoryIds = $episode->show->categories->pluck('id')->all();
            if (count($categoryIds) > 0) {
                $categoryIds = array_filter($categoryIds);
                $categoryIds = array_unique($categoryIds);
                $episode->categories()->sync($categoryIds);
            }

            //adding episode tags
            if (!empty($temp_data->tags)) {
                $tagData = explode(",", $temp_data->tags);
                $tagData = array_unique($tagData);
                $tagData = array_filter($tagData);
                $episode->tags()->sync($tagData);
            }

            // Insert extra episode info
            $episode_info = EpisodeDetail::firstOrNew(['episode_id' => $episode->id]);
            $episode_info->fill([
                "title" => $episode->title,
                "user_id" => $user->id,
                "show_id" => $show_id,
                "intro" => !empty($temp_data->intro) ? $temp_data->intro : null,
                "language" => !empty($episode->show->language) ? $episode->show->language : null,
                "rating" => !empty($episode->show->rating) ? $episode->show->rating : null,
                "country" => !empty($episode->show->country) ? $episode->show->country : null,
                "status" => $temp_status,
                "content_type" => $episode->show->content_type,
                "res_description" => !empty($temp_data->res_description) ? $temp_data->res_description : null,
                "file_original_name" => !empty($temp_data->file_original_name) ? $temp_data->file_original_name : null,
            ])->save();


            //Update episode id if we found audio_upload_id in tbl_episode_resource table
            $foundEpisodeResource = EpisodeResource::where('temp_id', $temp_data->audio_upload_id)->first();
            if (!is_null($foundEpisodeResource)) {
                EpisodeResource::where('temp_id', $temp_data->audio_upload_id)->update(['episode_id' => $episode->id]);
            }

            //adding authors with episode
            if (!empty($temp_data->authors)) {
                $authors = explode(",", $temp_data->authors);
                $authors = array_filter($authors);
                $authors = array_unique($authors);
                $episode->authors()->sync($authors);
            }

            //adding narrators with episode
            if (!empty($temp_data->narrators)) {
                $narrators = explode(",", $temp_data->narrators);
                $narrators = array_filter($narrators);
                $narrators = array_unique($narrators);
                $episode->narrators()->sync($narrators);
            }

            //adding producers with episode
            if (!empty($temp_data->producers)) {
                $producers = explode(",", $temp_data->producers);
                $producers = array_filter($producers);
                $producers = array_unique($producers);
                $episode->producers()->sync($producers);
            }

            //This code for elastic search
            if (is_null(env('DBLOCAL'))) {
                if ($temp_status == "Published") {
                    //$episode->searchable();
                    $episode->unsearchable();
                    $episode->show()->unsearchable(); //Just Remove This Line
                } else {
                    $episode->unsearchable();
                    $episode->show()->unsearchable(); //Just Remove This Line
                }
            }

            //adding pricing with episode
            if (!empty($temp_data->pricing)) {
                $episode_pricing = json_decode($temp_data->pricing, 1);
                foreach ($episode_pricing as $item) {
                    $attributes = [
                        "episode_id" => $episode->id,
                        "country_id" => $item['country_id'],
                        "currency_id" => $item['currency_id'],
                        "apple_product_id" => !empty($item['apple_product_id']) ? $item['apple_product_id'] : null,
                        "android_product_id" => !empty($item['android_product_id']) ? $item['android_product_id'] : null,
                        "web_price" => !empty($item['web_price']) ? $item['web_price'] : 0,
                        "price" => !empty($item['price']) ? $item['price'] : 0,
                        "markup_price" => !empty($item['markup_price']) ? $item['markup_price'] : 0,
                        "commission_price" => !empty($item['commission_price']) ? $item['commission_price'] : 0,
                        "selling_price" => !empty($item['selling_price']) ? $item['selling_price'] : 0,
                    ];
                    PremiumPricingEpisode::create($attributes);
                }
            }
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return true;
    }*/

    protected function publishedEpisode($temp_data, $user, $type)
    {
        try {
            $uuid = uniqid();
            $uuid .= $this->getRandomNumber(10);

            $date_created = Carbon::parse($temp_data->aired_date)->format('Y-m-d');

            //first we have to create new show for this standalone episode
            $show_id = null;
            $content_type = 1;
            if ($type == "standalone") {
                $series_data = $this->createStandaloneSeries($temp_data, $user);
                $show_id = $series_data->id;
                $content_type = 2;
            } else {
                $show_id = $temp_data->show_id;
            }

            $temp_status = $temp_data->status;

            $attributes = [
                'uuid' => $uuid,
                'title' => $temp_data->title,
                'show_id' => $show_id,
                'description' => $temp_data->description,
                'image' => $temp_data->image,
                'mp3' => $temp_data->mp3,
                'duration' => $temp_data->duration,
                'date_created' => $date_created,
                'date_added' => $temp_data->aired_date,
                'status' => $temp_status,
                'content_type' => $content_type, // 0 - Free, 1 - Premium Series, 2 - Premium Standalone
                'created_by' => $user->id,
                'updated_by' => $user->id
            ];

            if (!is_null(env('DBLOCAL'))) {
                $episodeId = Episode::insertGetId($attributes);
                $episode = Episode::find($episodeId);
            } else {
                $episode = Episode::create($attributes);
            }

            //Also Update status in tbl_episode_audio_file_uploads table
            \DB::table("episode_audio_file_uploads")->where('id', $temp_data->audio_upload_id)
                ->update(['status' => "Published", 'show_id' => $show_id, 'episode_id' => $episode->id]);

            //Also Update status in tbl_episode_audio_file_uploads table
            \DB::table("episode_temp")->where('audio_upload_id', $temp_data->audio_upload_id)
                ->update(['status' => "PushtoEpisode", 'show_id' => $show_id]);

            //Adding category
            $categoryIds = $episode->show->categories->pluck('id')->all();
            if (count($categoryIds) > 0) {
                $categoryIds = array_filter($categoryIds);
                $categoryIds = array_unique($categoryIds);
                $episode->categories()->sync($categoryIds);
            }

            //adding episode tags
            if (!empty($temp_data->tags)) {
                $tagData = explode(",", $temp_data->tags);
                $tagData = array_unique($tagData);
                $tagData = array_filter($tagData);
                $episode->tags()->sync($tagData);
            }

            // Insert extra episode info
            $episode_info = EpisodeDetail::firstOrNew(['episode_id' => $episode->id]);
            $episode_info->fill([
                "title" => $episode->title,
                "user_id" => $user->id,
                "show_id" => $show_id,
                "intro" => !empty($temp_data->intro) ? $temp_data->intro : null,
                "shots" => !empty($temp_data->shots) ? $temp_data->shots : null,
                "language" => !empty($episode->show->language) ? $episode->show->language : null,
                "rating" => !empty($episode->show->rating) ? $episode->show->rating : null,
                "country" => !empty($episode->show->country) ? $episode->show->country : null,
                "status" => $temp_status,
                "content_type" => $episode->show->content_type,
                "res_description" => !empty($temp_data->res_description) ? $temp_data->res_description : null,
                "file_original_name" => !empty($temp_data->file_original_name) ? $temp_data->file_original_name : null,
            ])->save();


            //Update episode id if we found audio_upload_id in tbl_episode_resource table
            $foundEpisodeResource = EpisodeResource::where('temp_id', $temp_data->audio_upload_id)->first();
            if (!is_null($foundEpisodeResource)) {
                EpisodeResource::where('temp_id', $temp_data->audio_upload_id)->update(['episode_id' => $episode->id]);
            }

            //adding authors with episode
            if (!empty($temp_data->authors)) {
                $authors = explode(",", $temp_data->authors);
                $authors = array_filter($authors);
                $authors = array_unique($authors);
                $episode->authors()->sync($authors);
            }

            //adding narrators with episode
            if (!empty($temp_data->narrators)) {
                $narrators = explode(",", $temp_data->narrators);
                $narrators = array_filter($narrators);
                $narrators = array_unique($narrators);
                $episode->narrators()->sync($narrators);
            }

            //adding producers with episode
            if (!empty($temp_data->producers)) {
                $producers = explode(",", $temp_data->producers);
                $producers = array_filter($producers);
                $producers = array_unique($producers);
                $episode->producers()->sync($producers);
            }

            //This code for elastic search
            if (is_null(env('DBLOCAL'))) {
                if ($temp_status == "Published") {
                    //$episode->searchable();
                    $episode->unsearchable();
                    $episode->show()->unsearchable(); //Just Remove This Line
                } else {
                    $episode->unsearchable();
                    $episode->show()->unsearchable(); //Just Remove This Line
                }
            }

            //adding pricing with episode
            if (!empty($temp_data->pricing)) {
                $episode_pricing = json_decode($temp_data->pricing, 1);
                foreach ($episode_pricing as $item) {
                    $attributes = [
                        "episode_id" => $episode->id,
                        "country_id" => $item['country_id'],
                        "currency_id" => $item['currency_id'],
                        "apple_product_id" => !empty($item['apple_product_id']) ? $item['apple_product_id'] : null,
                        "android_product_id" => !empty($item['android_product_id']) ? $item['android_product_id'] : null,
                        "web_price" => !empty($item['web_price']) ? $item['web_price'] : 0,
                        "price" => !empty($item['price']) ? $item['price'] : 0,
                        "markup_price" => !empty($item['markup_price']) ? $item['markup_price'] : 0,
                        "commission_price" => !empty($item['commission_price']) ? $item['commission_price'] : 0,
                        "selling_price" => !empty($item['selling_price']) ? $item['selling_price'] : 0,
                    ];
                    PremiumPricingEpisode::create($attributes);
                }
            }
            //Remove data from tbl_episode_temp
            //$temp_data->remove();

        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return true;
    }

    /**
     * Edit Series Episode
     */
    public function editSeriesEpisode(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'title' => 'required',
            //'intro' => 'required', 
            'description' => 'required',
            'aired_date' => 'required',
            'status' => 'required'
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
            'title.required' => "Please enter episode title.",
            //'intro.required' => "Please enter episode intro.",
            'description.required' => "Please enter episode description.",
            'aired_date.required' => "Please select episode aired date.",
            'status.required' => "Please select status.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $episode = Episode::find($request->episode_id);

            if (!$episode) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if (is_null($episode->episode_extra_info)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->user_id != $user->id) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->content_type == 2) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not modified this episode, because this episode is not belong to series."
                ]);
            }

            $date_created = Carbon::parse($request->aired_date)->format('Y-m-d');
            $attributes = [
                'title' => $request->title,
                'description' => !empty($request->description) ? $request->description : null,
                'image' => !empty($request->image) ? $request->image : null,
                'duration' => !empty($request->duration) ? $request->duration * 60 : 0,
                'date_created' => $date_created,
                'date_added' => $request->aired_date,
                'status' => $request->status,
                'updated_by' => $user->id
            ];

            //Update Episode
            if (is_null(env('DBLOCAL'))) {
                $episode->fill($attributes)->save();
            }

            // Insert extra episode info
            $episode_info = EpisodeDetail::where('episode_id', $episode->id)->first();
            $episode_info->fill([
                "title" => $episode->title,
                //"intro" => ! empty($request->intro) ? $request->intro : null,
                "language" => !empty($request->language) ? $request->language : null,
                "rating" => !empty($request->rating) ? $request->rating : null,
                "res_description" => !empty($request->res_description) ? $request->res_description : null,
                "status" => $request->status
            ])->save();

            if (is_null(env('DBLOCAL'))) {
                $showData = Show::where('id', $episode->show_id)->first();
                $showData->fill([
                    "language" => !empty($request->language) ? $request->language : null,
                    "rating" => !empty($request->rating) ? $request->rating : null
                ]);
            }

            // Insert / Update tbl_episode_resources
            if ($request->res_file1 != "" || $request->res_file2 != "" || $request->res_file3 != "" || $request->res_file4 != "") {
                $foundEpisodeResource = EpisodeResource::where('episode_id', $episode->id)->first();
                if (!is_null($foundEpisodeResource)) {

                    $request->res_title1 = !empty($request->res_title1) ? $request->res_title1 : null;
                    $request->res_title2 = !empty($request->res_title2) ? $request->res_title2 : null;
                    $request->res_title3 = !empty($request->res_title3) ? $request->res_title3 : null;
                    $request->res_title4 = !empty($request->res_title4) ? $request->res_title4 : null;

                    $attributes_resource = [
                        "res_title1" => !empty($request->res_file1) ? $request->res_title1 : null,
                        "res_title2" => !empty($request->res_file2) ? $request->res_title2 : null,
                        "res_title3" => !empty($request->res_file3) ? $request->res_title3 : null,
                        "res_title4" => !empty($request->res_file4) ? $request->res_title4 : null,
                    ];
                    EpisodeResource::updateOrCreate(['episode_id' => $episode->id], $attributes_resource);
                }
            }

            //adding episode tags
            $tagData = [];
            if (!empty($request->tags)) {
                $tagData = explode(",", $request->tags);
                $tagData = array_unique($tagData);
                $tagData = array_filter($tagData);
            }
            $episode->tags()->sync($tagData);

            //adding authors with episode
            $authors = [];
            if (!empty($request->authors)) {
                $authors = explode(",", $request->authors);
                $authors = array_filter($authors);
                $authors = array_unique($authors);
            }
            $episode->authors()->sync($authors);

            //adding narrators with episode
            $narrators = [];
            if (!empty($request->narrators)) {
                $narrators = explode(",", $request->narrators);
                $narrators = array_filter($narrators);
                $narrators = array_unique($narrators);
            }
            $episode->narrators()->sync($narrators);

            //adding producers with episode
            $producers = [];
            if (!empty($request->producers)) {
                $producers = explode(",", $request->producers);
                $producers = array_filter($producers);
                $producers = array_unique($producers);
            }
            $episode->producers()->sync($producers);

            if (is_null(env('DBLOCAL'))) {
                if ($request->status == 'Published') {
                    //$episode->searchable();
                    $episode->unsearchable();
                    $episode->show()->unsearchable(); //Just Remove this Line
                } else {
                    $episode->unsearchable();
                    $episode->show()->unsearchable(); //Just Remove this Line
                }
            }

            return response()->business_api([
                'status' => true,
                'message' => "Successfully updated."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return response()->business_api([
            'status' => false,
            'message' => "Something is went wrong, Please try again after some time."
        ]);
    }

    /**
     * Edit Standalone Episode
     */
    /*public function editStandaloneEpisode(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            //'intro' => 'required', 
            'description' => 'required',
            'aired_date' => 'required',
            'status' => 'required',
            'country' => 'required'
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
            'category_id.required' => '6000:1 - category id missing for episode',
            'title.required' => "Please enter episode title.",
            //'intro.required' => "Please enter episode intro.",
            'description.required' => "Please enter episode description.",
            'aired_date.required' => "Please select episode aired date.",
            'status.required' => "Please select status.",
            'country.required' => "Please select country.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $episode = Episode::find($request->episode_id);

            if (!$episode) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if (is_null($episode->episode_extra_info)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->user_id != $user->id) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->content_type == 1) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not modified this episode, because this episode is not standalone episode."
                ]);
            }

            $date_created = Carbon::parse($request->aired_date)->format('Y-m-d');
            $attributes = [
                'title' => $request->title,
                'description' => !empty($request->description) ? $request->description : null,
                'image' => !empty($request->image) ? $request->image : null,
                'duration' => !empty($request->duration) ? $request->duration * 60 : 0,
                'date_created' => $date_created,
                'date_added' => $request->aired_date,
                'status' => $request->status,
                'updated_by' => $user->id
            ];

            //Update Episode
            if (is_null(env('DBLOCAL'))) {
                $episode->fill($attributes)->save();
            }

            // Insert extra episode info
            $episode_info = EpisodeDetail::where('episode_id', $episode->id)->first();
            $episode_info->fill([
                "title" => $episode->title,
                //"intro" => ! empty($request->intro) ? $request->intro : null,
                "language" => !empty($request->language) ? $request->language : null,
                "rating" => !empty($request->rating) ? $request->rating : null,
                "country" => !empty($request->country) ? $request->country : null,
                "res_description" => !empty($request->res_description) ? $request->res_description : null,
                "status" => $request->status
            ])->save();

            //Also update in show table            
            if (is_null(env('DBLOCAL'))) {
                $showData = Show::where('id', $episode->show_id)->first();
                $showData->fill([
                    "language" => !empty($request->language) ? $request->language : null,
                    "rating" => !empty($request->rating) ? $request->rating : null,
                    "country" => !empty($request->country) ? $request->country : null,
                ]);

                //updating show categories
                $categoryShowData = [];
                if (!empty($request->category_id)) {
                    $categoryShowData = explode(",", $request->category_id);
                    $categoryShowData = array_unique($categoryShowData);
                    $categoryShowData = array_filter($categoryShowData);
                }
                $showData->categories()->sync($categoryShowData);
            }

            //updating episode categories
            $categoryData = [];
            if (!empty($request->category_id)) {
                $categoryData = explode(",", $request->category_id);
                $categoryData = array_unique($categoryData);
                $categoryData = array_filter($categoryData);
            }
            $episode->categories()->sync($categoryData);

            //updating episode tags
            $tagData = [];
            if (!empty($request->tags)) {
                $tagData = explode(",", $request->tags);
                $tagData = array_unique($tagData);
                $tagData = array_filter($tagData);
            }
            $episode->tags()->sync($tagData);

            //updating authors with episode
            $authors = [];
            if (!empty($request->authors)) {
                $authors = explode(",", $request->authors);
                $authors = array_filter($authors);
                $authors = array_unique($authors);
            }
            $episode->authors()->sync($authors);

            //updating narrators with episode
            $narrators = [];
            if (!empty($request->narrators)) {
                $narrators = explode(",", $request->narrators);
                $narrators = array_filter($narrators);
                $narrators = array_unique($narrators);
            }
            $episode->narrators()->sync($narrators);

            //updating producers with episode
            $producers = [];
            if (!empty($request->producers)) {
                $producers = explode(",", $request->producers);
                $producers = array_filter($producers);
                $producers = array_unique($producers);
            }
            $episode->producers()->sync($producers);

            if (is_null(env('DBLOCAL'))) {
                if ($request->status == 'Published') {
                    //$episode->searchable();
                    $episode->unsearchable();
                } else {
                    $episode->unsearchable();
                }
            }

            return response()->business_api([
                'status' => true,
                'message' => "Successfully updated."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return response()->business_api([
            'status' => false,
            'message' => "Something is went wrong, Please try again after some time."
        ]);
    }*/

    public function editStandaloneEpisode(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            //'intro' => 'required', 
            'description' => 'required',
            'aired_date' => 'required',
            'status' => 'required',
            'country' => 'required'
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
            'category_id.required' => '6000:1 - category id missing for episode',
            'title.required' => "Please enter episode title.",
            //'intro.required' => "Please enter episode intro.",
            'description.required' => "Please enter episode description.",
            'aired_date.required' => "Please select episode aired date.",
            'status.required' => "Please select status.",
            'country.required' => "Please select country.",
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $episode = Episode::find($request->episode_id);

            if (!$episode) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if (is_null($episode->episode_extra_info)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->user_id != $user->id) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->content_type == 1) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not modified this episode, because this episode is not standalone episode."
                ]);
            }

            $date_created = Carbon::parse($request->aired_date)->format('Y-m-d');
            $attributes = [
                'title' => $request->title,
                'description' => !empty($request->description) ? $request->description : null,
                'image' => !empty($request->image) ? $request->image : null,
                'duration' => !empty($request->duration) ? $request->duration * 60 : 0,
                'date_created' => $date_created,
                'date_added' => $request->aired_date,
                'status' => $request->status,
                'updated_by' => $user->id
            ];

            //Update Episode
            if (is_null(env('DBLOCAL'))) {
                $episode->fill($attributes)->save();
            }

            // Insert extra episode info
            $episode_info = EpisodeDetail::where('episode_id', $episode->id)->first();
            $episode_info->fill([
                "title" => $episode->title,
                //"intro" => ! empty($request->intro) ? $request->intro : null,
                "language" => !empty($request->language) ? $request->language : null,
                "rating" => !empty($request->rating) ? $request->rating : null,
                "country" => !empty($request->country) ? $request->country : null,
                "res_description" => !empty($request->res_description) ? $request->res_description : null,
                'shots' => !empty($request->shot) ? $request->shot : null,
                "status" => $request->status
            ])->save();

            //Also update in show table            
            if (is_null(env('DBLOCAL'))) {
                $showData = Show::where('id', $episode->show_id)->first();
                $showData->fill([
                    "title" => $episode->title,
                    "language" => !empty($request->language) ? $request->language : null,
                    "rating" => !empty($request->rating) ? $request->rating : null,
                    "country" => !empty($request->country) ? $request->country : null,
                ])->save();

                //updating show categories
                $categoryShowData = [];
                if (!empty($request->category_id)) {
                    $categoryShowData = explode(",", $request->category_id);
                    $categoryShowData = array_unique($categoryShowData);
                    $categoryShowData = array_filter($categoryShowData);
                }
                $showData->categories()->sync($categoryShowData);

                //$showData->searchable();
                $showData->unsearchable();
            }

            //updating episode categories
            $categoryData = [];
            if (!empty($request->category_id)) {
                $categoryData = explode(",", $request->category_id);
                $categoryData = array_unique($categoryData);
                $categoryData = array_filter($categoryData);
            }
            $episode->categories()->sync($categoryData);

            //updating episode tags
            $tagData = [];
            if (!empty($request->tags)) {
                $tagData = explode(",", $request->tags);
                $tagData = array_unique($tagData);
                $tagData = array_filter($tagData);
            }
            $episode->tags()->sync($tagData);

            //updating authors with episode
            $authors = [];
            if (!empty($request->authors)) {
                $authors = explode(",", $request->authors);
                $authors = array_filter($authors);
                $authors = array_unique($authors);
            }
            $episode->authors()->sync($authors);

            //updating narrators with episode
            $narrators = [];
            if (!empty($request->narrators)) {
                $narrators = explode(",", $request->narrators);
                $narrators = array_filter($narrators);
                $narrators = array_unique($narrators);
            }
            $episode->narrators()->sync($narrators);

            //updating producers with episode
            $producers = [];
            if (!empty($request->producers)) {
                $producers = explode(",", $request->producers);
                $producers = array_filter($producers);
                $producers = array_unique($producers);
            }
            $episode->producers()->sync($producers);

            if (is_null(env('DBLOCAL'))) {
                if ($request->status == 'Published') {
                    //$episode->searchable();
                    $episode->unsearchable();
                } else {
                    $episode->unsearchable();
                }
            }

            return response()->business_api([
                'status' => true,
                'message' => "Successfully updated."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        return response()->business_api([
            'status' => false,
            'message' => "Something is went wrong, Please try again after some time."
        ]);
    }

    /**
     * This function is use for getting series price
     */
    public function getEpisodePricing(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'type' => 'required',
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
            'type.required' => '6000:1 - type missing for episode'
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Series
            $episode = Episode::where('id', $request->episode_id)->first(['id', 'title']);

            if (!$episode) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if (is_null($episode->episode_extra_info)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->user_id != $user->id) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not modified this episode, because this episode is not belong to you."
                ]);
            }

            if ($request->type == "series") {
                if ($episode->episode_extra_info->content_type == 2) {
                    return response()->business_api([
                        'status' => false,
                        'message' => "You can not modified this episode, because this episode is not belong to series."
                    ]);
                }
            }

            //Getting currently published country
            //$marketplace_country = \App\Models\MarketplaceCountry::where('status', "Published")->pluck('id')->all();

            //Getting pricing
            $premium_price = PremiumPricingEpisode::where('episode_id', $episode->id)
                //->whereIn('country_id', $marketplace_country)
                ->get(['id', 'country_id', 'currency_id', 'apple_product_id', 'android_product_id', 'web_price',
                'price', 'markup_price', 'commission_price', 'selling_price'
            ]);

            $data = [];
            foreach ($premium_price as $item) {
                $data[] = [
                    "id" => $item->id,
                    "country_id" => $item->country_id,
                    "currency_id" => $item->currency_id,
                    "price" => $item->price,
                    "markup_price" => $item->markup_price,
                    "selling_price" => $item->selling_price
                ];
            }

            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => $data,
                'episode' => $episode->title,
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * Update Series Pricing
     */
    public function updateEpisodePricing(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'pricing' => 'required',
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
            'pricing.required' => "Please enter series pricing."
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Getting Episode
        $episode = Episode::find($request->episode_id);

        if (!$episode) {
            return response()->business_api([
                'status' => false,
                'message' => "No Episode Found."
            ]);
        }

        if (is_null($episode->episode_extra_info)) {
            return response()->business_api([
                'status' => false,
                'message' => "No Episode Found."
            ]);
        }

        if ($episode->episode_extra_info->user_id != $user->id) {
            return response()->business_api([
                'status' => false,
                'message' => "No Episode Found."
            ]);
        }

        try {
            if (!empty($request->pricing)) {
                $oldpricedataIds = PremiumPricingEpisode::where('episode_id', $episode->id)->pluck('id')->all();
                $show_pricing = json_decode($request->pricing, 1);
                $insertedArray = [];
                //dump($show_pricing);
                foreach ($show_pricing as $item) {
                    $checkContentPrice = PremiumPricingEpisode::where('episode_id', $episode->id)->where('country_id', $item['country_id'])->where('currency_id', $item['currency_id'])->first();
                    if (empty($checkContentPrice)) {
                        $attributes = [
                            "episode_id" => $episode->id,
                            "country_id" => $item['country_id'],
                            "currency_id" => $item['currency_id'],
                            "apple_product_id" => !empty($item['apple_product_id']) ? $item['apple_product_id'] : null,
                            "android_product_id" => !empty($item['android_product_id']) ? $item['android_product_id'] : null,
                            "web_price" => !empty($item['web_price']) ? $item['web_price'] : 0,
                            "price" => !empty($item['price']) ? $item['price'] : 0,
                            "markup_price" => !empty($item['markup_price']) ? $item['markup_price'] : 0,
                            "commission_price" => !empty($item['commission_price']) ? $item['commission_price'] : 0,
                            "selling_price" => !empty($item['selling_price']) ? $item['selling_price'] : 0
                        ];
                        PremiumPricingEpisode::create($attributes);
                    } else {
                        //Insert into history table
                        $createdHistoryArrayData = [
                            "premium_pricing_id" => $checkContentPrice->id,
                            "episode_id" => $checkContentPrice->episode_id,
                            "country_id" => $checkContentPrice->country_id,
                            "currency_id" => $checkContentPrice->currency_id,
                            "apple_product_id" => $checkContentPrice->apple_product_id,
                            "android_product_id" => $checkContentPrice->android_product_id,
                            "web_price" => $checkContentPrice->web_price,
                            "price" => $checkContentPrice->price,
                            "markup_price" => $checkContentPrice->markup_price,
                            "commission_price" => $checkContentPrice->commission_price,
                            "selling_price" => $checkContentPrice->selling_price
                        ];
                        PremiumPricingEpisodesHistory::create($createdHistoryArrayData);

                        //Update episode pricing table
                        $attributes = $item;
                        $attributes['apple_product_id'] = !empty($item['apple_product_id']) ? $item['apple_product_id'] : null;
                        $attributes['android_product_id'] = !empty($item['android_product_id']) ? $item['android_product_id'] : null;
                        $attributes['web_price'] = !empty($item['web_price']) ? $item['web_price'] : 0;

                        $checkContentPrice->fill($attributes)->save();

                        $insertedArray[] = $checkContentPrice->id;
                    }
                }

                $removeIds = [];
                if (count($oldpricedataIds) > 0) {
                    $removeIds = array_diff($oldpricedataIds, $insertedArray);
                }

                //Delete data from pricing table
                if (count($removeIds) > 0) {
                    $removeData = PremiumPricingEpisode::whereIn('id', $removeIds);
                    if ($removeData->count() > 0) {
                        $removeData->delete();
                    }
                }
            }

            return response()->business_api([
                'status' => true,
                'message' => "Episode price has been updated."
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * This function is display episode info
     */
    /*public function viewEpisode(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'type' => 'required',
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
            'type.required' => '6000:1 - type missing for episode',
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Episode Info
            $episode = Episode::where('id', $request->episode_id)->first(['id', 'show_id', 'title', 'image', 'description', 'mp3', 'duration', 'date_added', 'status']);            
            
            if (!$episode) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }
            
            if (is_null($episode->episode_extra_info)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }
            
            if ($episode->episode_extra_info->user_id != $user->id) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not modified this episode, because this episode is not belong to you."
                ]);
            }
            
            if ($request->type == "series") {
                if ($episode->episode_extra_info->content_type == 2) {
                    return response()->business_api([
                        'status' => false,
                        'message' => "You can not modified this episode, because this episode is not belong to series."
                    ]);
                }
            }
            

            //Getting categories
            $categories = $episode->categories->pluck('title', 'id')->all();
            $categoryData = [];
            foreach ($categories as $key => $value) {
                $categoryData[] = [
                    'id' => $key,
                    'name' => $value
                ];
            }

            //Getting authors
            $authors = $episode->authors()->get(['id', 'first_name', 'email']);
            $authorData = [];
            foreach ($authors as $key => $value) {
                $authorData[] = [
                    'id' => $value->id,
                    'name' => $value->first_name . " - " . $value->email
                ];
            }

            //dd($authorData);

            //Getting narrators
            $narrators = $episode->narrators()->get(['id', 'first_name', 'email']);
            $narratorData = [];
            foreach ($narrators as $key => $value) {
                $narratorData[] = [
                    'id' => $value->id,
                    'name' => $value->first_name . " - " . $value->email
                ];
            }

            //Getting producers
            $producers = $episode->producers()->get(['id', 'first_name', 'email']);
            $producerData = [];
            foreach ($producers as $key => $value) {
                $producerData[] = [
                    'id' => $value->id,
                    'name' => $value->first_name . " - " . $value->email
                ];
            }

            //Getting tags
            $tags = $episode->tags->pluck('title', 'id')->all();
            $tagData = [];
            foreach ($tags as $key => $value) {
                $tagData[] = [
                    'id' => $key,
                    'name' => trim($value, '"')
                ];
            }

            $full_image = !is_null($episode->image) ? $episode->episode_extra_info->getEpisodeImage(200) : '';
            $image_status = !is_null($episode->image) ? 1 : 0;

            $imgsize = '';
            $imgext = '';
            $img_name = '';
            if ($episode->image) {
                if (!is_null(env('DBLOCAL'))) {
                    if (file_exists(public_path('uploads/episodes/' . $episode->image))) {
                        $imgsize = filesize(public_path('uploads/episodes/' . $episode->image));
                        $path_parts = pathinfo(public_path('uploads/episodes/' . $episode->image));
                        $imgext = 'image/' . $path_parts['extension'];
                        $img_name = $path_parts['filename'];
                    } else {
                        $image_status = 0;
                    }
                } else {
                    if (Storage::disk('s3')->exists('episodes/' . $episode->image)) {
                        $imgsize = Storage::disk('s3')->size('episodes/' . $episode->image);
                        $path_parts = pathinfo($episode->episode_extra_info->getEpisodeImage());
                        $imgext = 'image/' . $path_parts['extension'];
                        $img_name = $path_parts['filename'];
                    } else {
                        $image_status = 0;
                    }
                }
            }

            //Fetching Country Data
            $countryData = [];
            if (!is_null($episode->episode_extra_info->country)) {
                $countrySql = Ip2LocationCountry::where('country_alpha3_code', $episode->episode_extra_info->country)
                    ->first(['country_name', 'country_alpha3_code']);
                if (!empty($countrySql)) {
                    $countryData[] = [
                        'id' => $countrySql->country_alpha3_code,
                        'name' => $countrySql->country_name
                    ];
                }
            }

            //Fetching Language Data
            $languageData = [];
            if (!is_null($episode->episode_extra_info->language)) {
                $languageSql = ShowLanguage::where('short_code', $episode->episode_extra_info->language)
                    ->first(['short_code', 'title']);
                if (!empty($languageSql)) {
                    $languageData[] = [
                        'id' => $languageSql->short_code,
                        'name' => $languageSql->title
                    ];
                }
            }

            //Getting all uploaded resources files
            $resourcesFiles = EpisodeResource::where('episode_id', $episode->id)->first([
                'res_title1', 'res_file1',
                'res_title2', 'res_file2',
                'res_title3', 'res_file3',
                'res_title4', 'res_file4'
            ]);
            $resFileData = [];
            if (!empty($resourcesFiles->res_file1)) {
                $resFileData[] = [
                    "id" => 1,
                    "title" => !empty($resourcesFiles->res_title1) ? $resourcesFiles->res_title1 : '',
                    "file" => $resourcesFiles->res_file1
                ];
            }
            if (!empty($resourcesFiles->res_file2)) {
                $resFileData[] = [
                    "id" => 2,
                    "title" => !empty($resourcesFiles->res_title2) ? $resourcesFiles->res_title2 : '',
                    "file" => $resourcesFiles->res_file2
                ];
            }
            if (!empty($resourcesFiles->res_file3)) {
                $resFileData[] = [
                    "id" => 3,
                    "title" => !empty($resourcesFiles->res_title3) ? $resourcesFiles->res_title3 : '',
                    "file" => $resourcesFiles->res_file3
                ];
            }
            if (!empty($resourcesFiles->res_file4)) {
                $resFileData[] = [
                    "id" => 4,
                    "title" => !empty($resourcesFiles->res_title4) ? $resourcesFiles->res_title4 : '',
                    "file" => $resourcesFiles->res_file4
                ];
            }

            $mp3 = is_null($episode->mp3) ? "" : $episode->mp3;
            $res_description = is_null($episode->episode_extra_info->res_description) ? "" : $episode->episode_extra_info->res_description;
            //dd($episode->episode_extra_info->file_original_name);
            $org_mp3 = ! is_null($episode->episode_extra_info->file_original_name) ? $episode->episode_extra_info->file_original_name : $mp3;

            $duration = (int) ceil($episode->duration / 60);
            $data = collect($episode)
                ->put('mp3', $mp3)
                ->put('file_original_name', $org_mp3)
                ->put('intro', $episode->episode_extra_info->intro)
                ->put('res_description', $res_description)
                //->put('resourcesFiles', $resourcesFiles)
                ->put('language', $languageData)
                ->put('duration', $duration)
                ->put('rating', $episode->episode_extra_info->rating)
                ->put('categories', $categoryData)
                ->put('country', $countryData)
                ->put('tags', $tagData)
                ->put("authors", $authorData)
                ->put('narrators', $narratorData)
                ->put('producers', $producerData)
                ->put('img_size', $imgsize)
                ->put('img_ext', $imgext)
                ->put('img_name', $img_name)
                ->put('full_image', $full_image)
                ->put('image_status', $image_status)
                ->put('resFileData', $resFileData);
            //dd($data);
            
            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }*/

    public function viewEpisode(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'type' => 'required',
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
            'type.required' => '6000:1 - type missing for episode',
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Episode Info
            $episode = Episode::where('id', $request->episode_id)->first(['id', 'show_id', 'title', 'image', 'description', 'mp3', 'duration', 'date_added', 'status']);            
            
            if (!$episode) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }
            
            if (is_null($episode->episode_extra_info)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }
            
            if ($episode->episode_extra_info->user_id != $user->id) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not modified this episode, because this episode is not belong to you."
                ]);
            }
            
            if ($request->type == "series") {
                if ($episode->episode_extra_info->content_type == 2) {
                    return response()->business_api([
                        'status' => false,
                        'message' => "You can not modified this episode, because this episode is not belong to series."
                    ]);
                }
            }
            

            //Getting categories
            $categories = $episode->categories->pluck('title', 'id')->all();
            $categoryData = [];
            foreach ($categories as $key => $value) {
                $categoryData[] = [
                    'id' => $key,
                    'name' => $value
                ];
            }

            //Getting authors
            $authors = $episode->authors()->get(['id', 'first_name', 'email']);
            $authorData = [];
            foreach ($authors as $key => $value) {
                $authorData[] = [
                    'id' => $value->id,
                    'name' => $value->first_name . " - " . $value->email
                ];
            }

            //dd($authorData);

            //Getting narrators
            $narrators = $episode->narrators()->get(['id', 'first_name', 'email']);
            $narratorData = [];
            foreach ($narrators as $key => $value) {
                $narratorData[] = [
                    'id' => $value->id,
                    'name' => $value->first_name . " - " . $value->email
                ];
            }

            //Getting producers
            $producers = $episode->producers()->get(['id', 'first_name', 'email']);
            $producerData = [];
            foreach ($producers as $key => $value) {
                $producerData[] = [
                    'id' => $value->id,
                    'name' => $value->first_name . " - " . $value->email
                ];
            }

            //Getting tags
            $tags = $episode->tags->pluck('title', 'id')->all();
            $tagData = [];
            foreach ($tags as $key => $value) {
                $tagData[] = [
                    'id' => $key,
                    'name' => trim($value, '"')
                ];
            }

            $full_image = !is_null($episode->image) ? $episode->episode_extra_info->getEpisodeImage(200) : '';
            $image_status = !is_null($episode->image) ? 1 : 0;

            $imgsize = '';
            $imgext = '';
            $img_name = '';
            if ($episode->image) {
                if (!is_null(env('DBLOCAL'))) {
                    if (file_exists(public_path('uploads/episodes/' . $episode->image))) {
                        $imgsize = filesize(public_path('uploads/episodes/' . $episode->image));
                        $path_parts = pathinfo(public_path('uploads/episodes/' . $episode->image));
                        $imgext = 'image/' . $path_parts['extension'];
                        $img_name = $path_parts['filename'];
                    } else {
                        $image_status = 0;
                    }
                } else {
                    if (Storage::disk('s3')->exists('episodes/' . $episode->image)) {
                        $imgsize = Storage::disk('s3')->size('episodes/' . $episode->image);
                        $path_parts = pathinfo($episode->episode_extra_info->getEpisodeImage());
                        $imgext = 'image/' . $path_parts['extension'];
                        $img_name = $path_parts['filename'];
                    } else {
                        $image_status = 0;
                    }
                }
            }

            // Shots image
            $full_image_shot = !is_null($episode->episode_extra_info->shots) ? $episode->episode_extra_info->getEpisodeShotImage(200) : '';
            $image_shot_status = !is_null($episode->episode_extra_info->shots) ? 1 : 0;

            $img_shot_size = '';
            $img_shot_ext = '';
            $img_shot_name = '';
            if ($episode->episode_extra_info->shots) {
                if (!is_null(env('DBLOCAL'))) {
                    if (file_exists(public_path('uploads/episodes/shots/' . $episode->episode_extra_info->shots))) {
                        $img_shot_size = filesize(public_path('uploads/episodes/shots/' . $episode->episode_extra_info->shots));
                        $path_parts = pathinfo(public_path('uploads/episodes/shots/' . $episode->episode_extra_info->shots));
                        $img_shot_ext = 'image/' . $path_parts['extension'];
                        $img_shot_name = $path_parts['filename'];
                    } else {
                        $image_shot_status = 0;
                    }
                } else {
                    if (Storage::disk('s3')->exists('episodes/shots/' . $episode->episode_extra_info->shots)) {
                        $img_shot_size = Storage::disk('s3')->size('episodes/shots/' . $episode->episode_extra_info->shots);
                        $path_parts = pathinfo($episode->episode_extra_info->getEpisodeShotImage());
                        $img_shot_ext = 'image/' . $path_parts['extension'];
                        $img_shot_name = $path_parts['filename'];
                    } else {
                        $image_shot_status = 0;
                    }
                }
            }

            //Fetching Country Data
            $countryData = [];
            if (!is_null($episode->episode_extra_info->country)) {
                $countrySql = Ip2LocationCountry::where('country_alpha3_code', $episode->episode_extra_info->country)
                    ->first(['country_name', 'country_alpha3_code']);
                if (!empty($countrySql)) {
                    $countryData[] = [
                        'id' => $countrySql->country_alpha3_code,
                        'name' => $countrySql->country_name
                    ];
                }
            }

            //Fetching Language Data
            $languageData = [];
            if (!is_null($episode->episode_extra_info->language)) {
                $languageSql = ShowLanguage::where('short_code', $episode->episode_extra_info->language)
                    ->first(['short_code', 'title']);
                if (!empty($languageSql)) {
                    $languageData[] = [
                        'id' => $languageSql->short_code,
                        'name' => $languageSql->title
                    ];
                }
            }

            //Getting all uploaded resources files
            $resourcesFiles = EpisodeResource::where('episode_id', $episode->id)->first([
                'res_title1', 'res_file1',
                'res_title2', 'res_file2',
                'res_title3', 'res_file3',
                'res_title4', 'res_file4'
            ]);
            $resFileData = [];
            if (!empty($resourcesFiles->res_file1)) {
                $resFileData[] = [
                    "id" => 1,
                    "title" => !empty($resourcesFiles->res_title1) ? $resourcesFiles->res_title1 : '',
                    "file" => $resourcesFiles->res_file1
                ];
            }
            if (!empty($resourcesFiles->res_file2)) {
                $resFileData[] = [
                    "id" => 2,
                    "title" => !empty($resourcesFiles->res_title2) ? $resourcesFiles->res_title2 : '',
                    "file" => $resourcesFiles->res_file2
                ];
            }
            if (!empty($resourcesFiles->res_file3)) {
                $resFileData[] = [
                    "id" => 3,
                    "title" => !empty($resourcesFiles->res_title3) ? $resourcesFiles->res_title3 : '',
                    "file" => $resourcesFiles->res_file3
                ];
            }
            if (!empty($resourcesFiles->res_file4)) {
                $resFileData[] = [
                    "id" => 4,
                    "title" => !empty($resourcesFiles->res_title4) ? $resourcesFiles->res_title4 : '',
                    "file" => $resourcesFiles->res_file4
                ];
            }

            $mp3 = is_null($episode->mp3) ? "" : $episode->mp3;
            $res_description = is_null($episode->episode_extra_info->res_description) ? "" : $episode->episode_extra_info->res_description;
            //dd($episode->episode_extra_info->file_original_name);
            $org_mp3 = ! is_null($episode->episode_extra_info->file_original_name) ? $episode->episode_extra_info->file_original_name : $mp3;

            $duration = (int) ceil($episode->duration / 60);
            $data = collect($episode)
                ->put('mp3', $mp3)
                ->put('file_original_name', $org_mp3)
                ->put('intro', $episode->episode_extra_info->intro)
                ->put('res_description', $res_description)
                //->put('resourcesFiles', $resourcesFiles)
                ->put('language', $languageData)
                ->put('duration', $duration)
                ->put('rating', $episode->episode_extra_info->rating)
                ->put('categories', $categoryData)
                ->put('country', $countryData)
                ->put('tags', $tagData)
                ->put("authors", $authorData)
                ->put('narrators', $narratorData)
                ->put('producers', $producerData)
                ->put('img_size', $imgsize)
                ->put('img_ext', $imgext)
                ->put('img_name', $img_name)
                ->put('full_image', $full_image)
                ->put('image_status', $image_status)

                ->put('shots', $episode->episode_extra_info->shots)
                ->put('img_shot_size', $img_shot_size)
                ->put('img_shot_ext', $img_shot_ext)
                ->put('img_shot_name', $img_shot_name)
                ->put('full_image_shot', $full_image_shot)
                ->put('image_shot_status', $image_shot_status)
                
                ->put('resFileData', $resFileData);
            //dd($data);
            
            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * Published Episode from temp
     */
    public function publishedEpisodefromTemp(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'audio_upload_id' => 'required'
        ], [
            'audio_upload_id.required' => '6000:1 - id missing for episode',
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //check if existing user can update episode info or not
        $checkEpisodeUpdate = EpisodeAudioFileUpload::where('id', $request->audio_upload_id)
            ->where('user_id', $user->id)->first();
        if (empty($checkEpisodeUpdate)) {
            return response()->business_api([
                'status' => false,
                'message' => "You can not proceed..."
            ]);
        }

        if ($checkEpisodeUpdate->status == "Incomplete") {
            return response()->business_api([
                'status' => false,
                'message' => "Please complete episode info."
            ]);
        }
        if ($checkEpisodeUpdate->status == "Published") {
            return response()->business_api([
                'status' => false,
                'message' => "This episode is already published."
            ]);
        }

        $temp_data = EpisodeTemp::where('audio_upload_id', $checkEpisodeUpdate->id)->first();

        if ($temp_data->status == "PushtoEpisode") {
            return response()->business_api([
                'status' => false,
                'message' => "This episode is already published."
            ]);
        }

        $type = "";
        if (is_null($checkEpisodeUpdate->show_id)) {
            $type = "standalone";
        } else {
            $type = "series";
        }

        if ($type == "") {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }

        //Published Episode
        $this->publishedEpisode($temp_data, $user, $type);

        return response()->business_api([
            'status' => true,
            'message' => "Episode published successfully."
        ]);
    }

    /**
     * Published Episode
     * 
     */
    public function publishedEpisodeData(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Episode Info
            $episode = Episode::find($request->episode_id);

            if (!$episode) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if (is_null($episode->episode_extra_info)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->user_id != $user->id) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not modified this episode."
                ]);
            }

            $episode_data = [];
            //$episode_data[] = ['status' => "publish"];
            if ($episode->status == "Draft") {
                if (is_null(env('DBLOCAL'))) {
                    //Update Episode Details table
                    $episode_details = EpisodeDetail::where('episode_id', $episode->id)->first();
                    if (!empty($episode_details)) {
                        $episode_details->status = 'Published';
                        $episode_details->save();

                        //Update Episode status
                        $episode->status = 'Published';
                        $episode->save();

                        //This episode added to elastic search
                        //$episode->searchable();
                        $episode->unsearchable();

                        $episode->show()->unsearchable(); //Just Remove This Line

                        return response()->business_api([
                            'status' => true,
                            'message' => "Episode published successfully.",
                            'data' => $episode_data
                        ]);
                    } else {
                        return response()->business_api([
                            'status' => false,
                            'message' => "No Episode Found."
                        ]);
                    }
                }
                return response()->business_api([
                    'status' => true,
                    'message' => "Episode published successfully.",
                    'data' => $episode_data
                ]);
            }
            return response()->business_api([
                'status' => true,
                'message' => "Episode published successfully.",
                'data' => $episode_data
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * Unpublished Episode
     * 
     */
    public function unpublishedEpisodeData(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => '6000:1 - id missing for episode',
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Getting Episode Info
            $episode = Episode::find($request->episode_id);
            //dd($episode->episode_extra_info, $user->id);

            if (!$episode) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if (is_null($episode->episode_extra_info)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }

            if ($episode->episode_extra_info->user_id != $user->id) {
                return response()->business_api([
                    'status' => false,
                    'message' => "You can not modified this episode."
                ]);
            }

            //Update Episode Details table
            $episode_data = [];
            //$episode_data[] = ['status' => "draft"];
            if ($episode->status == "Published") {
                if (is_null(env('DBLOCAL'))) {
                    $episode_details = EpisodeDetail::where('episode_id', $episode->id)->first();
                    if (!empty($episode_details)) {
                        $episode_details->status = 'Draft';
                        $episode_details->save();

                        //Update Episode status
                        $episode->status = 'Draft';
                        $episode->save();

                        //This episode remove from elastic search
                        $episode->unsearchable();

                        $episode->show()->unsearchable(); //Just Remove This Line

                        return response()->business_api([
                            'status' => true,
                            'message' => "Episode published successfully.",
                            'data' => $episode_data
                        ]);
                    } else {
                        return response()->business_api([
                            'status' => false,
                            'message' => "No Episode Found."
                        ]);
                    }
                }
                return response()->business_api([
                    'status' => true,
                    'message' => "Episode published successfully.",
                    'data' => $episode_data
                ]);
            }
            return response()->business_api([
                'status' => true,
                'message' => "Episode published successfully.",
                'data' => $episode_data
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * Thisfunction is use for calculate pricing with
     * specific country and currency
     */
    public function priceCalculation(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'country_id' => 'required',
            'currency_id' => 'required',
            'pricing' => 'required'
        ], [
            'country_id.required' => '6000:1 - country id missing for item',
            'currency_id.required' => '6000:1 - currency id missing for item',
            'pricing.required' => '6000:1 - price missing for item'
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Get markup price and commission price from marketplace country table
        $getCountryPrice = MarketplaceCountry::where('id', $request->country_id)
            ->where('currency_id', $request->currency_id)
            ->first(['id', 'markup_price', 'commission_price', 'tax_price']);

        if (is_null($getCountryPrice)) {
            return response()->business_api([
                'status' => false,
                'message' => "We don't found any country."
            ]);
        }

        //$markup_price = is_null($getCountryPrice->markup_price) ? 0 : ($request->pricing * $getCountryPrice->markup_price)/100;
        //$commission_price = is_null($getCountryPrice->commission_price) ? 0 : ($request->pricing * $getCountryPrice->commission_price)/100;
        //$total_price = round(($request->pricing + $markup_price + $commission_price), 2);        

        $markup_price_percent = is_null($getCountryPrice->markup_price) ? 0 : $getCountryPrice->markup_price;
        $commission_price_percent = is_null($getCountryPrice->commission_price) ? 0 : $getCountryPrice->commission_price;
        $tax_price_percent = is_null($getCountryPrice->tax_price) ? 0 : $getCountryPrice->tax_price;

        return response()->business_api([
            'status' => true,
            'message' => "",
            'data' => $this->getPriceCalculation($request->pricing, $markup_price_percent, $commission_price_percent, $tax_price_percent)
        ]);
    }

    /**
     * 
     * This function is calculate price
     */
    protected function getPriceCalculation($sp, $mp, $cp, $tax)
    {

        $storiyoh_commission = $mp == 0 ? 0 : (($sp * $mp) / 100);
        $appstore_commission = $cp == 0 ? 0 : (($sp * $cp) / 100);

        $tax_on_vendor_commission = (($storiyoh_commission * $tax) / 100);

        //Gross amount to seller (including Tax)
        $gross_amount_include_tax = ($sp - ($storiyoh_commission + $tax_on_vendor_commission + $appstore_commission));

        //Net without Tax to Seller
        $proceed_seller_net_amount = (100 * $gross_amount_include_tax) / (100 + $tax);

        //Net Price to Buyer (without tax)
        $net_amount_without_tax = $storiyoh_commission + $appstore_commission + $proceed_seller_net_amount;

        //Tax Chargeable on Invoice
        $tax_on_invoice = ($net_amount_without_tax * $tax) / 100;

        //Gross Invoice Amount
        $gross_invoice_amount = ($net_amount_without_tax + $tax_on_invoice);

        $gross_amount_include_tax = round($gross_amount_include_tax, 2);
        $proceed_seller_net_amount = round($proceed_seller_net_amount, 2);

        $net_amount_without_tax = round($net_amount_without_tax, 2);
        $tax_on_invoice = round($tax_on_invoice, 2);
        $gross_invoice_amount = round($gross_invoice_amount, 2);

        return [
            "storiyoh_commission" => $storiyoh_commission,
            "appstore_commission" => $appstore_commission,
            'tax_on_invoice' => $tax_on_invoice,
            'gross_invoice_amount' => $gross_invoice_amount,
            'net_amount_without_tax' => $net_amount_without_tax,
            "gross_amount_include_tax" => $gross_amount_include_tax,
            "proceed_seller_net_amount" => $proceed_seller_net_amount,
        ];
    }

    /**
     * This function is use for getting standalone episodes
     * 
     */
    public function standaloneEpisodes(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        try {
            //Fetching Premium Standalone episode listing
            $qry = EpisodeDetail::where('content_type', 2)->where('user_id', $user->id);
            $total_episodes = $qry->count();

            $incomplete_data_qry = EpisodeAudioFileUpload::where('user_id', $user->id)
                ->whereNull('show_id')->whereNull('episode_id')->where('status', '!=', 'Published');

            $incomplete_count = $incomplete_data_qry->count();

            $total_episodes_with_incomplete = $total_episodes;

            $data = [];
            if ($request->status == 'incomplete') {
                $all_episodes = $incomplete_data_qry->orderBy('created_at', 'DESC')
                    ->paginate(8, ['id', 'episode_id']);

                foreach ($all_episodes as $key => $item) {
                    $data[] = [
                        'id' => $item->id,
                        'title' => "Episode " . ($key + 1),
                        'description' => "",
                        'image' => asset('uploads/default/premium_show.jpg'),
                        'status' => 'incompete'
                    ];
                }
            } else {
                if (!empty($request->status)) {
                    if ($request->status != 'all') {
                        $qry->where('status', ucfirst($request->status));
                    }
                }

                $all_episodes = $qry->orderBy('created_at', 'DESC')
                    ->paginate(8, ['id', 'title', 'episode_id', 'intro', 'status', 'created_at']);

                //dd($all_episodes->toArray());

                foreach ($all_episodes as $item) {
                    $status = "";
                    if (strtolower($item->status) == "published") {
                        $status = "publish";
                    } else {
                        $status = "draft";
                    }

                    $data[] = [
                        'id' => $item->episode->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($item->episode->title))),
                        'description' => $item->intro,
                        'image' => !empty($item->episode->image) ? $item->getImage(200) : asset('uploads/default/premium_show.jpg'),
                        'status' => $status
                    ];
                }
            }


            $message = "";
            if (count($data) == 0) {
                $message = "No Episode found.";
            }
            return response()->business_api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_episodes->total(),
                    'per_page' => $all_episodes->perPage(),
                    'pages' => ceil($all_episodes->total() / $all_episodes->perPage()),
                    'items' => $data,
                    'total_episodes' => $total_episodes_with_incomplete,
                    'incomplete_count' => $incomplete_count
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->business_api([
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ]);
        }
    }

    /**
     * This function is use for
     * getting random number
     */
    protected function getRandomNumber($n)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    /**
     * Upload Cover Image for Series and Episode
     */
    /*public function fileImageUpload(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'file' => 'required',
            'type' => 'required'
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        if ($request->hasFile('file')) {
            $allowedfileExtension = ['jpg', 'jpeg', 'gif', 'png'];
            $extension = $request->file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            if ($check) {
                $filename = "";
                $upload_file_name = "";
                $fullname = "";
                if ($request->type == "series_cover") {
                    $filename = @date('YmdHis') . str_random(6) . '.' . $extension;
                    $upload_file_name = "shows/" . $filename;
                    $directory = public_path('uploads/shows/');
                } else if ($request->type == "episode_cover") {
                    $filename = @date('YmdHis') . str_random(6) . '.' . $extension;
                    $upload_file_name = "episodes/" . $filename;
                    $directory = public_path('uploads/episodes/');
                }

                if ($upload_file_name != "") {
                    //Upload file on S3 Bucket
                    if (!is_null(env('DBLOCAL'))) {
                        $request->file->move($directory, $filename);
                        $fullname = config('config.storiyoh_backend_url') . '/uploads/' . $upload_file_name;
                    } else {
                        Storage::disk('s3')->put($upload_file_name, file_get_contents($request->file));
                        if ($request->type == "series_cover") {
                            $fullname = config('config.s3_url') . '/shows/' . $filename;
                        } else if ($request->type == "episode_cover") {
                            $fullname = config('config.s3_url') . '/episodes/' . $filename;
                        }
                    }
                }
                return [
                    "status" => true,
                    "filename" => $filename,
                    "fullImgPath" => $fullname,
                ];
            } else {
                return [
                    'status' => false,
                    'code' => 200,
                    'message' => "Only jpg, jpeg, gif, png file allowed.."
                ];
            }
        }
    }*/

    public function fileImageUpload(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'file' => 'required',
            'type' => 'required'
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        if ($request->hasFile('file')) {
            $allowedfileExtension = ['jpg', 'jpeg', 'gif', 'png'];
            $extension = $request->file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            if ($check) {
                $filename = "";
                $upload_file_name = "";
                $fullname = "";
                if ($request->type == "series_cover") {
                    $filename = @date('YmdHis') . str_random(6) . '.' . $extension;
                    $upload_file_name = "shows/" . $filename;
                    $directory = public_path('uploads/shows/');
                } else if ($request->type == "episode_cover") {
                    $filename = @date('YmdHis') . str_random(6) . '.' . $extension;
                    $upload_file_name = "episodes/" . $filename;
                    $directory = public_path('uploads/episodes/');
                } else if ($request->type == "episode_shot") {
                    $filename = @date('YmdHis') . str_random(6) . '.' . $extension;
                    $upload_file_name = "episodes/shots/" . $filename;
                    $directory = public_path('uploads/episodes/shots/');
                }

                if ($upload_file_name != "") {
                    //Upload file on S3 Bucket
                    if (!is_null(env('DBLOCAL'))) {
                        $request->file->move($directory, $filename);
                        $fullname = config('config.storiyoh_backend_url') . '/uploads/' . $upload_file_name;
                    } else {
                        Storage::disk('s3')->put($upload_file_name, file_get_contents($request->file));
                        if ($request->type == "series_cover") {
                            $fullname = config('config.s3_url') . '/shows/' . $filename;
                        } else if ($request->type == "episode_cover"
                        ) {
                            $fullname = config('config.s3_url') . '/episodes/' . $filename;
                        } else if ($request->type == "episode_shot") {
                            $fullname = config('config.s3_url') . '/episodes/shots/' . $filename;
                        }
                    }
                }
                return [
                    "status" => true,
                    "filename" => $filename,
                    "fullImgPath" => $fullname,
                ];
            } else {
                return [
                    'status' => false,
                    'code' => 200,
                    'message' => "Only jpg, jpeg, gif, png file allowed.."
                ];
            }
        }
    }

    /**
     * Remove Image for Series and Episode
     */
    /*public function removeImageFile(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'filepath' => 'required',
            'type' => 'required'
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        if ($request->filepath) {
            $upload_file_name = "";
            $filename = "";
            if ($request->type == "series_cover") {
                $upload_file_name = "shows/";
                $filename = $request->filepath;
                $directory = public_path('uploads/shows/' . $filename);
            } else if ($request->type == "episode_cover") {
                $upload_file_name = "episodes/";
                $filename = $request->filepath;
                $directory = public_path('uploads/episodes/' . $filename);
            }

            if ($upload_file_name != "") {
                if (!is_null(env('DBLOCAL'))) {
                    unlink($directory);
                } else {
                    if ($filename != "") {
                        if (Storage::disk('s3')->exists($upload_file_name . $filename)) {
                            Storage::disk('s3')->delete($upload_file_name . $filename);
                        }
                    }
                }
            }
        }
        return [
            'status' => true,
            "filename" => '',
            'message' => 'Image deleted successfully.'
        ];
    }*/

    public function removeImageFile(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'filepath' => 'required',
            'type' => 'required'
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        if ($request->filepath) {
            $upload_file_name = "";
            $filename = "";
            if ($request->type == "series_cover") {
                $upload_file_name = "shows/";
                $filename = $request->filepath;
                $directory = public_path('uploads/shows/' . $filename);
            } else if ($request->type == "episode_cover") {
                $upload_file_name = "episodes/";
                $filename = $request->filepath;
                $directory = public_path('uploads/episodes/' . $filename);
            } else if ($request->type == "episode_shot") {
                $upload_file_name = "episodes/shots/";
                $filename = $request->filepath;
                $directory = public_path('uploads/episodes/shots/' . $filename);
            }

            if ($upload_file_name != "") {
                if (!is_null(env('DBLOCAL'))) {
                    unlink($directory);
                } else {
                    if ($filename != "") {
                        if (Storage::disk('s3')->exists($upload_file_name . $filename)) {
                            Storage::disk('s3')->delete($upload_file_name . $filename);
                        }
                    }
                }
            }
        }

        return response()->business_api([
            'status' => true,
            "filename" => '',
            'message' => 'Image deleted successfully.'
        ]);
    }

    /**
     * Remove Image for Series and Episode
     */
    /*public function removeImageFileonEdit(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'filepath' => 'required',
            'type' => 'required',
            'content_id' => 'required',
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        if ($request->filepath) {
            $upload_file_name = "";
            $filename = "";
            if ($request->type == "series_cover") {
                $upload_file_name = "shows/";
                $filename = $request->filepath;
                $directory = public_path('uploads/shows/' . $filename);
                $contentData = Show::where('user_id', $user->id)->where("id", $request->content_id)->first();
                if (!$contentData) {
                    return [
                        'status' => false,
                        'code' => 200,
                        'message' => "No Series Found."
                    ];
                }
            } else if ($request->type == "episode_cover") {
                $upload_file_name = "episodes/";
                $filename = $request->filepath;
                $directory = public_path('uploads/episodes/' . $filename);
                $episodeDetails = EpisodeDetail::where('episode_id', $request->content_id)
                    ->where('user_id', $user->id)->first();
                if (!$episodeDetails) {
                    return [
                        'status' => false,
                        'code' => 200,
                        'message' => "No Episode Found."
                    ];
                }

                $contentData = Episode::find($request->content_id);
            }

            if ($upload_file_name != "") {
                if (!is_null(env('DBLOCAL'))) {
                    unlink($directory);
                } else {
                    if ($filename != "") {
                        if (Storage::disk('s3')->exists($upload_file_name . $filename)) {
                            Storage::disk('s3')->delete($upload_file_name . $filename);
                        }
                    }
                }
                //Update Series
                if (is_null(env('DBLOCAL'))) {
                    $contentData->fill(['image' => null])->save();
                    if ($contentData->status == 'Published') {
                        //$episode->searchable();
                        $contentData->unsearchable();
                    } else {
                        $contentData->unsearchable();
                    }
                }
            }
        }
        return [
            'status' => true,
            "filename" => '',
            'message' => 'Image deleted successfully.'
        ];
    }*/

    public function removeImageFileonEdit(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'filepath' => 'required',
            'type' => 'required',
            'content_id' => 'required',
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        if ($request->filepath) {
            $upload_file_name = "";
            $filename = "";
            if ($request->type == "series_cover") {
                $upload_file_name = "shows/";
                $filename = $request->filepath;
                $directory = public_path('uploads/shows/' . $filename);
                $contentData = Show::where('user_id', $user->id)->where("id", $request->content_id)->first();
                if (!$contentData) {
                    return [
                        'status' => false,
                        'code' => 200,
                        'message' => "No Series Found."
                    ];
                }
            } else if ($request->type == "episode_cover") {
                $upload_file_name = "episodes/";
                $filename = $request->filepath;
                $directory = public_path('uploads/episodes/' . $filename);
                $episodeDetails = EpisodeDetail::where('episode_id', $request->content_id)
                    ->where('user_id', $user->id)->first();
                if (!$episodeDetails) {
                    return [
                        'status' => false,
                        'code' => 200,
                        'message' => "No Episode Found."
                    ];
                }

                $contentData = Episode::find($request->content_id);
            } else if ($request->type == "episode_shot") {
                $upload_file_name = "episodes/shots/";
                $filename = $request->filepath;
                $directory = public_path('uploads/episodes/shots/' . $filename);
                $episodeDetails = EpisodeDetail::where('episode_id', $request->content_id)
                ->where('user_id', $user->id)->first();
                if (!$episodeDetails) {
                    return [
                        'status' => false,
                        'code' => 200,
                        'message' => "No Episode Found."
                    ];
                }

                $contentData = Episode::find($request->content_id);
            }

            if ($upload_file_name != ""
            ) {
                if (!is_null(env('DBLOCAL'))) {
                    unlink($directory);
                } else {
                    if ($filename != "") {
                        if (Storage::disk('s3')->exists($upload_file_name . $filename)) {
                            Storage::disk('s3')->delete($upload_file_name . $filename);
                        }
                    }
                }
                //Update Series
                if (is_null(env('DBLOCAL'))) {
                    if ($request->type == "episode_shot") { //episode details table
                        $episodeDetails->fill(['shot' => null])->save();
                    } else { // Show and episode table
                        $contentData->fill(['image' => null])->save();
                    }
                    if ($contentData->status == 'Published') {
                        //$episode->searchable();
                        $contentData->unsearchable();
                    } else {
                        $contentData->unsearchable();
                    }
                }
            }
        }

        return response()->business_api([
            "status" => true,
            "filename" => '',
            'message' => 'Image deleted successfully.'
        ]);
    }

    /**
     * Remove Audio File and episode data
     */
    public function removeAudioFile(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        $audioFile = EpisodeAudioFileUpload::where('id', $request->id)->where('user_id', $user->id);
        if ($audioFile->count() > 0) {
            //Delete data from table and remove file from S3 server
            $data[] = [];
            $filename = $audioFile->first(['mp3']);
            if (!is_null(env('DBLOCAL'))) {
                $filename = public_path('uploads/marketplace/' . $filename->mp3);
                if (file_exists($filename)) {
                    unlink($filename);
                }
            } else {
                if (Storage::disk('s3B')->exists("marketplace/" . $filename->mp3)) {
                    Storage::disk('s3B')->delete("marketplace/" . $filename->mp3);
                }
            }
            //Delete data from table and remove file from S3 server
            $audioFile->delete();

            return response()->business_api([
                'status' => true,
                'message' => "Episode deleted successfully.",
            ]);
        } else {
            return response()->business_api([
                'status' => false,
                'message' => "No Episode Found."
            ]);
        }
    }

    /**
     * Remove Audio File and update episode data
     */
    public function removeEpisodeAudioFile(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        //Getting Episode Info
        $episode = EpisodeDetail::where('episode_id', $request->episode_id)->where('user_id', $user->id)->first();
        if (!$episode) {
            return response()->business_api([
                'status' => false,
                'message' => "No Episode Found."
            ]);
        }

        $audioFile = EpisodeAudioFileUpload::where('episode_id', $episode->episode_id)->where('user_id', $user->id);
        if ($audioFile->count() > 0) {
            //Delete data from table and remove file from S3 server
            $filename = $audioFile->first(['mp3']);
            if (!is_null(env('DBLOCAL'))) {
                $filename = public_path('uploads/marketplace/' . $filename->mp3);
                if (file_exists($filename)) {
                    unlink($filename);
                }
            } else {
                if (Storage::disk('s3B')->exists("marketplace/" . $filename->mp3)) {
                    Storage::disk('s3B')->delete("marketplace/" . $filename->mp3);
                }
            }

            //Delete data from table and remove file from S3 server
            $audioFile->delete();

            //Update episode mp3 file
            if (!is_null($episode->episode->mp3)) {
                if (is_null(env('DBLOCAL'))) {
                    $episode->episode()->update(['mp3' => null]);
                    
                    $episode->update(['file_original_name' => null]);

                    if ($episode->episode->status == 'Published') {
                        //$episode->episode->searchable();
                        $episode->episode->unsearchable();
                    } else {
                        $episode->episode->unsearchable();
                    }
                }
            }

            return response()->business_api([
                'status' => true,
                'message' => "Audio deleted successfully.",
            ]);
        } else {
            return response()->business_api([
                'status' => false,
                'message' => "No Episode Found."
            ]);
        }
    }

    /**
     * Upload audio files to episode table
     */
    public function file_upload_on_edit(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'file' => 'required',
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        $episode = EpisodeDetail::where('episode_id', $request->episode_id)->where('user_id', $user->id)->first();
        if (!$episode) {
            return [
                'status' => false,
                'code' => 200,
                'message' => "No Episode Found."
            ];
        }

        try {
            if ($request->hasFile('file')) {
                $allowedfileExtension = ['mp3', 'mp4'];
                $extension = $request->file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                $attributes = [];
                if ($check) {
                    $filename = @date('YmdHis') . str_random(6) . '.' . $extension;
                    $upload_file_name = "marketplace/" . $filename;

                    $file_title = null;
                    $duration = null;
                    try {
                        $file_title = @LaravelMP3::getTitle($request->file);
                        $file_title = is_null($file_title) ? null : $file_title[0];
                        $original_name = $request->file->getClientOriginalName();

                        $duration = @LaravelMP3::getDuration($request->file);

                        if (!is_null($duration)) {
                            $durationArray = explode(':', (string) $duration);
                            if (count($durationArray) > 1) {
                                sscanf($duration, '%d:%d:%d', $hours, $minutes, $seconds);
                                $duration = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
                            }
                        }
                    } catch (\Exception $ex) { }

                    //Upload file on S3 Bucket
                    if (!is_null(env('DBLOCAL'))) {
                        $directory = public_path('uploads/marketplace/');
                        $request->file->move($directory, $filename);
                    } else {
                        Storage::disk('s3B')->put($upload_file_name, file_get_contents($request->file));
                    }

                    //Insert audio data in table
                    $attributes[] = [
                        "user_id" => $user->id,
                        "episode_id" => $episode->episode_id,
                        "show_id" => $episode->episode->show_id,
                        "title" => $file_title,
                        "duration" => $duration,
                        "mp3" => $filename,
                        "file_original_name" => $original_name,
                        "status" => "Published",
                        "created_at" => now(),
                        "updated_at" => now(),
                    ];
                    EpisodeAudioFileUpload::insert($attributes);

                    //Update episode mp3 file
                    if (! empty($filename)) {
                        if (is_null(env('DBLOCAL'))) {
                            //Update Episode table
                            $episode->episode()->update([
                                "mp3" => $filename,
                                "duration" => $duration
                            ]);
                            
                            //Update Episode Details table
                            $episode->update(["file_original_name" => $original_name]);

                            if ($episode->episode->status == 'Published') {
                                //$episode->episode->searchable();
                                $episode->episode->unsearchable();
                            } else {
                                $episode->episode->unsearchable();
                            }
                        }
                    }

                    return [
                        'status' => true,
                        'code' => 200,
                        'message' => "Files uploaded successfully.",
                        'filename' => $filename,
                        'file_original_name' => $original_name
                    ];
                } else {
                    return [
                        'status' => false,
                        'code' => 200,
                        'message' => "Only Mp3 file allowed."
                    ];
                }
            } else {
                return [
                    'status' => false,
                    'code' => 200,
                    'message' => 'Only Mp3 file allowed.'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => false,
                'code' => 200,
                'message' => "Something is went wrong, Please try again after some time."
            ];
        }
    }

    /***
     * Create temporary URL for episode
     * 
     */
    public function single_signedin_url(Request $request)
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required',
        ], []);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }
        
        $episodeInfo = null;
        $filename = "";
        if ($request->type == "episode") {
            //Select Episode Info
            $episodeInfo = EpisodeDetail::where('user_id', $user->id)->where('episode_id', $request->content_id)->first();
            if (is_null($episodeInfo)) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Episode Found."
                ]);
            }
            $title = trim(str_replace("\n", '', html_entity_decode($episodeInfo->episode->title)));
            $show_title = $episodeInfo->episode->show->title;
            $image = !empty($episodeInfo->episode->image) ? $episodeInfo->episode->getImage(200) : asset('uploads/default/premium_show.jpg');
            $filename = $episodeInfo->episode->mp3;
        } else if ($request->type == "incomplete") {
            $episodeInfo = EpisodeAudioFileUpload::where('id', $request->content_id)->where('user_id', $user->id)->first(['mp3', 'title']);
            if (!$episodeInfo) {
                return response()->business_api([
                    'status' => false,
                    'message' => "No Audio File Found."
                ]);
            }
            $filename = $episodeInfo->mp3;
            $title = is_null($episodeInfo->title) ? "Storiyoh" : $episodeInfo->title;
            $show_title = is_null($episodeInfo->title) ? "Storiyoh" : $episodeInfo->title;
            $image = asset('uploads/default/premium_show.jpg');
        }

        if (!is_null($episodeInfo)) {
            $response = "";
            if (!is_null(env('DBLOCAL'))) {
                $response = config('config.storiyoh_backend_url') . '/uploads/marketplace/' . $filename;
            } else {
                if (! empty($filename)) {
                    $response = $this->getSignedUrl(config('config.s3_marketplace_folder') . $filename);
                }
            }

            $data = [
                'title' => $title,
                'show_title' => $show_title,
                'image' => $image,
                'file_url' => $response
            ];

            return response()->business_api([
                'status' => true,
                'message' => "",
                'data' => $data
            ]);
        } else {
            return response()->business_api([
                'status' => false,
                'message' => "We don't found any audio file."
            ]);
        }
    }

    /**
     * This function is use for create signed url
     */
    protected function getSignedUrl($audio_file_name)
    {
        //Instantiate an Amazon S3 client.
        $s3Client = new S3Client([
            'version' => 'latest',
            'region'  => 'ap-south-1'
        ]);

        //Creating a presigned URL
        $cmd = $s3Client->getCommand('GetObject', [
            'Bucket' => config('config.s3_bucket_name'),
            'Key' => $audio_file_name,
            //'ResponseContentDisposition' => 'attachment'
        ]);

        $request = $s3Client->createPresignedRequest($cmd, '+5 minutes');

        // Get the actual presigned-url
        return (string) $request->getUri();
    }

    /**
     * Upload resaurces file to episode table (Adding)
     */
    public function uploadEpisodeResourceData(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'type' => 'required',
            'file' => 'required',
            'res_file' => 'required'
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        if ($request->type == "Add") {
            $episode = EpisodeAudioFileUpload::where('id', $request->episode_id)->where('user_id', $user->id)->first();
            if (!$episode) {
                return [
                    'status' => false,
                    'code' => 200,
                    'message' => "No Data Found."
                ];
            }
        } else if ($request->type == "Edit") {
            $episode = EpisodeDetail::where('episode_id', $request->episode_id)->where('user_id', $user->id)->first();
            if (!$episode) {
                return [
                    'status' => false,
                    'code' => 200,
                    'message' => "No Data Found."
                ];
            }
        }

        try {
            if ($request->hasFile('file')) {
                $allowedfileExtension = ['pdf', 'ppt', 'docx', 'doc', 'xlsx', 'xls'];
                $extension = $request->file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    $filename = "";
                    $upload_file_name = "";
                    $fullname = "";

                    $original_filename = pathinfo($request->file->getClientOriginalName(), PATHINFO_FILENAME);
                    $filename = $original_filename . "_" . @date('YmdHis') . str_random(6) . '.' . $extension;
                    $upload_file_name = "marketplace_resources/" . $filename;
                    $directory = public_path('uploads/marketplace_resources/');

                    if ($upload_file_name != "") {
                        //Upload file on S3 Bucket
                        if (!is_null(env('DBLOCAL'))) {
                            $request->file->move($directory, $filename);
                            $fullname = config('config.storiyoh_backend_url') . '/uploads/' . $upload_file_name;
                        } else {
                            Storage::disk('s3B')->put($upload_file_name, file_get_contents($request->file));
                            $fullname = config('config.s3_url') . '/marketplace_resources/' . $filename;
                        }

                        //Insert audio data in table
                        $attributes = [
                            //"temp_id" => $episode->id,
                            "created_at" => now(),
                            "updated_at" => now(),
                        ];

                        if ($request->res_file == 1) {
                            //Insert audio data in table
                            $attributes = [
                                "res_title1" => $request->res_title,
                                "res_file1" => $filename,
                            ];
                        } else if ($request->res_file == 2) {
                            //Insert audio data in table
                            $attributes = [
                                "res_title2" => $request->res_title,
                                "res_file2" => $filename,
                            ];
                        } else if ($request->res_file == 3) {
                            //Insert audio data in table
                            $attributes = [
                                "res_title3" => $request->res_title,
                                "res_file3" => $filename,
                            ];
                        } else if ($request->res_file == 4) {
                            //Insert audio data in table
                            $attributes = [
                                "res_title4" => $request->res_title,
                                "res_file4" => $filename,
                            ];
                        }

                        if ($request->type == "Add") {
                            EpisodeResource::updateOrCreate(['temp_id' => $episode->id], $attributes);
                        } else if ($request->type == "Edit") {
                            EpisodeResource::updateOrCreate(['episode_id' => $episode->episode_id], $attributes);
                        }
                    }

                    return [
                        "status" => true,
                        "docId" => $request->res_file,
                        "filename" => $filename,
                        "fullImgPath" => $fullname
                    ];
                } else {
                    return [
                        'status' => false,
                        'code' => 200,
                        'message' => "Only PDF, DOC, XLS, PPT file allowed.."
                    ];
                }
            }
        } catch (\Exception $ex) {
            return [
                'status' => false,
                'code' => 200,
                'message' => "Something is went wrong, Please try again after some time."
            ];
        }
    }

    /**
     * Remove File and update episode data
     */
    public function removeEpisodeResourceData(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'type' => 'required',
            'docId' => 'required',
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }

        //Getting Episode Info
        if ($request->type == "Add") {
            $episode = EpisodeResource::where('temp_id', $request->episode_id);
        } else if ($request->type == "Edit") {
            $episode = EpisodeDetail::where('episode_id', $request->episode_id)->where('user_id', $user->id)->first();
        }

        if (!$episode) {
            return response()->business_api([
                'status' => false,
                'message' => "No Episode Found."
            ]);
        }

        if ($request->type == "Add") {
            $file = EpisodeResource::where('temp_id', $request->episode_id);
        } else if ($request->type == "Edit") {
            $file = EpisodeResource::where('episode_id', $episode->episode_id);
        }

        if ($file->count() > 0) {
            $imageName = "";
            if ($request->docId == 1) {
                $fname = $file->first(['res_file1']);
                $imageName = $fname->res_file1;
            } else if ($request->docId == 2) {
                $fname = $file->first(['res_file2']);
                $imageName = $fname->res_file2;
            } else if ($request->docId == 3) {
                $fname = $file->first(['res_file3']);
                $imageName = $fname->res_file3;
            } else if ($request->docId == 4) {
                $fname = $file->first(['res_file4']);
                $imageName = $fname->res_file4;
            }

            //Delete data from table and remove file from S3 server
            if ($imageName != "") {
                if (!is_null(env('DBLOCAL'))) {
                    $filename = public_path('uploads/marketplace_resources/' . $imageName);
                    if (file_exists($filename)) {
                        unlink($filename);
                    }
                } else {
                    if (Storage::disk('s3B')->exists("marketplace_resources/" . $imageName)) {
                        Storage::disk('s3B')->delete("marketplace_resources/" . $imageName);
                    }
                }
                //Delete data from table and remove file from S3 server
                if ($request->docId == 1) {
                    $file->update(['res_title1' => null, 'res_file1' => null]);
                } else if ($request->docId == 2) {
                    $file->update(['res_title2' => null, 'res_file2' => null]);
                } else if ($request->docId == 3) {
                    $file->update(['res_title3' => null, 'res_file3' => null]);
                } else if ($request->docId == 4) {
                    $file->update(['res_title4' => null, 'res_file4' => null]);
                }
            }

            return response()->business_api([
                'status' => true,
                'message' => "File deleted successfully."
            ]);
        } else {
            return response()->business_api([
                'status' => false,
                'message' => "No Episode Found."
            ]);
        }
    }

    /**
     * This function is use for adding new tag into database
     */
    public function tagAdded(Request $request) {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return $seller_info;
        }

        $validator = Validator::make($request->all(), [
            'tag' => 'required',            
        ], []);

        if ($validator->fails()) {
            return [
                'status' => false,
                'code' => 200,
                'message' => $validator->errors()->first()
            ];
        }
        
        try {
            //Added into datbase
            \App\Models\EpisodeTag::firstOrCreate(['title' => $request->tag]);

            return response()->business_api([
                'status' => true,
                'message' => "Tag added successfully."
            ]);
        } catch (\Exception $ex) {
            return [
                'status' => false,
                'message' => "Something is went wrong, Please try again after some time."
            ];
        }
        
    }

    /**
     * All Series Episodes Ordering
     */
    public function orderSeriesEpisodes(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            's_id' => 'required'
        ], [
            's_id.required' => '6000:1 - id missing for series'
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //reorder
        $s_id = $request->s_id;
        $order = $request->order;
        foreach ($order as $key => $item) {
            $episode_info = EpisodeDetail::where('show_id', $s_id)
                ->where('user_id', $user->id)
                ->where('episode_id', $item)->first();
            if ($episode_info) {
                $episode_info->order = $key;
                $episode_info->save();
            }
        }
        //end

        return response()->business_api([
            'status' => true,
            'message' => "Reordered successful",
            'data' => []
        ]);
    }

    /**
     * All Series Episodes Listing for reorder
     */
    public function viewSeriesEpisodesforReorder(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => "401:1 Token authentication error."
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if this user is seller or not
        $seller_info = HelperV2::isSeller($user);
        if (!$seller_info['status']) {
            return response()->business_api($seller_info);
        }

        $validator = Validator::make($request->all(), [
            's_id' => 'required'
        ], [
            's_id.required' => '6000:1 - id missing for series'
        ]);

        if ($validator->fails()) {
            return response()->business_api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Getting Series
        $series = Show::where('id', $request->s_id)->where('user_id', $user->id)->first();

        if (!$series) {
            return response()->business_api([
                'status' => false,
                'message' => "No Series Found."
            ]);
        }

        $episode_data = EpisodeDetail::where('show_id', $series->id)
            ->orderBy('order', 'asc')->get(['id', 'episode_id', 'title', 'status', 'created_at']);

        $responseData = [];
        foreach ($episode_data as $item) {
            $responseData[] = [
                'id' => $item->id,
                'episode_id' => $item->episode_id,
                'title' => $item->title,
                'status' => 'compete',
                'type' => 'episode',
                'updateinfo' => "Update Info",
                'updateprice' => "Update Pricing",
                'delist' => $item->status == "Draft" ? "Publish Episode" : "Delist Episode",
                'createDate' => strtotime($item->created_at),
            ];
        }

        $message = "";
        $status = true;
        if (count($responseData) == 0) {
            $status = true;
            $message = "No Episode Found";
        }
        return response()->business_api([
            'status' => $status,
            'message' => $message,
            'data' => [
                'items' => $responseData,
                'series_title' => $series->title
            ]
        ]);
    }
}
