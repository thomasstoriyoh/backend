<?php

namespace App\Http\Controllers\Api\new_version\V1\Auth;

use App\Packages\OTP\OTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Traits\ApiDataTrait;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Models\UserSocial;
use App\Traits\GeneratePassportToken;
use App\Traits\PassportToken;
use Laravel\Socialite\Facades\Socialite;

/** Topic  Subscription Code Start */
use App\Jobs\FirebaseTopicSubscription;
/** Topic  Subscription Code End */

use App\Traits\ResponseFormat;
use App\ClassesV3\V3_3\IP2Location;
//use App\Jobs\SendEmailUser;

class AuthController extends Controller
{
    use ApiDataTrait, GeneratePassportToken, PassportToken, ResponseFormat;    
    
    /**
     * This function is use for login
     * @param Request $request
     * @return mixed values
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'device_token' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Auth.login_username_lbl"),
            'password.required' => config('language.' . $this->getLocale() . ".Auth.login_password_lbl"),
            'device_token.required' => config('language.' . $this->getLocale() . ".Common.token_lbl"),
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'navigate' => 'login',
                'message' => $validator->errors()->first()
            ];

            return response()->api($response);
        }

        $user = User::where('username', $request->username)->orWhere('email', $request->username)->first([
            'id', 'provider_id', 'first_name', 'image', 'email', 'username', 'type', 'country_id', 
            'password', 'api_token', 'verified', 'admin_status', 'discreet', 'city', 'state', 'country', 
            'oauth_enabled', 'user_type', 'dashboard_status'
        ]);

        if (!$user) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_invalid_lbl")
            ]);
        }

        if (!Hash::check($request->password, $user->password)) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_invalid_lbl")
            ]);
        }

        if ($user->verified == 'Disabled') {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
            ]);
        }

        if (!$user->isVerified()) {

            $this->sendVerificationEmail($user);

            return response()->api([
                'status' => false,
                'navigate' => 'verify_email',
                'email' => $user->email,
                'message' => config('language.' . $this->getLocale() . ".Auth.login_account_verified_lbl")
            ]);
        }

        if (!$user->isApproved()) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_approved_lbl")
            ]);
        }

        UserSocial::firstOrCreate(['user_id' => $user->id, 'provider' => 'Direct']);

        //If oauth_enabled is false then
        //we update oauth_enabled true
        if ($user->oauth_enabled == 0) {
            $user->update(['oauth_enabled' => 1]);
        }

        //update last login type
        $user->update(['last_login_type' => 'Direct']);

        /* Creating Profile Access Token */
        //$access_token = $this->getBearerTokenByUser($user, config('config.client_id'), false);
        $access_token = $this->generateAccessToken($request, $request->username, $request->password);

        if (!array_key_exists('access_token', $access_token)) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Common.access_token_lbl")
            ]);
        }

        /*if (empty($user->username)) {
            $data = [
                'id' => @$user->id,
                'type' => @$user->type,
                'provider_id' => @$user->provider_id,
                'username' => $user->username,
                'full_name' => $user->full_name,
                'image' => !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png'),
                'country_id' => $user->country,
                'city' => $user->city ? $user->city : '',
                'state' => $user->state ? $user->state : '',
                'country' => $user->country ? $user->country : '',
                'verified' => $user->verified,
                'admin_status' => $user->admin_status,
                'api_token' => $user->api_token,
                'discreet' => $user->discreet,
                'access_token' => @$access_token['access_token'],
                'refresh_token' => @$access_token['refresh_token'],
                'expires_in' => @$access_token['expires_in']               
            ];

            return response()->api([
                'status' => false,
                'navigate' => 'username',
                'data' => $data,
                'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
            ]);
        }*/

        /*if (! empty($user->username)) {
            if ($user->user_type == 0) {
                $data = [
                    'id' => @$user->id,
                    'type' => @$user->type,
                    'provider_id' => @$user->provider_id,
                    'username' => $user->username,
                    'full_name' => $user->full_name,
                    'image' => !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png'),
                    'country_id' => $user->country,
                    'city' => $user->city ? $user->city : '',
                    'state' => $user->state ? $user->state : '',
                    'country' => $user->country ? $user->country : '',
                    'verified' => $user->verified,
                    'admin_status' => $user->admin_status,
                    'api_token' => $user->api_token,
                    'discreet' => $user->discreet,
                    'access_token' => @$access_token['access_token'],
                    'refresh_token' => @$access_token['refresh_token'],
                    'expires_in' => @$access_token['expires_in']                
                ];
    
                return response()->api([
                    'status' => false,
                    'navigate' => 'username',
                    'data' => $data,
                    'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
                ]);
            }
        }*/        

        $imageData = !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png');
        
        $city = $user->city ? $user->city : '';
        $state = $user->state ? $user->state : '';
        $country = $user->country ? $user->country : '';
        
        $data = collect($user)
            ->put('city', $city)
            ->put('state', $state)
            ->put('country', $country)
            ->put('full_name', $user->full_name)
            ->put('image', $imageData)
            ->put('discreet', $user->discreet)
            ->put('api_token', $user->api_token)
            ->put('access_token', @$access_token['access_token'])
            ->put('refresh_token', @$access_token['refresh_token'])
            ->put('expires_in', @$access_token['expires_in']);

        //$restore_data = $this->restore_user_data($user);        

        /** Topic  Subscription Code Start */
        //Subscribe all podcast for specific user
        $allShows = $user->subscribers()->wherePivot('notify', 1)->pluck('id')->all();
        if(count($allShows) > 0) {
            try {
                $uniqueShows = array_unique($allShows);
                $splitShows = array_chunk($uniqueShows, 10);
                foreach ($splitShows as $item) {
                    dispatch(new FirebaseTopicSubscription($item, $user->id, 'subscribe_on_login', [$request->device_token]))->onQueue('subscribe-topic');
                }
            } catch(\Exception $ex) {}            
        }
        /** Topic  Subscription Code End */

        $navigate = "feed";
        $category_notification_data = $user->categories()->count();
        if($category_notification_data == 0){
            $navigate = "select_category";
        }
        // else {
        //     if ($user->user_type == 2 && $user->dashboard_status == 0) {
        //         $navigate = "recommended_user";
        //     }
        // }

        return response()->api([
            'status' => true,
            'navigate' => $navigate,
            'data' => $data,
            //'category_notification_data' => $category_notification_data,
            'user_type' => $user->user_type,
            //'dashboard_status' => $user->dashboard_status
        ]);
    }

    /**
     * Use for check social login for V2
     *
     * @param Request $request
     * @return void
     */
    public function provider_login_v2(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'email' => 'required',
            'provider' => 'required',
            'provider_id' => 'required',
            'token' => 'required',
            'device_token' => 'required'
        ], [
            'provider.required' => config('language.' . $this->getLocale() . ".Common.provider_lbl"),
            'provider_id.required' => config('language.' . $this->getLocale() . ".Common.provider_id_lbl"),
            'token.required' => config('language.' . $this->getLocale() . ".Auth.login_token_lbl"),
            'device_token.required' => config('language.' . $this->getLocale() . ".Common.token_lbl"),            
            //'email.required' => 'Please enter your email.'
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'blocked' => false,
                'navigate' => 'register',
                'message' => $validator->errors()->first()
            ];

            return response()->api($response);
        }

        //New Social Code Start
        try {
            if (strtolower($request->provider) == 'twitter') {
                if (!$request->secret) {
                    $response = [
                        'status' => false,
                        'blocked' => false,
                        'navigate' => 'register',
                        'message' => config('language.' . $this->getLocale() . ".Auth.login_secret_token_lbl")
                    ];

                    return response()->api($response);
                }
                $social_user = Socialite::driver('twitter')->userFromTokenAndSecret($request->token, $request->secret);
            } else {
                if (strtolower($request->provider) == 'google') {
                    $accessTokenResponse = Socialite::driver('google')->getAccessTokenResponse($request->token);
                    $accessToken = $accessTokenResponse['access_token'];                    
                } else {
                    $accessToken = $request->token;
                }
                $social_user = Socialite::driver(strtolower($request->provider))->userFromToken($accessToken);
                //$social_user = Socialite::driver(strtolower($request->provider))->userFromToken($request->token);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => false,
                'blocked' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_malformed_lbl")
            ];

            return response()->api($response);
        }

        if(! $social_user->email) {
            $response = [
                'status' => false,
                'blocked' => false,
                'navigate' => 'register',
                'message' => config('language.' . $this->getLocale() . ".Common.email_not_found_lbl")
            ];

            return response()->api($response);
        }

        $user = User::where('email', $social_user->email)->first();
        //New Social Code End

        //$user = User::where('email', $request->email)->first();

        if ($user) {
            if ($user->verified == 'Disabled') {
                return response()->api([
                    'status' => false,
                    'blocked' => true,
                    'navigate' => 'login',
                    'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
                ]);
            }

            if ($user->verified == 'Unverified') {
                $user->update(['verified' => 'Verified']);
            }

            if (!$user->isApproved()) {
                return response()->api([
                    'status' => false,
                    'navigate' => 'login',
                    'blocked' => true,
                    'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_approved_lbl")
                ]);
            }

            if (!empty($request->provider) && !empty($request->provider_id)) {
                $login_info = UserSocial::firstOrCreate([
                    'user_id' => $user->id, 'provider' => $request->provider], [
                    'provider_id' => $request->provider_id]);
            }

            //If oauth_enabled is false then
            //we update oauth_enabled true
            if ($user->oauth_enabled == 0) {
                $user->update(['oauth_enabled' => 1]);
            }

            //update last login type
            $random_password = str_random(60);
            $user->update(['last_login_type' => 'Social', 'password_social' => Hash::make($random_password)]);

            /* Creating Social Profile Access Token */
            $access_token = $this->generateAccessToken($request, $user->email, $random_password);
            //$access_token = $this->getBearerTokenByUser($user, config('config.client_id'), false);

            if (!array_key_exists('access_token', $access_token)) {
                return response()->api([
                    'status' => false,
                    'blocked' => false,
                    'navigate' => 'login',
                    'message' => config('language.' . $this->getLocale() . ".Common.access_token_lbl")
                ]);
            }

            $data = [
                'id' => @$user->id,
                'type' => @$user->type,
                'provider_id' => $user->provider_id,
                'username' => $user->username,
                'full_name' => $user->full_name,
                'image' => !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png'),
                'country_id' => $user->country,
                'city' => $user->city ? $user->city : '',
                'state' => $user->state ? $user->state : '',
                'country' => $user->country ? $user->country : '',
                'verified' => $user->verified,
                'admin_status' => $user->admin_status,
                'api_token' => $user->api_token,
                'discreet' => $user->discreet,
                'access_token' => @$access_token['access_token'],
                'refresh_token' => @$access_token['refresh_token'],
                'expires_in' => @$access_token['expires_in']
            ];

            // if (empty($user->username)) {
            //     return response()->api([
            //         'status' => false,
            //         'blocked' => false,
            //         'navigate' => 'username',
            //         'data' => $data,
            //         'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
            //     ]);
            // }

            // if (! empty($user->username)) {
            //     if ($user->user_type == 0) {
            //         return response()->api([
            //             'status' => false,
            //             'blocked' => false,
            //             'navigate' => 'username',
            //             'data' => $data,
            //             'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
            //         ]);
            //     }
            // }           

            $user = User::find($user->id, ['id', 'first_name', 'email', 'username', 'image', 'type', 'country_id', 'password', 'provider_id', 'api_token', 'verified', 'admin_status', 'discreet', 'city', 'state', 'country', 'oauth_enabled']);            

            $imageData = !empty($user->image) ? $user->getImage(100) : '';

            $city = $user->city ? $user->city : '';
            $state = $user->state ? $user->state : '';
            $country = $user->country ? $user->country : '';

            $data = collect($user)
                ->put('full_name', $user->full_name)
                ->put('city', $city)
                ->put('state', $state)
                ->put('country', $country)                
                ->put('image', $imageData)
                ->put('discreet', @$user->discreet)
                ->put('api_token', $user->api_token)
                ->put('access_token', @$access_token['access_token'])
                ->put('refresh_token', @$access_token['refresh_token'])
                ->put('expires_in', @$access_token['expires_in']);

            //$restore_data = $this->restore_user_data($user);            
            //$navigate = $user->username == '' ? 'username' : '';

            /** Topic  Subscription Code Start */
            //Subscribe all podcast for specific user
            $allShows = $user->subscribers()->wherePivot('notify', 1)->pluck('id')->all();
            //\Log::info("ALl SHOWS LISTING === ". json_encode($allShows));
            if(count($allShows) > 0) {
                try {
                    $uniqueShows = array_unique($allShows);
                    $splitShows = array_chunk($uniqueShows, 10);
                    foreach ($splitShows as $item) {
                        dispatch(new FirebaseTopicSubscription($item, $user->id, 'subscribe_on_login', [$request->device_token]))->onQueue('subscribe-topic');
                    }                    
                } catch(\Exception $ex) {}            
            }
            /** Topic  Subscription Code End */

            $navigate = "feed";
            $category_notification_data = $user->categories()->count();
            if($category_notification_data == 0) {
                $navigate = "select_category";
            }
            //  else {
            //     if ($user->user_type == 2 && $user->dashboard_status == 0) {
            //         $navigate = "recommended_user";
            //     }
            // }

            return response()->api([
               'status' => true,
               'blocked' => false,
               'navigate' => $navigate,
               'data' => $data,
               //'category_notification_data' => $category_notification_data,
               'user_type' => $user->user_type,
               //'dashboard_status' => $user->dashboard_status
            ]);
        } else {
            return response()->api([
                'status' => false,
                'blocked' => false,
                'navigate' => 'register',
                'message' => config('language.' . $this->getLocale() . ".Common.user_not_found_lbl")
            ]);
        }
    }

    /**
     * Register the user and send email to the user for verification.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Foundation\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $type = [
            'Facebook' => Facebook::class, 
            'Google' => Google::class
        ];

        if ($request->provider == 'Direct') {
            $validator = $this->validator($request->all());
        } else {
            $validator = $this->social_validator($request->all());
        }

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'navigate' => 'register',
                'message' => $validator->errors()->first()
            ]);
        }

        //Check if this email exist and verified
        $userEmailInfo = \App\Models\User::where('email', $request->email)->first();
        //dd(is_null($request->guest_username));
        //dd($userEmailInfo->isVerified(), $userEmailInfo->toArray());

        if ($userEmailInfo) {
            if ($userEmailInfo->verified == 'Disabled') {
                return response()->api([
                    'status' => false,
                    'navigate' => 'login',
                    'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
                ]);
            }

            if ($userEmailInfo->isVerified()) {
                return response()->api([
                    'status' => false,
                    'navigate' => 'register',
                    'message' => config('language.' . $this->getLocale() . ".Auth.register_email_exist_lbl")
                ]);
            }

            if (! is_null($request->guest_username)) {
                if ($userEmailInfo->verified == 'Unverified') {
                    $userEmailInfo->update(['email' => null]);
                }
            }
        }

        $password = '';
        if ($request->provider == 'Direct') {
            $password = Hash::make($request->password);
        } else {
            $temp_password = str_random(8);
            $password = Hash::make($temp_password);
        }

        $user_type = 1;        
        if (! is_null($request->guest_username)) {
            $user_type = 2;
        }
        
        //Ip2Location code start
        $city = null;
        $state = null;
        $country = null;
        if (!empty($request->ip_address)) {
            $responseData = new IP2Location('api');
            $location = $responseData->get_location();
            
            if (count($location['data']) > 0) {
                $city = $location['data']['city_name'];
                $state = $location['data']['region_name'];
                $country = $location['data']['country_name'];
            }
        }
        //Ip2Location code end

        $user_data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'provider' => $request->provider,
            'provider_id' => $request->provider_id,
            'image' => $request->image,
            'password' => $password,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'admin_status' => 'Approved',
            'oauth_enabled' => 1,
            'registered_platform' => $request->platform,
            'user_type' => $user_type
        ];

        if (is_null($request->guest_username)) {
            $username = uniqid();        
            $username .= $this->getRandomNumber(10);
            $username = "drt_".$username;

            $user_data['username'] = $username;
        }

        if(! is_null($request->guest_username)) {
            $user_data['email'] = $request->email;
            $user = \App\Models\User::firstOrNew(['username' => trim($request->guest_username)]);
            $user->fill($user_data)->save();            
        } else {
            $user = \App\Models\User::firstOrNew(['email' => $request->email]);
            $user->fill($user_data)->save();
        }

        $access_token = null;
        if ($request->provider == 'Direct') {
            try {
                //Send Email to user
                $this->sendVerificationEmail($user);

                //Update Social info
                UserSocial::firstOrCreate(['user_id' => $user->id, 'provider' => 'Direct']);

            } catch (\Exception $ex) {
            }            

            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Auth.register_thank_you_message1_lbl"),
                'navigate' => 'verify_email',
                'data' => [
                    'id' => $user->id,
                    'email' => $user->email
                ]
            ]);            
        } else {
            $logged_in_user = User::find($user->id);
            try {
                //Send thank you mail
                $this->sendThankYouEmail($user);

                /* Save Social Profile image */
                $image = '';
                if ($request->provider != 'Twitter') {
                    $url = (new $type[$request->provider])->getAvatar($request->provider_id);

                    if (!empty($url) && $url != '') {
                        $data = file_get_contents($url);
                        $size = getimagesize($url);
                        $extension = image_type_to_extension($size[2]);                        
                        
                        $userArray = ['verified' => 'Verified'];

                        $extentionArray = ['jpeg', 'png', 'jpg', 'gif', 'svg'];
                        if (in_array(strtolower($extension), $extentionArray)) {
                            $image_name = 'users/' . $logged_in_user->id . '/' . $request->provider_id . $extension;
                            Storage::disk('s3')->put($image_name, $data);
                            $userArray['image']  = $request->provider_id . $extension;
                        }

                        $user->update($userArray);
                    }
                } else {
                    $user->update(['verified' => 'Verified']);
                }

                if (!empty($request->provider) && !empty($request->provider_id)) {
                    $login_info = UserSocial::firstOrCreate([
                        'user_id' => $user->id, 'provider' => $request->provider], [
                        'provider_id' => $request->provider_id]);
                }

                //update last login type
                $random_password = str_random(60);
                $user->update(['last_login_type' => 'Social', 'password_social' => Hash::make($random_password)]);

                /* Creating Social Profile Access Token */
                $access_token = $this->generateAccessToken($request, $logged_in_user->email, $random_password);
                //$access_token = $this->getBearerTokenByUser($user, config('config.client_id'), false);
            } catch (\Exception $ex) {
            }
            
            $user->update(['verified' => 'Verified', "last_access_time" => @date('Y-m-d H:i:s'), "last_access_time_social" => @date('Y-m-d H:i:s')]);
            // Auth::login($logged_in_user);

            //Update user id in push token
            $push_id = \App\Models\PushId::where('token', $request->device_token)->first();
            if ($push_id) {
                $push_id->update(['user_id' => $user->id]);
            }

            if (empty($logged_in_user->api_token)) {
                //$logged_in_user->update(['api_token' => str_random(60)]);
            }

            //If oauth_enabled is false then
            //we update oauth_enabled true
            if ($logged_in_user->oauth_enabled == 0) {
                $logged_in_user->update(['oauth_enabled' => 1]);
            }

            //Added default follower Storiyoh, Rohit and Rahul
            try {
                $logged_in_user->following()->attach(config('config.storiyoh_default_following_ids'));
            } catch (\Exception $ex) {
            }

            //$image = ! empty($image) ? asset('uploads/users/'.$logged_in_user->id .'/' . $image) : "";
            $image = !empty($image) ? $user->getImage(100) : '';

            $city = $logged_in_user->city ? $logged_in_user->city : '';
            $state = $logged_in_user->state ? $logged_in_user->state : '';
            $country = $logged_in_user->country ? $logged_in_user->country : '';            

            $data = collect($logged_in_user)->only(['id', 'api_token', 'username', 'provider', 'provider_id', 'first_name', 'email', 'type', 'country_id', 'city', 'state', 'country'])
                ->put('image', $image)
                ->put('city', $city)->put('state', $state)->put('country', $country)
                ->put('discreet', $logged_in_user->discreet)
                ->put('api_token', "")
                ->put('access_token', @$access_token['access_token'])
                ->put('refresh_token', @$access_token['refresh_token'])
                ->put('expires_in', @$access_token['expires_in']);
            
            $category_notification_data = $logged_in_user->categories()->count();
            $navigate = "feed";
            if($category_notification_data == 0) {
                $navigate = "select_category";
            }

            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Auth.register_thank_you_message2_lbl"),
                'navigate' => $navigate,//'username',
                'data' => $data
            ]);
        }
    }

    /**
     * This function is send otp to the given email
     * @param User $user
    */
    protected function sendVerificationEmail(\App\Models\User $user)
    {
        //$otp = OTP::generate($usertemp->email);
        $otp = OTP::generate($user->email);

        $mailData = [
            'file_path' => 'auth.email.verification',
            'from_email' => config('config.register.sender_email'),
            'from_name' => config('config.register.sender_name'),
            'to_email' => trim($user->email),
            'to_name' => $user->full_name,
            'subject' => 'Email Verification OTP',
            'filename' => null,
            'data' => ['user_data' => $user, 'otp' => $otp]
        ];

        //dispatch(new SendEmailUser($mailData))->onQueue('email');
        \App\Traits\Helper::sendAllEmail($mailData);

        return $otp;
    }

    /**
     * This function is Resend otp to the given email
     * @param User $user
     */
    public function send_email_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'navigate' => 'verify_email',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('email', $request->email)->first();

        $otp = OTP::generate($user->email);

        $mailData = [
            'file_path' => 'auth.email.verification',
            'from_email' => config('config.register.sender_email'),
            'from_name' => config('config.register.sender_name'),
            'to_email' => trim($user->email),
            'to_name' => $user->full_name,
            'subject' => 'Email Verification OTP',
            'filename' => null,
            'data' => ['user_data' => $user, 'otp' => $otp]
        ];

        //dispatch(new SendEmailUser($mailData))->onQueue('email');

        \App\Traits\Helper::sendAllEmail($mailData);

        return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Auth.register_email_otp_lbl")
            ]);
    }

    /**
     * This function is send thank you email for social registration
     * @param User $user
     */
    protected function sendThankYouEmail(\App\Models\User $user)
    {
        $mailData = [
            'file_path' => 'auth.email.thank-you',
            'from_email' => config('config.register.sender_email'),
            'from_name' => config('config.register.sender_name'),
            'to_email' => trim($user->email),
            'to_name' => $user->full_name,
            'subject' => 'Thank you for registering',
            'filename' => null,
            'data' => ['user_data' => $user]
        ];

        //dispatch(new SendEmailUser($mailData))->onQueue('email');
        \App\Traits\Helper::sendAllEmail($mailData);
    }

    /**
     * This function is use for verify email otp
     * @param User $user
    */
    public function verifyEmailOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'otp' => 'required'
        ], [
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl"),
            'otp.required' => config('language.' . $this->getLocale() . ".Auth.register_otp_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'navigate' => 'verify_email',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->api([
                'status' => false,
                'navigate' => 'verify_email',
                'message' => config('language.' . $this->getLocale() . ".Common.user_not_found_lbl")
            ]);
        }

        if ($user) {
            if (OTP::verify($request->otp, $user->email)) {
                
                $user->forceFill(['verified' => 'Verified', "last_access_time" => @date('Y-m-d H:i:s'), "last_access_time_social" => @date('Y-m-d H:i:s')])->save();

                //Added default follower Storiyoh, Rohit and Rahul
                try {
                    $user->following()->attach(config('config.storiyoh_default_following_ids'));
                } catch (\Exception $ex) {
                }

                return $this->login_by_id($request);
            } else {
                return response()->api([
                    'status' => false,
                    'navigate' => 'verify_email',
                    'message' => config('language.' . $this->getLocale() . ".Auth.register_invalid_otp_lbl")
                ]);
            }
        }
    }

    /**
     * This function is use for check if user verify with Email
     * @param User $user
     */
    public function login_by_id(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Common.user_not_found_lbl")
            ]);
        }

        if (!$user->isApproved()) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_approved_lbl")
            ]);
        }

        if (!$user->isVerified()) {
            return response()->api([
                'status' => false,
                'navigate' => 'verify_email',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_account_verified_lbl")
            ]);
        }

        //If Status has been verified than we will update or insert in user table
        //$user->update(['api_token' => str_random(60)]);

        //We update oauth_enabled true
        $user->update(['oauth_enabled' => 1]);

        $image = !empty($user->image) ? $user->getImage(100) : '';

        //Update random code
        $random_password = str_random(60);
        $user->update(['last_login_type' => 'Not_Verified', 'password_social' => Hash::make($random_password)]);

        /* Creating Social Profile Access Token */
        $access_token = $this->generateAccessToken($request, $user->email, $random_password);
        //$access_token = $this->getBearerTokenByUser($user, config('config.client_id'), false);

        if (!array_key_exists('access_token', $access_token)) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Common.access_token_lbl")
            ]);
        }

        // if(empty($access_token)) {
        //     return response()->api([
        //         'status' => false,
        //         'navigate' => 'login',
        //         'message' => 'No Token Found.'
        //     ]);
        // }

        $city = $user->city ? $user->city : '';
        $state = $user->state ? $user->state : '';
        $country = $user->country ? $user->country : '';

        $data = collect($user)->only(['id', 'api_token', 'username', 'provider', 'provider_id', 'first_name', 'email', 'type', 'country_id', 'discreet'])
            ->put('city', $city)->put('state', $state)->put('country', $country)
            ->put('image', $image)->put('api_token', $user->api_token)
            ->put('access_token', @$access_token['access_token'])
            ->put('refresh_token', @$access_token['refresh_token'])
            ->put('expires_in', @$access_token['expires_in']);

        $category_notification_data = $user->categories()->count();        
        $navigate = "feed";        
        if($category_notification_data == 0) {
            $navigate = "select_category";
        }
        // else if ($user->user_type == 2 && $user->dashboard_status == 0) {
        //     $navigate = "recommended_user";
        // }

        return response()->api([
            'status' => true,
            'navigate' => $navigate,
            'data' => $data
        ]);
    }

    /*
     * Device Register.
     *
     * return user data
     */
    public function device_register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'uuid' => 'required', 
            'device_token' => 'required', 
            'platform' => 'required'
        ], [
            'uuid.required' => config('language.' . $this->getLocale() . ".Common.uuid_lbl"),
            'device_token.required' => config('language.' . $this->getLocale() . ".Auth.token_lbl"),
            'platform.required' => config('language.' . $this->getLocale() . ".Common.platform"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'registered' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $user_id = 0;
        $user = Auth::guard('api')->user();
        if ($user) {
            $user_id = $user->id;
        }

        if (!empty($request->uuid) && !empty($request->device_token)) {
            $push_id = \App\Models\PushId::firstOrNew(['uuid' => $request->uuid]);

            $attributes = $request->all();
            $attributes['user_id'] = $user_id;
            $attributes['token'] = $request->device_token;
            $attributes['platform'] = $request->platform;
            $push_id->fill($attributes)->save();
        }

        $responseData = ['status' => true, 'message' => ''];

        return response()->api([
            'status' => true,
            'message' => ""
        ]);
        
        //return $this->getApiResponse($request, $responseData);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'provider' => 'required',
            'first_name' => 'required|max:255',
            //'email' => 'required|email|max:255|unique:users,email',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:8'
        ], [
            'provider.required' => config('language.' . $this->getLocale() . ".Common.provider_lbl"),
            'first_name.required' => config('language.' . $this->getLocale() . ".Auth.register_name_lbl"),
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl"),
            //'email.unique' => config('language.' . $this->getLocale() . ".Auth.register_email_unique_lbl"),
            'password.min' => config('language.' . $this->getLocale() . ".Auth.register_password_min_lbl"),
            'password.required' => config('language.' . $this->getLocale() . ".Auth.register_password_lbl"),
            'password.confirmed' => config('language.' . $this->getLocale() . ".Auth.register_password_confirm_lbl"),
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function social_validator(array $data)
    {
        return Validator::make($data, [
            'provider' => 'required',
            'provider_id' => 'required',
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ], [
            'provider.required' => config('language.' . $this->getLocale() . ".Common.provider_lbl"),
            'provider_id.required' => config('language.' . $this->getLocale() . ".Common.provider_id_lbl"),
            'first_name.required' => config('language.' . $this->getLocale() . ".Auth.register_name_lbl"),
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl"),
        ]);
    }

    /**
     * This function is use for check email address
     *
     * @param Request email
     * @return json
     */
    public function check_email_exist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255'
        ], [
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'navigate' => 'check_email',
                'message' => $validator->errors()->first()
            ]);
        }

        //Check if this email exist in our database
        $userEmailInfo = \App\Models\User::where('email', $request->email)->first();

        if ($userEmailInfo) {
            if ($userEmailInfo->verified == 'Unverified') {
                return response()->api([
                    'status' => true,
                    'navigate' => 'register',
                    'message' => config('language.' . $this->getLocale() . ".Common.email_available")
                ]);
            } elseif ($userEmailInfo->verified == 'Disabled') {
                return response()->api([
                    'status' => false,
                    'navigate' => 'check_email',
                    'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
                ]);
            } elseif ($userEmailInfo->admin_status == 'Pending') {
                return response()->api([
                    'status' => false,
                    'navigate' => 'check_email',
                    'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
                ]);
            } elseif ($userEmailInfo->verified == 'Verified') {
                return response()->api([
                    'status' => true,
                    'navigate' => 'login',
                    'message' => ''
                ]);
            }
        }

        //send JSON message
        return response()->api([
            'status' => true,
            'navigate' => 'register',
            'message' => config('language.' . $this->getLocale() . ".Common.email_available")
        ]);
    }

    /*
     * This function is use for creating
     * new access token
     *
     * return tokens
    */
    public function refresh_token(Request $request)
    {
        $validator = Validator::make($request->all(), 
        [
            'refresh_token' => 'required'
        ], [
            'refresh_token.required' => config('language.' . $this->getLocale() . ".Common.refresh_token")
        ]);
        if ($validator->fails()) {            
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'registered' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $access_token = $this->refreshAccessToken($request);

        if (array_key_exists('error', $access_token)) {
            return response()->api(['status' => false, 'code' => 401, 'message' => $access_token['message'], 'data' => []]);
        }

        $data = [
            'access_token' => @$access_token['access_token'],
            'refresh_token' => @$access_token['refresh_token'],
            'expires_in' => @$access_token['expires_in']
        ];

        return response()->api(['status' => true, 'code' => 200, 'message' => '', 'data' => $data]);
    }

    /**
     * This function is use for register
     * guest user
     */
    public function guest_register(Request $request)
    {
        $validator = Validator::make($request->all(), 
        [
            'uuid' => 'required',
            'device_token' => 'required',
            'platform' => 'required'
        ], [
            'uuid.required' => config('language.' . $this->getLocale() . ".Common.uuid_lbl"),
            'device_token.required' => config('language.' . $this->getLocale() . ".Common.token_lbl"),
            'platform.required' => config('language.' . $this->getLocale() . ".Common.platform")
        ]);

        if ($validator->fails()) {            
            return response()->api([
                'status' => false,
                'navigate' => 'guest_register',
                'registered' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $data = [];
        $user_id = null;
        /*if(! is_null($request->username)) {
            $userData = User::where('username', $request->username)
                ->where('verified', '!=', 'Disabled')->where('admin_status', 'Approved')->first();

            if(is_null($userData)) {

            }
            
            $random_password = str_random(60);
            $userData->update(['last_login_type' => 'Guest', 'password_social' => Hash::make($random_password)]);        
            $access_token = $this->generateAccessToken($request, $request->username, $random_password);        

            $data = collect($userData)->only(['first_name', 'city', 'state', 'country', 'discreet'])
                ->put('provider', "")
                ->put('provider_id', "")
                ->put('email', "")
                ->put('type', "")
                ->put('country_id', "")
                ->put('access_token', @$access_token['access_token'])
                ->put('refresh_token', @$access_token['refresh_token'])
                ->put('expires_in', @$access_token['expires_in']);
            
            
            if ($userData) {
                $user_id = $userData->id;
            }

        } else {*/
            //update last login type
            $random_password = str_random(60);

            $username = uniqid();        
            $username .= $this->getRandomNumber(10);
            $username = "gst_".$username;

            //Ip2Location code start
            $city = null;
            $state = null;
            $country = null;
            if (!empty($request->ip_address)) {
                $responseData = new IP2Location('api');
                $location = $responseData->get_location();
                
                if (count($location['data']) > 0) {
                    $city = $location['data']['city_name'];
                    $state = $location['data']['region_name'];
                    $country = $location['data']['country_name'];
                }
            }
            //Ip2Location code end

            $user_data = [
                'first_name' => "Guest",
                'username' => $username,
                'password' => $random_password,
                'city' => $city,
                'state' => $state,
                'country' => $country,
                'admin_status' => 'Approved',
                'oauth_enabled' => 1,
                'registered_platform' => $request->platform,
                'user_type' => 0,
                'discreet' => 'Y'
            ];

            $insertData = User::create($user_data);

            /* Creating Social Profile Access Token */
            $insertData->update(['last_login_type' => 'Guest', 'password_social' => Hash::make($random_password)]);        
            $access_token = $this->generateAccessToken($request, $username, $random_password);        

            $city = is_null($insertData->city) ? "" : $insertData->city;
            $state = is_null($insertData->state) ? "" : $insertData->state;
            $country = is_null($insertData->country) ? "" : $insertData->country;

            $data = collect($insertData)->only(['id', 'first_name', 'username', 'discreet'])
                //->put('username', $username)
                ->put('provider', "")
                ->put('provider_id', "")
                ->put('email', "")
                ->put('type', "")
                ->put('city', $city)
                ->put('state', $state)
                ->put('country', $country)
                ->put('country_id', "")
                ->put('access_token', @$access_token['access_token'])
                ->put('refresh_token', @$access_token['refresh_token'])
                ->put('expires_in', @$access_token['expires_in']);

            if ($insertData) {
                $user_id = $insertData->id;
            }
        //}
        
        //Insert user device token into tbl_push_id table        
        if (!empty($request->uuid) && !empty($request->device_token)) {
            if ($request->device_token != null) {
                $checkUUid = \App\Models\PushId::where('uuid', $request->uuid)->first();
                $attributes = $request->all();
                $attributes['user_id'] = $user_id;
                $attributes['token'] = $request->device_token;
                $attributes['platform'] = $request->platform;
                if (!$checkUUid) {
                    \App\Models\PushId::create($attributes);
                } else {
                    $checkUUid->fill($attributes)->save();
                }                
            }
        }

        return response()->api([
            'status' => true,
            'navigate' => 'select_category',
            'data' => $data
        ]);        
    }
    
    /**
     * This function is use for
     * getting random number
     */
    protected function getRandomNumber($n)
    { 
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
    
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
    
        return $randomString; 
    }
}
