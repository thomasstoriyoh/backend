<?php

namespace App\Http\Controllers\Api\new_version\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\ClassesV3\Collection;
use App\Traits\ResponseFormat;

class CollectionController extends Controller
{
    use ResponseFormat;

    /**
     * Collection Index Page.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error"),
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $collectionData = new Collection('api', $user);

        return $collectionData->Collections();
    }

    /**
     * Show Detail Page
     * @param Request $request
     * @return type
     */
    public function show(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error"),
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $collectionData = new Collection('api', $user);

        return $collectionData->Show();
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function getCollectionShareLink(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error"),
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $collectionData = new Collection('api', $user);

        return $collectionData->url_shortner();
    }
}
