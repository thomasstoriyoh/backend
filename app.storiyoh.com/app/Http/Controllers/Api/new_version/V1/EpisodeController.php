<?php

namespace App\Http\Controllers\Api\new_version\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Episode;
use App\Models\Show;
use App\Models\EpisodeUserComment;
use App\Traits\Helper;
use App\ClassesV3\Feed;
use App\Traits\FireBase;
use App\Traits\FireBaseAndroid;
use App\ClassesV3\PodcastEpisode2_2 as PodcastEpisode;
use App\Models\User;
use App\Models\Feed as FeedRepo;
use Carbon\Carbon;

use App\Traits\ResponseFormat;

class EpisodeController extends Controller
{
    use ResponseFormat;

    /**
     * Episode page view.
     *
     * @param Request $request
     * @return mixed
    */
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $episodeData = new PodcastEpisode('api', $user);

        return $episodeData->Episodes();        
    }

    /**
     * This function is use for episode search
     *
     * @param Request $request
     * @param RepositoryInterface $repo
     * @return void
     */
    public function search(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => config('language.' . $this->getLocale() . ".Common.search_keyword_lbl")
        ]);

        if ($validator->fails()) {
            $request->keyword = '*';            
        }

        $episodeData = new PodcastEpisode('api', $user);
        
        return $episodeData->Search();        
    }

    /**
     * Episode Detail Page
     * @param Request $request
     * @return type
     */
    public function show(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $episodeData = new PodcastEpisode('api', $user);

        return $episodeData->Show();        
    }    

    /**
    * This function is use for return user episode queue
    * @param Request $request
    */
    public function episode_queue(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $user_queue = $user->user_episode_queue()->orderBy('user_queue.order')
            ->orderBy('user_queue.created_at', 'DESC')->withCount(['like', 'comments'])->get();
        $data = [];
        $show_name = '';
        $show_image = '';

        foreach ($user_queue as $value) {
            $data[] = [
                'id' => $value->id,
                'title' => $value->title,
                'show_id' => !empty($value->show->id) ? $value->show->id : '',
                'show_name' => !empty($value->show->title) ? trim(str_replace("\n", '', html_entity_decode($value->show->title))) : '',
                'image' => !empty($value->show->image) ? $value->show->getWSImage(200) : asset('uploads/default/show.png'),
                'duration' => $value->getDurationText(),
                'audio_file' => $value->getAudioLink(),
                'listen_count' => Helper::shorten_count($value->listen_count), 
                "explicit" => $value->show->explicit,
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for add / Remove episode to queue
     * @param Request $request
     */
    public function episode_queue_action(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
            'action' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl"),
            'action.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_action_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $user_queue = $user->user_episode_queue()->find($request->episode_id, ['id', 'show_id', 'title', 'mp3', 'duration', 'image']);
        $data = [];
        if ($user_queue) {
            //return json from webservices
            if ($request->action == 'add') {
                return response()->api([
                   'status' => true,
                   'already_added' => 1,
                   'data' => $data,
                   'message' => 'You have already added this episode in the queue.'
                ]);
            } elseif ($request->action == 'remove') {
                $request->episode_id = is_array($request->episode_id) ? $request->episode_id : [$request->episode_id];

                $user->user_episode_queue()->detach($request->episode_id);

                //Also Update queue_count in tbl_episodes table
                try {
                    Episode::where('id', $request->episode_id)->update(['queue_count' => \DB::raw('queue_count - 1')]);
                } catch(\Exception $e) {}

                return response()->api([
                   'status' => true,
                   'episode_id' => $request->episode_id[0],
                   'already_added' => 0,
                   'data' => $data,
                   'message' => 'Removed!'
                ]);
            }
        } else {
            if ($request->action == 'add') {
                $user_queue = Episode::find($request->episode_id, ['id', 'show_id', 'title', 'mp3', 'duration', 'image', 'listen_count', 'explicit']);
                $data = [
                    'id' => $user_queue->id,
                    'title' => trim(str_replace("\n", '', $user_queue->title)),
                    'show_id' => $user_queue->show_id,
                    'show_name' => trim(str_replace("\n", '', html_entity_decode($user_queue->show->title))),
                    'image' => !empty($user_queue->image) ? $user_queue->show->getWSImage(200) : asset('uploads/default/show.png'),
                    'audio_file' => $user_queue->getAudioLink(),
                    'duration' => $user_queue->getDurationText(),
                    'listen_count' => Helper::shorten_count($user_queue->listen_count), 
                    "explicit" => $user_queue->show->explicit,
                ];

                $user->user_episode_queue()->attach($request->episode_id); 
                
                //Also Update queue_count in tbl_episodes table
                try {
                    Episode::where('id', $request->episode_id)->update(['queue_count' => \DB::raw('queue_count + 1')]);
                } catch(\Exception $e) {}

                return response()->api([
                    'status' => true,
                    'data' => $data,
                    'already_added' => 0,
                    'message' => 'Added to your Queue!'
                ]);
            } elseif ($request->action == 'remove') {
                return response()->api([
                   'status' => false,
                   'episode_id' => $request->episode_id[0],
                   'already_added' => 0,
                   'data' => $data,
                   'message' => 'Please provide valid episode id.'
                ]);
            }
        }
    }

    /**
     * This function is use for remove queue
     *
     * @param Request $request
     * @return void
     */
    public function remove_queue(Request $request) 
    {
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        } 
        
        if(count($user->user_episode_queue->pluck('id')->all()) == 0) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl")
            ]);
        }

        //Also Update queue_count in tbl_episodes table
        try{
            $all_queue_episodes = $user->user_episode_queue->pluck('id')->all();
            foreach($all_queue_episodes as $item) {
                Episode::where('id', $item)->update(['queue_count' => \DB::raw('queue_count - 1')]);
            }
        } catch(\Exception $ex){} 

        $user->user_episode_queue()->detach();

        return response()->api([
            'status' => true,
            'message' => 'Removed!'
         ]);
    }

    /**
     * This function is use for reorder episode in the queue
     * @param Request $request
    */
    public function queue_reorder(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'sequence' => 'required',
        ], [
            'sequence.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_sequence_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $sequesnce = explode(',', $request->sequence);
        array_filter($sequesnce);

        foreach ($sequesnce as $key => $value) {
            $user_queue = $user->user_episode_queue()->find($value);
            if ($user_queue) {
                $user_queue->pivot->order = $key + 1;
                $user_queue->pivot->save();
            }
        }        

        return response()->api([
            'status' => true,
            'message' => 'Reorder episode successfully'
        ]);
    }

    /**
     * This function is use to get episode audio link
     * @param Request $request
    */
    public function getEpisodeDownloadLink(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $request->episode_id = is_array($request->episode_id) ? $request->episode_id : [$request->episode_id];

        $episode_data = Episode::whereIn('id', $request->episode_id)->get();

        $data = [];
        if (count($episode_data) > 0) {
            foreach ($episode_data as $value) {
                $data[] = [
                    'id' => $value->id,
                    'audio_file' => $value->getAudioLink()
                ];
            }
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Episode download list based on timestamp.
     *
     * @param Request $request
     * @return mixed
     */
    public function episode_download_list(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $timestamp = $request->timestamp ? \Carbon\Carbon::createFromTimestamp($request->timestamp)
            ->toDateTimeString() : date('Y-m-d h:i:s');

        $user_shows = $user->subscribers()->with(['episodes' => function ($q) use ($timestamp) {
            $q->where('created_at', '>=', $timestamp);
        }])->has('episodes')->get(['id']);

        $episode_data = [];

        $last_updated = $timestamp;

        if (count($user_shows) > 0) {
            foreach ($user_shows as $show) {
                foreach ($show->episodes as $episode) {
                    $episode_data[] = [
                        'episode_id' => $episode->id,
                        'episode_title' => html_entity_decode($episode->title),
                        'audio_file' => $episode->getAudioLink()
                      ];

                    $last_updated = $episode->created_at;
                }
            }
        }

        $timestamp = $last_updated != '' ? strtotime($last_updated) : '';

        return response()->api([
            'status' => true,
            'data' => $episode_data,
            'timestamp' => $timestamp
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function getEpisodeShareLink(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $episodeData = new PodcastEpisode('api', $user);

        return $episodeData->url_shortner();
    }    

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function update_episode_duration(Request $request)
    {        
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'duration' => 'required',
            'episode_id' => 'required',
        ], [
            'duration.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_duration_lbl"),
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $episodeData = Episode::find($request->episode_id);                
        
        if($episodeData) {
            if ($episodeData->duration == 0 || $episodeData->duration == NULL) {
                $attributes = ['duration' => $request->duration];
                $episodeData->fill($attributes)->save();
            }
        }    

        return response()->api([
            'status' => true,
            'data' => ""
        ]);
    }

    /**
     * This function is use for connection list in episode details
     *
     * @param Request $request
     * @return void
     */
    public function episode_conections(Request $request) {
        
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required',
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try{
            $episode = Episode::where('id', $request->episode_id)->first(['id', 'show_id', 'title', 'updated_at', 'description', 'image', 'duration', 'mp3', 'date_created', 'listen_count']);
            
            if (!$episode) {            
                return response()->api([
                    'status' => true,
                    'message' => 'No Eposode Found.',
                    'data' => [],
                    'num_of_listens' => 0
                ]);
            }

            $recent_listens_arr = [];
                
            $followingIds = $user->following()->verified()->pluck('id')->all();
    
            //dd($followingIds);
            $user_ids = [];
            $create_data_array = [];
            if (count($followingIds) > 0) {
                $followingIds = implode(",", $followingIds);
                $recent_listens = \DB::select('select distinct(user_id), MAX(created_at) as created_at from `tbl_user_stream_data` where `episode_id` = ' . $episode->id . ' and `user_id` != ' . $user->id . ' and `discreet` = "N" and user_id IN(' . $followingIds . ') group by `user_id` order by `created_at` desc limit 10');
                if(count($recent_listens) == 0) {
                    $recent_listens = \DB::select('select distinct(user_id), MAX(created_at) as created_at from `tbl_user_stream_data` where `episode_id` = ' . $episode->id . ' and `user_id` != ' . $user->id . ' and `discreet` = "N" group by `user_id` order by `created_at` desc limit 10');    
                }
                foreach ($recent_listens as $recent_listen) {
                    $create_data_array[$recent_listen->user_id] = $recent_listen->created_at;
                    $user_ids[] = $recent_listen->user_id;
                }
            } else {
                $recent_listens = \DB::select('select distinct(user_id), MAX(created_at) as created_at from `tbl_user_stream_data` where `episode_id` = ' . $episode->id . ' and `user_id` != ' . $user->id . ' and `discreet` = "N" group by `user_id` order by `created_at` desc limit 10');                
                foreach ($recent_listens as $recent_listen) {
                    $create_data_array[$recent_listen->user_id] = $recent_listen->created_at;
                    $user_ids[] = $recent_listen->user_id;
                } 
            }
            
            if (count($user_ids) > 0) {
                $users = User::whereIn('id', $user_ids)->orderByRaw('FIELD(id,' . implode(',', $user_ids) . ') ASC')->get(['id', 'username', 'first_name', 'last_name', 'image']);                
                foreach ($users as $key => $user) {
                    array_push($recent_listens_arr, [
                        'username' => $user->username,
                        'name' => $user->first_name . ' ' . $user->last_name,
                        'image' => !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png'),
                        'created_at' => Carbon::createFromTimeStamp(strtotime($create_data_array[$user->id]))->diffForHumans()
                    ]);                    
                }
            }
            
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => $recent_listens_arr,
                'num_of_listens' => Helper::shorten_count($episode->listen_count),
            ]);
        } catch(\Exception $e) {
            return response()->api([
                'status' => false,
                'message' => '',
                'data' => [],
                'num_of_listens' => 0
            ]);
        }        
    }

    /**
    * This function is use for return user episode queue
    * @param Request $request
    */
    public function episode_queue_listing(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'episode_ids' => 'required',
        ], [
            'episode_ids.required' => config('language.' . $this->getLocale() . ".Episode.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $request->episode_ids = trim($request->episode_ids, ",");
        $episodeIds = explode(",", trim($request->episode_ids));
        $episodeIds = array_filter($episodeIds);
        $user_queue = Episode::whereIn('id', $episodeIds)->orderByRaw('FIELD(id,' . trim($request->episode_ids) . ')')->get(['id', 'show_id', 'title', 'duration', 'mp3', 'listen_count', 'explicit']);
        //$user_queue = Episode::whereIn('id', $episodeIds)->get(['id', 'show_id', 'title', 'duration', 'mp3', 'listen_count', 'explicit']);

        $data = [];
        foreach ($user_queue as $value) {
            $data[] = [
                'id' => $value->id,
                'title' => $value->title,
                'show_id' => !empty($value->show->id) ? $value->show->id : '',
                'show_name' => !empty($value->show->title) ? trim(str_replace("\n", '', html_entity_decode($value->show->title))) : '',
                'image' => !empty($value->show->image) ? $value->show->getWSImage(200) : asset('uploads/default/show.png'),
                'duration' => $value->getDurationText(),
                'audio_file' => $value->getAudioLink(),
                'listen_count' => Helper::shorten_count($value->listen_count),
                'explicit' => $value->show->explicit,
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }
}
