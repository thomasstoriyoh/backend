<?php

namespace App\Http\Controllers\Api\new_version\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Traits\ResponseFormat;

use App\ClassesV3\V3_3\IP2Location;

class IP2LocationController extends Controller
{
    use ResponseFormat;

    public function get_location (Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $responseData = new IP2Location('api', $user);

        return $responseData->get_location();
    }
}
