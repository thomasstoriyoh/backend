<?php

namespace App\Http\Controllers\Api\new_version\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Show;
use App\Models\Faqcategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\ClassesV3\V3_3\Banners;
use App\ClassesV3\V3_3\Common;
use App\ClassesV3\V3_3\Discover;
use App\ClassesV3\V3_3\Playlist;
use App\ClassesV3\V3_3\SmartPlaylist;
use App\ClassesV3\V3_3\Search;
use App\ClassesV3\Collection;
use App\ClassesV3\Podcast2_2 as Podcast;
use App\ClassesV3\PodcastEpisode2_2 as PodcastEpisode;
use App\Traits\ResponseFormat;
use App\Traits\Helper;
use App\Models\Board;
use App\Models\Episode;
use App\Models\SmartPlaylist as SPlaylist;
use DB;
class WebViewController extends Controller
{
    use ResponseFormat;

    /**
     * FAQ Category.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function faqs_category()
    {
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('config.auth_error') . ' Faqs Category.'
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $data = Faqcategory::published()->approved()
            ->orderBy('order')->latest()->get(['id', 'title']);
        
        $result = ['items' => $data ]; 

        return $this->sendResponse($result);
    }

    /**
     * FAQ Listing.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function faqs(Request $request)
    {
        $faq = Faqcategory::published()->approved()
            ->orderBy('order')->latest()->pluck('title', 'id')->all();

        return view('api.web.faqs', compact('faq'));        
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function faqs_details(Request $request, $catId)
    {
        $faqCategory = Faqcategory::find($catId, ['id', 'title']);

        $data = $faqCategory->faqs()->published()->approved()
            ->orderBy('order')->latest()->get(['id', 'title', 'answer']);
        
        return view('api.web.faqs_details', compact('faqCategory', 'data'));
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function faqs_with_categories(Request $request)
    {
        $categoryData = Faqcategory::published()->approved()
            ->orderBy('order')->latest()->get(['id', 'title']);

        $categoryDataInfo = [];
        foreach ($categoryData as $category) {
            $data = Faq::published()->approved()
                ->where('faq_categories_id', $category->id)
                ->orderBy('order')->latest()->get(['id', 'title', 'answer']);
            $categoryDataInfo[] = ['id' => $category->id, 'title' => $category->title, 'faqs' => $data];
        }

        return response()->api([
            'status' => true,
            'message' => '',
            'data' => [
                'items' => $categoryDataInfo
            ]
        ]);
    }

    /**
     * About Us.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about_us()
    {
        return view('api.web.about_us');
    }

    /**
     * Disclaimer.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function disclaimer()
    {
        return view('api.web.disclaimer');
    }

    /**
     * Terms & Condition.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function terms_of_use()
    {
        return view('api.web.terms_of_use');
    }

    /**
     * Privacy Policy.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privacy_policy()
    {
        return view('api.web.privacy_policy');
    }

    /**
     * Attributions.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function attributions()
    {
        return view('api.web.attributions');
    }

    /**
     * Copyright Policy.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function copyright_policy()
    {
        return view('api.web.copyright_policy');
    }

    /**
     * Community Guidelines.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function community_guidelines()
    {
        return view('api.web.community_guidelines');
    }

    /**
     * Podcast of the day
     *
     * @param Request $request
     * @return void
     */
    public function home_page_content_podcast_of_the_day(Request $request)
    {
        try {

            $request = request();
            
            $home_data = new Common('web');

            $podcast_of_the_day = $home_data->home_page_content_podcast_of_the_day();

            return $podcast_of_the_day;

        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        } 
    }

    /**
     * Banner Click function
     *
     * @param Request $request
     * @return void
     */
    public function banner_clicks(Request $request)
    {
        $validator = Validator::make($request->all(),[
             'banner_id' => 'required',
             'os' => 'required',
             'browser' => 'required'
        ]);

        // validation errors
        if ($validator->fails()) {
            return $this->sendValidationError($validator->messages()->getMessages(),$validator->messages()->first());
        }
        
        try {

            $banner_clicks = new Banners('web');

            $result = $banner_clicks->banner_clicks_event($request);

            return $this->sendResponse($result);
        
        } catch (\Exception $ex) {
            
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());     
        }
    }

    /**
     * Homepage
     *
     * @return void
     */
    public function home(Request $request)
    {
        try {
            $request = request();
            $banner = new Banners('web');
            //banner
            $bannerArray = $banner->banner_listing();
            $podcast_of_the_day = $this->home_page_content_podcast_of_the_day($request);
            $banner = $bannerArray->merge($podcast_of_the_day);
            //trending show
            $common = new Common('web');
            $pl = new Playlist('web');
            $spl = new SmartPlaylist('web');
            $trending_show = $common->home_page_content_trending_show(); 
            $trending_playlist = $pl->home_page_content_trending_playlist();
            $trending_smart_playlist = $spl->home_page_content_trending_smart_playlist();

            $result = [
                'banner' => $banner,
                'trending_playlist'=>$trending_playlist,
                'trending_show'=>$trending_show,
                'trending_smart_playlist'=>$trending_smart_playlist
            ];
            return $this->sendResponse($result);

        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }   
       
    }
 
    /**
     * Show Detail
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function show(Request $request)
    {
        $podcast = new Podcast('web');

        return $podcast->Show();
    }
  
   /**
    * collection list
    * @param Type $request
    * @return type
    * @throws conditon
    **/
   public function show_lists(Request $request)
   {
  
        $validator = Validator::make($request->all(), 
        ['show_id' => 'required'],
        ['show_id.required' => 'Please provide show id.',]);

        if ($validator->fails()) {
            return $this->sendValidationError($validator->messages()->getMessages(),$validator->messages()->first());
        }

        try {
            $show = Show::published()->where('id', $request->show_id)->first();

            if (!$show) {            
                return $this->sendEmptyResponse();
            }
            //Get Show collection
            $charts = $show->getCollection()->take(5)->published()->get(['id', 'title', 'image']);
            $smart = $show->getSmartList()
                    ->orderBy('show_smart_playlist.created_at','DESC')
                    ->select('id', 'title', 'image','user_id')
                    ->limit(5)
                    ->get();
            $list = collect($charts)->merge(collect($smart));

                   
            $chart_arr = [];
            foreach ($list as $key => $list) {
                $chart_arr[] = [
                    'id' => $list->id,
                    'key' => $key + 1,
                    'title' => $list->title,
                    'type' => isset($list->user_id) ? 'smart_playlist' : 'collection',
                    'image' => $list->getImage(200),
                    'url_slug'  => Helper::make_slug($list->title)
                ];
            }
            return $this->sendResponse($chart_arr);
        } catch(\Exception $ex){
            
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }  
   }
    
   /**
     * shows episode listing
     *
     * @param Request $request
     * @return void
     */
    public function show_episodes(Request $request)
    {
        $showData = new Podcast('web');

        return $showData->Episodes();
    }
    /**
     * Episode detail
     *
     * @param Request $request
     * @return void
     */
    public function episode_show(Request $request)
    {
        $episodeData = new PodcastEpisode('web');

        return $episodeData->Show();
    } 
    /**
     * Latest Episode listeners list
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function episode_listeners(Request $request)
    {
        $episodeData = new PodcastEpisode('web');

        return $episodeData->EpisodeListeners();
    }
    /**
     * Trending Playlist
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function trending_playlist(Request $request)
    {
        try {
            $playlist = new Playlist('web');
            return $playlist->trending_playlist();
        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Playlist List
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function playlist(Request $request)
    {
        try {
            $playlist = new Playlist('web');
            return $playlist->playlist();
        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Trending Playlist Detail
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function trending_playlist_detail(Request $request)
    {
        try {
            $playlist = new Playlist('web');
            return $playlist->trending_playlist_detail();
        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Trending Playlist Episodes
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function trending_playlist_episodes(Request $request)
    {
        try {
            $playlist = new Playlist('web');
            return $playlist->trending_playlist_episodes();
        } catch (\Exception $ex) {
           
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Trending Playlist Followers
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function trending_playlist_follwers(Request $request)
    {
        try {
            $playlist = new Playlist('web');
            return $playlist->trending_playlist_follwers();
        } catch (\Exception $ex) {
             
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Trending Smart Playlist List
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function trending_smartplaylist(Request $request)
    {
        try {
            $spl = new SmartPlaylist('web');
            return $spl->trending_smartplaylist();
        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Smart Playlist List
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function smartplaylist(Request $request)
    {
        try {
            $spl = new SmartPlaylist('web');
            return $spl->smartplaylist();
        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Trending Smart Playlist Detail
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function trending_smartplaylist_detail(Request $request)
    {
        try {
            $spl = new SmartPlaylist('web');
            return $spl->trending_smartplaylist_detail();
        } catch (\Exception $ex) {
          
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Trending Smart Playlist Episodes
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function trending_smartplaylist_episodes(Request $request)
    {
        try {
            $spl = new SmartPlaylist('web');
            return $spl->trending_smartplaylist_episodes();
        } catch (\Exception $ex) {
           
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Trending Smart Playlist Followers
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function trending_smartplaylist_follwers(Request $request)
    {
        try {
            $spl = new SmartPlaylist('web');
            return $spl->trending_smartplaylist_follwers();
        } catch (\Exception $ex) {
          
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
       
    }

    /**
     * Collection Listing
     *
     * @param Request $request
     * @return void
     */
    public function collection(Request $request)
    {
        $collectionData = new Collection('web');

        return $collectionData->Collections();
    }

    /**
     * Collection Show
     *
     * @param Request $request
     * @return void
     */
    public function collection_show(Request $request)
    {
        $collectionData = new Collection('web');

        return $collectionData->Show();
    }

    #############Discover Section##############
    /**
     * Discover Masthead
     */
    public function discover_mathead(Request $request)
    {
        $discover = new Discover('web');

        return $discover->discover_masthead();
    }
    
    /**
     * Discover pod of the day
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function pod_of_the_day(Request $request)
    {
        $discover = new Discover('web');

        return $discover->pod_of_the_day();
    }
     /**
     * Discover popular shows
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function popular_shows(Request $request)
    {
        $discover = new Discover('web');

        return $discover->popular_shows();
    }

    /**
     * Discover network list
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function networks(Request $request)
    {
        $discover = new Discover('web');

        return $discover->networks();
    }

    /**
     * Discover network shows
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function network_shows(Request $request)
    {
        $discover = new Discover('web');

        return $discover->network_shows();
    }

    /**
     * Discover categories
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function categories(Request $request)
    {
        $discover = new Discover('web');

        return $discover->categories();
    }

    /**
     * Discover categories shows
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function categories_shows(Request $request)
    {
        $discover = new Discover('web');

        return $discover->categories_shows();
    }

    /**
     *Source file for player
     *
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function sourceFile(Request $request)
    {
        $validator = Validator::make($request->all(), 
        ['type' => 'required','id' => 'required'],
        ['type.required' => 'Please provide type.','id.required' => 'ID missing']);

        if ($validator->fails()) {
            return $this->sendValidationError($validator->messages()->getMessages(),$validator->messages()->first());
        }

        try {
            $list = [];

            switch ( $request->type ) {
                case 'Playlist' : 
                    $board = Board::where('id',$request->id)->first();
                    if(!$board) {
                        return $this->sendEmptyResponse("No content available");
                    }
                    foreach ($board->episodes()->orderBy('id','DESC')->get() as $key => $value) {
                        $episode = Episode::find($value->pivot->episode_id);
                        
                        $list[] =  [
                            'title' => $episode->title,
                            'artist' => $board->user->getFullNameAttribute(),
                            'src' => $episode->getAudioLink(),
                            'pic' =>!empty($episode->show) ? $episode->show->getWSImage(200) : asset('uploads/default/board.png'),
                        ];
                    
                    }
                break;
                case 'SmartPlaylist' :
                    $smart_playlist = SPlaylist::where('id', $request->id)->first();
                    
                    if(!$smart_playlist) {
                        return $this->sendEmptyResponse("No content available");
                    }

                    foreach ($smart_playlist->shows()->published()->orderBy('id','DESC')->get() as $key => $value) {
                        $episode =$value->episodes()->published()->first();
                        $list[] =  [
                            'title' => $episode->title,
                            'artist' => $smart_playlist->user->getFullNameAttribute(),
                            'src' => $episode->getAudioLink(),
                            'pic' =>!empty($episode->show) ? $episode->show->getWSImage(200) : asset('uploads/default/board.png'),
                        ];
                    
                    }

                break;
                default :
                $episode = Episode::find($request->id);
                if(!$episode) {
                    return $this->sendEmptyResponse("No content available");
                }
                $list[] =  [
                    'title' => $episode->title,
                    'artist' => $episode->show->artist_name,
                    'src' => $episode->getAudioLink(),
                    'pic' =>!empty($episode->show) ? $episode->show->getWSImage(200) : asset('uploads/default/board.png'),
                ];
            }
             
           return $this->sendResponse($list);
        } catch (\Exception $ex) {
            
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());     
        }
     
    }

    /**
     * Unsubscribe newsletter function
     *
     * @param [type] $token
     * @return void
     */
    public function unsubscribe($token) {
        $data = '';
        try {
            $data = (object) json_decode(decrypt($token));            
        } catch (\Exception $e) {
            echo 'Something went worng!';
            exit;
        }

        if ($data) {
            $subscriber = \App\Models\Subscriber::where('id', $data->id)->where('email', $data->email)->first();

            if ($subscriber->subscription_status == 1) {
                $subscriber->update(['subscription_status' => 0]);
                echo 'You have been unsubscribed successfully !';
            } else {
                echo 'you have already been unsubscribed !';
            }
        }
    }

    /**
     * Autocomplete Search
     *
     * @param Request $request
     * @return void
     */
    public function auto_search(Request $request)
    {
        $search = new Search('web');

        return $search->auto_search();

    }

    /**
     * Show Search
     *
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function showSearch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => 'Please provide search keyword.'
        ]);

        if ($validator->fails()) {
            $request->keyword = '*';
        }

        $showData = new Podcast('web');

        return $showData->Search();
        
    }

    /**
     * Episode Search
     *
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function episodeSearch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => 'Please provide search keyword.'
        ]);

        if ($validator->fails()) {
            $request->keyword = '*';            
        }

        $episodeData = new PodcastEpisode('web');
        
        return $episodeData->Search();     
    }
    
    /**
     * Playlist Search
     *
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function playlistSearch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => 'Please provide search keyword.'
        ]);

        if ($validator->fails()) {
            $request->keyword = '*';            
        }

        $playlist = new Search('web');
        
        return $playlist->playlist_search();
    }

    /**
     * Smart Playlist Search
     *
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function smartplaylistSearch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => 'Please provide search keyword.'
        ]);

        if ($validator->fails()) {
            $request->keyword = '*';            
        }

        $splaylist = new Search('web');
        
        return $splaylist->smartplaylist_search();
    }

    /**
     * User Search
     *
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function userSearch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => 'Please provide search keyword.'
        ]);

        if ($validator->fails()) {
            $request->keyword = '*';            
        }

        $user = new Search('web');
        
        return $user->user_search();
    }

    /**
     * get country from ip address
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function getCountry(Request $request)
    {
        // Get IP number from dotted IP address
        $ipno = $this->Dot2LongIP(request()->ip());

        //Fetch location from ip number
        $country_region = DB::select(DB::raw("SELECT * FROM tbl_ip2location_db3 WHERE ? <= ip_to ORDER BY ip_to LIMIT 1"), array($ipno));

        $data = [];            
        if(count($country_region) > 0) {
            $data = [
                "country_code" => $country_region[0]->country_code,
            ];
        }

        return  $this->sendResponse($data);
    }

    // Convert dotted IP address into IP number in long
    protected function Dot2LongIP ($IPaddr)
    {
        if ($IPaddr == "") 
        {
            return 0;
        } 
        else 
        {
            $ips = explode(".", "$IPaddr");
            return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
        }
    }

}
