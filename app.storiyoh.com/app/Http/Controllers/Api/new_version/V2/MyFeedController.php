<?php

namespace App\Http\Controllers\Api\new_version\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Auth;
use App\Models\Feed;
use App\ClassesV3\Feed as FeedRepo;

use App\MyFeedResponse\UserFeedResponse;
use App\MyFeedResponse\BoardFeedResponse;
use App\MyFeedResponse\EpisodeHeardFeedResponse;
use App\MyFeedResponse\ShowFeedResponse;
use App\MyFeedResponse\BoardFollowResponse;
use App\MyFeedResponse\SmartPlaylistFollowResponse;
use App\MyFeedResponse\PostResponse;
use App\MyFeedResponse\PostLikeFeedResponse;
use App\MyFeedResponse\PostCommentFeedResponse;
use App\MyFeedResponse\PostTagFeedResponse;

use App\Models\PostShow;
use App\Models\PostEpisode;
use App\Models\PostPlaylist;
use App\Models\PostSmartPlaylist;
use App\Models\PostCollection;

use App\Models\PostShowComment;
use App\Models\PostEpisodeComment;
use App\Models\PostPlaylistComment;
use App\Models\PostSmartPlaylistComment;
use App\Models\PostCollectionComment;

use App\Traits\ResponseFormat;


class MyFeedController extends Controller
{
    use ResponseFormat;

    protected $feedMap = [
        FeedRepo::FOLLOW_USER => UserFeedResponse::class,
        FeedRepo::BOARD_UPDATE => BoardFeedResponse::class,
        FeedRepo::EPISODE_HEARD => EpisodeHeardFeedResponse::class,
        FeedRepo::SUBSCRIBE_SHOW => ShowFeedResponse::class,        
        FeedRepo::FOLLOW_BOARD => BoardFollowResponse::class,
        FeedRepo::FOLLOW_SMARTLIST => SmartPlaylistFollowResponse::class,        

        /** New Post Code Start */
        FeedRepo::NEW_SHOW_POST => PostResponse::class,
        FeedRepo::NEW_EPISODE_POST => PostResponse::class,
        FeedRepo::NEW_PLAYLIST_POST => PostResponse::class,
        FeedRepo::NEW_SMARTPLAYLIST_POST => PostResponse::class,
        FeedRepo::NEW_COLLECTION_POST => PostResponse::class,

        /** New Individual Share Post 21/05/2019 */
        FeedRepo::INDIVIDUAL_POST => PostResponse::class,

        /** Post Like Code Start */
        FeedRepo::SHOW_POST_LIKE => PostLikeFeedResponse::class,
        FeedRepo::EPISODE_POST_LIKE => PostLikeFeedResponse::class,
        FeedRepo::PLAYLIST_POST_LIKE => PostLikeFeedResponse::class,
        FeedRepo::SMARTPLAYLIST_POST_LIKE => PostLikeFeedResponse::class,
        FeedRepo::COLLECTION_POST_LIKE => PostLikeFeedResponse::class,

        /** New Individual Share Post 21/05/2019 */
        FeedRepo::INDIVIDUAL_POST_LIKE => PostLikeFeedResponse::class,

        /** Post Comment Code Start */
        FeedRepo::SHOW_POST_COMMENT => PostCommentFeedResponse::class,
        FeedRepo::EPISODE_POST_COMMENT => PostCommentFeedResponse::class,
        FeedRepo::PLAYLIST_POST_COMMENT => PostCommentFeedResponse::class,
        FeedRepo::SMARTPLAYLIST_POST_COMMENT => PostCommentFeedResponse::class,
        FeedRepo::COLLECTION_POST_COMMENT => PostCommentFeedResponse::class,

        /** New Individual Share Post 21/05/2019 */
        FeedRepo::INDIVIDUAL_POST_COMMENT => PostCommentFeedResponse::class,

        /** Post tags for all types */
        FeedRepo::SHOW_POST_COMMENT_TAG => PostTagFeedResponse::class,
        FeedRepo::EPISODE_POST_COMMENT_TAG => PostTagFeedResponse::class,
        FeedRepo::PLAYLIST_POST_COMMENT_TAG => PostTagFeedResponse::class,
        FeedRepo::SMARTPLAYLIST_POST_COMMENT_TAG => PostTagFeedResponse::class,
        FeedRepo::COLLECTION_POST_COMMENT_TAG => PostTagFeedResponse::class,

        /** New Individual Share Post 21/05/2019 */
        FeedRepo::INDIVIDUAL_POST_COMMENT_TAG => PostTagFeedResponse::class,
    ];

    /**
     * This function is use for my feed section
     *
     * @param Request $request
     * @return void
     */
    public function my_network_feed(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = Feed::query();
        $query->orWhere(function($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\User")
                ->whereIn('type', [
                    FeedRepo::FOLLOW_USER, 
                    //FeedRepo::BOARD_UPDATE, 
                    FeedRepo::EPISODE_HEARD, 
                    FeedRepo::SUBSCRIBE_SHOW, 
                    FeedRepo::FOLLOW_BOARD, 
                    FeedRepo::FOLLOW_SMARTLIST,

                    FeedRepo::NEW_SHOW_POST, 
                    FeedRepo::NEW_EPISODE_POST, 
                    FeedRepo::NEW_PLAYLIST_POST, 
                    FeedRepo::NEW_SMARTPLAYLIST_POST, 
                    FeedRepo::NEW_COLLECTION_POST,

                    /** New Individual Share Post 21/05/2019 */
                    FeedRepo::INDIVIDUAL_POST,

                    // FeedRepo::SHOW_POST_LIKE,
                    // FeedRepo::EPISODE_POST_LIKE,
                    // FeedRepo::PLAYLIST_POST_LIKE,
                    // FeedRepo::SMARTPLAYLIST_POST_LIKE,
                    // FeedRepo::COLLECTION_POST_LIKE,

                    // FeedRepo::SHOW_POST_COMMENT,
                    // FeedRepo::EPISODE_POST_COMMENT,
                    // FeedRepo::PLAYLIST_POST_COMMENT,
                    // FeedRepo::SMARTPLAYLIST_POST_COMMENT,
                    // FeedRepo::COLLECTION_POST_COMMENT,

                    // FeedRepo::SHOW_POST_COMMENT_TAG,
                    // FeedRepo::EPISODE_POST_COMMENT_TAG,
                    // FeedRepo::PLAYLIST_POST_COMMENT_TAG,
                    // FeedRepo::SMARTPLAYLIST_POST_COMMENT_TAG,
                    // FeedRepo::COLLECTION_POST_COMMENT_TAG,
                ])
                ->where('typeable_id', $user->id);
        });

        $query->orWhere(function($q) use ($user) {            
            $q->where('typeable_type', 'App\\Models\\Board');
            $q->where('type', FeedRepo::BOARD_UPDATE);
            $q->where('owner_id', $user->id);            
        });

        $feed_items = $query->orderBy('updated_at', 'DESC')->paginate(10);
        
        $data = [];
        foreach ($feed_items as $feed_item) {
            if (!empty($this->feedMap[$feed_item->type])) {
                try {
                    if(with(new $this->feedMap[$feed_item->type]($feed_item, $user))->response()) {
                        $data[] = with(new $this->feedMap[$feed_item->type]($feed_item, $user))->response();
                    }
                } catch(\Exception $ex) {}
            }
        }
        
        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Feed.network_feed_not_found_lbl");
        }

        return response()->api([
            'status' => true,
            'data' => [
                'total' => $feed_items->total(),
                'per_page' => $feed_items->perPage(),
                'pages' => ceil($feed_items->total() / $feed_items->perPage()),
                'items' => $data,
                'message' => $message
            ]
        ]);
    }

    /**
     * This function is use for delete feed
     *
     * @param Request $request
     * @return void
     */
    public function delete_feed(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'feed_id' => 'required'
        ], [
            'feed_id.required' => config('language.' . $this->getLocale() . ".MyFeed.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $feed_data = Feed::where('id', $request->feed_id)->where('typeable_type', "App\\Models\\User")
            ->where('typeable_id', $user->id)->first();
        if (!$feed_data) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Feed.network_feed_not_found_lbl")
            ]);
        }
        
        if($feed_data->type == FeedRepo::FOLLOW_USER || $feed_data->type == FeedRepo::BOARD_UPDATE || $feed_data->type == FeedRepo::EPISODE_HEARD || $feed_data->type == FeedRepo::SUBSCRIBE_SHOW || $feed_data->type == FeedRepo::FOLLOW_BOARD || $feed_data->type == FeedRepo::FOLLOW_SMARTLIST) {
            $feed_data->delete();
        }
        else if($feed_data->type == FeedRepo::NEW_SHOW_POST || $feed_data->type == FeedRepo::NEW_EPISODE_POST || $feed_data->type == FeedRepo::NEW_PLAYLIST_POST || $feed_data->type == FeedRepo::NEW_SMARTPLAYLIST_POST || $feed_data->type == FeedRepo::NEW_COLLECTION_POST) {
            $content_id = json_decode($feed_data->data, true);
            $content_id = (int) $content_id[0];            
            $feed_type3 = "";
            $feed_type4 = "";
            $feed_type5 = "";
            if ($feed_data->type == FeedRepo::NEW_SHOW_POST) {
                $content = PostShow::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type3 = FeedRepo::SHOW_POST_LIKE;
                $feed_type4 = FeedRepo::SHOW_POST_COMMENT;
                $feed_type5 = FeedRepo::SHOW_POST_COMMENT_TAG;
            }
            else if ($feed_data->type == FeedRepo::NEW_EPISODE_POST) {
                $content = PostEpisode::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type3 = FeedRepo::EPISODE_POST_LIKE;
                $feed_type4 = FeedRepo::EPISODE_POST_COMMENT;
                $feed_type5 = FeedRepo::EPISODE_POST_COMMENT_TAG;
            }
            else if ($feed_data->type == FeedRepo::NEW_PLAYLIST_POST) {
                $content = PostPlaylist::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type3 = FeedRepo::PLAYLIST_POST_LIKE;
                $feed_type4 = FeedRepo::PLAYLIST_POST_COMMENT;
                $feed_type5 = FeedRepo::PLAYLIST_POST_COMMENT_TAG;            
            }
            else if ($feed_data->type == FeedRepo::NEW_SMARTPLAYLIST_POST) {
                $content = PostSmartPlaylist::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type3 = FeedRepo::SMARTPLAYLIST_POST_LIKE;
                $feed_type4 = FeedRepo::SMARTPLAYLIST_POST_COMMENT;
                $feed_type5 = FeedRepo::SMARTPLAYLIST_POST_COMMENT_TAG;
            }
            else if ($feed_data->type == FeedRepo::NEW_COLLECTION_POST) {
                $content = PostCollection::where('id', $content_id)->where('user_id', $user->id)->first();                
                $feed_type3 = FeedRepo::COLLECTION_POST_LIKE;
                $feed_type4 = FeedRepo::COLLECTION_POST_COMMENT;
                $feed_type5 = FeedRepo::COLLECTION_POST_COMMENT_TAG;
            }                        
            
            //Remove Likes from feed table
            if (!empty($feed_type3)) {
                $feedDataMain2 = Feed::where('type', $feed_type3)
                    ->where('data', 'LIKE', '%"#' . $content_id . '#"%');
                //dump($feedDataMain2->count());
                if ($feedDataMain2->count()) {
                    $feedDataMain2->delete();
                }
            }

            //Remove Comments from feed table
            if (!empty($feed_type4)) {
                $content_post = $content->comments;
                //dump($content_post->toArray());
                foreach($content_post as $commentItem) {
                    $feedDataMain3 = Feed::where('type', $feed_type4)
                        ->where('data', 'LIKE', '%"#' . $commentItem->id . '#"%');
                    //dump($feedDataMain3->count());
                    if ($feedDataMain3->count()) {
                        $feedDataMain3->delete();
                    }

                    $feedDataMain4 = Feed::where('type', $feed_type5)
                        ->where('data', 'LIKE', '%"#' . $commentItem->id . '#"%');
                    //dump('commentItem->id ==== '.$commentItem->id, $feedDataMain4->count());
                    if ($feedDataMain4->count()) {
                        $feedDataMain4->delete();
                    }
                }
            }
            
            //Remove Post from feed tables
            $feed_data->delete();
            
            //Delete Post from Respective tables
            $content->delete();
            
            //Update Post Comment Count
            $content->post_data->update(['post_count' => \DB::raw('post_count - 1')]);
        }
        else if($feed_data->type == FeedRepo::SHOW_POST_LIKE || $feed_data->type == FeedRepo::EPISODE_POST_LIKE || $feed_data->type == FeedRepo::PLAYLIST_POST_LIKE || $feed_data->type == FeedRepo::SMARTPLAYLIST_POST_LIKE || $feed_data->type == FeedRepo::COLLECTION_POST_LIKE) {
            $content_id = json_decode($feed_data->data, true);
            $content_id = $content_id[0];
            $content_id = trim($content_id, '#');
            if ($feed_data->type == FeedRepo::SHOW_POST_LIKE) {
                $content = PostShow::where('id', $content_id)->first();
            }
            else if ($feed_data->type == FeedRepo::EPISODE_POST_LIKE) {
                $content = PostEpisode::where('id', $content_id)->first();
            }
            else if ($feed_data->type == FeedRepo::PLAYLIST_POST_LIKE) {
                $content = PostPlaylist::where('id', $content_id)->first();
            }
            else if ($feed_data->type == FeedRepo::SMARTPLAYLIST_POST_LIKE) {
                $content = PostSmartPlaylist::where('id', $content_id)->first();
            }
            else if ($feed_data->type == FeedRepo::COLLECTION_POST_LIKE) {
                $content = PostCollection::where('id', $content_id)->first();
            }
            $like_info = $content->likes()->find($user->id, ['id']);
            if ($like_info) {
                //Update Post Like Count
                $content->update(['likes_count' => \DB::raw('likes_count - 1')]);

                //Remove from pivot table
                $content->likes()->detach($user->id);

                //Remove from feed tables
                $feed_data->delete();                
            }
        }
        else if($feed_data->type == FeedRepo::SHOW_POST_COMMENT || $feed_data->type == FeedRepo::EPISODE_POST_COMMENT || $feed_data->type == FeedRepo::PLAYLIST_POST_COMMENT || $feed_data->type == FeedRepo::SMARTPLAYLIST_POST_COMMENT || $feed_data->type == FeedRepo::COLLECTION_POST_COMMENT) {            
            $content_id = json_decode($feed_data->data, true);
            $content_id = $content_id[0];
            $content_id = trim($content_id, '#');
            $feed_type2 = "";
            if ($feed_data->type == FeedRepo::SHOW_POST_COMMENT) {
                $content = PostShowComment::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type2 = FeedRepo::SHOW_POST_COMMENT_TAG;           
            }
            else if ($feed_data->type == FeedRepo::EPISODE_POST_COMMENT) {
                $content = PostEpisodeComment::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type2 = FeedRepo::EPISODE_POST_COMMENT_TAG;
            }
            else if ($feed_data->type == FeedRepo::PLAYLIST_POST_COMMENT) {
                $content = PostPlaylistComment::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type2 = FeedRepo::PLAYLIST_POST_COMMENT_TAG;
            }
            else if ($feed_data->type == FeedRepo::SMARTPLAYLIST_POST_COMMENT) {
                $content = PostSmartPlaylistComment::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type2 = FeedRepo::SMARTPLAYLIST_POST_COMMENT_TAG;
            }
            else if ($feed_data->type == FeedRepo::COLLECTION_POST_COMMENT) {
                $content = PostCollectionComment::where('id', $content_id)->where('user_id', $user->id)->first();
                $feed_type2 = FeedRepo::COLLECTION_POST_COMMENT_TAG;
            }
            
            //Remove tags from feeds tables
            if (!empty($feed_type2)) {
                $feedData2 = Feed::where('type', $feed_type2)
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"#' . $content_id . '#"%');
                if ($feedData2->count()) {
                    $feedData2->delete();
                }
            }
            
            //Remove from feed tables
            $feed_data->delete();
            
            //Update Post Comment Count
            $content->post->update(['comment_count' => \DB::raw('comment_count - 1')]);

            //Delete Comment from Respective tables
            $content->delete();
        }
        
        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.remove_lbl")
        ]);
    }
}
