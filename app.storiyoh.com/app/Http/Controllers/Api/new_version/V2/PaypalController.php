<?php

namespace App\Http\Controllers\Api\new_version\V2;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\PremiumFeature;

use App\Traits\ResponseFormat;

use Carbon\Carbon;
use App\Models\PurchaseData;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\Details;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

class PaypalController extends Controller
{
    use ResponseFormat;

    /**
     *This function is use for create aproval url
     */
    public function paypal_approval_url_for_feature(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'premium_feature_id' => 'required',
            'platform' => 'required'
        ], [
            'premium_feature_id.required' => config('language.' . $this->getLocale() . ".Paypal.missing_parameter_id_lbl"),
            'platform.required' => config('language.' . $this->getLocale() . ".Paypal.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first(),
                'user_premium_feature_status' => 0,
                'data'    => ["approval_link" => ""]
            ]);
        }

        //Fetching data from tbl_premium_features table
        $premium_feature = PremiumFeature::whereId($request->premium_feature_id)
            ->first(['id', 'feature_name', 'feature_description' , 'price_in_usd']);
        
        //If we not found premium feature
        if (is_null($premium_feature)) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Paypal.premium_feature_not_found_lbl"),
                'user_premium_feature_status' => 0,
                'data'    => ["approval_link" => ""]
            ]);
            //return $this->sendEmptyResponse('No Premium Feature Found.');
        }

        //check if this premium already subscribed
        $checkExistingFeature = $user->premium_features()->find($premium_feature->id);
        if($checkExistingFeature) {
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Paypal.premium_feature_already_subscribed_lbl"),
                'user_premium_feature_status' => 1,
                'data'    => ["approval_link" => ""]
            ]);
        }

        $apiContext = new ApiContext(new OAuthTokenCredential(
            config("config.paypal.clientId"), 
            config("config.paypal.secretId")
        ));        

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item1 = new Item();
        $item1->setName($premium_feature->feature_name)->setCurrency('USD')->setQuantity(1)
            //->setSku("aaa") // Similar to `item_number` in Classic API
            ->setPrice($premium_feature->price_in_usd);
        
        $itemList = new ItemList();
        $itemList->setItems([$item1]);

        $details = new Details();
        $details->setSubtotal($premium_feature->price_in_usd);

        $amount = new Amount();
        $amount->setCurrency("USD")->setTotal($premium_feature->price_in_usd)->setDetails($details);

        $transaction = new Transaction();        

        //Create Invoice Id
        $maxId = \DB::table('purchase_data')->max('id');
        $maxPurchaseId = 0;
        if(! is_null($maxId)) {
            $maxPurchaseId = $maxId;
        }
        $maxPurchaseId = $maxPurchaseId + 1;
        $invoice_no = "STR".date("ymdHi").$maxPurchaseId;

        $transaction->setAmount($amount)->setItemList($itemList)
            ->setDescription($premium_feature->feature_description)->setInvoiceNumber($invoice_no);
        
        //Redirect urls
        //Set the urls that the buyer must be redirected to after payment approval/ cancellation.            
        $baseUrl = url('/');
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("$baseUrl/".config("config.paypal.returnUrlSuccess"))
                ->setCancelUrl("$baseUrl/".config("config.paypal.returnUrlFailed"));
        //Payment
        //A Payment Resource; create one using the above types and intent set to 'sale'
            
        $payment = new Payment();
        $payment->setIntent("sale")->setPayer($payer)
            ->setRedirectUrls($redirectUrls)->setTransactions(array($transaction));
        
        //$request = clone $payment;
        //Create Payment
        //Create a payment by calling the 'create' method passing it a valid apiContext. (See bootstrap.php for more on ApiContext) The return object contains the state and the url to which the buyer must be redirected to for payment approval            
        try {
            $payment->create($apiContext);            

            //Insert data into our tables tbl_purchase_data and tbl_premium_feature_user            
            $insertArray = [
                "invoice_number" => $invoice_no,
                "user_id" => $user->id,
                "purchase_type" => 1,
                "feature_id" => $premium_feature->id,
                "gateway" => "international_pg",
                "vendor" => "paypal",
                "payment_mode" => "web",
                "transaction_id" => $payment->id,
                "billing_type" => "one_time",
                "currency" => "USD",
                "invoice_amount" => $premium_feature->price_in_usd,
                "status" => "initiated",
                "additional_status" => "-",
                "token" => $payment->getToken(),
                "platform" => $request->platform
            ];
            
            PurchaseData::create($insertArray);

            return response()->api([
                'status' => true,
                'message' => "", 
                'user_premium_feature_status' => 0,
                'data'    => [
                    'approval_link' => $payment->getApprovalLink()
                ]
            ]);

            //return redirect($payment->getApprovalLink());
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            //echo $ex->getData();
            return response()->api([
                'status' => false,
                'message' => $ex->getData(),
                'user_premium_feature_status' => 0,
                'data'    => ["approval_link" => ""]
            ]);            
        }
    }

    /**
     * This function is use for execute paypal payment
     */
    public function execute_feature_success(Request $request)
    {        
        $apiContext = new ApiContext(new OAuthTokenCredential(
            config("config.paypal.clientId"), 
            config("config.paypal.secretId")
        ));

        $paymentId = $request->paymentId;
        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($request->PayerID);

        $transaction = new Transaction();
        $amount = new Amount();
        $details = new Details();

        $trns = $payment->getTransactions();

        //update tbl_purchase_data with unique invoice id
        $premium_data = PurchaseData::where('invoice_number', $trns[0]->invoice_number)->where('token', $request->token)->first();
        if(! $premium_data) {
            ob_flush();
            $url = "Location:com.storiyoh.android.paypalcustomtab:failed/1";
            header($url, 302);
            exit;
        }

        $details->setSubtotal($trns[0]->amount->total);

        $amount->setCurrency($trns[0]->amount->currency);
        $amount->setTotal($trns[0]->amount->total);
        $amount->setDetails($details);
        
        $transaction->setAmount($amount);

        $execution->addTransaction($transaction);

        try {
            $result = $payment->execute($execution, $apiContext);

            //Insert data into our tables tbl_premium_feature_user and update tbl_purchase_data
            if($result->state == "approved") {                
                $premium_data->fill([
                    "invoice_date" => Carbon::now(),                    
                    "status" => "success",
                    "additional_status" => $result->getPayer()->status,
                    "token" => null,
                    "response" => $result
                ])->save();
                
                $userData = User::find($premium_data->user_id);
                $feature_user = $userData->premium_features()->find($premium_data->feature_id);
                if(is_null($feature_user)) {
                    $userData->premium_features()->attach($premium_data->feature_id, ["premium_feature_start_date" => Carbon::now()]);
                }                
            } else {
                //update tbl_purchase_data with unique invoice id
                $premium_data->fill([
                    "invoice_date" => Carbon::now(),
                    "status" => $result->state,
                    "additional_status" => $result->getPayer()->status,
                    "token" => null,
                    "response" => $result,
                ])->save();
            }
            ob_flush();
            $url = "Location:com.storiyoh.android.paypalcustomtab:success"."/".$premium_data->feature_id;
            header($url, 302);
            exit;            
        } catch(\Exception $ex) {
            ob_flush();
            $url = "Location:com.storiyoh.android.paypalcustomtab:failed/2";
            header($url, 302);
            exit;            
        }        
    }

    /**
     * This function is use for cancel paypal request
     */
    public function execute_feature_failed(Request $request)
    {
        $premium_data = PurchaseData::where('token', $request->token)->first();
        if($premium_data) {
            if ($premium_data->status == "initiated") {
                $premium_data->fill([
                "invoice_date" => Carbon::now(),
                "status" => "cancelled",
                "token" => null
            ])->save();
            }
        }
        ob_flush();
        $url = "Location:com.storiyoh.android.paypalcustomtab:failed/3";;
        header($url, 302);
        exit;
    }
}
