<?php

namespace App\Http\Controllers\Api\new_version\V3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\ClassesV3\Feed;
use App\Models\Show;
use App\Models\Episode;
use App\Models\Board;
use App\Models\SmartPlaylist;
use App\Models\Chart;
use App\Models\User;
use App\Models\PostShow;
use App\Models\PostEpisode;
use App\Models\PostPlaylist;
use App\Models\PostSmartPlaylist;
use App\Models\PostCollection;
use App\Models\PostShowComment;
use App\Models\PostEpisodeComment;
use App\Models\PostPlaylistComment;
use App\Models\PostSmartPlaylistComment;
use App\Models\PostCollectionComment;
use App\Models\Feed as FeedRepo;
use App\Traits\Helper;
use App\Jobs\IndividualPushNotification;

use App\Traits\ResponseFormat;
use App\Models\PostComment;
use App\Models\Post;

class PostController extends Controller
{
    use ResponseFormat;

    /**
     * This function is use for all types added post
     *
     * @param Request $request
     * @return void
     */
    public function share_post_added(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            //'post' => 'required',
            'type' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            //'post.required' => "Please provide post description.",
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $feed_type = '';
        if ($request->type == 'Show') {
            $show = Show::published()->where('id', $request->content_id)->first(['id']);

            if (!$show) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'show_id' => $show->id,
                    'post' => $request->post,
                    'status' => 'Published'
                ];

                //Create Board
                $post = PostShow::create($attributes);

                //Update Post Count
                Show::where('id', $request->content_id)->update(['post_count' => \DB::raw('post_count + 1')]);

                $feed_type = Feed::NEW_SHOW_POST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        } elseif ($request->type == 'Episode') {
            $episode = Episode::published()->where('id', $request->content_id)->first(['id']);

            if (!$episode) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'episode_id' => $episode->id,
                    'post' => $request->post,
                    'status' => 'Published'
                ];

                //Create Post for Episode
                $post = PostEpisode::create($attributes);

                //Update Post Count
                Episode::where('id', $request->content_id)->update(['post_count' => \DB::raw('post_count + 1')]);

                $feed_type = Feed::NEW_EPISODE_POST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        } elseif ($request->type == 'Playlist') {
            $playlist = Board::where('id', $request->content_id)->first(['id']);

            if (!$playlist) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'playlist_id' => $playlist->id,
                    'post' => $request->post,
                    'status' => 'Published'
                ];

                //Create Post for Playlist
                $post = PostPlaylist::create($attributes);

                //Update Post Count
                Board::where('id', $request->content_id)->update(['post_count' => \DB::raw('post_count + 1')]);

                $feed_type = Feed::NEW_PLAYLIST_POST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        } elseif ($request->type == 'Smart_Playlist') {
            $smart_playlist = SmartPlaylist::where('id', $request->content_id)->first(['id']);

            if (!$smart_playlist) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'smart_playlist_id' => $smart_playlist->id,
                    'post' => $request->post,
                    'status' => 'Published'
                ];

                //Create Post for Smart Playlist
                $post = PostSmartPlaylist::create($attributes);

                //Update Post Count
                SmartPlaylist::where('id', $request->content_id)->update(['post_count' => \DB::raw('post_count + 1')]);

                $feed_type = Feed::NEW_SMARTPLAYLIST_POST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        } elseif ($request->type == 'Collection') {
            $collection = Chart::published()->where('id', $request->content_id)->first(['id']);

            if (!$collection) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_collection_found_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'collection_id' => $collection->id,
                    'post' => $request->post,
                    'status' => 'Published'
                ];

                //Create Post for Collection
                $post = PostCollection::create($attributes);

                //Update Post Count
                Chart::where('id', $request->content_id)->update(['post_count' => \DB::raw('post_count + 1')]);

                $feed_type = Feed::NEW_COLLECTION_POST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        }

        //Insert into Feeds Table
        if (!empty($feed_type)) {
            (new Feed($user))->add(trim($post->id), $feed_type);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Post.success_lbl")
        ]);
    }

    /**
     * This function is use for all types repost
     *
     * @param Request $request
     * @return void
     */
    public function share_post_repost(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $feed_type = '';
        if ($request->type == 'Show') {
            $show = PostShow::find($request->content_id, ['id', 'user_id', 'show_id', 'post']);

            if (!$show) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl")
                ]);
            }

            if ($show->user_id == $user->id) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Post.post_repost_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'show_id' => $show->show_id,
                    'post_id' => $request->content_id,
                    'post' => $show->post,
                    'status' => 'Published'
                ];

                //Create Board
                $post = PostShow::create($attributes);

                //Update Repost Count
                $show->update(['repost_count' => \DB::raw('repost_count + 1')]);

                //Update Repost Count in Show Table
                Show::where('id', $show->show_id)->update(['repost_count' => \DB::raw('repost_count + 1')]);

                $feed_type = Feed::SHOW_REPOST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        } elseif ($request->type == 'Episode') {
            $episode = PostEpisode::find($request->content_id, ['id', 'user_id', 'episode_id', 'post']);

            if (!$episode) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl")
                ]);
            }

            if ($episode->user_id == $user->id) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Post.post_repost_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'episode_id' => $episode->episode_id,
                    'post_id' => $request->content_id,
                    'post' => $episode->post,
                    'status' => 'Published'
                ];

                //Create Post for Episode
                $post = PostEpisode::create($attributes);

                //Update Repost Count
                $episode->update(['repost_count' => \DB::raw('repost_count + 1')]);

                //Update Repost Count in Episode Table
                Episode::where('id', $episode->episode_id)->update(['repost_count' => \DB::raw('repost_count + 1')]);

                $feed_type = Feed::EPISODE_REPOST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        } elseif ($request->type == 'Playlist') {
            $playlist = PostPlaylist::find($request->content_id, ['id', 'user_id', 'playlist_id', 'post']);

            if (!$playlist) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl")
                ]);
            }

            if ($playlist->user_id == $user->id) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Post.post_repost_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'playlist_id' => $playlist->playlist_id,
                    'post_id' => $request->content_id,
                    'post' => $playlist->post,
                    'status' => 'Published'
                ];

                //Create Post for Playlist
                $post = PostPlaylist::create($attributes);

                //Update Repost Count
                $playlist->update(['repost_count' => \DB::raw('repost_count + 1')]);

                //Update Repost Count in Playlist Table
                Board::where('id', $playlist->playlist_id)->update(['repost_count' => \DB::raw('repost_count + 1')]);

                $feed_type = Feed::PLAYLIST_REPOST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        } elseif ($request->type == 'Smart_Playlist') {
            $smart_playlist = PostSmartPlaylist::find($request->content_id, ['id', 'user_id', 'smart_playlist_id', 'post']);

            if (!$smart_playlist) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
                ]);
            }

            if ($smart_playlist->user_id == $user->id) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Post.post_repost_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'smart_playlist_id' => $smart_playlist->smart_playlist_id,
                    'post_id' => $request->content_id,
                    'post' => $smart_playlist->post,
                    'status' => 'Published'
                ];

                //Create Post for Smart Playlist
                $post = PostSmartPlaylist::create($attributes);

                //Update Repost Count
                $smart_playlist->update(['repost_count' => \DB::raw('repost_count + 1')]);

                //Update Repost Count in Smart Playlist Table
                SmartPlaylist::where('id', $smart_playlist->smart_playlist_id)->update(['repost_count' => \DB::raw('repost_count + 1')]);

                $feed_type = Feed::SMARTPLAYLIST_REPOST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        } elseif ($request->type == 'Collection') {
            $collection = PostCollection::find($request->content_id, ['id', 'user_id', 'collection_id', 'post']);

            if (!$collection) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_collection_found_lbl")
                ]);
            }

            if ($collection->user_id == $user->id) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Post.post_repost_lbl")
                ]);
            }

            try {
                $attributes = [
                    'user_id' => $user->id,
                    'collection_id' => $collection->collection_id,
                    'post_id' => $request->content_id,
                    'post' => $collection->post,
                    'status' => 'Published'
                ];

                //Create Post for Collection
                $post = PostCollection::create($attributes);

                //Update Repost Count
                $collection->update(['repost_count' => \DB::raw('repost_count + 1')]);

                //Update Repost Count in Chart Table
                Chart::where('id', $collection->collection_id)->update(['repost_count' => \DB::raw('repost_count + 1')]);

                $feed_type = Feed::COLLECTION_REPOST;
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => ''
                ]);
            }
        }

        //Insert into Feeds Table
        if (!empty($feed_type)) {
            //(new Feed($user))->add(trim($post->id), $feed_type);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Post.success_lbl")
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function share_post_delete(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $feed_type = '';
        $feed_type2 = '';
        $feed_type3 = '';
        $feed_type4 = '';
        $feed_type5 = '';
        if ($request->type == 'Show') {
            $content = PostShow::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::NEW_SHOW_POST;
            $feed_type2 = Feed::SHOW_REPOST;
            $feed_type3 = Feed::SHOW_POST_LIKE;
            $feed_type4 = Feed::SHOW_POST_COMMENT;
            $feed_type5 = Feed::SHOW_POST_COMMENT_TAG;
        } elseif ($request->type == 'Episode') {
            $content = PostEpisode::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::NEW_EPISODE_POST;
            $feed_type2 = Feed::EPISODE_REPOST;
            $feed_type3 = Feed::EPISODE_POST_LIKE;
            $feed_type4 = Feed::EPISODE_POST_COMMENT;
            $feed_type5 = Feed::EPISODE_POST_COMMENT_TAG;
        } elseif ($request->type == 'Playlist') {
            $content = PostPlaylist::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::NEW_PLAYLIST_POST;
            $feed_type2 = Feed::PLAYLIST_REPOST;
            $feed_type3 = Feed::PLAYLIST_POST_LIKE;
            $feed_type4 = Feed::PLAYLIST_POST_COMMENT;
            $feed_type5 = Feed::PLAYLIST_POST_COMMENT_TAG;
        } elseif ($request->type == 'Smart_Playlist') {
            $content = PostSmartPlaylist::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::NEW_SMARTPLAYLIST_POST;
            $feed_type2 = Feed::SMARTPLAYLIST_REPOST;
            $feed_type3 = Feed::SMARTPLAYLIST_POST_LIKE;
            $feed_type4 = Feed::SMARTPLAYLIST_POST_COMMENT;
            $feed_type5 = Feed::SMARTPLAYLIST_POST_COMMENT_TAG;
        } elseif ($request->type == 'Collection') {
            $content = PostCollection::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::NEW_COLLECTION_POST;
            $feed_type2 = Feed::COLLECTION_REPOST;
            $feed_type3 = Feed::COLLECTION_POST_LIKE;
            $feed_type4 = Feed::COLLECTION_POST_COMMENT;
            $feed_type5 = Feed::COLLECTION_POST_COMMENT_TAG;
        }

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_content_found_lbl")
            ]);
        }

        try {
            //also delete repost from table and update repost_count from respactive table
            /*$count = $content->repost_data->count();
            if ($count > 0) {
                //Remove repost from feeds
                foreach ($content->repost_data as $repostItem) {
                    //Remove Repost Feed from feed table
                    $feedDataRepost1 = FeedRepo::where('type', $feed_type2)
                        ->where('data', 'LIKE', '%"' . $repostItem->id . '"%');
                    if ($feedDataRepost1->count()) {
                        $feedDataRepost1->delete();
                    }

                    //Remove Like Feed from feed table
                    $feedDataRepost2 = FeedRepo::where('type', $feed_type3)
                        ->where('data', 'LIKE', '%"' . $repostItem->id . '"%');
                    if ($feedDataRepost2->count()) {
                        $feedDataRepost2->delete();
                    }

                    //Remove Comment Feed from feed table
                    $feedDataRepost3 = FeedRepo::where('type', $feed_type4)
                        ->where('data', 'LIKE', '%"#' . $repostItem->id . '#"%');
                    if ($feedDataRepost3->count()) {
                        $feedDataRepost3->delete();
                    }
                }

                //delete repost from tables
                $content->repost_data()->delete();

                //Update Repost Comment Count
                $content->post_data->update(['repost_count' => \DB::raw('repost_count - '.$count)]);
            }*/

            //Remove post from feed tables
            if (!empty($feed_type)) {
                $feedDataMain = FeedRepo::where('type', $feed_type)
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"' . $request->content_id . '"%');
                if ($feedDataMain->count()) {
                    $feedDataMain->delete();
                }
            }

            //Remove Likes from feed table
            if (!empty($feed_type3)) {
                $feedDataMain2 = FeedRepo::where('type', $feed_type3)
                    ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                if ($feedDataMain2->count()) {
                    $feedDataMain2->delete();
                }
            }

            //Remove Comments from feed table
            if (!empty($feed_type4)) {
                $content_post = $content->comments;
                foreach ($content_post as $commentItem) {
                    $feedDataMain3 = FeedRepo::where('type', $feed_type4)
                        ->where('data', 'LIKE', '%"#' . $commentItem->id . '#"%');
                    if ($feedDataMain3->count()) {
                        $feedDataMain3->delete();
                    }

                    $feedDataMain4 = FeedRepo::where('type', $feed_type5)
                        ->where('data', 'LIKE', '%"#' . $commentItem->id . '#"%');
                    if ($feedDataMain4->count()) {
                        $feedDataMain4->delete();
                    }
                }
            }

            //Delete Comment from Respective tables
            $content->delete();

            //Update Post Comment Count
            $content->post_data->update(['post_count' => \DB::raw('post_count - 1')]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => $ex->getMessage()
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.remove_lbl")
        ]);
    }

    /**
     * This function is use for like all type of post
     *
     * @param Request $request
     * @return void
     */
    public function share_post_like(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $feed_type = '';

        if ($request->type == 'Show') {
            $content = PostShow::find($request->content_id, ['id', 'user_id', 'likes_count']);
            $feed_type = Feed::SHOW_POST_LIKE;
        } elseif ($request->type == 'Episode') {
            $content = PostEpisode::find($request->content_id, ['id', 'user_id', 'likes_count']);
            $feed_type = Feed::EPISODE_POST_LIKE;
        } elseif ($request->type == 'Playlist') {
            $content = PostPlaylist::find($request->content_id, ['id', 'user_id', 'likes_count']);
            $feed_type = Feed::PLAYLIST_POST_LIKE;
        } elseif ($request->type == 'Smart_Playlist') {
            $content = PostSmartPlaylist::find($request->content_id, ['id', 'user_id', 'likes_count']);
            $feed_type = Feed::SMARTPLAYLIST_POST_LIKE;
        } elseif ($request->type == 'Collection') {
            $content = PostCollection::find($request->content_id, ['id', 'user_id', 'likes_count']);
            $feed_type = Feed::COLLECTION_POST_LIKE;
        }

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_content_found_lbl")
            ]);
        }

        // if ($content->user_id == $user->id) {
        //     return response()->api([
        //         'status' => false,
        //         'message' => 'You can not like your own post.'
        //     ]);
        // }

        try {
            $like_info = $content->likes()->find($user->id, ['id']);

            if ($like_info) {
                //Update Post Like Count
                $content->update(['likes_count' => $content->likes_count - 1]);

                //Remove from Feeds table
                if (!empty($feed_type)) {
                    $feedData = FeedRepo::where('type', $feed_type)
                        ->where('typeable_type', 'App\\Models\\User')
                        ->where('typeable_id', $user->id)
                        ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                    if ($feedData->count()) {
                        $feedData->delete();
                    }
                }

                $content->likes()->detach($user->id);

                return response()->api([
                    'status' => true,
                    'message' => 'Like Remove!',
                    'flag' => 0,
                    'no_of_likes' => Helper::shorten_count($content->likes_count)
                ]);
            } else {
                $content->likes()->attach($user->id);

                //Update Post Like Count
                $content->update(['likes_count' => $content->likes_count + 1]);

                //Insert into Feeds Table
                if (!empty($feed_type)) {
                    $dataArray = ['#' . $request->content_id . '#', '*' . $content->user_id . '*'];
                    (new Feed($user))->add($dataArray, $feed_type);
                    //(new Feed($user))->add(trim($request->content_id), $feed_type);
                }

                //Send Push Notification
                if($content->user_id != $user->id) {
                    $tokens = User::find($content->user_id)->pushIds()->where('push_ids.status', 1)->pluck('token')->all();
                    if (count($tokens) > 0) {
                        $message = $user->notification_format_name . ' liked your post';
                        $title = $message;
                        $description = '';
                        $icon = '';
                        $data = [
                            'title' => $message,
                            'username' => $user->username,
                            'post_id' => $request->content_id,
                            'post_type' => $request->type,
                            'type' => 'POSTLIKE',
                            'image' => !empty($user->image) ? $user->getImage(200) : '',
                            'mediaUrl' => !empty($user->image) ? $user->getImage(200) : '',
                            'desc' => '',
                            'tray_icon' => ''
                        ];

                        dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
                    }
                }

                return response()->api([
                    'status' => true,
                    'message' => config('language.' . $this->getLocale() . ".Post.post_liked_lbl"),
                    'flag' => 1,
                    'no_of_likes' => Helper::shorten_count($content->likes_count)
                ]);
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => $ex->getMessage()
            ]);
        }
    }

    /**
     * This function is use for unlike all type of post
     *
     * @param Request $request
     * @return void
     */
    public function share_post_unlike(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $feed_type = '';

        if ($request->type == 'Show') {
            $content = PostShow::find($request->content_id, ['id', 'user_id']);
            $feed_type = Feed::SHOW_POST_LIKE;
        } elseif ($request->type == 'Episode') {
            $content = PostEpisode::find($request->content_id, ['id', 'user_id']);
            $feed_type = Feed::EPISODE_POST_LIKE;
        } elseif ($request->type == 'Playlist') {
            $content = PostPlaylist::find($request->content_id, ['id', 'user_id']);
            $feed_type = Feed::PLAYLIST_POST_LIKE;
        } elseif ($request->type == 'Smart_Playlist') {
            $content = PostSmartPlaylist::find($request->content_id, ['id', 'user_id']);
            $feed_type = Feed::SMARTPLAYLIST_POST_LIKE;
        } elseif ($request->type == 'Collection') {
            $content = PostCollection::find($request->content_id, ['id', 'user_id']);
            $feed_type = Feed::COLLECTION_POST_LIKE;
        }

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_content_found_lbl")
            ]);
        }

        try {
            $like_info = $content->likes()->find($user->id, ['id']);

            if (!$like_info) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Post.post_unlike_alredy_lbl")
                ]);
            }

            $content->likes()->detach($user->id);

            //Update Post Like Count
            $content->update(['likes_count' => \DB::raw('likes_count - 1')]);

            //Remove from Feeds table
            if (!empty($feed_type)) {
                $feedData = FeedRepo::where('type', $feed_type)
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"' . $request->content_id . '"%');
                if ($feedData->count()) {
                    $feedData->delete();
                }
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => ''
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Post.post_unliked_lbl")
        ]);
    }

    /**
     * List all post comments
     *
     * @param Request $request
     * @return void
     */
    public function share_post_comments(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $contents = [];
        if ($request->type == 'Show') {
            $contents = PostShowComment::where('post_show_id', $request->content_id)
                ->where('status', 'Published')->orderBy('updated_at', 'DESC')->paginate(10, ['id', 'user_id', 'comment', 'tagIds', 'created_at']);
        } elseif ($request->type == 'Episode') {
            $contents = PostEpisodeComment::where('post_episode_id', $request->content_id)
                ->where('status', 'Published')->orderBy('updated_at', 'DESC')->paginate(10, ['id', 'user_id', 'comment', 'tagIds', 'created_at']);
        } elseif ($request->type == 'Playlist') {
            $contents = PostPlaylistComment::where('post_playlist_id', $request->content_id)
                ->where('status', 'Published')->orderBy('updated_at', 'DESC')->paginate(10, ['id', 'user_id', 'comment', 'tagIds', 'created_at']);
        } elseif ($request->type == 'Smart_Playlist') {
            $contents = PostSmartPlaylistComment::where('post_smart_playlist_id', $request->content_id)
                ->where('status', 'Published')->orderBy('updated_at', 'DESC')->paginate(10, ['id', 'user_id', 'comment', 'tagIds', 'created_at']);
        } elseif ($request->type == 'Collection') {
            $contents = PostCollectionComment::where('post_collection_id', $request->content_id)
                ->where('status', 'Published')->orderBy('updated_at', 'DESC')->paginate(10, ['id', 'user_id', 'comment', 'tagIds', 'created_at']);
        }

        $data = [];        
        foreach ($contents as $comment) {
            //tag code start
            $tagUserData = [];
            if(! is_null($comment->tagIds)) {
                $tagData = User::whereIn('id', json_decode($comment->tagIds))->get(['id', 'username', 'first_name', 'image']);
                foreach($tagData as $item) {
                    $tagUserData[] = [
                        'id' => $item->id, 
                        'username' => $item->username, 
                        'name' => $item->first_name, 
                        'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/user.png')
                    ];
                }            
            }
            $data[] = [
                'id' => $comment->id,
                'post_id' => $request->content_id,
                'post_type' => $request->type,
                'user' => $comment->user->full_name,
                'user_photo' => !empty($comment->user->image) ? $comment->user->getImage(100) : asset('uploads/default/user.png'),
                'username' => $comment->user->username,
                'comment' => $comment->comment,
                'tagUserData' => $tagUserData,
                'comment_time' => $comment->created_at->diffForHumans(),
                'owner' => ($comment->user_id == $user->id) ? 1 : 0
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Post.post_no_comment_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $contents->total(),
                'per_page' => $contents->perPage(),
                'pages' => ceil($contents->total() / $contents->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for adding comment
     *
     * @param Request $request
     * @return void
     */
    public function share_post_comment_added(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required',
            'comment' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl"),
            'comment.required' => config('language.' . $this->getLocale() . ".Post.share_post_comment_added_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $feed_type = '';
        $feed_type2 = '';
        if ($request->type == 'Show') {
            $content = PostShow::find($request->content_id, ['id', 'user_id', 'comment_count']);
            $feed_type = Feed::SHOW_POST_COMMENT;
            $feed_type2 = Feed::SHOW_POST_COMMENT_TAG;
        } elseif ($request->type == 'Episode') {
            $content = PostEpisode::find($request->content_id, ['id', 'user_id', 'comment_count']);
            $feed_type = Feed::EPISODE_POST_COMMENT;
            $feed_type2 = Feed::EPISODE_POST_COMMENT_TAG;
        } elseif ($request->type == 'Playlist') {
            $content = PostPlaylist::find($request->content_id, ['id', 'user_id', 'comment_count']);
            $feed_type = Feed::PLAYLIST_POST_COMMENT;
            $feed_type2 = Feed::PLAYLIST_POST_COMMENT_TAG;
        } elseif ($request->type == 'Smart_Playlist') {
            $content = PostSmartPlaylist::find($request->content_id, ['id', 'user_id', 'comment_count']);
            $feed_type = Feed::SMARTPLAYLIST_POST_COMMENT;
            $feed_type2 = Feed::SMARTPLAYLIST_POST_COMMENT_TAG;
        } elseif ($request->type == 'Collection') {
            $content = PostCollection::find($request->content_id, ['id', 'user_id', 'comment_count']);
            $feed_type = Feed::COLLECTION_POST_COMMENT;
            $feed_type2 = Feed::COLLECTION_POST_COMMENT_TAG;
        }

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Post.post_no_comment_found_lbl")
            ]);
        }

        // if (($content->user_id == $user->id) && empty($request->post_id)) {
        //     return response()->api([
        //         'status' => false,
        //         'message' => 'You can not comment your own post.'
        //     ]);
        // }

        //dd($content->toArray());
        try {
            $attributes = [
                'user_id' => $user->id,
                'comment' => $request->comment,
                'status' => 'Published',
                'reply_post_id' => !empty($request->post_id) ? $request->post_id : 0
            ];
            $commentData = $content->comments()->create($attributes);

            //Update Post Comment Count
            $content->update(['comment_count' => $content->comment_count + 1]);

            //Insert into Feeds Table
            if (!empty($feed_type) && ($content->user_id != $user->id)) {
                $dataArray = ['#' . $commentData->id . '#', '*' . $content->user_id . '*'];
                (new Feed($user))->add($dataArray, $feed_type);
            }

            //Send Push Notification
            $tokens = User::find($content->user_id)->pushIds()->where('push_ids.status', 1)->pluck('token')->all();
            if (count($tokens) > 0 && ($content->user_id != $user->id)) {
                $message = $user->notification_format_name . ' commented on your post';
                $title = $message;
                $description = '';
                $icon = '';
                $data = [
                    'title' => $message,
                    'username' => $user->username,
                    'post_id' => $request->content_id,
                    'comment_id' => $commentData->id,
                    'post_type' => $request->type,
                    'type' => 'POSTCOMMENT',
                    'desc' => '',
                    'tray_icon' => ''
                ];
                dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
            }

            $merge_array = [];
            if (!empty($request->tag_id)) {
                $usernames = explode(',', trim($request->tag_id));
                $usernames = array_filter($usernames);

                $userArray = [];
                $pushArray = [];
                foreach ($usernames as $username) {
                    $userTagData = User::where('username', $username)->first();
                    $userArray[] = $userTagData->id;
                    $pushArray[] = $userTagData;
                }

                if (count($userArray) > 0) {
                    $user_data = array_unique($userArray);
                    $feed_data = [];
                    foreach ($user_data as $i) {
                        $feed_data[] = '*' . $i . '*';
                    }

                    //Added into feed table
                    if (count($feed_data) > 0) {
                        $merge_array = $feed_data;
                    }

                    $comment_id = $commentData->id;
                    $ep_id = $request->content_id;
                    $message = '';

                    foreach ($pushArray as $data) {
                        $user_push = $data;

                        if ($user_push) {
                            $message = $user->notification_format_name . ' tagged you in the post';
                            $description = '';
                            $icon = '';
                            $title = $message;
                            $data = [
                                'title' => $message,
                                'username' => $user_push->username,
                                'post_id' => $request->content_id,
                                'comment_id' => $commentData->id,
                                'post_type' => $request->type,
                                'type' => 'POSTTAGCOMMENT',
                                'desc' => '',
                                'tray_icon' => ''
                            ];

                            $tokens = $user_push->pushIds()->where('push_ids.status', 1)->get(['token', 'platform']);
                            if ($tokens) {
                                $tokenData['android'] = [];
                                $tokenData['iphone'] = [];
                                foreach ($tokens as $tokenItem) {
                                    $tokenData[$tokenItem->platform][] = $tokenItem->token;
                                }
                                $tokenData['android'] = array_filter(array_flatten($tokenData['android']));
                                $tokenData['iphone'] = array_filter(array_flatten($tokenData['iphone']));
                                if (count($tokenData['android']) > 0) {
                                    dispatch(new IndividualPushNotification($tokenData['android'], $title, $description, $icon, $data))->onQueue('single_push_notification');
                                }
                                if (count($tokenData['iphone']) > 0) {
                                    dispatch(new IndividualPushNotification($tokenData['iphone'], $title, $description, $icon, $data))->onQueue('single_push_notification');
                                }
                            }
                        }
                    }

                    //Insert into Feeds Table for Tags
                    if (!empty($feed_type2)) {
                        $dataArray2 = array_merge(['#' . $commentData->id . '#'], $merge_array);
                        (new Feed($user))->add($dataArray2, $feed_type2);
                    }

                    //update tag id in tbl_posts table
                    if (count($userArray) > 0) {
                        $commentData->update(["tagIds" => json_encode($userArray)]);
                    }
                }
            }

            //$data = $content->comment_count;
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Post.comment_success_lbl"),
                'no_of_comments' => $content->comment_count
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => $ex->getMessage()
            ]);
        }
    }

    /**
     * This function is use for delete post comment
     *
     * @param Request $request
     * @return void
     */
    public function share_post_comment_delete(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_comment_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $feed_type = '';
        $feed_type2 = '';
        if ($request->type == 'Show') {
            $content = PostShowComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::SHOW_POST_COMMENT;
            $feed_type2 = Feed::SHOW_POST_COMMENT_TAG;
        } elseif ($request->type == 'Episode') {
            $content = PostEpisodeComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::EPISODE_POST_COMMENT;
            $feed_type2 = Feed::EPISODE_POST_COMMENT_TAG;
        } elseif ($request->type == 'Playlist') {
            $content = PostPlaylistComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::PLAYLIST_POST_COMMENT;
            $feed_type2 = Feed::PLAYLIST_POST_COMMENT_TAG;
        } elseif ($request->type == 'Smart_Playlist') {
            $content = PostSmartPlaylistComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::SMARTPLAYLIST_POST_COMMENT;
            $feed_type2 = Feed::SMARTPLAYLIST_POST_COMMENT_TAG;
        } elseif ($request->type == 'Collection') {
            $content = PostCollectionComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
            $feed_type = Feed::COLLECTION_POST_COMMENT;
            $feed_type2 = Feed::COLLECTION_POST_COMMENT_TAG;
        }

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Post.post_no_comment_found_lbl")
            ]);
        }

        try {
            //Remove from feed tables
            if (!empty($feed_type)) {
                $feedData = FeedRepo::where('type', $feed_type)
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                if ($feedData->count()) {
                    $feedData->delete();
                }
            }

            //Remove tags from user_notifications tables
            if (!empty($feed_type2)) {
                $feedData2 = FeedRepo::where('type', $feed_type2)
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                if ($feedData2->count()) {
                    $feedData2->delete();
                }
            }

            //Delete Comment from Respective tables
            $content->delete();

            //Update Post Comment Count
            $content->post->update(['comment_count' => \DB::raw('comment_count - 1')]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => $ex->getMessage()
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.remove_lbl")
        ]);
    }

    /**
     * This function is use for specific comment info
     *
     * @param Request $request
     * @return void
     */
    public function share_post_comment_details(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_comment_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        if ($request->type == 'Show') {
            $content = PostShowComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
        } elseif ($request->type == 'Episode') {
            $content = PostEpisodeComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
        } elseif ($request->type == 'Playlist') {
            $content = PostPlaylistComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
        } elseif ($request->type == 'Smart_Playlist') {
            $content = PostSmartPlaylistComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
        } elseif ($request->type == 'Collection') {
            $content = PostCollectionComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
        }

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Post.post_no_comment_found_lbl")
            ]);
        }

        $data = [
            'id' => $content->id,
            'post_id' => $content->post_show_id,
            'post_type' => $request->type,
            'comment' => $content->comment,
            'comment_time' => $content->created_at->diffForHumans(),
            'owner' => ($content->user_id == $user->id) ? 1 : 0
        ];

        return response()->api([
            'status' => true,
            'message' => '',
            'items' => $data
        ]);
    }

    /**
     * This function is use for specific post info
     *
     * @param Request $request
     * @return void
     */
    public function share_post_details(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'type' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            'type.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_type_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $content = "";
        if ($request->type == 'Show') {
            $content = PostShow::where('id', $request->content_id)->first(['id', 'user_id', 'show_id', 'post', 'likes_count', 'comment_count', 'updated_at']);
        } elseif ($request->type == 'Episode') {
            $content = PostEpisode::where('id', $request->content_id)->first(['id', 'user_id', 'episode_id', 'post', 'likes_count', 'comment_count', 'updated_at']);
        } elseif ($request->type == 'Playlist') {
            $content = PostPlaylist::where('id', $request->content_id)->first(['id', 'user_id', 'playlist_id', 'post', 'likes_count', 'comment_count', 'updated_at']);
        } elseif ($request->type == 'Smart_Playlist') {
            $content = PostSmartPlaylist::where('id', $request->content_id)->first(['id', 'user_id', 'smart_playlist_id', 'post', 'likes_count', 'comment_count', 'updated_at']);
        } elseif ($request->type == 'Collection') {
            $content = PostCollection::where('id', $request->content_id)->first(['id', 'user_id', 'collection_id', 'post', 'likes_count', 'comment_count', 'updated_at']);
        }

        if (!$content) {
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Common.no_content_found_lbl")
            ]);
        }

        $data = [];
        if ($request->type == 'Show') {

            $like = 'false';
            if ($content->likes) {
                $like_info = $content->likes()->wherePivot('user_id', $user->id)->count();
                if ($like_info) {
                    $like = 'true';
                }
            }

            $subscribers = $user->subscribers()->pluck('show_id')->all();
            $follow_status = 0;
            if (@in_array($content->show->id, $subscribers)) {
                $follow_status = 1;
            }

            $data = [
                'post_id' => $content->id,
                'type' => $request->type,
                'post' => !empty($content->post) ? $content->post : '',
                'content_id' => $content->show->id,
                'content_name' => trim(str_replace("\n", '', html_entity_decode($content->show->title))),
                'description' => trim(str_replace('<br>', "\n", html_entity_decode(strip_tags($content->show->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                'content_image' => !empty($content->show->image) ? $content->show->getWSImage(200) : asset('uploads/default/show.png'),                
                'no_of_subscribers' => Helper::shorten_count($content->show->no_of_subscribers),
                'no_of_likes' => Helper::shorten_count($content->likes_count),
                'no_of_comments' => Helper::shorten_count($content->comment_count),
                'like' => $like,
                'explicit' => $content->show->explicit,
                'user_follow_status' => $follow_status,
                'share_url' => config('config.live_url') . '/podcasts/' . $content->show->id . '/' . Helper::make_slug(trim(@$content->show->title)),
                'last_update' => $content->updated_at->diffForHumans()
            ];            
        } elseif ($request->type == 'Episode') {

            $like = 'false';
            if ($content->likes) {
                $like_info = $content->likes()->wherePivot('user_id', $user->id)->count();
                if ($like_info) {
                    $like = 'true';
                }
            }

            $data = [
                'post_id' => $content->id,
                'type' => $request->type,
                'post' => !empty($content->post) ? $content->post : '',
                'content_id' => $content->episode->id,
                'content_name' => trim(str_replace("\n", '', html_entity_decode($content->episode->title))),
                'description' => trim(str_replace('<br>', "\n", html_entity_decode(strip_tags($content->episode->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                'content_image' => !empty($content->episode->show->image) ? $content->episode->show->getWSImage(200) : asset('uploads/default/show.png'),                
                'no_of_likes' => Helper::shorten_count($content->likes_count),
                'no_of_comments' => Helper::shorten_count($content->comment_count),
                'listen_count' => Helper::shorten_count($content->episode->listen_count),
                'explicit' => $content->episode->explicit,
                'like' => $like,
                'explicit' => $content->episode->show->explicit,                        
                'episode_audio' => $content->episode->getAudioLink(),
                'duration' => $content->episode->getDurationText(),
                'share_url' => config('config.live_url') . '/episode/' . $content->episode_id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$content->episode->title))),
                'last_update' => $content->updated_at->diffForHumans()
            ];
        } elseif ($request->type == 'Playlist') {

            $like = 'false';
            if ($content->likes) {
                $like_info = $content->likes()->wherePivot('user_id', $user->id)->count();
                if ($like_info) {
                    $like = 'true';
                }
            }

            $user_boards = $user->following_boards()->pluck('id')->all();
            $follow_status = 'false';
            if (@in_array($content->playlist->id, $user_boards)) {
                $follow_status = 'true';
            }

            $data = [
                'post_id' => $content->id,
                'type' => $request->type,
                'post' => !empty($content->post) ? $content->post : '',
                'content_id' => $content->playlist->id,
                'content_name' => trim(str_replace("\n", '', html_entity_decode($content->playlist->title))),
                'description' => trim(str_replace('<br>', "\n", html_entity_decode(strip_tags($content->playlist->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                'content_image' => !empty($content->playlist->image) ? $content->playlist->getImage(200) : asset('uploads/default/show.png'),                
                'followers_count' => Helper::shorten_count($content->playlist->followers_count),
                'no_of_likes' => Helper::shorten_count($content->likes_count),
                'no_of_comments' => Helper::shorten_count($content->comment_count),
                'no_of_followers' => Helper::shorten_count(0),
                'like' => $like,
                'explicit' => 0,
                'follow_status' => $follow_status,
                'last_update' => $content->updated_at->diffForHumans()                
            ];
        } elseif ($request->type == 'Smart_Playlist') {
            $like = 'false';
            if ($content->likes) {
                $like_info = $content->likes()->wherePivot('user_id', $user->id)->count();
                if ($like_info) {
                    $like = 'true';
                }
            }

            $user_boards = $user->following_smart_playlist()->pluck('id')->all();
            $follow_status = 'false';
            if (@in_array($content->smart_playlist->id, $user_boards)) {
                $follow_status = 'true';
            }

            $data = [
                'post_id' => $content->id,
                'type' => $request->type,
                'post' => !empty($content->post) ? $content->post : '',
                'content_id' => $content->smart_playlist->id,
                'content_name' => trim(str_replace("\n", '', html_entity_decode($content->smart_playlist->title))),
                'description' => trim(str_replace('<br>', "\n", html_entity_decode(strip_tags($content->smart_playlist->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                'content_image' => !empty($content->smart_playlist->image) ? $content->smart_playlist->getImage(200) : asset('uploads/default/show.png'),                
                'followers_count' => Helper::shorten_count($content->smart_playlist->followers_count),
                'no_of_likes' => Helper::shorten_count($content->likes_count),
                'no_of_comments' => Helper::shorten_count($content->comment_count),
                'no_of_followers' => Helper::shorten_count(0),
                'like' => $like,
                'explicit' => 0,
                'follow_status' => $follow_status,
                'last_update' => $content->updated_at->diffForHumans()
            ];
        } elseif ($request->type == 'Collection') {
            $like = 'false';
            if ($content->likes) {
                $like_info = $content->likes()->wherePivot('user_id', $user->id)->count();
                if ($like_info) {
                    $like = 'true';
                }
            }
            $data = [
                'post_id' => $content->id,
                'type' => $request->type,
                'post' => !empty($content->post) ? $content->post : '',
                'content_id' => $content->collection->id,
                'content_name' => trim(str_replace("\n", '', html_entity_decode($content->collection->title))),
                'description' => trim(str_replace('<br>', "\n", html_entity_decode(strip_tags($content->collection->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                'content_image' => !empty($content->collection->image) ? $content->collection->getImage(200) : asset('uploads/default/show.png'),                
                'followers_count' => Helper::shorten_count(0),
                'no_of_likes' => Helper::shorten_count($content->likes_count),
                'no_of_comments' => Helper::shorten_count($content->comment_count),
                'like' => $like,
                'explicit' => 0,
                'share_url' => config('config.live_url') . '/collections/' . $content->collection->id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$content->collection->title))),
                'last_update' => $content->updated_at->diffForHumans()
            ];
        }
        
        return response()->api([
            'status' => true,
            'message' => '',
            'items' => $data
        ]);
    }

    /*************************************************** Individual Post Code Start *************************************/

    /**
     * This function is use for added individual post
     *
     * @param Request $request
     * @return void
     */
    public function share_individual_post_added(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'post' => 'required'            
        ], [
            'post.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_comment_lbl")            
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $attributes = [
                'user_id' => $user->id,
                'post' => $request->post,
                'status' => 'Published'
            ];

            //Create Post
            $post = Post::create($attributes);
            
            //Added into tbl_user_feeds table
            (new Feed($user))->add(trim($post->id), Feed::INDIVIDUAL_POST);

            //If taged
            $merge_array = [];
            if (!empty($request->tag_id)) {
                $usernames = explode(',', trim($request->tag_id));
                $usernames = array_filter($usernames);

                $userArray = [];
                $pushArray = [];
                foreach ($usernames as $username) {
                    $userTagData = User::where('username', $username)->first();
                    if (! is_null($userTagData)) {
                        $userArray[] = $userTagData->id;
                        $pushArray[] = $userTagData;
                    }
                }
                
                if (count($userArray) > 0) {
                    $user_data = array_unique($userArray);
                    $feed_data = [];
                    foreach ($user_data as $i) {
                        $feed_data[] = '*' . $i . '*';
                    }

                    //Added into feed table
                    if (count($feed_data) > 0) {
                        $merge_array = $feed_data;
                    }

                    $message = '';
                    foreach ($pushArray as $data) {
                        $user_push = $data;
                        if ($user_push) {
                            $message = $user->notification_format_name . ' tagged you in the post';
                            $description = '';
                            $icon = '';
                            $title = $message;
                            $data = [
                                'title' => $message,
                                'username' => $user_push->username,
                                'post_id' => $post->id,
                                'post_type' => 'INDIVIDUALPOSTTAG',
                                'type' => 'INDIVIDUALPOSTTAG',
                                'desc' => '',
                                'tray_icon' => ''
                            ];

                            $tokens = $user_push->pushIds()->where('push_ids.status', 1)->get(['token', 'platform']);
                            if ($tokens) {
                                $tokenData['android'] = [];
                                $tokenData['iphone'] = [];
                                foreach ($tokens as $tokenItem) {
                                    $tokenData[$tokenItem->platform][] = $tokenItem->token;
                                }
                                $tokenData['android'] = array_filter(array_flatten($tokenData['android']));
                                $tokenData['iphone'] = array_filter(array_flatten($tokenData['iphone']));
                                if (count($tokenData['android']) > 0) {
                                    dispatch(new IndividualPushNotification($tokenData['android'], $title, $description, $icon, $data))->onQueue('single_push_notification');
                                }
                                if (count($tokenData['iphone']) > 0) {
                                    dispatch(new IndividualPushNotification($tokenData['iphone'], $title, $description, $icon, $data))->onQueue('single_push_notification');
                                }
                            }
                        }
                    }

                    //Insert into Feeds table for tags
                    $dataArray2 = array_merge(['#' . $post->id . '#'], $merge_array);
                    (new Feed($user))->add($dataArray2, Feed::INDIVIDUAL_POST_TAG);
                    
                    //update tag id in tbl_posts table
                    if (count($userArray) > 0) {
                        $post->update(["tagIds" => json_encode($userArray)]);
                    }
                }
            }

            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Post.success_lbl")
            ]);

        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }

    /**
     * This function is use for specific post info
     *
     * @param Request $request
     * @return void
     */
    public function share_individual_post_details(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }
        
        $content = Post::where('id', $request->content_id)->first(['id', 'user_id', 'post', 'tagIds', 'likes_count', 'comment_count', 'updated_at']);        
        if (!$content) {
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Common.no_content_found_lbl")
            ]);
        }
        
        $like = 'false';
        if ($content->likes) {
            $like_info = $content->likes()->wherePivot('user_id', $user->id)->count();
            if ($like_info) {
                $like = 'true';
            }
        } 
        
        //tag code start
        $tagUserData = [];
        if(! is_null($content->tagIds)) {
            $tagData = User::whereIn('id', json_decode($content->tagIds))->get(['id', 'username', 'first_name', 'image']);
            foreach($tagData as $item) {
                $tagUserData[] = [
                    'id' => $item->id, 
                    'username' => $item->username, 
                    'name' => $item->first_name, 
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/user.png')
                ];
            }            
        }

        $data = [
            'post_id' => $content->id,
            'post' => !empty($content->post) ? $content->post : '',
            'tagUserData' => $tagUserData,
            'no_of_likes' => Helper::shorten_count($content->likes_count),
            'no_of_comments' => Helper::shorten_count($content->comment_count),
            'like' => $like,
            'last_update' => $content->updated_at->diffForHumans()
        ];        
        
        return response()->api([
            'status' => true,
            'message' => '',
            'items' => $data
        ]);
    }

    /**
     * This function is use for delete independent post
     *
     * @param Request $request
     * @return void
     */
    public function share_individual_post_delete(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $content = Post::where('id', $request->content_id)->where('user_id', $user->id)->first();        

        $feed_type = Feed::INDIVIDUAL_POST;
        $feed_type2 = Feed::INDIVIDUAL_POST_LIKE;
        $feed_type3 = Feed::INDIVIDUAL_POST_COMMENT;
        $feed_type4 = Feed::INDIVIDUAL_POST_COMMENT_TAG;
        $feed_type5 = Feed::INDIVIDUAL_POST_TAG;        
        
        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_content_found_lbl")
            ]);
        }

        try {            
            //Remove post from feed tables
            if (!empty($feed_type)) {
                $feedDataMain = FeedRepo::where('type', $feed_type)
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"' . $request->content_id . '"%');
                if ($feedDataMain->count()) {
                    $feedDataMain->delete();
                }
            }

            //Remove like data from feed table
            if (!empty($feed_type2)) {
                $feedDataMain2 = FeedRepo::where('type', $feed_type2)
                    ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                if ($feedDataMain2->count()) {
                    $feedDataMain2->delete();
                }
            }

            //Remove comments and tag from feed table
            if (!empty($feed_type3)) {
                $content_post = $content->comments;
                foreach ($content_post as $commentItem) {
                    $feedDataMain3 = FeedRepo::where('type', $feed_type3)
                        ->where('data', 'LIKE', '%"#' . $commentItem->id . '#"%');
                    if ($feedDataMain3->count()) {
                        $feedDataMain3->delete();
                    }

                    $feedDataMain4 = FeedRepo::where('type', $feed_type4)
                        ->where('data', 'LIKE', '%"#' . $commentItem->id . '#"%');
                    if ($feedDataMain4->count()) {
                        $feedDataMain4->delete();
                    }
                }
            }

            //Remove post tag data from feed table
            if (!empty($feed_type5)) {
                $feedDataMain5 = FeedRepo::where('type', $feed_type5)
                    ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                if ($feedDataMain5->count()) {
                    $feedDataMain5->delete();
                }
            }

            //Delete Comment from Respective tables
            $content->delete();

        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => $ex->getMessage()
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.remove_lbl")
        ]);
    }
    /**
     * This function is use for like / unlike individual post
     *
     * @param Request $request
     * @return void
     */
    public function share_individual_post_like(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl")            
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }
        
        $content = Post::find($request->content_id, ['id', 'user_id', 'likes_count']);
        $feed_type = Feed::INDIVIDUAL_POST_LIKE;        

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_content_found_lbl")
            ]);
        }

        // if ($content->user_id == $user->id) {
        //     return response()->api([
        //         'status' => false,
        //         'message' => 'You can not like your own post.'
        //     ]);
        // }

        try {
            $like_info = $content->likes()->find($user->id, ['id']);

            if ($like_info) {
                //Update Post Like Count
                $content->update(['likes_count' => $content->likes_count - 1]);

                //Remove from Feeds table
                if (!empty($feed_type)) {
                    $feedData = FeedRepo::where('type', $feed_type)
                        ->where('typeable_type', 'App\\Models\\User')
                        ->where('typeable_id', $user->id)
                        ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                    if ($feedData->count()) {
                        $feedData->delete();
                    }
                }

                $content->likes()->detach($user->id);

                return response()->api([
                    'status' => true,
                    'message' => 'Like Remove!',
                    'flag' => 0,
                    'no_of_likes' => Helper::shorten_count($content->likes_count)
                ]);
            } else {
                $content->likes()->attach($user->id);

                //Update Post Like Count
                $content->update(['likes_count' => $content->likes_count + 1]);

                //Insert into Feeds Table
                if (!empty($feed_type)) {
                    if ($content->user_id != $user->id) {
                        $dataArray = ['#' . $request->content_id . '#', '*' . $content->user_id . '*'];
                        (new Feed($user))->add($dataArray, $feed_type);
                    }
                }

                //Send Push Notification
                if($content->user_id != $user->id) {
                    $tokens = User::find($content->user_id)->pushIds()->where('push_ids.status', 1)->pluck('token')->all();
                    if (count($tokens) > 0) {
                        $message = $user->notification_format_name . ' liked your post';
                        $title = $message;
                        $description = '';
                        $icon = '';
                        $data = [
                            'title' => $message,
                            'username' => $user->username,
                            'post_id' => $request->content_id,
                            'post_type' => $request->type,
                            'type' => 'INDIVIDUALPOSTLIKE',
                            'image' => !empty($user->image) ? $user->getImage(200) : '',
                            'mediaUrl' => !empty($user->image) ? $user->getImage(200) : '',
                            'desc' => '',
                            'tray_icon' => ''
                        ];

                        dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
                    }
                }

                return response()->api([
                    'status' => true,
                    'message' => config('language.' . $this->getLocale() . ".Post.post_liked_lbl"),
                    'flag' => 1,
                    'no_of_likes' => Helper::shorten_count($content->likes_count)
                ]);
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => $ex->getMessage()
            ]);
        }
    }

    /**
     * List all post comments
     *
     * @param Request $request
     * @return void
     */
    public function share_individual_post_comments(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }
        
        $contents = PostComment::where('post_id', $request->content_id)
            ->where('status', 'Published')->orderBy('updated_at', 'DESC')->paginate(10, ['id', 'user_id', 'comment', 'tagIds', 'created_at']);
        
        $data = [];
        foreach ($contents as $comment) {
            //tag code start
            $tagUserData = [];
            if(! is_null($comment->tagIds)) {
                $tagData = User::whereIn('id', json_decode($comment->tagIds))->get(['id', 'username', 'first_name', 'image']);
                foreach($tagData as $item) {
                    $tagUserData[] = [
                        'id' => $item->id, 
                        'username' => $item->username, 
                        'name' => $item->first_name, 
                        'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/user.png')
                    ];
                }            
            }
            $data[] = [
                'id' => $comment->id,
                'post_id' => $request->content_id,                
                'user' => $comment->user->full_name,
                'user_photo' => !empty($comment->user->image) ? $comment->user->getImage(100) : asset('uploads/default/user.png'),
                'username' => $comment->user->username,
                'comment' => $comment->comment,
                'tagUserData' => $tagUserData,
                'comment_time' => $comment->created_at->diffForHumans(),
                'owner' => ($comment->user_id == $user->id) ? 1 : 0
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Post.post_no_comment_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $contents->total(),
                'per_page' => $contents->perPage(),
                'pages' => ceil($contents->total() / $contents->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for adding comment
     *
     * @param Request $request
     * @return void
     */
    public function share_individual_post_comment_added(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'comment' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_id_lbl"),
            'comment.required' => config('language.' . $this->getLocale() . ".Post.share_post_comment_added_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $content = Post::find($request->content_id, ['id', 'user_id', 'comment_count']);
        $feed_type = Feed::INDIVIDUAL_POST_COMMENT;
        $feed_type2 = Feed::INDIVIDUAL_POST_COMMENT_TAG;        

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Post.post_no_comment_found_lbl")
            ]);
        }

        // if (($content->user_id == $user->id) && empty($request->post_id)) {
        //     return response()->api([
        //         'status' => false,
        //         'message' => 'You can not comment your own post.'
        //     ]);
        // }

        //dd($content->toArray());            
        //try {
            $attributes = [
                'user_id' => $user->id,
                'comment' => $request->comment,
                'status' => 'Published',
                'reply_post_id' => !empty($request->post_id) ? $request->post_id : 0
            ];
            $commentData = $content->comments()->create($attributes);

            //Update Post Comment Count
            $content->update(['comment_count' => $content->comment_count + 1]);

            //Insert into Feeds Table
            if (!empty($feed_type) && ($content->user_id != $user->id)) {
                $dataArray = ['#' . $commentData->id . '#', '*' . $content->user_id . '*'];
                (new Feed($user))->add($dataArray, $feed_type);
            }

            //Send Push Notification to Post User
            $tokens = User::find($content->user_id)->pushIds()->where('push_ids.status', 1)->pluck('token')->all();
            if (count($tokens) > 0 && ($content->user_id != $user->id)) {
                $message = $user->notification_format_name . ' commented on your post';
                $title = $message;
                $description = '';
                $icon = '';
                $data = [
                    'title' => $message,
                    'username' => $user->username,
                    'post_id' => $request->content_id,
                    'comment_id' => $commentData->id,
                    'post_type' => 'INDIVIDUALPOSTCOMMENT',
                    'type' => 'INDIVIDUALPOSTCOMMENT',
                    'desc' => '',
                    'tray_icon' => ''
                ];
                dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
            }

            $merge_array = [];
            if (!empty($request->tag_id)) {
                $usernames = explode(',', trim($request->tag_id));
                $usernames = array_filter($usernames);

                $userArray = [];
                $pushArray = [];
                foreach ($usernames as $username) {
                    $userTagData = User::where('username', $username)->first();
                    if (! is_null($userTagData)) {
                        $userArray[] = $userTagData->id;
                        $pushArray[] = $userTagData;
                    }
                }

                if (count($userArray) > 0) {
                    $user_data = array_unique($userArray);
                    $feed_data = [];
                    foreach ($user_data as $i) {
                        $feed_data[] = '*' . $i . '*';
                    }

                    //Added into feed table
                    if (count($feed_data) > 0) {
                        $merge_array = $feed_data;
                    }

                    $message = '';
                    foreach ($pushArray as $data) {
                        $user_push = $data;
                        if ($user_push) {
                            $message = $user->notification_format_name . ' tagged you in the post comment';
                            $description = '';
                            $icon = '';
                            $title = $message;
                            $data = [
                                'title' => $message,
                                'username' => $user_push->username,
                                'post_id' => $request->content_id,
                                'comment_id' => $commentData->id,
                                'post_type' => 'INDIVIDUALPOSTTAGCOMMENT',
                                'type' => 'INDIVIDUALPOSTTAGCOMMENT',
                                'desc' => '',
                                'tray_icon' => ''
                            ];

                            $tokens = $user_push->pushIds()->where('push_ids.status', 1)->get(['token', 'platform']);
                            if ($tokens) {
                                $tokenData['android'] = [];
                                $tokenData['iphone'] = [];
                                foreach ($tokens as $tokenItem) {
                                    $tokenData[$tokenItem->platform][] = $tokenItem->token;
                                }
                                $tokenData['android'] = array_filter(array_flatten($tokenData['android']));
                                $tokenData['iphone'] = array_filter(array_flatten($tokenData['iphone']));
                                if (count($tokenData['android']) > 0) {
                                    dispatch(new IndividualPushNotification($tokenData['android'], $title, $description, $icon, $data))->onQueue('single_push_notification');
                                }
                                if (count($tokenData['iphone']) > 0) {
                                    dispatch(new IndividualPushNotification($tokenData['iphone'], $title, $description, $icon, $data))->onQueue('single_push_notification');
                                }
                            }
                        }
                    }

                    //Insert into Feeds table for Tags
                    if (!empty($feed_type2)) {
                        $dataArray2 = array_merge(['#' . $commentData->id . '#'], $merge_array);
                        (new Feed($user))->add($dataArray2, $feed_type2);
                    }

                    //update tag id in tbl_posts table
                    if (count($userArray) > 0) {
                        $commentData->update(["tagIds" => json_encode($userArray)]);
                    }
                }
            }

            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Post.comment_success_lbl"),
                'no_of_comments' => $content->comment_count
            ]);
        // } catch (\Exception $ex) {
        //     return response()->api([
        //         'status' => false,
        //         'message' => $ex->getMessage()
        //     ]);
        // }
    }

    /**
     * This function is use for delete post comment
     *
     * @param Request $request
     * @return void
     */
    public function share_individual_post_comment_delete(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_comment_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }
        
        $content = PostComment::where('id', $request->content_id)->where('user_id', $user->id)->first();
        $feed_type = Feed::INDIVIDUAL_POST_COMMENT;
        $feed_type2 = Feed::INDIVIDUAL_POST_COMMENT_TAG;
        
        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Post.post_no_comment_found_lbl")
            ]);
        }

        try {
            //Remove from feed tables
            if (!empty($feed_type)) {
                $feedData = FeedRepo::where('type', $feed_type)
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                if ($feedData->count()) {
                    $feedData->delete();
                }
            }

            //Remove tags from user_notifications tables
            if (!empty($feed_type2)) {
                $feedData2 = FeedRepo::where('type', $feed_type2)
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"#' . $request->content_id . '#"%');
                if ($feedData2->count()) {
                    $feedData2->delete();
                }
            }

            //Delete Comment from Respective tables
            $content->delete();

            //Update Post Comment Count
            $content->post->update(['comment_count' => \DB::raw('comment_count - 1')]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => $ex->getMessage()
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.remove_lbl")
        ]);
    }

    /**
     * This function is use for specific comment info
     *
     * @param Request $request
     * @return void
     */
    public function share_individual_post_comment_details(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Post.missing_parameter_comment_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }
        
        $content = PostComment::where('id', $request->content_id)->where('status', 'Published')->first();        

        if (!$content) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Post.post_no_comment_found_lbl")
            ]);
        }

        //tag code start
        $tagUserData = [];
        if(! is_null($content->tagIds)) {
            $tagData = User::whereIn('id', json_decode($content->tagIds))->get(['id', 'username', 'first_name', 'image']);
            foreach($tagData as $item) {
                $tagUserData[] = [
                    'id' => $item->id, 
                    'username' => $item->username, 
                    'name' => $item->first_name, 
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/user.png')
                ];
            }            
        }

        $data = [
            'id' => $content->id,
            'post_id' => $content->post_id,
            'comment' => $content->comment,
            'tagUserData' => $tagUserData,
            'comment_time' => $content->created_at->diffForHumans(),
            'owner' => ($content->user_id == $user->id) ? 1 : 0
        ];

        return response()->api([
            'status' => true,
            'message' => '',
            'items' => $data
        ]);
    }
    /*************************************************** Individual Post Code End *************************************/
}
