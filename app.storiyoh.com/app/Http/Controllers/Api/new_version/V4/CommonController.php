<?php

namespace App\Http\Controllers\Api\new_version\V4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Country;
use App\Models\Category;
use App\Models\Network;
use App\Models\ShowLanguage;
use App\Models\Subscriber;
use App\Models\User;
use App\Traits\ResponseFormat;
class CommonController extends Controller
{
    use ResponseFormat;

    /**
     *
     * @param Request $request
     * @return type
     */
    public function country(Request $request)
    {
        $country_list = Country::orderBy('title', 'ASC')->get(['id', 'title', 'countries_iso_code_3']);

        $data = [];
        foreach ($country_list as $item) {
            $data[] = [
                'id' => $item->id,
                'title' => $item->title,
                'countries_iso_code_3' => $item->countries_iso_code_3
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for reset username
     * @param Request $request
     * @return type
     */
    public function checkUsername(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'username' => 'required|alpha_num|min:5|max:15|unique:users,username'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Commonc.checkusername_username_lbl"),
            'username.alpha_num' => config('language.' . $this->getLocale() . ".Commonc.checkusername_username_numlbl"),
            'username.min' => config('language.' . $this->getLocale() . ".Commonc.checkusername_username_minlbl"),
            'username.max' => config('language.' . $this->getLocale() . ".Commonc.checkusername_username_maxlbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        return response()->api([
           'status' => true,
           'message' => 'This username is available.'
        ]);
    }

    /*
     * This function is for register a device.
     *
     * return data
     */
    public function device_register(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
            //return $this->sendAuthErrorResponse($response, 200);
        }

        $validator = Validator::make($request->all(), 
        [
            'uuid' => 'required',
            'device_token' => 'required',
            'platform' => 'required'
        ], [
            'uuid.required' => config('language.' . $this->getLocale() . ".Common.uuid_lbl"),
            'device_token.required' => config('language.' . $this->getLocale() . ".Common.token_lbl"),
            'platform.required' => config('language.' . $this->getLocale() . ".Common.platform")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $user_id = null;
        $user = Auth::guard('api')->user();
        if ($user) {
            $user_id = $user->id;
        }

        //dd($request->user_id);
        if (!empty($request->uuid) && !empty($request->device_token)) {
            if ($request->device_token != null) {
                $checkUUid = \App\Models\PushId::where('uuid', $request->uuid)->first();
                $attributes = $request->all();
                $attributes['user_id'] = $user_id;
                $attributes['token'] = $request->device_token;
                $attributes['platform'] = $request->platform;
                if (!$checkUUid) {
                    \App\Models\PushId::create($attributes);
                } else {
                    $checkUUid->fill($attributes)->save();
                }

                /*$push_id = \App\Models\PushId::firstOrNew(['uuid' => $request->uuid]);
                $attributes = $request->all();
                $attributes['user_id'] = $user_id;
                $attributes['token'] = $request->device_token;
                $attributes['platform'] = $request->platform;
                $push_id->fill($attributes)->save();

                if($user_id == 10) {
                    $newOrOld = $push_id->wasRecentlyCreated;
                    if ($newOrOld) {
                        \Mail::raw('Your Device UUID change.', function ($message) {
                            $message->from('cto.ns@bcat.in', 'Storiyoh');
                            $message->subject('Live Device UUID Change');
                            $message->to('cto.ns@bcat.in');
                            $message->cc('zahir@brandcatmedia.com');
                        });
                    }
                }*/
            }
        }

        return response()->api([
            'status' => true,
            'message' => ''
        ]);
    }

    /**
     * This function is use for check if new version
     * @param Request $request
     * @return type
     */
    public function appVersion(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'version' => 'required',
           'platform' => 'required'
        ], [
            'version.required' => config('language.' . $this->getLocale() . ".Commonc.missing_parameter_version"),
            'platform.required' => config('language.' . $this->getLocale() . ".Commonc.missing_parameter_platform"),
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => true,
               'message' => $validator->errors()->first()
            ]);
        }

        $app_info = \App\Models\AppVersion::where('platform', $request->platform)
            ->first(['version', 'force_update', 'title', 'image', 'description']);

        $playlist_sync = config('config.playlist_sync');

        if ($app_info) {
            $v1 = str_replace('.', '', $app_info->version);
            $v2 = str_replace('.', '', $request->version);

            if ($request->platform == 'android') {
                if ($v1 > $v2) {
                    $response = [
                        'version' => $app_info->version,
                        'force_update' => $app_info->force_update,
                        'title' => $app_info->title,
                        'image' => $app_info->image ? asset('uploads/version/thumbs/640_480_' . $app_info->image) : '',
                        'description' => $app_info->description
                    ];

                    return response()->api([
                        'status' => false,
                        'message' => '',
                        'data' => $response,
                        'playlist_sync' => $playlist_sync,
                        'market_place' => true
                    ]);
                } else {
                    return response()->api([
                        'status' => true,
                        'message' => '',
                        'playlist_sync' => $playlist_sync,
                        'market_place' => true
                    ]);
                }
            } else {
                if ($v1 > $v2) {
                    $response = [
                        'version' => $app_info->version,
                        'force_update' => $app_info->force_update,
                        'title' => $app_info->title,
                        'image' => $app_info->image ? asset('uploads/version/thumbs/640_480_' . $app_info->image) : '',
                        'description' => $app_info->description
                    ];

                    return response()->api([
                        'status' => false,
                        'message' => '',
                        'data' => $response,
                        'playlist_sync' => $playlist_sync,
                        'market_place' => true
                    ]);
                } else {
                    return response()->api([
                        'status' => true,
                        'message' => '',
                        'playlist_sync' => $playlist_sync,
                        'market_place' => true
                    ]);
                }
            }
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Commonc.appversion_message_lbl"),
            'playlist_sync' => $playlist_sync
        ]);
    }

    /**
     * This function is use for get all categories and
     * also auto complete
     *
     * @param Request $request
     */
    public function categories(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = Category::published()->whereDoesntHave('children');

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('title', 'LIKE', '%' . $searchText . '%');
            });

            $query->take(config('config.pagination.autocomplete'));
        }

        $query->orderBy('title', 'ASC');

        $all_category = $query->get(['id', 'title']);

        return response()->api([
            'status' => true,
            'data' => $all_category
        ]);
    }

    /**
     * This function is use for get all categories and
     * also auto complete
     *
     * @param Request $request
     */
    public function networks(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = Network::published();

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('title', 'LIKE', '%' . $searchText . '%');
            });

            $query->take(config('config.pagination.autocomplete'));
        }

        $query->orderBy('title', 'ASC');

        $all_networks = $query->get(['id', 'title']);

        return response()->api([
            'status' => true,
            'data' => $all_networks
        ]);
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function languages(Request $request)
    {
        $languages = ShowLanguage::where('status', 'Published')
            ->whereNotNull('title')->orderBy('title', 'ASC')->get(['id', 'title', 'short_code']);

        $data = [];
        foreach ($languages as $item) {
            $data[] = [
                'id' => $item->id,
                'title' => $item->title,
                'short_code' => $item->short_code
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function filter_search(Request $request)
    {
        //Country Listing
        $country_list = Country::orderBy('title', 'ASC')->get(['id', 'title', 'countries_iso_code_3']);

        $countryData = [];
        foreach ($country_list as $item) {
            $countryData[] = [
                'id' => $item->id,
                'title' => $item->title,
                'countries_iso_code_3' => $item->countries_iso_code_3
            ];
        }

        //Category Listing
        $category_list = Category::published()->whereDoesntHave('children')->orderBy('title', 'ASC')->get(['id', 'title']);
        $categoryData = [];
        foreach ($category_list as $item) {
            $categoryData[] = [
                'id' => $item->id,
                'title' => $item->title
            ];
        }

        $languages_listing = ShowLanguage::where('status', 'Published')
            ->whereNotNull('title')->orderBy('title', 'ASC')->get(['id', 'title', 'short_code']);

        $languageData = [];
        foreach ($languages_listing as $item) {
            $languageData[] = [
                'id' => $item->id,
                'title' => $item->title,
                'short_code' => $item->short_code
            ];
        }

        return response()->api([
            'status' => true,
            'data' => [
                'categoryData' => $categoryData,
                'countryData' => $countryData,
                'languageData' => $languageData,
            ]
        ]);
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ], [
            'email.required' => config('language.' . $this->getLocale() . ".Commonc.subscribe_email_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
            'status' => false,
            'message' => $validator->errors()->first()
            ]);
        }

        $subscribe = Subscriber::where('email', $request->email)
            ->where('subscription_status', 1)->first();
        if ($subscribe) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Commonc.subscribe_message_lbl"),
            ]);
        }

        $subscribe = Subscriber::where('email', $request->email)->first();

        if ($subscribe) {
            $subscribe->update([
                'source' => $request->source,
                'subscription_status' => 1
            ]);

            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Commonc.subscribe_message1_lbl"),
            ]);
        }

        $subscribe = Subscriber::create([
            'email' => $request->email,
            'source' => $request->source,
            'subscription_status' => 1
        ]);

        $mailData = [
            'file_path' => 'emails.thank-you-subscribe',
            'from_email' => config('config.register.sender_email'),
            'from_name' => config('config.register.sender_name'),
            'to_email' => trim($request->email),
            'to_name' => trim($request->email),
            'subject' => 'Thank You for Subscribing Us',
            'filename' => null,
            'data' => ['email' => $request->email]
        ];

        \App\Traits\Helper::sendAllEmail($mailData);

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Commonc.subscribe_message1_lbl"),
        ]);
    }

    /**
     * This function is use for get all user queue
     *
     *
     * @param Request $request
     */
    public function user_queues(Request $request)
    {
        $all_users = \DB::select('select user_id from `tbl_user_queue` group by `user_id`');
        $user_Array = [];
        foreach ($all_users as $user) {
            $userData = User::find($user->user_id);
            $episodeIds = $userData->user_episode_queue()->pluck('id')->all();
            $user_Array[] = [
                'username' => $userData->username,
                'sequence' => implode(',', $episodeIds)
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $user_Array
        ]);
    }

    //Added by datta
    public function contact(Request $request) {
        $validator = Validator::make($request->all(), [
            // 'email' => 'required|email',
            // 'first_name' => 'required',
            // 'last_name' => 'required',
            // 'phone' => 'required',
            'message' => 'required',
        ], [
            // 'email.required' => config('language.' . $this->getLocale() . ".Commonc.subscribe_email_lbl"),
            // 'first_name.required' => config('language.' . $this->getLocale() . ".Commonc.contact_error_lbl"),
            // 'last_name.required' => config('language.' . $this->getLocale() . ".Commonc.contact_error_lbl"),
            // 'phone.required' => config('language.' . $this->getLocale() . ".Commonc.contact_error_lbl"),
            'message.required' => config('language.' . $this->getLocale() . ".Commonc.contact_error_lbl"),
        ]);

        if ($validator->fails()) {
            return $this->sendValidationError($validator->messages()->getMessages(),$validator->messages()->first());
        }

        try {
            $data = $request->all();  
            
            Mail::send('emails.contact', compact('data'), function ($mail) use ( $request, $data) {
                $mail->from(config('config.notification.sender_email'), config('config.notification.sender_name'));
                $mail->to(config('config.admin.email'))
                    ->subject('Contact Us');
            });
            $message = config('language.' . $this->getLocale() . ".Commonc.contact_message_lbl");
            return $this->sendResponse($message);

        } catch (\Exception $ex) {
            
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());     
        }
    }
}
