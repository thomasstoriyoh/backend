<?php

namespace App\Http\Controllers\Api\new_version\V4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Category;
use App\Models\Show;
use App\Traits\Helper;
use App\Traits\FireBase;
use App\ClassesV3\Feed;
use App\Models\UserSocial;
use App\Jobs\IndividualPushNotification;

use App\Traits\ResponseFormat;

/** Topic  Subscription Code Start */
use App\Jobs\FirebaseTopicSubscription;
/** Topic  Subscription Code End */

class DashboardController extends Controller
{
    use ResponseFormat;

    /**
     * This function is use for get all categories
     * @param Request $request
     * @return type
    */
    public function getRecommendedCategories(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        // if (empty($user->username)) {
        //     return response()->api([
        //         'status' => false,
        //         'navigate' => 'username',                   
        //         'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
        //     ]);
        // } 

        // if (! empty($user->username)) {
        //     if ($user->user_type == 0) {
        //         return response()->api([
        //             'status' => false,
        //             'navigate' => 'username',
        //             'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
        //         ]);
        //     }
        // }

        $category = Category::published()->whereDoesntHave('children')
            ->orderBy('title')->get(['id', 'title']);

        $data = [];

        foreach ($category as $key => $values) {
            $data[] = [
                'id' => $values->id,
                'title' => $values->title
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }
    
    /**
     * This function is use for recommended shows
     * @param Request $request
     */
    public function getRecommendedShows(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        // if (empty($user->username)) {
        //     return response()->api([
        //         'status' => false,
        //         'navigate' => 'username',                   
        //         'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
        //     ]);
        // } 
        // if (! empty($user->username)) {
        //     if ($user->user_type == 0) {
        //         return response()->api([
        //             'status' => false,
        //             'navigate' => 'username',
        //             'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
        //         ]);
        //     }
        // }

        //validation code
        $validator = Validator::make($request->all(), [
            'category_id' => 'required'
         ], [
            'category_id.required' => config('language.' . $this->getLocale() . ".Dashboard.missing_category_id_lbl")
         ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
             ]);
        }

        if (! empty($request->category_id)) {
            $interest_category = explode(',', $request->category_id);
            $interest_category = array_filter($interest_category);
        } else {
            $interest_category = $user->categories()->pluck('id')->all();
        }

        $recommended_shows = [];

        $query = Show::published();
        $query->whereHas('categories', function ($q) use ($interest_category) {
            if (count($interest_category) > 0) {
                $q->whereIn('category_id', $interest_category);
            }
        });
                
        $shows = $query->take(config('config.pagination.dashboard_recommended_show'))
            ->orderBy('no_of_subscribers', 'desc')->get(['id', 'title', 'image']);
        
        foreach ($shows as $show) {
            $recommended_shows[] = [
                'id' => $show['id'],
                'title' => trim(str_replace("\n", '', html_entity_decode($show['title']))),
                'image' => !empty($show['image']) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'no_of_subscriber' => Helper::shorten_count($show->no_of_subscribers),
                'no_of_episode' => 0,//$show->episodes()->published()->count()
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $recommended_shows
        ]);
    }

    /**
     * This function is use for recommended User
     * @param Request $request
     */
    public function getRecommendedUsers(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        // if (empty($user->username)) {
        //     return response()->api([
        //         'status' => false,
        //         'navigate' => 'username',                   
        //         'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
        //     ]);
        // } 

        // if (! empty($user->username)) {
        //     if ($user->user_type == 0) {
        //         return response()->api([
        //             'status' => false,
        //             'navigate' => 'username',
        //             'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
        //         ]);
        //     }
        // }        

        $followed_user = $user->following()->pluck('id')->all();

        $query = User::withCount('follower')->approved()->verified()
            ->where('username', '!=', '')->whereNotNull('username')
            ->where('id', '!=', $user->id)
            ->whereNotIn('id', $followed_user);

        $fb_array = [];
        if (! empty($request->fb_friends_list)) {
            $fb_array_ids = explode(',', $request->fb_friends_list);            
            $fb_array = UserSocial::whereIn('provider_id', $fb_array_ids)->where('provider', 'Facebook')->pluck('user_id')->toArray();

            if(count($fb_array) > 0) {
                $order_fb_friends_list = @implode(",", $fb_array);
                $query->orderByRaw('FIELD(id,' . $order_fb_friends_list . ') desc');
            }            
        }        

        $user_list = $query->orderBy('follower_count', 'desc')
            ->get(['id', 'provider', 'provider_id', 'first_name', 'last_name', 'username', 'image']);

        $data['fb'] = [];
        $data['twitter'] = [];
        $data['general'] = [];
        foreach ($user_list as $user_details) {
            if (@in_array($user_details->id, $fb_array)) {
                $data['fb'][] = [
                    'name' => $user_details->notification_format_name,
                    'username' => $user_details->username,
                    'image' => $user_details->image != '' ? $user_details->getImage(100) : asset('uploads/default/user.png'),
                    'follower_count' => Helper::shorten_count($user_details->follower_count),
                    'flag' => 'fb'
                ];
            } else {
                if (count($data['general']) == config('config.pagination.dashboard_recommended_user')) {
                    break;
                }
                $data['general'][] = [
                    'name' => $user_details->notification_format_name,
                    'username' => $user_details->username,
                    'image' => $user_details->image != '' ? $user_details->getImage(100) : asset('uploads/default/user.png'),
                    'follower_count' => Helper::shorten_count($user_details->follower_count),
                    'flag' => 'gn'
                ];
            }
        }

        $social_data = array_merge($data['fb'], $data['twitter']);

        return response()->api([
           'status' => true,
           'data' => array_merge($social_data, $data['general'])
        ]);
    }

    /**
     * This function is use for follow user
     * @param Request $request
     */
    public function follow_user(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
            'username' => 'required'
         ], [
            'username.required' => config('language.' . $this->getLocale() . ".Dashboard.follow_user_missing_username_lbl")
         ]);
 
         if ($validator->fails()) {
             return response()->api([
                'status' => true,
                'message' => $validator->errors()->first()
             ]);
         }
        
        if (! is_null($request->username)) {
            try {
                $usernames = explode(',', $request->username);
                //  $usernames = array_filter($usernames);
                //!empty($request->username) ? $request->username : [];

                $userArray = [];
                $tokens = [];
                if (count($usernames) > 0) {
                    foreach ($usernames as $item) {
                        $following_user = User::where('username', $item)->first(['id']);                        
                        if(! is_null($following_user)) {
                            $follow_user = $user->following()->find($following_user->id, ['id']);
                            if (! $follow_user) {

                                //Insert into Feeds Table
                                (new Feed($user))->add(trim($following_user->id), Feed::FOLLOW_USER);

                                $userArray[] = $following_user->id;
                                $tokens = array_merge($tokens, $following_user->pushIds()->where('push_ids.status', 1)->pluck('token')->all());
                            }
                        }
                    }                    

                    //Sync without existing follow user data
                    $user->following()->syncWithoutDetaching($userArray);

                    //Send Push Notification to the Users
                    if (count($tokens) > 0) {
                        $tokens = array_unique($tokens);
                        $tokens = array_filter(array_flatten($tokens));
                        $title = $user->notification_format_name . ' started following you';
                        $description = '';
                        $icon = '';
                        $data = [
                            'title' => 'New Connection',
                            'username' => $user->username,
                            'type' => 'FU',
                            'desc' => $user->notification_format_name . ' started following you',
                            'tray_icon' => '',
                            'image' => !empty($user->image) ? $user->getImage(200) : '',
                            'mediaUrl' => !empty($user->image) ? $user->getImage(200) : '',
                        ];
                        //FireBase::sendFireBaseNotification($tokens, $title, $description, $icon, $data);
                        dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
                    }
                }
            } catch (\Exception $ex) {
            }
        } 

        //Update Dashboard Status
        $user->update(['dashboard_status' => 1]);        

        //return json from webservices
        return response()->api([
           'status' => true,
           'message' => config('language.' . $this->getLocale() . ".Dashboard.follow_user_success_message_lbl")
        ]);
    }

    /**
     * This function is use for set recommendations
     * @param Request $request
     */
    public function setRecommendations(Request $request)
    {        
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $category_notification_data = $user->categories()->count();

        if ($category_notification_data == 0) {
            
            //Validation code
            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'device_token' => 'required'
            ], [
                'category_id.required' => config('language.' . $this->getLocale() . ".Dashboard.missing_category_id_lbl"),
                'device_token.required' => config('language.' . $this->getLocale() . ".Common.token_lbl")
            ]);

            if ($validator->fails()) {
                return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
            }

            $category_ids = explode(',', $request->category_id);
            $category_ids = array_unique(array_filter($category_ids));
            //$category_ids = array_filter($category_ids);

            //check if user not select min 3 category
            if (count($category_ids) < 3) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Dashboard.set_recommendations_min_3_categories_lbl")
                ]);
            }

            //$request->category_id = is_array($request->category_id) ? $request->category_id : [$request->category_id];

            //Sync category data with user
            $user->categories()->sync($category_ids);
        }

        //dd($category_ids);

        /* Subscribe Recommmended Shows Sync */

        if (! empty($request->show_id)) {
            $show_ids = explode(',', $request->show_id);
            $show_ids = array_unique(array_filter($show_ids));

            foreach ($show_ids as $show_id) {
                Show::where('id', $show_id)->update(['no_of_subscribers' => \DB::raw('no_of_subscribers + 1')]);
            }

            //Sync show data with user
            $user->subscribers()->sync($show_ids);

            /** Topic  Subscription Code Start */
            //Subscribe all podcast for specific user            
            try {
                if ($request->device_token) {
                    dispatch(new FirebaseTopicSubscription($show_ids, $user->id, 'subscribe_on_dashboard', [$request->device_token]))->onQueue('subscribe-topic');
                }
            } catch(\Exception $ex) {}                        
            /** Topic  Subscription Code End */
        }

        /* Follow Recommended User Sync */
        if (! empty($request->username)) {
            try {
                $usernames = explode(',', $request->username);
                //  $usernames = array_filter($usernames);
                //!empty($request->username) ? $request->username : [];
    
                $userArray = [];
                $tokens = [];
    
                if (count($usernames) > 0) {
                    foreach ($usernames as $item) {
                        $following_user = User::where('username', $item)->first(['id']);
    
                        //$follow_user = $user->following()->find($following_user->id, ['id']);
                        //if (! $follow_user) {
    
                        //Insert into Feeds Table
                        (new Feed($user))->add(trim($following_user->id), Feed::FOLLOW_USER);
    
                        $userArray[] = $following_user->id;
                        $tokens = array_merge($tokens, $following_user->pushIds()->where('push_ids.status', 1)->pluck('token')->all());
                        //}
                    }
    
                    //Sync User Data
                    //$user->following()->sync($userArray);
                    $user->following()->syncWithoutDetaching($userArray);                    
    
                    //Send Push Notification to the Users
                    if (count($tokens) > 0) {
                        $tokens = array_unique($tokens);
                        $tokens = array_filter(array_flatten($tokens));
                        $title = $user->notification_format_name . ' started following you';
                        $description = '';
                        $icon = '';
                        $data = [
                            'title' => 'New Connection',
                            'username' => $user->username,
                            'type' => 'FU',
                            'desc' => $user->notification_format_name . ' started following you',
                            'tray_icon' => '',
                            'image' => !empty($user->image) ? $user->getImage(200) : '',
                            'mediaUrl' => !empty($user->image) ? $user->getImage(200) : '',
                        ];
                        FireBase::sendFireBaseNotification($tokens, $title, $description, $icon, $data);
                        //dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');                        
                    }
                }
            } 
            catch(\Exception $ex) {

            }           
        }

        /* Follow Playlist Sync */
        if (! empty($request->board_id)) {
            try {
                $board_ids = explode(',', $request->board_id);
                $board_ids = array_unique(array_filter($board_ids));
    
                foreach ($board_ids as $board_id) {
                    $playlist = \App\Models\Board::find($board_id, ['id', 'user_id']);
                    $playlist->timestamps = false;
                    $playlist->update(['followers_count' => \DB::raw('followers_count + 1')]);                    
                }
    
                //Sync playlist data with user
                $user->following_boards()->sync($board_ids);
            } catch(\Exception $ex) {}            
        }

        /* Follow Smart Playlist Sync */
        if (! empty($request->smart_playlist_id)) {
            try {
                $smart_playlist_ids = explode(',', $request->smart_playlist_id);
                $smart_playlist_ids = array_unique(array_filter($smart_playlist_ids));
    
                foreach ($smart_playlist_ids as $smart_playlist_id) {
                    $smart_playlist = \App\Models\SmartPlaylist::find($smart_playlist_id, ['id', 'user_id']);
                    $smart_playlist->timestamps = false;
                    $smart_playlist->update(['followers_count' => \DB::raw('followers_count + 1')]);
                }
    
                //Sync smart playlist data with user
                $user->following_smart_playlist()->sync($smart_playlist_ids);
            } catch(\Exception $ex) {}            
        }

        //Update Dashboard Status
        if($user->user_type == 2) {
            $user->update(['dashboard_status' => 1]);
        }

        //return json from webservices
        return response()->api([
           'status' => true,
           'message' => config('language.' . $this->getLocale() . ".Dashboard.set_recommendations_success_message_lbl")
        ]);
    }
}
