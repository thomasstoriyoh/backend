<?php

namespace App\Http\Controllers\Api\new_version\V4;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Traits\ResponseFormat;
use App\ClassesV3\V3_3\IP2Location;
use App\Models\MarketplaceCountry;
use App\Models\Show;
use Carbon\Carbon;
use App\Models\EpisodeDetail;
use App\Traits\HelperV2;
use Bcm\Paytm\BcmPaytmWallet;
use App\Models\Order;
use App\Models\ShowPurchase;
use App\Models\EpisodePurchase;
use App\Jobs\InvoiceEmail;
use App\Jobs\SendEmailUser;
use App\ClassesV3\Feed;
use App\Jobs\SendPushNotification;

class PaytmController extends Controller
{
    use ResponseFormat, HelperV2;

    /**
     *This function is use for create aproval url
     */
    public function create_checksum(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];            
            return response()->json($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'product_type' => 'required',                
            'platform' => 'required',
            'payment_mode' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl"),
            'product_type.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_type_lbl"),
            'platform.required' => config('language.' . $this->getLocale() . ".Marketplace.platform_missing"),
            'payment_mode.required' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_missing"),
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Ip2Location code start
            // $ip_address = "183.87.113.164";
            $ip_address = $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();

            if (empty($location['data']['country_code']) || $location['data']['country_code'] == "-") {
                return response()->json([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing")
                ]);
            }

            //Check if marketplace available in user country
            $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id", "tax_name", "tax_price"]);
            
            if (empty($marketplace_country)) {
                return response()->json([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing")
                ]);
            }
            
            $flag = false;            
            if ($request->product_type == 'Series') {
                $content = Show::where("id", $request->content_id)->where('content_type', '!=', 0)->whereNotNull('user_id')
                    ->where('status', 'Published')->first(['id', 'user_id']);
                if ($content) {
                    //First we check if this show already purchased by that user
                    $check = $this->check_if_item_purchase_by_user($content->user_id, $user->id, $content->id, 'series');
                    if ($check == 'yes') {
                        return response()->json([
                            'status' => true,
                            'purchase' => 'yes',
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_already_purchased_lbl"),
                        ]);
                    }
                    //Now we get price for current country and currency id
                    $product_ids = $content->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = ! empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                    $product_type = 1;

                    $flag = true;
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found"),
                    ]);
                }
            } elseif ($request->product_type == 'Episode') {
                $content = EpisodeDetail::where("episode_id", $request->content_id)->where('status', 'Published')->first(['id', 'episode_id', 'user_id', 'content_type']);
                if ($content) {
                    //First we check if this show already purchased by that user
                    $check = $this->check_if_item_purchase_by_user($content->user_id, $user->id, $content->episode_id, 'episode', $content->episode->show->id);
                    //dd($check);
                    if ($check == 'yes') {
                        return response()->json([
                            'status' => true,
                            'purchase' => 'yes',
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_already_purchased_lbl"),
                        ]);
                    }
                    //Now we get price for current country and currency id
                    $product_ids = $content->episode->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = ! empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                    $product_type = ($content->content_type + 1);

                    $flag = true;
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found"),
                    ]);
                }
            }

            if ($flag == true) {
                //Create Invoice Id
                $maxId = \DB::table('orders')->max('id');
                $maxPurchaseId = 0;
                if (! is_null($maxId)) {
                    $maxPurchaseId = $maxId;
                }
                $maxPurchaseId = $maxPurchaseId + 1;
                $date_time = date("ymdHis");
                $invoice_no = "STR" . $date_time . $maxPurchaseId;

                $cust_transaction_id = $this->getRandomNumber(10) . $date_time;

                //Final Price Calculation
                $net_amount = $selling_price;
                $tax_amount = 0;
                $invoice_total = ($net_amount + $tax_amount);
            
                //Insert data into our tables tbl_orders
                $insertArray = [
                    "invoice_number" => $invoice_no,
                    "cust_transaction_id" => $cust_transaction_id,
                    "invoice_date" => Carbon::now(),
                    "user_id" => $user->id,
                    "purchase_type" => 2,
                    "product_type" => $product_type,
                    "product_id" => $request->content_id,
                    "invoice_type" => 1,
                    "country_id" => $marketplace_country->id,
                    "billing_city" => $location['data']['city_name'],
                    "billing_state" => $location['data']['region_name'],
                    "billing_country" => $location['data']['country_name'],
                    "ip_address" => $request->ip(),
                    "gateway" => "PayTM",
                    "platform" => $request->platform,
                    "payment_mode" => $request->payment_mode,
                    "billing_type" => "one_time",
                    "currency" => "INR",
                    "net_amount" => $net_amount,
                    "tax_name" => $marketplace_country->tax_name,
                    "tax_percent" => $marketplace_country->tax_price,
                    "tax_amount" => $tax_amount,
                    "invoice_total" => $invoice_total,
                    "status" => "initiated",
                ];

                Order::create($insertArray);                

                /*$request['ORDER_ID'] = $invoice_no;
                $request['CUST_ID'] = $user->username;
                $request['TXN_AMOUNT'] = $selling_price;
                $request['CALLBACK_URL'] = 'N';
                
                $request_parameter = $request->except('lang', 'content_id', 'product_type', 'platform', 'payment_mode');
                $request = new \Illuminate\Http\Request($request_parameter);                    
                
                $paytm = new BcmPaytmWallet($request);

                $paytmData = $paytm->generateChecksum();*/

                $paytmData = $this->getCheckSum($request, $invoice_no, $cust_transaction_id, $selling_price);

                if(! $paytmData) {
                    return response()->json([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Common.error")
                    ]);
                }

                return response()->json([
                    "status" => true, 
                    "mesage"=> "", 
                    "data" => $paytmData
                ]);                
            } else {
                return response()->json([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.error")
                ]);
            }
        } catch (\Exception $ex) {           
            return response()->json([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error"),
                'data'    => []
            ]);            
        }
    }

     /**
     *This function is use for create aproval url
     */
    public function verify_checksum(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        try {
            $receipt = json_decode($request->receipt, 1);

            $response_receipt = $request->receipt;

            foreach ($receipt as $key => $value) {
                $request[$key] = $value;
            }

            $request_parameter = $request->except('lang', 'receipt');
            $request = new \Illuminate\Http\Request($request_parameter);            

            $request['CHECKSUMHASH'] = preg_replace('/\s+/', '+', $request->CHECKSUMHASH);            
            
            $paytm = new BcmPaytmWallet($request);
            $response = $paytm->verifyChecksum();
           
            $arrayUpdate['transaction_id'] = $response['TXNID'];
            $arrayUpdate['invoice_date'] = $response['TXNDATE'];                        
            $arrayUpdate['response'] = $response_receipt;
            $success_flag = "";
            $message = "";
            if($response['STATUS'] == 'TXN_SUCCESS') {                
                $arrayUpdate['status'] = "success";
                $arrayUpdate['additional_status'] = null;
                $success_flag = "S";
                $message = config('language.' . $this->getLocale() . ".Marketplace.payment_mode_success_lbl");
            } else if($response['STATUS'] == 'TXN_FAILURE') {                
                $arrayUpdate['status'] = "failed";
                $arrayUpdate['additional_status'] = null;
                $success_flag = "F";
                $message = config('language.' . $this->getLocale() . ".Marketplace.payment_mode_failed_lbl");
            } else if($response['STATUS'] == 'TXN_PENDING') {                
                $arrayUpdate['status'] = "pending";
                $arrayUpdate['additional_status'] = null;
                $success_flag = "P";
                $message = config('language.' . $this->getLocale() . ".Marketplace.payment_mode_pending_lbl");
            }

            $order = Order::where("invoice_number", $response['ORDERID'])
                ->first(['id', 'user_id', 'product_type', 'product_id', 'status']);
            
            if($order) {
                if ($order->status == 'initiated') {                    
                    //Update Status
                    $order->fill($arrayUpdate)->save();                    
                    if($success_flag == "S") {
                        // Update purchase status
                        if($order->product_type == 1) { // Show
                            ShowPurchase::create(["show_id" => $order->product_id, "user_id" => $order->user_id, "purchased_date" => now()]);
                        } else if($order->product_type == 2 || $order->product_type == 3) { // Episode or Standalone episode
                            EpisodePurchase::create(["episode_id" => $order->product_id, "user_id" => $order->user_id, "purchased_date" => now()]);
                        }                        
                        
                        // Send invoice Email to User
                        dispatch(new InvoiceEmail($order->id, "self"))->onQueue('email');

                        return response()->api([
                            'status' => true,
                            'message' => $message
                        ]);
                    } else if($success_flag == "P") {
                        return response()->api([
                            'status' => true,
                            'message' => $message
                        ]);
                    }
                    return response()->api([
                        'status' => false,
                        'message' => $message
                    ]);              
                } else {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_already_done_lbl")
                    ]);                
                }                
            } else {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl")
                ]);
            }
        } catch (\Exception $ex) {           
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);            
        }
    }

    /**
     *This function is use for create aproval url for gift
     */
    public function create_checksum_for_gift(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];            
            return response()->json($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'product_type' => 'required',
            'giftBy' => 'required',
            'platform' => 'required',
            'payment_mode' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl"),
            'product_type.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_type_lbl"),
            'giftBy.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_recipient_id_lbl"),
            'platform.required' => config('language.' . $this->getLocale() . ".Marketplace.platform_missing"),
            'payment_mode.required' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_missing"),
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Ip2Location code start
            $ip_address = "183.87.113.164";
            //$ip_address = $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();

            if (empty($location['data']['country_code']) || $location['data']['country_code'] == "-") {
                return response()->json([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing")
                ]);
            }

            //Check if marketplace available in user country
            $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id", "tax_name", "tax_price"]);
            
            if (empty($marketplace_country)) {
                return response()->json([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing")
                ]);
            }
            
            $flag = false;            
            if ($request->product_type == 'Series') {
                $content = Show::where("id", $request->content_id)->where('content_type', '!=', 0)->whereNotNull('user_id')
                    ->where('status', 'Published')->first(['id', 'user_id']);
                if ($content) {
                    //First we check if this show already purchased by that user
                    $check = $this->check_if_item_purchase_by_user_for_gift($content->user_id, $request->giftBy, $content->id, 'series');
                    if ($check == 'yes') {
                        return response()->json([
                            'status' => true,
                            'purchase' => 'yes',
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_already_purchased_lbl"),
                        ]);
                    }
                    //Now we get price for current country and currency id
                    $product_ids = $content->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = ! empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                    $product_type = 1;

                    $flag = true;
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found"),
                    ]);
                }
            } elseif ($request->product_type == 'Episode') {
                $content = EpisodeDetail::where("episode_id", $request->content_id)->where('status', 'Published')->first(['id', 'episode_id', 'user_id', 'content_type']);
                if ($content) {
                    //First we check if this show already purchased by that user
                    $check = $this->check_if_item_purchase_by_user_for_gift($content->user_id, $request->giftBy, $content->episode_id, 'episode', $content->episode->show->id);
                    //dd($check, $content->user_id, $request->giftBy, $content->episode_id, $content->episode->show->id);
                    if ($check == 'yes') {
                        return response()->json([
                            'status' => true,
                            'purchase' => 'yes',
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_already_purchased_lbl"),
                        ]);
                    }
                    //Now we get price for current country and currency id
                    $product_ids = $content->episode->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = ! empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                    $product_type = ($content->content_type + 1);

                    $flag = true;
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found"),
                    ]);
                }
            }

            if ($flag == true) {
                //Create Invoice Id
                $maxId = \DB::table('orders')->max('id');
                $maxPurchaseId = 0;
                if (! is_null($maxId)) {
                    $maxPurchaseId = $maxId;
                }
                $maxPurchaseId = $maxPurchaseId + 1;
                $date_time = date("ymdHis");
                $invoice_no = "STR" . $date_time . $maxPurchaseId;

                $cust_transaction_id = $this->getRandomNumber(10) . $date_time;

                //Final Price Calculation
                $net_amount = $selling_price;
                $tax_amount = 0;
                $invoice_total = ($net_amount + $tax_amount);
            
                //Insert data into our tables tbl_orders
                $insertArray = [
                    "invoice_number" => $invoice_no,
                    "cust_transaction_id" => $cust_transaction_id,
                    "invoice_date" => Carbon::now(),
                    "user_id" => $request->giftBy,
                    "giftBy" => $user->id,
                    "purchase_type" => 2,
                    "product_type" => $product_type,
                    "product_id" => $request->content_id,
                    "invoice_type" => 1,
                    "country_id" => $marketplace_country->id,
                    "billing_city" => $location['data']['city_name'],
                    "billing_state" => $location['data']['region_name'],
                    "billing_country" => $location['data']['country_name'],
                    "ip_address" => $request->ip(),
                    "gateway" => "PayTM",
                    "platform" => $request->platform,
                    "payment_mode" => $request->payment_mode,
                    "billing_type" => "one_time",
                    "currency" => "INR",
                    "net_amount" => $net_amount,
                    "tax_name" => $marketplace_country->tax_name,
                    "tax_percent" => $marketplace_country->tax_price,
                    "tax_amount" => $tax_amount,
                    "invoice_total" => $invoice_total,
                    "status" => "initiated",
                    "gift_message" => $request->gift_message,
                ];

                Order::create($insertArray);                

                $paytmData = $this->getCheckSum($request, $invoice_no, $cust_transaction_id, $selling_price);

                if(! $paytmData) {
                    return response()->json([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Common.error")
                    ]);
                }

                return response()->json([
                    "status" => true, 
                    "mesage"=> "", 
                    "data" => $paytmData
                ]);                
            } else {
                return response()->json([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.error")
                ]);
            }
        } catch (\Exception $ex) {           
            return response()->json([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error"),
                'data'    => []
            ]);            
        }
    }

     /**
     *This function is use for create aproval url
     */
    public function verify_checksum_for_gift(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        try {
            $receipt = json_decode($request->receipt, 1);

            $response_receipt = $request->receipt;

            foreach ($receipt as $key => $value) {
                $request[$key] = $value;
            }

            $request_parameter = $request->except('lang', 'receipt');
            $request = new \Illuminate\Http\Request($request_parameter);            

            $request['CHECKSUMHASH'] = preg_replace('/\s+/', '+', $request->CHECKSUMHASH);            
            
            $paytm = new BcmPaytmWallet($request);
            $response = $paytm->verifyChecksum();
           
            $arrayUpdate['transaction_id'] = $response['TXNID'];
            $arrayUpdate['invoice_date'] = $response['TXNDATE'];                        
            $arrayUpdate['response'] = $response_receipt;
            $success_flag = "";
            $message = "";
            if($response['STATUS'] == 'TXN_SUCCESS') {                
                $arrayUpdate['status'] = "success";
                $arrayUpdate['additional_status'] = null;
                $success_flag = "S";
                $message = config('language.' . $this->getLocale() . ".Marketplace.payment_mode_success_lbl");
            } else if($response['STATUS'] == 'TXN_FAILURE') {                
                $arrayUpdate['status'] = "failed";
                $arrayUpdate['additional_status'] = null;
                $success_flag = "F";
                $message = config('language.' . $this->getLocale() . ".Marketplace.payment_mode_failed_lbl");
            } else if($response['STATUS'] == 'TXN_PENDING') {                
                $arrayUpdate['status'] = "pending";
                $arrayUpdate['additional_status'] = null;
                $success_flag = "P";
                $message = config('language.' . $this->getLocale() . ".Marketplace.payment_mode_pending_lbl");
            }            

            $order = Order::where("invoice_number", $response['ORDERID'])->first(['id', 'user_id', 'giftBy', 'product_type', 'product_id', 'status', 'gift_message']);
            if(is_null($order->giftBy)) {
                if($order->user_id != $user->id) {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl")
                    ]);
                }
            } else {
                if($order->giftBy != $user->id) {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl")
                    ]);
                }
            }
            
            if($order) {
                if ($order->status == 'initiated') {                    
                    //Update Status
                    $order->fill($arrayUpdate)->save();                    
                    if($success_flag == "S") {
                        // Update purchase status
                        if($order->product_type == 1) { // Show
                            ShowPurchase::create(["show_id" => $order->product_id, "user_id" => $order->user_id, "purchased_date" => now()]);
                        } else if($order->product_type == 2 || $order->product_type == 3) { // Episode or Standalone episode
                            EpisodePurchase::create(["episode_id" => $order->product_id, "user_id" => $order->user_id, "purchased_date" => now()]);
                        }                        
                        
                        // Send invoice Email to User
                        dispatch(new InvoiceEmail($order->id, "gift"))->onQueue('email');

                        // Send email to recipient
                        $mailData = [
                            'file_path' => 'emails.gift_message',
                            'from_email' => config('config.register.sender_email'),
                            'from_name' => config('config.register.sender_name'),
                            'to_email' => trim($order->user->email),
                            'to_name' => $order->user->full_name,
                            'subject' => 'Gift Subject',
                            'filename' => null,
                            'data' => ['order' => $order]
                        ];
                        dispatch(new SendEmailUser($mailData))->onQueue('email');                        
                        
                        // Insert into Feeds Table                        
                        $dataArray = [trim($order->id)];
                        (new Feed($order->user))->add($dataArray, Feed::GIFTING_CONTENT); 
                        
                        //Send push notification
                        $message = $order->sender->notification_format_name . ' gifted you';
                        $title = $message;
                        $description = '';
                        $icon = '';                            
                        $data = [
                            'title' => $message,
                            'username' => '',
                            'content_id' => $order->product_id,
                            'content_type' => $order->product_type == 1 ? "S" : "E",
                            'type' => 'GIFTCONTENT',
                            'image' => '',
                            'mediaUrl' => '',
                            'desc' => '',
                            'tray_icon' => ''
                        ];
                                                    
                        dispatch(new SendPushNotification($order->user_id, $title, $description, $icon, $data))->onQueue('push_notification');
                        
                        return response()->api([
                            'status' => true,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_success_lbl")
                        ]);
                    } else if($success_flag == "P") {
                        return response()->api([
                            'status' => true,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_pending_lbl")
                        ]);
                    }
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Common.error")
                    ]);              
                } else {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_already_done_lbl")
                    ]);                
                }                
            } else {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl")
                ]);
            }
        } catch (\Exception $ex) {           
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);            
        }
    }
    
    /**
     * This function is use for
     * getting random number
     */
    protected function getRandomNumber($n) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
    
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
    
        return $randomString; 
    }
}
