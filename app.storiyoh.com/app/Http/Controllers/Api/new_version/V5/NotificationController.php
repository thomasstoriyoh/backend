<?php

namespace App\Http\Controllers\Api\new_version\V5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Traits\Helper;
use App\Models\Feed;
use App\ClassesV3\Feed as NotificationRepo;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\NotificationResponseV2\FollowUserResponse;
use App\NotificationResponseV2\FollowBoardResponse;
use App\NotificationResponseV2\BoardFeedResponse;
use App\NotificationResponseV2\ShowNewEpisodeFeedResponse;
use App\NotificationResponseV2\EpisodeCommentFeedResponse;
use App\Models\Episode;

/* New Code Added */
use App\ClassesV3\Notification;
use App\Models\ShowNotification;
use App\NotificationResponseV2\ShowNewEpisodeFeedResponseV3;
  
//use App\FeedResponse\ShowFeedResponse;
//use App\FeedResponse\BoardNewFeedResponse;

/* New Code Added 01/09/2018 */
use App\NotificationResponseV2\PostLikeFeedResponse;
use App\NotificationResponseV2\PostCommentFeedResponse;
use App\NotificationResponseV2\PostTagFeedResponse;
use App\NotificationResponseV2\BoardContributorResponse;
use App\NotificationResponseV2\BoardContributorFeedResponse;

/** Gift Code */
use App\NotificationResponseV2\UserGiftResponse;

use App\Traits\ResponseFormat;

class NotificationController extends Controller
{
    use ResponseFormat;
    
    protected $notificationMap = [
        NotificationRepo::FOLLOW_USER => FollowUserResponse::class,
        NotificationRepo::FOLLOW_BOARD => FollowBoardResponse::class,
        NotificationRepo::BOARD_UPDATE => BoardFeedResponse::class,
        NotificationRepo::SHOW_UPDATE => ShowNewEpisodeFeedResponse::class,
        NotificationRepo::EPISODE_COMMENT => EpisodeCommentFeedResponse::class,
        //NotificationRepo::SUBSCRIBE_SHOW => ShowFeedResponse::class,
        //NotificationRepo::NEW_BOARD => BoardNewFeedResponse::class,

        /* New Code Added 01/09/2018 */
        /** Post like for all types */
        NotificationRepo::SHOW_POST_LIKE => PostLikeFeedResponse::class,
        NotificationRepo::EPISODE_POST_LIKE => PostLikeFeedResponse::class,
        NotificationRepo::PLAYLIST_POST_LIKE => PostLikeFeedResponse::class,
        NotificationRepo::SMARTPLAYLIST_POST_LIKE => PostLikeFeedResponse::class,
        NotificationRepo::COLLECTION_POST_LIKE => PostLikeFeedResponse::class,
        NotificationRepo::INDIVIDUAL_POST_LIKE => PostLikeFeedResponse::class,

        /** Post comment for all types */
        NotificationRepo::SHOW_POST_COMMENT => PostCommentFeedResponse::class,
        NotificationRepo::EPISODE_POST_COMMENT => PostCommentFeedResponse::class,
        NotificationRepo::PLAYLIST_POST_COMMENT => PostCommentFeedResponse::class,
        NotificationRepo::SMARTPLAYLIST_POST_COMMENT => PostCommentFeedResponse::class,
        NotificationRepo::COLLECTION_POST_COMMENT => PostCommentFeedResponse::class,
        NotificationRepo::INDIVIDUAL_POST_COMMENT => PostCommentFeedResponse::class,

        /** Post tags for all types */
        NotificationRepo::SHOW_POST_COMMENT_TAG => PostTagFeedResponse::class,
        NotificationRepo::EPISODE_POST_COMMENT_TAG => PostTagFeedResponse::class,
        NotificationRepo::PLAYLIST_POST_COMMENT_TAG => PostTagFeedResponse::class,
        NotificationRepo::SMARTPLAYLIST_POST_COMMENT_TAG => PostTagFeedResponse::class,
        NotificationRepo::COLLECTION_POST_COMMENT_TAG => PostTagFeedResponse::class,
        NotificationRepo::INDIVIDUAL_POST_COMMENT_TAG => PostTagFeedResponse::class,
        NotificationRepo::INDIVIDUAL_POST_TAG => PostTagFeedResponse::class,

        /** Playlist Contributor */
        NotificationRepo::PLAYLIST_CONTRIBUTOR => BoardContributorResponse::class,
        NotificationRepo::PLAYLIST_CONTRIBUTOR_EPISODE => BoardContributorFeedResponse::class,

        /** Gift Code */
        NotificationRepo::GIFTING_CONTENT => UserGiftResponse::class,
    ];

    /* New Code Added */
    protected $showNotificationMap = [
        Notification::SHOW_UPDATE => ShowNewEpisodeFeedResponseV3::class        
    ];

    /* New Code Added 01/09/2018 */
    /** Post like array for all types */
    protected $like_array = [
        NotificationRepo::SHOW_POST_LIKE,
        NotificationRepo::EPISODE_POST_LIKE,
        NotificationRepo::PLAYLIST_POST_LIKE,
        NotificationRepo::SMARTPLAYLIST_POST_LIKE,
        NotificationRepo::COLLECTION_POST_LIKE,
        NotificationRepo::INDIVIDUAL_POST_LIKE
    ];

    /** Post comment array for all types */
    protected $comment_array = [
        NotificationRepo::SHOW_POST_COMMENT,
        NotificationRepo::EPISODE_POST_COMMENT,
        NotificationRepo::PLAYLIST_POST_COMMENT,
        NotificationRepo::SMARTPLAYLIST_POST_COMMENT,
        NotificationRepo::COLLECTION_POST_COMMENT,
        NotificationRepo::INDIVIDUAL_POST_COMMENT
    ];

    /** Post tags array for all types */
    protected $tag_array = [
        NotificationRepo::SHOW_POST_COMMENT_TAG,
        NotificationRepo::EPISODE_POST_COMMENT_TAG,
        NotificationRepo::PLAYLIST_POST_COMMENT_TAG,
        NotificationRepo::SMARTPLAYLIST_POST_COMMENT_TAG,
        NotificationRepo::COLLECTION_POST_COMMENT_TAG,
        NotificationRepo::INDIVIDUAL_POST_COMMENT_TAG,
        NotificationRepo::INDIVIDUAL_POST_TAG
    ];    

    /**
    * This function is use for notification testing
    * @param Request $request
    * @return type
    */
    public function notification_for_test (Request $request) 
    {        
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            // return response()->api([
            //     'status' => false,
            //     'message' => config('config.missing_parameter3').' Notification.'
            // ]);
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('config.auth_error') . ' notification_for_test.'
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $boards = $user->boards()->pluck('id')->all();       

        $types = [
            'App\\Models\\Board' => ['board_user', 'board_id', 'user_id']
        ];

        // 'Board' => 'followers', 'Show' => 'subscribers'
        $query = Feed::query();
        foreach ($types as $type => $info) {
            $query->orWhere(function($q) use ($type, $info, $user) {
                $q->where('typeable_type', $type)
                //->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                ->where('type', NotificationRepo::BOARD_UPDATE)
                ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                    $que->select($info[1])->from($info[0])->where($info[2], $user->id);
                    // if($type == 'Show') {
                    //    $que->where('notify', 1);
                    // }
                });
                // Table: $info[0]
                // Created at grater than created at in the pivot table
                $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                    return $builder->select('created_at')->from($info[0])
                        ->whereRaw($info[1].' = tbl_user_feeds.typeable_id')
                        ->where($info[2], $user->id);
                // ->orderBy('created_at', 'DESC')->take(1);
                });               
            });
        }        

        $query->orWhere(function($q) use ($user) {
           $q->where('typeable_type', "App\\Models\\User")
               ->where('type', NotificationRepo::FOLLOW_USER)
               ->where('data', 'LIKE', '%"'.$user->id.'"%');
        });        
  
        $query->orWhere(function($q) use ($boards) {            
            foreach($boards as $board) {
                $q->orWhere('data', 'LIKE', '%"'.$board.'"%');
                $q->where('typeable_type', 'App\\Models\\User');
                $q->where('type', NotificationRepo::FOLLOW_BOARD);
            }
        });

        $query->orWhere(function($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\Board")
                ->where('type', NotificationRepo::PLAYLIST_CONTRIBUTOR)
                ->where('data', 'LIKE', '%"'.$user->id.'"%');
        });

        $query->orWhere(function($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\Board")
                ->where('type', NotificationRepo::PLAYLIST_CONTRIBUTOR_EPISODE)
                ->where('data', 'LIKE', '%"$'.$user->id.'$"%');
        });

        //echo $query->toSql();exit;

        foreach($this->like_array as $item) {
            $query->orWhere(function($q) use ($user, $item) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', $item)
                    ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
            }); 
        }

        foreach($this->comment_array as $item) {
            $query->orWhere(function($q) use ($user, $item) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', $item)
                    ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
            }); 
        }
        
        foreach($this->tag_array as $item) {
            $query->orWhere(function($q) use ($user, $item) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', $item)
                    ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
            }); 
        }

        $query->orWhere(function($q) use ($user) {
           $q->where('typeable_type', "App\\Models\\EpisodeUserComment")
               ->where('data', 'LIKE', '%"'.$user->id.'"%');
        });
        
        /** Gift Code */
        $query->orWhere(function($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\User")
                ->where('type', NotificationRepo::GIFTING_CONTENT)
                ->where('typeable_id', $user->id);
        });
    
        $feed_items = $query->orderBy('updated_at', 'DESC')->paginate(config('config.pagination.notification_list'));

        $data = [];
        foreach ($feed_items as $feed_item) {
           if (!empty($this->notificationMap[$feed_item->type])) {
            if (with(new $this->notificationMap[$feed_item->type]($feed_item, $user))->response()) {
                $data[] = with(new $this->notificationMap[$feed_item->type]($feed_item, $user))->response();
            }
           }
        }

        $message = '';

        if(count($data) == 0) {
            $message = "No Notification Found.";
        }

        if($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true, 
                'data' => [
                    'items' => $data, 
                    'message' => $message
                ]
            ]);
        }

        return response()->api([
            'status' => true, 
            'data' => [
                'total' => $feed_items->total(), 
                'per_page' => $feed_items->perPage(),
                'pages' => ceil($feed_items->total()/$feed_items->perPage()),
                'items' => $data, 
                'message' => $message
            ]
        ]);    
    }

    /**
    * This function is use for notification
    * @param Request $request
    * @return type
    */
    public function notification (Request $request) 
    {        
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $boards = $user->boards()->pluck('id')->all();       

        $types = [
            'App\\Models\\Board' => ['board_user', 'board_id', 'user_id']
        ];

        // 'Board' => 'followers', 'Show' => 'subscribers'
        $query = Feed::query();
        foreach ($types as $type => $info) {
            $query->orWhere(function($q) use ($type, $info, $user) {
                $q->where('typeable_type', $type)
                //->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                ->where('type', NotificationRepo::BOARD_UPDATE)
                ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                    $que->select($info[1])->from($info[0])->where($info[2], $user->id);
                    // if($type == 'Show') {
                    //    $que->where('notify', 1);
                    // }
                });
                // Table: $info[0]
                // Created at grater than created at in the pivot table
                $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                    return $builder->select('created_at')->from($info[0])
                        ->whereRaw($info[1].' = tbl_user_feeds.typeable_id')
                        ->where($info[2], $user->id);
                        // ->orderBy('created_at', 'DESC')->take(1);
                });               
            });
        }        

        $query->orWhere(function($q) use ($user) {
           $q->where('typeable_type', "App\\Models\\User")
               ->where('type', NotificationRepo::FOLLOW_USER)
               ->where('data', 'LIKE', '%"'.$user->id.'"%');
        });        
  
        $query->orWhere(function($q) use ($boards) {            
            foreach($boards as $board) {
                $q->orWhere('data', 'LIKE', '%"'.$board.'"%');
                $q->where('typeable_type', 'App\\Models\\User');
                $q->where('type', NotificationRepo::FOLLOW_BOARD);
            }
        });

        $query->orWhere(function($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\Board")
                ->where('type', NotificationRepo::PLAYLIST_CONTRIBUTOR)
                ->where('data', 'LIKE', '%"'.$user->id.'"%');
        });

        $query->orWhere(function($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\Board")
                ->where('type', NotificationRepo::PLAYLIST_CONTRIBUTOR_EPISODE)
                ->where('data', 'LIKE', '%"$'.$user->id.'$"%');
        });        

        foreach($this->like_array as $item) {
            $query->orWhere(function($q) use ($user, $item) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', $item)
                    ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
            }); 
        }

        foreach($this->comment_array as $item) {
            $query->orWhere(function($q) use ($user, $item) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', $item)
                    ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
            }); 
        }
        
        foreach($this->tag_array as $item) {
            $query->orWhere(function($q) use ($user, $item) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', $item)
                    ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
            }); 
        }

        $query->orWhere(function($q) use ($user) {
           $q->where('typeable_type', "App\\Models\\EpisodeUserComment")
               ->where('data', 'LIKE', '%"'.$user->id.'"%');
        }); 
        
        /** Gift Code */
        $query->orWhere(function($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\User")
                ->where('type', NotificationRepo::GIFTING_CONTENT)
                ->where('typeable_id', $user->id);
        });

        //echo $query->toSql();exit;
    
        $feed_items = $query->orderBy('updated_at', 'DESC')->paginate(config('config.pagination.notification_list'));

        $data = [];
        foreach ($feed_items as $feed_item) {
            if (!empty($this->notificationMap[$feed_item->type])) {
                if (with(new $this->notificationMap[$feed_item->type]($feed_item, $user))->response()) {
                    $data[] = with(new $this->notificationMap[$feed_item->type]($feed_item, $user))->response();
                }
            }
        }

        $message = '';

        if(count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Notification.notification_not_found_lbl");
        }

        if($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true, 
                'data' => [
                    'items' => $data, 
                    'message' => $message
                ]
            ]);
        }

        return response()->api([
            'status' => true, 
            'data' => [
                'total' => $feed_items->total(), 
                'per_page' => $feed_items->perPage(),
                'pages' => ceil($feed_items->total()/$feed_items->perPage()),
                'items' => $data, 
                'message' => $message
            ]
        ]);    
    }

    /**
    * This function is use for notification
    * @param Request $request
    * @return type
    */
    public function show_notification (Request $request) 
    {    
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //$boards = $user->boards()->pluck('id')->all();       

        $types = [
            'App\\Models\\Show' => ['show_user', 'show_id', 'user_id']
        ];

        // 'Board' => 'followers', 'Show' => 'subscribers'
        $query = Feed::query();
        foreach ($types as $type => $info) {
            $query->orWhere(function($q) use ($type, $info, $user) {
                $q->where('typeable_type', $type)
                //->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                ->where('type', NotificationRepo::SHOW_UPDATE)
                ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                    $que->select($info[1])->from($info[0])->where($info[2], $user->id);
                    // if($type == 'Show') {
                    //    $que->where('notify', 1);
                    // }
                });
                // Table: $info[0]
                // Created at grater than created at in the pivot table
                $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                    return $builder->select('created_at')->from($info[0])
                        ->whereRaw($info[1].' = tbl_user_feeds.typeable_id')
                        ->where($info[2], $user->id);
                // ->orderBy('created_at', 'DESC')->take(1);
                });               
            });
        }        

        $feed_items = $query->orderBy('updated_at', 'DESC')
            ->paginate(config('config.pagination.notification_list'));
        
        $data = [];
        foreach ($feed_items as $feed_item) {
            if (!empty($this->notificationMap[$feed_item->type])) {
                try {
                    $data[] = with(new $this->notificationMap[$feed_item->type]($feed_item,$user))->response();
                } catch(\Exception $ex) {}
            }
        }

        $message = '';
        if(count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Notification.notification_not_found_lbl");
        }

        if($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true, 
                'data' => [
                    'items' => $data, 
                    'message' => $message
                ]
            ]);
        }        

        return response()->api([
            'status' => true, 
            'data' => [
                'total' => $feed_items->total(), 
                'per_page' => $feed_items->perPage(),
                'pages' => ceil($feed_items->total()/$feed_items->perPage()),
                'items' => $data, 
                'message' => $message
            ]
        ]);
    }

    /**
    * This function is use for display show on Feed listing
    * @param Request $request
    * @return type
    */
    public function feed_show_notification (Request $request) 
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $types = [
            'App\\Models\\Show' => ['show_user', 'show_id', 'user_id']
        ];

        // 'Board' => 'followers', 'Show' => 'subscribers'
        $query = Feed::query();
        foreach ($types as $type => $info) {
            $query->orWhere(function($q) use ($type, $info, $user) {
                $q->where('typeable_type', $type)
                //->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                ->where('type', NotificationRepo::SHOW_UPDATE)
                ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                    $que->select($info[1])->from($info[0])->where($info[2], $user->id);
                    // if($type == 'Show') {
                    //    $que->where('notify', 1);
                    // }
                });
                // Table: $info[0]
                // Created at grater than created at in the pivot table
                $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                    return $builder->select('created_at')->from($info[0])
                        ->whereRaw($info[1].' = tbl_user_feeds.typeable_id')
                        ->where($info[2], $user->id);
                // ->orderBy('created_at', 'DESC')->take(1);
                });               
            });
        }        

        //dd($feed_items->toArray());
    
        $feed_items = $query->orderBy('updated_at', 'DESC')
            ->paginate(config('config.pagination.notification_list'));
        
        $data = [];
        foreach ($feed_items as $feed_item) {
            if (!empty($this->notificationMap[$feed_item->type])) {
                try {
                    $data[] = with(new $this->notificationMap[$feed_item->type]($feed_item,$user))->response();
                } catch(\Exception $ex) {}
            }
        }

        //If there is no show update for specific user 
        //than we will show user subscribed podcast
        $item_type = "Notification";
        if (count($data) == 0) {
            $data = $this->userSubscribedShow($user);
            $item_type = "Show";
        }

        $message = '';
        if(count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Notification.notification_not_found_lbl");
        }
        
        if($item_type == "Notification") {
            $total = $feed_items->total();
            $per_page = $feed_items->perPage();
            $pages = ceil($feed_items->total()/$feed_items->perPage());
        } else {
            $total = count($data);
            $per_page = 10;
            $pages = 1;
        }
        
        return response()->api([
            'status' => true, 
            'data' => [
                'total' => $total,
                'per_page' => $per_page,
                'pages' => $pages,
                'items' => $data,
                'message' => $message
            ]
        ]);
    }    

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    protected function userSubscribedShow($user) {
        
        $user_shows = $user->shows()->withCount('episodes')->take(10)->get(['id', 'title', 'image']);
        
        $podcastData = [];
        foreach($user_shows as $item) {

            $episodes = Episode::withCount('like', 'comments')->where('show_id', $item->id)
                ->orderby('updated_at', 'DESC')
                ->first(['id', 'title','show_id', 'mp3', 'duration', 'updated_at', 'listen_count', 'explicit']);            
            $podcastData[] = [
                'id' => $item->id,
                'type' => NotificationRepo::SHOW_UPDATE,
                'show_id' => $item->id,
                'show_name' => trim(str_replace("\n", "", html_entity_decode(@$item->title))),
                'show_image' => ! empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                'episode_count' => 1,
                'display_text' => html_entity_decode($episodes->title),
                'episode_data' => $this->episode_list($episodes),
                'read_status' => 1,
                'time_status' => 1
            ];
        }
        
        return $podcastData;        
    }

    /**
     * Undocumented function
     *
     * @param [type] $episodes
     * @return void
     */
    protected function episode_list($episode)
    {        
        $data[] = [
            'episode_id' => $episode->id,
            'episode_name' => html_entity_decode($episode->title),
            'episode_audio' => $episode->getAudioLink(),
            'duration' => $episode->getDurationText(),            
            'display_text' => 'has a new episode ' . html_entity_decode($episode->title),
            'listen_count' => Helper::shorten_count($episode->listen_count), 
            "explicit" => $episode->show->explicit,
            'last_update' => $episode->updated_at->diffForHumans()
        ];        
        
        return $data;
    }

    /**
     * Set Read/Unread Notification Status.
     *
     * @param Request $request
     * @return mixed
     */

    public function set_notification_status (Request $request)
    {
    	$user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $validator = Validator::make($request->all(), [
            'feed_id' => 'required',
            'status' => 'required'
        ], [
            'feed_id.required' => config('language.' . $this->getLocale() . ".Notification.missing_parameter_id_lbl"),
            'status.required' => config('language.' . $this->getLocale() . ".Notification.missing_parameter_status_lbl")
        ]);
        
        if ($validator->fails()) {
            return response()->api([
                'status' => false, 
                'message' => $validator->errors()->first()
            ]);
        }

        $read_info = $user->user_notification_status()->find($request->feed_id);
        
        if($read_info) {
            return response()->api([
                 'status' => true,
                 'message' => ''
             ]);
        }

        $feedInfo = Feed::find($request->feed_id, ['typeable_id', 'type']);

        $user_status_feed[] = [
            'user_id' => $user->id,
            'feed_id' => $request->feed_id,
            'typeable_id' => $feedInfo->typeable_id,
            'type' => $feedInfo->type,
            'status' => $request->status
        ];
				  
        //Sync data with user
        $user->user_notification_status()->attach($user_status_feed);

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Notification.success_status_lbl")
        ]);
    }

    /**
    * This function is use for count new notification
    * @param Request $request
    * @return type
    */
    public function user_notification_count(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $boards = $user->boards()->pluck('id')->all();       
        
        $types = [
            'App\\Models\\Board' => ['board_user', 'board_id', 'user_id'],
            'App\\Models\\Show' => ['show_user', 'show_id', 'user_id']
        ];

        //'Board' => 'followers', 'Show' => 'subscribers'
        $query = Feed::query();

        //dd($user->toArray());
        
        foreach ($types as $type => $info) {
            $query->orWhere(function($q) use ($type, $info, $user) {
                $q->where('typeable_type', $type)
                ->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                    $que->select($info[1])->from($info[0])->where($info[2], $user->id);                    
                });
                
                // Created at grater than created at in the pivot table
                $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                    return $builder->select('created_at')->from($info[0])
                        ->whereRaw($info[1].' = tbl_user_feeds.typeable_id')
                        ->where($info[2], $user->id);
                });
                if ($user->last_access_time) {
                    $q->where('updated_at', '>=', $user->last_access_time);
                }
            });
        }

        $query->orWhere(function($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\User")
                ->where('type', NotificationRepo::FOLLOW_USER)
                ->where('data', 'LIKE', '%"'.$user->id.'"%');
                if ($user->last_access_time) {
                    $q->where('updated_at', '>=', $user->last_access_time);
                }
        });
    
        $query->orWhere(function($q) use ($boards, $user) {            
            foreach($boards as $board) {
                $q->orWhere('data', 'LIKE', '%"'.$board.'"%');
                $q->where('typeable_type', 'App\\Models\\User');
                $q->where('type', NotificationRepo::FOLLOW_BOARD);
                if ($user->last_access_time) {
                    $q->where('updated_at', '>=', $user->last_access_time);
                }
            }
        });

        // $query->orWhere(function($q) use ($user) {
        //     $q->where('typeable_type', "App\\Models\\EpisodeUserComment")
        //         ->where('data', 'LIKE', '%"'.$user->id.'"%');
        //     if ($user->last_access_time) {
        //         $q->where('updated_at', '>=', $user->last_access_time);
        //     }
        // });
        
        //echo $query->toSql();
        //$feedIds = $query->pluck('id')->all();

        $user_queue_count = \DB::table('user_queue')->where('user_id', $user->id)->count();

        return response()->api([
            'status' => true,
            'message' => '',
            'data' => [
                'notification_count' => $query->count() > 99 ? "99+" : Helper::shorten_count($query->count()),
                'queue_count' => Helper::shorten_count($user_queue_count),
                //'notification_count2' => Helper::shorten_count($query->count())
            ]
        ]);
    }

    /* New Code Added */

    /**
    * This function is use for display podcast notification v2
    * @param Request $request
    * @return type
    */
    public function show_notification_v2 (Request $request) 
    {
        try {
            $user = Auth::guard('api')->user();
            
            //check if user exist or not
            if (!$user) {
                $response = [
                    'status' => false,
                    'code' => 401,
                    'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                ];
                return $this->sendAuthErrorResponse($response, 401);
            }
                
            $types = ['App\\Models\\Show' => ['show_user', 'show_id', 'user_id']];
        
            $query = ShowNotification::query();
            foreach ($types as $type => $info) {
                $query->orWhere(function($q) use ($type, $info, $user) {
                    $q->where('typeable_type', $type)
                    //->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                    ->where('type', Notification::SHOW_UPDATE)
                    ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                        $que->select($info[1])->from($info[0])->where($info[2], $user->id);                     
                    });
                    // Table: $info[0]
                    // Created at grater than created at in the pivot table
                    $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                        return $builder->select('created_at')->from($info[0])
                            ->whereRaw($info[1].' = tbl_show_notifications.typeable_id')
                            ->where($info[2], $user->id);
                    // ->orderBy('created_at', 'DESC')->take(1);
                    });               
                });
            }        
        
            $feed_items = $query->orderBy('updated_at', 'DESC')
                ->paginate(config('config.pagination.notification_list'));
                
            $data = [];
            foreach ($feed_items as $feed_item) {
                //dump($feed_item->type);
                if (!empty($this->showNotificationMap[$feed_item->type])) {
                    try {
                        $data[] = with(new $this->showNotificationMap[$feed_item->type]($feed_item,$user))->response();
                    } catch(\Exception $ex) {}
                }
            }
    
            $message = '';
            if(count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Notification.notification_not_found_lbl");
            }
        
            if($request->no_paginate == 'Yes') {
                return response()->api([
                    'status' => true, 
                    'data' => [
                        'items' => $data, 
                        'message' => $message
                    ]
                ]);
            }        
    
            return response()->api([
                'status' => true, 
                'data' => [
                    'total' => $feed_items->total(), 
                    'per_page' => $feed_items->perPage(),
                    'pages' => ceil($feed_items->total()/$feed_items->perPage()),
                    'items' => $data, 
                    'message' => $message
                ]
            ]);
        } catch(\Exception $ex) {
            return response()->api([
                'status' => true, 
                'data' => [
                    'items' => [], 
                    'message' => ""
                ]
            ]);
        }        
    }

    /**
    * This function is use for display podcast notification on feed section
    * @param Request $request
    * @return type
    */
    public function feed_show_notification_v2 (Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            
            //check if user exist or not
            if (!$user) {                
                $response = [
                    'status' => true,
                    'data' => [
                        'oauth_enabled' => 0,
                        'total' => 0,
                        'per_page' => 10,
                        'pages' => 1,
                        'items' => [],
                        'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                    ]
                ];
                return $this->sendAuthErrorResponse($response, 401);
                //return $this->sendAuthErrorResponse($response, 200);
            }
    
            $types = ['App\\Models\\Show' => ['show_user', 'show_id', 'user_id']];
    
            // 'Board' => 'followers', 'Show' => 'subscribers'
            $query = ShowNotification::query();
            foreach ($types as $type => $info) {
                $query->orWhere(function($q) use ($type, $info, $user) {
                    $q->where('typeable_type', $type)
                    //->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                    ->where('type', Notification::SHOW_UPDATE)
                    ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                        $que->select($info[1])->from($info[0])->where($info[2], $user->id);                    
                    });
                    // Table: $info[0]
                    // Created at grater than created at in the pivot table
                    $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                        return $builder->select('created_at')->from($info[0])
                            ->whereRaw($info[1].' = tbl_show_notifications.typeable_id')
                            ->where($info[2], $user->id);
                    // ->orderBy('created_at', 'DESC')->take(1);
                    });               
                });
            }        
    
            $feed_items = $query->orderBy('updated_at', 'DESC')
                ->paginate(config('config.pagination.notification_list'));
            
            $data = [];
            foreach ($feed_items as $feed_item) {
                if (!empty($this->showNotificationMap[$feed_item->type])) {
                    try {
                        $data[] = with(new $this->showNotificationMap[$feed_item->type]($feed_item,$user))->response();
                    } catch(\Exception $ex) {}
                }
            }
    
            //If there is no show update for specific user 
            //than we will show user subscribed podcast
            $item_type = "Notification";
            if (count($data) == 0) {
                $data = $this->userSubscribedShow($user);
                $item_type = "Show";
            }
    
            $message = '';
            if(count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Notification.notification_not_found_lbl");
            }
            
            if($item_type == "Notification") {
                $total = $feed_items->total();
                $per_page = $feed_items->perPage();
                $pages = ceil($feed_items->total()/$feed_items->perPage());
            } else {
                $total = count($data);
                $per_page = 10;
                $pages = 1;
            }
            
            return response()->api([
                'status' => true, 
                'data' => [
                    'oauth_enabled' => $user->oauth_enabled,
                    'total' => $total,
                    'per_page' => $per_page,
                    'pages' => $pages,
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        } catch(\Exception $ex) {
            return response()->api([
                'status' => true, 
                'data' => [
                    'items' => [], 
                    'message' => ""
                ]
            ]);
        }
        
    }
    
    /**
     * Set Read/Unread Notification Status v2
     *
     * @param Request $request
     * @return mixed
     */

    public function set_notification_status_v2 (Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            
            if (!$user) {
                $response = ['data' => [
                    'status' => false,
                    'code' => 401,
                    'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                ]];
                return $this->sendAuthErrorResponse($response, 401);
            }
            
            $validator = Validator::make($request->all(), [
                'feed_id' => 'required',
                'status' => 'required'
            ], [
                'feed_id.required' => config('language.' . $this->getLocale() . ".Notification.missing_parameter_id_lbl"),
                'status.required' => config('language.' . $this->getLocale() . ".Notification.missing_parameter_status_lbl")
            ]);
            
            if ($validator->fails()) {
                return response()->api([
                    'status' => false, 
                    'message' => $validator->errors()->first()
                ]);
            }
    
            $read_info = $user->user_show_notification_status()->find($request->feed_id);
            
            if($read_info) {
                return response()->api([
                     'status' => true,
                     'message' => ''
                 ]);
            }
    
            $feedInfo = ShowNotification::find($request->feed_id, ['typeable_id', 'type']);
    
            $user_status_feed[] = [
                'user_id' => $user->id,
                'notification_id' => $request->feed_id,
                'typeable_id' => $feedInfo->typeable_id,
                'type' => $feedInfo->type,
                'status' => $request->status
            ];
                      
            //Sync data with user
            $user->user_show_notification_status()->attach($user_status_feed);
    
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Notification.success_status_lbl")
            ]);
        } catch(\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => ''
            ]);
        }    	
    }

    /**
    * This function is use for count new notification v2
    * @param Request $request
    * @return type
    */
    public function user_notification_count_v2(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            
            //check if user exist or not
            if (!$user) {
                $response = [
                    'status' => false,
                    'code' => 401,
                    'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                ];
                return $this->sendAuthErrorResponse($response, 401);
            }
    
            $boards = $user->boards()->pluck('id')->all();       
            
            $types = [
                'App\\Models\\Board' => ['board_user', 'board_id', 'user_id'],
                //'Show' => ['show_user', 'show_id', 'user_id']
            ];
    
            //'Board' => 'followers', 'Show' => 'subscribers'
            $query = Feed::query();
    
            //dd($user->toArray());
            
            foreach ($types as $type => $info) {
                $query->orWhere(function($q) use ($type, $info, $user) {
                    $q->where('typeable_type', $type)
                    ->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                    ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                        $que->select($info[1])->from($info[0])->where($info[2], $user->id);                    
                    });
                    
                    // Created at grater than created at in the pivot table
                    $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                        return $builder->select('created_at')->from($info[0])
                            ->whereRaw($info[1].' = tbl_user_feeds.typeable_id')
                            ->where($info[2], $user->id);
                    });
                    if ($user->last_access_time) {
                        $q->where('updated_at', '>=', $user->last_access_time);
                    }
                });
            }
    
            $query->orWhere(function($q) use ($user) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', NotificationRepo::FOLLOW_USER)
                    ->where('data', 'LIKE', '%"'.$user->id.'"%');
                    if ($user->last_access_time) {
                        $q->where('updated_at', '>=', $user->last_access_time);
                    }
            });
        
            $query->orWhere(function($q) use ($boards, $user) {            
                foreach($boards as $board) {
                    $q->orWhere('data', 'LIKE', '%"'.$board.'"%');
                    $q->where('typeable_type', 'App\\Models\\User');
                    $q->where('type', NotificationRepo::FOLLOW_BOARD);
                    if ($user->last_access_time) {
                        $q->where('updated_at', '>=', $user->last_access_time);
                    }
                }
            });

            /* New Code Added 01/09/2018 */
            foreach($this->like_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time) {
                        $q->where('updated_at', '>=', $user->last_access_time);
                    }
                }); 
            }
    
            foreach($this->comment_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time) {
                        $q->where('updated_at', '>=', $user->last_access_time);
                    }
                }); 
            }
            
            foreach($this->tag_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time) {
                        $q->where('updated_at', '>=', $user->last_access_time);
                    }
                });
            }
    
            // $query->orWhere(function($q) use ($user) {
            //     $q->where('typeable_type', "App\\Models\\EpisodeUserComment")
            //         ->where('data', 'LIKE', '%"'.$user->id.'"%');
            //     if ($user->last_access_time) {
            //         $q->where('updated_at', '>=', $user->last_access_time);
            //     }
            // });
    
            /** New Notification for Count */
            $notification_types = [
                'App\\Models\\Show' => ['show_user', 'show_id', 'user_id']
            ];
    
            $query_notification = ShowNotification::query();
    
            //dd($user->toArray());
            
            foreach ($notification_types as $notification_type => $notification_info) {
                $query_notification->orWhere(function($q) use ($notification_type, $notification_info, $user) {
                    $q->where('typeable_type', $notification_type)
                    ->where('type', Notification::SHOW_UPDATE)
                    ->whereIn('typeable_id', function($que) use ($user, $notification_info, $notification_type) {
                        $que->select($notification_info[1])->from($notification_info[0])->where($notification_info[2], $user->id);                    
                    });
                    
                    // Created at grater than created at in the pivot table
                    $q->where('created_at', '>=', function ($builder) use ($notification_info, $user) {
                        return $builder->select('created_at')->from($notification_info[0])
                            ->whereRaw($notification_info[1].' = tbl_show_notifications.typeable_id')
                            ->where($notification_info[2], $user->id);
                    });
                    if ($user->last_access_time) {
                        $q->where('updated_at', '>=', $user->last_access_time);
                    }
                });
            }
            /** New Notification for Count */
    
            $total_notification_count = $query_notification->count() + $query->count();
            
            //echo $query->toSql();
            //$feedIds = $query->pluck('id')->all();
    
            $user_queue_count = \DB::table('user_queue')->where('user_id', $user->id)->count();
    
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'notification_count' => $total_notification_count > 99 ? "99+" : Helper::shorten_count($total_notification_count),
                    'social_count' => $query->count(),
                    'podcast_count' => $query_notification->count(),
                    'queue_count' => Helper::shorten_count($user_queue_count)
                ]
            ]);
        } catch(\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => ''
            ]);
        }        
    }

    /**
     * This function is use for social notification count
     *
     * @param Request $request
     * @return void
     */
    public function user_notification_podcast_social_count(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            
            //check if user exist or not
            if (!$user) {
                $response = [
                    'status' => false,
                    'code' => 401,
                    'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                ];
                return $this->sendAuthErrorResponse($response, 401);
                //return $this->sendAuthErrorResponse($response, 200);
            }
    
            $boards = $user->boards()->pluck('id')->all();       
            
            $types = [
                'App\\Models\\Board' => ['board_user', 'board_id', 'user_id'],
                //'Show' => ['show_user', 'show_id', 'user_id']
            ];
    
            //'Board' => 'followers', 'Show' => 'subscribers'
            $query = Feed::query();
    
            //dd($user->toArray());
            
            foreach ($types as $type => $info) {
                $query->orWhere(function($q) use ($type, $info, $user) {
                    $q->where('typeable_type', $type)
                    ->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                    ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                        $que->select($info[1])->from($info[0])->where($info[2], $user->id);                    
                    });
                    
                    // Created at grater than created at in the pivot table
                    $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                        return $builder->select('created_at')->from($info[0])
                            ->whereRaw($info[1].' = tbl_user_feeds.typeable_id')
                            ->where($info[2], $user->id);
                    });
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                });
            }
    
            $query->orWhere(function($q) use ($user) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', NotificationRepo::FOLLOW_USER)
                    ->where('data', 'LIKE', '%"'.$user->id.'"%');
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
            });
        
            $query->orWhere(function($q) use ($boards, $user) {            
                foreach($boards as $board) {
                    $q->orWhere('data', 'LIKE', '%"'.$board.'"%');
                    $q->where('typeable_type', 'App\\Models\\User');
                    $q->where('type', NotificationRepo::FOLLOW_BOARD);
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                }
            });

            /* New Code Added 01/09/2018 */
            foreach($this->like_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                }); 
            }
    
            foreach($this->comment_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                }); 
            }
            
            foreach($this->tag_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                });
            }
    
            // $query->orWhere(function($q) use ($user) {
            //     $q->where('typeable_type', "App\\Models\\EpisodeUserComment")
            //         ->where('data', 'LIKE', '%"'.$user->id.'"%');
            //     if ($user->last_access_time) {
            //         $q->where('updated_at', '>=', $user->last_access_time);
            //     }
            // });
    
            /** New Notification for Count */
            $notification_types = [
                'App\\Models\\Show' => ['show_user', 'show_id', 'user_id']
            ];
    
            $query_notification = ShowNotification::query();
    
            //dd($user->toArray());
            
            foreach ($notification_types as $notification_type => $notification_info) {
                $query_notification->orWhere(function($q) use ($notification_type, $notification_info, $user) {
                    //$q->where('typeable_type', $notification_type)
                    //->where('type', Notification::SHOW_UPDATE)
                    $q->whereIn('typeable_id', function($que) use ($user, $notification_info, $notification_type) {
                        $que->select($notification_info[1])->from($notification_info[0])->where($notification_info[2], $user->id);                    
                    });
                    
                    // Created at grater than created at in the pivot table
                    $q->where('created_at', '>=', function ($builder) use ($notification_info, $user) {
                        return $builder->select('created_at')->from($notification_info[0])
                            ->whereRaw($notification_info[1].' = tbl_show_notifications.typeable_id')
                            ->where($notification_info[2], $user->id);
                    });
                    if ($user->last_access_time) {
                        $q->where('updated_at', '>=', $user->last_access_time);
                    }
                });
            }
            /** New Notification for Count */
    
            $total_notification_count = $query_notification->count() + $query->count();
            
            //echo $query->toSql();
            //$feedIds = $query->pluck('id')->all();
    
            $user_queue_count = \DB::table('user_queue')->where('user_id', $user->id)->count();
    
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'notification_count' => $query->count(),
                    'podcast_count' => $query_notification->count(),
                    'queue_count' => Helper::shorten_count($user_queue_count)
                ]
            ]);
        } catch(\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => ''
            ]);
        }        
    }
}
