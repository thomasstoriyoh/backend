<?php

namespace App\Http\Controllers\Api\new_version\V5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Show;
use App\Models\Episode;
use App\ClassesV3\Podcast4 as Podcast;
use Carbon\Carbon;
use App\ClassesV3\Common;
use App\Traits\Helper;
use App\Models\PopularShow;
use App\Traits\ResponseFormat;
use App\ClassesV3\V3_3\Search;
class ShowController extends Controller
{
    use ResponseFormat;

    /**
     * Shows page view / Search Page View.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $showData = new Podcast('api', $user);

        return $showData->Shows();
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function search(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => config('language.' . $this->getLocale() . ".Common.search_keyword_lbl")
        ]);

        if ($validator->fails()) {
            $request->keyword = '*';
            // return response()->api([
            //     'status' => false,
            //     'message' => $validator->errors()->first()
            // ]);
        }

        $showData = new Podcast('api', $user);

        return $showData->Search();
    }

    /**
     * Show Detail Page
     * @param Request $request
     * @return type
     */
    public function show(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $showData = new Podcast('api', $user);

        return $showData->Show();
    }

    /**
     * Show Detail Page
     * @param Request $request
     * @return type
     */
    public function episodes(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'show_id' => 'required'
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //All Episodes
        $query = Episode::published()->where('show_id', $request->show_id);

        //No of record per Page
        $noofrecords = config('config.pagination.episode');
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        $query->orderBy('date_created', 'DESC');

        if ($request->no_paginate == 'Yes') {
            $all_show_episode = $query->get(['id', 'title', 'duration', 'mp3', 'show_id', 'updated_at', 'date_created', 'listen_count', 'explicit']);
        } else {
            $all_show_episode = $query->paginate($noofrecords, ['id', 'title', 'duration', 'mp3', 'show_id', 'updated_at', 'date_created', 'listen_count', 'explicit']);
        }

        $data = [];
        foreach ($all_show_episode as $item) {
            $data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'category' => @implode(', ', @$item->categories()->pluck('title')->all()),
                'show_id' => $item->show->id,
                'show_title' => trim(str_replace("\n", '', html_entity_decode($item->show->title))),
                'image' => !empty($item->show->image) ? $item->show->getWSImage(200) : asset('uploads/default/show.png'),
                'duration' => $item->getDurationText(),
                'audio_file' => $item->getAudioLink(),
                'last_updated' => Carbon::parse($item->date_created)->format('jS M Y'), //$item->updated_at->diffForHumans(),
                'listen_count' => Helper::shorten_count($item->listen_count), 
                "explicit" => $item->show->explicit,
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'data' => [
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => $all_show_episode->total(),
                    'per_page' => $all_show_episode->perPage(),
                    'pages' => ceil($all_show_episode->total() / $all_show_episode->perPage()),
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        }
    }

    /**
     * This function is use for podcast autosearch
     *
     * @param Request $request
     * @return void
     */
    public function auto_search(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        /*$validator = Validator::make($request->all(), [
            'keyword' => 'required'
        ], [
            'keyword.required' => 'Please enter keyword.'
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $showData = Show::published()->where('title', 'LIKE', '%' . $request->keyword . '%')->has('episodes')
            ->take(10)->get(['id', 'title', 'image']);

        $data = [];
        foreach ($showData as $show) {
            $data[] = [
                'id' => $show->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No Podcast Found.';
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'items' => $data,
            ]
        ]);*/
        $search = new Search('web');

        return $search->auto_search();
    }

    /**
     * This function is use for podcast of the day
     *
     * @param Request $request
     * @return void
     */
    public function home_page_content_podcast_of_the_day(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }        

        $home_data = new Common('api');
        
        return $home_data->home_page_content_podcast_of_the_day();
    }

    /**
     * This function is use for subscribers list in show details
     *
     * @param Request $request
     * @return void
     */
    public function show_subscribers(Request $request) 
    {        
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'show_id' => 'required',
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $show = Show::published()->where('id', $request->show_id)
                ->first(['id', 'title', 'image', 'language', 'artist_name', 'description', 'updated_at']);

            if (!$show) {            
                return response()->api([
                    'status' => true,
                    'message' => 'No Show Found.',
                    'data' => []
                ]);
            }
            
            $followingIds = $user->following()->verified()->pluck('id')->all();
            
            $query = $show->subscribers();

            $query->whereIn('id', $followingIds)->where('id', '!=', $user->id)->verified()->take(10)->orderByRaw("RAND()");

            if($query->count() == 0) {
                $query = $show->subscribers()->where('id', '!=', $user->id)->verified()->take(10)->orderByRaw("RAND()");    
            }
            
            $user_arr = $query->get(['id', 'username', 'first_name', 'image']); 
            $User_Data = [];
            foreach ($user_arr as $user_data) {
                $User_Data[] = [
                    'name' => $user_data->first_name . ' ' . $user_data->last_name,
                    'image' => !empty($user_data->image) ? $user_data->getImage(100) : asset('uploads/default/user.png'),
                    'username' => $user_data->username
                ];
            }
            
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => $User_Data
            ]);
        } catch(\Exception $e){
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                'data' => []
            ]);
        }        
    }

    /**
     * This function is use for subscribers list in show details
     *
     * @param Request $request
     * @return void
     */
    public function show_lists(Request $request) {
        
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'show_id' => 'required',
        ], [
            'show_id.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            $show = Show::published()->where('id', $request->show_id)
                ->first(['id', 'title', 'image', 'language', 'artist_name', 'description', 'updated_at']);

            if (!$show) {            
                return response()->api([
                    'status' => true,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                    'data' => []
                ]);
            }
            
            //Get Show collection
            $charts = $show->getCollection()->take(10)->published()->get(['id', 'title', 'image', 'date', 'description']);
            
            $chart_arr = [];
            foreach ($charts as $chart) {
                $chart_arr[] = [
                    'id' => $chart->id,
                    'title' => $chart->title,
                    'date' => Carbon::parse($chart->date)->format('jS M Y'),
                    'description' => str_limit($chart->description, 100, '...'),
                    'image' => $chart->getImage(200),
                    'url_slug' => Helper::make_slug(trim($chart->title))
                ];
            }
            
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => $chart_arr
            ]);
        } catch(\Exception $e){
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl"),
                'data' => []
            ]);
        }        
    }

    
    /**
     * Popular Categories
     *
     * @param Request $request
     * @return void
     */
    public function popular_categories(Request $request) {
        
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'filter_category' => 'required',
        ], [
            'filter_category.required' => config('language.' . $this->getLocale() . ".Show.missing_parameter_category_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $subscribedShows = $user->subscribers()->pluck('id')->all();

        $popular_shows = PopularShow::where('category_id', $request->filter_category)->first();
        
        $data = [];
        $message = '';
        if(! is_null($popular_shows)) {
            $user_unsubscribed_shows = $popular_shows->shows()->published()
                ->whereNotIn('show_id', $subscribedShows)
                ->orderBy('no_of_subscribers', 'DESC')->paginate(10, ['id', 'title', 'image', 'no_of_subscribers']);
                    
            foreach ($user_unsubscribed_shows as $show) {
                $data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'no_of_episodes' => 0,//$show->episodes()->published()->count(),
                    'no_of_subscribers' => $show->subscribers->count(),
                    'subscribe' => 'false'
                ];
            }

            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
            }

            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $user_unsubscribed_shows->total(),
                    'per_page' => $user_unsubscribed_shows->perPage(),
                    'pages' => ceil($user_unsubscribed_shows->total() / $user_unsubscribed_shows->perPage()),
                    'items' => $data
                ]
            ]);
        }   
        
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
        }
        
        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => []
        ]);       
    }
}
