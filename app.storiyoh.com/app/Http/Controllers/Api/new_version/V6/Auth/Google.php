<?php

namespace App\Http\Controllers\Api\new_version\V6\Auth;

class Google implements HasAvatar
{
    /**
     * @param $id
     * @return string
     */
    public function getAvatar($id)
    {
        //return "https://www.googleapis.com/plus/v1/people/".$id."?fields=image&key=AIzaSyAuW9ZVyTrbMdp1dFGgf8JGCHo0FcVDjEY?sz=200";
        try {
            $url = 'https://www.googleapis.com/plus/v1/people/' . $id . '?fields=image%2Furl&key=' . config('config.g_plus') . '&fields=image';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            //dd($response);
            curl_close($ch);
            $d = json_decode($response);
            $profile_url = $d->{'image'}->{'url'};

            $url = explode('?', $profile_url);

            return $url[0] . '?sz=300';
        } catch (\Exception $e) {
            return '';
        }
    }
}
