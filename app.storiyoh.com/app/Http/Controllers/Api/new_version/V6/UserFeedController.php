<?php

namespace App\Http\Controllers\Api\new_version\V6;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Feed;
use App\ClassesV3\Feed as FeedRepo;
use App\UserFeedResponse\UserFeedResponse;
use App\UserFeedResponse\BoardFeedResponse;
use App\UserFeedResponse\EpisodeHeardFeedResponse;
use App\UserFeedResponse\ShowFeedResponse;
use App\UserFeedResponse\BoardFollowResponse;
use App\UserFeedResponse\SmartPlaylistFollowResponse;
use App\UserFeedResponse\PostResponse;
use App\UserFeedResponse\PostLikeFeedResponse;
use App\UserFeedResponse\PostCommentFeedResponse;
use App\UserFeedResponse\PostTagFeedResponse;

use App\Models\User;
use App\Traits\ResponseFormat;

class UserFeedController extends Controller
{
    use ResponseFormat;

    protected $feedMap = [
        FeedRepo::FOLLOW_USER => UserFeedResponse::class,
        FeedRepo::BOARD_UPDATE => BoardFeedResponse::class,
        FeedRepo::EPISODE_HEARD => EpisodeHeardFeedResponse::class,
        FeedRepo::SUBSCRIBE_SHOW => ShowFeedResponse::class,
        FeedRepo::FOLLOW_BOARD => BoardFollowResponse::class,
        FeedRepo::FOLLOW_SMARTLIST => SmartPlaylistFollowResponse::class,

        /** New Post Code Start */
        FeedRepo::NEW_SHOW_POST => PostResponse::class,
        FeedRepo::NEW_EPISODE_POST => PostResponse::class,
        FeedRepo::NEW_PLAYLIST_POST => PostResponse::class,
        FeedRepo::NEW_SMARTPLAYLIST_POST => PostResponse::class,
        FeedRepo::NEW_COLLECTION_POST => PostResponse::class,

        /** New Individual Share Post 21/05/2019 */
        FeedRepo::INDIVIDUAL_POST => PostResponse::class,

        /** Post Like Code Start */
        FeedRepo::SHOW_POST_LIKE => PostLikeFeedResponse::class,
        FeedRepo::EPISODE_POST_LIKE => PostLikeFeedResponse::class,
        FeedRepo::PLAYLIST_POST_LIKE => PostLikeFeedResponse::class,
        FeedRepo::SMARTPLAYLIST_POST_LIKE => PostLikeFeedResponse::class,
        FeedRepo::COLLECTION_POST_LIKE => PostLikeFeedResponse::class,

        /** New Individual Share Post 21/05/2019 */
        FeedRepo::INDIVIDUAL_POST_LIKE => PostLikeFeedResponse::class,

        /** Post Comment Code Start */
        FeedRepo::SHOW_POST_COMMENT => PostCommentFeedResponse::class,
        FeedRepo::EPISODE_POST_COMMENT => PostCommentFeedResponse::class,
        FeedRepo::PLAYLIST_POST_COMMENT => PostCommentFeedResponse::class,
        FeedRepo::SMARTPLAYLIST_POST_COMMENT => PostCommentFeedResponse::class,
        FeedRepo::COLLECTION_POST_COMMENT => PostCommentFeedResponse::class,

        /** New Individual Share Post 21/05/2019 */
        FeedRepo::INDIVIDUAL_POST_COMMENT => PostCommentFeedResponse::class,

        /** Post tags for all types */
        // FeedRepo::SHOW_POST_COMMENT_TAG => PostTagFeedResponse::class,
        // FeedRepo::EPISODE_POST_COMMENT_TAG => PostTagFeedResponse::class,
        // FeedRepo::PLAYLIST_POST_COMMENT_TAG => PostTagFeedResponse::class,
        // FeedRepo::SMARTPLAYLIST_POST_COMMENT_TAG => PostTagFeedResponse::class,
        // FeedRepo::COLLECTION_POST_COMMENT_TAG => PostTagFeedResponse::class,    
    ];

    /**
     * This function is use for my feed section
     *
     * @param Request $request
     * @return void
     */
    public function user_network_feed(Request $request)
    {
        $user_info = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user_info) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".UserFeedActivity.missing_parameter_username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //User Info
        $user = User::where('username', $request->username)
            ->whereNotNull('username')->where('username', '!=', '')->first();

        if (!$user) {
            return response()->api([
                'status' => false,
                'message' => 'No User Found.'
            ]);
        }

        $query = Feed::query();
        $query->orWhere(function ($q) use ($user) {
            $q->where('typeable_type', 'App\\Models\\User')
                ->whereIn('type', [
                    FeedRepo::FOLLOW_USER,
                    //FeedRepo::BOARD_UPDATE,
                    FeedRepo::EPISODE_HEARD,
                    FeedRepo::SUBSCRIBE_SHOW,
                    FeedRepo::FOLLOW_BOARD,
                    FeedRepo::FOLLOW_SMARTLIST,

                    FeedRepo::NEW_SHOW_POST,
                    FeedRepo::NEW_EPISODE_POST,
                    FeedRepo::NEW_PLAYLIST_POST,
                    FeedRepo::NEW_SMARTPLAYLIST_POST,
                    FeedRepo::NEW_COLLECTION_POST,

                    /** New Individual Share Post 21/05/2019 */
                    FeedRepo::INDIVIDUAL_POST,

                    FeedRepo::SHOW_POST_LIKE,
                    FeedRepo::EPISODE_POST_LIKE,
                    FeedRepo::PLAYLIST_POST_LIKE,
                    FeedRepo::SMARTPLAYLIST_POST_LIKE,
                    FeedRepo::COLLECTION_POST_LIKE,

                    /** New Individual Share Post 21/05/2019 */
                    FeedRepo::INDIVIDUAL_POST_LIKE,

                    FeedRepo::SHOW_POST_COMMENT,
                    FeedRepo::EPISODE_POST_COMMENT,
                    FeedRepo::PLAYLIST_POST_COMMENT,
                    FeedRepo::SMARTPLAYLIST_POST_COMMENT,
                    FeedRepo::COLLECTION_POST_COMMENT,

                    /** New Individual Share Post 21/05/2019 */
                    FeedRepo::INDIVIDUAL_POST_COMMENT

                    // FeedRepo::SHOW_POST_COMMENT_TAG,
                    // FeedRepo::EPISODE_POST_COMMENT_TAG,
                    // FeedRepo::PLAYLIST_POST_COMMENT_TAG,
                    // FeedRepo::SMARTPLAYLIST_POST_COMMENT_TAG,
                    // FeedRepo::COLLECTION_POST_COMMENT_TAG,
                ])
                ->where('typeable_id', $user->id);
        });
        $query->orWhere(function($q) use ($user) {            
            $q->where('typeable_type', 'App\\Models\\Board');
            $q->where('type', FeedRepo::BOARD_UPDATE);
            $q->where('owner_id', $user->id);            
        });

        $feed_items = $query->orderBy('updated_at', 'DESC')->paginate(10);

        $data = [];
        foreach ($feed_items as $feed_item) {
            if (!empty($this->feedMap[$feed_item->type])) {
                try {
                    if(with(new $this->feedMap[$feed_item->type]($feed_item, $user_info))->response()) {
                        $data[] = with(new $this->feedMap[$feed_item->type]($feed_item, $user_info))->response();
                    }
                } catch (\Exception $ex) {
                }
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No Feed Found.';
        }

        return response()->api([
            'status' => true,
            'data' => [
                'total' => $feed_items->total(),
                'per_page' => $feed_items->perPage(),
                'pages' => ceil($feed_items->total() / $feed_items->perPage()),
                'items' => $data,
                'message' => $message
            ]
        ]);
    }
}
