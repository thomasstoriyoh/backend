<?php

namespace App\Http\Controllers\Api\new_version\V7\Auth;

interface HasAvatar
{
    /**
     * @param $id
     * @return string
     */
    public function getAvatar($id);
}
