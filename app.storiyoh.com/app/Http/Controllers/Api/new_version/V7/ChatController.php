<?php

namespace App\Http\Controllers\Api\new_version\V7;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ResponseFormat;
use App\Traits\HelperV2;
use Exception;

class ChatController extends Controller
{
    use ResponseFormat, HelperV2;

    /**
     * This function is use for update sendbird id in tbl_users table
     */
    public function update_sendbird_id_to_user(Request $request)
    {
        $user = Auth::guard('api')->user();        

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'chat_type' => 'required',            
        ], [
            'chat_type.required' => config('language.' . $this->getLocale() . ".Chat.missing_parameter_chat_type_lbl"),            
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first(),
                'data'    => []
            ]);
        }

        try {
            //Update sendbird_id
            $chat_update = null;
            if($request->chat_type == "update") {
                $chat_update = config('config.sendbird_pre_text') . $user->id;
            }
            $user->timestamps = false;
            $user->update(["sendbird_id" => $chat_update]);

            return response()->api([
                'status' => true,
                'message' => "",
                'data'    => []
            ]);
        } catch(Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error"),
                'data'    => []
            ]);
        }
    }

    /**
     * This function is listing all sendbird user listings
     */
    public function sendbird_user_listings(Request $request)
    {
        $user = Auth::guard('api')->user();
        //dd($user->id);

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }        
        
        try {
            $query = User::approved()->verified()->whereNotNull('username')
                ->where('username', '!=', '')->where('id', '!=', $user->id)
                ->whereNotNull('sendbird_id')
                ->withCount(['follower' => function ($qry) use ($user) {
                    $qry->where('id', $user->id);
                }])
                ->orderBy('created_at', 'Desc');

            //Search Query
            if (!empty($request->keyword)) {
                $searchText = urldecode(trim($request->keyword));
                $query->where(function ($query) use ($searchText) {
                    $query->orWhere('first_name', 'LIKE', '%' . $searchText . '%');                    
                });
            }

            if ($request->no_paginate == 'Yes') {
                $users = $query->get(['id', 'first_name', 'last_name', 'username', 'image', 'sendbird_id']);
            } else {
                $users = $query->paginate(10, ['id', 'first_name', 'last_name', 'username', 'image', 'sendbird_id']);
            }            

            $data = [];
            foreach ($users as $item) {
                $data[] = [
                    "sendbird_id" => $item->sendbird_id,
                    'name' => $item->notification_format_name,
                    'username' => $item->username,
                    'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                    'follow_status' => $item->follower_count,
                ];
            }

            $message = '';
            if (count($data) == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
            }

            if ($request->no_paginate == 'Yes') {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'items' => $data
                    ]
                ]);
            } else {
                return response()->api([
                    'status' => true,
                    'message' => $message,
                    'data' => [
                        'total' => $users->total(),
                        'per_page' => $users->perPage(),
                        'pages' => ceil($users->total() / $users->perPage()),
                        'items' => $data
                    ]
                ]);
            }
        } catch (Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error"),
                'data'    => []
            ]);
        }
    }
}
