<?php

namespace App\Http\Controllers\Api\new_version\V7;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Validator;
use App\Traits\ResponseFormatNew;
use App\ClassesV3\V3_3\MarketPlaceV2;
//use Bcm\Paytm\BcmPaytmWallet;

class MarketPlaceController extends Controller
{
    use ResponseFormatNew;

    /**
     * check if marketplace is available or not
     *
     * @param Request $request
     * @return mixed
     */
    public function check_if_marketplace_available_for_country()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->check_if_marketplace_available_for_country();
    }

    /**
     * Promotion Banner
     *
     * @param Request $request
     * @return mixed
     */
    public function promo_banner()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->promo_banner();
    }

    /**
     * Newly created series
     */
    public function new_series()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->new_series();
    }

    /**
     * Newly created series
     */
    public function updated_series()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->updated_series();
    }

    /**
     * Newly created series
     */
    public function new_episodes()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->new_episodes();
    }

    /**
     * Show Details Page
     */
    public function show_details()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->show_details();
    }

    /**
     * Show Cast & Crew Page
     */
    public function show_cast_crew()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->show_cast_crew();
    }

    /**
     * Show Episode Listing
     */
    public function show_episodes()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->show_episodes();
    }

    /**
     * Show Details Page
     */
    public function episode_details()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->episode_details();
    }

    /**
     * Episode Cast & Crew Page
     */
    public function episode_cast_crew()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->episode_cast_crew();
    }

    /***
     * Create temporary URL for episode
     * 
     */
    public function single_signedin_url()
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->single_signedin_url();
    }


    /**
     * This webservices is use for seller profile
     * 
     */
    public function seller_profile()
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->seller_profile();
    }

    /**
     * This webservices is use for seller shows
     * 
     */
    public function seller_series()
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->seller_series();
    }

    /**
     * This webservices is use for seller episodes
     * 
     */
    public function seller_episodes()
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->seller_episodes();
    }

    /**
     * This function is use for user already
     * purchase this item or not
     * 
     */
    public function check_recipient_gift_status()
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->check_recipient_gift_status();
    }

    /***
     * Create temporary URL for episode
     * 
     */
    public function single_signedin_url_resaurces()
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->single_signedin_url_resaurces();
    }

    /**
     * Listing for highlighted episodes
     */
    public function highlighted_episodes()
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->highlighted_episodes();
    }

    /**
     * This function is use for check if 
     * user email exist or not 
     * if not then create temporary user
     */
    public function check_recipient_email_for_gift()
    {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Check if user is guest user or not        
        $isUserVerified = \App\Traits\HelperV2::isUserVerified($user);
        if (!$isUserVerified['status']) {
            $response = [
                'status' => false,
                'message' => $isUserVerified['message']
            ];
            return response()->json($response, 200);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->check_recipient_email_for_gift();
    }

    /**
     * Newly created dashboard standalone
     * episodes shots banner
     */
    public function standalone_episodes_shots()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->standalone_episodes_shots();
    }

    /**
     * Newly created standalone episodes
     */
    public function standalone_episodes()
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $content = new MarketPlaceV2('api', $user);

        return $content->standalone_episodes();
    }
}
