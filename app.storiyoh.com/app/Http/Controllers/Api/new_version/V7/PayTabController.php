<?php

namespace App\Http\Controllers\Api\new_version\V7;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Traits\ResponseFormat;
use App\ClassesV3\V3_3\IP2Location;
use App\Models\MarketplaceCountry;
use App\Models\Show;
use Carbon\Carbon;
use App\Models\EpisodeDetail;
use App\Traits\HelperV2;
use App\Models\Order;
use App\Models\ShowPurchase;
use App\Models\EpisodePurchase;
use App\Jobs\InvoiceEmail;
use App\Jobs\SendEmailUser;
use App\ClassesV3\Feed;
use App\Jobs\SendPushNotification;
use App\Models\UserAddress;

class PayTabController extends Controller
{
    use ResponseFormat, HelperV2;

    /***
     * Getting Basic Information 
     * about user
     */
    public function get_user_address_info(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        //Check if user is guest user or not
        $isUserVerified = $this->isUserVerified($user);
        if (!$isUserVerified['status']) {
            $response = [
                'status' => false,
                'message' => $isUserVerified['message']
            ];
            return response()->json($response, 200);
        }

        $ip_address = $request->ip_address;
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();

        //Check if marketplace available in user country
        $marketplace_country = MarketplaceCountry::where("status", "Published")
            ->where("admin_status", "Approved")
            ->where('country_alpha2_code', $location['data']['country_code'])
            ->first(["id", "title", "country_alpha2_code", "currency_id"]);

        // $marketplace_country = MarketplaceCountry::where("admin_status", "Approved")
        //     ->where('country_alpha2_code', $location['data']['country_code'])
        //     ->first(["id", "title", "country_alpha2_code", "currency_id"]);

        /* Check marketplace is available for country if availabel display wishlist
        otherwise display empty wishlist 
        */
        if (empty($marketplace_country)) {
            return response()->api([
                'status'    => false,
                'message'   => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),                
            ]);
        }
        /* End */

        $user_address = UserAddress::where('user_id', $user->id);

        if ($user_address->count() > 0) {
            $user_address = $user_address->first();
            return response()->api([
                'status' => true,
                'message' => "",
                'dataFlag' => 1,
                'data'    => [
                    "address" => is_null($user_address->address) ? "" : $user_address->address,
                    "city" => is_null($user_address->city) ? "" : $user_address->city,
                    "state" => is_null($user_address->state) ? "" : $user_address->state,
                    "country" => is_null($user_address->country) ? "" : $user_address->country,
                    "phone" => is_null($user_address->phone) ? "" : $user_address->phone,
                    "pincode" => is_null($user_address->pincode) ? "" : $user_address->pincode
                ]
            ]);
        } else {
            //Get city, state and country from ip2location database
            $ip_address = $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();

            $city = empty($location['data']['city_name']) ? "" : $location['data']['city_name'];
            $state = empty($location['data']['region_name']) ? "" : $location['data']['region_name'];
            $country = empty($location['data']['country_name']) ? "" : $location['data']['country_name'];

            return response()->api([
                'status' => true,
                'message' => "",
                'dataFlag' => 0,
                'data'    => [
                    "address" => "",
                    "city" => $city,
                    "state" => $state,
                    "country" => $country,
                    "phone" => "",
                    "pincode" => ""
                ]
            ]);
        }
    }

    /***
     * Update Basic Information 
     */
    public function update_user_address_info(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        //Check if user is guest user or not
        $isUserVerified = $this->isUserVerified($user);
        if (!$isUserVerified['status']) {
            $response = [
                'status' => false,
                'message' => $isUserVerified['message']
            ];
            return response()->json($response, 200);
        }

        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'phone' => 'required|numeric',
            'pincode' => 'required',
        ], [
            // 'address.required' => "",
            // 'city.required' => "",
            // 'state.required' => "",
            // 'country.required' => "",
            // 'phone.required' => "",
            // 'pincode.required' => "",
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first(),
            ]);
        }

        UserAddress::updateOrCreate(
            ['user_id' => $user->id],
            [
                'address' => $request->address,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'phone' => $request->phone,
                'pincode' => $request->pincode,
            ]
        );

        return response()->api([
            'status' => true,
            'message' => "Your Information has been saved for next time."
        ]);
    }

    /**
     *This function is use for create aproval url
     */
    public function create_order(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        //Check if user is guest user or not
        $isUserVerified = $this->isUserVerified($user);
        if (!$isUserVerified['status']) {
            $response = [
                'status' => false,
                'message' => $isUserVerified['message']
            ];
            return response()->json($response, 200);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'product_type' => 'required',
            'platform' => 'required',
            'payment_mode' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl"),
            'product_type.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_type_lbl"),
            'platform.required' => config('language.' . $this->getLocale() . ".Marketplace.platform_missing"),
            'payment_mode.required' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_missing"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first(),
                'data'    => [
                    'order_id' => "",
                    'currency' => "",
                    'amount' => 0
                ]
            ]);
        }

        try {
            //Ip2Location code start
            $ip_address = $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();

            if (empty($location['data']['country_code']) || $location['data']['country_code'] == "-") {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
                    'data'    => [
                        'order_id' => "",
                        'currency' => "",
                        'amount' => 0
                    ]
                ]);
            }

            //Check if marketplace available in user country
            $marketplace_country = MarketplaceCountry::where("status", "Published")
                ->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id", "tax_name", "tax_price"]);

            // $marketplace_country = MarketplaceCountry::where("admin_status", "Approved")
            //     ->where('country_alpha2_code', $location['data']['country_code'])
            //     ->first(["id", "title", "country_alpha2_code", "currency_id", "tax_name", "tax_price"]);

            if (empty($marketplace_country)) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
                    'data'    => [
                        'order_id' => "",
                        'currency' => "",
                        'amount' => 0
                    ]
                ]);
            }

            $flag = false;
            if ($request->product_type == 'Series') {
                $content = Show::where("id", $request->content_id)->where('content_type', '!=', 0)->whereNotNull('user_id')
                    ->where('status', 'Published')->first(['id', 'user_id']);
                if ($content) {
                    //First we check if this show already purchased by that user
                    $check = $this->check_if_item_purchase_by_user($content->user_id, $user->id, $content->id, 'series');
                    if ($check == 'yes') {
                        return response()->api([
                            'status' => true,
                            'purchase' => 'yes',
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_already_purchased_lbl"),
                            'data'    => [
                                'order_id' => "",
                                'currency' => "",
                                'amount' => 0
                            ]
                        ]);
                    }
                    //Now we get price for current country and currency id
                    $product_ids = $content->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                    $product_type = 1;

                    $flag = true;
                } else {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found"),
                        'data'    => [
                            'order_id' => "",
                            'currency' => "",
                            'amount' => 0
                        ]
                    ]);
                }
            } elseif ($request->product_type == 'Episode') {
                $content = EpisodeDetail::where("episode_id", $request->content_id)->where('status', 'Published')->first(['id', 'episode_id', 'user_id', 'content_type']);
                if ($content) {
                    //First we check if this show already purchased by that user
                    $check = $this->check_if_item_purchase_by_user($content->user_id, $user->id, $content->episode_id, 'episode', $content->episode->show->id);
                    //dd($check);
                    if ($check == 'yes') {
                        return response()->api([
                            'status' => true,
                            'purchase' => 'yes',
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_already_purchased_lbl"),
                            'data'    => [
                                'order_id' => "",
                                'currency' => "",
                                'amount' => 0
                            ]
                        ]);
                    }
                    //Now we get price for current country and currency id
                    $product_ids = $content->episode->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                    $product_type = ($content->content_type + 1);

                    $flag = true;
                } else {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found"),
                        'data'    => [
                            'order_id' => "",
                            'currency' => "",
                            'amount' => 0
                        ]
                    ]);
                }
            }

            if ($flag == true) {
                //Create Invoice Id
                $maxId = \DB::table('orders')->max('id');
                $maxPurchaseId = 0;
                if (!is_null($maxId)) {
                    $maxPurchaseId = $maxId;
                }
                $maxPurchaseId = $maxPurchaseId + 1;
                $date_time = date("ymd");
                $invoice_no = "STR" . $date_time . $maxPurchaseId;

                //$cust_transaction_id = $this->getRandomNumber(10) . $date_time;

                //Final Price Calculation
                $net_amount = $selling_price;
                $tax_amount = 0;

                //$tax_percent = $marketplace_country->tax_price;
                //$tax_amount = round($selling_price - ($selling_price * (100 / (100 + $tax_percent))));
                //$net_amount = round($selling_price - $tax_amount);

                $invoice_total = ($net_amount + $tax_amount);

                //Insert data into our tables tbl_orders
                $insertArray = [
                    "invoice_number" => $invoice_no,
                    "invoice_date" => Carbon::now(),
                    "user_id" => $user->id,
                    "purchase_type" => 2,
                    "product_type" => $product_type,
                    "product_id" => $request->content_id,
                    "invoice_type" => 1,
                    "country_id" => $marketplace_country->id,
                    "billing_city" => $location['data']['city_name'],
                    "billing_state" => $location['data']['region_name'],
                    "billing_country" => $location['data']['country_name'],
                    "ip_address" => $request->ip(),
                    "gateway" => "PayTabs",
                    "platform" => $request->platform,
                    "payment_mode" => $request->payment_mode,
                    "billing_type" => "one_time",
                    "currency" => $marketplace_country->currency->title,
                    "net_amount" => $net_amount,
                    "tax_name" => $marketplace_country->tax_name,
                    "tax_percent" => $marketplace_country->tax_price,
                    "tax_amount" => $tax_amount,
                    "invoice_total" => $invoice_total,
                    "status" => "initiated",
                ];

                $orderData = Order::create($insertArray);

                if (!$orderData) {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Common.error"),
                        'data'    => [
                            'order_id' => "",
                            'currency' => "",
                            'amount' => 0
                        ]
                    ]);
                }

                return response()->api([
                    "status" => true,
                    "message" => "",
                    "data" => [
                        'order_id' => $invoice_no,
                        'currency' => $marketplace_country->currency->title,
                        'amount' => $invoice_total,
                        //"billing_city" => $location['data']['city_name'],
                        //"billing_state" => $location['data']['region_name'],
                        //"billing_country" => $location['data']['country_name'],
                    ]
                ]);
            } else {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.error"),
                    'data'    => [
                        'order_id' => "",
                        'currency' => "",
                        'amount' => 0
                    ]
                ]);
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error"),
                'data'    => [
                    'order_id' => "",
                    'currency' => "",
                    'amount' => 0
                ]
            ]);
        }
    }

    /**
     *This function is use for create aproval url
     */
    public function verify_order(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        //Check if user is guest user or not
        $isUserVerified = $this->isUserVerified($user);
        if (!$isUserVerified['status']) {
            $response = [
                'status' => false,
                'message' => $isUserVerified['message']
            ];
            return response()->json($response, 200);
        }

        if ($request->paytabs_status == "S") {
            $validator = Validator::make($request->all(), [
                'paytabs_transaction_id' => 'required',
                'paytabs_order_id' => 'required',
                'paytabs_status' => 'required'
            ], [
                'paytabs_transaction_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_payment_id_lbl"),
                'paytabs_order_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_order_id_lbl"),
                'paytabs_status.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_status_lbl")
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'paytabs_order_id' => 'required',
                'paytabs_status' => 'required'
            ], [
                'paytabs_order_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_order_id_lbl"),
                'paytabs_status.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_status_lbl")
            ]);
        }


        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first(),
                'data'    => []
            ]);
        }


        try {
            $arrayUpdate['transaction_id'] = !empty($request->paytabs_transaction_id) ? $request->paytabs_transaction_id : "";
            $arrayUpdate['invoice_date'] = Carbon::now();
            $arrayUpdate['additional_status'] = null;

            //Verify transaction code start
            if ($request->paytabs_status == "S") {
                $verify = $this->verifyPaytabsTransaction($request->paytabs_transaction_id, $request->paytabs_order_id);
                if ($verify['response_code'] == 100) {
                    $arrayUpdate['status'] = "success";
                    $arrayUpdate['response'] = $verify['result'];
                    $success_flag = "S";
                } else {
                    $arrayUpdate['status'] = "failed";
                    $arrayUpdate['response'] = $verify['result'];
                    $success_flag = "F";
                }
            } else if ($request->paytabs_status == "C") {
                $arrayUpdate['status'] = "cancelled";
                $arrayUpdate['response'] = "Your transaction has been cancelled.";
                $success_flag = "C";
            } else {
                $arrayUpdate['status'] = "failed";
                $arrayUpdate['response'] = !empty($request->paytabs_message) ? $request->paytabs_message : null;
                $success_flag = "F";
            }
            //Verify transaction code end

            $order = Order::where("invoice_number", $request->paytabs_order_id)->where('user_id', $user->id)
                ->first(['id', 'user_id', 'product_type', 'product_id','status', 'currency', 'invoice_total']);
            if ($order) {
                if ($order->status == 'initiated') {
                    //Update Status
                    $order->fill($arrayUpdate)->save();
                    if ($success_flag == "S") {
                        // Update purchase status
                        if ($order->product_type == 1) { // Show
                            ShowPurchase::create(["show_id" => $order->product_id, "user_id" => $order->user_id, "purchased_date" => now()]);
                        } else if ($order->product_type == 2 || $order->product_type == 3) { // Episode or Standalone episode
                            EpisodePurchase::create(["episode_id" => $order->product_id, "user_id" => $order->user_id, "purchased_date" => now()]);
                        }

                        // Send invoice Email to User
                        dispatch(new InvoiceEmail($order->id, "self"))->onQueue('email');

                        //Update on Mixpanel code start
                        if ($order->product_type == 2 || $order->product_type == 3) {
                            //$this->trackMixpanelEvent($order);
                        }
                        //Update on Mixpanel code end

                        return response()->api([
                            'status' => true,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_success_lbl"),
                            'data'    => []
                        ]);
                    } else if ($success_flag == "C") {
                        return response()->api([
                            'status' => false,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_cancelled_lbl"),
                            'data'    => []
                        ]);
                    } else {
                        return response()->api([
                            'status' => false,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_failed_lbl"),
                            'data'    => []
                        ]);
                    }
                } else if ($order->status == 'success') {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_already_done_lbl"),
                        'data'    => []
                    ]);
                } else {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Common.error"),
                        'data'    => []
                    ]);
                }
            } else {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl"),
                    'data'    => []
                ]);
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error"),
                'data'    => []
            ]);
        }
    }

    /**
     *This function is use for create aproval url for gift
     */
    public function create_order_for_gift(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        //Check if user is guest user or not
        $isUserVerified = $this->isUserVerified($user);
        if (!$isUserVerified['status']) {
            $response = [
                'status' => false,
                'message' => $isUserVerified['message']
            ];
            return response()->json($response, 200);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'product_type' => 'required',
            'giftBy' => 'required',
            'platform' => 'required',
            'payment_mode' => 'required',
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl"),
            'product_type.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_type_lbl"),
            'giftBy.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_recipient_id_lbl"),
            'platform.required' => config('language.' . $this->getLocale() . ".Marketplace.platform_missing"),
            'payment_mode.required' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_missing"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Ip2Location code start
            $ip_address = $request->ip_address;
            $responseData = new IP2Location('api', null, $ip_address);
            $location = $responseData->get_location();

            if (empty($location['data']['country_code']) || $location['data']['country_code'] == "-") {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
                    'data'    => [
                        'order_id' => "",
                        'currency' => "",
                        'amount' => 0
                    ]
                ]);
            }

            //Check if marketplace available in user country
            $marketplace_country = MarketplaceCountry::where("status", "Published")
                ->where("admin_status", "Approved")
                ->where('country_alpha2_code', $location['data']['country_code'])
                ->first(["id", "title", "country_alpha2_code", "currency_id", "tax_name", "tax_price"]);

            // $marketplace_country = MarketplaceCountry::where("admin_status", "Approved")
            //     ->where('country_alpha2_code', $location['data']['country_code'])
            //     ->first(["id", "title", "country_alpha2_code", "currency_id", "tax_name", "tax_price"]);

            if (empty($marketplace_country)) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
                    'data'    => [
                        'order_id' => "",
                        'currency' => "",
                        'amount' => 0
                    ]
                ]);
            }

            $flag = false;
            if ($request->product_type == 'Series') {
                $content = Show::where("id", $request->content_id)->where('content_type', '!=', 0)->whereNotNull('user_id')
                    ->where('status', 'Published')->first(['id', 'user_id']);
                if ($content) {
                    //First we check if this show already purchased by that user
                    $check = $this->check_if_item_purchase_by_user_for_gift($content->user_id, $request->giftBy, $content->id, 'series');
                    if ($check == 'yes') {
                        return response()->api([
                            'status' => true,
                            'purchase' => 'yes',
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_already_purchased_lbl"),
                            'data'    => [
                                'order_id' => "",
                                'currency' => "",
                                'amount' => 0
                            ]
                        ]);
                    }
                    //Now we get price for current country and currency id
                    $product_ids = $content->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                    $product_type = 1;

                    $flag = true;
                } else {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found"),
                        'data'    => [
                            'order_id' => "",
                            'currency' => "",
                            'amount' => 0
                        ]
                    ]);
                }
            } elseif ($request->product_type == 'Episode') {
                $content = EpisodeDetail::where("episode_id", $request->content_id)->where('status', 'Published')->first(['id', 'episode_id', 'user_id', 'content_type']);
                if ($content) {
                    //First we check if this show already purchased by that user
                    $check = $this->check_if_item_purchase_by_user_for_gift($content->user_id, $request->giftBy, $content->episode_id, 'episode', $content->episode->show->id);
                    //dd($check, $content->user_id, $request->giftBy, $content->episode_id, $content->episode->show->id);
                    if ($check == 'yes') {
                        return response()->api([
                            'status' => true,
                            'purchase' => 'yes',
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_already_purchased_lbl"),
                            'data'    => [
                                'order_id' => "",
                                'currency' => "",
                                'amount' => 0
                            ]
                        ]);
                    }

                    //Now we get price for current country and currency id
                    $product_ids = $content->episode->premium_pricing()->where('country_id', $marketplace_country->id)
                        ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                    $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";

                    $product_type = ($content->content_type + 1);

                    $flag = true;
                } else {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found"),
                        'data'    => [
                            'order_id' => "",
                            'currency' => "",
                            'amount' => 0
                        ]
                    ]);
                }
            }

            if ($flag == true) {
                //Create Invoice Id
                $maxId = \DB::table('orders')->max('id');
                $maxPurchaseId = 0;
                if (!is_null($maxId)) {
                    $maxPurchaseId = $maxId;
                }
                $maxPurchaseId = $maxPurchaseId + 1;
                $date_time = date("ymd");
                $invoice_no = "STR" . $date_time . $maxPurchaseId;

                //$cust_transaction_id = $this->getRandomNumber(10) . $date_time;

                //Final Price Calculation
                $net_amount = $selling_price;
                $tax_amount = 0;

                //$tax_percent = $marketplace_country->tax_price;
                //$tax_amount = round($selling_price - ($selling_price * (100 / (100 + $tax_percent))));
                //$net_amount = round($selling_price - $tax_amount);

                $invoice_total = ($net_amount + $tax_amount);                

                //Insert data into our tables tbl_orders
                $insertArray = [
                    "invoice_number" => $invoice_no,
                    "invoice_date" => Carbon::now(),
                    "user_id" => $request->giftBy,
                    "giftBy" => $user->id,
                    "purchase_type" => 2,
                    "product_type" => $product_type,
                    "product_id" => $request->content_id,
                    "invoice_type" => 1,
                    "country_id" => $marketplace_country->id,
                    "billing_city" => $location['data']['city_name'],
                    "billing_state" => $location['data']['region_name'],
                    "billing_country" => $location['data']['country_name'],
                    "ip_address" => $request->ip(),
                    "gateway" => "PayTabs",
                    "platform" => $request->platform,
                    "payment_mode" => $request->payment_mode,
                    "billing_type" => "one_time",
                    "currency" => $marketplace_country->currency->title,
                    "net_amount" => $net_amount,
                    "tax_name" => $marketplace_country->tax_name,
                    "tax_percent" => $marketplace_country->tax_price,
                    "tax_amount" => $tax_amount,
                    "invoice_total" => $invoice_total,
                    "status" => "initiated",
                    "gift_message" => $request->gift_message,
                ];

                $orderData = Order::create($insertArray);

                if (!$orderData) {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Common.error"),
                        'data'    => [
                            'order_id' => "",
                            'currency' => "",
                            'amount' => 0
                        ]
                    ]);
                }

                return response()->api([
                    "status" => true,
                    'purchase' => 'no',
                    "message" => "",
                    "data" => [
                        'order_id' => $invoice_no,
                        'currency' => $marketplace_country->currency->title,
                        'amount' => $invoice_total,
                        //"billing_city" => $location['data']['city_name'],
                        //"billing_state" => $location['data']['region_name'],
                        //"billing_country" => $location['data']['country_name'],
                    ]
                ]);
            } else {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.error"),
                    'data'    => [
                        'order_id' => "",
                        'currency' => "",
                        'amount' => 0
                    ]
                ]);
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error"),
                'data'    => [
                    'order_id' => "",
                    'currency' => "",
                    'amount' => 0
                ]
            ]);
        }
    }

    /**
     *This function is use for create aproval url
     */
    public function verify_order_for_gift(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        //Check if user is guest user or not
        $isUserVerified = $this->isUserVerified($user);
        if (!$isUserVerified['status']) {
            $response = [
                'status' => false,
                'message' => $isUserVerified['message']
            ];
            return response()->json($response, 200);
        }

        if ($request->paytabs_status == "S") {
            $validator = Validator::make($request->all(), [
                'paytabs_transaction_id' => 'required',
                'paytabs_order_id' => 'required',
                'paytabs_status' => 'required'
            ], [
                'paytabs_transaction_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_payment_id_lbl"),
                'paytabs_order_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_order_id_lbl"),
                'paytabs_status.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_status_lbl")
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'paytabs_order_id' => 'required',
                'paytabs_status' => 'required'
            ], [
                'paytabs_order_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_order_id_lbl"),
                'paytabs_status.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_paytabs_status_lbl")
            ]);
        }
        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first(),
                'data'    => []
            ]);
        }

        try {
            $arrayUpdate['transaction_id'] = !empty($request->paytabs_transaction_id) ? $request->paytabs_transaction_id : "";
            $arrayUpdate['invoice_date'] = Carbon::now();
            $arrayUpdate['additional_status'] = null;
            
            //Verify transaction code start
            if ($request->paytabs_status == "S") {
                $verify = $this->verifyPaytabsTransaction($request->paytabs_transaction_id, $request->paytabs_order_id);
                if ($verify['response_code'] == 100) {
                    $arrayUpdate['status'] = "success";
                    $arrayUpdate['response'] = $verify['result'];
                    $success_flag = "S";
                } else {
                    $arrayUpdate['status'] = "failed";
                    $arrayUpdate['response'] = $verify['result'];
                    $success_flag = "F";
                }
            } else if ($request->paytabs_status == "C") {
                $arrayUpdate['status'] = "cancelled";
                $arrayUpdate['response'] = "Your transaction has been cancelled.";
                $success_flag = "C";
            } else {
                $arrayUpdate['status'] = "failed";
                $arrayUpdate['response'] = !empty($request->paytabs_message) ? $request->paytabs_message : null;
                $success_flag = "F";
            }
            //Verify transaction code end

            //$message = config('language.' . $this->getLocale() . ".Marketplace.payment_mode_success_lbl");

            $order = Order::where("invoice_number", $request->paytabs_order_id)->where('giftBy', $user->id)
                ->first(['id', 'user_id', 'giftBy', 'product_type', 'product_id', 'status','gift_message', 'currency', 'invoice_total']);

            if ($order) {
                if (is_null($order->giftBy)) {
                    if ($order->user_id != $user->id) {
                        return response()->api([
                            'status' => false,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl")
                        ]);
                    }
                } else {
                    if ($order->giftBy != $user->id) {
                        return response()->api([
                            'status' => false,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl")
                        ]);
                    }
                }

                if ($order->status == 'initiated') {
                    //Update Status
                    $order->fill($arrayUpdate)->save();
                    if ($success_flag == "S") {
                        // Update purchase status
                        if ($order->product_type == 1) { // Show
                            ShowPurchase::create(["show_id" => $order->product_id, "user_id" => $order->user_id, "purchased_date" => now()]);
                        } else if ($order->product_type == 2 || $order->product_type == 3) { // Episode or Standalone episode
                            EpisodePurchase::create(["episode_id" => $order->product_id, "user_id" => $order->user_id, "purchased_date" => now()]);
                        }

                        // Send invoice Email to User
                        dispatch(new InvoiceEmail($order->id, "gift"))->onQueue('email');

                        // Send email to recipient
                        // Send email to recipient
                        $send_file_name = 'emails.gift_message_guest';
                        if ($order->user->user_type != 0 && $order->user->verified == "Verified" && $order->user->admin_status == "Approved") {
                            $send_file_name = 'emails.gift_message';
                        }
                        $mailData = [
                            'file_path' => $send_file_name,
                            'from_email' => config('config.invoice.sender_email'),
                            'from_name' => config('config.invoice.sender_name'),
                            'to_email' => trim($order->user->email),
                            'to_name' => $order->user->full_name,
                            'subject' => $order->sender->notification_format_name . ' has gifted you a premium audio content on Storiyoh!',
                            'filename' => null,
                            'data' => ['order' => $order]
                        ];
                        dispatch(new SendEmailUser($mailData))->onQueue('email');

                        //Only verified user get the push notification
                        if ($order->user->user_type != 0 && $order->user->verified == "Verified" && $order->user->admin_status == "Approved") {
                            // Insert into Feeds Table
                            $dataArray = [trim($order->id)];
                            (new Feed($order->user))->add($dataArray, Feed::GIFTING_CONTENT);

                            //Send push notification
                            //Yay!! [insert emoji for excitement] 
                            //You just received a gift on Storiyoh. Listen to it now! 
                            $message = "Yay!! 🤗 You just received a gift on Storiyoh. Listen to it now!";
                            //$order->sender->notification_format_name . ' gifted you';
                            $title = $message;
                            $description = '';
                            $icon = '';
                            $data = [
                                'title' => $message,
                                'username' => '',
                                'content_id' => $order->product_id,
                                'content_type' => $order->product_type == 1 ? "S" : "E",
                                'type' => 'GIFTCONTENT',
                                'image' => '',
                                'mediaUrl' => '',
                                'desc' => '',
                                'tray_icon' => ''
                            ];

                            dispatch(new SendPushNotification($order->user_id, $title, $description, $icon, $data))->onQueue('push_notification');
                        }

                        //Update on Mixpanel code start
                        if ($order->product_type == 2 || $order->product_type == 3) {
                            //$this->trackMixpanelEvent($order);
                        }
                        //Update on Mixpanel code end

                        return response()->api([
                            'status' => true,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_success_lbl")
                        ]);
                    } else if ($success_flag == "C") {
                        return response()->api([
                            'status' => false,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_cancelled_lbl")
                        ]);
                    } else {
                        return response()->api([
                            'status' => false,
                            'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_failed_lbl")
                        ]);
                    }
                } else if ($order->status == 'success') {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.payment_mode_already_done_lbl")
                    ]);
                } else {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Common.error")
                    ]);
                }
            } else {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl")
                ]);
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }
}
