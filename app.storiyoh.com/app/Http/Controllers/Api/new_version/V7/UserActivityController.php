<?php

namespace App\Http\Controllers\Api\new_version\V7;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Board;
use App\Models\Show;
use App\Models\Category;
use App\ClassesV3\Feed;
use App\Traits\FireBase;
use App\Traits\Helper;
use Carbon\Carbon;
use App\Models\Episode;
use App\Models\Feed as FeedRepo;
use App\Jobs\IndividualPushNotification;
use Illuminate\Support\Facades\Log;
use App\Jobs\NewOlderShows;
use App\Traits\ResponseFormat;
use App\ClassesV3\V3_3\Search;
/** Topic  Subscription Code Start */
use App\Jobs\FirebaseTopicSubscription;
/** Topic  Subscription Code End */

/** Send Push Notification */
use App\Jobs\SendPushNotification;

class UserActivityController extends Controller
{
    use ResponseFormat;

    /**
     * This function is use for follow board
     * @param Request $request
     * @return type
     */
    public function follow_board(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_playlist_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Check if board found
        $board = Board::find($request->board_id, ['user_id', 'title', 'private']);
        //dd($board->toArray());

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl")
            ]);
        }

        if ($board->private == 'Y') {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".UserActivity.follow_board_private_playlist_lbl")                
            ]);
        }

        //check if this board is your own board
        if ($board->user_id == $user->id) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".UserActivity.follow_board_own_playlist_lbl")                
            ]);
        }

        //Check if this board already follow
        $board_info = $user->following_boards()->find($request->board_id, ['id']);

        if ($board_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".UserActivity.follow_board_already_follow_playlist_lbl")                
            ]);
        }

        if (!$board_info) {
            //First we check board user is follow or not
            //if user is not follow than first we follow that user
            $follow_user = $user->following()->find($board->user_id);

            //$userData = User::find($board->user_id);
            // $tokens = [];
            // if ($userData) {
            //     $tokens = $userData->pushIds()->where('push_ids.status', 1)->pluck('token')->all();
            // }

            $message = $user->notification_format_name . ' started following your playlist ' . $board->title;
            if (!$follow_user) {
                //Sync data with user
                $user->following()->attach($board->user_id);

                //Insert into Feeds Table
                (new Feed($user))->add(trim($board->user_id), Feed::FOLLOW_USER);

                $message = $user->notification_format_name . ' started following you and your playlist ' . $board->title;
            }

            // add user into board
            $user->following_boards()->attach($request->board_id);

            //Update Follower Count
            try {
                Board::where('id', $request->board_id)
                    ->update(['followers_count' => \DB::raw('followers_count + 1')]);
            } catch (\Exception $ex) {
            }

            //Insert into Feed Table
            (new Feed($user))->add($request->board_id, Feed::FOLLOW_BOARD);

            //Send Push Notification to the owner of the board
            //if (count($tokens) > 0) {
                //$title = $message;
                //$description = '';
                $title = 'Follow Playlist';
                $description = $message;
                $icon = '';
                $data = [
                    'title' => 'Follow Playlist',
                    'username' => $user->username,
                    'type' => 'FBU',
                    'desc' => $message,
                    'tray_icon' => '',
                    'image' => !empty($user->image) ? $user->getImage(200) : '',
                    'mediaUrl' => !empty($user->image) ? $user->getImage(200) : '',
                ];
                dispatch(new SendPushNotification($board->user_id, $title, $description, $icon, $data))->onQueue('single_push_notification');
                //dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
                //FireBase::sendFireBaseNotification($tokens, $title, $description, $icon, $data);
            //}
        }

        return response()->api([
            'status' => true,
            //'message' => 'You have been successfully follow this board.'
            'message' => config('language.' . $this->getLocale() . ".UserActivity.follow_board_success_lbl") . $board->title 
            //'You are now following ' . $board->title
        ]);
    }

    /**
     * This function is use for un-follow board
     * @param Request $request
     * @return type
     */
    public function unfollow_board(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_playlist_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //check if this board already unfollow
        $board_info = $user->following_boards()->find($request->board_id, ['id', 'title']);

        if (!$board_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".UserActivity.follow_board_already_unfollow_playlist_lbl")
            ]);
        }

        if ($board_info) {
            // remove user from board
            $user->following_boards()->detach($request->board_id);

            //Update Follower Count
            try {
                Board::where('id', $request->board_id)
                    ->update(['followers_count' => \DB::raw('followers_count - 1')]);
            } catch (\Exception $ex) {
            }

            //Also remove from feed table
            $boardFeedData = FeedRepo::where('type', 'Follow Board')
                ->where('typeable_type', 'App\\Models\\User')
                ->where('typeable_id', $user->id)
                ->where('data', 'LIKE', '%"' . $request->board_id . '"%');
            if ($boardFeedData->count()) {
                $boardFeedData->delete();
            }
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".UserActivity.unfollow_board_success_lbl") . $board_info->title
            //'You have been successfully unfollow ' . $board_info->title
        ]);
    }

    /**
     * This function is use for subscribe shows
     * @param Request $request
     */
    public function subscribe_show(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
           'show_id' => 'required'
        ], [
           'show_id.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_show_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }        

        $subscriber_user = $user->subscribers()->find($request->show_id, ['id']);
        if ($subscriber_user) {
            //return json from webservices
            return response()->api([
               'status' => true,
               'message' => config('language.' . $this->getLocale() . ".UserActivity.subscribe_show_already_subscribed_show_lbl")
            ]);
        }

        $showData = Show::find($request->show_id);
        
        if ($showData->content_type != 0) {
            //return json from webservices
            return response()->api([
                'status' => true,
                'message' => "You can not subscribed premium show."
            ]);
        }

        //Sync show data with user
        $user->subscribers()->attach($request->show_id);

        // Update Subscriber count in show table
        $showData->timestamps = false;
        $showData->update(['no_of_subscribers' => \DB::raw('no_of_subscribers + 1')]);
        //Show::where('id', $request->show_id)->update(['no_of_subscribers' => \DB::raw('no_of_subscribers + 1')]);
        
        /** Topic  Subscription Code Start */
        //Subscribed user to that topic (Podcast)        
        try {
            dispatch(new FirebaseTopicSubscription($request->show_id, $user->id, 'subscribe'))->onQueue('subscribe-topic');                 
        } catch(\Exception $ex) {}
        /** Topic  Subscription Code End */

        //Insert into Feeds Table
        if($user->verified == 'Verified') {
            (new Feed($user))->add(trim($request->show_id), Feed::SUBSCRIBE_SHOW);
        }

        //Fetched New Episodes from show this is code by nitesh
        dispatch(new NewOlderShows($request->show_id))->onQueue('default');        

        //return json from webservices
        return response()->api([
           'status' => true,
           //'message' => 'You have been successfully subscribe to this show.'
           'message' => config('language.' . $this->getLocale() . ".UserActivity.subscribe_show_success_lbl") . $showData->title
           //'You are now following ' . $showData->title
        ]);
    }

    /**
     * This function is use for unsubscribe shows
     * @param Request $request
     */
    public function unsubscribe_show(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
           'show_id' => 'required'
        ], [
           'show_id.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_show_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }        

        $unsubscribe_show = $user->subscribers()->find($request->show_id);

        if ($unsubscribe_show) {
            //Detach show data with user
            $user->subscribers()->detach($request->show_id);

            //Also remove from feed table
            if ($user->verified == 'Verified') {
                $showFeedData = FeedRepo::where('type', 'Subscribe Show')
                    ->where('typeable_type', 'App\\Models\\User')
                    ->where('typeable_id', $user->id)
                    ->where('data', 'LIKE', '%"' . $request->show_id . '"%');
                    if ($showFeedData->count()) {
                        $showFeedData->delete();
                    }
            }

            // Update Subscriber count in show table
            $unsubscribe_show->timestamps = false;
            $unsubscribe_show->update(['no_of_subscribers' => \DB::raw('no_of_subscribers - 1')]);
            //Show::where('id', $request->show_id)->update(['no_of_subscribers' => \DB::raw('no_of_subscribers - 1')]);

            /** Topic  Subscription Code Start */
            //Unsubscribe all token of that topic (Podcast)
            try {
                dispatch(new FirebaseTopicSubscription($request->show_id, $user->id, 'unsubscribe'))->onQueue('unsubscribe-topic');                 
            } catch(\Exception $ex) {}
            /** Topic  Subscription Code End */

            //return json from webservices
            return response()->api([
               'status' => true,
               'message' => config('language.' . $this->getLocale() . ".UserActivity.unsubscribe_show_success_lbl") . $unsubscribe_show->title
               //'You have successfully unfollowed ' . $unsubscribe_show->title
            ]);
        } else {
            return response()->api([
               'status' => false,
               'message' => config('language.' . $this->getLocale() . ".UserActivity.unsubscribe_show_already_unfollow_lbl")
               //'You have already unfollowed this show.'
            ]);
        }
    }

    /**
     * This function is use for follow user
     * @param Request $request
    */
    public function follow_user(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
           'username' => 'required'
        ], [
           'username.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        $following_user = User::where('username', $request->username)->first(['id']);

        if ($following_user->id == $user->id) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".UserActivity.follow_user_not_follow_lbl")
             ]);
        }

        $follow_user = $user->following()->find($following_user->id, ['id']);

        if ($follow_user) {
            //return json from webservices
            return response()->api([
               'status' => true,
               'message' => config('language.' . $this->getLocale() . ".UserActivity.follow_user_already_follow_lbl")
            ]);
        }

        //Sync data with user
        $user->following()->attach($following_user->id);

        //Insert into Feeds Table
        (new Feed($user))->add(trim($following_user->id), Feed::FOLLOW_USER);

        //dd($user->id, $following_user->id, Feed::FOLLOW_USER);

        //Send Push Notification to the User
        $userData = User::find($following_user->id);

        //$tokens = [];
        if ($userData) {
            try {
                //$tokens = $userData->pushIds()->where('push_ids.status', 1)->pluck('token')->all();
                //if (count($tokens) > 0) {
                $title = $user->notification_format_name . ' started following you';
                $description = '';
                $icon = '';
                $data = [
                    'title' => 'New Connection',
                    'username' => $user->username,
                    'type' => 'FU',
                    'desc' => $user->notification_format_name . ' started following you',
                    'tray_icon' => '',
                    'image' => !empty($user->image) ? $user->getImage(200) : '',
                    'mediaUrl' => !empty($user->image) ? $user->getImage(200) : '',
                ];
                
                dispatch(new SendPushNotification($following_user->id, $title, $description, $icon, $data))->onQueue('single_push_notification');
                //dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
                //FireBase::sendFireBaseNotification($tokens, $title, $description, $icon, $data);
                //}
            } catch (\Exception $ex) {
            }
        }

        //return json from webservices
        return response()->api([
           'status' => true,
           'message' => config('language.' . $this->getLocale() . ".UserActivity.follow_user_success_lbl") . $userData->first_name
           //'You are now following ' . $request->username
           //'message' => 'You have been successfully followed to this user.'
        ]);
    }

    /**
     * This function is use for unfollow user
     * @param Request $request
    */
    public function unfollow_user(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
           'username' => 'required'
        ], [
           'username.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        $following_user = User::where('username', $request->username)->first(['id']);

        $unfollow_user = $user->following()->find($following_user->id, ['id']);

        if ($unfollow_user) {
            //Detach data with user
            $user->following()->detach($following_user->id);

            /* Unfollow boards */
            if ($unfollow_user->boards) {
                $boards = $unfollow_user->boards()->pluck('id')->all();
                if (count($boards) > 0) {
                    $user->following_boards()->detach($boards);
                }
            }

            /* Unfollow smartplaylist */
            if ($unfollow_user->smart_playlists) {
                $smartListIds = $unfollow_user->smart_playlists()->pluck('id')->all();
                if (count($smartListIds) > 0) {
                    $user->following_smart_playlist()->detach($smartListIds);
                }
            }

            //Also remove from feed table
            $userFeedData = FeedRepo::where('type', 'Follow User')
                ->where('typeable_id', $user->id)
                ->where('typeable_type', 'App\\Models\\User')
                ->where('data', 'LIKE', '%"' . $following_user->id . '"%');
            if ($userFeedData->count()) {
                $feedData = $userFeedData->get();
                foreach ($feedData as $item) {
                    $dataArray = json_decode($item->data);
                    $arr = array_diff($dataArray, [$following_user->id]);
                    //dump($following_user->id, $item->toArray(), $arr);
                    if (count($arr) == 0) {
                        $item->delete();
                    } else {
                        //dump(json_encode(array_flatten($arr)));
                        $item->timestamps = false;
                        $item->fill(['data' => json_encode(array_flatten($arr))])->save();
                        //also remove from notification read table
                        $readData = $user->user_notification_status()->find($item->id);
                        if ($readData) {
                            $user->user_notification_status()->detach($item->id);
                        }
                    }
                }
            }

            if ($unfollow_user->boards) {
                $boards = $unfollow_user->boards()->pluck('id')->all();
                foreach ($boards as $boardData) {
                    $followBoardFeedData = FeedRepo::where('type', 'Follow Board')
                        ->where('typeable_type', 'App\\Models\\User')
                        ->where('typeable_id', $user->id)
                        ->where('data', 'LIKE', '%"' . $boardData . '"%');
                    if ($followBoardFeedData->count()) {
                        //dump($followBoardFeedData->get()->toArray(), $boardData, $user->id);
                        $followBoardFeedData->delete();
                    }
                }
            }

            //return json from webservices
            return response()->api([
               'status' => true,
               'message' => config('language.' . $this->getLocale() . ".UserActivity.unfollow_user_success_lbl")
               //'You have successfully unfollowed ' . $request->username
            ]);
        } else {
            return response()->api([
               'status' => false,
               'message' => config('language.' . $this->getLocale() . ".UserActivity.unfollow_user_already_unfolow_lbl")
               //'You have already unfollowed this user.'
            ]);
        }
    }

    /**
     * This function is use for user listing
     * @param Request $request
     * @return type
     */
    public function users(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $user = new Search('api', $user);
        
        return $user->user_search();
        /*
        //Fetch all Users
        $query = User::approved()->verified()
            ->whereNotNull('username')
            ->where('username', '!=', '')
            ->where('id', '!=', $user->id)
            ->withCount(['follower', 'follower as follower_status_count' => function ($qry) use ($user) {
                $qry->where('id', $user->id);
            }])
            ->with(['categories' => function ($q) use ($user) {
                $q->whereNotIn('id', $user->categories()->pluck('id')->all());
            }]);

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('first_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $searchText . '%');
                $query->orWhere('username', 'LIKE', '%' . $searchText . '%');
            });
        }

        //Sort By
        if ($request->sort_by == 'last_updated') {
            $query->orderBy('updated_at', 'Desc');
        } else {
            $query->orderBy('first_name', 'ASC');
        }

        // No of Records
        $noofrecords = config('config.pagination.user');
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }

        if ($request->no_paginate == 'Yes') {
            $users = $query->get(['id', 'first_name', 'last_name', 'username', 'image']);
        } else {
            $users = $query->paginate($noofrecords, ['id', 'first_name', 'last_name', 'username', 'image']);
        }

        $data = [];
        foreach ($users as $item) {
            $data[] = [
               //"id" => $item->id,
               'name' => $item->notification_format_name,
               'username' => $item->username,
               'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
               'also_interested_category' => $item->categories()->pluck('title', 'id')->all(),
               'follower_count' => $item->follower_count,
               'follow_status' => $item->follower_status_count
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No User Found.';
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'data' => [
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => $users->total(),
                    'per_page' => $users->perPage(),
                    'pages' => ceil($users->total() / $users->perPage()),
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        }*/

    }

    /**
     * User Show
     * @param Request $request
     * @return type
     */
    public function show(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {            
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //User Info
        $user_info = User::withCount([
            'follower as follower_status_count' => function ($qry) use ($user) {
                $qry->where('id', $user->id);
            },
            'following as following_status_count' => function ($qry) use ($user) {
                $qry->where('id', $user->id);
            }])
        ->where('username', $request->username)
        ->whereNotNull('username')
        ->where('username', '!=', '')->first(['username', 'image', 'verified']);

        if (!$user_info) {
            return response()->api([
                'status' => false,
                'message' => 'No User Found.'
            ]);
        }

        if ($user_info->verified == 'Disabled') {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_user_found_lbl")
            ]);
        }

        //dd($user_info);
        $user_playlist = $user_info->boards()->where('private', 'N')->has('episodes')->count();
        $user_smart_playlist = $user_info->smart_playlists()->where('private', 'N')->has('shows')->count();

        $data = [
            'name' => $user_info->notification_format_name,
            'user_name' => $user_info->username,
            'location' => @$user_info->country_name->title,
            'bio' => $user_info->bio ? $user_info->bio : '',
            'user_photo' => !empty($user_info->image) ? $user_info->getImage(100) : asset('uploads/default/user.png'),
            //'no_of_following' => $user_info->following_count,
            //'no_of_follower' => $user_info->follower_count,
            'no_of_following' => $user_info->following()->verified()->count(),
            'no_of_follower' => $user_info->follower()->verified()->count(),
            'no_of_boards' => $user_info->boards()->where('private', 'N')->has('episodes')->count(),
            'no_of_playlist_and_smart_playlists' => $user_playlist + $user_smart_playlist,
            'follow_status' => $user_info->follower_status_count,
            'following_status' => $user_info->following_status_count,
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * User Show
     * @param Request $request
     * @return type
     */
    public function user_boards(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //User Info
        $user_info = User::approved()->verified()
            ->where('username', $request->username)
            ->first(['id', 'username', 'image']);

        if (!$user_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_user_found_lbl")
            ]);
        }

        // $query = $user_info->boards()->withCount(['episodes', 'followers', 'followers as subscribe' => function($qry) use ($user) {
        //     $qry->where('id', $user->id);
        // }])->has('episodes');

        $query = $user_info->boards()->where('private', 'N')->has('episodes');

        if ($request->sort_by == 'last_updated') {
            $query->orderBy('updated_at', 'Desc');
        } else {
            $query->orderBy('id', 'DESC');
        }

        if ($request->no_paginate == 'Yes') {
            $all_boards = $query->get(['id', 'user_id', 'title', 'image', 'private', 'updated_at']);
        } else {
            $all_boards = $query->paginate(config('config.pagination.board'), ['id', 'user_id', 'title', 'image', 'private', 'updated_at']);
        }

        $user_boards = $user->following_boards()->pluck('id')->all();

        $data = [];
        foreach ($all_boards as $item) {
            $follow_status = 0;
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 1;
            }
            $data[] = [
               'id' => $item->id,
               'board_name' => $item->title,
               'private' => $item->private,
               'board_owner' => $user->id == $item->user_id ? 1 : 0,
               'no_of_episodes' => $item->episodes()->count(),
               'no_of_follower' => Helper::shorten_count($item->followers()->count()),
               'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
               'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
               'name' => $item->user->notification_format_name,
               'username' => $item->user->username,
               'follow_status' => $follow_status
           ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'data' => [
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => $all_boards->total(),
                    'per_page' => $all_boards->perPage(),
                    'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        }
    }

    /**
     * User Show
     * @param Request $request
     * @return type
     */
    public function user_connection(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Fetch all Users
        $user_info = User::approved()->verified()
            ->where('username', $request->username)
            ->first(['id', 'username', 'image']);

        if (!$user_info) {
            return response()->api(['data' => [
                'status' => false,
                'message' => 'No User Found.'
            ]]);
        }

        $following_user_id = $user_info->following()->with(['categories' => function ($q) use ($user) {
            $q->whereNotIn('id', $user->categories()->pluck('id')->all());
        }])->verified()->get();

        $user_followers = $user_info->follower()->with(['categories' => function ($q) use ($user) {
            $q->whereNotIn('id', $user->categories()->pluck('id')->all());
        }])->verified()->get();

        $followers_user_data = [];
        $following_user_data = [];

        $user_categories = $user->categories()->pluck('id')->all();

        $user_follow_status = $user->following()->pluck('id')->all();

        foreach ($user_followers as $item) {
            $following_status = 0;
            if (@in_array($item->id, $user_follow_status)) {
                $following_status = 1;
            }

            $followers_user_data[] = [
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follower_status' => $following_status,
                'follower_count' => Helper::shorten_count($item->follower()->count())
            ];
        }

        foreach ($following_user_id as $item) {
            $following_status = 0;
            if (@in_array($item->id, $user_follow_status)) {
                $following_status = 1;
            }
            $following_user_data[] = [
               'name' => $item->notification_format_name,
               'username' => $item->username,
               'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
               'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
               'follower_status' => $following_status,
               'follower_count' => Helper::shorten_count($item->following()->verified()->count())
            ];
        }

        $data = [
            'follower_user' => $followers_user_data,
            'following_user' => $following_user_data
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
        * Track Stream data Page
        * @param Request $request
        * @return type
    */
    public function user_stream_data(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".UserActivity.user_stream_data_missing_parameter_episode_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        if($user->verified == 'Verified') {
            $existing_episode_data = $user->user_stream_data()->where('episode_id', $request->episode_id)
                ->where('user_id', $user->id)->whereBetween('user_stream_data.created_at', [date('Y-m-d') . ' 00:00:01', date('Y-m-d') . ' 23:59:59'])
                ->orderBy('user_stream_data.created_at', 'DESC')->first();
            if (!$existing_episode_data) {
                $unique_no = $request->episode_id . str_random(60) . date('YmdHis');
                $user->user_stream_data()->attach($request->episode_id, ['unique_no' => $unique_no, 'discreet' => $user->discreet]);
            } else {
                $existing_episode_data->pivot->updated_at = Carbon::now();
                $existing_episode_data->pivot->save();
            }

            //$user->user_stream_data()->attach($request->episode_id);
            //$unique_no = $request->episode_id . str_random(60) . date('YmdHis');
            //$user->user_stream_data()->attach($request->episode_id, ['unique_no' => $unique_no, 'discreet' => $user->discreet]);

            //Updating Listen Hostory in episode tables
            $episodeData = Episode::where('id', $request->episode_id); // updating neetendra on 27/09/2019 (Premium data)
            try {
                $episodeData->update(['listen_count' => \DB::raw('listen_count + 1')]);
            } catch (\Exception $e) {
            }

            $this->user_historical_data($request->episode_id, $user);

            //Check if user privacy setting yes or no
            //If yes than we will not post this to feed table
            // if ($user->discreet == 'N') {
            //     //Insert into Feeds Table
            //     $feed = (new Feed($user))->add(trim($request->episode_id), Feed::EPISODE_HEARD);
            // }

            //Check if user privacy setting yes or no
            //If yes than we will not post this to feed table
            if ($user->discreet == 'N') {
                $checkData = $episodeData->first();
                if(is_null($checkData->episode_extra_info)) {
                    $checkHeardEpisode = FeedRepo::where('typeable_id', $user->id)
                        ->where('typeable_type', 'App\\Models\\User')
                        ->where('type', 'Episode Heard')->where('data', 'LIKE', '%"' . $request->episode_id . '"%')
                        ->whereBetween('created_at', [date('Y-m-d') . ' 00:00:01', date('Y-m-d') . ' 23:59:59'])
                        ->orderBy('created_at', 'DESC')->first();

                    if (empty($checkHeardEpisode)) {
                        //Insert into Feeds Table
                        (new Feed($user))->add(trim($request->episode_id), Feed::EPISODE_HEARD);
                    } else {
                        $checkHeardEpisode->forceFill(['updated_at' => Carbon::now()])->save(['timestamps' => false]);
                    }
                }            
            }
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".UserActivity.user_stream_data_message_lbl")
        ]);
    }

    /**
       * Track download data Page
       * @param Request $request
       * @return type
    */
    public function user_download_data(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'episode_id' => 'required'
        ], [
            'episode_id.required' => config('language.' . $this->getLocale() . ".UserActivity.user_stream_data_missing_parameter_episode_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $user->user_download_data()->attach($request->episode_id);

        //$this->user_historical_data($request);

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".UserActivity.user_stream_data_message_lbl")
        ]);
    }

    /**
       * Track User historical data Page
       * @param Request $request
       * @return type
    */
    protected function user_historical_data($episode_id, User $user)
    {
        $episode_data = Episode::find($episode_id);

        if (!$episode_data) {
            //return json from webservices
            return response()->api([
               'status' => false,
               'message' => config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl")
            ]);
        }

        $categories = json_encode($episode_data->show->categories()->pluck('id')->all());

        $show_id = $episode_data->show_id;

        if ($show_id != '') {
            $episode = $user->user_historical_data()->withPivot(['show_id', 'category'])
                ->wherePivot('user_id', $user->id)
                ->wherePivot('episode_id', $episode_id)->first();

            if ($episode) {
                $episode->pivot->user_id = $user->id;
                $episode->pivot->category = $categories;
                $episode->pivot->show_id = $show_id;
                $episode->pivot->episode_id = $episode->id;
                $episode->pivot->save();
            } else {
                $episode_data->user_historical_data()->attach($user->id, [
                  'category' => $categories,
                  'show_id' => $show_id,
                ]);
            }
        }

        return true;
    }

    /**
        * Listing user listen history
        * @param Request $request
        * @return type
    */
    public function user_listen_history(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        try {
            $all_episodes = $user->user_stream_data()->orderBy('user_stream_data.updated_at', 'DESC')->paginate(10, ['id', 'title', 'show_id', 'mp3', 'duration', 'listen_count', 'explicit', 'user_stream_data.updated_at', 'image']);

            $data = [];
            foreach ($all_episodes as $dataInfo) {
                $image_name = ! is_null($dataInfo->show->image) ? $dataInfo->show->getImage(200) : asset('uploads/default/show.png');
                $type = 0;
                if(! is_null($dataInfo->episode_extra_info)) {
                    $image_name = ! is_null($dataInfo->image) ? $dataInfo->getImage(200) : asset('uploads/default/show.png');
                    $type = 1;
                }
                $data[] = [
                    'id' => $dataInfo->id,
                    'type' => $type,
                    'unique_id' => $dataInfo->pivot->unique_no,
                    'listen_discreet_status' => $dataInfo->pivot->discreet,
                    'show_id' => $dataInfo->show_id,
                    'show_title' => trim(str_replace("\n", '', $dataInfo->show->title)),
                    'episode_title' => trim(str_replace("\n", '', $dataInfo->title)),
                    'audio_file' => $dataInfo->getAudioLink(),
                    'show_title' => trim(str_replace("\n", '', $dataInfo->show->title)),
                    'episode_image' => $image_name,
                    'duration' => $dataInfo->getDurationText(),
                    'listen_count' => Helper::shorten_count($dataInfo->listen_count),
                    'explicit' => $dataInfo->show->explicit,
                    'listen_time' => $dataInfo->updated_at->diffForHumans()
                ];
            }

            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'total' => $all_episodes->total(),
                    'per_page' => $all_episodes->perPage(),
                    'pages' => ceil($all_episodes->total() / $all_episodes->perPage()),
                    'items' => $data
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'episodes' => []
                ]
            ]);
        }
    }

    /**
     * This function is use for remove user listen history
     *
     * @param Request $request
     * @return void
     */
    public function remove_user_listen_history(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $user->user_stream_data()->detach();

        return response()->api([
            'status' => true,
            'message' => 'Removed!'
         ]);
    }

    /**
     * User Connection with Pagination
     * @param Request $request
     * @return type
     */
    public function user_connection_with_pagination(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'type' => 'required',
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_username_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //fetch user info
        $user_info = User::approved()->verified()->where('username', $request->username)
            ->first(['id', 'username', 'image']);

        if (!$user_info) {
            return response()->api(['data' => [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_user_found_lbl")
            ]]);
        }

        $user_follow_status = $user->following()->pluck('id')->all();

        if ($request->type == 'followers') {
            $user_followers = $user_info->follower()->with(['categories' => function ($q) use ($user) {
                $q->whereNotIn('id', $user->categories()->pluck('id')->all());
            }])->verified()->paginate(10);

            $followers_user_data = [];
            foreach ($user_followers as $item) {
                $following_status = 0;
                if (@in_array($item->id, $user_follow_status)) {
                    $following_status = 1;
                }
                $followers_user_data[] = [
                    'name' => $item->notification_format_name,
                    'username' => $item->username,
                    'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                    'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                    'follower_status' => $following_status,
                    'follower_count' => Helper::shorten_count($item->follower()->count())
                ];
            }

            $message = '';
            if ($user_followers->total() == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
            }

            return response()->api([
                'status' => true,
                'message' => $message,
                'total' => $user_followers->total(),
                'per_page' => $user_followers->perPage(),
                'pages' => ceil($user_followers->total() / $user_followers->perPage()),
                'items' => $followers_user_data
            ]);
        } elseif ($request->type == 'following') {
            $following_user_id = $user_info->following()->with(['categories' => function ($q) use ($user) {
                $q->whereNotIn('id', $user->categories()->pluck('id')->all());
            }])->verified()->paginate(10);

            $following_user_data = [];
            foreach ($following_user_id as $item) {
                $following_status = 0;
                if (@in_array($item->id, $user_follow_status)) {
                    $following_status = 1;
                }
                $following_user_data[] = [
                    'name' => $item->notification_format_name,
                    'username' => $item->username,
                    'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                    'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                    'follower_status' => $following_status,
                    'follower_count' => Helper::shorten_count($item->following()->verified()->count())
                ];
            }

            $message = '';
            if ($following_user_id->total() == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
            }

            return response()->api([
                'status' => true,
                'message' => $message,
                'total' => $following_user_id->total(),
                'per_page' => $following_user_id->perPage(),
                'pages' => ceil($following_user_id->total() / $following_user_id->perPage()),
                'items' => $following_user_data
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => '',
            'total' => 0,
            'per_page' => 0,
            'pages' => 0,
            'data' => []
        ]);
    }

    /**
     * This function is use for recommended user similar interests
     *
     * @param Request $request
     * @return void
     */
    public function recommended_users_similar_interests(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interest_category = $user->categories()->pluck('id')->all();

        $following_users = $user->following()->pluck('id')->all();

        $query = User::withCount('follower')->approved()->verified()->where('username', '!=', '')
            ->whereNotNull('username')->where('id', '!=', $user->id)->whereNotIn('id', $following_users);

        $query->whereHas('categories', function ($q) use ($interest_category) {
            $q->whereIn('id', $interest_category);
        });

        $users_data = $query->orderBy('follower_count', 'desc')->paginate(10, ['id', 'username', 'image']);

        $data = [];
        foreach ($users_data as $key => $user_data) {
            //$also_intrested_categories = $user_data->categories()->whereNotIn('id', $interest_category)->pluck('title')->all();
            $data[] = [
                'name' => $user_data->first_name . ' ' . $user_data->last_name,
                'image' => !empty($user_data->image) ? $user_data->getImage(100) : asset('uploads/default/user.png'),
                'username' => $user_data->username,
                'follower_count' => $user_data->follower_count,
                'follow_status' => 'false',
                'also_intrested' => ''//@implode(", ", $also_intrested_categories)
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
             'status' => true,
             'message' => $message,
             'total' => $users_data->total(),
             'per_page' => $users_data->perPage(),
             'pages' => ceil($users_data->total() / $users_data->perPage()),
             'items' => $data
         ]);
    }

    /**
     * This function is use for recommended user different interests
     *
     * @param Request $request
     * @return void
     */
    public function recommended_users_different_interests(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interest_category = $user->categories()->pluck('id')->all();

        $following_users = $user->following()->pluck('id')->all();

        $query = User::withCount('follower')->approved()->verified()->where('username', '!=', '')
            ->whereNotNull('username')->where('id', '!=', $user->id)->whereNotIn('id', $following_users);

        $query->whereDoesntHave('categories', function ($q) use ($interest_category) {
            $q->whereIn('id', $interest_category);
        });

        $users_data = $query->orderBy('follower_count', 'desc')->paginate(10, ['id', 'username', 'image']);

        $data = [];
        foreach ($users_data as $key => $user_data) {
            //$also_intrested_categories = $user_data->categories()->whereNotIn('id', $interest_category)->pluck('title')->all();
            $data[] = [
                'name' => $user_data->first_name . ' ' . $user_data->last_name,
                'image' => !empty($user_data->image) ? $user_data->getImage(100) : asset('uploads/default/user.png'),
                'username' => $user_data->username,
                'follower_count' => $user_data->follower_count,
                'follow_status' => 'false',
                'also_intrested' => ''//@implode(", ", $also_intrested_categories)
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
             'status' => true,
             'message' => $message,
             'total' => $users_data->total(),
             'per_page' => $users_data->perPage(),
             'pages' => ceil($users_data->total() / $users_data->perPage()),
             'items' => $data
         ]);
    }

    /**
     * User Smart Playlist
     * @param Request $request
     * @return type
     */
    public function user_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('config.missing_parameter1') . ' user' . config('config.missing_parameter2') . ' User Playlist.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //User Info
        $user_info = User::approved()->verified()->where('username', $request->username)
            ->first(['id', 'username', 'image']);

        if (!$user_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_user_found_lbl")
            ]);
        }

        $query = $user_info->smart_playlists()->where('private', 'N')->has('shows');

        if ($request->sort_by == 'last_updated') {
            $query->orderBy('updated_at', 'Desc');
        } else {
            $query->orderBy('id', 'DESC');
        }

        if ($request->no_paginate == 'Yes') {
            $all_smartplaylist = $query->get(['id', 'user_id', 'title', 'image', 'private', 'updated_at']);
        } else {
            $all_smartplaylist = $query->paginate(config('config.pagination.board'), ['id', 'user_id', 'title', 'image', 'private', 'updated_at']);
        }

        $user_smartplaylist = $user->following_smart_playlist()->pluck('id')->all();

        $data = [];
        foreach ($all_smartplaylist as $item) {
            $follow_status = 0;
            if (@in_array($item->id, $user_smartplaylist)) {
                $follow_status = 1;
            }
            $data[] = [
               'id' => $item->id,
               'board_name' => $item->title,
               'private' => $item->private,
               'board_owner' => $user->id == $item->user_id ? 1 : 0,
               'no_of_episodes' => $item->shows()->count(),
               'no_of_follower' => Helper::shorten_count($item->followers()->count()),
               'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
               'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
               'name' => $item->user->notification_format_name,
               'username' => $item->user->username,
               'follow_status' => $follow_status
           ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'data' => [
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => $all_smartplaylist->total(),
                    'per_page' => $all_smartplaylist->perPage(),
                    'pages' => ceil($all_smartplaylist->total() / $all_smartplaylist->perPage()),
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        }
    }

    /**
     * User Subscribe Shows
     * @param Request $request
     * @return type
     */
    public function user_subscribe_shows(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".UserActivity.missing_parameter_username_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //User Info
        $user_info = User::approved()->verified()->where('username', $request->username)
            ->first(['id', 'username', 'image']);

        if (!$user_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_user_found_lbl")
            ]);
        }

        // $query = $user_info->shows()
        //     ->withCount(['episodes' => function ($qry) {
        //         $qry->published();
        //     }])->orderBy('show_user.updated_at', 'DESC');

        $query = $user_info->shows()->orderBy('show_user.updated_at', 'DESC');

        if ($request->no_paginate == 'Yes') {
            $user_shows = $query->distinct()->get(['id', 'title', 'image', 'no_of_subscribers']);
        } else {
            $user_shows = $query->distinct()->paginate(12, ['id', 'title', 'image', 'no_of_subscribers']);
        }

        $subscribers = $user->shows()->pluck('id')->all();

        $data = [];
        foreach ($user_shows as $item) {
            $subscribe = @in_array($item->id, $subscribers) ? 'true' : 'false';
            $data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                'no_of_episode' => 0,//$item->episodes_count,
                'no_of_subscribers' => $item->no_of_subscribers > 0 ? Helper::shorten_count($item->no_of_subscribers) : 0,
                'subscribe' => $subscribe
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => $data
            ]);
        } else {
            return response()->api([
               'status' => true,
               'message' => $message,
               'total' => $user_shows->total(),
               'per_page' => $user_shows->perPage(),
               'pages' => ceil($user_shows->total() / $user_shows->perPage()),
               'data' => $data
            ]);
        }
    }
}
