<?php

namespace App\Http\Controllers\Api\new_version\V8;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiTestController extends Controller
{
    public function index(Request $request)
    {
        return response()->api(['data' => $request->all()]);
    }
}
