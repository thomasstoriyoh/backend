<?php

namespace App\Http\Controllers\Api\new_version\V8\Auth;

use App\Packages\OTP\OTP;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;

use App\Traits\ResponseFormat;

class PasswordController extends Controller
{
    use ResponseFormat;

    public function forgot(Request $request)
    {
        $validator = Validator::make($request->all(), 
        [
            'email' => 'required',
        ], [
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'navigate' => 'forgot_password',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->api([
                'status' => false,
                'navigate' => 'forgot_password',
                'message' => config('language.' . $this->getLocale() . ".Password.password_username_not_found_lbl")
            ]);
        }

        if (!$user->isApproved()) {
            return response()->api([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_approved_lbl")
            ]);
        }

        if (!$user->isVerified()) {
            return response()->api([
                'status' => false,
                'navigate' => 'verify_email',
                'message' => config('language.' . $this->getLocale() . ".Password.password_username_not_verify_lbl")
            ]);
        }

        try {
            $this->sendForgetEmailOtp($user);
        } catch (\Exception $ex) {
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Password.password_email_otp_lbl"),
            'navigate' => "forgot_password_otp",
            'data' => ['email' => $request->email]
        ]);
    }

    /**
     * This function is send otp to the given email
     * @param User $user
     */
    protected function sendForgetEmailOtp($user)
    {
        $otp = OTP::generate($user->email);

        $mailData = [
                'file_path' => 'auth.email.forgot-password',
                'from_email' => config('config.register.sender_email'),
                'from_name' => config('config.register.sender_name'),
                'to_email' => trim($user->email),
                'to_name' => $user->full_name,
                'subject' => ' Forgot Password OTP',
                'filename' => null,
                'data' => [
                    'user_data' => $user,
                    'otp' => $otp
                ]
            ];

        \App\Traits\Helper::sendAllEmail($mailData);

        return $otp;
    }

    /**
     * This function is use for verify email otp
     * @param User $user
     */
    public function verifyForgotEmailOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'otp' => 'required'
        ], [
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl"),
            'otp.required' => config('language.' . $this->getLocale() . ".Auth.register_otp_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'navigate' => 'forgot_password_otp',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (OTP::verify($request->otp, $user->email)) {
                return response()->api([
                    'status' => true,
                    'email' => $user->email,
                    'navigate' => 'forgot_password_reset',
                    'message' => ''
                ]);
            } else {
                return response()->api([
                    'status' => false,
                    'navigate' => 'forgot_password_email_otp',
                    'message' => config('language.' . $this->getLocale() . ".Password.password_resent_otp_lbl")
                ]);
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function reset_password(Request $request)
    {
        $validator = Validator::make($request->all(), 
        [
            'email' => 'required',
            'password' => 'required|confirmed'
        ], [
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl"),
            'password.required' => config('language.' . $this->getLocale() . ".Auth.register_password_lbl"),
            'password.confirmed' => config('language.' . $this->getLocale() . ".Auth.register_password_confirm_lbl"), 
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'navigate' => 'forgot_password_reset',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->api([
               'status' => false,
               'navigate' => 'forgot_password',
               'message' => config('language.' . $this->getLocale() . ".Password.password_username_not_found_lbl")               
           ]);
        }

        $user->update(['password' => Hash::make($request->password)]);

        return response()->api([
            'status' => true,
            'navigate' => 'login',
            'message' => config('language.' . $this->getLocale() . ".Password.password_success_lbl") 
        ]);
    }
}
