<?php

namespace App\Http\Controllers\Api\new_version\V8;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Tzsk\Collage\Facade\Collage;
use App\Models\Board;
use App\Models\Episode;
use App\Models\Show;
use App\Traits\Helper;
use App\ClassesV3\Feed;
use App\ClassesV3\V3_3\Playlist;
use App\ClassesV3\V3_3\Search;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\Feed as FeedRepo;
use App\Jobs\SendNewEpisodePushNotificationV2;
use function GuzzleHttp\json_encode;
use App\Models\User;
use App\Jobs\IndividualPushNotification;
use App\Traits\ResponseFormat;

class BoardController extends Controller
{
    use ResponseFormat;
    
    /**
     * Boards page view.
     *
     * @param Request $request
     * @return mixed
     */
    public function myboards(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' =>config('language.' . $this->getLocale() . ".Common.auth_error") 
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = $user->boards()->withCount('episodes', 'followers');

        if ($request->sort_by == 'last_updated') {
            $query->orderBy('updated_at', 'Desc');
        } else {
            $query->orderBy('id', 'DESC');
        }

        $all_boards = $query->get(['id', 'user_id', 'title', 'image', 'private', 'episodes_count', 'updated_at']);

        $timestamps = $request->timestamp != '' ? explode(',', $request->timestamp) : '';

        $data = [];
        if ($timestamps == '') {
            foreach ($all_boards as $item) {
                $all_board_episode = $item->episodes()->whereNotNull('mp3')->get(['id', 'show_id', 'title', 'image', 'mp3', 'show_id', 'duration', 'listen_count', 'explicit']);
                $episodeArray = [];
                foreach ($all_board_episode as $episode) {
                    $episodeArray[] = [
                        'id' => $episode->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($episode->title))),
                        'duration' => $episode->getDurationText(),
                        'audio_file' => $episode->getAudioLink(),
                        'show_id' => $episode->show_id,
                        'show_title' => !empty($episode->show) ? trim(str_replace("\n", '', html_entity_decode($episode->show->title))) : '-',
                        'image' => !empty($episode->show) ? $episode->show->getWSImage(200) : asset('uploads/default/board.png'),
                        'listen_count' => Helper::shorten_count($episode->listen_count),
                        'explicit' => $episode->show->explicit
                    ];
                }

                $data[] = [
                   'id' => $item->id,
                   'name' => @$item->user->notification_format_name,
                   'username' => @$item->user->username,
                   'board_name' => $item->title,
                   'private' => $item->private,
                   'board_owner' => 1,
                   'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                   'user_image' => @$item->user->image ? @$item->user->getImage(100) : '',
                   'followers_count' => Helper::shorten_count($item->followers_count),
                   'no_of_episodes' => $item->episodes_count,
                   'timestamp' => strtotime($item->updated_at),
                   //"last_updated" => $item->updated_at->diffForHumans()
                   'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
                   'episodes' => $episodeArray,
               ];
            }
        } else {
            $user_boards = $user->boards->pluck('id')->all();

            foreach ($all_boards as $item) {
                //$episodes = $item->episodes()->get(['id', 'title', 'image', 'mp3']);

                $all_board_episode = $item->episodes()->whereNotNull('mp3')->get(['id', 'show_id', 'title', 'image', 'mp3', 'show_id', 'duration', 'listen_count', 'explicit']);

                $episodeArray = [];
                foreach ($all_board_episode as $episode) {
                    $episodeArray[] = [
                        'id' => $episode->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($episode->title))),
                        'duration' => $episode->getDurationText(),
                        'audio_file' => $episode->getAudioLink(),
                        'show_id' => $episode->show_id,
                        'show_title' => !empty($episode->show) ? trim(str_replace("\n", '', html_entity_decode($episode->show->title))) : '-',
                        'image' => !empty($episode->show) ? $episode->show->getWSImage(200) : asset('uploads/default/board.png'),
                        'listen_count' => Helper::shorten_count($episode->listen_count),
                        'explicit' => $episode->show->explicit
                    ];
                }

                foreach ($timestamps as $timestamp) {
                    $board_timestamp = explode('-', $timestamp);

                    $timestamp_value = \Carbon\Carbon::createFromTimestamp($board_timestamp[1])->toDateTimeString();

                    if ($board_timestamp[0] == $item->id && $timestamp_value != $item->updated_at) {
                        $data[] = [
                           'id' => $item->id,
                           'name' => @$item->user->notification_format_name,
                           'username' => @$item->user->username,
                           'board_name' => $item->title,
                           'private' => $item->private,
                           'board_owner' => 1,
                           'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                           'user_image' => @$item->user->image ? @$item->user->getImage(100) : '',
                           'no_of_episodes' => $item->episodes_count,
                           'followers_count' => Helper::shorten_count($item->followers_count),
                           'timestamp' => strtotime($item->updated_at),
                           //"last_updated" => $item->updated_at->diffForHumans()
                           'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
                           'episodes' => $episodeArray,
                       ];
                    }
                    if (!in_array($item->id, $user_boards)) {
                        $data[] = [
                           'id' => $item->id,
                           'name' => @$item->user->notification_format_name,
                           'username' => @$item->user->username,
                           'board_name' => $item->title,
                           'private' => $item->private,
                           'board_owner' => 1,
                           'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                           'user_image' => @$item->user->image ? @$item->user->getImage(100) : '',
                           'no_of_episodes' => $item->episodes_count,
                           'followers_count' => Helper::shorten_count($item->followers_count),
                           'timestamp' => strtotime($item->updated_at),
                           //"last_updated" => $item->updated_at->diffForHumans()
                           'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
                           'episodes' => $episodeArray,
                       ];
                    }
                }
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Board.myboards_message_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_boards->count(),
                    'per_page' => 50,
                    'pages' => ceil($all_boards->count() / 50),
                    'items' => $data
                ]
            ]);
        }
    }

    /**
     * Boards page view.
     *
     * @param Request $request
     * @return mixed
     */
    public function boards_with_paginate(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //$query = $user->boards()->withCount('episodes', 'followers');

        $query = Board::where('user_id', $user->id)
            ->orWhere('contributors', 'LIKE', '%"' . $user->id . '"%')->withCount('episodes');

        $query->orderBy('updated_at', 'Desc');

        $all_boards = $query->paginate(config('config.pagination.board'), ['id', 'user_id', 'title', 'description', 'image', 'private', 'updated_at', 'shared', 'followers_count', 'contributors']);

        $data = [];
        foreach ($all_boards as $item) {
            $contributor_array = [];
            if (!is_null($item->contributors)) {
                $con_array = json_decode($item->contributors, 1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();
                foreach ($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }
            }
            $data[] = [
                'id' => $item->id,
                'name' => @$item->user->notification_format_name,
                'username' => @$item->user->username,
                'board_name' => $item->title,
                'description' => $item->description,
                'private' => $item->private,
                'shared' => $item->shared,
                'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors, 1)),
                'contributors' => $contributor_array,
                'board_owner' => ($item->user_id == $user->id) ? 1 : 0,
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'user_image' => @$item->user->image ? @$item->user->getImage(100) : '',
                'followers_count' => Helper::shorten_count($item->followers_count),
                'no_of_episodes' => $item->episodes_count,
                'timestamp' => strtotime($item->updated_at),
                'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y')
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Board.boards_with_paginate_message_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_boards->total(),
                'per_page' => $all_boards->perPage(),
                'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * Boards Listing view.
     *
     * @param Request $request
     * @return mixed
     */
    public function board_listing(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $all_boards = Board::where('user_id', $user->id)->orWhere('contributors', 'LIKE', '%"' . $user->id . '"%')
            ->orderBy('updated_at', 'DESC')->get(['id', 'title', 'private', 'shared']);

        $data = [];
        foreach ($all_boards as $key => $item) {
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'shared' => $item->shared
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Board.playlist_not_found") ;
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'items' => $data
            ]
        ]);
    }

    /**
     * Create Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function add_board(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_name' => 'required',
            'private' => 'required'
        ], [
            'board_name.required' => config('language.' . $this->getLocale() . ".Board.add_board_name_lbl"),
            'private.required' => config('language.' . $this->getLocale() . ".Board.add_board_private_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        if ($request->private == 'Y' && $request->shared == 'Y') {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.add_board_message_lbl")
            ]);
        }

        $attributes = [
            'user_id' => $user->id,
            'title' => $request->board_name,
            'description' => !empty($request->description) ? $request->description : '',
            'private' => $request->private ? $request->private : 'N',
            'shared' => !empty($request->shared) ? $request->shared : 'N'
        ];

        //Create Board
        $board = Board::create($attributes);

        if (!empty($request->episode_id)) {
            $request->episode_id = is_array($request->episode_id) ? $request->episode_id : [$request->episode_id];
        }

        if (!empty($request->episode_id)) {
            //Sync with episodes
            $board->episodes()->sync($request->episode_id);

            //Sync board with category
            $category = [];
            $episodes = Episode::whereIn('id', $request->episode_id)->get(['id', 'show_id']);
            foreach ($episodes as $item) {
                foreach ($item->show->categories()->get(['id']) as $category_item) {
                    $category[] = $category_item['id'];
                }

                //Update Episode playlist_count
                try {
                    Episode::where('id', $item->id)->update(['playlist_count' => \DB::raw('playlist_count + 1')]);
                } catch (\Exception $ex) {
                }
            }
            $category = array_unique($category);
            $board->categories()->sync($category);

            //Generate Collage Image
            $episode_id_desc = array_reverse($request->episode_id);
            $filename = $this->generateCollageImage($episode_id_desc);
            $board->update(['image' => $filename]);
        }

        //Insert into Feeds Table
        //(new Feed($user))->add(trim($board->id), Feed::NEW_BOARD);

        $image = asset('uploads/default/board.png');
        if (!empty($request->episode_id)) {
            $image = !empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png');
        }

        $data = [
            'id' => $board->id,
            'board_name' => $board->title,
            'image' => $image,
            'private' => $board->private,
            'shared' => $board->shared,
            'no_of_episodes' => !empty($request->episode_id) ? count($request->episode_id) : 0,
            'timestamp' => strtotime($board->updated_at),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $board->updated_at)->format('jS M Y')
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Edit Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function edit_board(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'board_name' => 'required',
            'private' => 'required',
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
            'board_name.required' => config('language.' . $this->getLocale() . ".Board.add_board_name_lbl"),
            'private.required' => config('language.' . $this->getLocale() . ".Board.add_board_private_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)
            ->where('user_id', $user->id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        if ($request->private == 'Y' && $request->shared == 'Y') {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.add_board_message_lbl")
            ]);
        }

        $contributors = $board->contributors;
        if (is_null($request->shared) || $request->shared == 'N') {
            $contributors = null;
        }

        $attributes = [
            'title' => $request->board_name,
            'description' => !empty($request->description) ? $request->description : '',
            'private' => $request->private ? $request->private : $board->private,
            'shared' => !empty($request->shared) ? $request->shared : $board->shared,
            'contributors' => $contributors
        ];

        //dd($attributes);

        $board->fill($attributes)->save();

        $data = [
            'id' => $board->id,
            'board_name' => $board->title,
            'image' => !empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
            'private' => $board->private,
            'shared' => $board->shared,
            'no_of_episodes' => $board->episodes()->count(),
            'timestamp' => strtotime($board->updated_at),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $board->updated_at)->format('jS M Y'),
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function remove_board(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)
            ->where('user_id', $user->id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        //Update Episode playlist_count
        try {
            $episodeIds = $board->episodes->pluck('id')->all();
            foreach ($episodeIds as $item) {
                Episode::where('id', $item)->update(['playlist_count' => \DB::raw('playlist_count - 1')]);
            }
        } catch (\Exception $ex) {
        }

        if (!empty($board->image)) {
            if (Storage::disk('s3')->exists('boards/' . $board->image)) {
                Storage::disk('s3')->delete('boards/' . $board->image);
            }
            //@unlink(public_path('/uploads/boards/' . $board->image));
        }

        //Also remove from feed table
        $boardFeedData = FeedRepo::where('type', 'Board Update')
            ->where('typeable_type', 'App\\Models\\Board')
            ->where('typeable_id', $board->id);
        if ($boardFeedData->count()) {
            $boardFeedData->delete();
        }

        $followBoardFeedData = FeedRepo::where('type', 'Follow Board')
            ->where('typeable_type', 'App\\Models\\User')
            ->where('data', 'LIKE', '%"' . $board->id . '"%');
        if ($followBoardFeedData->count()) {
            $followBoardFeedData->delete();
        }

        //Remove Playlist Contributtor
        try {
            $boardContributorData = FeedRepo::where('type', 'Playlist Contributor')
            ->where('typeable_type', 'App\\Models\\Board')
            ->where('typeable_id', $board->id);
            if ($boardContributorData->count()) {
                $boardContributorData->delete();
            }
        } catch(\Exception $ex) {}

        //Remove Playlist Episode
        try {
            $boardPlaylistEpisodeData = FeedRepo::where('type', 'Playlist Contributor Episode')
            ->where('typeable_type', 'App\\Models\\Board')
            ->where('typeable_id', $board->id);
            if ($boardPlaylistEpisodeData->count()) {
                $boardPlaylistEpisodeData->delete();
            }
        }catch(\Exception $ex){}

        //dd($boardFeedData->get()->toArray(), $followBoardFeedData->get()->toArray());

        $board->delete();

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Board.remove_board_message_lbl"),
        ]);
    }

    /**
     * Added Episode into Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function add_episode(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                    'status' => false,
                    'code' => 401,
                    'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                ]
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'episode_id' => 'required'
        ], [
            'board_id.required' =>config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
            'episode_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_episode_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //First we check if contributor = Y than we check if this user is contributor or not
        if ($request->contributor == 'Y') {
            $board = Board::where('id', $request->board_id)->first();

            //Check if this playlist is private or not
            if ($board->private == 'Y') {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Board.add_episode_message_lbl")
                ]);
            }

            //Now check if this user is contributor for this playlist
            $contributor_array = [];
            if (!is_null($board->contributors)) {
                $contributor_array = json_decode($board->contributors, 1);
            }
            if (!in_array($user->id, $contributor_array)) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Board.add_episode_message1_lbl")
                ]);
            }
        } else {
            $board = Board::where('id', $request->board_id)->where('user_id', $user->id)->first();
        }

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        //check if this episode already added
        $episode_info = $board->episodes()->find($request->episode_id, ['id']);

        if ($episode_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.add_episode_message2_lbl")
            ]);
        }

        if (!$episode_info) {
            // added episode into board
            if ($request->contributor == 'Y') {
                $board->episodes()->attach([$request->episode_id => ['contributor_id' => $user->id]]);
            } else {
                $board->episodes()->attach($request->episode_id);
            }

            //Update Episode playlist_count
            try {
                Episode::where('id', $request->episode_id)->update(['playlist_count' => \DB::raw('playlist_count + 1')]);
            } catch (\Exception $ex) {
            }

            $episode_info_after_attach = Episode::find($request->episode_id);

            // added category into board
            foreach ($episode_info_after_attach->show->categories()->get(['id']) as $category_item) {
                $category_info = $board->categories()->find($category_item['id'], ['id']);
                if (!$category_info) {
                    $board->categories()->attach($category_item['id']);
                }
            }

            //Generate Collage Image
            $episodesData = $board->episodes()->orderBy('board_episode.updated_at', 'desc')
                ->pluck('id')->all();
            $filename = $this->generateCollageImage($episodesData);

            if (!empty($board->image) && $filename) {
                //@unlink(public_path('/uploads/boards/' . $board->image));
                if (Storage::disk('s3')->exists('boards/' . $board->image)) {
                    Storage::disk('s3')->delete('boards/' . $board->image);
                }
            }

            $board->update(['image' => $filename]);

            //check if this board is private or public
            if ($board->private == 'N') {
                if ($board->followers->count() > 0) {
                    //Insert into Feeds Table
                    (new Feed($board))->add(trim($request->episode_id), Feed::BOARD_UPDATE);

                    //Here we will send this to queue for sending push notification to user
                    //Dispatch Queue to schedule
                    dispatch(new SendNewEpisodePushNotification($board, $episode_info_after_attach));
                }
            }

            //Update Board updated_at timestamp
            $board->touch();
        }

        $data = [
            'id' => $board->id,
            'board_name' => $board->title,
            'image' => !empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
            'private' => $board->private,
            'shared' => $board->shared,
            'no_of_episodes' => $board->episodes()->count(),
            //"last_updated" => $board->updated_at->diffForHumans()
            'timestamp' => strtotime($board->updated_at),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $board->updated_at)->format('jS M Y'),
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove Episode into Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function remove_episode(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'episode_id' => 'required'
        ], [
            'board_id.required' =>config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
            'episode_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_episode_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board

        $board = Board::where('id', $request->board_id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        if ($board->user_id == $user->id) {
            //check if this episode already remove
            $episode_info = $board->episodes()->find($request->episode_id, ['id']);

            if (!$episode_info) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Board.remove_episode_message_lbl")
                ]);
            }

            if ($board->episodes()->count() == 1) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Board.remove_episode_message1_lbl")
                ]);
            }

            if ($episode_info) {
                // remove episode into board
                $board->episodes()->detach($request->episode_id);

                //Update Episode playlist_count
                try {
                    Episode::where('id', $request->episode_id)->update(['playlist_count' => \DB::raw('playlist_count - 1')]);
                } catch (\Exception $ex) {
                }

                $episode_ids = $board->episodes->pluck(['id'])->toArray();

                //Sync board with category
                $category = [];
                $episodes = Episode::whereIn('id', $episode_ids)->get(['id']);
                foreach ($episodes as $item) {
                    foreach ($item->show->categories()->get(['id']) as $category_item) {
                        $category[] = $category_item['id'];
                    }
                }
                $category = array_unique($category);
                $board->categories()->sync($category);

                //Generate Collage Image
                $episodes = $board->episodes()->orderBy('board_episode.updated_at', 'desc')
                    ->pluck('id')->all();
                $filename = $this->generateCollageImage($episodes);

                if (!empty($board->image) && $filename) {
                    //@unlink(public_path('/uploads/boards/' . $board->image));
                    if (Storage::disk('s3')->exists('boards/' . $board->image)) {
                        Storage::disk('s3')->delete('boards/' . $board->image);
                    }
                    $board->update(['image' => $filename]);
                }

                $followBoardFeedData = FeedRepo::where('type', 'Board Update')
                    ->where('typeable_type', 'App\\Models\\Board')
                    ->where('typeable_id', $board->id)
                    ->where('data', 'LIKE', '%"' . $request->episode_id . '"%');

                if ($followBoardFeedData->count()) {
                    $followBoardFeedData->delete();
                }

                //Remove Playlist Episode
                try {
                    $boardPlaylistEpisodeData = FeedRepo::where('type', 'Playlist Contributor Episode')
                    ->where('typeable_type', 'App\\Models\\Board')
                    ->where('typeable_id', $board->id)
                    ->where('data', 'LIKE', '%"' . $request->episode_id . '"%');
                    if ($boardPlaylistEpisodeData->count()) {
                        $boardPlaylistEpisodeData->delete();
                    }
                } catch(\Exception $ex) {}

                //Update Board updated_at timestamp
                $board->touch();
            }
        } else {
            //Now check if this user is contributor for this playlist
            $contributor_array = [];
            if (!is_null($board->contributors)) {
                $contributor_array = json_decode($board->contributors, 1);
            }
            //dd($user->id, $contributor_array);
            if (in_array($user->id, $contributor_array)) {
                //check if this episode already remove
                $episode_info = $board->episodes()->find($request->episode_id, ['id']);

                if (!$episode_info) {
                    return response()->api([
                        'status' => false,
                        'message' =>config('language.' . $this->getLocale() . ".Board.remove_episode_message2_lbl")
                    ]);
                }

                if ($board->episodes()->count() == 1) {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Board.remove_episode_message3_lbl")
                    ]);
                }

                if ($episode_info->pivot->contributor_id != $user->id) {
                    return response()->api([
                        'status' => false,
                        'message' =>config('language.' . $this->getLocale() . ".Board.remove_episode_message4_lbl")
                    ]);
                }

                if ($episode_info) {
                    // remove episode into board
                    $board->episodes()->detach($request->episode_id);

                    //Update Episode playlist_count
                    try {
                        Episode::where('id', $request->episode_id)->update(['playlist_count' => \DB::raw('playlist_count - 1')]);
                    } catch (\Exception $ex) {
                    }

                    $episode_ids = $board->episodes->pluck(['id'])->toArray();

                    //Sync board with category
                    $category = [];
                    $episodes = Episode::whereIn('id', $episode_ids)->get(['id']);
                    foreach ($episodes as $item) {
                        foreach ($item->categories()->get(['id']) as $category_item) {
                            $category[] = $category_item['id'];
                        }
                    }
                    $category = array_unique($category);
                    $board->categories()->sync($category);

                    //Generate Collage Image
                    $episodes = $board->episodes()->orderBy('board_episode.updated_at', 'desc')
                        ->pluck('id')->all();
                    $filename = $this->generateCollageImage($episodes);

                    if (!empty($board->image) && $filename) {
                        //@unlink(public_path('/uploads/boards/' . $board->image));
                        if (Storage::disk('s3')->exists('boards/' . $board->image)) {
                            Storage::disk('s3')->delete('boards/' . $board->image);
                        }
                        $board->update(['image' => $filename]);
                    }

                    $followBoardFeedData = FeedRepo::where('type', 'Board Update')
                        ->where('typeable_type', 'App\\Models\\Board')
                        ->where('typeable_id', $board->id)
                        ->where('data', 'LIKE', '%"' . $request->episode_id . '"%');

                    if ($followBoardFeedData->count()) {
                        $followBoardFeedData->delete();
                    }

                    //Remove Playlist Episode
                    try {
                        $boardPlaylistEpisodeData = FeedRepo::where('type', 'Playlist Contributor Episode')
                        ->where('typeable_type', 'App\\Models\\Board')
                        ->where('typeable_id', $board->id)
                        ->where('data', 'LIKE', '%"' . $request->episode_id . '"%');
                        if ($boardPlaylistEpisodeData->count()) {
                            $boardPlaylistEpisodeData->delete();
                        }
                    } catch(\Exception $ex) {}

                    //Update Board updated_at timestamp
                    $board->touch();
                }
            } else {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Board.remove_episode_message4_lbl")
                ]);
            }
        }

        $data = [
            'id' => $board->id,
            'board_name' => $board->title,
            'image' => !empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
            'private' => $board->private,
            'shared' => $board->shared,
            'no_of_episodes' => $board->episodes()->count(),
            'timestamp' => strtotime($board->updated_at),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $board->updated_at)->format('jS M Y'),
        ];

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Board.remove_episode_message5_lbl"),
            'data' => $data
        ]);
    }

    /**
     * My Followed Playlist
     * @param Request $request
     * @return type
     */
    public function my_followed_boards(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Select Board
        $query = $user->following_boards()->where('private', 'N')
            ->withCount(['episodes', 'followers']);

        //if ($request->sort_by == 'last_updated') {
        $query->orderBy('updated_at', 'Desc');
        // } else {
        //     $query->orderBy('title', 'ASC');
        // }

        if ($request->no_paginate == 'Yes') {
            $all_boards = $query->get();
        } else {
            $all_boards = $query->paginate(config('config.pagination.followed_board'), ['id', 'boards.user_id', 'title', 'image', 'private', 'image', 'boards.updated_at', 'shared', 'contributors']);
        }

        $data = [];
        foreach ($all_boards as $item) {
            $contributor_array = [];
            if (!is_null($item->contributors)) {
                $con_array = json_decode($item->contributors, 1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();
                foreach ($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }
            }
            $data[] = [
               'id' => $item->id,
               'board_name' => $item->title,
               'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
               'private' => $item->private,
               'shared' => $item->shared,
               'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors, 1)),
               'contributors' => $contributor_array,
               'no_of_episodes' => Helper::shorten_count($item->episodes_count),
               'no_of_follower' => Helper::shorten_count($item->followers_count),
               'name' => $item->user->notification_format_name,
               'username' => $item->user->username,
               'user_photo' => !empty($item->user->image) ? $item->user->getImage(100) : asset('uploads/default/user.png'),
              // "last_updated" => $item->updated_at->diffForHumans()
               'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
           ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Board.playlist_not_found");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_boards->total(),
                    'per_page' => $all_boards->perPage(),
                    'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                    'items' => $data
                ]
            ]);
        }
    }

    /**
     * Playlists page view / Search Page View.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        /*$query = Board::where('private', 'N')
            ->withCount(['episodes', 'followers'])->has('episodes');

        //Search Query
        if (!empty($request->keyword)) {
            $searchText = urldecode(trim($request->keyword));
            $query->where(function ($query) use ($searchText) {
                $query->orWhere('title', 'LIKE', '%' . $searchText . '%');

                //Search in username
                $query->orWhereHas('user', function ($q) use ($searchText) {
                    $q->where('username', 'LIKE', '%' . $searchText . '%');
                });
            });
        }

        if ($request->sort_by == 'last_updated') {
            $query->orderBy('updated_at', 'Desc');
        } else {
            $query->orderBy('title', 'ASC');
        }

        if (!empty($request->discover)) {
            $query->whereDoesntHave('followers', function ($q) use ($user) {
                $q->where('user_id', $user->id);
            });
        }

        $noofrecords = config('config.pagination.board');
        if (!empty($request->noofrecords)) {
            $noofrecords = $request->noofrecords;
        }
        //echo $query->toSql();
        if ($request->no_paginate == 'Yes') {
            $all_boards = $query->get(['id', 'user_id', 'title', 'image', 'private', 'episodes_count', 'updated_at', 'shared', 'contributors']);
        } else {
            $all_boards = $query->paginate($noofrecords, ['id', 'user_id', 'title', 'image', 'private', 'episodes_count', 'updated_at', 'shared', 'contributors']);
        }

        $follow_status = 0;
        $user_boards = $user->following_boards()->pluck('id')->all();

        $data = [];
        foreach ($all_boards as $item) {
            $follow_status = 0;
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 1;
            }
            $contributor_array = [];
            if (!is_null($item->contributors)) {
                $con_array = json_decode($item->contributors, 1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();
                foreach ($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }
            }
            $data[] = [
               'id' => $item->id,
               'board_name' => $item->title,
               'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
               'private' => $item->private,
               'shared' => $item->shared,
               'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors, 1)),
               'contributors' => $contributor_array,
               'board_owner' => $user->id == $item->user_id ? 1 : 0,
               'user_follow_status' => $follow_status,
               'username' => $item->user->username,
               'user_photo' => !empty($item->user->image) ? $item->user->getImage(100) : asset('uploads/default/user.png'),
               'no_of_episodes' => $item->episodes_count,
               'no_of_follower' => Helper::shorten_count($item->followers_count),
               'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Board.playlist_not_found");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_boards->total(),
                    'per_page' => $all_boards->perPage(),
                    'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                    'items' => $data
                ]
            ]);
        }*/
        $playlist = new Search('api', $user);
        
        return $playlist->playlist_search();
    }

    /**
     * Show Playlist
     * @param Request $request
     * @return type
     */
    public function show(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('config.missing_parameter1') . ' playlisy id' . config('config.missing_parameter2') . ' Playlist Details.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        //All Board Episodes
        $all_board_episode = $board->episodes()->published()->orderBy('id', 'DESC')->get(['id', 'title', 'duration', 'mp3', 'show_id', 'listen_count', 'explicit']);

        $episodeArray = [];
        foreach ($all_board_episode as $item) {
            $episodeArray[] = [
                'id' => $item->id,
                'title' => html_entity_decode($item->title),
                'private' => $item->private,
                'duration' => $item->getDurationText(),
                'audio_file' => $item->getAudioLink(),
                'show_id' => $item->show_id,
                'show_title' => !empty($item->show) ? html_entity_decode($item->show->title) : 'No Show',
                'image' => !empty($item->show) ? $item->show->getWSImage(200) : asset('uploads/default/board.png'),
                'listen_count' => Helper::shorten_count($item->listen_count),
                'explicit' => $item->show->explicit
            ];
        }

        $contributor_array = [];
        if (!is_null($board->contributors)) {
            $con_array = json_decode($board->contributors, 1);
            $con_array = array_reverse($con_array);
            $con_array = array_slice($con_array, 0, 5);
            $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();
            foreach ($contributor_lists as $key => $contributor_list) {
                $contributor_array[] = [
                    'name' => $contributor_list,
                    'username' => $key
                ];
            }
        }

        //$board_follow_status = $user->following_boards()->where('board_user.user_id', $board->user_id)->first();
        $board_follow_status = $user->following_boards()->where('board_user.board_id', $board->id)->first();

        $user_follow_status = $user->following()->where('user_follow.following_id', $board->user_id)->first();

        $data = [
            'id' => $board->id,
            'name' => $board->user->notification_format_name,
            'username' => $board->user->username,
            'board_name' => $board->title,
            'board_description' => $board->description,
            'private' => $board->private,
            'shared' => $board->shared,
            'total_contributors' => is_null($board->contributors) ? 0 : count(json_decode($board->contributors, 1)),
            'contributors' => $contributor_array,
            'board_owner' => $user->id == $board->user_id ? 1 : 0,
            'image' => !empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
            'user_image' => $board->user->image ? $board->user->getImage(100) : asset('uploads/default/user.png'),
            'followers_count' => $board->followers_count,
            'follow_status' => !empty($board_follow_status) ? 1 : 0,
            'user_follow_status' => !empty($user_follow_status) ? 1 : 0,
            'no_of_episodes' => count($episodeArray),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $board->updated_at)->format('jS M Y'),
            'episodes' => $episodeArray
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Show Playlist
     * @param Request $request
     * @return type
     */
    public function show_without_episodes(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $playlist = new Playlist('api', $user);

        return $playlist->trending_playlist_detail();

        /*$validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('config.missing_parameter1') . ' playlisy id' . config('config.missing_parameter2') . ' Playlist Details.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        $board_follow_status = $user->following_boards()->where('board_user.board_id', $board->id)->count();

        $user_follow_status = $user->following()->where('user_follow.following_id', $board->user_id)->count();

        $contributor_array = [];
        if (!is_null($board->contributors)) {
            $con_array = json_decode($board->contributors, 1);
            $con_array = array_reverse($con_array);
            $con_array = array_slice($con_array, 0, 5);
            $contributor_lists = User::whereIn('id', $con_array)->get(['id', 'first_name', 'username', 'image']);
            foreach ($contributor_lists as $key => $contributor_list) {
                $contributor_array[] = [
                'name' => $contributor_list->first_name,
                'username' => $contributor_list->username,
                'user_image' => $contributor_list->image ? $contributor_list->getImage(100) : asset('uploads/default/user.png')
                ];
            }
        }

        $data = [
            'id' => $board->id,
            'name' => $board->user->notification_format_name,
            'username' => $board->user->username,
            'board_name' => $board->title,
            'description' => $board->description,
            'private' => $board->private,
            'shared' => $board->shared,
            'total_contributors' => is_null($board->contributors) ? 0 : count(json_decode($board->contributors, 1)),
            'contributors' => $contributor_array,
            'board_owner' => $user->id == $board->user_id ? 1 : 0,
            'image' => !empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
            'user_image' => $board->user->image ? $board->user->getImage(100) : asset('uploads/default/user.png'),
            'followers_count' => $board->followers_count,
            'follow_status' => $board_follow_status,
            'user_follow_status' => $user_follow_status,
            'no_of_episodes' => $board->episodes()->count(),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $board->updated_at)->format('jS M Y')
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);*/
    }

    /**
     * All Playlist Episodes
     * @param Request $request
     */
    public function board_episodes(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('config.missing_parameter1') . ' playlist id' . config('config.missing_parameter2') . ' Playlist Episode Listing.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        //All Board Episodes
        $all_board_episode = $board->episodes()->published()->orderBy('id', 'DESC')->get(['id', 'title', 'duration', 'mp3', 'show_id', 'listen_count', 'explicit']);

        $data = [];
        foreach ($all_board_episode as $item) {
            $data[] = [
                'id' => $item->id,
                'title' => html_entity_decode($item->title),
                'duration' => $item->getDurationText(),
                'audio_file' => $item->getAudioLink(),
                'show_id' => $item->show_id,
                'show_title' => !empty($item->show) ? html_entity_decode($item->show->title) : 'No Show',
                'episode_image' => !empty($item->show) ? $item->show->getWSImage(200) : asset('uploads/default/board.png'),
                'listen_count' => Helper::shorten_count($item->listen_count),
                'explicit' => $item->show->explicit
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * All Playlist Episodes with Pagination
     * @param Request $request
     */
    public function episodes_with_paginate(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $playlist = new Playlist('api', $user);

        return $playlist->trending_playlist_episodes();

        /*$validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('config.missing_parameter1') . ' playlist id' . config('config.missing_parameter2') . ' Playlist Episode Listing.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        //All Board Episodes
        $all_board_episode = $board->episodes()->orderBy('id', 'DESC')->paginate(10, ['id', 'title', 'duration', 'mp3', 'show_id', 'listen_count', 'explicit']);

        //dd($all_board_episode->toArray());
        $data = [];
        foreach ($all_board_episode as $item) {
            $show_delete_button = 'no';
            if ($board->user_id == $user->id) {
                $show_delete_button = 'yes';
            } else if ($item->pivot->contributor_id == $user->id) {
                $show_delete_button = 'yes';
            }

            $owner_name = "";
            $owner_username = "";
            if($board->shared == "Y") {
                if ($item->pivot->contributor_id == 0) {
                    $owner_name = $board->user->first_name;
                    $owner_username = $board->user->username;
                } else {
                    if ($item->pivot->contributor_id == $user->id) {
                        $owner_name = $user->first_name;
                        $owner_username = $user->username;
                    } else {
                        $user_info = User::find($item->pivot->contributor_id, ['first_name', 'username']);
                        $owner_name = $user_info->first_name;
                        $owner_username = $user_info->username;
                    }
                }
            }            

            $data[] = [
                'id' => $item->id,
                'title' => html_entity_decode($item->title),
                'duration' => $item->getDurationText(),
                'audio_file' => $item->getAudioLink(),
                'show_id' => $item->show_id,
                'show_title' => !empty($item->show) ? html_entity_decode($item->show->title) : 'No Show',
                'episode_image' => !empty($item->show) ? $item->show->getWSImage(200) : asset('uploads/default/board.png'),
                'listen_count' => Helper::shorten_count($item->listen_count),
                'explicit' => $item->show->explicit,
                'show_delete_button' => $show_delete_button,
                'shared' => $board->shared,
                'owner_name' => $owner_name,
                'owner_username' => $owner_username                
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No Episode Found.';
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_board_episode->total(),
                'per_page' => $all_board_episode->perPage(),
                'pages' => ceil($all_board_episode->total() / $all_board_episode->perPage()),
                'items' => $data
            ]
        ]);*/
    }

    /**
     * My Network Playlist
     * @param Request $request
     * @return type
     */
    public function my_network_boards(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $follow_user = $user->following()->pluck('id')->all();

        $data = [];
        if (count($follow_user) > 0) {
            //Select Board
            $query = Board::where('private', 'N')->whereIn('user_id', $follow_user)
                ->withCount('episodes')->has('episodes');

            $query->orderBy('updated_at', 'Desc');

            $all_boards = $query->paginate(config('config.pagination.followed_board'));

            $user_boards = $user->following_boards()->pluck('id')->all();
            //dump($user->toArray(), $user_boards);

            foreach ($all_boards as $item) {
                $follow_status = 'false';
                if (@in_array($item->id, $user_boards)) {
                    $follow_status = 'true';
                }
                $contributor_array = [];
                if (!is_null($item->contributors)) {
                    $con_array = json_decode($item->contributors, 1);
                    $con_array = array_reverse($con_array);
                    $con_array = array_slice($con_array, 0, 5);
                    $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();
                    foreach ($contributor_lists as $key => $contributor_list) {
                        $contributor_array[] = [
                            'name' => $contributor_list,
                            'username' => $key
                        ];
                    }
                }
                $data[] = [
                    'id' => $item->id,
                    'board_name' => $item->title,
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                    'private' => $item->private,
                    'shared' => $item->shared,
                    'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors, 1)),
                    'contributors' => $contributor_array,
                    'no_of_episodes' => Helper::shorten_count($item->episodes_count),
                    'no_of_follower' => Helper::shorten_count($item->followers_count),
                    'follow_status' => $follow_status,
                    'name' => $item->user->notification_format_name,
                    'username' => $item->user->username,
                    'user_photo' => !empty($item->user->image) ? $item->user->getImage(100) : asset('uploads/default/user.png'),
                    'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
                ];
            }
        }

        $message = '';
        if (count($data) == 0) {
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Board.my_network_boards_message_lbl"),
                'data' => []
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_boards->total(),
                'per_page' => $all_boards->perPage(),
                'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * Playlist Followed by User
     * @param Request $request
     * @return type
     */
    public function followed_board_user(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $playlist = new Playlist('api', $user);

        return $playlist->trending_playlist_follwers();

        /*$validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('config.missing_parameter1') . ' playlist id' . config('config.missing_parameter2') . ' Folllow Board User.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();

        $message = '';
        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        $all_users = $board->followers()->with('categories')->where('verified', 'Verified')
            ->where('id', '!=', $user->id)
            ->where('admin_status', 'Approved')
            ->withCount(['follower as follower_status_count' => function ($qry) use ($user) {
                $qry->where('id', $user->id);
            }])->paginate(10, ['id', 'first_name', 'last_name', 'username', 'image']);

        //dd($all_users->toArray());

        $data = [];
        foreach ($all_users as $item) {
            $data[] = [
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follow_status' => $item->follower_status_count
            ];
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_users->total(),
                'per_page' => $all_users->perPage(),
                'pages' => ceil($all_users->total() / $all_users->perPage()),
                'items' => $data
            ]
        ]);*/
    }

    /**
     * Added Episode into multiple Playlists.
     *
     * @param Request $request
     * @return mixed
     */
    public function add_episode_into_multiple_boards(Request $request)
    {
        $user = Auth::guard('api')->user();        

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'episode_id' => 'required'
        ], [
            'board_id.required' =>config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
            'episode_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_episode_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $boards = explode(',', $request->board_id);
        $boards = array_filter($boards);
        $boards = array_unique($boards);        

        //dd($boards[0]);
        if (count($boards) == 1) {
            $board = Board::where('id', $boards[0])->first();

            if (!$board) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
                ]);
            }

            if ($board->user_id != $user->id) {
                //Check if this playlist is private or not
                if ($board->private == 'Y') {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Board.add_episode_into_mb_message_lbl")
                    ]);
                }

                //Now check if this user is contributor for this playlist
                $contributor_array = [];
                if (!is_null($board->contributors)) {
                    $contributor_array = json_decode($board->contributors, 1);
                }

                if (!in_array($user->id, $contributor_array)) {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Board.add_episode_into_mb_message1_lbl")
                    ]);
                }
            }

            //check if this episode already added
            $episode_info = $board->episodes()->find($request->episode_id, ['id']);
            //dd($episode_info);

            if ($episode_info) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Board.add_episode_into_mb_message2_lbl")
                ]);
            }
        }

        foreach ($boards as $board) {
            try {
                //Select Board
                //$board = Board::where('id', $board)->where('user_id', $user->id)->first();

                $board = Board::where('id', $board)->first();

                if (!empty($board)) {
                    //If user is a owner of that playlist
                    if ($board->user_id == $user->id) {
                        //check if this episode already added
                        $episode_info = $board->episodes()->find($request->episode_id, ['id']);

                        if (empty($episode_info)) {
                            // added episode into board
                            $board->episodes()->attach($request->episode_id);

                            //Update Episode playlist_count
                            try {
                                Episode::where('id', $request->episode_id)->update(['playlist_count' => \DB::raw('playlist_count + 1')]);
                            } catch (\Exception $ex) {
                            }

                            $episode_info_after_attach = Episode::find($request->episode_id, ['id', 'title']);

                            // added category into board
                            // foreach ($episode_info_after_attach->show->categories()->get(['id']) as $category_item) {
                            //     $category_info = $board->categories()->find($category_item['id'], ['id']);
                            //     if (!$category_info) {
                            //         $board->categories()->attach($category_item['id']);
                            //     }
                            // }

                            //Generate Collage Image
                            $episodesData = $board->episodes()->orderBy('board_episode.updated_at', 'desc')
                                ->pluck('id')->all();
                            $filename = $this->generateCollageImage($episodesData);

                            if (!empty($board->image) && $filename) {
                                //@unlink(public_path('/uploads/boards/' . $board->image));
                                if (Storage::disk('s3')->exists('boards/' . $board->image)) {
                                    Storage::disk('s3')->delete('boards/' . $board->image);
                                }
                            }

                            $board->update(['image' => $filename]);

                            //check if this board is private or public
                            if ($board->private == 'N') {
                                if ($board->followers->count() > 0) {
                                    //Insert into Feeds Table
                                    //(new Feed($board))->add(trim($request->episode_id), Feed::BOARD_UPDATE);
                                    $attributes = [
                                        'typeable_type' => 'App\Models\Board',
                                        'typeable_id' => $board->id,
                                        'data' => json_encode([trim($request->episode_id)]),
                                        'owner_id' => $board->user_id,
                                        'type' => Feed::BOARD_UPDATE
                                    ];
                                    FeedRepo::create($attributes);

                                    //Here we will send this to queue for sending push notification to user
                                    //Dispatch Queue to schedule
                                    dispatch(new SendNewEpisodePushNotificationV2($board, $episode_info_after_attach, $user->id));
                                }
                            }

                            //Update Board updated_at timestamp
                            $board->touch();
                        }
                    } else {                        
                        //Now check if this user is contributor for this playlist
                        $contributor_array = [];
                        if (!is_null($board->contributors)) {
                            $contributor_array = json_decode($board->contributors, 1);
                        }
                        //dd($user->id, $contributor_array);
                        if (in_array($user->id, $contributor_array)) {
                            //check if this episode already added
                            $episode_info = $board->episodes()->find($request->episode_id, ['id']);

                            if (empty($episode_info)) {
                                // added episode into board
                                //$board->episodes()->attach($request->episode_id);

                                $board->episodes()->attach([$request->episode_id => ['contributor_id' => $user->id]]);

                                //Update Episode playlist_count
                                try {
                                    Episode::where('id', $request->episode_id)->update(['playlist_count' => \DB::raw('playlist_count + 1')]);
                                } catch (\Exception $ex) {
                                }

                                $episode_info_after_attach = Episode::find($request->episode_id, ['id', 'title']);

                                // added category into board
                                // foreach ($episode_info_after_attach->show->categories()->get(['id']) as $category_item) {
                                //     $category_info = $board->categories()->find($category_item['id'], ['id']);
                                //     if (!$category_info) {
                                //         $board->categories()->attach($category_item['id']);
                                //     }
                                // }

                                //Generate Collage Image
                                $episodesData = $board->episodes()->orderBy('board_episode.updated_at', 'desc')
                                    ->pluck('id')->all();
                                $filename = $this->generateCollageImage($episodesData);

                                if (!empty($board->image) && $filename) {
                                    //@unlink(public_path('/uploads/boards/' . $board->image));
                                    if (Storage::disk('s3')->exists('boards/' . $board->image)) {
                                        Storage::disk('s3')->delete('boards/' . $board->image);
                                    }
                                }

                                $board->update(['image' => $filename]);

                                //check if this board is private or public
                                if ($board->private == 'N') {
                                    if ($board->followers->count() > 0) {
                                        //Insert into Feeds Table
                                        //(new Feed($board))->add(trim($request->episode_id), Feed::BOARD_UPDATE);

                                        //Added into tbl_user_feeds table for notification
                                        $attributes = [
                                            'typeable_type' => 'App\Models\Board',
                                            'typeable_id' => $board->id,
                                            'data' => json_encode([trim($request->episode_id)]),
                                            'owner_id' => $board->user_id,
                                            'type' => Feed::BOARD_UPDATE
                                        ];
                                        FeedRepo::create($attributes);

                                        //Send push notification to owner of this playlist
                                        $this->pushNotificationforOwner($board, $user);

                                        //Added into tbl_user_feeds table for notification
                                        $attributes2 = [
                                            'typeable_type' => 'App\Models\Board',
                                            'typeable_id' => $board->id,
                                            'data' => json_encode([trim($request->episode_id), trim('$' . $board->user_id . '$')]),
                                            'owner_id' => $board->user_id,
                                            'type' => Feed::PLAYLIST_CONTRIBUTOR_EPISODE
                                        ];
                                        FeedRepo::create($attributes2);

                                        //Here we will send this to queue for sending push notification to user
                                        //Dispatch Queue to schedule
                                        dispatch(new SendNewEpisodePushNotificationV2($board, $episode_info_after_attach, $user->id));
                                    }
                                }

                                //Update Board updated_at timestamp
                                $board->touch();
                            }
                        }
                    }
                }
            } catch (\Exception $ex) {
                continue;
            }
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Board.add_episode_into_mb_message3_lbl")
        ]);
    }

    /**
     * Send push notification to owner
     *
     * @param [type] $board
     * @param [type] $user
     * @return void
     */
    protected function pushNotificationforOwner($board, $user)
    {
        $contributor_users = User::where('id', $board->user_id)->first(['id']);

        $tokens = $contributor_users->pushIds()->where('push_ids.status', 1)->pluck('token')->all();

        //Send Push Notification to the Users
        if (count($tokens) > 0) {
            $tokens = array_unique($tokens);
            $title = 'Playlist Update';
            $description = $user->notification_format_name . ' has been added new episode in your Playlist - ' . $board->title;
            $icon = '';
            $data = [
                'title' => 'Playlist Update',
                'username' => $user->username,
                'board_id' => $board->id,
                'type' => 'SPL',
                'desc' => $user->notification_format_name . ' has been added new episode in your Playlist - ' . $board->title,
                'tray_icon' => '',
                'image' => !empty($user->image) ? $user->getImage(200) : '',
                'mediaUrl' => !empty($user->image) ? $user->getImage(200) : '',
            ];

            dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
        }
    }

    /**
     * Generate Collage Image
     * @param type $episodes
     * @return string
     */
    protected function generateCollageImage($episodes)
    {
        if (count($episodes) > 0) {
            $show_images = Show::whereHas('episodes', function ($q) use ($episodes) {
                $q->whereIn('id', $episodes);
            })->take(4)->pluck('image')->all();

            $images = [];
            foreach ($show_images as $epp_image) {
                $images[] = config('config.s3_url') . '/shows/' . $epp_image;
            }


            if (count($images) > 1) {
                $images = array_pad($images, 4, Image::canvas(600, 600, '#C0C0C0'));
            }

            if (count($images)) {
                $imageArray = [];
                foreach ($images as $image) {
                    try {
                        $imageArray[] = Image::make($image);
                    } catch (\Exception $e) {
                        continue;
                    }
                }

                if (count($imageArray) > 0) {
                    $collage = Collage::make(400, 400)->from($imageArray);
                    $filename = date('YmdHis') . rand(100000, 999999) . '.png';
                    $collage->save('uploads/boards/' . $filename, 100);

                    Storage::disk('s3')->put('boards/' . $filename, file_get_contents(asset('uploads/boards/' . $filename)));

                    unlink(public_path('uploads/boards/' . $filename));

                    return $filename;
                }

                return '';
            }
        }

        return '';
    }

    /**
     * Contributor Listing
     *
     * @param Request $request
     * @return void
     */
    public function contributor_listing(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Playlist
        $board = Board::where('id', $request->board_id)->first();

        $message = '';
        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        try {
            if (is_null($board->contributors)) {
                return response()->api([
                    'status' => true,
                    'message' => 'No Contributor Added.',
                    'data' => [
                        'total' => 0,
                        'per_page' => 0,
                        'pages' => 0,
                        'items' => []
                    ]
                ]);
            }

            $all_users = User::whereIn('id', json_decode($board->contributors, 1))
                ->with('categories')->where('verified', 'Verified')->where('admin_status', 'Approved')
                ->withCount(['follower as follower_status_count' => function ($qry) use ($user) {
                    $qry->where('id', $user->id);
                }])->paginate(10, ['id', 'first_name', 'last_name', 'username', 'image']);

            $data = [];
            foreach ($all_users as $item) {
                $data[] = [
                    'name' => $item->notification_format_name,
                    'username' => $item->username,
                    'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                    'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                    'follow_status' => $item->follower_status_count,
                    'follower_count' => Helper::shorten_count($item->follower()->verified()->count())
                ];
            }

            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $all_users->total(),
                    'per_page' => $all_users->perPage(),
                    'pages' => ceil($all_users->total() / $all_users->perPage()),
                    'items' => $data
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => 'Please try again.'
            ]);
        }
    }

    /**
     * This function is use for adding new contributors
     *
     * @param Request $request
     * @return void
     */
    public function adding_contributors(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'username' => 'required',
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
            'username.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_username"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->first();

        $message = '';
        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        try {
            if ($board->shared == 'N') {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Board.adding_contributors_message_lbl")
                ]);
            }

            $usernames = explode(',', $request->username);
            $usernames = array_unique(array_filter($usernames));

            if (count($usernames) > 0) {
                $existing_contributors = [];
                if (!is_null($board->contributors)) {
                    $existing_contributors = json_decode($board->contributors);
                }

                $user_ids = [];
                $tokens = [];
                foreach ($usernames as $item) {
                    $contributor_users = User::where('username', trim($item))->first(['id']);
                    //Get Push Notification tokens
                    if (!in_array($contributor_users->id, $existing_contributors)) {
                        $user_ids[] = trim($contributor_users->id);
                        $tokens = array_merge($tokens, $contributor_users->pushIds()->where('push_ids.status', 1)->pluck('token')->all());

                        //Added into tbl_user_feeds table for notification
                        $attributes = [
                            'typeable_type' => 'App\Models\Board',
                            'typeable_id' => $board->id,
                            'data' => json_encode([trim($contributor_users->id)]),
                            'type' => Feed::PLAYLIST_CONTRIBUTOR
                        ];
                        FeedRepo::create($attributes);
                    }
                }

                //merge array
                $final_array = array_merge($existing_contributors, $user_ids);
                $final_array = array_unique($final_array);

                $board->fill(['contributors' => json_encode($final_array)])->save();

                //Send Push Notification to the Users
                if (count($tokens) > 0) {
                    $tokens = array_unique($tokens);
                    $title = 'Shared Playlist';
                    $description = 'You have been added by ' . $user->notification_format_name . ' as a Contributor to the Shared Playlist - ' . $board->title;
                    $icon = '';
                    $data = [
                        'title' => 'You have been added as a Contributor',
                        'username' => $user->username,
                        'board_id' => $board->id,
                        'type' => 'SPL',
                        'desc' => 'You have been added by ' . $user->notification_format_name . ' as a Contributor to the Shared Playlist - ' . $board->title,
                        'tray_icon' => '',
                        'image' => !empty($user->image) ? $user->getImage(200) : '',
                        'mediaUrl' => !empty($user->image) ? $user->getImage(200) : '',
                    ];

                    dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
                }
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.adding_contributors_message1_lbl")
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Board.adding_contributors_message2_lbl")
        ]);
    }

    /**
     * Remove Contributor from playlist
     *
     * @param Request $request
     * @return void
     */
    public function remove_contributor(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'username' => 'required',
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
            'username.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_username"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Playlist
        $board = Board::where('id', $request->board_id)->first();

        $message = '';
        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        try {
            $contributor_user = User::where('username', trim($request->username))->first(['id']);
            $user_id = trim($contributor_user->id);

            $existing_contributors = json_decode($board->contributors, true);

            if (in_array($user_id, $existing_contributors)) {
                $current_arr = array_diff($existing_contributors, [$user_id]);
                $current_arr = array_flatten($current_arr);
                if (count($current_arr) == 0) {
                    $board->fill(['contributors' => null])->save();
                } else {
                    $board->fill(['contributors' => json_encode($current_arr)])->save();
                }

                //remove from tbl_user_feeds table
                $boardFeedData = FeedRepo::where('type', 'Playlist Contributor')
                    ->where('typeable_type', 'App\\Models\\Board')
                    ->where('typeable_id', $board->id)
                    ->where('data', 'LIKE', '%"' . $user_id . '"%');
                if ($boardFeedData->count()) {
                    $boardFeedData->delete();
                }
            } else {
                return response()->api([
                    'status' => false,
                    'message' =>  config('language.' . $this->getLocale() . ".Board.remove_contributor_message_lbl")
                ]);
            }
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.remove_contributor_message1_lbl")
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Board.remove_contributor_message2_lbl")
        ]);
    }

    /**
     * Board Followed by User
     * @param Request $request
     * @return type
     */
    public function followed_playlist_users(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Board.missing_parameter_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->where('user_id', $user->id)->first();

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Board.playlist_not_found")
            ]);
        }

        $contributor_array = [];
        if (!is_null($board->contributors)) {
            $contributor_array = json_decode($board->contributors, 1);
        }

        $sqlQuery = $board->followers()->with('categories')->where('verified', 'Verified')
            ->where('id', '!=', $user->id)->where('admin_status', 'Approved')
            ->withCount(['follower as follower_status_count' => function ($qry) use ($user) {
                $qry->where('id', $user->id);
            }]);

        if (count($contributor_array) > 0) {
            $sqlQuery->whereNotIn('id', $contributor_array);
        }

        $all_users = $sqlQuery->paginate(10, ['id', 'first_name', 'last_name', 'username', 'image']);

        $data = [];
        foreach ($all_users as $item) {
            $data[] = [
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follow_status' => $item->follower_status_count,
                'follower_count' => Helper::shorten_count($item->follower()->verified()->count())
            ];
        }
        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_users->total(),
                'per_page' => $all_users->perPage(),
                'pages' => ceil($all_users->total() / $all_users->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * Playlists //added by datta
     *
     * @param Type $request
     * @return type
     * @throws conditon
     **/
    public function playlists(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        try {
            $playlist = new Playlist('api', $user);
            return $playlist->playlist();
        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
    }
}
