<?php
namespace App\Http\Controllers\Api\new_version\V8;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Traits\ResponseFormat;
use App\ClassesV3\V3_3\IP2Location;
use App\Models\MarketplaceCountry;
use App\Models\Show;
use Carbon\Carbon;
use App\Models\EpisodeDetail;
use App\Traits\HelperV2;
use App\Models\Order;
use App\Models\ShowPurchase;
use App\Models\EpisodePurchase;
use App\Jobs\InvoiceEmail;
use App\Jobs\SendEmailUser;
use App\ClassesV3\Feed;
use App\Jobs\SendPushNotification;
use Razorpay\Api\Api;


class CartController extends Controller
{

    public function index(){

        $user = Auth::guard('api')->user();
        
        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

    }
}