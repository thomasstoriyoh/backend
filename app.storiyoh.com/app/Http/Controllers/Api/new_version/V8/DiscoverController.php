<?php

namespace App\Http\Controllers\Api\new_version\V8;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Board;
use App\Models\Category;
use App\Models\Show;
use App\Traits\Helper;
use App\Models\Network;
use App\Models\Chart;
use Illuminate\Support\Facades\Validator;
use App\Models\FeedConfiguration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Episode;
use App\Models\SmartPlaylist;
use Illuminate\Support\Facades\Cache;
use App\ClassesV3\V3_3\Discover;
use App\Traits\ResponseFormat;

class DiscoverController extends Controller
{  
    use ResponseFormat;

    protected $user;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user= Auth::guard('api')->user();
            if (!$this->user) {
                $response = [
                    'status' => false,
                    'code' => 401,
                    'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                ];
                return $this->sendAuthErrorResponse($response, 401);
            }
            return $next($request);
         });
         
    }
    /**
     * This function is use for get Discover dashboard featured show data
     * @param Request $request
     * @return response
    */    
    public function discover_dashboard_featured_show_data(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }        
        
        if (config('config.cache_status.featured_show')) {
            if (Cache::get('featured.show')) {
                $featured_shows = Cache::get('featured.show');                
            } else {
                $query = Show::published()->featured()->select(['id', 'title', 'description', 'image', 'updated_at']);
                $featured_shows = $query->orderBy('no_of_subscribers', 'desc')->orderBy('updated_at', 'DESC')->take(10)->get(['id', 'title', 'description', 'image', 'updated_at']);

                //This code is use for cache that query data
                $expiresAt = Carbon::now()->endOfDay()->addSecond();
                Cache::put('featured.show', $featured_shows, $expiresAt);                
            }
        } else {
            $query = Show::published()->featured()->select(['id', 'title', 'description', 'image', 'updated_at']);
            $featured_shows = $query->orderBy('no_of_subscribers', 'desc')->orderBy('updated_at', 'DESC')->take(10)->get(['id', 'title', 'description', 'image', 'updated_at']);
        }

        $subscribe = false;
        $subscribers = $user->subscribers()->pluck('show_id')->all();
        $shows_data = [];
        foreach ($featured_shows as $item) {
            $subscribe = @in_array($item->id, $subscribers) ? 'true' : 'false';
            $shows_data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'description' => strip_tags($item->description),
                'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                'subscribe' => $subscribe,
                'categories_count' => 0
             ];
        }        
        
        return response()->api([
             'status' => true,
             'data' => $shows_data
         ]);
    }
    
    /**
     * This function is use for get Discover dashboard popular shows data
     * @param Request $request
     * @return response
     */    
    public function discover_dashboard_popular_shows_data(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        if (config('config.cache_status.popular_shows')) {
            if (Cache::get('popular.show')) {
                $category_shows = Cache::get('popular.show');                
            } else {
                $query = Show::published()->select(['id', 'title', 'image', 'updated_at']);
                
                // $query->whereDoesntHave('subscribers', function ($q) use ($user) {
                //     $q->where('user_id', $user->id);
                // });
        
                $category_shows = $query->take(10)->orderBy('no_of_subscribers', 'desc')->orderBy('updated_at', 'DESC')->get(['id', 'title', 'image', 'updated_at']);

                //This code is use for cache that query data
                $expiresAt = Carbon::now()->endOfDay()->addSecond();
                Cache::put('popular.show', $category_shows, $expiresAt);
            }
        } else {
            $query = Show::published()->select(['id', 'title', 'image', 'updated_at']);
            
            // $query->whereDoesntHave('subscribers', function ($q) use ($user) {
            //     $q->where('user_id', $user->id);
            // });
    
            $category_shows = $query->take(10)->orderBy('no_of_subscribers', 'desc')->orderBy('updated_at', 'DESC')->get(['id', 'title', 'image', 'updated_at']);
        }        
        
        $shows_data = [];        
        $subscribers = $user->subscribers()->pluck('show_id')->all();
        $subscribe = 'false';
        foreach ($category_shows as $item) {
            $subscribe = @in_array($item->id, $subscribers) ? 'true' : 'false';
            $shows_data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                'subscribe' => $subscribe,
                'categories_count' => 0//$item->categories_count
             ];
        }        

        return response()->api([
             'status' => true,
             'data' => $shows_data
         ]);
    }

    /**
     * This function is use for get Discover dashboard popular shows more data
     * @param Request $request
     * @return response
    */
    public function discover_dashboard_popular_shows_data_more(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = Show::published()->select(['id', 'title', 'image', 'updated_at', 'no_of_subscribers']);

        $query->whereDoesntHave('subscribers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        if ($request->no_paginate == 'Yes') {
            $popolar_shows = $query->get(['id', 'title', 'image', 'no_of_subscribers']);
        } else {
            $popolar_shows = $query->orderBy('no_of_subscribers', 'desc')
                ->orderBy('updated_at', 'DESC')->paginate(config('config.pagination.discover_show_view_more'), ['id', 'title', 'image', 'updated_at', 'no_of_subscribers']);
        }        

        $subscribe = 'false';
        $data = [];
        foreach ($popolar_shows as $item) {
            $data[] = [
               'id' => $item->id,
               'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
               'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
               'subscribe' => $subscribe,
               'no_of_episodes' => 0,//$item->episodes()->published()->count(),
               'no_of_subscribers' => empty($item->no_of_subscribers) ? 0 : $item->no_of_subscribers               
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
        }
        
        return response()->api([
            'status' => true,
            'data' => [
                'total' => $popolar_shows->total(),
                'per_page' => $popolar_shows->perPage(),
                'pages' => ceil($popolar_shows->total() / $popolar_shows->perPage()),
                'items' => $data,
                'message' => $message
            ]
        ]);        
    }    

    /**
     * This webservice use for trending show for
     * discover section
     *
     * @param Request $request
     * @return void
     */    
    public function discover_trending_shows(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $feed_config = FeedConfiguration::first();

        try {
            if (config('config.cache_status.trending_shows')) {
                if (Cache::get('trending.show')) {
                    $all_shows = Cache::get('trending.show');
                } else {
                    $all_shows = DB::select(DB::raw("SELECT `show_id`, count(*) as `total` FROM `tbl_show_user` WHERE `created_at` BETWEEN '" . Carbon::now()->subDay($feed_config->trending_shows) . "' AND '" . Carbon::now() . "' group by show_id order by total desc limit " . config('config.pagination.discover_trending_shows')));
                    
                    //This code is use for cache that query data
                    $expiresAt = Carbon::now()->endOfDay()->addSecond();
                    Cache::put('trending.show', $all_shows, $expiresAt);                    
                }
            } else {
                $all_shows = DB::select(DB::raw("SELECT `show_id`, count(*) as `total` FROM `tbl_show_user` WHERE `created_at` BETWEEN '" . Carbon::now()->subDay($feed_config->trending_shows) . "' AND '" . Carbon::now() . "' group by show_id order by total desc limit " . config('config.pagination.discover_trending_shows')));                
            }
            
            $subscribers = $user->subscribers()->pluck('show_id')->all();
            $data = [];
            foreach($all_shows as $show) {
                $subscribe = @in_array($show->show_id, $subscribers) ? 'true' : 'false';
                $dataInfo = Show::where('id', $show->show_id)->first(['id', 'title', 'image', 'no_of_subscribers']);
                if($dataInfo) {
                    $data[] = [
                        'id' => $dataInfo->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($dataInfo->title))),
                        'image' => !empty($dataInfo->image) ? $dataInfo->getWSImage(200) : asset('uploads/default/show.png'),                
                        'subscribe' => $subscribe,
                        'no_of_episodes' => 0,//$dataInfo->episodes()->published()->count(),
                        'no_of_subscribers' => $dataInfo->no_of_subscribers
                    ];
                }
            }    
    
            return response()->api([
                'status' => true,
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => []                
            ]);
        }        
    }
    
    /**
     * This webservice use for view all trending show
     *
     * @param Request $request
     * @return void
     */
    public function discover_trending_shows_view_more(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $feed_config = FeedConfiguration::first();

        $max = config('config.pagination.discover_trending_shows_more');
        $page_no = $request->page ? $request->page : 1; 
        $start = ($page_no) ? ($page_no-1) * $max : 0;

        try {
            $all_shows_count = DB::select(DB::raw("SELECT `show_id`, count(*) as `total` FROM `tbl_show_user` WHERE `created_at` BETWEEN '".Carbon::now()->subDay($feed_config->trending_shows)."' AND '".Carbon::now()."' group by show_id order by total desc"));
            
            $all_shows = DB::select(DB::raw("SELECT `show_id`, count(*) as `total` FROM `tbl_show_user` WHERE `created_at` BETWEEN '".Carbon::now()->subDay($feed_config->trending_shows)."' AND '".Carbon::now()."' group by show_id order by total desc limit ?, ?"), array($start, $max));
            
            // $all_shows = DB::select(DB::raw("SELECT `show_id`, count(*) as `total` FROM `tbl_show_user` WHERE `created_at` BETWEEN '".Carbon::now()->subDay($feed_config->trending_shows)."' AND '".Carbon::now()."' group by show_id order by total desc limit $start, ".$max));
            
            $subscribers = $user->subscribers()->pluck('show_id')->all();
            $data = [];
            foreach($all_shows as $show) {
                $subscribe = @in_array($show->show_id, $subscribers) ? 'true' : 'false';
                $dataInfo = Show::where('id', $show->show_id)->first(['id', 'title', 'image', 'no_of_subscribers']);
                if($dataInfo) {
                    $data[] = [
                        'id' => $dataInfo->id,
                        'title' => trim(str_replace("\n", '', html_entity_decode($dataInfo->title))),
                        'image' => !empty($dataInfo->image) ? $dataInfo->getWSImage(200) : asset('uploads/default/show.png'),                
                        'subscribe' => $subscribe,
                        'no_of_episodes' => 0,//$dataInfo->episodes()->published()->count(),
                        'no_of_subscribers' => $dataInfo->no_of_subscribers
                    ];
                }
            }                        
    
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => count($all_shows_count),
                    'per_page' => $max,
                    'pages' => ceil(count($all_shows_count) / $max),
                    'items' => $data,
                    'message' => ""                
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'shows' => []
                ]
            ]);
        }                
    }

    /**
     * New Added Shows
     *
     * @param Request $request
     * @return void
     */
    public function discover_new_shows (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        //$latest_shows = Show::published()->with('episodes', 'subscribers')
        $latest_shows = Show::published()->with('subscribers')
            ->take(config('config.pagination.discover_new_shows'))
            ->orderBy('created_at', 'desc')->get(['id', 'title', 'image', 'created_at', 'no_of_subscribers']);
        
        $subscribers = $user->subscribers()->pluck('show_id')->all();
        $data = [];
        foreach($latest_shows as $show) {
            $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';
            $data[] = [
                'id' => $show->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'no_of_episodes' => 0,//$show->episodes()->published()->count(),
                'no_of_subscribers' => $show->no_of_subscribers,
                'subscribe' => $subscribe,
                'created_at' => $show->created_at->diffForHumans(),
            ];            
        }

        return response()->api([
            'status' => true,
            'message' => "",
            'data' => $data
        ]);        
    }

    /**
     * New Added Shows - View More
     *
     * @param Request $request
     * @return void
     */
    public function discover_new_shows_view_more (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        //$query = Show::published()->with('episodes', 'subscribers');
        $query = Show::published()->with('subscribers');
        $latest_shows = $query->orderBy('created_at', 'desc')
            ->paginate(config('config.pagination.discover_new_users_more'), ['id', 'title', 'image', 'created_at', 'no_of_subscribers']);
        
        $subscribers = $user->subscribers()->pluck('show_id')->all();
        $data = [];
        foreach($latest_shows as $show) {
            $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';
            $data[] = [
                'id' => $show->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'no_of_episodes' => 0,//$show->episodes()->published()->count(),
                'no_of_subscribers' => $show->no_of_subscribers,
                'subscribe' => $subscribe,
                'created_at' => $show->created_at->diffForHumans(),
            ];            
        }

        return response()->api([
            'status' => true,
            'message' => "",
            'data' => [
                'total' => $latest_shows->total(),
                'per_page' => $latest_shows->perPage(),
                'pages' => ceil($latest_shows->total() / $latest_shows->perPage()),
                'items' => $data
            ]
        ]);        
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function discover_dashboard_data_for_categories(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {            
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        if (config('config.cache_status.show_categories')) {
            if (Cache::get('show.categories')) {
                $categories = Cache::get('show.categories');
            } else {
                $categories = Category::published()->orderBy('title')->get(['id', 'title']);

                //This code is use for cache that query data
                $expiresAt = Carbon::now()->endOfDay()->addSecond();
                Cache::put('show.categories', $categories, $expiresAt);
            }
        } else {
            $categories = Category::published()->orderBy('title')->get(['id', 'title']);
        }

        $categories_data = [];
        foreach ($categories as $values) {
            $categories_data[] = [
                'id' => $values->id,
                'title' => $values->title
            ];
        }        

        return response()->api([
             'status' => true,
             'data' => $categories_data
         ]);
    }

    /**
     * This function is use for get Discover dashboard featured playlist data
     * @param Request $request
     * @return response
     */
    public function discover_dashboard_featured_playlist_data(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        if (config('config.cache_status.featured_playlist')) {
            if (Cache::get('featured.playlist')) {
                $boards = Cache::get('featured.playlist');                
            } else {
                $query = Board::where('private', 'N')->featured()->has('episodes');
                $boards = $query->take(5)->orderBy('updated_at', 'desc')
                    ->get(['id', 'user_id', 'title', 'description', 'image', 'updated_at', 'private', 'shared', 'contributors']);
                
                //This code is use for cache that query data
                $expiresAt = Carbon::now()->endOfDay()->addSecond();
                Cache::put('featured.playlist', $boards, $expiresAt);                
            }
        } else {
            $query = Board::where('private', 'N')->featured()->has('episodes');
            $boards = $query->take(5)->orderBy('updated_at', 'desc')
                ->get(['id', 'user_id', 'title', 'description', 'image', 'updated_at', 'private', 'shared', 'contributors']);            
        }

        //$follow_status = 0;
        $user_boards = $user->following_boards()->pluck('id')->all();

        $board_data = [];
        foreach ($boards as $item) {
            $follow_status = 'false';
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 'true';
            }
            $contributor_array = [];
            if(! is_null($item->contributors)) {
                $con_array = json_decode($item->contributors,1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);                
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();                
                foreach($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }                
            }
            $board_data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'description' => strip_tags($item->description),
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'follow_status' => $follow_status,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,
                'shared' => $item->shared,
                'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors,1)),
                'contributors' => is_null($item->contributors) ? "" : $contributor_array              
            ];
        }

        $message = "";
        if(count($board_data) == 0){
            $message = config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => $board_data
        ]);
    }

    /**
     * New Updated Boards
     *
     * @param Request $request
     * @return void
     */
    public function discover_new_updated_boards (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $all_boards = Board::where('private', 'N')->where('user_id', '!=', $user->id)->has('episodes')
            ->with('episodes')->take(10)->orderBy('updated_at', 'desc')
            ->get(['id', 'user_id','title', 'image', 'private', 'updated_at', 'followers_count', 'shared', 'contributors']);
        
        $user_follow_boards = $user->following_boards()->pluck('id')->all();
        
        $data = [];
        foreach($all_boards as $item) {
            $follower = @in_array($item->id, $user_follow_boards) ? 'true' : 'false';
            $contributor_array = [];
            if(! is_null($item->contributors)) {
                $con_array = json_decode($item->contributors,1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);                
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();                
                foreach($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }                
            }
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'shared' => $item->shared,
                'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors,1)),
                'contributors' => is_null($item->contributors) ? "" : $contributor_array,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,                
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
                'no_of_episodes' => Helper::shorten_count($item->episodes()->count()),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                "last_updated" => $item->updated_at->diffForHumans(),
                "follow_status" => $follower,
            ];            
        } 

        return response()->api([
            'status' => true,
            'message' => "",
            'data' => $data
        ]);        
    }

    /**
     * New Updated Boards
     *
     * @param Request $request
     * @return void
     */
    public function discover_new_updated_boards_view_more (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $all_boards = Board::where('private', 'N')->where('user_id', '!=', $user->id)->has('episodes')
            ->with('episodes')->orderBy('updated_at', 'desc')
            ->paginate(config('config.pagination.discover_new_updated_boards_more'), ['id', 'user_id','title', 'image', 'private', 'updated_at', 'followers_count', 'shared', 'contributors']);
        
        $user_follow_boards = $user->following_boards()->pluck('id')->all();
        
        $data = [];
        foreach($all_boards as $item) {
            $follower = @in_array($item->id, $user_follow_boards) ? 'true' : 'false';
            $contributor_array = [];
            if(! is_null($item->contributors)) {
                $con_array = json_decode($item->contributors,1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);                
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();                
                foreach($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }                
            }
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'shared' => $item->shared,
                'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors,1)),
                'contributors' => is_null($item->contributors) ? "" : $contributor_array,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,                
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
                'no_of_episodes' => Helper::shorten_count($item->episodes()->count()),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                "last_updated" => $item->updated_at->diffForHumans(),
                "follow_status" => $follower,
            ];            
        } 

        return response()->api([
            'status' => true,
            'message' => "",
            'data' => [
                'total' => $all_boards->total(),
                'per_page' => $all_boards->perPage(),
                'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                'items' => $data
            ]
        ]);        
    } 

    /**
     * Popular Boards
     *
     * @param Request $request
     * @return void
     */
    public function discover_popular_boards (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $all_boards = Board::where('private', 'N')->where('user_id', '!=', $user->id)->has('episodes')
            ->with('episodes')->take(10)->orderBy('followers_count', 'desc')
            ->orderBy('updated_at', 'desc')->get(['id', 'user_id','title', 'image', 'private', 'updated_at', 'followers_count', 'shared', 'contributors']);
        
        $user_follow_boards = $user->following_boards()->pluck('id')->all();
        
        $data = [];
        foreach($all_boards as $item) {
            $follower = @in_array($item->id, $user_follow_boards) ? 'true' : 'false';
            $contributor_array = [];
            if(! is_null($item->contributors)) {
                $con_array = json_decode($item->contributors,1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);                
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();                
                foreach($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }                
            }
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'shared' => $item->shared,
                'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors,1)),
                'contributors' => is_null($item->contributors) ? "" : $contributor_array,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,                
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
                'no_of_episodes' => Helper::shorten_count($item->episodes()->count()),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                "last_updated" => $item->updated_at->diffForHumans(),
                "follow_status" => $follower,
            ];
            
        } 

        return response()->api([
            'status' => true,
            'message' => "",
            'data' => $data
        ]);        
    }

    /**
     * Popular Boards view more
     *
     * @param Request $request
     * @return void
     */
    public function discover_popular_boards_view_more (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $all_boards = Board::where('private', 'N')->where('user_id', '!=', $user->id)->has('episodes')
            ->with('episodes')->orderBy('followers_count', 'desc')->orderBy('updated_at', 'desc')
            ->paginate(10, ['id', 'user_id','title', 'image', 'private', 'updated_at', 'followers_count', 'shared', 'contributors']);
        
        $user_follow_boards = $user->following_boards()->pluck('id')->all();
        
        $data = [];
        foreach($all_boards as $item) {
            $follower = @in_array($item->id, $user_follow_boards) ? 'true' : 'false';
            $contributor_array = [];
            if(! is_null($item->contributors)) {
                $con_array = json_decode($item->contributors,1);
                $con_array = array_reverse($con_array);
                $con_array = array_slice($con_array, 0, 5);                
                $contributor_lists = User::whereIn('id', $con_array)->pluck('first_name', 'username')->all();                
                foreach($contributor_lists as $key => $contributor_list) {
                    $contributor_array[] = [
                        'name' => $contributor_list,
                        'username' => $key
                    ];
                }                
            }
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'shared' => $item->shared,
                'total_contributors' => is_null($item->contributors) ? 0 : count(json_decode($item->contributors,1)),
                'contributors' => is_null($item->contributors) ? "" : $contributor_array,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,                
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
                'no_of_episodes' => Helper::shorten_count($item->episodes()->count()),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                "last_updated" => $item->updated_at->diffForHumans(),
                "follow_status" => $follower,
            ];            
        } 

        return response()->api([
            'status' => true,
            'message' => "",
            'data' => [
                'total' => $all_boards->total(),
                'per_page' => $all_boards->perPage(),
                'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                'items' => $data
            ]
        ]);        
    } 

    /**
     * This webservice use for featured collection for
     * discover section
     *
     * @param Request $request
     * @return void
     */    
    public function discover_featured_collection(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Get featured collection from database
        if (config('config.cache_status.featured_collection')) {
            if (Cache::get('featured.collection')) {
                $collections = Cache::get('featured.collection');                
            } else {
                $collections = Chart::published()->featured()->orderBy('updated_at', 'DESC')
                    ->take(5)->get(['id', 'title', 'description', 'image']);
                
                //This code is use for cache that query data
                $expiresAt = Carbon::now()->endOfDay()->addSecond();
                Cache::put('featured.collection', $collections, $expiresAt);                
            }
        } else {
            $collections = Chart::published()->featured()->orderBy('updated_at', 'DESC')
                ->take(5)->get(['id', 'title', 'description', 'image']);            
        }

        $data = [];
        foreach ($collections as $item) {            
            $data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', $item->title)),
                'description' => strip_tags($item->description),
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/show.png')        
             ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }
    
    /**
     * This webservice use for new dashboard collection for
     * discover section
     *
     * @param Request $request
     * @return void
     */
    public function discover_dashboard_new_collection(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //Get new created collection from database
        $collections = Chart::where('featured', 0)->with('chart_poadcast')->published()
            ->orderBy('created_at', 'DESC')->take(10)->get(['id', 'title', 'image']);

        $data = [];
        foreach ($collections as $item) {
            $showData = [];
            $podcastData = $item->chart_poadcast()->take(10)->published()->orderBy('chart_show.order')->get(['id', 'title', 'image']);
            foreach ($podcastData as $showItem) {
                $showData[] = [
                    'id' => $showItem->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($showItem->title))),
                    'image' => !empty($showItem->image) ? $showItem->getImage(200) : asset('uploads/default/show.png')
                ];
            }

            $data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'total_count' => $item->chart_poadcast()->count(),
                'items' => $showData
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * People with common interests
     *
     * @param Request $request
     * @return void
     */
    public function people_with_common_interests (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $interest_category = $user->categories()->pluck('id')->all();

        $query = User::withCount('follower')->approved()->verified()->where('username', '!=', '')
            ->whereNotNull('username')->where('id', '!=', $user->id);
        
        $query->whereHas('categories', function ($q) use ($interest_category) {
            $q->whereIn('id', $interest_category);
        });
        
        // $query->whereDoesntHave('follower', function ($q) use ($user) {
        //     $q->where('id', $user->id);
        // });
                
        $recommended_users =  $query->take(5)
            ->orderBy('follower_count', 'desc')->get(['id', 'username', 'image']);
        
        //dd($user->id,$recommended_users->toArray());
        
        $data = [];
        $follow_status = 'false';
        $user_follower = $user->following()->pluck('id')->all();
        foreach ($recommended_users as $values) {
            $also_intrested_categories = $values->categories()->whereNotIn('id', $interest_category)->pluck('title')->all();
            $follow_status = @in_array($values->id, $user_follower) ? 'true' : 'false';
            $data[] = [
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower_count,
                'follow_status' => $follow_status,
                'also_intrested' => @implode(", ", $also_intrested_categories)
            ];
        }

        $message = "";
        if(count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => $data
        ]);        
    }

    /**
     * Most Follow User
     *
     * @param Request $request
     * @return void
     */
    public function discover_popular_users (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $query = User::withCount('follower')->approved()->verified()
            ->where('username', '!=', '')->whereNotNull('username')->where('id', '!=', $user->id);            
        
        // $query->whereDoesntHave('follower', function ($q) use ($user) {
        //     $q->where('id', $user->id);
        // });
        
        $recommended_users = $query->take(4)->orderBy('follower_count', 'desc')->get(['id', 'username', 'image', 'country_id']);
            
        $data = [];
        $follow_status = 'false';
        $user_follower = $user->following()->pluck('id')->all();
        foreach ($recommended_users as $values) {
            $follow_status = @in_array($values->id, $user_follower) ? 'true' : 'false';
            $data[] = [
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'country' => @$values->country_name->title,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower_count,
                'follow_status' => $follow_status,
            ];
        }

        $message = "";
        if(count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => $data
        ]);        
    }

    /**
     * Most Follow User
     *
     * @param Request $request
     * @return void
     */
    public function discover_popular_users_view_more (Request $request)
    {        
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $query = User::withCount('follower')->approved()->verified()->where('username', '!=', '')
            ->whereNotNull('username')->where('id', '!=', $user->id);
                    
        // $query->whereDoesntHave('follower', function ($q) use ($user) {
        //     $q->where('id', $user->id);
        // });

        $recommended_users = $query->orderBy('follower_count', 'desc')
            ->paginate(config('config.pagination.discover_popular_users'), ['id', 'username', 'image', 'country_id']);
        
        $follow_status = 'false';
        $user_follower = $user->following()->pluck('id')->all();
        $data = [];
        foreach ($recommended_users as $values) {
            $follow_status = @in_array($values->id, $user_follower) ? 'true' : 'false';
            $data[] = [
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'country' => @$values->country_name->title,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower_count,
                'follow_status' => $follow_status,
            ];
        }

        $message = "";
        if(count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $recommended_users->total(),
                'per_page' => $recommended_users->perPage(),
                'pages' => ceil($recommended_users->total() / $recommended_users->perPage()),
                'items' => $data
            ]
        ]);        
    }

    /**
     * Recently Added User
     *
     * @param Request $request
     * @return void
     */
    public function discover_new_recent_users (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $query = User::withCount('follower')->approved()->verified()
            ->where('username', '!=', '')->whereNotNull('username')->where('id', '!=', $user->id);            
        
        // $query->whereDoesntHave('follower', function ($q) use ($user) {
        //     $q->where('id', $user->id);
        // });

        $recent_users = $query->take(8)->orderBy('created_at', 'desc')->get(['id', 'username', 'image']);
        
        $user_follower = $user->following()->pluck('id')->all();
        
        $data = [];
        $follow_status = 'false';
        foreach ($recent_users as $values) {
            $follow_status = @in_array($values->id, $user_follower) ? 'true' : 'false';
            $data[] = [
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower_count,
                'created_date' => $values->created_at->diffForHumans(),
                'follow_status' => $follow_status,
            ];
        }

        $message = "";
        if(count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => $data
        ]);        
    }

    /**
     * Recently Added User - View More
     *
     * @param Request $request
     * @return void
     */
    public function discover_new_recent_users_view_more (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $query = User::withCount('follower')->approved()->verified()
            ->where('username', '!=', '')->whereNotNull('username')
            ->where('id', '!=', $user->id);
        
        // $query->whereDoesntHave('follower', function ($q) use ($user) {
        //     $q->where('id', $user->id);
        // });

        $recent_users = $query->orderBy('created_at', 'desc')
            ->paginate(config('config.pagination.discover_new_users_more'), ['id', 'username', 'image']);

        $user_follower = $user->following()->pluck('id')->all();

        $data = [];
        $follow_status = 'false';
        foreach ($recent_users as $values) {
            $follow_status = @in_array($values->id, $user_follower) ? 'true' : 'false';
            $data[] = [
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower_count,
                'created_date' => $values->created_at->diffForHumans(),
                'follow_status' => $follow_status,
            ];
        }

        $message = "";
        if(count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $recent_users->total(),
                'per_page' => $recent_users->perPage(),
                'pages' => ceil($recent_users->total() / $recent_users->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * This webservice use for network for
     * discover section
     *
     * @param Request $request
     * @return void
     */
    public function discover_network(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        /*
        $networks = Network::published()
            ->orderBy('updated_at', 'DESC')->paginate(12, ['id', 'title', 'image']);

        $data = [];
        foreach ($networks as $item) {
            $data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/show.png')
            ];
        }

        return response()->api([
            'status' => true,
            'message' => "",
            'data' => [
                'total' => $networks->total(),
                'per_page' => $networks->perPage(),
                'pages' => ceil($networks->total() / $networks->perPage()),
                'items' => $data
            ]
        ]);   */
        
        $discover = new Discover('api', $this->user);

        return $discover->networks();   
    }

    /**
     * This function is use for networks details
     *
     * @param Request $request
     * @return void
     */
    /* public function networks(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'network_id' => 'required'
        ], [
            'network_id.required' => config('config.missing_parameter1').' network id'. config('config.missing_parameter2').' Network Details.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $networkData = Network::find($request->network_id);

        if (!$networkData) {
            return response()->api([
                'status' => false,
                'message' => 'No Network Found.'
                ]);
        }

        $query = $networkData->shows()->published();

        $total_query = clone $query;

        $pageNo = $request->page ? ($request->page - 1) : 0;
        $skip = (config('config.pagination.historical_recommended_show') * $pageNo) ;
        
        if($skip) {
            $query->skip($skip);
        }
        $network_shows = $query->take(config('config.pagination.historical_recommended_show'))
            ->orderBy('updated_at', 'DESC')->get(['id', 'title', 'image']);

        $subscribers = $user->subscribers()->pluck('show_id')->all();

        $data = [];
        foreach ($network_shows as $show) {
            $subscribe = @in_array($show->id, $subscribers) ? 'true' : 'false';            
            $data[] = [
                'id' => $show->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),                
                'url_slug' => str_slug(trim($show->title)),
                'no_of_episodes' => $show->episodes()->published()->count(),
                'subscribe' => $subscribe
            ];
        }

        $message = '';

        if (count($data) == 0) {
            $message = 'No shows found.';
        }
        
        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $total_query->count(),
                'per_page' => config('config.pagination.historical_recommended_show'),
                'pages' => ceil($total_query->count() / config('config.pagination.historical_recommended_show')),
                'items' => $data
            ]
        ]);        
    }
 */
    /**
     * This function is use for get Discover dashboard featured smart playlist data
     * @param Request $request
     * @return response
     */    
    public function discover_dashboard_featured_smart_playlist_data(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        if (config('config.cache_status.featured_smartplaylist')) {
            if (Cache::get('featured.smartplaylist')) {
                $boards = Cache::get('featured.smartplaylist');                
            } else {
                $query = SmartPlaylist::where('private', 'N')->featured()->has('shows');
                $boards = $query->take(5)->orderBy('updated_at', 'desc')
                    ->get(['id', 'user_id', 'title', 'description', 'image', 'updated_at', 'private']);
                
                //This code is use for cache that query data
                $expiresAt = Carbon::now()->endOfDay()->addSecond();
                Cache::put('featured.smartplaylist', $boards, $expiresAt);                
            }
        } else {
            $query = SmartPlaylist::where('private', 'N')->featured()->has('shows');
            $boards = $query->take(5)->orderBy('updated_at', 'desc')
                ->get(['id', 'user_id', 'title', 'description', 'image', 'updated_at', 'private']);            
        }

        //$follow_status = 0;
        $user_boards = $user->following_smart_playlist()->pluck('id')->all();

        $board_data = [];
        foreach ($boards as $item) {
            $follow_status = 'false';
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 'true';
            }
            $board_data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'description' => strip_tags($item->description),
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'follow_status' => $follow_status,
                'board_owner' => $user->id == $item->user_id ? 1 : 0                
            ];
        }

        $message = "";
        if(count($board_data) == 0){
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => $board_data
        ]);
    }

    /**
     * New Updated Smart Playlists
     *
     * @param Request $request
     * @return void
     */
    public function discover_new_updated_smart_playlists (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }        
        
        $all_boards = SmartPlaylist::where('private', 'N')->where('user_id', '!=', $user->id)->has('shows')
            ->with('shows')->take(10)->orderBy('updated_at', 'desc')
            ->get(['id', 'user_id','title', 'image', 'private', 'updated_at', 'followers_count']);
        
        $user_follow_boards = $user->following_smart_playlist()->pluck('id')->all();
        
        $data = [];
        foreach($all_boards as $item) {
            $follower = @in_array($item->id, $user_follow_boards) ? 'true' : 'false';
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,                
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
                'no_of_episodes' => Helper::shorten_count($item->shows()->count()),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                "last_updated" => $item->updated_at->diffForHumans(),
                "follow_status" => $follower,
            ];            
        }
        
        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => $data
        ]);        
    }

    /**
     * New Updated Smart Playlist
     *
     * @param Request $request
     * @return void
     */
    public function discover_new_updated_smart_playlists_view_more (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $all_boards = SmartPlaylist::where('private', 'N')->where('user_id', '!=', $user->id)->has('shows')
            ->with('shows')->orderBy('updated_at', 'desc')
            ->paginate(10, ['id', 'user_id','title', 'image', 'private', 'updated_at', 'followers_count']);
        
        $user_follow_boards = $user->following_smart_playlist()->pluck('id')->all();
        
        $data = [];
        foreach($all_boards as $item) {
            $follower = @in_array($item->id, $user_follow_boards) ? 'true' : 'false';
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,                
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
                'no_of_episodes' => Helper::shorten_count($item->shows()->count()),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                "last_updated" => $item->updated_at->diffForHumans(),
                "follow_status" => $follower,
            ];            
        }
        
        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_boards->total(),
                'per_page' => $all_boards->perPage(),
                'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                'items' => $data
            ]
        ]);        
    }

    /**
     * Popular Smart Playlist
     *
     * @param Request $request
     * @return void
     */
    public function discover_popular_smart_playlists (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $all_boards = SmartPlaylist::where('private', 'N')->where('user_id', '!=', $user->id)->has('shows')
            ->with('shows')->take(10)->orderBy('followers_count', 'desc')
            ->orderBy('updated_at', 'desc')->get(['id', 'user_id','title', 'image', 'private', 'updated_at', 'followers_count']);
        
        $user_follow_boards = $user->following_smart_playlist()->pluck('id')->all();
        
        $data = [];
        foreach($all_boards as $item) {
            $follower = @in_array($item->id, $user_follow_boards) ? 'true' : 'false';
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,                
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
                'no_of_episodes' => Helper::shorten_count($item->shows()->count()),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                "last_updated" => $item->updated_at->diffForHumans(),
                "follow_status" => $follower,
            ];            
        } 

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => $data
        ]);        
    }

    /**
     * Popular Smart Playlist view more
     *
     * @param Request $request
     * @return void
     */
    public function discover_popular_smart_playlists_view_more (Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $all_boards = SmartPlaylist::where('private', 'N')->where('user_id', '!=', $user->id)->has('shows')
            ->with('shows')->orderBy('followers_count', 'desc')->orderBy('updated_at', 'desc')
            ->paginate(10, ['id', 'user_id','title', 'image', 'private', 'updated_at', 'followers_count']);
        
        $user_follow_boards = $user->following_smart_playlist()->pluck('id')->all();
        
        $data = [];
        foreach($all_boards as $item) {
            $follower = @in_array($item->id, $user_follow_boards) ? 'true' : 'false';
            $data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,                
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
                'no_of_episodes' => Helper::shorten_count($item->shows()->count()),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                "last_updated" => $item->updated_at->diffForHumans(),
                "follow_status" => $follower,
            ];            
        }
        
        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_boards->total(),
                'per_page' => $all_boards->perPage(),
                'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                'items' => $data
            ]
        ]);        
    }

    //////////////////////old Webservices////////////////
    /**
     * This webservice use for featured network for
     * discover section
     *
     * @param Request $request
     * @return void
     */
    public function discover_featured_network(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $collections = Network::published()->featured()->take(6)
            ->orderBy('updated_at', 'DESC')->get(['id', 'title', 'image']);

        $data = [];
        foreach ($collections as $item) {
            $data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', $item->title)),
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/show.png')
            ];
        }

        return response()->api([
            'status' => true,
            'data' => [
                'items' => $data
            ]
        ]);
    }
    /**
     * This function is use for get Discover dashboard data
     * @param Request $request
     * @return response
     */
    public function discover_dashboard_data(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = Show::published()->select(['id', 'title', 'image', 'updated_at']);

        $query->whereDoesntHave('subscribers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        $category_shows = $query->take(config('config.pagination.discover_show'))
            ->orderBy('no_of_subscribers', 'desc')->orderBy('updated_at', 'DESC')
            ->get(['id', 'title', 'image', 'updated_at']);

        $subscribe = false;
        $shows_data = [];
        foreach ($category_shows as $item) {
            $shows_data[] = [
                'id' => $item->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
                'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
                'subscribe' => $subscribe,            
                'categories_count' => $item->categories_count
             ];
        }        

        $data = [
             'shows' => $shows_data,
             'boards' => [],
             'categories' => []
         ];

        return response()->api([
             'status' => true,
             'data' => $data
         ]);
    }

    /**
     * This function is use for get all shows
     * @param Request $request
     * @return response
    */
    public function show_viewmore(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = Show::published()->select(['id', 'title', 'image', 'updated_at', 'no_of_subscribers']);

        $query->whereDoesntHave('subscribers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        if ($request->no_paginate == 'Yes') {
            $category_shows = $query->get(['id', 'title', 'image', 'no_of_subscribers']);
        } else {
            $category_shows = $query->orderBy('updated_at', 'DESC')
            ->orderBy('no_of_subscribers', 'desc')->paginate(config('config.pagination.discover_show_view_more'), ['id', 'title', 'image', 'updated_at', 'no_of_subscribers']);
        }
        //exit;

        $subscribe = [];
        $subscribers = $user->subscribers()->pluck('show_id')->all();

        $data = [];
        foreach ($category_shows as $item) {
            $subscribe = @in_array($item->id, $subscribers) ? 'true' : 'false';
            $data[] = [
               'id' => $item->id,
               'title' => trim(str_replace("\n", '', html_entity_decode($item->title))),
               'image' => !empty($item->image) ? $item->getWSImage(200) : asset('uploads/default/show.png'),
               'subscribe' => $subscribe,
               'no_of_episodes' => 0,//$item->episodes()->published()->count(),
               'no_of_subscribers' => $item->no_of_subscribers               
            ];
        }

        $message = '';

        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'data' => [
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => $category_shows->total(),
                    'per_page' => $category_shows->perPage(),
                    'pages' => ceil($category_shows->total() / $category_shows->perPage()),
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function discover_dashboard_data_for_boards(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = Board::where('private', 'N')->withCount(['episodes', 'followers'])->has('episodes');

        $query->whereDoesntHave('followers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        $boards = $query->take(config('config.pagination.discover_board'))
             ->orderByDesc('updated_at')
             ->get(['id', 'user_id', 'title', 'image', 'episodes_count', 'updated_at', 'private']);

        //$follow_status = 0;
        $user_boards = $user->following_boards()->pluck('id')->all();

        $board_data = [];
        foreach ($boards as $item) {
            $follow_status = 0;
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 1;
            }
            $board_data[] = [
                'id' => $item->id,
                'board_name' => $item->title,
                'private' => $item->private,
                'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                'follow_status' => $follow_status,
                'board_owner' => $user->id == $item->user_id ? 1 : 0,
                'no_of_episodes' => Helper::shorten_count($item->episodes_count),
                'no_of_follower' => Helper::shorten_count($item->followers_count),
                'last_updated' => $item->updated_at->diffForHumans(),
                'name' => $item->user->notification_format_name,
                'username' => $item->user->username,
             ];
        }

        $data = [
             'boards' => $board_data
         ];

        return response()->api([
             'status' => true,
             'data' => $data
         ]);
    }
        
    /**
     * This function is use for view more boards
     * @param Request $request
     * @return type
     */
    public function boards_viewmore(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = Board::where('private', 'N')->withCount(['episodes', 'followers'])->has('episodes');

        $query->whereDoesntHave('followers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        $boards = $query->orderBy('updated_at', 'desc')
            ->paginate(config('config.pagination.discover_board_view_more'), ['id', 'user_id', 'title', 'image', 'private', 'updated_at']);

        //$follow_status = 0;
        $user_boards = $user->following_boards()->pluck('id')->all();

        $data = [];
        foreach ($boards as $item) {
            $follow_status = 0;
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 1;
            }
            $data[] = [
               'id' => $item->id,
               'board_name' => $item->title,
               'private' => $item->private,
               'board_owner' => $user->id == $item->user_id ? 1 : 0,
               'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
               'no_of_episodes' => Helper::shorten_count($item->episodes_count),
               'no_of_follower' => Helper::shorten_count($item->followers_count),
               'name' => $item->user->notification_format_name,
               'username' => $item->user->username,
               'follow_status' => $follow_status,
               'last_updated' => $item->updated_at->diffForHumans(),
            ];
        }

        $message = '';

        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'data' => [
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => $boards->total(),
                    'per_page' => $boards->perPage(),
                    'pages' => ceil($boards->total() / $boards->perPage()),
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        }
    }    

    /**
     * This webservice use for view more trending boards for
     * discover section
     *
     * @param Request $request
     * @return void
     */
    public function discover_trending_boards(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $feed_config = FeedConfiguration::first();

        try {
            $all_boards = DB::select(DB::raw("SELECT `board_id`, count(*) as `total` FROM `tbl_board_user` TBU, tbl_boards B WHERE (TBU.created_at BETWEEN '".Carbon::now()->subDay($feed_config->trending_boards)."' AND '".Carbon::now()."') AND (TBU.board_id = B.id) AND (B.private = 1) group by board_id order by total desc limit " . config('config.pagination.discover_trending_boards')));
            
            $user_follow_boards = $user->following_boards()->pluck('id')->all();

            $data = [];
            foreach($all_boards as $item) {
                $follower = @in_array($item->board_id, $user_follow_boards) ? 1 : 0;
                $dataInfo = Board::where('id', $item->board_id)->with('episodes', 'followers')
                    ->first(['id', 'title', 'user_id', 'image', 'private', 'updated_at']);
                if($dataInfo) {
                    $data[] = [
                        'id' => $dataInfo->id,
                        'board_name' => $dataInfo->title,
                        'private' => $dataInfo->private,
                        'board_owner' => $user->id == $dataInfo->user_id ? 1 : 0,
                        'no_of_episodes' => Helper::shorten_count($dataInfo->episodes()->count()),
                        'no_of_follower' => Helper::shorten_count($dataInfo->followers()->count()),
                        "last_updated" => $dataInfo->updated_at->diffForHumans(),
                        'image' => !empty($dataInfo->image) ? $dataInfo->getImage(200) : asset('uploads/default/board.png'),
                        'name' => $dataInfo->user->notification_format_name,
                        'username' => $dataInfo->user->username,
                        "follow_status" => $follower,
                    ];
                }
            }        
    
            return response()->api([
                'status' => true,
                'data' => [
                    'boards' => $data
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'boards' => []
                ]
            ]);
        }
    }

    /**
     * This webservice use for view more trending boards for
     * discover section
     *
     * @param Request $request
     * @return void
     */
    public function discover_trending_boards_view_more(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $feed_config = FeedConfiguration::first();

        $max = config('config.pagination.discover_trending_boards_more');
        $page_no = $request->page ? $request->page : 1; 
        $start = ($page_no) ? ($page_no-1) * $max : 0;

        try {
            $all_boards_count = DB::select(DB::raw("SELECT `board_id`, count(*) as `total` FROM `tbl_board_user` TBU, tbl_boards B WHERE (TBU.created_at BETWEEN '".Carbon::now()->subDay($feed_config->trending_boards)."' AND '".Carbon::now()."') AND (TBU.board_id = B.id) AND (B.private = 'N') group by board_id order by total desc"));

            $all_boards = DB::select(DB::raw("SELECT `board_id`, count(*) as `total` FROM `tbl_board_user` TBU, tbl_boards B WHERE (TBU.created_at BETWEEN '".Carbon::now()->subDay($feed_config->trending_boards)."' AND '".Carbon::now()."') AND (TBU.board_id = B.id) AND (B.private = 'N') group by board_id order by total desc limit ?, ?"), array($start, $max));

            //$all_boards = DB::select(DB::raw("SELECT `board_id`, count(*) as `total` FROM `tbl_board_user` WHERE `created_at` BETWEEN '".Carbon::now()->subDay($feed_config->trending_boards)."' AND '".Carbon::now()."' group by board_id order by total desc limit $start, ".$max));
            
            $user_follow_boards = $user->following_boards()->pluck('id')->all();

            $data = [];
            foreach($all_boards as $item) {
                $follower = @in_array($item->board_id, $user_follow_boards) ? 1 : 0;
                $dataInfo = Board::where('id', $item->board_id)
                    ->with('episodes', 'followers')
                    ->first(['id', 'title', 'user_id', 'image', 'private', 'updated_at']);
                if($dataInfo) {
                    $data[] = [
                        'id' => $dataInfo->id,
                        'board_name' => $dataInfo->title,
                        'private' => $dataInfo->private,
                        'board_owner' => $user->id == $dataInfo->user_id ? 1 : 0,
                        'no_of_episodes' => Helper::shorten_count($dataInfo->episodes()->count()),
                        'no_of_follower' => Helper::shorten_count($dataInfo->followers()->count()),
                        "last_updated" => $dataInfo->updated_at->diffForHumans(),
                        'image' => !empty($dataInfo->image) ? $dataInfo->getImage(200) : asset('uploads/default/board.png'),
                        'name' => $dataInfo->user->notification_format_name,
                        'username' => $dataInfo->user->username,
                        "follow_status" => $follower,
                    ];
                }
            }        
    
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => count($all_boards_count),
                    'per_page' => $max,
                    'pages' => ceil(count($all_boards_count) / $max),
                    'boards' => $data,
                    'message' => ""
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'boards' => []
                ]
            ]);
        }
    }

    /**
     * This webservice use for trending episode for
     * discover section
     *
     * @param Request $request
     * @return void
     */
    public function discover_trending_episodes(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $feed_config = FeedConfiguration::first();
        
        try {
            $all_episodes = DB::select(DB::raw("SELECT `episode_id`, count(*) as `total` FROM `tbl_user_stream_data` WHERE `created_at` BETWEEN '".Carbon::now()->subDay($feed_config->trending_episodes)."' AND '".Carbon::now()."' group by episode_id order by total desc limit " . config('config.pagination.discover_trending_episodes')));            
            $data = [];
            foreach($all_episodes as $episode) {
                $dataInfo = Episode::where('id', $episode->episode_id)->first(['id', 'title', 'show_id', 'mp3', 'duration', 'listen_count', 'explicit']);
                if($dataInfo) {
                    $data[] = [
                        'id' => $dataInfo->id,
                        'show_id' => $dataInfo->show_id,
                        'episode_title' => trim(str_replace("\n", '', html_entity_decode($dataInfo->title))),
                        'audio_file' => $dataInfo->getAudioLink(),
                        'show_title' => trim(str_replace("\n", '', html_entity_decode($dataInfo->show->title))),
                        'episode_image' => !empty($dataInfo->show->image) ? $dataInfo->show->getWSImage(200) : asset('uploads/default/show.png'),
                        'duration' => $dataInfo->getDurationText(),
                        'no_of_listen' => Helper::shorten_count($dataInfo->listen_count), 
                        'explicit' => $dataInfo->show->explicit                
                    ];
                }
            }        
    
            return response()->api([
                'status' => true,
                'data' => [
                    'episodes' => $data
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'episodes' => []
                ]
            ]);
        }
    }

    /**
     * This webservice use for view more trending episode for
     * discover section
     *
     * @param Request $request
     * @return void
     */
    public function discover_trending_episodes_view_more(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $feed_config = FeedConfiguration::first();

        $max = config('config.pagination.discover_trending_episodes_more');
        $page_no = $request->page ? $request->page : 1; 
        $start = ($page_no) ? ($page_no-1) * $max : 0;
        
        try {
            $all_episode_count = DB::select(DB::raw("SELECT `episode_id`, count(*) as `total` FROM `tbl_user_stream_data` WHERE `created_at` BETWEEN '".Carbon::now()->subDay($feed_config->trending_episodes)."' AND '".Carbon::now()."' group by episode_id order by total desc"));

            $all_episodes = DB::select(DB::raw("SELECT `episode_id`, count(*) as `total` FROM `tbl_user_stream_data` WHERE `created_at` BETWEEN '".Carbon::now()->subDay($feed_config->trending_episodes)."' AND '".Carbon::now()."' group by episode_id order by total desc limit ?, ?"), array($start, $max));

            //$all_episodes = DB::select(DB::raw("SELECT `episode_id`, count(*) as `total` FROM `tbl_user_stream_data` WHERE `created_at` BETWEEN '".Carbon::now()->subDay($feed_config->trending_episodes)."' AND '".Carbon::now()."' group by episode_id order by total desc limit $start, ".$max));
            
            $data = [];            
            foreach($all_episodes as $episode) {
                $dataInfo = Episode::where('id', $episode->episode_id)->first(['id', 'title', 'show_id', 'mp3', 'duration', 'listen_count', 'explicit']);
                if($dataInfo) {
                    $data[] = [
                        'id' => $dataInfo->id,
                        'show_id' => $dataInfo->show_id,
                        'episode_title' => trim(str_replace("\n", '', html_entity_decode($dataInfo->title))),
                        'audio_file' => $dataInfo->getAudioLink(),
                        'show_title' => trim(str_replace("\n", '', html_entity_decode($dataInfo->show->title))),
                        'episode_image' => !empty($dataInfo->show->image) ? $dataInfo->show->getWSImage(200) : asset('uploads/default/show.png'),
                        'duration' => $dataInfo->getDurationText(),
                        'no_of_listen' => Helper::shorten_count($dataInfo->listen_count),
                        'explicit' => $dataInfo->show->explicit                                          
                    ];
                }
            }        
    
            return response()->api([
                'status' => true,
                'data' => [
                    'total' => count($all_episode_count),
                    'per_page' => $max,
                    'pages' => ceil(count($all_episode_count) / $max),
                    'episodes' => $data,
                    'message' => ""
                ]
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => true,
                'message' => '',
                'data' => [
                    'episodes' => []
                ]
            ]);
        }
    }  
    
    #############Discover Section##############
    /**
     * Discover Masthead
     */
    public function discover_mathead(Request $request)
    {
        $discover = new Discover('api', $this->user);

        return $discover->discover_masthead();
    }
    
    /**
     * Discover pod of the day
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function pod_of_the_day(Request $request)
    {
        $discover = new Discover('api', $this->user);

        return $discover->pod_of_the_day();
    }
     /**
     * Discover popular shows
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function popular_shows(Request $request)
    {
        $discover = new Discover('api', $this->user);

        return $discover->popular_shows();
    }

    /**
     * Discover network list
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function networks(Request $request)
    {
        $discover = new Discover('api', $this->user);

        return $discover->networks();
    }

    /**
     * Discover network shows
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function network_shows(Request $request)
    {
        $discover = new Discover('api', $this->user);

        return $discover->network_shows();
    }

    /**
     * Discover categories
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function categories(Request $request)
    {
        $discover = new Discover('api', $this->user);

        return $discover->categories();
    }

    /**
     * Discover categories shows
     *  @param Request $request
     * @return type
     * @throws conditon
     **/
    public function categories_shows(Request $request)
    {
        $discover = new Discover('api', $this->user);

        return $discover->categories_shows();
    } 
}
