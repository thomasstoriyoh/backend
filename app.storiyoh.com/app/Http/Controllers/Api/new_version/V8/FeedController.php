<?php

namespace App\Http\Controllers\Api\new_version\V8;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Episode;
use App\Models\Board;
use App\Models\Show;
use App\Models\FeedConfiguration;
use App\Models\User;
use App\Traits\Helper;
use App\Models\Feed;
use App\ClassesV3\Feed as FeedRepo;
use App\ClassesV3\Feed as NotificationRepo;
use App\FeedResponseV2\UserFeedResponse;
use App\FeedResponseV2\BoardFeedResponse;
use App\FeedResponseV2\EpisodeHeardFeedResponse;
use App\FeedResponseV2\EpisodeLikeFeedResponse;
use App\FeedResponseV2\ShowNewEpisodeFeedResponse;
use App\FeedResponseV2\ShowFeedResponse;
use App\FeedResponseV2\BoardFollowResponse;
use App\FeedResponseV2\SmartPlaylistFollowResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\UserSocial;
use App\FeedResponseV2\PostResponse;
use App\Models\Masthead;
use App\Models\PodcastDay;
use App\Traits\ResponseFormat;
use App\Models\Factoid;

/** Topic  Subscription Code Start */
use App\Jobs\UserSubscribedFirstTime;
/** Topic  Subscription Code End */

//use App\FeedResponse\BoardNewFeedResponse;
class FeedController extends Controller
{
    use ResponseFormat;

    protected $feedMap = [
        FeedRepo::FOLLOW_USER => UserFeedResponse::class,
        FeedRepo::BOARD_UPDATE => BoardFeedResponse::class,
        FeedRepo::EPISODE_HEARD => EpisodeHeardFeedResponse::class,
        FeedRepo::EPISODE_LIKE => EpisodeLikeFeedResponse::class,
        FeedRepo::SHOW_UPDATE => ShowNewEpisodeFeedResponse::class,
        FeedRepo::SUBSCRIBE_SHOW => ShowFeedResponse::class,
        FeedRepo::FOLLOW_BOARD => BoardFollowResponse::class,
        FeedRepo::FOLLOW_SMARTLIST => SmartPlaylistFollowResponse::class,
        //FeedRepo::NEW_BOARD => BoardNewFeedResponse::class,

        /* New Code Added 01/09/2018 */
        /** New Post Code Start */
        FeedRepo::NEW_SHOW_POST => PostResponse::class,
        FeedRepo::NEW_EPISODE_POST => PostResponse::class,
        FeedRepo::NEW_PLAYLIST_POST => PostResponse::class,
        FeedRepo::NEW_SMARTPLAYLIST_POST => PostResponse::class,
        FeedRepo::NEW_COLLECTION_POST => PostResponse::class,

        /** New Individual Share Post 20/05/2019 */
        FeedRepo::INDIVIDUAL_POST => PostResponse::class,

        /** Repost Code Start */
        // FeedRepo::SHOW_REPOST => RePostResponse::class,
        // FeedRepo::EPISODE_REPOST => RePostResponse::class,
        // FeedRepo::PLAYLIST_REPOST => RePostResponse::class,
        // FeedRepo::SMARTPLAYLIST_REPOST => RePostResponse::class,
        // FeedRepo::COLLECTION_REPOST => RePostResponse::class,

        /** Post Like Code Start */
        // FeedRepo::SHOW_POST_LIKE => NewShowPostResponse::class,
        // FeedRepo::EPISODE_POST_LIKE => NewShowPostResponse::class,
        // FeedRepo::PLAYLIST_POST_LIKE => NewShowPostResponse::class,
        // FeedRepo::SMARTPLAYLIST_POST_LIKE => NewShowPostResponse::class,
        // FeedRepo::COLLECTION_POST_LIKE => NewShowPostResponse::class,

        /** Post Comment Code Start */
        // FeedRepo::SHOW_POST_COMMENT => NewShowPostResponse::class,
        // FeedRepo::EPISODE_POST_COMMENT => NewShowPostResponse::class,
        // FeedRepo::PLAYLIST_POST_COMMENT => NewShowPostResponse::class,
        // FeedRepo::SMARTPLAYLIST_POST_COMMENT => NewShowPostResponse::class,
        // FeedRepo::COLLECTION_POST_COMMENT => NewShowPostResponse::class,
    ];

    protected $notificationMap = [
        NotificationRepo::FOLLOW_USER => FollowUserResponse::class,
        NotificationRepo::FOLLOW_BOARD => FollowBoardResponse::class,
        NotificationRepo::BOARD_UPDATE => BoardFeedResponse::class,
        NotificationRepo::SHOW_UPDATE => ShowNewEpisodeFeedResponse::class,
        NotificationRepo::EPISODE_COMMENT => EpisodeCommentFeedResponse::class
        //NotificationRepo::SUBSCRIBE_SHOW => ShowFeedResponse::class,
        //NotificationRepo::NEW_BOARD => BoardNewFeedResponse::class,
    ];

    /**
    * This function is use for networks
    * @param Request $request
    * @return type
    */
    public function networks(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $latest_shows_array = [];

        $user_queue_count = \DB::table('user_queue')->where('user_id', $user->id)->count();
        $user_notification_count = 1;

        return response()->api([
            'status' => true,
            'message' => count($latest_shows_array) == 0 ? config('language.' . $this->getLocale() . ".Feed.networks_latest_showname_lbl") : '',
            'data' => [
                'latest_shows' => $latest_shows_array,
                'queue_count' => Helper::shorten_count($user_queue_count),
                'notification_count' => $this->notification_count($user) > 99 ? '99+' : trim($this->notification_count($user)),
                'notification_count2' => Helper::shorten_count($this->notification_count($user))
            ]
        ]);
    }

    /**
    * This function is use for notification
    * @param Request $request
    * @return type
    */
    public function notification_count(User $user)
    {
        $boards = $user->boards()->pluck('id')->all();

        $types = [
            'App\\Models\\Board' => ['board_user', 'board_id', 'user_id'],
            'App\\Models\\Show' => ['show_user', 'show_id', 'user_id']
        ];

        //'Board' => 'followers', 'Show' => 'subscribers'
        $query = Feed::query();
        foreach ($types as $type => $info) {
            $query->orWhere(function ($q) use ($type, $info, $user) {
                $q->where('typeable_type', $type)
                ->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                ->whereIn('typeable_id', function ($que) use ($user, $info, $type) {
                    $que->select($info[1])->from($info[0])->where($info[2], $user->id);
                });

                // Created at grater than created at in the pivot table
                $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                    return $builder->select('created_at')->from($info[0])
                        ->whereRaw($info[1] . ' = tbl_user_feeds.typeable_id')
                        ->where($info[2], $user->id);
                });
            });
        }

        $query->orWhere(function ($q) use ($user) {
            $q->where('typeable_type', 'App\\Models\\User')
               ->where('type', NotificationRepo::FOLLOW_USER)
               ->where('data', 'LIKE', '%"' . $user->id . '"%');
        });

        $query->orWhere(function ($q) use ($boards) {
            foreach ($boards as $board) {
                $q->orWhere('data', 'LIKE', '%"' . $board . '"%');
                $q->where('typeable_type', 'App\\Models\\User');
                $q->where('type', NotificationRepo::FOLLOW_BOARD);
            }
        });

        // $query->orWhere(function($q) use ($user) {
        //    $q->where('typeable_type', "App\\Models\\EpisodeUserComment")
        //        ->where('data', 'LIKE', '%"'.$user->id.'"%');
        // });

        $feedIds = $query->pluck('id')->all();

        $notification_read_count = Feed::whereHas('user_notification_status', function ($q) use ($user, $feedIds) {
            $q->where('user_id', $user->id);
            $q->whereIn('feed_id', $feedIds);
        })->count();

        $unread_notification_count = $query->count() - $notification_read_count;

        return $unread_notification_count >= 0 ? $unread_notification_count : '0';
    }

    /**
    * This function is use for networks
    * @param Request $request
    * @return type
    */
    public function network_feed(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => true,
                'data' => [
                    'oauth_enabled' => 0,
                    'total' => 0,
                    'per_page' => 10,
                    'pages' => 1,
                    'items' => [],
                    'type' => 'user',
                    'fixed_user_data' => [],
                    'fixed_show_data' => [],
                    'fixed_board_data' => [],
                    'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                ]
            ];
            return $this->sendAuthErrorResponse($response, 401);
            //return $this->sendAuthErrorResponse($response, 200);            
        }

        $types = [
            'App\\Models\\User' => ['user_follow', 'following_id', 'follower_id'],
            'App\\Models\\Board' => ['board_user', 'board_id', 'user_id']
        ];

        $query = Feed::query();

        foreach ($types as $type => $info) {
            $query->orWhere(function ($q) use ($type, $info, $user) {
                $q->where('typeable_type', $type)
                    ->whereIn('type', [
                        FeedRepo::FOLLOW_USER, 
                        FeedRepo::BOARD_UPDATE, 
                        FeedRepo::EPISODE_HEARD, 
                        FeedRepo::SUBSCRIBE_SHOW, 
                        FeedRepo::FOLLOW_BOARD, 
                        FeedRepo::FOLLOW_SMARTLIST, 
                        FeedRepo::NEW_SHOW_POST, 
                        FeedRepo::NEW_EPISODE_POST, 
                        FeedRepo::NEW_PLAYLIST_POST, 
                        FeedRepo::NEW_SMARTPLAYLIST_POST, 
                        FeedRepo::NEW_COLLECTION_POST,
                        FeedRepo::INDIVIDUAL_POST /** New Individual Share Post 20/05/2019 */
                    ])
                    ->whereIn('typeable_id', function ($que) use ($user, $info, $type) {
                        $que->select($info[1])->from($info[0])->where($info[2], $user->id);
                    });
            });
        }

        $feed_items = $query->orderBy('updated_at', 'DESC')->paginate(config('config.pagination.feed_network'));

        $data = [];
        foreach ($feed_items as $feed_item) {
            if (!empty($this->feedMap[$feed_item->type])) {
                try {
                    if (with(new $this->feedMap[$feed_item->type]($feed_item, $user))->response()) {
                        $data[] = with(new $this->feedMap[$feed_item->type]($feed_item, $user))->response();
                    }
                } catch (\Exception $ex) {
                }
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Feed.network_feed_not_found_lbl");
        }        

        //Update last user app_access_time only when page = 1
        $my_page = !empty($request->page) ? $request->page : 1;
        $firebase_status = $user->firebase;
        if ($my_page == 1) {
            $user->timestamps = false;
            $user->update(['app_last_access_time' => Carbon::now()]);

            //If topic_sync is 0 then we will call sync all topic for specific user
            if($user->topic_sync == 0) {
                dispatch(new UserSubscribedFirstTime($user->id))->onQueue('default');
            }

            //If firebase is 0 then we will update firebase status for specific user
            if (! empty($request->platform)) {
                if ($user->firebase == 0 || $user->firebase == 1) {
                    $user->update(['firebase' => 2]);
                }
            }
        }

        return response()->api([
            'status' => true,
            'data' => [
                'oauth_enabled' => $user->oauth_enabled,
                'total' => $feed_items->total(),
                'per_page' => $feed_items->perPage(),
                'pages' => ceil($feed_items->total() / $feed_items->perPage()),
                'items' => $data,
                'message' => $message,
                'id' => $user->id,
                'firebase' => $firebase_status,
            ]
        ]);
    }

    /**
     * This function is use for get Dashboard trending
     * @param Request $request
     * @return response
     */
    public function dashboard_trending(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interest_category = $user->categories()->pluck('id')->all();

        $feed_config = FeedConfiguration::first();

        $start_feed_data = Carbon::now()->subDays($feed_config->trending)->format('Y-m-d h:i:s');
        $end_feed_data = Carbon::now()->format('Y-m-d h:i:s');

        $query = Episode::select('id', 'show_id', 'title', 'duration', 'image', 'mp3')
            ->whereNotNull('show_id')->whereNotNull('mp3')
            ->whereIn('id', function ($que) use ($start_feed_data, $end_feed_data) {
                $que->select('episode_id')->from('user_stream_data')->whereBetween('updated_at', [$start_feed_data, $end_feed_data]);
            });

        if (count($interest_category) > 0) {
            $query->whereHas('categories', function ($q) use ($interest_category) {
                $q->whereIn('category_id', $interest_category);
            });
        }

        $episode_data = $query->orderBy('user_stream_data_count', 'desc')
            ->take(config('config.pagination.dashboard_trending_episode'))
            ->get(['id', 'title', 'show_id', 'user_stream_data_count', 'mp3', 'duration', 'listen_count', 'explicit']);

        $trending_episode_data = [];
        $show_title = '';
        foreach ($episode_data as $values) {
            $trending_episode_data[] = [
                'id' => $values->id,
                'show_id' => $values->show_id,
                'episode_title' => trim(str_replace("\n", '', html_entity_decode($values->title))),
                'audio_file' => $values->getAudioLink(),
                'show_title' => trim(str_replace("\n", '', html_entity_decode($values->show->title))),
                'episode_image' => !empty($values->show->image) ? $values->show->getWSImage(200) : asset('uploads/default/show.png'),
                'duration' => $values->getDurationText(),
                'no_of_listen' => Helper::shorten_count($values->listen_count),
                'explicit' => $values->show->explicit
            ];
        }

        $query = Board::where('private', 'N')->withCount(['episodes', 'followers' => function ($que) use ($start_feed_data, $end_feed_data) {
            $que->whereBetween('board_user.updated_at', [$start_feed_data, $end_feed_data]);
        }])->has('episodes')->orderBy('followers_count', 'desc');

        $query->whereDoesntHave('followers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        $trending_board = $query->take(config('config.pagination.dashboard_trending_board'))
            ->get(['id', 'title', 'user_id', 'private', 'episodes_count', 'followers_count', 'updated_at', 'image']);

        $user_boards = $user->following_boards()->pluck('id')->all();

        $trending_board_data = [];
        foreach ($trending_board as $item) {
            $follow_status = 0;
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 1;
            }
            $trending_board_data[] = [
               'id' => $item->id,
               'board_name' => $item->title,
               'private' => $item->private,
               'board_owner' => $user->id == $item->user_id ? 1 : 0,
               'no_of_episodes' => Helper::shorten_count($item->episodes_count),
               'no_of_follower' => Helper::shorten_count($item->followers_count),
               'last_updated' => $item->updated_at->diffForHumans(),
               'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
               'name' => $item->user->notification_format_name,
               'username' => $item->user->username,
               'follow_status' => $follow_status,
            ];
        }

        $data = [
            'trending_episode_data' => $trending_episode_data,
            'trending_board_data' => $trending_board_data,
        ];

        return response()->api([
            'status' => true,
            'data' => $data,
        ]);
    }

    /**
     * This function is use for get trending episode
     * @param Request $request
     * @return response
     */
    public function trending_episode(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interest_category = $user->categories()->pluck('id')->all();

        $feed_config = FeedConfiguration::first();

        $start_feed_data = Carbon::now()->subDays($feed_config->trending)->format('Y-m-d h:i:s');
        $end_feed_data = Carbon::now()->format('Y-m-d h:i:s');

        $query = Episode::select('id', 'show_id', 'title', 'duration', 'image', 'mp3')
            ->whereNotNull('show_id')->whereNotNull('mp3')
            ->whereIn('id', function ($que) use ($start_feed_data, $end_feed_data) {
                $que->select('episode_id')->from('user_stream_data')->whereBetween('updated_at', [$start_feed_data, $end_feed_data]);
            });

        if (count($interest_category) > 0) {
            $query->whereHas('categories', function ($q) use ($interest_category) {
                $q->whereIn('category_id', $interest_category);
            });
        }

        if ($request->no_paginate == 'Yes') {
            $episode_data = $query->orderByDesc('user_stream_data_count')
                ->get(['id', 'title', 'show_id', 'user_stream_data_count']);
        } else {
            $episode_data = $query->orderByDesc('user_stream_data_count')
                ->paginate(config('config.pagination.episode'), ['id', 'title', 'show_id', 'user_stream_data_count', 'mp3', 'duration', 'listen_count', 'explicit']);
        }

        $data = [];
        $show_title = '';
        foreach ($episode_data as $values) {
            $data[] = [
                'id' => $values->id,
                'show_id' => $values->show_id,
                'episode_title' => trim(str_replace("\n", '', html_entity_decode($values->title))),
                'audio_file' => $values->getAudioLink(),
                'show_title' => $values->show_id ? trim(str_replace("\n", '', html_entity_decode($values->show->title))) : '',
                'episode_image' => !empty($values->show->image) ? $values->show->getWSImage(200) : asset('uploads/default/show.png'),
                'duration' => $values->getDurationText(),
                'no_of_listen' => Helper::shorten_count($values->listen_count),
                'explicit' => $values->show->explicit
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $episode_data->total(),
                    'per_page' => $episode_data->perPage(),
                    'pages' => ceil($episode_data->total() / $episode_data->perPage()),
                    'items' => $data
                ]
            ]);
        }
    }

    /**
     * This function is use for get trending boards
     * @param Request $request
     * @return response
    */
    public function trending_board(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $feed_config = FeedConfiguration::first();

        $start_feed_data = Carbon::now()->subDays($feed_config->trending)->format('Y-m-d h:i:s');
        $end_feed_data = Carbon::now()->format('Y-m-d h:i:s');

        $query = Board::where('private', 'N')->withCount(['episodes', 'followers' => function ($que) use ($start_feed_data, $end_feed_data) {
            $que->whereBetween('board_user.updated_at', [$start_feed_data, $end_feed_data]);
        }])->has('episodes')->orderBy('followers_count', 'desc');

        $query->whereDoesntHave('followers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        if ($request->no_paginate == 'Yes') {
            $trending_board = $query->get(['id', 'title', 'image', 'private']);
        } else {
            $trending_board = $query->paginate(config('config.pagination.board'), ['id', 'title', 'image', 'user_id', 'private']);
        }

        $data = [];
        $user_boards = $user->boards()->pluck('id')->all();
        foreach ($trending_board as $item) {
            $follow_status = 0;
            if (@in_array($item->id, $user_boards)) {
                $follow_status = 1;
            }
            $data[] = [
               'id' => $item->id,
               'board_name' => $item->title,
               'private' => $item->private,
               'board_owner' => $user->id == $item->user_id ? 1 : 0,
               'no_of_episodes' => Helper::shorten_count($item->episodes_count),
               'no_of_follower' => Helper::shorten_count($item->followers_count),
               'last_updated' => $item->updated_at->diffForHumans(),
               'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
               'name' => $item->user->notification_format_name,
               'username' => $item->user->username,
               'follow_status' => $follow_status,
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $trending_board->total(),
                    'per_page' => $trending_board->perPage(),
                    'pages' => ceil($trending_board->total() / $trending_board->perPage()),
                    'items' => $data
                ]
            ]);
        }
    }

    /**
     * Feed for you
     *
     * @param Request $request
     * @return void
     */
    public function feed_for_you(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interested_category = $user->categories->pluck('id')->all();

        $subscribedShows = $user->subscribers()->pluck('id')->all();

        $qry = DB::table('show_popular_show')->distinct('show_id')
            ->whereIn('category_id', $interested_category);

        if (count($subscribedShows) > 0) {
            $qry->whereNotIn('show_id', $subscribedShows);
        }

        $results = $qry->orderBy('sequence')->take(config('config.pagination.feed_for_you_show'))
            ->get(['show_id', 'sequence']);

        $orderArray = [];
        foreach ($results as $item) {
            $orderArray[] = $item->show_id;
        }

        $recommended_shows_data = [];
        $recommended_episode_data = [];

        if (count($orderArray)) {
            $query = Show::whereIn('id', $orderArray)->published();

            $query->orderByRaw('FIELD(id,' . @implode(',', $orderArray) . ') ASC');

            $shows = $query->get(['id', 'title', 'image']);

            $episodes = [];
            foreach ($shows as $show) {
                $episodes[] = $show->episodes()->published()->orderBy('date_created', 'DESC')->first(['id', 'title', 'show_id', 'mp3', 'duration', 'listen_count', 'explicit']);
                $recommended_shows_data[] = [
                    'id' => $show->id,
                    'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                    'show_image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                    'no_of_episodes' => 0,//$show->episodes()->published()->count()
                ];
            }

            // Episode Code Start Here
            foreach ($episodes as $value) {
                if ($value) {
                    $recommended_episode_data[] = [
                        'id' => $value->id,
                        'show_id' => $value->show_id,
                        'episode_title' => trim(str_replace("\n", '', html_entity_decode($value->title))),
                        'audio_file' => $value->getAudioLink(),
                        'show_title' => $value->show_id != '' ? trim(str_replace("\n", '', html_entity_decode($value->show->title))) : '',
                        'show_image' => !empty($value->show->image) ? $value->show->getWSImage(200) : asset('uploads/default/show.png'),
                        'duration' => $value->getDurationText(),
                        'listen_count' => Helper::shorten_count($value->listen_count),
                        'explicit' => $value->show->explicit
                    ];
                }
            }
            // Episode Code End Here
        }

        //Recomended Board
        $query = Board::where('private', 'N')->withCount('followers')->whereHas('categories', function ($q) use ($interested_category) {
            $q->whereIn('category_id', $interested_category);
        })->has('episodes');

        $query->whereDoesntHave('followers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        $recommended_boards = $query->take(config('config.pagination.feed_for_you_board'))
            ->get(['id', 'title', 'user_id', 'image', 'private']);

        //$user_boards = $user->following_boards()->pluck('id')->all();

        $recommended_boards_data = [];
        foreach ($recommended_boards as $values) {
            $follow_status = 0;
            // if(@in_array($values->id, $user_boards)) {
            //     $follow_status = 1;
            // }
            $recommended_boards_data[] = [
                'id' => $values->id,
                'board_name' => $values->title,
                'private' => $values->private,
                'board_owner' => $values->user_id == $user->id ? 1 : 0,
                'image' => !empty($values->image) ? $values->getImage(200) : asset('uploads/default/board.png'),
                'no_of_episodes' => Helper::shorten_count($values->episodes()->count()),
                'no_of_follower' => Helper::shorten_count($values->followers()->count()),
                'last_updated' => $values->updated_at->diffForHumans(),
                'name' => $values->user->notification_format_name,
                'username' => $values->user->username,
                'follow_status' => $follow_status
            ];
        }

        $followed_user = $user->following()->pluck('id')->all();

        $query = User::verified()->approved()->where('id', '!=', $user->id)
            ->whereNotNull('username')->where('id', '!=', $user->id)->whereNotIn('id', $followed_user);
        // ->with(['following' => function($q) use($user) {
        //     $q->where('id', '!=', $user->id);
        // }]);

        $query->whereHas('categories', function ($q) use ($interested_category) {
            $q->whereIn('category_id', $interested_category);
        });

        $recommended_users = $query->take(config('config.pagination.feed_for_you_users'))
            ->get(['id', 'first_name', 'last_name', 'username', 'image']);

        $recommended_users_data = [];
        foreach ($recommended_users as $values) {
            $recommended_users_data[] = [
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower()->count(),
                'follow_status' => 0,
            ];
        }

        $data = [
            'recommended_shows_data' => $recommended_shows_data,
            'recommended_boards_data' => $recommended_boards_data,
            'recommended_episode_data' => $recommended_episode_data,
            'users_common_interest' => $recommended_users_data
        ];

        return response()->api([
            'status' => true,
            'data' => $data,
        ]);
    }

    /**
     * Recommended Shows
     *
     * @param Request $request
     * @return void
    */
    public function recommended_shows(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interested_category = $user->categories->pluck('id')->all();

        $subscribedShows = $user->subscribers()->pluck('id')->all();

        $qry = DB::table('show_popular_show')->distinct('show_id')
            ->whereIn('category_id', $interested_category);

        if (count($subscribedShows) > 0) {
            $qry->whereNotIn('show_id', $subscribedShows);
        }

        $results = $qry->orderBy('sequence')->paginate(config('config.pagination.historical_recommended_show'), ['show_id', 'sequence']);
        //take(config('config.pagination.feed_for_you_show'))
        //->get(['show_id', 'sequence']);

        $orderArray = [];
        foreach ($results as $item) {
            $orderArray[] = $item->show_id;
        }

        $query = Show::whereIn('id', $orderArray)->published();

        // $query->whereDoesntHave('subscribers', function ($q) use ($user) {
        //     $q->where('user_id', $user->id);
        // });

        $query->orderByRaw('FIELD(id,' . @implode(',', $orderArray) . ') ASC');

        $user_unsubscribed_shows = $query->get(['id', 'title', 'image']);

        $data = [];
        foreach ($user_unsubscribed_shows as $show) {
            $data[] = [
                'id' => $show->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'no_of_episodes' => 0,//$show->episodes()->published()->count(),
            ];
        }

        $message = '';

        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $results->total(),
                    'per_page' => $results->perPage(),
                    'pages' => ceil($results->total() / $results->perPage()),
                    'items' => $data
                ]
            ]);
        }
    }

    /**
     * This function is use for get recommended Users
     * @param Request $request
     * @return response
     */
    public function recommended_users(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //$interested_category = $user->categories->pluck('id')->all();

        $followed_user = $user->following()->pluck('id')->all();

        //dump($followed_user);

        $query = User::whereIn('provider', ['Direct', 'Google'])->verified()->approved()
            ->whereNotNull('username')->where('id', '!=', $user->id)->whereNotIn('id', $followed_user);
        //->with(['following' => function($q) use($user, $followed_user) {
        //$q->where('id', '!=', $user->id);
        //}]);

        // $query->whereHas('categories', function ($q) use ($interested_category) {
        //     $q->whereIn('category_id', $interested_category);
        // });

        // $query->orderBy('app_last_access_time', 'DESC')->orderBy('id', 'DESC');

        if ($request->no_paginate == 'Yes') {
            $recommended_users = $query->get();
        } else {
            $recommended_users = $query->paginate(config('config.pagination.historical_recommended_user'), ['id', 'username', 'first_name', 'last_name', 'image', 'app_last_access_time']);
        }

        //dd($recommended_users->toArray());

        $data = [];
        foreach ($recommended_users as $values) {
            $data[] = [
                //'id' => $values->id,
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower()->count(),
                'follow_status' => 0,
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $recommended_users->total(),
                    'per_page' => $recommended_users->perPage(),
                    'pages' => ceil($recommended_users->total() / $recommended_users->perPage()),
                    'items' => $data
                ]
            ]);
        }
    }

    /**
     * This function is use for get recommended facebook Users
     * @param Request $request
     * @return response
     */
    public function recommended_fb_users(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
            'fb_friends_list' => 'required'
         ], [
            'fb_friends_list.required' => config('language.' . $this->getLocale() . ".Feed.missing_parameter_fb_friends_list_lbl")
         ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
             ]);
        }

        //$interested_category = $user->categories->pluck('id')->all();

        $followed_user = $user->following()->pluck('id')->all();

        $fb_array = [];
        if (!empty($request->fb_friends_list)) {
            $fb_array_ids = explode(',', $request->fb_friends_list);
            $fb_array = UserSocial::whereIn('provider_id', $fb_array_ids)
                ->where('provider', 'Facebook')->pluck('user_id')->all();
        }

        //dd($fb_array, $followed_user);

        $query = User::verified()->approved()
            ->whereNotNull('username')->where('id', '!=', $user->id)
            ->whereNotIn('id', $followed_user)->whereIn('id', $fb_array);

        //->with(['following' => function($q) use($user, $followed_user) {
        //$q->where('id', '!=', $user->id);
        //}]);

        // $query->whereHas('categories', function ($q) use ($interested_category) {
        //     $q->whereIn('category_id', $interested_category);
        // });

        // $query->orderBy('app_last_access_time', 'DESC')->orderBy('id', 'DESC');

        if ($request->no_paginate == 'Yes') {
            $recommended_users = $query->get();
        } else {
            $recommended_users = $query->paginate(config('config.pagination.historical_recommended_user'), ['id', 'username', 'first_name', 'last_name', 'image', 'app_last_access_time']);
        }

        //dd($recommended_users->toArray());

        $data = [];
        foreach ($recommended_users as $values) {
            $data[] = [
                //'id' => $values->id,
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower()->count(),
                'follow_status' => 0,
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $recommended_users->total(),
                'per_page' => $recommended_users->perPage(),
                'pages' => ceil($recommended_users->total() / $recommended_users->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for get recommended twitter Users
     * @param Request $request
     * @return response
     */
    public function recommended_twitter_users(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
            'twitter_friends_list' => 'required'
         ], [
            'twitter_friends_list.required' => config('language.' . $this->getLocale() . ".Feed.missing_parameter_twitter_friends_list_lbl")
         ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
             ]);
        }

        //$interested_category = $user->categories->pluck('id')->all();

        $followed_user = $user->following()->pluck('id')->all();

        $twitter_array = [];
        if (!empty($request->twitter_friends_list)) {
            $twitter_array_ids = explode(',', $request->twitter_friends_list);
            $twitter_array = UserSocial::whereIn('provider_id', $twitter_array_ids)
                ->where('provider', 'Twitter')->pluck('user_id')->all();
        }

        //dd($twitter_array, $followed_user);

        $query = User::verified()->approved()
            ->whereNotNull('username')->where('id', '!=', $user->id)
            ->whereIn('id', $twitter_array);
        //->whereNotIn('id', $followed_user)->whereIn('id', $twitter_array);

        //->with(['following' => function($q) use($user, $followed_user) {
        //$q->where('id', '!=', $user->id);
        //}]);

        // $query->whereHas('categories', function ($q) use ($interested_category) {
        //     $q->whereIn('category_id', $interested_category);
        // });

        // $query->orderBy('app_last_access_time', 'DESC')->orderBy('id', 'DESC');

        if ($request->no_paginate == 'Yes') {
            $recommended_users = $query->get(['id', 'username', 'first_name', 'last_name', 'image', 'app_last_access_time']);
        } else {
            $recommended_users = $query->paginate(20, ['id', 'username', 'first_name', 'last_name', 'image', 'app_last_access_time']);
        }

        //dd($recommended_users->toArray());

        $data = [];
        $follow_status = 'false';
        foreach ($recommended_users as $values) {
            $twiterInfo = $values->socialIds()->where('provider', 'Twitter')->first(['provider_id']);
            $follow_status = @in_array($values->id, $followed_user) ? 'true' : 'false';
            $data[] = [
                //'id' => $values->id,
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower()->count(),
                'follow_status' => $follow_status,
                'twitter_id' => $twiterInfo->provider_id
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $recommended_users->total(),
                'per_page' => $recommended_users->perPage(),
                'pages' => ceil($recommended_users->total() / $recommended_users->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for get recommended Users
     * @param Request $request
     * @return response
     */
    public function recommended_boards(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interested_category = $user->categories->pluck('id')->all();

        $query = Board::where('user_id', '!=', $user->id)->where('private', 'N')->whereHas('categories', function ($q) use ($interested_category) {
            $q->whereIn('category_id', $interested_category);
        })->has('episodes');

        $query->whereDoesntHave('followers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        if ($request->no_paginate == 'Yes') {
            $recommended_boards = $query->get();
        } else {
            $recommended_boards = $query->paginate(config('config.pagination.historical_recommended_board'));
        }

        $user_boards = $user->boards()->pluck('id')->all();

        $data = [];

        foreach ($recommended_boards as $values) {
            $follow_status = 0;
            if (@in_array($values->id, $user_boards)) {
                $follow_status = 1;
            }
            $data[] = [
                'id' => $values->id,
                'board_name' => $values->title,
                'private' => $values->private,
                'board_owner' => $user->id == $values->user_id ? 1 : 0,
                'no_of_episodes' => Helper::shorten_count($values->episodes()->count()),
                'no_of_follower' => Helper::shorten_count($values->followers()->count()),
                'last_updated' => $values->updated_at->diffForHumans(),
                'image' => !empty($values->image) ? $values->getImage(200) : asset('uploads/default/board.png'),
                'name' => $values->user->notification_format_name,
                'username' => $values->user->username,
                'follow_status' => $follow_status
             ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $recommended_boards->total(),
                    'per_page' => $recommended_boards->perPage(),
                    'pages' => ceil($recommended_boards->total() / $recommended_boards->perPage()),
                    'items' => $data
                ]
            ]);
        }
    }

    /**
     * Recommended Episodes
     *
     * @param Request $request
     * @return void
     */
    public function recommended_episodes(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interested_category = $user->categories->pluck('id')->all();

        $subscribedShows = $user->subscribers()->pluck('id')->all();

        $qry = DB::table('show_popular_show')->distinct('show_id')
            ->whereIn('category_id', $interested_category);

        if (count($subscribedShows) > 0) {
            $qry->whereNotIn('show_id', $subscribedShows);
        }

        $request->page = $request->page ? ($request->page - 1) : 0;

        $skip = config('config.pagination.historical_recommended_episode') * $request->page;

        // $results = $qry->orderBy('sequence')
        //     ->take(config('config.pagination.historical_recommended_episode'))
        //     ->skip($skip)
        //     ->get(['show_id', 'sequence']);

        $results = $qry->orderBy('sequence')->paginate(config('config.pagination.historical_recommended_episode'), ['show_id', 'sequence']);

        $orderArray = [];
        foreach ($results as $item) {
            $orderArray[] = $item->show_id;
        }

        $query = Show::whereIn('id', $orderArray)->published();

        // $query->whereDoesntHave('subscribers', function ($q) use ($user) {
        //     $q->where('user_id', $user->id);
        // });

        $query->orderByRaw('FIELD(id,' . @implode(',', $orderArray) . ') ASC');

        $user_unsubscribed_shows = $query->with('episodes')->get(['id', 'title', 'image']);

        $episodes = [];
        foreach ($user_unsubscribed_shows as $show) {
            $episodes[] = $show->episodes()->published()->orderBy('date_created', 'DESC')->first(['id', 'title', 'show_id', 'mp3', 'duration', 'listen_count', 'explicit']);
        }

        $data = [];
        if (count($episodes)) {
            foreach ($episodes as $value) {
                $data[] = [
                    'id' => $value->id,
                    'show_id' => $value->show_id,
                    'episode_title' => trim(str_replace("\n", '', html_entity_decode($value->title))),
                    'audio_file' => $value->getAudioLink(),
                    'show_title' => $value->show_id != '' ? trim(str_replace("\n", '', html_entity_decode($value->show->title))) : '',
                    'episode_image' => !empty($value->show->image) ? $value->show->getWSImage(200) : asset('uploads/default/show.png'),
                    'duration' => $value->getDurationText(),
                    'listen_count' => Helper::shorten_count($value->listen_count),
                    'explicit' => $value->show->explicit
                ];
            }
        }

        $message = '';

        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_episode_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    // 'total' => 50000, //$results->total(),
                    // 'per_page' => 10, //$results->perPage(),
                    // 'pages' => 5000, //ceil($results->total()/$results->perPage()),
                    // 'items' => $data,
                    // 'message' => $message
                    'total' => $results->total(),
                    'per_page' => $results->perPage(),
                    'pages' => ceil($results->total() / $results->perPage()),
                    'items' => $data,
                    'message' => $message
                ]
            ]);
        }
    }

    /**
     * This function is use for feed masthead
     *
     * @param Request $request
     * @return void
     */
    public function feed_mastheads(Request $request)
    {        
        $data = [];
        //Podcast of the day
        $date = Carbon::today();
        $podcast_day = PodcastDay::Published()->where('date', date($date->parse()->format('Y-m-d')))->take(1)->first();
        if ($podcast_day) {
            $data[] = [
                'id' => $podcast_day->id,
                'title' => trim(str_replace("\n", '', $podcast_day->show->title)),
                'image' => !empty($podcast_day->show->image) ? $podcast_day->show->getWSImage(600) : '',
                'heading' => 'Podcast of the Day',
                'type' => 'podcast_of_day',
                'premium' => false,
                'content_type' => 'Podcast',
                'content_id' => $podcast_day->show->id,
                'link' => ''
            ];
        }

        //App Banner
        $mastheads = Masthead::published()->approved()->orderBy('order', 'ASC')->get();
        foreach ($mastheads as $item) {
            $data[] = [
                'id' => $item->id,
                'title' => $item->title,
                'image' => !empty($item->image) ? $item->getImage(1200, 600) : '',
                'heading' => '',
                'type' => $item->type,
                'premium' => $item->premium == 1 ? true : false,
                'content_type' => !empty($item->content_type) ? $item->content_type : '',
                'content_id' => $item->content_id,
                'link' => !empty($item->link) ? $item->link : ''
            ];
        }

        return response()->api([
            'status' => true,
            'message' => '',
            'data' => [
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for get recommended Users
     * @param Request $request
     * @return response
     */
    public function recommended_users_v2(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        $interested_category = $user->categories->pluck('id')->all();

        $followed_user = $user->following()->pluck('id')->all();

        //dump($followed_user);

        $query = User::whereIn('provider', ['Direct', 'Google'])->verified()->approved()
            ->whereNotNull('username')->where('id', '!=', $user->id)->whereNotIn('id', $followed_user);
        //->with(['following' => function($q) use($user, $followed_user) {
        //$q->where('id', '!=', $user->id);
        //}]);

        $query->whereHas('categories', function ($q) use ($interested_category) {
            $q->whereIn('category_id', $interested_category);
        });

        $query->orderBy('app_last_access_time', 'DESC')->orderBy('id', 'DESC');

        if ($request->no_paginate == 'Yes') {
            $recommended_users = $query->get();
        } else {
            $recommended_users = $query->paginate(config('config.pagination.historical_recommended_user'), ['id', 'username', 'first_name', 'last_name', 'image', 'app_last_access_time']);
        }

        //dd($recommended_users->toArray());

        $data = [];
        foreach ($recommended_users as $values) {
            $data[] = [
                //'id' => $values->id,
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower()->count(),
                'follow_status' => 0,
                'last_access_time' => $values->app_last_access_time
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => $recommended_users->total(),
                    'per_page' => $recommended_users->perPage(),
                    'pages' => ceil($recommended_users->total() / $recommended_users->perPage()),
                    'items' => $data
                ]
            ]);
        }
    }

    /**
     * This function is use for get recommended facebook Users for v2
     * @param Request $request
     * @return response
     */
    public function recommended_fb_users_v2(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
            'fb_friends_list' => 'required'
         ], [
            'fb_friends_list.required' => config('language.' . $this->getLocale() . ".Feed.missing_parameter_fb_friends_list_lbl")
         ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
             ]);
        }

        $interested_category = $user->categories->pluck('id')->all();

        $followed_user = $user->following()->pluck('id')->all();

        $fb_array = [];
        if (!empty($request->fb_friends_list)) {
            $fb_array_ids = explode(',', $request->fb_friends_list);
            $fb_array = UserSocial::whereIn('provider_id', $fb_array_ids)
                ->where('provider', 'Facebook')->pluck('user_id')->all();
        }

        //dd($fb_array, $followed_user);

        $query = User::verified()->approved()
            ->whereNotNull('username')->where('id', '!=', $user->id)
            ->whereNotIn('id', $followed_user)->whereIn('id', $fb_array);

        //->with(['following' => function($q) use($user, $followed_user) {
        //$q->where('id', '!=', $user->id);
        //}]);

        $query->whereHas('categories', function ($q) use ($interested_category) {
            $q->whereIn('category_id', $interested_category);
        });

        $query->orderBy('app_last_access_time', 'DESC')->orderBy('id', 'DESC');

        if ($request->no_paginate == 'Yes') {
            $recommended_users = $query->get();
        } else {
            $recommended_users = $query->paginate(config('config.pagination.historical_recommended_user'), ['id', 'username', 'first_name', 'last_name', 'image', 'app_last_access_time']);
        }

        //dd($recommended_users->toArray());

        $data = [];
        foreach ($recommended_users as $values) {
            $data[] = [
                //'id' => $values->id,
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower()->count(),
                'follow_status' => 0,
                'last_access_time' => $values->app_last_access_time
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $recommended_users->total(),
                'per_page' => $recommended_users->perPage(),
                'pages' => ceil($recommended_users->total() / $recommended_users->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for get recommended twitter Users for v2
     * @param Request $request
     * @return response
     */
    public function recommended_twitter_users_v2(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];

            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
            'twitter_friends_list' => 'required'
         ], [
            'twitter_friends_list.required' => config('language.' . $this->getLocale() . ".Feed.missing_parameter_twitter_friends_list_lbl")
         ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
             ]);
        }

        $interested_category = $user->categories->pluck('id')->all();

        $followed_user = $user->following()->pluck('id')->all();

        $twitter_array = [];
        if (!empty($request->twitter_friends_list)) {
            $twitter_array_ids = explode(',', $request->twitter_friends_list);
            $twitter_array = UserSocial::whereIn('provider_id', $twitter_array_ids)
                ->where('provider', 'Twitter')->pluck('user_id')->all();
        }

        //dd($twitter_array, $followed_user);

        $query = User::verified()->approved()
            ->whereNotNull('username')->where('id', '!=', $user->id)
            ->whereIn('id', $twitter_array);
        //->whereNotIn('id', $followed_user)->whereIn('id', $twitter_array);

        //->with(['following' => function($q) use($user, $followed_user) {
        //$q->where('id', '!=', $user->id);
        //}]);

        $query->whereHas('categories', function ($q) use ($interested_category) {
            $q->whereIn('category_id', $interested_category);
        });

        $query->orderBy('app_last_access_time', 'DESC')->orderBy('id', 'DESC');

        if ($request->no_paginate == 'Yes') {
            $recommended_users = $query->get(['id', 'username', 'first_name', 'last_name', 'image', 'app_last_access_time']);
        } else {
            $recommended_users = $query->paginate(20, ['id', 'username', 'first_name', 'last_name', 'image', 'app_last_access_time']);
        }

        //dd($recommended_users->toArray());

        $data = [];
        $follow_status = 'false';
        foreach ($recommended_users as $values) {
            $twiterInfo = $values->socialIds()->where('provider', 'Twitter')->first(['provider_id']);
            $follow_status = @in_array($values->id, $followed_user) ? 'true' : 'false';
            $data[] = [
                //'id' => $values->id,
                'name' => $values->notification_format_name,
                'username' => $values->username,
                'image' => !empty($values->image) ? $values->getImage(100) : asset('uploads/default/user.png'),
                'follower_count' => $values->follower()->count(),
                'follow_status' => $follow_status,
                'twitter_id' => $twiterInfo->provider_id,
                'last_access_time' => $values->app_last_access_time
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $recommended_users->total(),
                'per_page' => $recommended_users->perPage(),
                'pages' => ceil($recommended_users->total() / $recommended_users->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for get Factoids of the day for api v3.2
     * @param Request $request
     * @return response
     */
    public function factoids_of_the_day(Request $request)
    {        
        $date = Carbon::today()->toDateString();
        $factoids = Factoid::Published()->whereDate('date_appear', $date)->first(['id', 'title', 'episode_id']);

        if (!$factoids) {
            return response()->api([
                'status' => true,
                'data' => [],
                'message' => config('language.' . $this->getLocale() . ".Feed.factoids_of_the_day_not_found_lbl"),
                'factoid_display' => false
            ]);
        } else {
            $factoids_arr = [];

            if ($factoids->episode_id == 0) {
                $episode_flag = false;
                $episode = false;
            } else {
                $episode_flag = true;
                $episode = Episode::find($factoids->episode_id, ['id', 'show_id']);                
            }
            
            //dd($episode);
            $factoids_arr = [
                'id' => $factoids->id,
                'title' => $factoids->title,
                'episode_id' => $factoids->episode_id,
                'episode_image' => $episode != false ? $episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                'episode_flag' => $episode_flag
            ];

            return response()->api([
                'status' => true,
                'data' => $factoids_arr,
                'message' => config('language.' . $this->getLocale() . ".Common.success_lbl"),
                'factoid_display' => true
            ]);
        }
    }

    /**
    * This function is use for networks
    * @param Request $request
    * @return type
    */
    public function community_feeds(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => true,
                'data' => [
                    'oauth_enabled' => 0,
                    'total' => 0,
                    'per_page' => 10,
                    'pages' => 1,
                    'items' => [],                    
                    'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
                ]
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $query = Feed::query();
        $query->orWhere(function ($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\User")
                ->whereIn('type', [
                    FeedRepo::FOLLOW_USER, 
                    FeedRepo::EPISODE_HEARD, 
                    FeedRepo::SUBSCRIBE_SHOW, 
                    FeedRepo::FOLLOW_BOARD, 
                    FeedRepo::FOLLOW_SMARTLIST, 
                    FeedRepo::NEW_SHOW_POST, 
                    FeedRepo::NEW_EPISODE_POST, 
                    FeedRepo::NEW_PLAYLIST_POST, 
                    FeedRepo::NEW_SMARTPLAYLIST_POST, 
                    FeedRepo::NEW_COLLECTION_POST,
                    FeedRepo::INDIVIDUAL_POST /** New Individual Share Post 20/05/2019 */
                ])
                ->where('typeable_id', '!=', $user->id);                
        });

        $query->orWhere(function ($q) use ($user) {
            $q->where('typeable_type', "App\\Models\\Board")
                ->whereIn('type', [FeedRepo::BOARD_UPDATE])
                ->whereNotIn('typeable_id', function ($que) use ($user) {
                    $que->select('id')->from('boards')->where('user_id', $user->id);
            });
        });                

        $feed_items = $query->orderBy('updated_at', 'DESC')->paginate(config('config.pagination.feed_network'));

        $data = [];
        foreach ($feed_items as $feed_item) {
            if (!empty($this->feedMap[$feed_item->type])) {
                try {
                    if (with(new $this->feedMap[$feed_item->type]($feed_item, $user))->response()) {
                        $data[] = with(new $this->feedMap[$feed_item->type]($feed_item, $user))->response();
                    }
                } catch (\Exception $ex) {
                }
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Feed.network_feed_not_found_lbl");
        }        

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $feed_items->total(),
                'per_page' => $feed_items->perPage(),
                'pages' => ceil($feed_items->total() / $feed_items->perPage()),
                'items' => $data                
            ]
        ]);
    }
}
