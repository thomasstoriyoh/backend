<?php

namespace App\Http\Controllers\Api\new_version\V8;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\PremiumFeature;

use App\Traits\ResponseFormat;

use ReceiptValidator\iTunes\Validator as iTunesValidator;
use Carbon\Carbon;
use App\Models\PurchaseData;
use DateTime;

class InAppContentController extends Controller
{
    use ResponseFormat;
    
    /**
     * This function is use for fetching premium feature info data.
     * @param Request $request
     * @return array
     */
    public function premium_feature_info(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'premium_feature_id' => 'required'
        ], [
            'premium_feature_id.required' => config('language.' . $this->getLocale() . ".Inapp.missing_premium_feature_id")            
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Fetching data from tbl_premium_features table
        $premium_feature = PremiumFeature::where("availability_ios", 1)
            ->whereId($request->premium_feature_id)->first(['id', 'feature_name', 'feature_description' , 'apple_product_id']);

        //If we not found premium feature
        if (is_null($premium_feature)) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Inapp.premium_feature_info_message_lbl"),
                'user_premium_feature_status' => 0
            ]);            
        }

        //check if this premium already subscribed
        $checkExistingFeature = $user->premium_features()->find($premium_feature->id);
        if($checkExistingFeature) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Inapp.premium_feature_info_message1_lbl"),
                'user_premium_feature_status' => 1
            ]);
        }
        
        $data = [
            "feature_id" => $premium_feature->id,
            "feature_name" => $premium_feature->feature_name,
            "feature_description" => $premium_feature->feature_description,
            "product_id" => $premium_feature->apple_product_id
        ];
        
        //Send successfull response
        return response()->api([
            'status' => true,
            'message' => "",
            'user_premium_feature_status' => 0,
            'data' => $data
        ]);        
    }

    /**
     * This function is getting all premium features
     * @param Request $request
     * @return array
     */
    public function user_premium_features(Request $request) {

        $user = Auth::guard('api')->user();
        
        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        dd($user->premium_features->pluck('id')->all());
    }

    /**
     * This function is use for purchase validate
     * @param Request $request
     * @return array
     */
    public function purchase_validate(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'premium_feature_id' => 'required',
            'token' => 'required'
        ], [
            'premium_feature_id.required' => config('language.' . $this->getLocale() . ".Inapp.missing_premium_feature_id"),            
            'token.required' => config('language.' . $this->getLocale() . ".Inapp.missing_token")            
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }        
                
        //$validator = new iTunesValidator(iTunesValidator::ENDPOINT_PRODUCTION); // Or iTunesValidator::ENDPOINT_SANDBOX if sandbox testing
        $validator = new iTunesValidator(iTunesValidator::ENDPOINT_SANDBOX);

        $receiptBase64Data = $request->token;
        
        try {
            $response = $validator->setReceiptData($receiptBase64Data)->validate();
        } catch (Exception $e) {
            return response()->api([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }        
                        
        if ($response->isValid()) {
            try{
                $purchase = $response->getPurchases();
                $purchase = $purchase[0];                
                        
                //Insert data into our tables tbl_purchase_data and tbl_premium_feature_user
                if ($purchase) {

                    //Fetching data from tbl_premium_features table
                    $premium_feature = PremiumFeature::where('apple_product_id', $purchase->getProductId())
                        ->first(['id', 'feature_name', 'feature_description' ]);

                    //If we not found premium feature
                    if (is_null($premium_feature)) {
                        return response()->api([
                            'status' => false,
                            'message' => config('language.' . $this->getLocale() . ".Inapp.purchase_validate_message_lbl"),                
                        ]);            
                    }

                    //Create Invoice Id
                    $maxId = \DB::table('purchase_data')->max('id');
                    $maxPurchaseId = 0;
                    if(! is_null($maxId)) {
                        $maxPurchaseId = $maxId;
                    }
                    $maxPurchaseId = $maxPurchaseId + 1;
                    $invoice_no = "STR".date("ymdHi").$maxPurchaseId;

                    $published_at = new DateTime();
                    $published_at->setTimestamp(strtotime($purchase->getPurchaseDate()->toIso8601String()));
    
                    $insertArray = [
                        "invoice_number" => $invoice_no,
                        "user_id" => $user->id,
                        "purchase_type" => 1,
                        "feature_id" => $premium_feature->id,
                        "gateway" => "in_app",
                        "vendor" => "apple",
                        "payment_mode" => "in_app",
                        "transaction_id" => $purchase->getTransactionId(),
                        "billing_type" => "one_time",
                        "currency" => "USD",
                        "invoice_amount" => null,
                        "status" => "initiated",
                        "additional_status" => "-",
                        "token" => '',
                        "platform" => 'iphone',
                        "invoice_date" => $published_at->format('Y-m-d H:i:s'),                    
                        "status" => "success",
                        "additional_status" => "-",
                        "token" => null,
                        "response" => json_encode($purchase->getRawResponse())
                    ];
                    
                    PurchaseData::create($insertArray);
                    
                    $feature_user = $user->premium_features()->find($premium_feature->id);
                    if(is_null($feature_user)) {
                        $user->premium_features()->attach($premium_feature->id, ["premium_feature_start_date" => $published_at->format('Y-m-d H:i:s')]);
                    } 

                    return response()->api([
                        'status' => true,
                        'premium_feature_id' => $premium_feature->id,
                        'message' =>config('language.' . $this->getLocale() . ".Inapp.purchase_validate_message1_lbl"),
                    ]);
                }                
            } catch(Exception $e) {
                return response()->api([
                    'status' => false,
                    'message' => "Something is went wrong. Please try again."
                ]);
            }            
        } else {
            //Fetching data from tbl_premium_features table
            $premium_feature = PremiumFeature::whereId($request->premium_feature_id)
                ->first(['id', 'feature_name', 'feature_description' ]);

            //If we not found premium feature
            if (is_null($premium_feature)) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Inapp.purchase_validate_message2_lbl"),                
                ]);            
            }

            //Create Invoice Id
            $maxId = \DB::table('purchase_data')->max('id');
            $maxPurchaseId = 0;
            if(! is_null($maxId)) {
                $maxPurchaseId = $maxId;
            }
            $maxPurchaseId = $maxPurchaseId + 1;
            $invoice_no = "STR".date("ymdHi").$maxPurchaseId;

            $insertArray = [
                "invoice_number" => $invoice_no,
                "user_id" => $user->id,
                "purchase_type" => 1,
                "feature_id" => $premium_feature->id,
                "gateway" => "in_app",
                "vendor" => "apple",
                "payment_mode" => "in_app",
                "transaction_id" => "",
                "billing_type" => "one_time",
                "currency" => "USD",
                "invoice_amount" => null,
                "status" => "initiated",
                "additional_status" => "-",
                "token" => '',
                "platform" => 'iphone',
                "invoice_date" => Carbon::now(),                    
                "status" => "failed",
                "additional_status" => "-",
                "token" => null,
                "response" => json_encode($response->getRawData())
            ];
            
            PurchaseData::create($insertArray);

            return response()->api([
                'status' => false,
                'message' =>config('language.' . $this->getLocale() . ".Inapp.purchase_validate_message3_lbl"),
            ]);            
        }        
    }
}
