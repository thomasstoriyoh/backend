<?php

namespace App\Http\Controllers\Api\new_version\V8;

use App\Packages\OTP\OTP;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\Show;
use App\Models\UserEmailReset;
use App\Models\Category;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Traits\Helper;
use App\Models\Board;
use App\Traits\ResponseFormat;

/** Topic  Subscription Code Start */
use App\Jobs\FirebaseTopicSubscription;
/** Topic  Subscription Code End */

use App\ClassesV3\V3_3\IP2Location;

use App\Models\Feed;
use App\ClassesV3\Feed as NotificationRepo;
use App\Models\ShowNotification;
use Carbon\Carbon;
use App\Models\Episode;
use App\Models\Order;
use App\Models\UserWishlist;
use App\Traits\HelperV2;
use App\Models\EpisodeDetail;
use App\Models\MarketplaceCountry;

class MyAccountController extends Controller
{
    use ResponseFormat, HelperV2;

    /** Post like array for all types */
    protected $like_array = [
        NotificationRepo::SHOW_POST_LIKE,
        NotificationRepo::EPISODE_POST_LIKE,
        NotificationRepo::PLAYLIST_POST_LIKE,
        NotificationRepo::SMARTPLAYLIST_POST_LIKE,
        NotificationRepo::COLLECTION_POST_LIKE,
        NotificationRepo::INDIVIDUAL_POST_LIKE
    ];

    /** Post comment array for all types */
    protected $comment_array = [
        NotificationRepo::SHOW_POST_COMMENT,
        NotificationRepo::EPISODE_POST_COMMENT,
        NotificationRepo::PLAYLIST_POST_COMMENT,
        NotificationRepo::SMARTPLAYLIST_POST_COMMENT,
        NotificationRepo::COLLECTION_POST_COMMENT,
        NotificationRepo::INDIVIDUAL_POST_COMMENT
    ];

    /** Post tags array for all types */
    protected $tag_array = [
        NotificationRepo::SHOW_POST_COMMENT_TAG,
        NotificationRepo::EPISODE_POST_COMMENT_TAG,
        NotificationRepo::PLAYLIST_POST_COMMENT_TAG,
        NotificationRepo::SMARTPLAYLIST_POST_COMMENT_TAG,
        NotificationRepo::COLLECTION_POST_COMMENT_TAG,
        NotificationRepo::INDIVIDUAL_POST_COMMENT_TAG,
        NotificationRepo::INDIVIDUAL_POST_TAG
    ];

    /**
     * My Account page view.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $imageData = $user->image ? $user->getImage(100) : asset('uploads/default/user.png');

        $country = "";
        if(! is_null($user->country)) {
            if ($user->country != 'null') {
                $country = $user->country;
            }
        }

        $total_notification_count = $this->user_notification_social_count($user);

        $data = [];
        $data[] = [
            'provider' => $user->provider,
            'name' => $user->notification_format_name,
            'username' => $user->username,
            'email' => $user->email,
            'image' => $imageData,
            'city' => $user->city ? $user->city : '',
            'state' => $user->state ? $user->state : '',
            'country' => $country,
            'country_id' => @$user->country_id,
            'expiry' => $user->expiry,
            'discreet' => $user->discreet,
            'about_me' => $user->bio ? $user->bio : '',
            'following_users' => $user->following()->verified()->count(),
            'follower_users' => $user->follower()->verified()->count(),
            'my_interest' => $user->categories->count(),
            'my_shows' => $user->subscribers()->count(),
            'my_boards' => $user->boards()->count(),
            'notification_count' => $total_notification_count > 99 ? "99+" : Helper::shorten_count($total_notification_count),
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for social notification count
     *
     * @param Request $request
     * @return void
     */
    protected function user_notification_social_count($user)
    {
        try {            
    
            $boards = $user->boards()->pluck('id')->all();       
            
            $types = ['App\\Models\\Board' => ['board_user', 'board_id', 'user_id']];
                
            $query = Feed::query();
    
            foreach ($types as $type => $info) {
                $query->orWhere(function($q) use ($type, $info, $user) {
                    $q->where('typeable_type', $type)
                    ->whereIn('type', [NotificationRepo::BOARD_UPDATE, NotificationRepo::SHOW_UPDATE])
                    ->whereIn('typeable_id', function($que) use ($user, $info, $type) {
                        $que->select($info[1])->from($info[0])->where($info[2], $user->id);                    
                    });
                    
                    // Created at grater than created at in the pivot table
                    $q->where('created_at', '>=', function ($builder) use ($info, $user) {
                        return $builder->select('created_at')->from($info[0])
                            ->whereRaw($info[1].' = tbl_user_feeds.typeable_id')
                            ->where($info[2], $user->id);
                    });
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                });
            }
    
            $query->orWhere(function($q) use ($user) {
                $q->where('typeable_type', "App\\Models\\User")
                    ->where('type', NotificationRepo::FOLLOW_USER)
                    ->where('data', 'LIKE', '%"'.$user->id.'"%');
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
            });
        
            $query->orWhere(function($q) use ($boards, $user) {            
                foreach($boards as $board) {
                    $q->orWhere('data', 'LIKE', '%"'.$board.'"%');
                    $q->where('typeable_type', 'App\\Models\\User');
                    $q->where('type', NotificationRepo::FOLLOW_BOARD);
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                }
            });

            /* New Code Added 01/09/2018 */
            foreach($this->like_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                }); 
            }
    
            foreach($this->comment_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                }); 
            }
            
            foreach($this->tag_array as $item) {
                $query->orWhere(function($q) use ($user, $item) {
                    $q->where('typeable_type', "App\\Models\\User")
                        ->where('type', $item)
                        ->where('data', 'LIKE', '%"*'.$user->id.'*"%');
                    if ($user->last_access_time_social) {
                        $q->where('updated_at', '>=', $user->last_access_time_social);
                    }
                });
            }
            
            return $query->count();

        } catch(\Exception $ex) {
            return 0;
        }        
    }

    /**
     * This function is use for reset username
     * @param Request $request
     * @return type
     */
    public function checkUsername(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required|alpha_num|min:5|max:15|unique:users,username'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Account.val_username_lbl"),
            'username.alpha_num' => config('language.' . $this->getLocale() . ".Account.val_username_numlbl"),
            'username.min' => config('language.' . $this->getLocale() . ".Account.val_username_minlbl"),
            'username.max' => config('language.' . $this->getLocale() . ".Account.val_username_maxlbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        /** Bloacked Username Code Start */
        $blocked_username = config('config.blocked_username');

        if (in_array(strtolower(trim($request->username)), $blocked_username)) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.val_message_lbl")
            ]);
        }
        /** Bloacked Username Code End */

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Account.checkusername_message_bl")
        ]);
    }

    /**
     * My Account edit profile page.
     *
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
        ], [
            'first_name.required' => config('language.' . $this->getLocale() . ".Account.update_firstname_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $attributes = $request->only(['first_name', 'bio']);

        //Ip2Location code start
        $attributes['city'] = $user->city;
        $attributes['state'] = $user->state;
        $attributes['country'] = $user->country;
        if (!empty($request->ip_address)) {
            $responseData = new IP2Location('api');
            $location = $responseData->get_location();
            
            if (count($location['data']) > 0) {
                $attributes['city'] = $location['data']['city_name'];
                $attributes['state'] = $location['data']['region_name'];
                $attributes['country'] = $location['data']['country_name'];
            }
        }
        //Ip2Location code end

        //Update user info
        $user->forceFill($attributes)->save();

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Account.update_message_lbl"),
            'data' => [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'username' => $user->username,
                'country_id' => @$user->country_id,
                'city' => $user->city ? $user->city : '',
                'state' => $user->state ? $user->state : '',
                'country' => $user->country ? $user->country : '',
                'bio' => $user->bio,
            ]
        ]);
    }

    /**
     * My Account edit profile page.
     *
     * @param Request $request
     * @return mixed
     */
    public function update_image(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ], []);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $attributes = [];

        $old_image = $user->image;
        $filename = '';
        if ($request->image instanceof UploadedFile) {
            $filename = @date('YmdHis') . str_random(6) . '.' . $request->image->getClientOriginalExtension();

            $image_name = 'users/' . $user->id . '/' . $filename;
            Storage::disk('s3')->put($image_name, file_get_contents($request->image));

            $attributes['image'] = $filename;

            if (Storage::disk('s3')->exists('users/' . $user->id . '/' . $old_image)) {
                Storage::disk('s3')->delete('users/' . $user->id . '/' . $old_image);
                //@unlink($directory . '/' . $old_image);
            }
        } else {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.update_image_message_lbl")
            ]);
        }

        //Update user info
        $user->forceFill($attributes)->save();

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Account.update_image_message1_lbl"),
            'data' => [
                'image' => $user->image ? $user->getImage(100) : asset('uploads/default/user.png'),
                'full_image' => $user->image . '----' . $filename
            ]
        ]);
    }

    /**
    * This function is update username
    * @param User $user
    */
    public function update_username(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        if (!$user) {            
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        if (!$user->isVerified()) {
            return response()->api([
                'status' => false,
                'user_status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.update_username_message_lbl")
            ]);
        }

        if (!$user->isApproved()) {
            return response()->api([
                'status' => false,
                'user_status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.update_username_message1_lbl")
            ]);
        }

        $validator = Validator::make($request->all(), [
           'username' => 'required|alpha_num|min:5|max:15|unique:users,username',
           //'user_type' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Account.val_username_lbl"),
            'username.alpha_num' => config('language.' . $this->getLocale() . ".Account.val_username_numlbl"),
            'username.min' => config('language.' . $this->getLocale() . ".Account.val_username_minlbl"),
            'username.max' => config('language.' . $this->getLocale() . ".Account.val_username_maxlbl"),
            //'user_type.required' => config('language.' . $this->getLocale() . ".Account.val_usertype_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'user_status' => true,
               'message' => $validator->errors()->first()
            ]);
        }

        /** Bloacked Username Code Start */
        $blocked_username = config('config.blocked_username');

        if (in_array(strtolower(trim($request->username)), $blocked_username)) {
            return response()->api([
                'status' => false,
                'user_status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.val_message_lbl")
            ]);
        }
        /** Bloacked Username Code End */
                
        $category_notification_data = $user->categories()->count();

        $navigate = "feed";
        $user_type = 1;
        if($category_notification_data == 0) {
            $navigate = "select_category";
        }        
        if ($user->user_type == 0) {
            $user_type = 2;
        }
        
        // if ($user_type == 2 && $user->dashboard_status == 0) {
        //     $navigate = "recommended_user";
        // }

        $old_username = "";
        if(! is_null($user->username)) {
            $old_username = $user->username;
        }        
        
        $user->fill([
            "username" => $request->username,
            "discreet" => "N",
            'user_type' => $user_type,
            "last_access_time" => @date('Y-m-d H:i:s'),
            "last_access_time_social" => @date('Y-m-d H:i:s'),
        ])->save();

        //Added default follower Storiyoh, Rohit and Rahul
        try {
            $user->following()->attach(config('config.storiyoh_default_following_ids'));
        } catch (\Exception $ex) {
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Account.update_username_message2_lbl"),
            'navigate' => $navigate,
            'old_username' => $old_username,
            'user_type' => $request->user_type
        ]);
    }

    /**
    * @param $upload
    * @param $directory
    * @param $filename
    */
    protected function resizeUploadedImage($upload, $directory, $filename)
    {
        $image = Image::make($upload);
        $image->fit(300, 300)->save($directory . '/' . $filename, 75);
    }

    /** This function is use for update password
     * My Account reset password page.
     * @param Request $request
     * @return mixed
     */
    public function resetPassword(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
           'old_password' => 'required',
           'password' => 'required|min:8|confirmed'
        ], [
            'old_password.required' => config('language.' . $this->getLocale() . ".Account.val_old_password"),
            'password.required' => config('language.' . $this->getLocale() . ".Account.val_password"),
            'password.min' => config('language.' . $this->getLocale() . ".Account.val_password_min"),
            'password.confirmed' => config('language.' . $this->getLocale() . ".Account.val_password_con") 
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        if (!Hash::check($request->old_password, $user->password)) {
            return response()->api([
               'status' => false,
               'message' => config('language.' . $this->getLocale() . ".Account.resetpassword_message") 
            ]);
        }

        $user->update(['password' => Hash::make($request->password)]);

        return response()->api([
           'status' => true,
           'message' => config('language.' . $this->getLocale() . ".Account.resetpassword_message1") 
        ]);
    }

    /**
     * This function is use for change username
     * @param Request $request
     * @return type
     */
    public function resetUserName(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
           'username' => 'required|alpha_num|min:5|max:15|unique:users,username,' . $user->id,
           'password' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Account.val_username_lbl"),
            'username.alpha_num' => config('language.' . $this->getLocale() . ".Account.val_username_numlbl"),
            'username.min' => config('language.' . $this->getLocale() . ".Account.val_username_minlbl"),
            'username.max' => config('language.' . $this->getLocale() . ".Account.val_username_maxlbl"),
            'password.required' => config('language.' . $this->getLocale() . ".Account.val_password_cur")
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        if (!Hash::check($request->password, $user->password)) {
            return response()->api([
               'status' => false,
               'message' => config('language.' . $this->getLocale() . ".Account.resetusername_message")
            ]);
        }

        /** Bloacked Username Code Start */
        $blocked_username = config('config.blocked_username');

        if (in_array(strtolower(trim($request->username)), $blocked_username)) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.val_message_lbl")
            ]);
        }
        /** Bloacked Username Code End */

        $user->update(['username' => $request->username]);

        return response()->api([
           'status' => true,
           'message' => config('language.' . $this->getLocale() . ".Account.resetusername_message1")
        ]);
    }

    /**
     * This function is use for Email Reset
     * @param Request $request
     * @return type
     *
     */
    public function emailReset(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        if ($user->email == $request->email) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.emailreset_message")
            ]);
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required'
        ], [
            'email.required' => config('language.' . $this->getLocale() . ".Account.val_email"),
            'password.required' => config('language.' . $this->getLocale() . ".Account.val_password_cur")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        if (!Hash::check($request->password, $user->password)) {
            return response()->api([
               'status' => false,
               'message' =>  config('language.' . $this->getLocale() . ".Account.emailreset_message1")
            ]);
        }

        //check if user is exist or not
        $userExist = UserEmailReset::where('user_id', $user->id)->first();

        //Send OTP to the given email
        $email_otp = $this->sendVerificationEmail($request);

        //if old entry found delete it
        if ($userExist) {
            UserEmailReset::where('user_id', $user->id)->delete();
        }

        //create new entry for email change
        UserEmailReset::create([
            'user_id' => $user->id,
            'otp' => $email_otp,
            'value' => $request->email
        ]);

        //send JSON message
        return response()->api([
            'status' => true,
            'navigate' => 'verify_email_otp',
            'message' => config('language.' . $this->getLocale() . ".Account.emailreset_message2")
        ]);
    }

    /**
     * This function is send otp to the given email
     * @param User $user
     */
    protected function sendVerificationEmail(Request $request)
    {
        $otp = OTP::generate($request->email);

        $user = Auth::guard('api')->user();

        // Send SMS with OTP.
        Mail::send('emails.reset-email', compact('user', 'otp'), function ($mail) use ($user, $request) {
            $mail->from(config('config.notification.sender_email'), config('config.notification.sender_name'));
            $mail->to($request->email, $user->full_name)
                ->subject('Email Change Request');
        });

        return $otp;
    }

    /**
     * This function is use for verify SMS OTP
     * @param User $user
     */
    public function verifyEmailOtp(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'navigate' => 'my_account',
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'otp' => 'required'
        ], [
            'otp.required' => config('language.' . $this->getLocale() . ".Account.val_otp")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        if ($user) {
            $userExist = UserEmailReset::where('user_id', $user->id)->first();

            //check if this email is already exist
            $checkUser = User::where('email', $userExist->value)->count();
            if ($checkUser > 0) {
                return response()->api([
                  'status' => false,
                  'message' => config('language.' . $this->getLocale() . ".Account.verifyemailotp_message")
              ]);
            }

            if ($userExist) {
                if (OTP::verify($request->otp, $userExist->value)) {
                    //Update User Email
                    $user->forceFill(['email' => $userExist->value])->save();

                    //Delete Entry from DB
                    UserEmailReset::where('user_id', $user->id)->delete();

                    return response()->api([
                        'status' => true,
                        'navigate' => 'my_account',
                        'message' => config('language.' . $this->getLocale() . ".Account.verifyemailotp_message1")
                    ]);
                } else {
                    return response()->api([
                        'status' => false,
                        'navigate' => 'verify_email_otp',
                        'message' => config('language.' . $this->getLocale() . ".Account.verifyemailotp_message2")
                    ]);
                }
            } else {
                return response()->api([
                    'status' => false,
                    'navigate' => 'my_account',
                    'message' => config('language.' . $this->getLocale() . ".Account.verifyemailotp_message3")
                ]);
            }
        }

        return response()->api([
            'status' => false,
            'navigate' => 'my_account',
            'message' => config('language.' . $this->getLocale() . ".Account.verifyemailotp_message4")
        ]);
    }

    /**
     * This function is use for get all categories
     * @param Request $request
     * @return type
     */
    public function getMyInterest(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $user_categories = $user->categories()->pluck('id')->all();

        $category = Category::published()->whereDoesntHave('children')
            ->orderBy('title')->get(['id', 'title']);
        $select_status = false;
        $data = [];

        foreach ($category as $key => $values) {
            if (@in_array($values->id, $user_categories)) {
                $select_status = true;
            } else {
                $select_status = false;
            }
            $data[] = [
                'id' => $values->id,
                'title' => $values->title,
                'select_status' => $select_status
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
    * This function is use for set category
    * @param Request $request
    */
    public function setMyInterest(Request $request)
    {
        $user = Auth::guard('api')->user();
        
         //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
           'category_id' => 'required'
        ], [
           'category_id.required' => config('language.' . $this->getLocale() . ".Account.val_category_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        $user_categories = explode(',', $request->category_id);

        //check if user not select min 3 category
        if (sizeof($user_categories) < 3) {
            return response()->api([
               'status' => false,
               'message' => config('language.' . $this->getLocale() . ".Account.setmyinterest_message")
            ]);
        }

        //Sync category data with user
        $user->categories()->sync($user_categories);

        //return json from webservices
        return response()->api([
           'status' => true,
           'message' => config('language.' . $this->getLocale() . ".Account.setmyinterest_message1")
        ]);
    }

    /**
    * This function is use for get all user shows
    * @param Request $request
    * @return response
    */
    public function getMyShows(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        // $user_shows = $user->shows()
        //     ->withCount(['episodes' => function ($qry) {
        //         $qry->published();
        // }])->orderBy('show_user.updated_at', 'DESC')->paginate(12, ['id', 'title', 'image']);

        $user_shows = $user->shows()->orderBy('show_user.updated_at', 'DESC')->paginate(12, ['id', 'title', 'image']);
        $data = [];
        foreach ($user_shows as $values) {
            $data[] = [
                'id' => $values->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($values->title))),
                'image' => !empty($values->image) ? $values->getWSImage(200) : asset('uploads/default/show.png'),
                'notify' => $values->pivot->notify,
                'no_of_episode' => 0,//$values->episodes_count
            ];
        }

        return response()->api([
            'status' => true,
            'total' => $user_shows->total(),
            'per_page' => $user_shows->perPage(),
            'pages' => ceil($user_shows->total() / $user_shows->perPage()),
            'data' => $data
        ]);
    }

    /**
     * This function is use for get followers
     * @param Request $request
     * @return type
    */
    public function my_connections(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $following_user_id = $user->following()->with(['categories' => function ($q) use ($user) {
            $q->whereNotIn('id', $user->categories()->pluck('id')->all());
        }])->verified()->get();

        $user_followers = $user->follower()->with(['categories' => function ($q) use ($user) {
            $q->whereNotIn('id', $user->categories()->pluck('id')->all());
        }])->verified()->get();


        $followers_user_data = [];
        $following_user_data = [];

        foreach ($user_followers as $item) {
            $following_status = 0;
            if (@in_array($item->id, $following_user_id->pluck('id')->all())) {
                $following_status = 1;
            }

            $followers_user_data[] = [
                'id' => $item->id,
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follower_status' => $following_status,
                'follower_count' => Helper::shorten_count($item->follower()->verified()->count())
            ];
        }

        foreach ($following_user_id as $item) {
            $following_user_data[] = [
               'id' => $item->id,
               'name' => $item->notification_format_name,
               'username' => $item->username,
               'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
               'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
               'follower_status' => 1,
               'follower_count' => Helper::shorten_count($item->following()->verified()->count())
            ];
        }

        $data = [
            'follower_user' => $followers_user_data,
            'following_user' => $following_user_data
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for logout session for device in push table
     * @param Request $request
     * @return type
     */
    public function logout_device(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
           'device_token' => 'required',
        ], [
            'device_token.required' => config('language.' . $this->getLocale() . ".Account.val_device_token"),
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        /** Topic  Subscription Code Start */
        //Unsubscribe all podcast for specific user
        $allShows = $user->subscribers()->pluck('id')->all();
        //\Log::info("ALl SHOWS LISTING In Logout === ". json_encode($allShows));
        if(count($allShows) > 0) {
            try {
                $uniqueShows = array_unique($allShows);
                $splitShows = array_chunk($uniqueShows, 10);
                foreach ($splitShows as $item) {
                    dispatch(new FirebaseTopicSubscription($item, $user->id, 'unsubscribe_on_logout', [$request->device_token]))->onQueue('unsubscribe-topic');
                }
            } catch(\Exception $ex) {}            
        }
        /** Topic  Subscription Code End */

        $push_id = \App\Models\PushId::where('user_id', $user->id)
            ->where('token', $request->device_token)->first();

        if ($push_id) {
            $push_id->update(['user_id' => null]);
        }

        //Revoke Access Token
        $request->user('api')->token()->revoke();

        //Revoke Refresh Token
        \DB::table('oauth_refresh_tokens')->where('access_token_id', $request->user('api')
            ->token()->id)->update(['revoked' => true]);        

        return response()->api([
            'status' => false,
            'message' => config('language.' . $this->getLocale() . ".Account.logout_device_message")
        ]);
    }

    /**
     * This function is use for setting show subscribe notification status
     * @param Request $request
     * @return type
     */
    public function show_notification_setting(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
           'show_id' => 'required',
           'status' => 'required'
        ], [
           'show_id.required' => config('language.' . $this->getLocale() . ".Account.val_show_id"),
           'status.required' => config('language.' . $this->getLocale() . ".Account.val_status")
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        $subscribed_user = $user->subscribers()->where('show_user.show_id', $request->show_id)->first();
        $subscribed_user->pivot->notify = $request->status;
        $subscribed_user->pivot->save();

        /** Topic  Unsubscribe Code Start */        
        try {
            if($request->status == 1) {
                dispatch(new FirebaseTopicSubscription($request->show_id, $user->id, 'subscribe'))->onQueue('subscribe-topic');
            } else {
                dispatch(new FirebaseTopicSubscription($request->show_id, $user->id, 'unsubscribe'))->onQueue('unsubscribe-topic');
            }
        } catch(\Exception $ex) {}                    
        /** Topic  Unsubscribe Code End */

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Account.show_ns_message")
        ]);
    }

    /**
     * This function is use for setting category notification status
     * @param Request $request
     * @return type
     */
    public function category_notification_setting(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
           'status' => 'required'
        ], [
           'status.required' => config('language.' . $this->getLocale() . ".Account.val_status")
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        $user->categories()
              ->newPivotStatement()
              ->where('user_id', '=', $user->id)
              ->update(['notify' => $request->status, 'updated_at' => date('Y-m-d h:i:s')]);

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Account.category_ns_message")
        ]);
    }

    /**
    * This function is use for get all categories
    * @param Request $request
    * @return type
    */
    public function getMyCategories(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $user_categories = $user->categories()->get(['id', 'title']);
        $data = [];

        foreach ($user_categories as $key => $values) {
            $data[] = [
                'id' => $values->id,
                'title' => $values->title,
                'notify' => $values->pivot->notify
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for maintain status of in-app purchases
     * @param Request $request
     * @return type
     */
    public function in_app_purchase(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
           'payment_status' => 'required'
        ], [
           'payment_status.required' => config('language.' . $this->getLocale() . ".Account.val_paymentstatus")
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        $user->type = $request->payment_status;
        $user->save();

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Account.inapp_purchase_message")
        ]);
    }

    /**
    * This function is use for get all user shows
    * @param Request $request
    * @return response
    */
    public function getMySubShows(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        // $user_shows = $user->shows()
        //     ->withCount(['episodes' => function ($qry) {
        //         $qry->published();
        //     }])->orderBy('show_user.updated_at', 'DESC')->take(6)->get(['id', 'title', 'image']);

        $user_shows = $user->shows()->orderBy('show_user.updated_at', 'DESC')->take(6)->get(['id', 'title', 'image']);

        $data = [];
        foreach ($user_shows as $key => $values) {
            $data[] = [
                'id' => $values->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($values->title))),
                'image' => !empty($values->image) ? $values->getWSImage(200) : asset('uploads/default/show.png'),
                'no_of_episode' => 0,//$values->episodes_count,
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
    * This function is use for set privacy for your hear activity
    * @param Request $request
    */
    public function setPrivacyStatus(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
           'discreet' => 'required'
        ], [
           'discreet.required' => config('language.' . $this->getLocale() . ".Account.val_discreet"),
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        $user->fill(['discreet' => $request->discreet])->save();

        return response()->api([
           'status' => true,
           'message' => config('language.' . $this->getLocale() . ".Account.setprivacystatus_message"),
           'discreet' => $request->discreet
        ]);
    }

    /**
    * This function is use for update last access time
    * @param Request $request
    */
    public function update_last_access_time(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        if($request->type == "Social") {
             $user->fill(['last_access_time_social' => date('Y-m-d H:i:s')])->save();
         } else {
            $user->fill(['last_access_time' => date('Y-m-d H:i:s')])->save();
        }

        //return json from webservices
        return response()->api([
           'status' => true,
           'message' => ''
        ]);
    }

    /**
     * Upload OPML File to server
     *
     */
    public function import_opml(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'file' => 'required',
        ], []);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $extentionArray = ['opml', 'xml'];
        if (!in_array(strtolower($request->file->getClientOriginalExtension()), $extentionArray)) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.import_opml_message")
            ]);
        }

        $filename = '';
        $data = [];
        if ($request->file instanceof UploadedFile) {
            $filename = $user->id . '.' . $request->file->getClientOriginalExtension();
            $file_name = 'users/' . $user->id . '/' . $filename;
            //Storage::disk('s3')->put($file_name, file_get_contents($request->file));

            $dataXml = file_get_contents($request->file);

            try {
                $myxmldata = simplexml_load_string($dataXml);
                $forLoopData = [];
                if (strtolower($request->file->getClientOriginalExtension()) == 'opml') {
                    if ($myxmldata->body->outline->outline) {
                        if ($myxmldata->body->outline->outline->count() > 1) {
                            $forLoopData = $myxmldata->body->outline->outline;
                        } else {
                            $forLoopData = $myxmldata->body->outline;
                        }
                    } else {
                        $forLoopData = $myxmldata->body->outline;
                    }
                } elseif (strtolower($request->file->getClientOriginalExtension()) == 'xml') {
                    //$forLoopData = $myxmldata->body->outline->outline;
                    if ($myxmldata->body->outline->outline) {
                        $forLoopData = $myxmldata->body->outline->outline;
                    } else {
                        $forLoopData = $myxmldata->body->outline;
                    }
                }
                foreach ($forLoopData as $item) {
                    $xml_file_name = (string) $item['xmlUrl'];

                    //Elastic Search
                    $show = Show::search('*')->where('feed_url', $xml_file_name)->where('status', 'published')->first();

                    //$show = Show::where('feed_url', $xml_file_name)->where('status', 'published')->first();
                    if (!empty($show)) {
                        $user_follow_status = $user->subscribers()->where('show_user.show_id', $show->id)->count();
                        $data[] = [
                            'id' => $show->id,
                            'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                            'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                            'no_of_episode' => 0,//$show->episodes()->count(),
                            'user_follow_status' => $user_follow_status,
                        ];
                    }
                }
            } catch (\Exception $ex) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Account.import_opml_message1")
                ]);
            }
        } else {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Account.import_opml_message2")
            ]);
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Account.import_opml_message3");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'item' => $data,
            ]
        ]);
    }

    /**
     * This function is use for subscribe shows
     * @param Request $request
     */
    public function subscribe_multiple_show(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //validation code
        $validator = Validator::make($request->all(), [
           'show_id' => 'required'
        ], [
           'show_id.required' => config('language.' . $this->getLocale() . ".Account.val_show_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
               'status' => false,
               'message' => $validator->errors()->first()
            ]);
        }

        $show_ids = explode(',', $request->show_id);
        $show_ids = array_unique(array_filter($show_ids));

        foreach ($show_ids as $show_id) {
            $subscriber_user = $user->subscribers()->find($show_id, ['id']);

            if (empty($subscriber_user)) {
                $showData = Show::find($show_id, ['title']);

                if (!empty($showData)) {
                    //Sync show data with user
                    $user->subscribers()->attach($show_id);

                    // Update Subscriber count in show table
                    Show::where('id', $show_id)->update(['no_of_subscribers' => \DB::raw('no_of_subscribers + 1')]);
                }
            }
        }

        //return json from webservices
        return response()->api([
           'status' => true,
           'message' => config('language.' . $this->getLocale() . ".Account.subscribe_multiple_show_message"),
        ]);
    }

    /**
     * This function is use for get user connections
     * @param Request $request
     * @return type
    */
    public function my_connections_with_pagination(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ], []);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }
        if ($request->type == 'followers') {
            $user_followers = $user->follower()->with(['categories' => function ($q) use ($user) {
                $q->whereNotIn('id', $user->categories()->pluck('id')->all());
            }])->verified()->orderBy('user_follow.updated_at', 'DESC')->paginate(10);

            $followers_user_data = [];
            $user_following_ids = $user->following()->pluck('id')->all();
            foreach ($user_followers as $item) {
                $following_status = 0;
                if (@in_array($item->id, $user_following_ids)) {
                    $following_status = 1;
                }
                $followers_user_data[] = [
                    'name' => $item->notification_format_name,
                    'username' => $item->username,
                    'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                    'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                    'follower_status' => $following_status,
                    'follower_count' => Helper::shorten_count($item->follower()->verified()->count())
                ];
            }

            $message = "";
            if($user_followers->total() == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
            }
            return response()->api([
                'status' => true,
                'message' => $message,
                'total' => $user_followers->total(),
                'per_page' => $user_followers->perPage(),
                'pages' => ceil($user_followers->total() / $user_followers->perPage()),
                'items' => $followers_user_data
            ]);
        } else if ($request->type == 'following') {
            $following_user_id = $user->following()->with(['categories' => function ($q) use ($user) {
                $q->whereNotIn('id', $user->categories()->pluck('id')->all());
            }])->verified()->orderBy('user_follow.updated_at', 'DESC')->paginate(10);
            $following_user_data = [];
            foreach ($following_user_id as $item) {
                $following_user_data[] = [
                    'name' => $item->notification_format_name,
                    'username' => $item->username,
                    'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                    'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                    'follower_status' => 1,
                    'follower_count' => Helper::shorten_count($item->following()->verified()->count())
                ];
            }

            $message = "";
            if($following_user_id->total() == 0) {
                $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
            }
            return response()->api([
                'status' => true,
                'message' => $message,
                'total' => $following_user_id->total(),
                'per_page' => $following_user_id->perPage(),
                'pages' => ceil($following_user_id->total() / $following_user_id->perPage()),
                'items' => $following_user_data
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.error"),
            'total' => 0,
            'per_page' => 0,
            'pages' => 0,
            'data' => []
        ]);
    }

    /**
     * This function is use for get user connections
     * @param Request $request
     * @return type
    */
    public function my_followers (Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".Account.val_board_id"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $board = Board::where('id', $request->board_id)->where('user_id', $user->id)
            ->first(['id','contributors']);

        if (!$board) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_playlist_found_lbl")
            ]);
        }

        $contributor_array = [];
        if (! is_null($board->contributors)) {
            $contributor_array = json_decode($board->contributors, 1);
        }               

        $sqlQuery = $user->follower()->with(['categories' => function ($q) use ($user) {
                $q->whereNotIn('id', $user->categories()->pluck('id')->all());
            }])->verified()->approved();
        if (count($contributor_array) > 0) {
            $sqlQuery->whereNotIn('id', $contributor_array);
        }
        
        $user_followers = $sqlQuery->paginate(10, ['id', 'first_name', 'last_name', 'username', 'image']);

        $followers_user_data = [];
        $user_following_ids = $user->following()->pluck('id')->all();
        foreach ($user_followers as $item) {
            $following_status = 0;
            if (@in_array($item->id, $user_following_ids)) {
                $following_status = 1;
            }
            $followers_user_data[] = [
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follower_status' => $following_status,
                'follower_count' => Helper::shorten_count($item->follower()->verified()->count())
            ];
        }

        $message = "";
        if($user_followers->total() == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_user_found_lbl");
        }
        return response()->api([
            'status' => true,
            'message' => $message,            
            'data' => [
                'total' => $user_followers->total(),
                'per_page' => $user_followers->perPage(),
                'pages' => ceil($user_followers->total() / $user_followers->perPage()),
                'items' => $followers_user_data
            ]
        ]);
        
        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.error"),
            'total' => 0,
            'per_page' => 0,
            'pages' => 0,
            'data' => []
        ]);
    }

    /**
     * Get User Id
     */
    public function getUserId(Request $request) {

        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $data = ["id" => $user->id];

        return response()->api([
            'status' => true,
            'message' => '',
            'data' => $data
        ]);
    }

    /**
     * This function is use for get followers
     * @param Request $request
     * @return type
    */
    public function my_following_users(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $following_users = $user->following()->verified()->approved()->orderBy('user_follow.updated_at', 'DESC')->get(['id', 'first_name', 'username', 'image']);

        $following_user_data = [];
        foreach ($following_users as $item) {
            $following_user_data[] = [
               'id' => $item->id,
               'name' => $item->notification_format_name,
               'username' => $item->username,
               'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png')
            ];
        }

        $data = [
            'following_user' => $following_user_data
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * This function is use for get user purchases
     * @param Request $request
     * @return type
    */
    public function my_purchases(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }        

        $user_purchases = $user->orders()->orderBy('updated_at', 'DESC')->where('status', 'success')->paginate(10, [
            'id', 'invoice_number', 'invoice_date', 'user_id', 'product_type', 'product_id',
            'invoice_type', 'payment_mode', 'currency', 'net_amount', 'tax_name', 'tax_percent', 'tax_amount', 
            'invoice_total', 'status', 'platform', 'giftBy'
        ]);

        $user_purchases_data = [];
        foreach ($user_purchases as $item) {            
            $show_id = "";
            $show_title = "";
            $episode_id = "";
            $episode_title = "";
            $content_type = "";
            $show_image = "";
            $episode_image = "";
            $duration = "";
            $explicit = 0;
            if($item->product_type == 1) { //Series
                $content = Show::find($item->product_id, ['id', 'title', 'image']);
                $content_type = "S";
                $show_id = $content->id;
                $show_title = $content->title;
                $show_image = !empty($content->image) ? $content->getWSImage(200) : asset('uploads/default/show.png');
            } else if($item->product_type == 2) { //Episode
                $content = Episode::find($item->product_id, ['id', 'title', 'show_id', 'image', 'duration']);
                $content_type = "E";
                $show_id = $content->show_id;
                $show_title = $content->show->title;
                $episode_id = $content->id;
                $episode_title = $content->title;
                $episode_image = !empty($content->image) ? $content->getImage(200) : asset('uploads/default/show.png');
                $duration = $content->getDurationText();
                $explicit = $content->show->explicit;
            } else if($item->product_type == 3) { //Independent Episode
                $content = Episode::find($item->product_id, ['id', 'title', 'show_id', 'image', 'duration']);
                $content_type = "IE";                
                $episode_id = $content->id;
                $episode_title = $content->title;
                $episode_image = !empty($content->image) ? $content->getImage(200) : asset('uploads/default/show.png');
                $duration = $content->getDurationText();
                $explicit = $content->show->explicit;
            }

            $giftedBy = "";
            if(! is_null($item->giftBy)) {
                $gifted = $item->sender;
                $giftedBy = @$gifted->first_name;
            }

            $user_purchases_data[] = [
                "order_id" => $item->id,
                "invoice_number" => $item->invoice_number,                
                "show_id" => trim($show_id),
                "show_title" => $show_title,
                "show_image" => $show_image,
                "episode_id" => trim($episode_id),
                "episode_title" => $episode_title,
                "episode_image" => $episode_image,
                "duration" => $duration,
                "explicit" => $explicit,
                "content_type" => $content_type,
                "invoice_date" => Carbon::parse($item->invoice_date)->format('jS M Y H:i'),
                "invoice_type" => $item->invoice_type == 1 ? "Local" : "International",
                "payment_via" => $item->payment_mode,
                "currency" => $item->currency,
                //"net_amount" => $item->net_amount,
                "tax_name" => $item->tax_name,
                "tax_percent" => $item->tax_percent,
                "tax_amount" => $item->tax_amount,
                "invoice_total" => $item->invoice_total,
                "status" => $item->status,
                "platform" => $item->platform,
                "giftedBy" => $giftedBy == "" ? "" : "Gifted by " . $giftedBy
            ];
        }

        $message = "";
        if(count($user_purchases_data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'total' => $user_purchases->total(),
            'per_page' => $user_purchases->perPage(),
            'pages' => ceil($user_purchases->total() / $user_purchases->perPage()),
            'items' => $user_purchases_data
        ]);
    }

    /**
     * This function is use for get purchase details
     * @param Request $request
     * @return type
    */
    public function purchase_show(Request $request)
    {
        $user = Auth::guard('api')->user();

        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }        

        $user_purchase = Order::where('user_id', $user->id)->where('id', $request->order_id)->first([
            'id', 'invoice_number', 'invoice_date', 'user_id', 'product_type', 'product_id',
            'invoice_type', 'payment_mode', 'currency', 'net_amount', 'tax_name', 'tax_percent', 
            'tax_amount', 'invoice_total', 'status', 'platform'
        ]);

        if (!$user_purchase) {            
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Marketplace.order_not_found_lbl")
            ]);
        }

        $content_type = "";
        if($user_purchase->product_type == 1) { //Series
            $content = Show::find($user_purchase->product_id, ['id', 'title']);
            $content_type = "S";
        } else if($user_purchase->product_type == 2) { //Episode
            $content = Episode::find($user_purchase->product_id, ['id', 'title', 'show_id']);
            $content_type = "E";
        } else if($user_purchase->product_type == 3) { //Independent Episode
            $content = Episode::find($user_purchase->product_id, ['id', 'title', 'show_id']);
            $content_type = "IE";
        }

        $data = [
            "order_id" => $user_purchase->id,
            "invoice_number" => $user_purchase->invoice_number,                
            "content_id" => $user_purchase->product_id,
            "content_title" => $content->title,
            "content_type" => $content_type,
            "invoice_date" => Carbon::parse($user_purchase->invoice_date)->format('jS M Y H:i'),
            "invoice_type" => $user_purchase->invoice_type == 1 ? "Local" : "International",
            "payment_via" => $user_purchase->payment_mode,
            "currency" => $user_purchase->currency,
            //"net_amount" => $user_purchase->net_amount,
            "tax_name" => $user_purchase->tax_name,
            "tax_percent" => $user_purchase->tax_percent,
            "tax_amount" => $user_purchase->tax_amount,
            "invoice_total" => $user_purchase->invoice_total,
            "status" => $user_purchase->status,
            "platform" => $user_purchase->platform,
        ];

        return response()->api([
            'status' => true,
            'message' => "",
            'data' => $data
        ]);
    }

    /**
     * This function is use for get user wishlist
     * @param Request $request
     * @return type
    */
    public function my_wishlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }
        
        $ip_address = $request->ip_address;
        //$ip_address = "100.87.113.164";
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();

        //Check if marketplace available in user country
        $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
            ->where('country_alpha2_code', $location['data']['country_code'])
            ->first(["id", "title", "country_alpha2_code", "currency_id"]);

        /* Check marketplace is available for country if availabel display wishlist
        otherwise display empty wishlist 
        */
        if (empty($marketplace_country)) {
            return response()->api([
                'status'    => true,
                'message'   => config('language.' . $this->getLocale() . ".Marketplace.country_missing"),
                'total'     => 0,
                'per_page'  => 0,
                'pages'     => 0,
                'items'     => []
            ]);
        }
        /* End */

        $user_wishlist_data = [];
        $user_wishlist = $user->wishlists()->orderBy('updated_at', 'DESC')->paginate(10, ['id', 'content_id', 'content_type', 'wishlist_date', 'updated_at']);        

        foreach ($user_wishlist as $item) {
            try {
                $show_id = "";
                $show_title = "";
                $episode_id = "";
                $episode_title = "";
                $content_type = "";
                $show_image = "";
                $episode_image = "";
                $duration = "";
                $explicit = 0;
                $purchase = 'no';
                $selling_price = 0;
                $selling_currency = 0;
                if($item->content_type == "Series") { //Series
                    $content = Show::find($item->content_id, ['id', 'title', 'image']);
                    if($content) {
                        $content_type = "S";
                        $show_id = $content->id;
                        $show_title = $content->title;
                        $show_image = !empty($content->image) ? $content->getWSImage(200) : asset('uploads/default/show.png');

                        if ($marketplace_country) {
                            $product_ids = $content->premium_pricing()->where('country_id', $marketplace_country->id)
                                ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                            $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                            $selling_currency = $marketplace_country->country_alpha2_code;
                        }

                        $purchase = $this->check_if_item_purchase_by_user($content->user_id, $user->id, $content->id, 'series', $selling_price);                        
                    }
                } else if($item->content_type == "Episode") { //Episode
                    $content = Episode::find($item->content_id, ['id', 'title', 'show_id', 'image', 'duration']);
                    $content_type = "E";
                    if($content) {
                        $show_id = $content->show_id;
                        $show_title = $content->show->title;
                        $episode_id = $content->id;
                        $episode_title = $content->title;
                        $episode_image = !empty($content->image) ? $content->getImage(200) : asset('uploads/default/show.png');
                        $duration = $content->getDurationText();
                        $explicit = $content->show->explicit;

                        if ($marketplace_country) {
                            $product_ids = $content->premium_pricing()->where('country_id', $marketplace_country->id)
                                ->where('currency_id', $marketplace_country->currency_id)->first(['selling_price']);
                            $selling_price = !empty($product_ids->selling_price) ?  $product_ids->selling_price : "";
                            $selling_currency = $marketplace_country->country_alpha2_code;
                        }

                        $purchase = $this->check_if_item_purchase_by_user($content->user_id, $user->id, $content->id, 'episode', $content->show->id, $selling_price);                        
                    }                
                }
            
                $user_wishlist_data[] = [
                    "id" => $item->id,
                    "show_id" => trim($show_id),
                    "show_title" => $show_title,
                    "show_image" => $show_image,
                    "episode_id" => trim($episode_id),
                    "episode_title" => $episode_title,
                    "episode_image" => $episode_image,
                    "duration" => $duration,
                    "explicit" => $explicit,
                    "content_type" => $content_type,
                    "wishlist_date" => Carbon::parse($item->wishlist_date)->format('jS M Y H:i'),
                    'purchase' => $purchase,
                    'selling_price' => $selling_price,
                    'selling_currency' => $selling_currency
                ];
            } catch(\Exception $ex) {
                continue;
            }
        }

        $message = "";
        if(count($user_wishlist_data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Marketplace.wishlist_not_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'total' => $user_wishlist->total(),
            'per_page' => $user_wishlist->perPage(),
            'pages' => ceil($user_wishlist->total() / $user_wishlist->perPage()),
            'items' => $user_wishlist_data
        ]);        
        
        $message = "";
        if(count($user_wishlist_data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Marketplace.wishlist_not_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message                
        ]);
    }

    /**
     * Added into Wishlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function add_wishlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $ip_address = $request->ip_address;
        //$ip_address = "100.87.113.164";
        $responseData = new IP2Location('api', null, $ip_address);
        $location = $responseData->get_location();

        //Check if marketplace available in user country
        $marketplace_country = MarketplaceCountry::where("status", "Published")->where("admin_status", "Approved")
            ->where('country_alpha2_code', $location['data']['country_code'])
            ->first(["id", "title", "country_alpha2_code", "currency_id"]);

        /* Check marketplace is available for country if availabel display wishlist
        otherwise display empty wishlist 
        */
        if (empty($marketplace_country)) {
            return response()->api([
                'status'    => false,
                'message'   => config('language.' . $this->getLocale() . ".Marketplace.country_missing")
            ]);
        }
        /* End */

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'content_type' => 'required'
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl1"),
            'content_type.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl2")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $check = $user->wishlists()->where('content_id', $request->content_id)
            ->where('content_type', $request->content_type);

        if($check->count()) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Marketplace.content_already_added")
            ]);
        }

        if($check->count() == 0) {
            $flag = false;
            if($request->content_type == "Series") {
                $contentData = Show::where("id", $request->content_id)->where('content_type', 1)->first();
                if(is_null($contentData)) {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found")
                    ]);
                }
                $flag = true;
            } else if($request->content_type == "Episode") {
                $contentData = EpisodeDetail::where('episode_id', $request->content_id)->first();
                if(is_null($contentData)) {
                    return response()->api([
                        'status' => false,
                        'message' => config('language.' . $this->getLocale() . ".Marketplace.item_not_found")
                    ]);
                }
                $flag = true;
            }
            
            if($flag == true) {
                $attributes = [
                    'content_id' => $request->content_id,
                    'content_type' => $request->content_type,
                    'user_id' => $user->id,
                    'wishlist_date' => date("Y-m-d H:i:s")                
                ];            
                
                //Create User Wishlist Entry
                UserWishlist::create($attributes);
            }            
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Marketplace.content_successful_added")
        ]);
    }

    /**
     * Delete into Wishlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function delete_wishlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'content_id' => 'required',
            'content_type' => 'required'            
        ], [
            'content_id.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl1"),
            'content_type.required' => config('language.' . $this->getLocale() . ".Marketplace.missing_parameter_content_id_lbl2")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $checkItem = $user->wishlists()->where('content_id', $request->content_id)->where('content_type', $request->content_type);        

        if($checkItem->count() == 0) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Marketplace.content_not_found_lbl")
            ]);
        }

        if($checkItem->count()) {
            $checkItem->delete();
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Marketplace.content_successful_remove")
        ]);
    }

    /**
     * This function is use for update developer id in tbl_users table
     */
    public function update_developer_id_to_user(Request $request)
    {
        $user = Auth::guard('api')->user();

        //Check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return response()->json($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'developer_id' => 'required',
        ], [
            'developer_id.required' => config('language.' . $this->getLocale() . ".Mixpanel.missing_parameter_developer_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        try {
            //Update developer_id
            $user->timestamps = false;
            $user->update(["developer_id" => $request->developer_id]);

            return response()->api([
                'status' => true,
                'message' => "",
                'developer_id' => $request->developer_id
            ]);
        } catch (\Exception $ex) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.error")
            ]);
        }
    }
}
