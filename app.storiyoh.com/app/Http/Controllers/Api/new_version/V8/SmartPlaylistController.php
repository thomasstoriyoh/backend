<?php

namespace App\Http\Controllers\Api\new_version\V8;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Tzsk\Collage\Facade\Collage;
use App\Models\SmartPlaylist;
use App\Models\Episode;
use App\Models\Show;
use App\Traits\Helper;
use App\ClassesV3\Feed;
use App\ClassesV3\V3_3\SmartPlaylist as SmartPlaylistClass;
use App\ClassesV3\V3_3\Search;
use App\Traits\FireBase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Feed as FeedRepo;
use App\Jobs\IndividualPushNotification;
use Illuminate\Support\Facades\Log;
use App\Traits\ResponseFormat;

class SmartPlaylistController extends Controller
{
    use ResponseFormat;

    /**
     * Smart Playlist Dashboard page view.
     *
     * @param Request $request
     * @return mixed
     */
    public function dashboard(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //fetching all following ids
        $allIds = $user->following_smart_playlist()->where('private', 'N')->with(['my_lists' => function ($q) use ($user) {
            $q->where('board_type', 0);
        }])->pluck('id')->all();

        $allIds2 = $user->my_lists()->where('board_type', 1)->pluck('id')->all();

        $boardIds = [];
        $boardTypeIds = [];

        foreach ($allIds as $item) {
            $boardIds[] = $item;
            $boardTypeIds[$item] = 0;
        }

        foreach ($allIds2 as $item) {
            $boardIds[] = $item;
            $boardTypeIds[$item] = 1;
        }

        //dd($boardIds, $boardTypeIds, $user->id);

        $query = SmartPlaylist::whereIn('id', $boardIds);

        $boardData = [];
        foreach ($query->get(['id', 'updated_at']) as $item) {
            $ShowArray = 0;
            foreach ($item->shows()->get(['shows.updated_at']) as $show) {
                if (strtotime($show->updated_at) > $ShowArray) {
                    $ShowArray = strtotime($show->updated_at);
                }
            }
            if ($ShowArray != 0) {
                if (strtotime($item->updated_at) > $ShowArray) {
                    $boardData[$item->id] = strtotime($item->updated_at);
                } else {
                    $boardData[$item->id] = $ShowArray;
                }
            }
        }

        $data = [];
        if (count($boardData) > 0) {
            arsort($boardData);

            $final_ids = array_keys($boardData);

            $query->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC');

            $all_boards = $query->withCount('shows', 'followers')->paginate(config('config.pagination.board'), ['id', 'user_id', 'title', 'description', 'image', 'private', 'updated_at']);

            foreach ($all_boards as $item) {
                //dump($item->toArray(), $boardData);
                $data[] = [
                    'id' => $item->id,
                    'name' => @$item->user->notification_format_name,
                    'username' => @$item->user->username,
                    'board_name' => $item->title,
                    'description' => $item->description,
                    'private' => $item->private,
                    'board_owner' => $user->id == $item->user_id ? 1 : 0,
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                    'user_image' => @$item->user->image ? @$item->user->getImage(100) : '',
                    'followers_count' => Helper::shorten_count($item->followers_count),
                    'no_of_episodes' => $item->shows_count,
                    'timestamp' => strtotime($item->updated_at),
                    'board_type' => @$boardTypeIds[$item->id],
                    'last_updated' => @Carbon::createFromTimestamp($boardData[$item->id])->format('jS M Y'),
                ];
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'items' => $data
            ]
        ]);
    }

    /**
     * Smart Playlist listing view.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $splaylist = new Search('api', $user);
        
        return $splaylist->smartplaylist_search();

        /* $query = SmartPlaylist::where('private', 'N');

        $query->whereDoesntHave('followers', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        });

        $boardData = [];
        foreach ($query->get(['id', 'updated_at']) as $item) {
            $ShowArray = 0;
            foreach ($item->shows()->get(['shows.updated_at']) as $show) {
                if (strtotime($show->updated_at) > $ShowArray) {
                    $ShowArray = strtotime($show->updated_at);
                }
            }
            if ($ShowArray != 0) {
                if (strtotime($item->updated_at) > $ShowArray) {
                    $boardData[$item->id] = strtotime($item->updated_at);
                } else {
                    $boardData[$item->id] = $ShowArray;
                }
            }
        }

        $data = [];
        if (count($boardData) > 0) {
            arsort($boardData);

            $final_ids = array_keys($boardData);

            $query->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC');

            //Search Query
            if (! empty($request->keyword)) {
                $searchText = urldecode(trim($request->keyword));
                $query->where(function ($query) use ($searchText) {
                    $query->orWhere('title', 'LIKE', '%' . $searchText . '%');

                    //Search in username
                    $query->orWhereHas('user', function ($q) use ($searchText) {
                        $q->where('username', 'LIKE', '%' . $searchText . '%');
                    });
                });
            }

            $all_boards = $query->withCount('shows', 'followers')->paginate(10, ['id', 'user_id', 'title', 'description', 'image', 'private', 'updated_at']);

            //$follow_status = 0;
            //$user_boards = $user->following_smart_playlist()->pluck('id')->all();

            foreach ($all_boards as $item) {
                $data[] = [
                    'id' => $item->id,
                    'name' => @$item->user->notification_format_name,
                    'username' => @$item->user->username,
                    'board_name' => $item->title,
                    'description' => $item->description,
                    'private' => $item->private,
                    'board_owner' => $user->id == $item->user_id ? 1 : 0,
                    'user_follow_status' => 0,
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                    'user_image' => @$item->user->image ? @$item->user->getImage(100) : '',
                    'no_of_follower' => Helper::shorten_count($item->followers_count),
                    'no_of_episodes' => $item->shows_count,
                    'timestamp' => strtotime($item->updated_at),
                    'last_updated' => @Carbon::createFromTimestamp($boardData[$item->id])->format('jS M Y'),
                ];
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No Smart Playlist Found.';
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_boards->total(),
                'per_page' => $all_boards->perPage(),
                'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                'items' => $data
            ]
        ]);*/
    }

    /**
     * My Smart Playlist listing view.
     *
     * @param Request $request
     * @return mixed
     */
    public function my_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $query = SmartPlaylist::where('user_id', $user->id);

        $boardData = [];
        foreach ($query->get(['id', 'updated_at']) as $item) {
            $ShowArray = 0;
            foreach ($item->shows()->get(['shows.updated_at']) as $show) {
                if (strtotime($show->updated_at) > $ShowArray) {
                    $ShowArray = strtotime($show->updated_at);
                }
            }
            if ($ShowArray != 0) {
                if (strtotime($item->updated_at) > $ShowArray) {
                    $boardData[$item->id] = strtotime($item->updated_at);
                } else {
                    $boardData[$item->id] = $ShowArray;
                }
            }
        }

        $data = [];
        if (count($boardData) > 0) {
            arsort($boardData);

            $final_ids = array_keys($boardData);

            $query->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC');

            $all_boards = $query->withCount('shows', 'followers')->paginate(config('config.pagination.board'), ['id', 'user_id', 'title', 'description', 'image', 'private', 'updated_at']);

            foreach ($all_boards as $item) {
                //dump($item->toArray(), $boardData);
                $data[] = [
                    'id' => $item->id,
                    'name' => @$item->user->notification_format_name,
                    'username' => @$item->user->username,
                    'board_name' => $item->title,
                    'description' => $item->description,
                    'private' => $item->private,
                    'board_owner' => 1,
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                    'user_image' => @$item->user->image ? @$item->user->getImage(100) : '',
                    'followers_count' => Helper::shorten_count($item->followers_count),
                    'no_of_episodes' => $item->shows_count,
                    'timestamp' => strtotime($item->updated_at),
                    'last_updated' => @Carbon::createFromTimestamp($boardData[$item->id])->format('jS M Y'),
                ];
            }

            return response()->api([
                'status' => true,
                'message' => count($data) == 0 ? config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl") : '',
                'data' => [
                    'total' => $all_boards->total(),
                    'per_page' => $all_boards->perPage(),
                    'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                    'items' => $data
                ]
            ]);
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'items' => $data
            ]
        ]);
    }

    /**
     * Show Smart Playlist
     * @param Request $request
     * @return type
     */
    public function show(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $spl = new SmartPlaylistClass('api', $user);

        return $spl->trending_smartplaylist_detail();

        /*$validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('config.missing_parameter1').' smart playlist id'. config('config.missing_parameter2').' Smart Playlist Details.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SmartPlaylist::where('id', $request->board_id)->withCount('shows', 'followers')->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => 'No Smart Playlist Found.'
            ]);
        }

        $board_follow_status = $user->following_smart_playlist()->where('smart_playlist_user.smart_playlist_id', $smart_playlist->id)->count();

        $user_follow_status = $user->following()->where('user_follow.following_id', $smart_playlist->user_id)->count();

        $data = [
            'id' => $smart_playlist->id,
            'name' => $smart_playlist->user->notification_format_name,
            'username' => $smart_playlist->user->username,
            'board_name' => $smart_playlist->title,
            'description' => $smart_playlist->description,
            'private' => $smart_playlist->private,
            'board_owner' => $user->id == $smart_playlist->user_id ? 1 : 0,
            'image' => !empty($smart_playlist->image) ? $smart_playlist->getImage(200) : asset('uploads/default/board.png'),
            'user_image' => $smart_playlist->user->image ? $smart_playlist->user->getImage(100) : asset('uploads/default/user.png'),
            'followers_count' => $smart_playlist->followers_count,
            'follow_status' => $board_follow_status,
            'user_follow_status' => $user_follow_status,
            'no_of_episodes' => $smart_playlist->shows_count,
           // "last_updated" => $smart_playlist->updated_at->diffForHumans(),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $smart_playlist->updated_at)->format('jS M Y')
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);*/
    }

    /**
     * Create Smart Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function add_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_name' => 'required',
            'private' => 'required',
            'show_id' => 'required'
        ], [
            'board_name.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_smart_playlist_name_message_lbl"),
            'private.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_smart_playlist_private_message_lbl"),
            'show_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_smart_playlist_show_id_message_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        if (! empty($request->show_id)) {
            $request->show_id = explode(',', $request->show_id);
            $request->show_id = array_filter($request->show_id);
            $request->show_id = array_unique($request->show_id);
        }

        $attributes = [
            'user_id' => $user->id,
            'title' => $request->board_name,
            'description' => ! empty($request->description) ? $request->description : '',
            'private' => $request->private ? $request->private : 'N'
        ];

        //dd($attributes);

        //Create Board
        $smart_playlist = SmartPlaylist::create($attributes);

        //insert into middle table for listings
        $smart_playlist->my_lists()->attach($user->id, ['board_type' => 1]);

        if (! empty($request->show_id)) {
            //Sync with shows
            $smart_playlist->shows()->sync($request->show_id);

            //Sync board with category
            $category = [];
            $shows = Show::whereIn('id', $request->show_id)->get(['id']);
            foreach ($shows as $item) {
                foreach ($item->categories()->get(['id']) as $category_item) {
                    $category[] = $category_item['id'];
                }

                //Update Show playlist_count
                try {
                    Show::where('id', $item->id)->update(['playlist_count' => \DB::raw('playlist_count + 1')]);
                } catch (\Exception $ex) {}
            }

            $category = array_filter($category);
            $category = array_unique($category);
            $smart_playlist->categories()->sync($category);

            //Generate Collage Image
            $show_id_reverse = array_reverse($request->show_id);
            $filename = $this->generateCollageImage($show_id_reverse);
            $smart_playlist->update(['image' => $filename]);
        }

        $image = asset('uploads/default/board.png');
        if (! empty($request->show_id)) {
            $image = !empty($smart_playlist->image) ? $smart_playlist->getImage(200) : asset('uploads/default/board.png');
        }

        $data = [
            'id' => $smart_playlist->id,
            'board_name' => $smart_playlist->title,
            'description' => $smart_playlist->description,
            'image' => $image,
            'private' => $smart_playlist->private,
            'timestamp' => strtotime($smart_playlist->updated_at),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $smart_playlist->updated_at)->format('jS M Y')
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Edit Smart Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function edit_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'board_name' => 'required',
            'private' => 'required',
            //'show_id' => 'required',
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
            'board_name.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_smart_playlist_name_message_lbl"),
            'private.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_smart_playlist_private_message_lbl")
            //'show_id.required' => 'Please select atleast one show.',            
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SmartPlaylist::where('id', $request->board_id)
            ->where('user_id', $user->id)->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }

        if (! empty($request->show_id)) {
            $request->show_id = explode(',', $request->show_id);
            $request->show_id = array_filter($request->show_id);
            $request->show_id = array_unique($request->show_id);
        }

        $attributes = [
            'title' => $request->board_name,
            'description' => ! empty($request->description) ? $request->description : '',
            'private' => $request->private ? $request->private : $smart_playlist->private
        ];

        $smart_playlist->fill($attributes)->save();

        /*if (! empty($request->show_id)) {
            //Sync with shows
            $smart_playlist->shows()->sync($request->show_id);

            //Sync board with category
            $category = [];
            $shows = Show::whereIn('id', $request->show_id)->get(['id']);
            foreach ($shows as $item) {
                foreach ($item->categories()->get(['id']) as $category_item) {
                    $category[] = $category_item['id'];
                }
            }

            $category = array_filter($category);
            $category = array_unique($category);
            $smart_playlist->categories()->sync($category);

            //Generate Collage Image
            $show_id_reverse = array_reverse($request->show_id);
            $filename = $this->generateCollageImage($show_id_reverse);
            $smart_playlist->update(['image' => $filename]);
        }*/

        $data = [
            'id' => $smart_playlist->id,
            'board_name' => $smart_playlist->title,
            'description' => $smart_playlist->description,
            'image' => !empty($smart_playlist->image) ? $smart_playlist->getImage(200) : asset('uploads/default/board.png'),
            'private' => $smart_playlist->private,
            'timestamp' => strtotime($smart_playlist->updated_at),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $smart_playlist->updated_at)->format('jS M Y'),
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove Board.
     *
     * @param Request $request
     * @return mixed
     */
    public function remove_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SmartPlaylist::where('id', $request->board_id)->where('user_id', $user->id)->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }

        //Update Show playlist_count
        try {
            $showIds = $smart_playlist->shows->pluck('id')->all();
            foreach($showIds as $item) {
                Show::where('id', $item)->update(['playlist_count' => \DB::raw('playlist_count - 1')]);
            }
        } catch(\Exception $ex){}
        

        if (!empty($smart_playlist->image)) {
            if (Storage::disk('s3')->exists('smart_playlists/' . $smart_playlist->image)) {
                Storage::disk('s3')->delete('smart_playlists/' . $smart_playlist->image);
            }
        }        

        //DELETE FOLLOW SMARTPLAYLIST
        $followBoardFeedData = FeedRepo::where('type', 'Follow Smartlist')
            ->where('typeable_type', 'App\\Models\\User')
            ->where('data', 'LIKE', '%"' . $smart_playlist->id . '"%');
        if ($followBoardFeedData->count()) {
            $followBoardFeedData->delete();
        }
        
        $smart_playlist->delete();

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.delete_lbl"),
        ]);
    }

    /**
     * Added Show into Smart Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function add_show_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'show_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
            'show_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_show_smart_playlist_message_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Check Board
        $smart_playlist = SmartPlaylist::where('id', $request->board_id)->where('user_id', $user->id)->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }

        //Check if this show already added
        $show_info = $smart_playlist->shows()->find($request->show_id, ['id']);

        if ($show_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_show_smart_playlist_already_added_lbl")
                //'This show is already added into this smart playlist.'
            ]);
        }

        if (!$show_info) {
            // added show into board
            $smart_playlist->shows()->attach($request->show_id);

            //Update Show playlist_count
            try {
                Show::where('id', $request->show_id)->update(['playlist_count' => \DB::raw('playlist_count + 1')]);
            } catch(\Exception $ex){}

            $show_info = Show::find($request->show_id);

            // added category into board
            foreach ($show_info->categories()->get(['id']) as $category_item) {
                $category_info = $smart_playlist->categories()->find($category_item['id'], ['id']);
                if (!$category_info) {
                    $smart_playlist->categories()->attach($category_item['id']);
                }
            }

            //Generate Collage Image
            $showReverseData = $smart_playlist->shows()->orderBy('show_smart_playlist.updated_at', 'desc')
                ->pluck('id')->all();
            $filename = $this->generateCollageImage($showReverseData);

            if (!empty($smart_playlist->image) && $filename) {
                if (Storage::disk('s3')->exists('smart_playlists/' . $smart_playlist->image)) {
                    Storage::disk('s3')->delete('smart_playlists/' . $smart_playlist->image);
                }
            }

            $smart_playlist->update(['image' => $filename]);

            //Update Board updated_at timestamp
            $smart_playlist->touch();
        }

        $data = [
            'id' => $smart_playlist->id,
            'board_name' => $smart_playlist->title,
            'image' => !empty($smart_playlist->image) ? $smart_playlist->getImage(200) : asset('uploads/default/board.png'),
            'private' => $smart_playlist->private,
            'timestamp' => strtotime($smart_playlist->updated_at),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $smart_playlist->updated_at)->format('jS M Y'),
        ];

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Added Show into Multiple Smart Playlist.
     *
     * @param Request $request
     * @return mixed
     */
    public function add_show_into_mutiple_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = ['data' => [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ]];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'show_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
            'show_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_show_smart_playlist_message_lbl")            
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $boards = explode(',', $request->board_id);
        $boards = array_filter($boards);
        $boards = array_unique($boards);

        if (count($boards) == 1) {
            //Select Board
            $smart_playlist = SmartPlaylist::where('id', $boards[0])
                ->where('user_id', $user->id)->first();

            if (!$smart_playlist) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
                ]);
            }

            //check if this episode already added
            $show_info = $smart_playlist->shows()->find($request->show_id, ['id']);
            //dd($episode_info);

            if ($show_info) {
                return response()->api([
                    'status' => false,
                    'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_show_smart_playlist_already_added_lbl")
                    //'This podcast is already added into this smart playlist.'
                ]);
            }
        }

        foreach ($boards as $board) {
            //Check Board
            $smart_playlist = SmartPlaylist::where('id', $board)->where('user_id', $user->id)->first();
            if (!empty($smart_playlist)) {
                //Check if this show already added
                $show_info = $smart_playlist->shows()->find($request->show_id, ['id']);
                if (empty($show_info)) {
                    // added show into board
                    $smart_playlist->shows()->attach($request->show_id);

                    //Update Episode playlist_count
                    try {
                        Show::where('id', $request->show_id)->update(['playlist_count' => \DB::raw('playlist_count + 1')]);
                    } catch(\Exception $ex){}

                    $show_info = Show::find($request->show_id);

                    // added category into board
                    foreach ($show_info->categories()->get(['id']) as $category_item) {
                        $category_info = $smart_playlist->categories()->find($category_item['id'], ['id']);
                        if (!$category_info) {
                            $smart_playlist->categories()->attach($category_item['id']);
                        }
                    }

                    //Generate Collage Image
                    $showReverseData = $smart_playlist->shows()
                        ->orderBy('show_smart_playlist.updated_at', 'desc')->pluck('id')->all();
                    $filename = $this->generateCollageImage($showReverseData);

                    if (!empty($smart_playlist->image) && $filename) {
                        if (Storage::disk('s3')->exists('smart_playlists/' . $smart_playlist->image)) {
                            Storage::disk('s3')->delete('smart_playlists/' . $smart_playlist->image);
                        }
                    }

                    $smart_playlist->update(['image' => $filename]);

                    //Update Smart Playlist updated_at timestamp
                    $smart_playlist->touch();
                }
            }
        }        

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Common.added_lbl")
        ]);
    }

    /**
     * Remove Show from Board.
     *
     * @param Request $request
     * @return mixed
     */
    public function remove_show_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required',
            'show_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
            'show_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.add_show_smart_playlist_message_lbl")            
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SmartPlaylist::where('id', $request->board_id)->where('user_id', $user->id)->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }

        if ($smart_playlist->shows()->count() == 1) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.remove_show_smart_playlist_message_lbl")
                //'You can not remove this show because smart playlist needed atleast 1 show.'
            ]);
        }

        //check if this show already remove
        $show_info = $smart_playlist->shows()->find($request->show_id, ['id']);

        if (!$show_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.remove_show_smart_playlist_already_remove_message_lbl")
                //'This show is already remove from this smart playlist.'
            ]);
        }

        if ($show_info) {
            // remove episode into board
            $smart_playlist->shows()->detach($request->show_id);

            //Update Show playlist_count
            try {
                Show::where('id', $request->show_id)->update(['playlist_count' => \DB::raw('playlist_count - 1')]);
            } catch(\Exception $ex){}

            $show_ids = $smart_playlist->shows->pluck(['id'])->all();

            //Sync board with category
            $category = [];
            $showsData = Show::whereIn('id', $show_ids)->get(['id']);
            foreach ($showsData as $item) {
                foreach ($item->categories()->get(['id']) as $category_item) {
                    $category[] = $category_item['id'];
                }
            }

            $category = array_filter($category);
            $category = array_unique($category);

            $smart_playlist->categories()->sync($category);

            //Generate Collage Image
            $generate_show_data = $smart_playlist->shows()->orderBy('show_smart_playlist.updated_at', 'desc')
                ->pluck('id')->all();
            $filename = $this->generateCollageImage($generate_show_data);

            if (!empty($smart_playlist->image) && $filename) {
                if (Storage::disk('s3')->exists('smart_playlists/' . $smart_playlist->image)) {
                    Storage::disk('s3')->delete('smart_playlists/' . $smart_playlist->image);
                }
                $smart_playlist->update(['image' => $filename]);
            }

            //Update Board updated_at timestamp
            $smart_playlist->touch();
        }

        $data = [
            'id' => $smart_playlist->id,
            'board_name' => $smart_playlist->title,
            'description' => $smart_playlist->description,
            'image' => !empty($smart_playlist->image) ? $smart_playlist->getImage(200) : asset('uploads/default/board.png'),
            'private' => $smart_playlist->private,
            'timestamp' => strtotime($smart_playlist->updated_at),
            'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $smart_playlist->updated_at)->format('jS M Y'),
        ];

        return response()->api([
            'status' => true,
            //'message' => 'Episode has been removed from your board list.',
            'message' => config('language.' . $this->getLocale() . ".Common.delete_lbl"),
            'data' => $data
        ]);
    }

    /**
     * Smart Smart Playlist Listing view.
     *
     * @param Request $request
     * @return mixed
     */
    public function smart_playlist_listing(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $all_boards = $user->smart_playlists()->orderBy('updated_at', 'Desc')->pluck('title', 'id');

        $data = [];
        foreach ($all_boards as $key => $item) {
            $data[] = [
                'id' => $key,
                'board_name' => $item
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'items' => $data
            ]
        ]);
    }

    /**
     * Smart Smart Playlist Episodes
     *
     * @param Request $request
     */
    public function smart_playlist_episodes(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl")
            //config('config.missing_parameter1').' smart playlist id'. config('config.missing_parameter2').' Smart Playlist Episode.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $spl = new SmartPlaylistClass('api', $user);

        return $spl->trending_smartplaylist_episodes();

        //Select Board
        /*$smart_playlist = SmartPlaylist::where('id', $request->board_id)->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => 'No Smart Playlist Found.'
            ]);
        }

        //All Smart playlist Episodes
        $all_board_shows_temp = $smart_playlist->shows()->published()->orderBy('id', 'DESC')->get(['id', 'title']);
        
        $tempId = [];
        foreach($all_board_shows_temp as $temp) {
            $item = $temp->episodes()->published()->orderBy('date_created', 'DESC')->first(['id', 'title', 'duration', 'mp3', 'show_id', 'updated_at']);
            $tempId[$temp->id] = strtotime($item->updated_at);
        }

        arsort($tempId);

        $final_ids = array_keys($tempId);
                
        if ($request->no_paginate == 'Yes') {
            $all_board_shows = $smart_playlist->shows()->published()->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC')
                ->get(['id', 'title']);
        } else {
            $all_board_shows = $smart_playlist->shows()->published()->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC')
                ->paginate(10, ['id', 'title']);
        }
        
        $data = [];
        foreach ($all_board_shows as $show) {
            $item = $show->episodes()->published()->orderBy('date_created', 'DESC')->first(['id', 'title', 'duration', 'mp3', 'show_id', 'created_at', 'listen_count', 'explicit']);
            if (!empty($item)) {
                $data[] = [
                    'id' => $item->id,
                    'title' => html_entity_decode($item->title),
                    'duration' => $item->getDurationText(),
                    'audio_file' => $item->getAudioLink(),
                    'show_id' => $item->show_id,
                    'show_title' => !empty($item->show) ? html_entity_decode($item->show->title) : 'No Show',
                    'episode_image' => !empty($item->show) ? $item->show->getWSImage(200) : asset('uploads/default/board.png'),
                    'listen_count' => Helper::shorten_count($item->listen_count), 
                    "explicit" => $item->show->explicit
                ];
            }
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No Episode Found.';
        }

        if ($request->no_paginate == 'Yes') {
            return response()->api([
                'status' => true,
                'message' => $message,
                'data' => [
                    'total' => count($data),
                    'per_page' => 10,
                    'pages' => ceil(count($data) / 10),
                    'items' => $data
                ]
            ]);
        } else {
            return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_board_shows->total(),
                'per_page' => $all_board_shows->perPage(),
                'pages' => ceil($all_board_shows->total() / $all_board_shows->perPage()),
                'items' => $data
                ]
            ]);
        }*/
    }

    /**
     * My Followed Boards
     * @param Request $request
     * @return type
     */
    public function my_followed_smart_playlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        //fetching all following ids
        $boardIds = $user->following_smart_playlist()->pluck('id')->all();

        $query = SmartPlaylist::whereIn('id', $boardIds)->where('private', 'N');
        $boardData = [];
        foreach ($query->get(['id', 'updated_at']) as $item) {
            $ShowArray = 0;
            foreach ($item->shows()->get(['shows.updated_at']) as $show) {
                if (strtotime($show->updated_at) > $ShowArray) {
                    $ShowArray = strtotime($show->updated_at);
                }
            }
            if ($ShowArray != 0) {
                if (strtotime($item->updated_at) > $ShowArray) {
                    $boardData[$item->id] = strtotime($item->updated_at);
                } else {
                    $boardData[$item->id] = $ShowArray;
                }
            }
        }

        $data = [];
        if (count($boardData) > 0) {
            arsort($boardData);
            $final_ids = array_keys($boardData);
            $query->orderByRaw('FIELD(id,' . implode(',', $final_ids) . ') ASC');

            $all_boards = $query->withCount(['shows', 'followers'])->paginate(config('config.pagination.followed_board'), ['id', 'smart_playlists.user_id', 'title', 'image', 'private', 'image', 'smart_playlists.updated_at']);

            foreach ($all_boards as $item) {
                //dump($boardData[$item->id]);
                $data[] = [
                    'id' => $item->id,
                    'board_name' => $item->title,
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                    'private' => $item->private,
                    'no_of_episodes' => $item->shows_count,
                    'no_of_follower' => Helper::shorten_count($item->followers_count),
                    'name' => $item->user->notification_format_name,
                    'username' => $item->user->username,
                    'user_photo' => !empty($item->user->image) ? $item->user->getImage(100) : asset('uploads/default/user.png'),
                    'last_updated' => Carbon::createFromTimestamp($boardData[$item->id])->format('jS M Y'),
                ];
            }

            return response()->api([
                'status' => true,
                'message' => count($data) == 0 ? config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl") : '',
                'data' => [
                    'total' => $all_boards->total(),
                    'per_page' => $all_boards->perPage(),
                    'pages' => ceil($all_boards->total() / $all_boards->perPage()),
                    'items' => $data
                ]
            ]);
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'items' => $data
            ]
        ]);
    }

    /**
     * User Followed Board
     * @param Request $request
     * @return type
     */
    public function followed_board_user(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {           
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $spl = new SmartPlaylistClass('api', $user);

        return $spl->trending_smartplaylist_follwers();

        /*$validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('config.missing_parameter1').' smart playlist id'. config('config.missing_parameter2').' Folllow Smart Playlist User.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SmartPlaylist::where('id', $request->board_id)->first();

        $message = '';
        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => 'No Smart Playlist Found.'
            ]);
        }

        $all_users = $smart_playlist->following_user()->with('categories')->where('verified', 'Verified')
            ->where('id', '!=', $user->id)
            ->where('admin_status', 'Approved')
            ->withCount(['follower as follower_status_count' => function ($qry) use ($user) {
                $qry->where('id', $user->id);
            }])->paginate(10, ['id', 'first_name', 'last_name', 'username', 'image']);

        //dd($all_users->toArray());

        $data = [];
        foreach ($all_users as $item) {
            $data[] = [
                'name' => $item->notification_format_name,
                'username' => $item->username,
                'image' => !empty($item->image) ? $item->getImage(100) : asset('uploads/default/user.png'),
                'also_interested_category' => @implode(', ', $item->categories()->pluck('title')->all()),
                'follow_status' => $item->follower_status_count
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = 'No User Found.';
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_users->total(),
                'per_page' => $all_users->perPage(),
                'pages' => ceil($all_users->total() / $all_users->perPage()),
                'items' => $data
            ]
        ]);*/
    }

    /**
     * My Network Boards
     * @param Request $request
     * @return type
     */
    public function my_network_smartlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $follow_user = $user->following()->pluck('id')->all();

        $data = [];
        if (count($follow_user) > 0) {
            //Select Board
            $query = SmartPlaylist::where('private', 'N')->whereIn('user_id', $follow_user)
                ->withCount(['shows', 'followers'])->has('shows');

            $query->orderBy('updated_at', 'Desc');

            $all_smartlist = $query->paginate(10);

            foreach ($all_smartlist as $item) {
                $data[] = [
                    'id' => $item->id,
                    'board_name' => $item->title,
                    'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/board.png'),
                    'private' => $item->private,
                    'no_of_episodes' => Helper::shorten_count($item->shows_count),
                    'no_of_follower' => Helper::shorten_count($item->followers_count),
                    'name' => $item->user->notification_format_name,
                    'username' => $item->user->username,
                    'user_photo' => !empty($item->user->image) ? $item->user->getImage(100) : asset('uploads/default/user.png'),
                    // "last_updated" => $item->updated_at->diffForHumans()
                    'last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at)->format('jS M Y'),
                ];
            }
        }

        if (count($data) == 0) {
            return response()->api([
                'status' => true,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl"),
                'data' => []
            ]);
        }

        return response()->api([
            'status' => true,
            'message' => '',
            'data' => [
                'total' => $all_smartlist->total(),
                'per_page' => $all_smartlist->perPage(),
                'pages' => ceil($all_smartlist->total() / $all_smartlist->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
     * This function is use for follow Smart Playlist
     * @param Request $request
     * @return type
     */
    public function follow_smartlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('config.missing_parameter1').' smart playlist id'. config('config.missing_parameter2').' Folllow Smart Playlist.',
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Check if board found
        $smart_playlist = SmartPlaylist::find($request->board_id);
        //dd($smart_playlist->toArray());

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }

        if ($smart_playlist->private == 'Y') {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.follow_smartlist_private_playlist_lbl")
                //'You can not follow private smart playlist.'
            ]);
        }

        //check if this board is your own board
        if ($smart_playlist->user_id == $user->id) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.follow_smartlist_own_playlist_lbl")
                //'You can not follow your own smart playlist.'
            ]);
        }

        //Check if this smart playlist already follow
        $board_info = $user->following_smart_playlist()->find($request->board_id, ['id']);

        if ($board_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.already_follow_smartlist_lbl")
                //'You have already followed this smart playlist.'
            ]);
        }

        if (!$board_info) {
            //First we check board user is follow or not
            //if user is not follow than first we follow that user
            $follow_user = $user->following()->find($smart_playlist->user_id);

            $userData = User::find($smart_playlist->user_id);
            $tokens = [];
            if ($userData) {
                $tokens = $userData->pushIds()->where('push_ids.status', 1)->pluck('token')->all();
            }

            $message = $user->notification_format_name . ' started following your smart playlist ' . $smart_playlist->title;
            if (!$follow_user) {
                //Sync data with user
                $user->following()->attach($smart_playlist->user_id);

                //Insert into Feeds Table
                (new Feed($user))->add(trim($smart_playlist->user_id), Feed::FOLLOW_USER);

                $message = $user->notification_format_name . ' started following you and your smart playlist ' . $smart_playlist->title;
            }

            // add user into board
            $user->following_smart_playlist()->attach($request->board_id);

            // also added into middle table
            $find_my_list = $user->my_lists()->find($request->board_id);
            if (!$find_my_list) {
                $user->my_lists()->attach($smart_playlist->id, ['board_type' => 0]);
            }

            //Update Follower Count
            try {
                $smart_playlist->timestamps = false;
                $smart_playlist->update(['followers_count' => \DB::raw('followers_count + 1')]);
                //SmartPlaylist::where('id', $request->board_id)->update(['followers_count' => \DB::raw('followers_count + 1')]);
            } catch(\Exception $ex) {}
            
            //Insert into Feed Table
            (new Feed($user))->add($request->board_id, Feed::FOLLOW_SMARTLIST);
            
            //Send Push Notification to the owner of the board
            if (count($tokens) > 0) {
                $title = $message;
                $description = '';
                $icon = '';
                $data = [
                    'title' => 'Follow Smart Playlist',
                    'username' => $user->username,
                    'type' => 'FSLU',
                    'desc' => $message,
                    'tray_icon' => ''
                ];
                //dispatch(new IndividualPushNotification($tokens, $title, $description, $icon, $data))->onQueue('single_push_notification');
                //FireBase::sendFireBaseNotification($tokens, $title, $description, $icon, $data);
            }
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.follow_smartlist_success_lbl") . $smart_playlist->title
            //'You are now following ' . $smart_playlist->title
        ]);
    }

    /**
     * This function is use for un-follow Smart Playlist
     * @param Request $request
     * @return type
     */
    public function unfollow_smartlist(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl")
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //check if this board already unfollow
        $board_info = $user->following_smart_playlist()->find($request->board_id);

        if (!$board_info) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.unfollow_smartlist_already_unfollow_success_lbl")
                //'You have already unfollowed this smart playlist.'
            ]);
        }

        if ($board_info) {
            // remove user from board
            $user->following_smart_playlist()->detach($request->board_id);

            // also added into middle table
            $find_my_list = $user->my_lists()->find($request->board_id);
            if ($find_my_list) {
                $user->my_lists()->detach($request->board_id);
            }

            //Update Follower Count
            try {
                $board_info->timestamps = false;
                $board_info->update(['followers_count' => \DB::raw('followers_count - 1')]);
                //SmartPlaylist::where('id', $request->board_id)->update(['followers_count' => \DB::raw('followers_count - 1')]);
            } catch(\Exception $ex) {}

            //Also remove from feed table
            $boardFeedData = FeedRepo::where('type', 'Follow Smartlist')
                ->where('typeable_type', 'App\\Models\\User')
                ->where('typeable_id', $user->id)
                ->where('data', 'LIKE', '%"' . $request->board_id . '"%');
            if ($boardFeedData->count()) {
                $boardFeedData->delete();
            }
        }

        return response()->api([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".SmartPlaylist.unfollow_smartlist_success_lbl")
            //'You have been successfully unfollow '.$board_info->title
        ]);
    }

    /**
     * Smart Smart Playlist Shows
     *
     * @param Request $request
     */
    public function smart_playlist_shows(Request $request)
    {
        $user = Auth::guard('api')->user();

        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Board
        $smart_playlist = SmartPlaylist::where('id', $request->board_id)->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }

        //All Board Shows
        $all_board_shows = $smart_playlist->shows()->published()->orderBy('id', 'DESC')->paginate(10, ['id', 'title', 'image']);
        //dd($all_board_shows->toArray());

        $data = [];
        foreach ($all_board_shows as $show) {
            $data[] = [
                'id' => $show->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($show->title))),
                'image' => !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png'),
                'no_of_episode' => 0,//$show->episodes()->count(),
            ];
        }

        $message = '';
        if (count($data) == 0) {
            $message = config('language.' . $this->getLocale() . ".Common.no_podcast_found_lbl");
        }

        return response()->api([
            'status' => true,
            'message' => $message,
            'data' => [
                'total' => $all_board_shows->total(),
                'per_page' => $all_board_shows->perPage(),
                'pages' => ceil($all_board_shows->total() / $all_board_shows->perPage()),
                'items' => $data
            ]
        ]);
    }

    /**
    * This function is use for get all user subscribe shows
    * @param Request $request
    * @return response
    */
    public function getMySubShowsforSmartPlaylist(Request $request)
    {
        $user = Auth::guard('api')->user();
        //check if user exist or not
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        $validator = Validator::make($request->all(), [
            'board_id' => 'required'
        ], [
            'board_id.required' => config('language.' . $this->getLocale() . ".SmartPlaylist.missing_parameter_id_lbl"),
        ]);

        if ($validator->fails()) {
            return response()->api([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        //Select Smart Playlist
        $smart_playlist = SmartPlaylist::where('id', $request->board_id)->first();

        if (!$smart_playlist) {
            return response()->api([
                'status' => false,
                'message' => config('language.' . $this->getLocale() . ".Common.no_smart_playlist_found_lbl")
            ]);
        }

        $smart_list_shows = $smart_playlist->shows->pluck('id')->all();        

        // $user_shows = $user->shows()->whereNotIn('id',$smart_list_shows)
        //     ->withCount(['episodes' => function ($qry) {
        //         $qry->published();
        //     }])->get(['id', 'title', 'image']);
        $user_shows = $user->shows()->whereNotIn('id',$smart_list_shows)->get(['id', 'title', 'image']);
        //dd($user_shows);

        $data = [];
        foreach ($user_shows as $key => $values) {
            $data[] = [
                'id' => $values->id,
                'title' => trim(str_replace("\n", '', html_entity_decode($values->title))),
                'image' => !empty($values->image) ? $values->getWSImage(200) : asset('uploads/default/show.png'),
                'no_of_episode' => 0,//$values->episodes_count,
            ];
        }

        return response()->api([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Generate Collage Image for smart Smart Playlist
     * @param type $show
     * @return string
     */
    protected function generateCollageImage($show_ids)
    {
        if (count($show_ids) > 0) {
            $show_images = Show::whereIn('id', $show_ids)->take(4)->pluck('image')->all();

            $images = [];
            foreach ($show_images as $epp_image) {
                $images[] = config('config.s3_url') . '/shows/' . $epp_image;
            }

            if (count($images) > 1) {
                $images = array_pad($images, 4, Image::canvas(600, 600, '#C0C0C0'));
            }

            if (count($images)) {
                $imageArray = [];
                foreach ($images as $image) {
                    try {
                        $imageArray[] = Image::make($image);
                    } catch (\Exception $e) {
                        continue;
                    }
                }

                if (count($imageArray) > 0) {
                    $collage = Collage::make(400, 400)->from($imageArray);
                    $filename = date('YmdHis') . rand(100000, 999999) . '.png';
                    $collage->save('uploads/smart_playlists/' . $filename, 100);

                    Storage::disk('s3')->put('smart_playlists/' . $filename, file_get_contents(asset('uploads/smart_playlists/' . $filename)));

                    unlink(public_path('uploads/smart_playlists/' . $filename));

                    return $filename;
                }

                return '';
            }
        }

        return '';
    }

    /**
     * Smart Playlists 
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function smartPlaylists(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        if (!$user) {
            $response = [
                'status' => false,
                'code' => 401,
                'message' => config('language.' . $this->getLocale() . ".Common.auth_error")
            ];
            return $this->sendAuthErrorResponse($response, 401);
        }

        try {
            $spl = new SmartPlaylistClass('api', $user);
            return $spl->smartplaylist();
        } catch (\Exception $ex) {
            return $this->sendExceptionError($ex->getCode(), $ex->getMessage());
        }
    }
}
