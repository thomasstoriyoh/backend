<?php

namespace App\Http\Controllers\Api\new_version\V8;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Show;
use App\Models\Episode;
use App\Models\Board;
use App\Models\SmartPlaylist;

use App\Models\Network;
use App\Models\Category;

use Carbon\Carbon;

class StoriyohInsights extends Controller
{
    protected $to_date;
    protected $from_date;

    public function __construct() {
        $this->to_date = Carbon::now()->addMonths(-1)->toDateString();
        $this->from_date = Carbon::today()->toDateString();        
    }    

    public function users_count(Request $request)
    {
        // dd($todate, $from_date);
        $shows_count = Show::published()->count();
        $episode_count = Episode::published()->count();
        $total_registered_users = User::count();
        
        $array_Data = [];
        $array_Data = [
            'total_registered_users' => $total_registered_users,
            'total_show' => $shows_count,
            'total_episode' => $episode_count
        ];

        return response()->api([
            $array_Data
        ]);

    }


    public function podcasts_subscribed_show(Request $request)
    {
        // Top 100 Subscribed Shows
        // $show = Show::where('no_of_subscribers','!=','0')->take(100)->orderBy('no_of_subscribers', 'desc')->get(['id', 'title', 'no_of_subscribers']);

        $show = Show::where('no_of_subscribers', '>', 0)
                        // where('no_of_subscribers','!=','0')
                    ->orderBy('no_of_subscribers', 'desc')
                    ->paginate(15, ['id', 'title', 'no_of_subscribers']);
        
        $array_show = [];
        foreach ($show as $key => $shows) {
            $array_show[] = [
                'id' => $shows->id,
                'title' => $shows->title,
                'no_of_subscribers' => $shows->no_of_subscribers
            ];
        }

        $array_Data = [];
        $array_Data = [
            'subscribed_show' => $array_show,
            'pagination' => $show

        ];

        return response()->api([
                $array_Data
            ]);
    }

    // subscribed_Networks
    public function podcast_subscribe_networks(Request $request)
    {
        $network = \DB::table("shows")
                ->where('network_id', '!=', 0)
                ->select(\DB::raw('network_id, COUNT(network_id) AS count_network'))
                ->orderBy(\DB::raw('count_network'), 'desc')
                ->groupBy(\DB::raw('network_id'))
                ->paginate(15);

        // return response()->json($network);

        $array_network = [];
        foreach ($network as $key => $networks) {
            $network_data = Network::where('id', $networks->network_id)->first(['title']);
            if ($network_data) {
                $array_network[] = [
                    'id' => $networks->network_id,
                    'title' => $network_data->title,
                    'count_network' => $networks->count_network,
                ];
            }
        }

        $array_Data = [];
        $array_Data = [
            'subscribed_network' => $array_network,
            'pagination' => $network
                
        ];

        return response()->api([
                $array_Data
            ]);
        // return response()->json($array_Data); 

        
        // Old code without pagination
        // Top Subscribed Networks
        // $network = \DB::select("SELECT `network_id`, COUNT(`network_id`) AS `count_network` FROM `tbl_shows` WHERE `network_id` != '0' GROUP By `network_id` ORDER BY `network_id` DESC LIMIT 10");
        // $array_network = [];
        // foreach ($network as $key => $networks) {
        //     $network_data = Network::where('id', $networks->network_id)->first(['title']);
        //     $array_network[] = [
        //         'id' => $networks->network_id,
        //         'title' => $network_data->title,
        //         'count_network' => $networks->count_network,
        //     ];
        // }

        // $array_Data = [];
        // $array_Data = [
        //     'subscribed_network' => $array_network,
        // ];

        // return response()->api([
        //         $array_Data
        //     ]);
    }

    // Subscribed_Categories
    public function podcast_subscribe_categories(Request $request)
    {
        // Top Subscribed Categories of Interest
        // return $cat_interest = \DB::select("SELECT `category_id`, COUNT(`user_id`) AS `count_users` FROM `tbl_category_user`  GROUP BY `category_id` ORDER BY `count_users` DESC LIMIT 10");

        $cat_interest = \DB::table('category_user')
                        ->select(\DB::raw('category_id, COUNT(user_id) AS count_users' ))
                        ->orderBy(\DB::raw('COUNT(user_id)'), 'desc')
                        ->groupBy(\DB::raw('category_id'))
                        ->paginate(15)
                        ;        

        $array_cat_interest = [];
        foreach ($cat_interest as $key => $cat_interests) {
            $category_data = Category::where('id', $cat_interests->category_id)->first(['title']);
            $array_cat_interest[] = [
                'id' => $cat_interests->category_id,
                'title' => $category_data->title,
                'count_users' => $cat_interests->count_users
            ];
        }
        // dd($array_cat_interest);

        $array_Data = [];
        $array_Data = [
            'subscribed_category' => $array_cat_interest,
            'pagination' => $cat_interest
        ];

        return response()->api([
                $array_Data
            ]);
    }

    // Podcast Shows in Playlist
    public function podcast_shows_in_playlist(Request $request)
    {
        // Top 10 Shows in Playlists

        // $show_in_playlist = Show::where('playlist_count','!=','0')->take(100)->orderBy('playlist_count', 'desc')->get(['id', 'title', 'playlist_count']); // old code without pagination

        $show_in_playlist = Show::where('playlist_count','>','0')
                        ->orderBy('playlist_count', 'desc')
                        ->paginate(15,['id', 'title', 'playlist_count']);
 
        $array_show_playlist = [];
        foreach ($show_in_playlist as $key => $shows) {
            $array_show_playlist[] = [
                'id' => $shows->id,
                'title' => $shows->title,
                'count_show' => $shows->playlist_count
            ];
        }
        // dd($show_in_playlist);
        $array_Data = [];
        $array_Data = [
            'shows_in_playlist' => $array_show_playlist,
            'pagination' => $show_in_playlist
        ];

        return response()->api([
                $array_Data
            ]);
    }

    // Episode Listened
    public function episode_listened(Request $request)
    {
        if ($request->date == '') {
            $to_date = $this->to_date;
            $from_date = $this->from_date;
        }
        else
        {
            $dates = explode(",",$request->date);
            $to_date = $dates[0];
            $from_date = $dates[1];
        }
        //Top 100 Listened Episodes
        // $episode = \DB::select("SELECT `episode_id`, COUNT(`user_id`) AS `listen_count` FROM `tbl_user_stream_data` WHERE `created_at` BETWEEN '$to_date' AND '$from_date' GROUP BY `episode_id` ORDER BY COUNT(`user_id`) DESC LIMIT 100"); // old code without pagination

        $episode = \DB::table('user_stream_data')
                    // ->whereBetween('created_at', [$to_date, $from_date])
                    ->where('created_at', '>=', $to_date . " 00:00:00")
                    ->where('created_at', '<=', $from_date . " 23:59:59")
                    ->select(\DB::raw('episode_id, COUNT(user_id) AS listen_count'))
                    ->orderBy(\DB::raw('listen_count'), 'desc')
                    ->groupBy(\DB::raw('episode_id'))
                    ->paginate(15)
                    ->appends(request()->query())
                    ;

        // $episode->withPath('custom/url');        

        $array_episode = [];
        foreach ($episode as $key => $episodes) {
            $episode_data = Episode::where('id', $episodes->episode_id)->first(['title']);
            // dd($show_data);
            $array_episode[] = [
                'id' => $episodes->episode_id,
                'title' => $episode_data->title,
                'listen_count' => $episodes->listen_count
            ];
        }

        $array_Data = [];
        $array_Data = [
            'listened_episode' => $array_episode,
            'pagination' => $episode
        ];

        // return response()->json($array_Data);

        return response()->api([
                $array_Data
            ]);
    }

    // Episode Playlist
    public function episode_playlist(Request $request)
    {
        //Top 100 episode in playlist 
        
        // $episode_in_playlist = Episode::where('playlist_count','!=','0')->take(100)->orderBy('playlist_count', 'desc')->get(['id', 'title', 'playlist_count']);
            
        // $array_episode_in_playlist = [];
        // foreach ($episode_in_playlist as $key => $episode_in_playlists) {
        //     $array_episode_in_playlist[] = [
        //         'id' => $episode_in_playlists->id,
        //         'title' => $episode_in_playlists->title,
        //         'count_episode' => $episode_in_playlists->playlist_count
        //     ];
        // }

        // Top 100 Episodes in Playlists
        // $episode_in_playlist = \DB::select("SELECT `episode_id`, COUNT(`episode_id`) AS `count_episode` FROM `tbl_board_episode` GROUP BY `episode_id` ORDER BY `count_episode` DESC LIMIT 100"); // old code without pagination

        $episode_in_playlist = \DB::table('board_episode')
                        ->select(\DB::raw('episode_id, COUNT(episode_id) AS count_episode'))
                        ->orderBy(\DB::raw('count_episode'), 'desc')
                        ->groupBy(\DB::raw('episode_id'))
                        ->paginate(15);

        $array_episode_in_playlist = [];
        foreach ($episode_in_playlist as $key => $episode_in_playlists) {
            $episode_data = Episode::where('id', $episode_in_playlists->episode_id)->first(['title']);
            $array_episode_in_playlist[] = [
                'id' => $episode_in_playlists->episode_id,
                'title' => $episode_data->title,
                'count_episode' => $episode_in_playlists->count_episode
            ];
        }

        $array_Data = [];
        $array_Data = [
            'episode_in_playlist' => $array_episode_in_playlist,
            'pagination' => $episode_in_playlist
        ];

        return response()->api([
                $array_Data
            ]);
    }

    // Episode Downloaded
    public function episode_downloaded(Request $request)
    {
        if ($request->date == '') {
            $to_date = $this->to_date;
            $from_date = $this->from_date;
        }
        else
        {
            $dates = explode(",",$request->date);
            $to_date = $dates[0];
            $from_date = $dates[1];
        }

        // Top 100 Episodes Downloaded (Currently)
        // $episode_in_download = \DB::select("SELECT `episode_id`, COUNT(`episode_id`) AS `count_episode` FROM `tbl_user_download_data` WHERE `created_at` BETWEEN '$to_date' AND '$from_date' GROUP BY `episode_id` ORDER BY `count_episode` DESC LIMIT 100"); // old code without pagination 

        $episode_in_download = \DB::table('user_download_data')
                            // ->whereBetween('created_at', [$to_date, $from_date])
                            ->where('created_at', '>=', $to_date . " 00:00:00")
                            ->where('created_at', '<=', $from_date . " 23:59:59")
                            ->select(\DB::raw('episode_id, COUNT(episode_id) AS count_episode'))
                            ->orderBy(\DB::raw('count_episode'), 'desc')
                            ->groupBy(\DB::raw('episode_id'))
                            ->paginate(15)
                            ->appends(request()->query());

        $array_episode_in_downlaod = [];
        foreach ($episode_in_download as $key => $episode_in_downloads) {
            $episode_download = Episode::where('id', $episode_in_downloads->episode_id)->first(['title']);
            $array_episode_in_downlaod[] = [
                'id' => $episode_in_downloads->episode_id,
                'title' => $episode_download->title,
                'count_episode' => $episode_in_downloads->count_episode,
            ];
        }

        $array_Data = [];
        $array_Data = [
            'episode_in_download' => $array_episode_in_downlaod,
            'pagination' => $episode_in_download
        ];

        return response()->api([
                $array_Data
            ]);
    }

    //dashboard_linegraph
    public function dashboard_linegraph(Request $request)
    {
        if ($request->date == '') {
            $to_date = $this->to_date;
            $from_date = $this->from_date;
        } else {
            $dates = explode(",",$request->date);
            $to_date = $dates[0];
            $from_date = $dates[1];
        }
       
        $users = \DB::table('users')
                ->where('created_at', '>=', $to_date . " 00:00:00")
                ->where('created_at', '<=', $from_date . " 23:59:59")
                ->select(\DB::raw('DATE(created_at) AS created_date'), \DB::raw("SUM(CASE 
                WHEN user_type = '0' THEN 1 ELSE 0 END) AS guest_count"),  \DB::raw("SUM(CASE 
                WHEN user_type = '1' THEN 1 ELSE 0 END) AS register_count"))
                ->groupBy(\DB::raw('created_date'))
                ->get();

        $array_users = [];
        foreach ($users as $key => $user) {
            $array_users[] = [
                'guest_count' => $user->guest_count,
                'register_count' => $user->register_count,
                'dates' => $user->created_date,
            ];
        }

        $array_Data = [];
        $array_Data = [
            'linegraph_users' => $array_users
        ];

        return response()->api([
                $array_Data
            ]);
    }

    // dashboard_bargraph
    public function dashboard_bargraph(Request $request)
    {
        if ($request->date == '') {
            $to_date = $this->to_date;
            $from_date = $this->from_date;
        }
        else
        {
            $dates = explode(",",$request->date);
            $to_date = $dates[0];
            $from_date = $dates[1];
        }

        // users data
        // $query = User::whereBetween('created_at', [$to_date, $from_date])->get(['provider']);
        $query = User::where('created_at', '>=', $to_date . " 00:00:00")
        ->where('created_at', '<=', $from_date . " 23:59:59")->get(['provider']);
        $provider = [];
        $provider_facebook = 0;
        $provider_google = 0;
        $provider_direct = 0;
        $provider_twitter = 0;
        foreach ($query as $key => $item) {
            if ($item->provider == 'Facebook') {
                $provider_facebook++;
            }
            elseif ($item->provider == 'Google') {
                $provider_google++;
            }
            elseif ($item->provider == 'Direct') {
                $provider_direct++;
            }
            elseif ($item->provider == 'Twitter') {
                $provider_twitter++;
            }
        }

        $provider = ['Facebook' => $provider_facebook, 'Google' => $provider_google, 'Direct' => $provider_direct, 'Twitter' => $provider_twitter];
        $array_Data = [];
        $array_Data = [
            'registrations_by_provider' => $provider,
        ];
        // dd($array_Data);
        return response()->api([
                $array_Data
            ]);
    }

    // dashboard_doughnut_graph
    public function dashboard_doughnut_graph(Request $request)
    {
        if ($request->date == '') {
            $to_date = $this->to_date;
            $from_date = $this->from_date;
        }
        else
        {
            $dates = explode(",",$request->date);
            $to_date = $dates[0];
            $from_date = $dates[1];
        }

        // $platform = \DB::select("SELECT `registered_platform`, COUNT(`registered_platform`) AS registered_platform_count FROM `tbl_users` WHERE `registered_platform` != 'null' AND `created_at` BETWEEN '$to_date' AND '$from_date' GROUP BY `registered_platform`");

        $platform = \DB::table('users')
                    ->select(\DB::raw('registered_platform, COUNT(`registered_platform`) AS registered_platform_count'))
                    ->where('created_at', '>=', $to_date . " 00:00:00")
                    ->where('created_at', '<=', $from_date . " 23:59:59")
                    ->where('registered_platform', '!=', null)
                    ->groupBy(\DB::raw('registered_platform'))
                    ->get();

        $array_platform = [];
        $array_platform['Web'] = 0;
        foreach ($platform as $key => $platforms) {
            // dd(ucfirst($platforms->registered_platform), $platforms->registered_platform_count);
            $array_platform[ucfirst($platforms->registered_platform)] = $platforms->registered_platform_count
            ;
        }
        $array_Data = [];
        $array_Data = [
            'registrations_by_platform' => $array_platform,
        ];
        // dd($array_Data);
        return response()->api([
                $array_Data
            ]);    
    }

    // dashboard_top_listing_table
    public function dashboard_top_listing_table(Request $request)
    {
        // dd(1);
        // if ($request->date == '') {
        //     $to_date = $this->to_date;
        //     $from_date = $this->from_date;
        // }
        // else
        // {
        //     $dates = explode(",",$request->date);
        //     $to_date = $dates[0];
        //     $from_date = $dates[1];
        // }

        //Top 10 Subscribed Shows
        $show = Show::where('no_of_subscribers','!=','0')->take(10)->orderBy('no_of_subscribers', 'desc')->get(['id', 'title', 'no_of_subscribers']);
 
        $array_show = [];
        foreach ($show as $key => $shows) {
            $array_show[] = [
                'id' => $shows->id,
                'title' => $shows->title,
                'no_of_subscribers' => $shows->no_of_subscribers
            ];
        }
        // dd($array_show);

        //Top 10 Listened Episodes

        // $episode = \DB::select("SELECT `episode_id`, COUNT(`user_id`) AS `listen_count` FROM `tbl_user_stream_data` GROUP BY `episode_id` ORDER BY listen_count DESC LIMIT 10");

        $episode = \DB::table('user_stream_data')
                    ->select(\DB::raw('episode_id, COUNT(user_id) AS listen_count'))
                    ->groupBy(\DB::raw('episode_id'))
                    ->orderBy(\DB::raw('listen_count'), 'desc')
                    ->take(10)
                    ->get();
                        
        $array_episode = [];
        foreach ($episode as $key => $episodes) {
            $episode_data = Episode::where('id', $episodes->episode_id)->first(['title']);
            if ($episode_data) {
                $array_episode[] = [
                    'id' => $episodes->episode_id,
                    'title' => $episode_data->title,
                    'listen_count' => $episodes->listen_count
                ];
            }
        }

        // $episode = Episode::take(10)->orderBy('listen_count', 'desc')->get(['id', 'title', 'listen_count']);

        // $array_episode = [];
        // foreach ($episode as $key => $episodes) {
        //     $array_episode[] = [
        //         'id' => $episodes->id,
        //         'title' => $episodes->title,
        //         'listen_count' => $episodes->listen_count
        //     ];
        // }
        // dd($array_episode);

        // Top 10 Followed Playlists
        $board = Board::take(10)->orderBy('followers_count', 'desc')->get(['id', 'title', 'followers_count']);
        $array_board = [];
        foreach ($board as $key => $boards) {
            $array_board[] = [
                'id' => $boards->id,
                'title' => $boards->title,
                'followers_count' => $boards->followers_count
            ];
        }
        // dd($array_board);

        // Top 10 Followed Smart Playlists
        $smart_playlist = SmartPlaylist::take(10)->orderBy('followers_count', 'desc')->get(['id', 'title', 'followers_count']);
        $array_smart_playlist = [];
        foreach ($smart_playlist as $key => $smart_playlists) {
            $array_smart_playlist[] = [
                'id' => $smart_playlists->id,
                'title' => $smart_playlists->title,
                'followers_count' => $smart_playlists->followers_count
            ];
        }
        // dd($array_smart_playlist);

         // Registrations by Location
        $locations = \DB::table('users')
                ->select(\DB::raw('country, COUNT(country) AS country_count'))
                ->where('country' , '!=', '')
                ->where('country', '!=', 'null')
                ->where('country', '!=', null)
                ->groupBy(\DB::raw('country'))
                ->orderBy(\DB::raw('country_count'), 'desc')
                ->take(10)
                ->get();

        $array_location = [];
        foreach ($locations as $key => $locations) {
            $array_location[] = [
                'country' => $locations->country,
                'country_count' => $locations->country_count
            ];
        }
        // dd($array_location);

        $array_Data = [];
        $array_Data = [
            'top_10_subscribed_shows' => $array_show,
            'top_10_listened_episodes' => $array_episode,
            'top_10_followed_playlists' => $array_board,
            'top_10_followed_smart_playlists' => $array_smart_playlist,
            'registrations_by_location' => $array_location,

        ];
        // dd($array_Data);
        return response()->api([
                $array_Data
            ]);
    }
}
