<?php

namespace App\Http\Controllers\Api\new_version\Website\Auth;

class Facebook implements HasAvatar
{
    /**
     * @param $id
     * This id function for facebook
     * @return string
     */
    public function getAvatar($id)
    {
        return 'http://graph.facebook.com/' . $id . '/picture?type=large';
    }
}
