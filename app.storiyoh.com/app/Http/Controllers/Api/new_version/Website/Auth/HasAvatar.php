<?php

namespace App\Http\Controllers\Api\new_version\Website\Auth;

interface HasAvatar
{
    /**
     * @param $id
     * @return string
     */
    public function getAvatar($id);
}
