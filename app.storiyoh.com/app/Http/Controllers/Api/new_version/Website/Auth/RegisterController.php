<?php
namespace App\Http\Controllers\Api\new_version\Website\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Traits\ApiDataTrait;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Models\UserSocial;
use App\Traits\GeneratePassportToken;
use App\Traits\PassportToken;
use Laravel\Socialite\Facades\Socialite;

/** Topic  Subscription Code Start */
use App\Jobs\FirebaseTopicSubscription;
/** Topic  Subscription Code End */

use App\Traits\ResponseFormat;
use App\ClassesV3\V3_3\IP2Location;

use App\Mail\VerifyUserMail;

class RegisterController extends Controller
{

    //use ApiDataTrait, GeneratePassportToken, PassportToken, ResponseFormat;    
    use  ResponseFormat;    

    
    /**
     * Register the user and send email to the user for verification.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Foundation\Validation\ValidationException
     */
    public function register(Request $request)
    {
        // $type = [
        //     'Facebook' => Facebook::class, 
        //     'Google' => Google::class
        // ];

        // if ($request->provider == 'Direct') {
        //     $validator = $this->validator($request->all());
        // } else {
        //     $validator = $this->social_validator($request->all());
        // }

        $this->validator($request->all())->validate();

        //$validator = $this->validator($request->all());

        // if ($validator->fails()) {
        //     return response()->json([
        //         'status' => false,                
        //         'errors' => $validator->errors()
        //     ]);
        // }

        //Check if this email exist and verified
        $userEmailInfo = User::where('email', $request->email)->first();
        //dd($userEmailInfo->isVerified(), $userEmailInfo->toArray());

        if ($userEmailInfo) {
            if ($userEmailInfo->verified == 'Disabled') {
                return response()->json([
                    'status' => false,                    
                    'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
                ]);
            }

            if ($userEmailInfo) {
                return response()->json([
                    'status' => false,                    
                    'message' => config('language.' . $this->getLocale() . ".Auth.register_email_exist_lbl")
                ]);
            }

        }

        // $password = '';
        // if ($request->provider == 'Direct') {
        //     $password = Hash::make($request->password);
        // } else {
        //     $temp_password = str_random(8);
        //     $password = Hash::make($temp_password);
        // }

        $password = Hash::make($request->password);

        $user_type = 2; // register       
        if (! is_null($request->guest_username)) {
            $user_type = 2;
        }
        
        //Ip2Location code start
        $city = null;
        $state = null;
        $country = null;
        if (!empty($request->ip_address)) {
            $responseData = new IP2Location('api');
            $location = $responseData->get_location();
            
            if (count($location['data']) > 0) {
                $city = $location['data']['city_name'];
                $state = $location['data']['region_name'];
                $country = $location['data']['country_name'];
            }
        }
        //Ip2Location code end

        $user_data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'provider' => 'Direct',
            'provider_id' => $request->provider_id,
            'image' => $request->image,
            'password' => $password,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'admin_status' => 'Approved',
            'oauth_enabled' => 1,
            'registered_platform' => $request->platform,
            'user_type' => $user_type
        ];

        if (is_null($request->guest_username)) {
            $username = uniqid();        
            $username .= $this->getRandomNumber(10);
            $username = "drt_".$username;

            $user_data['username'] = $username;
        }

        if(! is_null($request->guest_username)) {
            $user_data['email'] = $request->email;
            $user = \App\Models\User::firstOrNew(['username' => trim($request->guest_username)]);
            $user->fill($user_data)->save();            
        } else {
            $user = \App\Models\User::firstOrNew(['email' => $request->email]);
            $user->fill($user_data)->save();
        }
        
        try{
            \Mail::to($user->email)->send(new VerifyUserMail($user));        
        } 
        catch (\Exception $ex) 
        {            
            Log::error($ex->getMessage());
        }            
        

        //Update Social info
        UserSocial::firstOrCreate(['user_id' => $user->id, 'provider' => 'Direct']);


        return response()->json([
            'status' => true,
            'message' => config('language.' . $this->getLocale() . ".Auth.register_web_thank_you_message"),                
            'data' => [
                'id' => $user->id,
                'email' => $user->email
            ]
        ]);                    
        
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [            
            'first_name' => 'required|max:255',
            //'email' => 'required|email|max:255|unique:users,email',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:8'
        ], [
            'provider.required' => config('language.' . $this->getLocale() . ".Common.provider_lbl"),
            'first_name.required' => config('language.' . $this->getLocale() . ".Auth.register_name_lbl"),
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl"),
            //'email.unique' => config('language.' . $this->getLocale() . ".Auth.register_email_unique_lbl"),
            'password.min' => config('language.' . $this->getLocale() . ".Auth.register_password_min_lbl"),
            'password.required' => config('language.' . $this->getLocale() . ".Auth.register_password_lbl"),
            'password.confirmed' => config('language.' . $this->getLocale() . ".Auth.register_password_confirm_lbl"),
        ]);
    }


    /**
     * This function is use for verify email otp
     * @param User $user
    */
    public function verifyEmailOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'otp' => 'required'
        ], [
            'email.required' => config('language.' . $this->getLocale() . ".Auth.register_email_lbl"),
            'otp.required' => config('language.' . $this->getLocale() . ".Auth.register_otp_lbl"),
        ]);
        
        $validator->validate();
        
        // if ($validator->fails()) {
        //     return response()->api([
        //         'status' => false,
        //         'navigate' => 'verify_email',
        //         'message' => $validator->errors()->first()
        //     ]);
        // }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->api([
                'status' => false,
                'navigate' => 'verify_email',
                'message' => config('language.' . $this->getLocale() . ".Common.user_not_found_lbl")
            ]);
        }

        if ($user) {
            if (OTP::verify($request->otp, $user->email)) {
                
                $user->forceFill(['verified' => 'Verified', "last_access_time" => @date('Y-m-d H:i:s'), "last_access_time_social" => @date('Y-m-d H:i:s')])->save();

                //Added default follower Storiyoh, Rohit and Rahul
                try {
                    $user->following()->attach(config('config.storiyoh_default_following_ids'));
                } catch (\Exception $ex) {
                }

                return $this->login_by_id($request);
            } else {
                return response()->api([
                    'status' => false,
                    'navigate' => 'verify_email',
                    'message' => config('language.' . $this->getLocale() . ".Auth.register_invalid_otp_lbl")
                ]);
            }
        }
    }


    /**
     * This function is use for
     * getting random number
     */
    protected function getRandomNumber($n)
    { 
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
    
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
    
        return $randomString; 
    }

}