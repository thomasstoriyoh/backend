<?php

namespace App\Http\Controllers\Api\new_version\Website\Auth;

use App\Packages\OTP\OTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Traits\ApiDataTrait;
use Illuminate\Support\Facades\Log;
//use Illuminate\Support\Facades\Storage;
use App\Models\UserSocial;
use App\Traits\GeneratePassportToken;
use App\Traits\PassportToken;
use Laravel\Socialite\Facades\Socialite;

/** Topic  Subscription Code Start */
//use App\Jobs\FirebaseTopicSubscription;
/** Topic  Subscription Code End */

use App\Traits\ResponseFormat;
use App\ClassesV3\V3_3\IP2Location;

use App\Mail\VerifyUserMail;

class WebLoginController extends Controller
{
    use ApiDataTrait, GeneratePassportToken, PassportToken, ResponseFormat;

    /**
     * This function is use for login
     * @param Request $request
     * @return mixed values
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ], [
            'username.required' => config('language.' . $this->getLocale() . ".Auth.login_username_lbl"),
            'password.required' => config('language.' . $this->getLocale() . ".Auth.login_password_lbl"),
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'navigate' => 'login',
                'message' => $validator->errors()->first()
            ];

            return response()->json($response);
        }

        $user = User::where('username', $request->username)->orWhere('email', $request->username)->first([
            'id', 'provider_id', 'first_name', 'image', 'email', 'username', 'type', 'country_id', 
            'password', 'api_token', 'verified', 'admin_status', 'discreet', 'city', 'state', 'country', 
            'oauth_enabled', 'user_type', 'dashboard_status', 'validation_key'
        ]);

        if (!$user) {
            return response()->json([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_invalid_lbl")
            ]);
        }

        if (!Hash::check($request->password, $user->password)) {
            return response()->json([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_invalid_lbl")
            ]);
        }

        if ($user->verified == 'Disabled') {
            return response()->json([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
            ]);
        }

        if (!$user->isVerified()) {            
            try{
                \Mail::to($user->email)->send(new VerifyUserMail($user));        
            } 
            catch (\Exception $ex) 
            {            
                Log::error($ex->getMessage());
            }            
    
            //$this->sendVerificationEmail($user);

            return response()->json([
                'status' => false,
                'navigate' => 'verify_email',
                'email' => $user->email,
                'message' => config('language.' . $this->getLocale() . ".Auth.login_account_verified_lbl")
            ]);
        }

        if (!$user->isApproved()) {
            return response()->json([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_approved_lbl")
            ]);
        }

        UserSocial::firstOrCreate(['user_id' => $user->id, 'provider' => 'Direct']);

        //If oauth_enabled is false then
        //we update oauth_enabled true
        if ($user->oauth_enabled == 0) {
            $user->update(['oauth_enabled' => 1]);
        }

        //update last login type
        $user->update(['last_login_type' => 'Direct']);

        /* Creating Profile Access Token */
        //$access_token = $this->getBearerTokenByUser($user, config('config.client_id'), false);
        $access_token = $this->generateAccessToken($request, $request->username, $request->password);

        if (!array_key_exists('access_token', $access_token)) {
            return response()->json([
                'status' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Common.access_token_lbl")
            ]);
        }

        $imageData = !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png');
        
        $city = $user->city ? $user->city : '';
        $state = $user->state ? $user->state : '';
        $country = $user->country ? $user->country : '';
        
        $data = collect($user)
            ->put('city', $city)
            ->put('state', $state)
            ->put('country', $country)
            ->put('full_name', $user->full_name)
            ->put('image', $imageData)
            ->put('discreet', $user->discreet)
            ->put('api_token', $user->api_token)
            ->put('access_token', @$access_token['access_token'])
            ->put('refresh_token', @$access_token['refresh_token'])
            ->put('expires_in', @$access_token['expires_in']);

        //$restore_data = $this->restore_user_data($user);        

        /** Topic  Subscription Code Start */
        //Subscribe all podcast for specific user
        $allShows = $user->subscribers()->wherePivot('notify', 1)->pluck('id')->all();
        // if(count($allShows) > 0) {
        //     try {
        //         $uniqueShows = array_unique($allShows);
        //         $splitShows = array_chunk($uniqueShows, 10);
        //         foreach ($splitShows as $item) {
        //             dispatch(new FirebaseTopicSubscription($item, $user->id, 'subscribe_on_login', [$request->device_token]))->onQueue('subscribe-topic');
        //         }
        //     } catch(\Exception $ex) {}            
        // }
        /** Topic  Subscription Code End */

        $navigate = "feed";
        
        // else {
        //     if ($user->user_type == 2 && $user->dashboard_status == 0) {
        //         $navigate = "recommended_user";
        //     }
        // }

        return response()->json([
            'status' => true,
            'navigate' => $navigate,
            'data' => $data,
            //'category_notification_data' => $category_notification_data,
            'user_type' => $user->user_type,
            //'dashboard_status' => $user->dashboard_status
        ]);
    }


    // protected function sendVerificationEmail(\App\Models\User $user)
    // {
    //     //$otp = OTP::generate($usertemp->email);
    //     $otp = OTP::generate($user->email);

    //     $mailData = [
    //         'file_path' => 'auth.email.verification',
    //         'from_email' => config('config.register.sender_email'),
    //         'from_name' => config('config.register.sender_name'),
    //         'to_email' => trim($user->email),
    //         'to_name' => $user->full_name,
    //         'subject' => 'Email Verification OTP',
    //         'filename' => null,
    //         'data' => ['user_data' => $user, 'otp' => $otp]
    //     ];

    //     //dispatch(new SendEmailUser($mailData))->onQueue('email');
    //     \App\Traits\Helper::sendAllEmail($mailData);

    //     return $otp;
    // }


   /**
     * Use for check social login for V2
     *
     * @param Request $request
     * @return void
     */
    public function provider_login_v2(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'provider' => 'required',
            'provider_id' => 'required',
            'token' => 'required'
            //'device_token' => 'required'
        ], [
            'provider.required' => config('language.' . $this->getLocale() . ".Common.provider_lbl"),
            'provider_id.required' => config('language.' . $this->getLocale() . ".Common.provider_id_lbl"),
            'token.required' => config('language.' . $this->getLocale() . ".Auth.login_token_lbl"),
            //'device_token.required' => config('language.' . $this->getLocale() . ".Common.token_lbl"),            
            'email.required' => 'Please enter your email.'
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'blocked' => false,
                'navigate' => 'register',
                'message' => $validator->errors()->first()
            ];

            return response()->json($response);
        }

        //New Social Code Start
        try {
            if (strtolower($request->provider) == 'twitter') {
                if (!$request->secret) {
                    $response = [
                        'status' => false,
                        'blocked' => false,
                        'navigate' => 'register',
                        'message' => config('language.' . $this->getLocale() . ".Auth.login_secret_token_lbl")
                    ];

                    return response()->json($response);
                }
                $social_user = Socialite::driver('twitter')->userFromTokenAndSecret($request->token, $request->secret);
            } else {
                if (strtolower($request->provider) == 'google') {
                    $accessTokenResponse = Socialite::driver('google')->getAccessTokenResponse($request->token);
                    $accessToken = $accessTokenResponse['access_token'];                    
                } else {
                    $accessToken = $request->token;
                }
                $social_user = Socialite::driver(strtolower($request->provider))->userFromToken($accessToken);
                //$social_user = Socialite::driver(strtolower($request->provider))->userFromToken($request->token);
            }
        } catch (\Exception $ex) {
            $response = [
                'status' => false,
                'blocked' => false,
                'navigate' => 'login',
                'message' => config('language.' . $this->getLocale() . ".Auth.login_malformed_lbl")
            ];

            return response()->json($response);
        }

        if(! $social_user->email) {
            $response = [
                'status' => false,
                'blocked' => false,
                'navigate' => 'register',
                'message' => config('language.' . $this->getLocale() . ".Common.email_not_found_lbl")
            ];

            return response()->json($response);
        }

        $user = User::where('email', $social_user->email)->first();
        //New Social Code End

        //$user = User::where('email', $request->email)->first();

        if ($user) {
            if ($user->verified == 'Disabled') {
                return response()->json([
                    'status' => false,
                    'blocked' => true,
                    'navigate' => 'login',
                    'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_verified_lbl")
                ]);
            }

            if ($user->verified == 'Unverified') {
                $user->update(['verified' => 'Verified']);
            }

            if (!$user->isApproved()) {
                return response()->json([
                    'status' => false,
                    'navigate' => 'login',
                    'blocked' => true,
                    'message' => config('language.' . $this->getLocale() . ".Auth.login_admin_approved_lbl")
                ]);
            }

            if (!empty($request->provider) && !empty($request->provider_id)) {
                $login_info = UserSocial::firstOrCreate([
                    'user_id' => $user->id, 'provider' => $request->provider], [
                    'provider_id' => $request->provider_id]);
            }

            //If oauth_enabled is false then
            //we update oauth_enabled true
            if ($user->oauth_enabled == 0) {
                $user->update(['oauth_enabled' => 1]);
            }

            //update last login type
            $random_password = str_random(60);
            $user->update(['last_login_type' => 'Social', 'password_social' => Hash::make($random_password)]);

            /* Creating Social Profile Access Token */
            $access_token = $this->generateAccessToken($request, $user->email, $random_password);
            //$access_token = $this->getBearerTokenByUser($user, config('config.client_id'), false);

            if (!array_key_exists('access_token', $access_token)) {
                return response()->json([
                    'status' => false,
                    'blocked' => false,
                    'navigate' => 'login',
                    'message' => config('language.' . $this->getLocale() . ".Common.access_token_lbl")
                ]);
            }

            $data = [
                'id' => @$user->id,
                'type' => @$user->type,
                'provider_id' => $user->provider_id,
                'username' => $user->username,
                'full_name' => $user->full_name,
                'image' => !empty($user->image) ? $user->getImage(100) : asset('uploads/default/user.png'),
                'country_id' => $user->country,
                'city' => $user->city ? $user->city : '',
                'state' => $user->state ? $user->state : '',
                'country' => $user->country ? $user->country : '',
                'verified' => $user->verified,
                'admin_status' => $user->admin_status,
                'api_token' => $user->api_token,
                'discreet' => $user->discreet,
                'access_token' => @$access_token['access_token'],
                'refresh_token' => @$access_token['refresh_token'],
                'expires_in' => @$access_token['expires_in']
            ];

            // if (empty($user->username)) {
            //     return response()->json([
            //         'status' => false,
            //         'blocked' => false,
            //         'navigate' => 'username',
            //         'data' => $data,
            //         'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
            //     ]);
            // }

            // if (! empty($user->username)) {
            //     if ($user->user_type == 0) {
            //         return response()->json([
            //             'status' => false,
            //             'blocked' => false,
            //             'navigate' => 'username',
            //             'data' => $data,
            //             'message' => config('language.' . $this->getLocale() . ".Common.username_not_created_lbl")
            //         ]);
            //     }
            // }           

            $user = User::find($user->id, ['id', 'first_name', 'email', 'username', 'image', 'type', 'country_id', 'password', 'provider_id', 'api_token', 'verified', 'admin_status', 'discreet', 'city', 'state', 'country', 'oauth_enabled']);            

            $imageData = !empty($user->image) ? $user->getImage(100) : '';

            $city = $user->city ? $user->city : '';
            $state = $user->state ? $user->state : '';
            $country = $user->country ? $user->country : '';

            $data = collect($user)
                ->put('full_name', $user->full_name)
                ->put('city', $city)
                ->put('state', $state)
                ->put('country', $country)                
                ->put('image', $imageData)
                ->put('discreet', @$user->discreet)
                ->put('api_token', $user->api_token)
                ->put('access_token', @$access_token['access_token'])
                ->put('refresh_token', @$access_token['refresh_token'])
                ->put('expires_in', @$access_token['expires_in']);

            //$restore_data = $this->restore_user_data($user);            
            //$navigate = $user->username == '' ? 'username' : '';

            /** Topic  Subscription Code Start */
            //Subscribe all podcast for specific user
            // $allShows = $user->subscribers()->wherePivot('notify', 1)->pluck('id')->all();
            
            // if(count($allShows) > 0) {
            //     try {
            //         $uniqueShows = array_unique($allShows);
            //         $splitShows = array_chunk($uniqueShows, 10);
            //         foreach ($splitShows as $item) {
            //             dispatch(new FirebaseTopicSubscription($item, $user->id, 'subscribe_on_login', [$request->device_token]))->onQueue('subscribe-topic');
            //         }                    
            //     } catch(\Exception $ex) {}            
            // }
            /** Topic  Subscription Code End */

            $navigate = "feed";
            // $category_notification_data = $user->categories()->count();
            // if($category_notification_data == 0) {
            //     $navigate = "select_category";
            // }
            //  else {
            //     if ($user->user_type == 2 && $user->dashboard_status == 0) {
            //         $navigate = "recommended_user";
            //     }
            // }

            return response()->json([
               'status' => true,
               'blocked' => false,
               'navigate' => $navigate,
               'data' => $data,
               //'category_notification_data' => $category_notification_data,
               'user_type' => $user->user_type,
               //'dashboard_status' => $user->dashboard_status
            ]);
        } else {
            return response()->json([
                'status' => false,
                'blocked' => false,
                'navigate' => 'register',
                'message' => config('language.' . $this->getLocale() . ".Common.user_not_found_lbl")
            ]);
        }
    }
}
