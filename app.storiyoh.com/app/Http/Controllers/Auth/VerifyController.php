<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use DB;

class VerifyController extends Controller
{

    function index($id, $token){
        $user = User::where(
                        [
                            'id' => $id,  
                            'validation_key' => $token
                        ]
                      )->first();
        if(isset($user) ){            
            if($user->verified == 'Unverified') {
                $user->verified = 'Verified';
                $user->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return view('auth.verify')->with([
                'user' => $user,
                'warning' => "Sorry, your email cannot be verified."
            ]);
        }
        return view('auth.verify')->with([
            'user' => $user,
            'status' => $status
            ]);    
    }

    public function verifypasswordlink($email,$token)
    {
        $user = User::where(
                        [
                            'id' => $email
                        ]
                      )->first();
        $existToken = DB::table('password_resets')->where('email', $user->email)->orderBy('created_at', 'DESC')->first();
        $tokenreq = $existToken->token;

        if (!Hash::check($token, $tokenreq)) {
            return response()->json([
                'status' => false,
                'navigate' => 'forgot_password_email_otp',
                'message' => 'Invalid password reset link'
            ]);
        } else {
            return response()->json([
                'status' => true,
                'email' => $user->email,
                'navigate' => 'forgot_password_reset',
                'message' => ''
            ]);
        }
    }

}