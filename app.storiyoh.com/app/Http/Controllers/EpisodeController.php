<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Episode;
use App\Models\Show;

class EpisodeController extends Controller
{
    /**
     * This function is use url scheme
     * @param Request $request
     * @return type
     */
    public function url_scheme($data)
    {

        $request_url = explode("/", $_SERVER['REQUEST_URI']);
        if ($request_url[1] == "e") {
            //Episode
            $episode = Episode::where('id', $data)->first(['id', 'show_id', 'title', 'image', 'updated_at', 'description', 'duration', 'mp3']);

            if (!$episode) {
                die("No Episode Found.");
            }

            $image = !empty($episode->show->image) ? $episode->show->getWSImage(200) : asset('uploads/default/show.png');
            if (!is_null($episode->episode_extra_info)) {
                if ($episode->episode_extra_info->content_type == 2) {
                    $image = !empty($episode->image) ? $episode->getImage(200) : asset('uploads/default/show.png');
                }
            }

            $type = "episode";

            $item = [
                "id" => $episode->id,
                "image" => $image,
                "title" => trim(str_replace("\n", "", $episode->title)),
                "desc" => trim($episode->description)
            ];

            return view('pages.api.middle', compact('type', 'item'));
        } else if ($request_url[1] == "s") {
            //Show
            $show = Show::where('id', $data)->first(['id', 'title', 'image', 'description']);

            if (!$show) {
                die("No Podcast Found.");
            }

            $image = !empty($show->image) ? $show->getWSImage(200) : asset('uploads/default/show.png');

            $type = "show";

            $item = [
                "id" => $show->id,
                "image" => $image,
                "title" => trim(str_replace("\n", "", $show->title)),
                "desc" => trim($show->description)
            ];

            return view('pages.api.middle', compact('type', 'item'));
        }
    }
}
