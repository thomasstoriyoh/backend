<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class OptimizeImageController extends Controller
{
    /**
     * @param $token
     * @return mixed
     */
    public function index($token, Request $request)
    {
        $resource = (object) json_decode(base64_decode($token));

        //dd($resource);

        if($request->has('w') && $request->has('h')) {
            $image = Image::make($resource->image)->fit($request->w, $request->h, function ($img) {
                $img->upsize();
            });
        } else {
            $image = Image::make($resource->image)->fit($resource->w, $resource->h, function ($img) {
                $img->upsize();
            });
            //$image = Image::make($resource->image)->resize(400, 400);
        }

        return $image->response('jpg');
    }
}
