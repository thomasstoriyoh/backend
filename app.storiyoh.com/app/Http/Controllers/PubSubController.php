<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \Pubsubhubbub\Subscriber\Subscriber;
use App\Models\ShowActiveHourly;
use App\Models\ShowActiveDaily;
use App\Models\ShowActiveDailyOnce;
use Carbon\Carbon;
use App\Classes\XMLParse;
use App\Models\Episode;
use App\Classes\AddNewEpisode;
use Illuminate\Support\Facades\Log;

class PubSubController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function pull_and_push_rss_data (Request $request) {
        if ($request->has('hub_challenge')) {
            echo $request->hub_challenge;
            //update bucket
            $has_subscribe = 0;
            if($request->hub_mode == "subscribe") {
                $has_subscribe = 1;
            }
            if($request->bucket == 1) {
                ShowActiveHourly::where('show_id', $request->show_id)->update(['has_subscribe' => $has_subscribe]);
            } else if ($request->bucket == 2) {
                ShowActiveDaily::where('show_id', $request->show_id)->update(['has_subscribe' => $has_subscribe]);
            } else if ($request->bucket == 3) {
                ShowActiveDailyOnce::where('show_id', $request->show_id)->update(['has_subscribe' => $has_subscribe]);
            }            
            //Storage::disk('local')->put('PUBSUBHUBBUB/hub_challenge_'.$request->show_id.'_'.@date("YmdHis").'.txt', json_encode($request->all()));
            //Storage::disk('local')->put('PUBSUBHUBBUB/pubsub.txt', @implode(" , ", $request->all()));
            exit;
        }
        else {            
            $xmlData = file_get_contents("php://input");

            $data = new XMLParse($xmlData);
            $parseXMLData = $data->parseXMLData();

            $date_time = @date("YmdHis");
            Storage::disk('local')->put('PUBSUBHUBBUB/pubsub_'.$date_time.'.txt', $xmlData);
            Storage::disk('local')->put('PUBSUBHUBBUB/requst_'.$date_time.'.txt', json_encode($request->all()));
            //Storage::disk('local')->put('PUBSUBHUBBUB/pubsub2.txt', json_encode($parseXMLData));
            //Storage::disk('local')->put('PUBSUBHUBBUB/pubsub3.txt', json_encode($request->all()));            

            if($request->has('show_id')) {
                
                $show_id = $request->show_id;

                $episodeData = $this->checkEpisodeExists($show_id, $parseXMLData);

                if (empty($episodeData)) {
                    //Add new episode
                    //$this->addNewEpisode($show_id, $parseXMLData);
                } else {
                    //update episode
                    $this->updateNewEpisode($show_id, $episodeData, $parseXMLData);
                }
            }       
        }
    }
    
    /**
     * This function is use for subscribe on pubsubhubbub
     *
     * bucket = 1 - show_active_hourly, 2 - show_active_daily, 3 - show_active_daily_once
     * show_id
     * @param Request $request
     * @return void
     */
    public function subscribe($show_id, $hub_url, $bucket, $feed_url) {

        $callback_url = config('config.storiyoh_pubsub_url')."/pubsub/pull_and_push_rss_data?show_id=".$show_id."&bucket=".$bucket;
        
        $feed = $feed_url;
      
        // create a new subscriber
        $s = new Subscriber($hub_url, $callback_url);
      
        // subscribe to a feed
        $s->subscribe($feed);
    }

    /**
     * This function is use for unsubscribe on pubsubhubbub
     *
     * bucket = 1 - show_active_hourly, 2 - show_active_daily, 3 - show_active_daily_once
     * show_id
     * @param Request $request
     * @return void
     */
    public function unsubscribe($show_id, $hub_url, $bucket, $feed_url) {
        
        $callback_url = config('config.storiyoh_pubsub_url')."/pubsub/pull_and_push_rss_data?show_id=".$show_id."&bucket=".$bucket;
        
        $feed = $feed_url;
      
        // create a new subscriber
        $s = new Subscriber($hub_url, $callback_url);
      
        // unsubscribe to a feed
        $s->unsubscribe($feed);
    }    

    /**
     * Undocumented function
     *
     * @param [type] $show_id
     * @param [type] $data
     * @return void
     */
    protected function checkEpisodeExists($show_id, $data) {

        $checkEpisodeMp3Data = Episode::where('show_id', $show_id)->where('mp3', $data['data']['episodes'][0]['mp3'])->first();
        
        return $checkEpisodeMp3Data;
    }

    /**
     * Insert New Episode
     *
     * @param [type] $show_id
     * @param [type] $data
     * @return void
     */
    protected function addNewEpisode($show_id, $data) {
        Log::info("I am in Added Function");
        $new_episode = new AddNewEpisode($show_id, $data);
        $new_episode->fetchingEpisodes();
    }

    /**
     * Update Episode Data
     *
     * @param [type] $show_id
     * @param [type] $data
     * @return void
     */
    protected function updateNewEpisode($show_id, $episodeData, $parseXMLData) {
        Log::info("I am in Updated Function");
        $updateData = [
            "title" => @$parseXMLData['data']['episodes'][0]['title'],
            "description" => @$parseXMLData['data']['episodes'][0]['description'],
            "size" => @$parseXMLData['data']['episodes'][0]['size'],
            "duration" => @$parseXMLData['data']['episodes'][0]['duration']
        ];
        //Episode::where('show_id', $show_id)->where('id', $episodeData->id)->update($updateData);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function podcast_subscribe() {        
        $item = ShowActiveHourly::where('id', 57)->first();
        //dd($item->toArray());
        $this->subscribe($item->show_id, $item->hub_url, 1, $item->feed_url);
    }
}
