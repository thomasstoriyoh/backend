<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        //\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\HttpsMiddleware::class,
        ],

        'api' => [
            //'throttle:200,1',
            'bindings',
            \App\Http\Middleware\Api\HttpsMiddleware::class,
            \Barryvdh\Cors\HandleCors::class,            
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        //'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'throttle' => \App\Http\Middleware\Api\ThrottleLimit::class,
        'auth.admin' => \App\Http\Middleware\Admin\Authenticate::class,
        'guest.admin' => \App\Http\Middleware\Admin\RedirectIfAuthenticated::class,
        'apc' => \App\Http\Middleware\Admin\AccessPermissionMiddleware::class,
        'key' => \App\Http\Middleware\Api\VerifyApiKey::class,
        'crypt' => \App\Http\Middleware\Api\EncryptRequestResponse::class,
        'auth.api' => \App\Http\Middleware\Authenticate::class,
        'cors' => \App\Http\Middleware\Api\Cors::class,
        //'oauth2' => \App\Http\Middleware\Api\Oauth2::class,
        //'https_api' => \App\Http\Middleware\Api\HttpsMiddleware::class,
        '2fa' => \App\Http\Middleware\Admin\Google2FAMiddleware::class,
        'business_crypt' => \App\Http\Middleware\Api\EncryptRequestBusiness::class,
        'auth.business_api' => \App\Http\Middleware\Api\Authenticate::class,
    ];
}
