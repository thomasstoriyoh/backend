<?php

namespace App\Http\Middleware\Admin;

use Closure;
use App\Models\Admin\Module;
use Illuminate\Support\Facades\Auth;

class AccessPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        list($controller, $action) = explode("@", $request->route()->getActionName());
        $controllerName = $this->getControllerName($controller);
        $module = Module::whereController($controllerName)->first();
        if (!is_null($module)) {
            if (! Auth::guard($guard)->user()->can($action . "." .$module->slug)) {
                
                return $this->redirectIfNotAuthorized($request);
            }
        }
        return $next($request);
    }
    
    /**
     * Parse controller name from the full namespaced controller name.
     * @param string $controller
     * @return string Controller
     */
    public function getControllerName($controller) {
        $controller_array = explode('\\', $controller);
        return end($controller_array);
    }

    /**
     * 
     * @param $request
     * @return mixed
     */
    public function redirectIfNotAuthorized($request) {
        if ( $request->isJson() || $request->wantsJson() ) {
            return response()->json([
                'status' => false, 
                'message' => 'You do not have permission to access this feature.'
                ], 200);
        }

        return redirect()->action('Admin\Core\DashboardController@index')
                ->with('error', 'You do not have permission to access the page.');
    }

}
