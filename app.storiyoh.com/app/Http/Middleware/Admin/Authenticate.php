<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = "admin")
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                Auth::guard($guard)->logout();
                return redirect()->guest('catalyst/login');
            }
        }

        if (! Auth::guard($guard)->user()->isActive() ||  ! Auth::guard($guard)->user()->isApproved()) {
            Auth::guard($guard)->logout();
            return redirect()->guest('catalyst/login')->withErrors([
                'username' => 'Your account has been disabled please contact the Administrator.',
            ]);
        }

        return $next($request);
    }
}
