<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = "admin")
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::guard($guard)->user()->isActive() && Auth::guard($guard)->user()->isApproved()) {
                return redirect('/catalyst');
            }
        }

        return $next($request);
    }
}
