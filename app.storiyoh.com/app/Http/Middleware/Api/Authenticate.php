<?php

namespace App\Http\Middleware\Api;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Traits\ResponseFormat;

class Authenticate
{
    use ResponseFormat;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        if (Auth::guard('api')->guest()) {
            if ($request->ajax() || $request->wantsJson()) {                    
                $service = $request->path();
                $code = 401;
                if($code != null && $service != null) {
                    return $this->sendExceptionError($code, $service);
                }
            }
        }

        return $next($request);
    }
}
