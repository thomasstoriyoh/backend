<?php

namespace App\Http\Middleware\Api;

use Closure;

use App\Traits\ResponseFormatBusiness;

class EncryptRequestBusiness
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(2);
        if($request->has('encData')) {
            $decrypted = ResponseFormatBusiness::decrypt($request->encData);
            $query = json_decode($decrypted, 1);
            
            if (is_array($query)) {
                $request->merge($query);
            }
        }

        return $next($request);
    }
}
