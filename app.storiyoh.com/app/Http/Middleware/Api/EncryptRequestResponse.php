<?php

namespace App\Http\Middleware\Api;

use Closure;
use Tzsk\Crypt\Facade\StrCrypt;
use App\Traits\EncryptDecrypt;

class EncryptRequestResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request_data = explode('/',$request->path());
        if (in_array('v2', $request_data) && in_array('new_version', $request_data)) {
            if ($request->has('encData')) {
                $decrypted = EncryptDecrypt::decrypt($request->encData);
                $query = json_decode($decrypted, 1);
                if (is_array($query)) {
                    $request->merge($query);
                }
            }
        } else if (in_array('v5', $request_data) && in_array('new_version', $request_data)) {
            if ($request->has('encData')) {
                $decrypted = StrCrypt::decrypt($request->encData);
                $query = json_decode($decrypted, 1);
                if (is_array($query)) {
                    $request->merge($query);
                }
            }
        } else if (in_array('v6', $request_data) && in_array('new_version', $request_data)) {
            if ($request->has('encData')) {
                $decrypted = StrCrypt::decrypt($request->encData);
                $query = json_decode($decrypted, 1);
                if (is_array($query)) {
                    $request->merge($query);
                }
            }
        } else if (in_array('v7', $request_data) && in_array('new_version', $request_data)) {
            if ($request->has('encData')) {
                $decrypted = StrCrypt::decrypt($request->encData);
                $query = json_decode($decrypted, 1);
                if (is_array($query)) {
                    $request->merge($query);
                }
            }
        } else if (in_array('v8', $request_data) && in_array('new_version', $request_data)) {
            if ($request->has('encData')) {
                $decrypted = StrCrypt::decrypt($request->encData);
                $query = json_decode($decrypted, 1);
                if (is_array($query)) {
                    $request->merge($query);
                }
            }
        } /*else {
            if ($request->header('request-encrypted', 'No') == 'Yes') {
                if ($request->has('encData')) {
                    $decrypted = StrCrypt::decrypt($request->encData);
                    $query = json_decode($decrypted, 1);
                    //dd($request->all(), $decrypted, $query);
                    if (is_array($query)) {
                        $request->merge($query);
                    }
                }
            }
        }*/

        return $next($request);
    }
}
