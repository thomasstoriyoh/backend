<?php

namespace App\Http\Middleware\Api;

use Closure;
use App\Traits\ResponseFormat;

class HttpsMiddleware
{
    use ResponseFormat;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        if (!$request->secure()  && in_array(env('APP_ENV'), ['stage', 'production'])) {
            $service = $request->path();
            return $this->sendExceptionError(404, $service);
        }

        return $next($request);
    }
}
