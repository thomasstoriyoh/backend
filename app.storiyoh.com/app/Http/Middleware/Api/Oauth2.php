<?php

namespace App\Http\Middleware\Api;

use Closure;

class Oauth2
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        // ->header('Access-Control-Allow-Origin', true)
        // ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        // ->header('Access-Control-Allow-Headers', 'api-key, content-type, request-encrypted, response-encrypted');
    }
}
