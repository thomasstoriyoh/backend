<?php

    namespace App\Http\Middleware\Api;

use Closure;
    use Illuminate\Support\Str;
    use App\Traits\ResponseFormat;

    class ThrottleLimit extends \Illuminate\Routing\Middleware\ThrottleRequests
    {
        use ResponseFormat;

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @param  int|string  $maxAttempts
         * @param  float|int  $decayMinutes
         * @return mixed
         * @throws \Illuminate\Http\Exceptions\ThrottleRequestsException
         */
        public function handle($request, Closure $next, $maxAttempts = 60, $decayMinutes = 1)
        {
            $key = $this->resolveRequestSignature($request);

            $maxAttempts = $this->resolveMaxAttempts($request, $maxAttempts);

            if ($this->limiter->tooManyAttempts($key, $maxAttempts)) {
                // return response()->api([
                //     'status' => false,
                //     'message' => 'Too many requests.'
                // ]);
                $service = $request->path();

                return $this->sendExceptionError(429, $service);
                //throw $this->buildException($key, $maxAttempts);
            }

            $this->limiter->hit($key, $decayMinutes);

            $response = $next($request);

            return $this->addHeaders($response, $maxAttempts, $this->calculateRemainingAttempts($key, $maxAttempts));
        }

        /**
         * Undocumented function
         *
         * @param [type] $request
         * @return void
         */
        protected function resolveRequestSignature($request)
        {
            //dd(\Auth::guard('api')->user());
            if ($user = \Auth::guard('api')->user()) {
                return sha1($user->getAuthIdentifier());
            }

            $ip = isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : '';

            if ($route = $request->route()) {
                //return sha1($route->getDomain().'|'.$request->ip());
                return sha1(implode('|', [
                    $request->method(),
                    $request->path(),
                    $request->ip(),
                    $ip
                ]));
            }

            return response()->api([
                'status' => false,
                'message' => 'Unable to generate the request signature. Route unavailable.'
            ]);
            //throw new RuntimeException('Unable to generate the request signature. Route unavailable.');
        }

        /**
         * Resolve the number of attempts if the user is authenticated or not.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int|string  $maxAttempts
         * @return int
         */
        protected function resolveMaxAttempts($request, $maxAttempts)
        {
            if (Str::contains($maxAttempts, '|')) {
                $maxAttempts = explode('|', $maxAttempts, 2)[\Auth::guard('api')->user() ? 1 : 0];
            }

            if (!is_numeric($maxAttempts) && \Auth::guard('api')->user()) {
                $maxAttempts = \Auth::guard('api')->user()->{$maxAttempts};
            }

            return (int) $maxAttempts;
        }
    }
