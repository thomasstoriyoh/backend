<?php

namespace App\Http\Middleware\Api;

use Closure;

class VerifyApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->header('api-key', false)) {
            return response()->json(['status' => false, 'message' => 'Invalid API KEY']);
        }

        if ($request->header('api-key') != 'sFaoqUMh8z8FfMqzfeuXPHcywYFDP2LH') {
            return response()->json(['status' => false, 'message' => 'Invalid API KEY']);
        }

        //\Log::info('Calling WS===' . $_SERVER['REQUEST_URI']);
        //\Log::info('Remote Address===' . $_SERVER['REMOTE_ADDR']);

        return $next($request);
    }
}
