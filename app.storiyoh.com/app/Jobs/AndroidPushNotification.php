<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Traits\FireBaseAndroid;
use Log;

class AndroidPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $tokens;
    
    /**
     * @var string
     */
    protected $title;
    
    /**
     * @var string
     */
    protected $description;
    
    /**
     * @var string
     */
    protected $icon;
    
    /**
     * @var string
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tokens, $title, $description, $icon, $data)
    {
        $this->tokens = $tokens;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("I am on Android call");
        if($this->tokens) {
            $result = FireBaseAndroid::sendFireBaseNotification($this->tokens, $this->title, $this->description, $this->icon, $this->data);
            Log::info($result);
        }        
    }
}
