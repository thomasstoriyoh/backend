<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Cache;
use App\Traits\FireBase;
use Log;
use App\Models\AppVersion;
use App\Models\PushId;

class AppVersionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $itemId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->itemId = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $app_version = AppVersion::find($this->itemId);

        if($app_version) {
            $tokens = PushId::active()
                ->where('platform', $app_version->platform)
                ->pluck('token')->all();

            if(count($tokens)) {
                $title = "New Version Available";
                $description = @$app_version->description;
                $icon = "";
                $data = [
                    'id' => $app_version->id,
                    'title' => "New Version Available",
                    'type' => "AV",
                    'desc' => @$app_version->description,
                    'version' => @$app_version->version,
                    'force_update' => @$app_version->force_update,
                    'platform' => @$app_version->platform,
                    'image' => @$app_version->image ? asset('uploads/version/thumbs/640_480_' . @$app_version->image) : "",
                    'tray_icon' => ''
                ];
                $result = FireBase::sendFireBaseNotification($tokens, $title, $description, $icon, $data);
                Log::info($result);
            }
        }

        Log::info("TOKENS".json_encode($tokens));

        @Cache::forget('ProcessRunning.App.Push'.$app_version->id);
    }
}
