<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Traits\FireBase;
use Log;

class CategoryEpisodePushNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $tokens;
    
    /**
     * @var string
     */
    protected $title;
    
    /**
     * @var string
     */
    protected $description;
    
    /**
     * @var string
     */
    protected $icon;
    
    /**
     * @var string
     */
    protected $data;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tokens, $title, $description, $icon, $data)
    {
        $this->tokens = $tokens;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("I am category call");
        if($this->tokens) {
            $result = FireBase::sendFireBaseNotification($this->tokens, $this->title, $this->description, $this->icon, $this->data);
        }
        Log::info($result);
    }
}
