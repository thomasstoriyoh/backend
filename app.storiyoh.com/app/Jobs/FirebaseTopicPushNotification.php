<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Show;
use App\Models\Episode;

use App\Classes\Firebase as FirebaseTopic;
use Log;

class FirebaseTopicPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Show Ids
     *
     * @var [type]
     */
    protected $show_id;

    /**
     * User Ids
     *
     * @var [type]
     */
    protected $userIds;

    /**
     * Episode Ids
     *
     * @var [type]
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show_id, $userIds, $data)
    {
        $this->show_id = $show_id;
        $this->userIds = $userIds;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $show = Show::published()->where('id', $this->show_id)->first(['id', 'title', 'image']);

        $episode = $this->data;
        $episode_count = count($episode);            
        $ep_id = '';
        $epi_desc = '';
        if ($episode_count == 1) {
            $episodeInfo = Episode::find($episode[0], ['id', 'title', 'description']);
            $mesage = 'New Episode: ' . str_replace('"', "'", $episodeInfo->title);
            $ep_id = $episodeInfo->id;
            $epi_desc = '';
        } else {
            $mesage = 'New Episodes';
        }

        $title = str_replace('"', "'", $show->title);
        $description = $mesage;

        $topic = config('config.firebase_topic_pre_text').$show->id;

        if($episode_count > 1) {
            $sendData = [
                'topic' => $topic,
                // 'notification' => [
                //     'title' => $title,
                //     'body' => $mesage,
                //     'icon' => '',
                //     'badge' => '1',
                //     'sound' => 'default',
                // ],
                'data' => [
                    'title' => $title,
                    'type' => 'SNE',
                    'desc' => $mesage,
                    'epi_desc' => $epi_desc,
                    'id' => trim($show->id),
                    'epi_id' => trim($ep_id),
                    'episode_count' => trim($episode_count),
                    'image' => !empty($show->image) ? $show->getWSImage(600) : '',
                    'mediaUrl' => !empty($show->image) ? $show->getWSImage(600) : '',
                    'tray_icon' => ''                
                ],
                'android' => [
                    //'ttl' => '3600s',
                    'priority' => 'normal',
                    // 'notification' => [
                    //     'title' => $title,
                    //     'body' => $mesage,
                    //     'icon' => '',
                    //     'color' => '',
                    // ],
                ],
                'apns' => [
                    'headers' => [
                        'apns-priority' => '10',
                    ],
                    'payload' => [
                        'aps' => [
                            'mutable-content' => 1,                        
                            //'click_action' => 'local', // If only one episode
                            'alert' => [
                                'title' => $title,
                                'body' => $mesage,
                            ],
                            //'badge' => 42,
                            'sound' => 'default',
                        ],
                    ],
                ]
            ];
        } else {
            $sendData = [
                'topic' => $topic,
                // 'notification' => [
                //     'title' => $title,
                //     'body' => $mesage,
                //     'icon' => '',
                //     'badge' => '1',
                //     'sound' => 'default',
                // ],
                'data' => [
                    'title' => $title,
                    'type' => 'SNE',
                    'desc' => $mesage,
                    'epi_desc' => $epi_desc,
                    'id' => trim($show->id),
                    'epi_id' => trim($ep_id),
                    'episode_count' => trim($episode_count),
                    'image' => !empty($show->image) ? $show->getWSImage(600) : '',
                    'mediaUrl' => !empty($show->image) ? $show->getWSImage(600) : '',
                    'tray_icon' => ''                
                ],
                'android' => [
                    //'ttl' => '3600s',
                    'priority' => 'normal',
                    // 'notification' => [
                    //     'title' => $title,
                    //     'body' => $mesage,
                    //     'icon' => '',
                    //     'color' => '',
                    // ],
                ],
                'apns' => [
                    'headers' => [
                        'apns-priority' => '10',
                    ],
                    'payload' => [
                        'aps' => [
                            'mutable-content' => 1,                        
                            'category' => 'local', // If only one episode
                            'alert' => [
                                'title' => $title,
                                'body' => $mesage,
                            ],
                            //'badge' => 42,
                            'sound' => 'default',
                        ],
                    ],
                ]
            ];
        }
        
        $firebaseTopic = new FirebaseTopic;
        $firebaseTopic->sendApnandAndroidMessagetoTopic($sendData);

        //Log::info("Send Data Json");
        //Log::info(json_encode($sendData));
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['firebase_push_notification'];
    }
}
