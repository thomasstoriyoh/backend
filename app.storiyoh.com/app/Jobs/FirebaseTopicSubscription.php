<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Classes\Firebase as FirebaseTopic;
use App\Models\PushId;
use App\Models\User;

class FirebaseTopicSubscription implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $show_id;
    
    /**
     * @var string
     */
    protected $userId;
    
    /**
     * @var string
     */
    protected $subscribe_type;

    protected $device_tokens;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show_id, $user_id, $type, $tokens = [])
    {
        $this->show_id = $show_id;
        $this->userId = $user_id;
        $this->subscribe_type = $type;
        $this->device_tokens = $tokens;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Fetch All tokens from tbl_push tables and unsubscribed from topic        
        $firebaseTopic = new FirebaseTopic;
        if($this->subscribe_type == "subscribe") {            
            $tokens = PushId::where('user_id', $this->userId)->pluck('token')->all();
            //\Log::info("Single Show Subscribed". $this->show_id);
            //\Log::info("Single Show Subscribed Tokens Array ". json_encode($tokens));
            if (count($tokens) > 0) {
                try {
                    $resultReturn = $firebaseTopic->subscribetoATopic($this->show_id, $tokens);
                    //\Log::info("Subscribed Result === ".json_encode($resultReturn));
                } catch (\Exception $ex) {
                    \Log::info("Catch Subscribed");
                    \Log::info($ex->getMessage());
                }
            }            
        } else if ($this->subscribe_type == "unsubscribe") {            
            $tokens = PushId::where('user_id', $this->userId)->pluck('token')->all();
            //\Log::info("Single Show Unsubscribed". $this->show_id);
            //\Log::info("Single Show Unsubscribed Tokens Array ". json_encode($tokens));
            if (count($tokens) > 0) {
                try {
                    $resultReturn = $firebaseTopic->unSubscribefromTopic($this->show_id, $tokens);
                    //\Log::info("Unsubscribed Result === ".json_encode($resultReturn));
                } catch (\Exception $ex) {
                    \Log::info("Catch Unsubscribed");
                    \Log::info($ex->getMessage());
                }
            }            
        } else if ($this->subscribe_type == "subscribe_on_login") {
            if (count($this->device_tokens) > 0) {
                $tokens = $this->device_tokens;
                //\Log::info("Login Show Array ". json_encode($this->show_id));
                //\Log::info("Login Tokens Array ". json_encode($tokens));
                foreach ($this->show_id as $showId) {
                    try {
                        $resultReturn = $firebaseTopic->subscribetoATopic($showId, $tokens);
                        //\Log::info("subscribe_on_login Result === ".json_encode($resultReturn));
                    } catch (\Exception $ex) {
                        \Log::info("Catch Login Subscribed");
                        \Log::info($ex->getMessage());
                        continue;
                    }
                }
            }
        } else if ($this->subscribe_type == "subscribe_on_dashboard") {
            if (count($this->device_tokens) > 0) {
                $tokens = $this->device_tokens;
                //\Log::info("Dashboard Show Array ". json_encode($this->show_id));
                //\Log::info("Dashboard Tokens Array ". json_encode($tokens));
                foreach ($this->show_id as $showId) {
                    try {
                        $resultReturn = $firebaseTopic->subscribetoATopic($showId, $tokens);
                        //\Log::info("subscribe_on_dashboard Result === ".json_encode($resultReturn));
                    } catch (\Exception $ex) {
                        \Log::info("Catch Dashboard Subscribed");
                        \Log::info($ex->getMessage());
                        continue;
                    }
                }
            }
        } else if ($this->subscribe_type == "unsubscribe_on_logout") {            
            if (count($this->device_tokens) > 0) {
                $tokens = $this->device_tokens;
                //\Log::info("Logout Show Array ". json_encode($this->show_id));
                //\Log::info("Logout Tokens Array ". json_encode($tokens));
                foreach ($this->show_id as $showId) {
                    try {
                        $resultReturn = $firebaseTopic->unSubscribefromTopic($showId, $tokens);
                        //\Log::info("unsubscribe_on_logout Result === ".json_encode($resultReturn));
                    } catch (\Exception $ex) {
                        \Log::info("Catch Logout Subscribed");
                        \Log::info($ex->getMessage());
                        continue;
                    }
                }
            }
        } else if ($this->subscribe_type == "subscribe_on_first_time") {
            $tokens = PushId::where('user_id', $this->userId)->pluck('token')->all();
            //\Log::info("First Time User Id ". $this->userId);
            //\Log::info("First Time Subscribe Show Array ". json_encode($this->show_id));
            //\Log::info("First Time Subscribe Tokens Array ". json_encode($tokens));
            if (count($tokens) > 0) {
                foreach ($this->show_id as $showId) {
                    try {
                        $resultReturn = $firebaseTopic->subscribetoATopic($showId, $tokens);
                        //\Log::info("subscribe_on_first_time Result === ".json_encode($resultReturn));
                        //\Log::info("subscribe_on_first_time Show Id === " . $showId);
                    } catch (\Exception $ex) {
                        \Log::info("Catch First Time Subscribed");
                        \Log::info($ex->getMessage());
                        continue;
                    }
                }

                //update topic_sync to 1 in tbl_users table
                $userData = User::find($this->userId);
                $userData->forceFill(["topic_sync" => 1])->save();
            }
        }
    }
}
