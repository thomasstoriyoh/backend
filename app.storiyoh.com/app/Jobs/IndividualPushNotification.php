<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Traits\FireBase;
use Illuminate\Support\Facades\Log;

class IndividualPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $tokens;
    protected $title;
    protected $description;
    protected $icon;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tokens, $title, $description, $icon, $data)
    {
        $this->tokens = $tokens;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        FireBase::sendFireBaseNotification($this->tokens, $this->title, $this->description, $this->icon, $this->data);
    }
    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['single_push_notification'];
    }
}
