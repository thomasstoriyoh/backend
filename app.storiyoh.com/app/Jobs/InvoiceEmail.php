<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Order;
use Illuminate\Support\Facades\Log;
use App\Traits\HelperV2;
use App\Models\Show;
use App\Models\Episode;
use Illuminate\Support\Facades\Storage;

class InvoiceEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orderId;

    protected $orderType;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($orderId, $orderType)
    {
        $this->orderId = $orderId;
        $this->orderType = $orderType;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = Order::with(['user', 'country', 'sender'])->find($this->orderId, [
            'id', 'purchase_type', 'product_type', 'gateway', 'country_id', 'product_id', 'user_id', 'billing_city', 'billing_state', 
            'billing_country', 'invoice_number', 'invoice_date', 'invoice_total', 'tax_name', 'tax_percent', 'tax_amount', 'giftBy', 
            'gift_message', 'net_amount', 'currency'
        ]);

        if($this->orderType == "self") {
            $invoice_data['user_billing_name'] = $order->user->full_name;
            $invoice_data['user_billing_email'] = $order->user->email;
        } else if($this->orderType == "gift") {
            $invoice_data['user_billing_name'] = $order->sender->full_name;
            $invoice_data['user_billing_email'] = $order->sender->email;
        }

        $invoice_data['invoice_id'] = $order->id;
        $invoice_data['invoice_number'] = $order->invoice_number;
        $invoice_data['invoice_date'] = $order->invoice_date;

        $invoice_data['net_amount'] = $order->net_amount;
        $invoice_data['invoice_total'] = $order->invoice_total;
        $invoice_data['invoice_tax'] = $order->invoice_date;

        $invoice_data['tax_title'] = $order->tax_name;
        $invoice_data['tax_percent'] = $order->tax_percent;
        $invoice_data['tax_amount'] = is_null($order->tax_amount) ? 0 : $order->tax_amount;

        $invoice_data['gateway'] = $order->gateway;

        //$curreny_array = config('config.currency');
        //$invoice_data['currency'] = $curreny_array[$order->country->id];
        $invoice_data['currency'] = $order->currency;

        $invoice_data['gift_by'] = is_null($order->giftBy) ? "" : $order->user->first_name;

        if($order->purchase_type == 2) {
            if($order->product_type == 1) { // Series
                $content = Show::find($order->product_id, ['id', 'title']);
                $invoice_data['content_title'] = "Show Name : ".$content->title;
            } else if($order->product_type == 2 || $order->product_type == 3) { // Episode
                $content = Episode::find($order->product_id, ['id', 'title', 'show_id']);
                $invoice_data['content_title'] = "Episode Name : ".$content->title;
                $invoice_data['content_title'] .= ($content->show->content_type == 1) ? " (".$content->show->title.")" : "";
            }
        }

        //Create PDF File
        $pdf = \PDF::loadView('emails.pdf-invoice', compact('invoice_data'))->setPaper('a4');
        //$filename = "uploads/pdf/".$order->id.'.pdf';
        //$pdf->save(public_path($filename));

        //Upload pdf file on S3 Bucket
        $upload_file_name =  @date('YmdHis') . str_random(6) . '.pdf';;
        $upload_file_path = Storage::disk('s3')->put("invoice/" . $upload_file_name, $pdf->output());

        $filename = null;
        if($upload_file_path) {
            $filename = config('config.s3_url') . '/invoice/'.$upload_file_name;
        }

        $sender_name = "";
        $sender_email = "";
        if($this->orderType == "self") {
            $sender_name = $order->user->full_name;
            $sender_email = $order->user->email;
        } else if($this->orderType == "gift") {
            $sender_name = $order->sender->full_name;
            $sender_email = $order->sender->email;
        }

        //\Log::info("Name: ". $sender_name);
        //\Log::info("Email: ". $sender_email);

        $mailData = [
            'file_path' => 'emails.invoice',
            'from_email' => config('config.invoice.sender_email'),
            'from_name' => config('config.invoice.sender_name'),
            'to_email' => trim($sender_email),
            'to_name' => $sender_name,
            'subject' => 'Your invoice from Storiyoh',
            'filename' => $filename,
            'data' => ['invoice_data' => $invoice_data]
        ];

        HelperV2::sendAllEmail($mailData);

        //Delete PDF File
        if (Storage::disk('s3')->exists('invoice/' . $upload_file_name)) {
            Storage::disk('s3')->delete('invoice/' . $upload_file_name);
        }
    }
}
