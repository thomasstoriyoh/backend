<?php

namespace App\Jobs;

use Artisan;
use Cache;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ManualProcessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $commands = [
        "Show" => "process:shows",
        "Episode" => "process:episodes",
    ];

    /**
     * Create a new job instance.
     * @param $item mixed
     */
    public function __construct($item)
    {
        $this->name = $item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (! empty($this->commands[$this->name])) {
            Artisan::call($this->commands[$this->name]);
        }
    }
}
