<?php
namespace App\Jobs;

use Artisan;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ManualSyncJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $commands = [
        "Category" => "sync:categories",
        "Network" => "sync:networks",
        "Show" => "sync:shows",
        "Episode" => "sync:episodes",
    ];

    /**
     * Create a new job instance.
     * @param $item mixed
     */
    public function __construct($item)
    {
        $this->name = $item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Artisan::call($this->commands[$this->name]);

        $item = $this->getModelInstance();
        if (! $item->isProcessing()) {
            dispatch(new ManualProcessJob($this->name));
        }
    }

    /**
     * @param string
     * @return mixed
     */
    protected function getModelInstance()
    {
        $name = 'App\Models\\' . $this->name;

        return new $name;
    }
}
