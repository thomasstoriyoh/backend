<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\ShowActiveDaily;
use App\Models\ShowActiveDailyOnce;
use App\Models\ShowActiveHourly;
use App\Models\ShowNewActiveBucket;
use App\Models\ShowActiveOlderShow;
use App\Models\Show;
use App\Models\Episode;
use Tzsk\ScrapePod\Facade\ScrapePod;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Classes\Notification;
use App\Models\EpisodeTag;

class NewOlderShows implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * show id
     *
     * @var [type]
     */
    protected $show_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show_id)
    {
        $this->show_id = $show_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userInfo = 1;

        $show_id = $this->show_id;

        $ShowActiveDaily = ShowActiveDaily::where('show_id', $show_id)->first();
        if (is_null($ShowActiveDaily)) {
            $ShowActiveDailyOnce = ShowActiveDailyOnce::where('show_id', $show_id)->first();
            if (is_null($ShowActiveDailyOnce)) {
                $ShowActiveHourly = ShowActiveHourly::where('show_id', $show_id)->first();
                if (is_null($ShowActiveHourly)) {
                    $ShowNewActiveBucket = ShowNewActiveBucket::where('show_id', $show_id)->first();
                    if (is_null($ShowNewActiveBucket)) {
                        $ShowActiveOlderShow = ShowActiveOlderShow::where('show_id', $show_id)->first();
                        if (is_null($ShowActiveOlderShow)) {
                            try {
                                $showData = Show::find($show_id);
                                $length = 0;

                                //Fetch Data from Feed URL
                                $response = ScrapePod::feed($showData->feed_url);
                                // dd($response);
                                try {
                                    $no_of_episode = 0;
                                    if (count($response['data']['episodes']) > 0) {
                                        try {
                                            //Insert all episodes to main tbl_episodes table
                                            $userInfo = 1;

                                            $showIds = $this->insertintoEpisodes($response['data']['episodes'], $showData, $userInfo);

                                            //Check if any episode added in last 12 month
                                            $backdate = Carbon::now()->subMonths(config('config.older_show'));
                                            $episode_count = $showData->episodes()->where('date_created', '>=', $backdate->format('Y-m-d'))->count();
                                            if (!empty($showData->feed_url)) {
                                                $responseActive = ShowActiveOlderShow::firstOrNew(['show_id' => $showData->id]);
                                                $responseActive->fill([
                                                    'show_id' => $showData->id,
                                                    'itunes_id' => $showData->itunes_id,
                                                    'feed_url' => $showData->feed_url,
                                                    'content_length' => 0,
                                                    'no_of_episodes' => @count($response['data']['episodes']),
                                                    'cron_run_at' => Carbon::now()->format('Y-m-d')
                                                ])->save();
                                            }

                                            //This is use for new episode added
                                            //$this->makeActivity(array_filter($showIds));

                                            $no_of_episode = count($response['data']['episodes']);
                                        } catch (\Exception $e) {
                                            //Log::info($e->getMessage());
                                            //Log::info('Insert into Episodes 2===' . $showData->id);
                                        }
                                    }

                                    try {
                                        $showData->fill([
                                            'language' => strtolower(trim(@$response['data']['language'])),
                                            'description' => @$response['data']['description'],
                                            'web_url' => @$response['data']['site'],
                                            'no_of_episode' => @count($response['data']['episodes']),
                                            'content_length' => 0,
                                            'status' => 'Published'
                                        ])->save();
                                    } catch (\Exception $e) {
                                    }
                                    $showData->searchable();
                                } catch (\Exception $e) {
                                    //$this->error($e->getMessage());
                                    //$showData->fill(['status' => 'Draft'])->save();
                                    //$showData->unsearchable();
                                    Log::info($e->getMessage());
                                    Log::info('Parse Error');
                                }
                            } catch (\Exception $ex) {
                                Log::info($ex->getMessage());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param type $data
     * @return array
     */
    protected function insertintoEpisodes($items, $showData, $createdBy)
    {
        $showIds = [];
        foreach ($items as $data) {
            try {
                //If same MP3 found than we will not added this episode
                //and continue to next episode
                $checkEpisodeMp3Data = Episode::where('status', 'Published')->where('show_id', $showData->id)->where('mp3', $data['mp3'])->count();
                if ($checkEpisodeMp3Data) {
                    continue;
                }

                $uniqueID = md5($data['title'] . $showData->id . $data['size'] . $data['duration'] . $data['published_at']);

                $checkEpisodeData = Episode::where('uuid', $uniqueID)->count();

                if (!$checkEpisodeData) {
                    $episode = Episode::firstOrNew(['uuid' => $uniqueID]);

                    $newOrOld = $episode->exists;

                    $attributes = $this->getEpisodeAttributes($data, $showData, $createdBy);

                    $episode->fill($attributes)->save();

                    $tags = $this->getTags($data);
                    $episode->tags()->sync($tags);

                    $categories = $showData->categories()->pluck('id')->all();
                    $categories = array_filter($categories);
                    $episode->categories()->sync($categories);

                    if (!empty($attributes['show_id']) && !$newOrOld) {
                        $showIds[$attributes['show_id']][] = trim($episode->id);
                    }

                    $episode->searchable();
                }
            } catch (\Exception $e) {
                $episode->unsearchable();
                //$this->error($e->getMessage());
                //Log::info($e->getMessage());
                //Log::info('Insert into Episodes 1===' . $showData->id);
                continue;
            }
        }

        return $showIds;
    }

    /**
     * @param array $showIds
     * @return void
     */
    protected function makeActivity($showIds)
    {
        if (count($showIds) > 0) {
            $shows = Show::whereIn('id', array_keys($showIds))->get(['id', 'updated_at']);
            foreach ($shows as $show) {
                // $show->activities()->create([
                //     'type' => Feed::SHOW_UPDATE,
                //     'data' => json_encode($showIds[$show->id])
                // ]);
                try {
                    $show->activities_show()->create([
                        'type' => Notification::SHOW_UPDATE,
                        'data' => json_encode($showIds[$show->id])
                    ]);
                } catch (\Exception $ex) {
                    //Log::info($ex->getMessage());
                }
                $show->touch();
            }
        }
    }

    /**
     * @param $data
     * @return array
     */
    protected function getEpisodeAttributes($data, $show, $createdBy)
    {
        $date_created = null;
        if (!empty(@$data['published_at'])) {
            $date_created = Carbon::parse(@$data['published_at'])->format('Y-m-d');
            if ($date_created == '1970-01-01') {
                $date_created = null;
            }
        }

        $date_added = null;
        if (!empty(@$data['published_at'])) {
            $date_added = Carbon::parse(@$data['published_at'])->format('Y-m-d H:i:s');
            if ($date_added == '1970-01-01 05:30:00') {
                $date_added = null;
            }
        }

        $duration = 0;
        if (@$data['duration']) {
            if ($data['duration'] > 0) {
                $duration = $data['duration'];
            }
        }

        $size = 0;
        if (@$data['size']) {
            if ($data['size'] > 0) {
                $size = $data['size'];
            }
        }

        $attributes = [
            'show_id' => @$show->id,
            'itunes_id' => @$show->itunes_id,
            'title' => empty($data['title']) ? '-' : $data['title'],
            'description' => @$data['description'],
            'image' => @$data['image'],
            'duration' => $duration,
            'size' => $size,
            'mp3' => @$data['mp3'],
            'link' => @$data['link'],
            'date_created' => $date_created,
            'date_added' => $date_added,
            'created_by' => $createdBy,
            'updated_by' => $createdBy
        ];

        return $attributes;
    }

    /**
     * @param $data
     * @return array
     */
    protected function getTags($data)
    {
        $tags = [];
        $keywordsData = array_filter($data['keywords']);
        if (count($keywordsData)) {
            foreach ($keywordsData as $tag) {
                $tag = EpisodeTag::firstOrCreate(['title' => $tag]);
                $tags[] = $tag->id;
            }
        }

        return $tags;
    }
}
