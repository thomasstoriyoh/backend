<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Cache;
use App\Models\PushId;
use App\Models\PushNotification;
use App\Traits\FireBase;
use App\Models\TargetGroup;
use Log;
use Carbon\Carbon;

class PushNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $itemId;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->itemId = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification = PushNotification::find($this->itemId);
        $tokens = "";
        if(! empty($notification->target_group_id)) { //Fetch Data from specific target type user
            
            $target_type_user = TargetGroup::find($notification->target_group_id, ['country', 'os', 'last_active']);
            
            $query = PushId::active();
    
            $start_date = Carbon::now()->subDays($target_type_user->last_active)->format('Y-m-d h:i:s');

            $query->whereHas('user', function($q) use ($target_type_user, $start_date) {
                $q->whereIn('country_id', json_decode($target_type_user->country)); 
                if(! empty($target_type_user->last_active)) {
                    $q->where('updated_at', ">=", $start_date);
                }
            });

            if(! empty($target_type_user->os)) {
                $query->where('platform', $target_type_user->os);
            }
            
            $tokens = $query->pluck('token')->all();

        } else {
            $tokens = PushId::active()->whereNotNull('user_id')->pluck('token')->all();
        }
        
        //Log::info("TOKENS".json_encode($tokens));
        
        if(count($tokens)) {                            
            $title = $notification->title; 
            $description = @$notification->message;
            $icon = "";
            $data = [
                'id' => $notification->id,
                'title' => $notification->title, 
                'desc' => @$notification->message,
                "type" => @$notification->destination, // URL or App
                "url" => @$notification->destination,
                "app_page" => @$notification->app_page, //Search or Feed
                "search_query" => @$notification->search_query, // If Search
                "feed_page" => @$notification->feed_page, // If Feed than use Network or Recommendations
                'tray_icon' => ''
            ];
            FireBase::sendFireBaseNotification($tokens, $title, $description, $icon, $data); 
        }
        
        @Cache::forget('ProcessRunning.Push'.$notification->id);
    }        
}
