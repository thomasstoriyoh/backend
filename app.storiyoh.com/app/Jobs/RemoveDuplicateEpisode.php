<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\ShowNotification;
use App\Models\Show;
use App\Models\Episode;
use App\Models\Feed;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class RemoveDuplicateEpisode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $itemId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show_id)
    {
        $this->itemId = $show_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $show_active = Show::with('episodes')->where('id', $this->itemId)->orderBy('id', 'ASC')->get(['id']);
            Log::info($this->itemId);

            $showArray = [];
            $episodeIdsArray = [];
            foreach ($show_active as $item) {
                if ($item->episodes()->count() > 0) {
                    $showArray[$item->id] = [];
                    $allPodcastEpisodeData = $item->episodes()->where('status', 'Published')->whereNotNull('date_added')->orderBy('id', 'DESC')->get();
                    foreach ($allPodcastEpisodeData as $episode_item) {
                        if (! is_null($episode_item->date_added)) {
                            if (array_key_exists(strtotime(Carbon::parse($episode_item->date_added)->format("Y-m-d H:i")), $showArray[$item->id])) {
                                $episodeIdsArray[] = $episode_item->id;
                            } else {
                                $date_added = strtotime(Carbon::parse($episode_item->date_added)->format("Y-m-d H:i"));
                                $showArray[$item->id][$date_added] = $episode_item->id;
                            }
                        }
                    }
                }
            }
            
            //delete or draft episodes and also remove from elasticsearch
            foreach ($episodeIdsArray as $episodeDraftData) {
                
                $epiData = Episode::find($episodeDraftData);
                $epiData->fill(['status' => 'Duplicate'])->save();
                $epiData->unsearchable();

                //also remove show_notification table
                $showNotificationData = ShowNotification::where('typeable_id', $this->itemId)
                    ->where('data', 'LIKE', '%"' . $episodeDraftData . '"%');
                if ($showNotificationData->count()) {
                    $feedData = $showNotificationData->get();
                    foreach ($feedData as $item) {
                        $dataArray = json_decode($item->data);
                        $arr = array_diff($dataArray, [$episodeDraftData]);
                        if (count($arr) == 0) {
                            $item->delete();
                        } else {
                            $item->timestamps = false;
                            $item->fill(['data' => json_encode(array_flatten($arr))])->save();
                        }
                    }
                }

                //Remove episode id for listen from feed
                $feedNotificationData = Feed::where('type', "Episode Heard")
                    ->where('data', 'LIKE', '%"' . $episodeDraftData . '"%');
                if ($feedNotificationData->count()) {
                    $feedListenData = $feedNotificationData->get();
                    foreach ($feedListenData as $itemListenData) {
                        $itemListenData->delete();
                    }
                }
            }
        } catch(\Exception $ex) {
            \Log::info($ex->getMessage());
        }

        \Cache::forget('duplicate.episode.' . $this->itemId);
    }
}
