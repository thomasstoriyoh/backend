<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Board;
use App\Traits\FireBase;
use App\Traits\FireBaseAndroid;
use App\Models\PushId;
use Illuminate\Support\Facades\Log;

class SendNewEpisodePushNotificationV2 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

     /**
     * @var object
     */
    protected $board;
    
    /**
     * @var object
     */
    protected $episode_info_after_attach;

    /**
     * @var object
     */
    protected $exclude_user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($board_data, $episode, $exclude_user_id)
    {
        $this->board = $board_data;
        $this->episode_info_after_attach = $episode;
        $this->exclude_user_id = $exclude_user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $board = $this->board;
        $episode_info_after_attach = $this->episode_info_after_attach;
        $exclude_user_id = $this->exclude_user_id;

        //Log::info("exclude_user_id====".$exclude_user_id);
        //Log::info("episode_info_after_attach=====".$episode_info_after_attach);
                
        //Send Push Notification to the User
        $userIds = $board->followers()->where('users.verified', 'Verified')
            ->where('id', '!=', $exclude_user_id)
            ->where('users.admin_status', 'Approved')->pluck('id')->all();

        //Log::info("userIds====".json_encode($userIds));

        if (count($userIds) > 0) {

            $tokens = PushId::whereIn('user_id', $userIds)->where('id', '!=', $exclude_user_id)
                ->where('push_ids.status', 1)->get(['token', 'platform']);

            if (count($tokens) > 0) {
                $title = "Playlist Update";
                $description = $board->title . ' added a new episode ' . $episode_info_after_attach->title;
                $icon = '';
                $data = [
                    'title' => 'Updates to a Playlist you are following',
                    'type' => 'BNE',
                    'id' => $board->id,
                    'desc' => $board->title . ' added a new episode ' . $episode_info_after_attach->title,
                    'tray_icon' => ''
                ];

                $tokenData['android'] = [];
                $tokenData['iphone'] = [];
                foreach ($tokens as $tokenItem) {
                    $tokenData[$tokenItem->platform][] = $tokenItem->token;
                }

                $tokenData['android'] = array_unique($tokenData['android']);
                $tokenData['iphone'] = array_unique($tokenData['iphone']);
                
                $tokenData['android'] = array_filter(array_flatten($tokenData['android']));
                $tokenData['iphone'] = array_filter(array_flatten($tokenData['iphone']));

                //Log::info("Android TOKENS====".json_encode(array_unique($tokenData['android'])));
                //Log::info("Iphone TOKENS===".json_encode(array_unique($tokenData['iphone'])));
                
                if(count($tokenData['android']) > 0) {
                    FireBaseAndroid::sendFireBaseNotification($tokenData['android'], $title, $description, $icon, $data);                                
                }
                if(count($tokenData['iphone']) > 0) {
                    FireBase::sendFireBaseNotification($tokenData['iphone'], $title, $description, $icon, $data);
                }                
            }
        }
    }
}
