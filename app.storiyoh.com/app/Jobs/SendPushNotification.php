<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Traits\FireBase;
use App\Traits\FireBaseAndroid;
use App\Models\PushId;

class SendPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Order User Id
     */
    protected $user_id;

    /**
     * Push Title
     */
    protected $title;

    /**
     * Push Description
     */
    protected $description;

    /**
     * Push Icon
     */
    protected $icon;

    /**
     * Push Data
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id, $title, $description, $icon, $data)
    {
        $this->user_id = $user_id;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tokens = PushId::where('user_id', $this->user_id)->where('push_ids.status', 1)->get(['token', 'platform']);        
        $tokenData['android'] = [];
        $tokenData['iphone'] = [];
        foreach ($tokens as $tokenItem) {
            $tokenData[$tokenItem->platform][] = $tokenItem->token;
        }

        $tokenData['android'] = array_unique($tokenData['android']);
        $tokenData['iphone'] = array_unique($tokenData['iphone']);
        
        $tokenData['android'] = array_filter(array_flatten($tokenData['android']));
        $tokenData['iphone'] = array_filter(array_flatten($tokenData['iphone']));

        if(count($tokenData['android']) > 0) {
            FireBaseAndroid::sendFireBaseNotification($tokenData['android'], $this->title, $this->description, $this->icon, $this->data);
        }
        if(count($tokenData['iphone']) > 0) {
            FireBase::sendFireBaseNotification($tokenData['iphone'], $this->title, $this->description, $this->icon, $this->data);
        }
    }
}
