<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Traits\FireBase;
use App\Models\Show;
use App\Models\PushId;
use Log;
use function GuzzleHttp\json_encode;

class ShowPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $showIds;

    /**
     * @var string
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ids, $data)
    {
        $this->showIds = $ids;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $s_id = $this->showIds;
        $showIds[$s_id] = $this->data;
        
        //Log::info("Show Id == ".$s_id);
        //Log::info("Episode Ids == ". json_encode($showIds[$s_id]));

        $show = Show::where('id', $s_id)
            ->with(['subscribers' => function ($q) {
                $q->where('verified', 'Verified')
                    ->where('admin_status', 'Approved');
                $q->wherePivot('notify', 1);
            }])->first(['id', 'title']);

        $tokens = [];
        foreach ($show->subscribers as $sub) {
            $tokens[] = PushId::active()->where('user_id', $sub->id)->pluck('token')->all();
        }
        $tokens = array_filter(array_flatten($tokens));

        if (count($tokens) > 0) {
            $tokens = array_unique($tokens);
            $episode_count = count($showIds[$s_id]);
            $message2 = count($showIds[$s_id]) == 1 ? ' new episode available.' : ' new episodes available.';
            $mesage = str_replace('"', "'", $show->title) . ' has ' . $episode_count . $message2;

            $title = 'Storiyoh Update';
            $description = $mesage;
            $icon = '';
            $data = [
                'title' => 'Storiyoh Update',
                'type' => 'SNE',
                'desc' => $mesage,
                'ids' => $show->id,
                'tray_icon' => ''
            ];

            //Dispatch Queue to schedule
            FireBase::sendFireBaseNotification($tokens, $title, $description, $icon, $data);            
        }
    }
}
