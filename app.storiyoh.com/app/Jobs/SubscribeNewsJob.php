<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Traits\Helper;
use Log;

use App\Models\Show;
use App\Models\Chart;
use App\Models\Episode;
use App\Models\Subscriber;
use App\Models\User;

class SubscribeNewsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    protected $newsletter;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($newsletter,$data)
    {
        $this->newsletter = $newsletter;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $data = $this->data;
        $newsletter = $this->newsletter;

        $trending_podcasts = Show::whereIn('id',$newsletter->trending_podcasts)->get();
        $featured_collections = Chart::whereIn('id',$newsletter->featured_collections)->with('chart_poadcast')->get();
        $featured_episodes = Episode::whereIn('id',$newsletter->featured_episodes)->get();   
        $essentials = Show::whereIn('id',$newsletter->essentials)->get();
        $podcast_of_the_week = Show::where('id',$newsletter->podcast_of_the_week)->first(); 


        if($data['user_type'] == 'subscriber'){
            $users = Subscriber::where('subscription_status',1)->get();
        } elseif ($data['user_type'] == 'user') {
            $users = User::where('verified','Verified')->get();
        } elseif ($data['user_type'] == 'both') {
            $users = User::where('verified','Verified')->get();
            $subscribers = Subscriber::where('subscription_status',1)->get();
            $users = $users->merge($subscribers);
        }

        $filename = null;
        foreach ($users as $key => $user) {

            $mailData =   [
                'file_path' => 'emails.newsletter',
                'from_email' => $data['from_email'],
                'from_name' => $data['from_name'],
                'to_email' => $user['email'],
                'to_name' => $user['email'],
                'subject' => $data['subject'],
                'filename' => $filename,
                'data' => ['newsletter' => $newsletter, 
                'user' => $user, 
                    'trending_podcasts' => $trending_podcasts, 
                    'featured_collections' => $featured_collections, 
                    'featured_episodes' => $featured_episodes, 
                    'essentials' => $essentials, 
                    'podcast_of_the_week' => $podcast_of_the_week,
                    'newsletter_subject' => $data['subject']
                ],
            ];

            Helper::sendAllEmail($mailData);
        }                
    }



    public function failed()
    {
        error_log('Job failed');
    }


}
