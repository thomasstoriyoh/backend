<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Tzsk\ScrapePod\Facade\ScrapePod;
use App\Traits\Helper;
use App\Models\Show;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class UpdateShowInfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Show Id variable
     *
     * @var [type]
     */
    protected $show_id;

    /**
     * Itune Id variable
     *
     * @var [type]
     */
    protected $itune_id;

    /**
     * Show Feed Url variable
     *
     * @var [type]
     */
    protected $feed_url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show_id, $itune_id, $feed_url)
    {
        $this->show_id = $show_id;
        $this->itune_id = $itune_id;
        $this->feed_url = $feed_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = ScrapePod::original()->feed(trim($this->feed_url));

        try {
            $value = $response['data'];
            if (count($value) > 0) {
                $data = [
                    'title' => $value['title']
                ];

                if (!empty($value['image'])) {
                    $data['full_image'] = $value['image'];

                    //Create or Update Image
                    //$imageData = Helper::createImage($this->itune_id, $value['image']);
                    $imageData = '';
                    $itune_id = $this->itune_id;
                    $image = $value['image'];
                    if ($itune_id && $image) {
                        try {
                            $image = Image::make($image)->fit(600, 600, function ($img) {
                                $img->upsize();
                            });

                            $image_name = $itune_id . '.jpg';
                            $file = public_path('uploads/show_assets/' . $image_name);

                            // finally we save the image as a new file
                            $image->save($file);

                            //Upload Image onto S3 Bucket
                            Storage::disk('s3')->put('shows/' . $image_name, file_get_contents($file));

                            //Delete from local storage
                            if (file_exists($file)) {
                                unlink($file);
                            }
                        } catch (\Exception $e) {
                        }
                    }

                    if (!empty($imageData)) {
                        $data['image'] = $imageData;
                    }
                }

                $show = Show::find($this->show_id);
                if ($show) {
                    $show->timestamps = false;
                    $show->fill($data)->save();
                }
            }
        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
        }
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['show_update'];
    }
}
