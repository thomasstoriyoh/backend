<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\ShowItuneId;
use Log;

class UploadShowIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Upload Show Id Job');
        foreach ($this->data as $item) {
            try {
                $show_itunes_id = ShowItuneId::firstOrNew(['itune_id' => $item]);
                $show_itunes_id->fill(['itune_id' => $item])->save();
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                continue;
            }
        }
    }
}
