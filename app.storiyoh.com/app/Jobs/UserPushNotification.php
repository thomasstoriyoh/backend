<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Show;
use App\Models\PushId;
use App\Models\Episode;
use App\Traits\FireBase;
use App\Traits\FireBaseAndroid;
use App\Models\User;
use Log;

class UserPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Show Ids
     *
     * @var [type]
     */
    protected $show_id;

    /**
     * User Ids
     *
     * @var [type]
     */
    protected $userIds;

    /**
     * Episode Ids
     *
     * @var [type]
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($show_id, $userIds, $data)
    {
        $this->show_id = $show_id;
        $this->userIds = $userIds;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $show = Show::published()->where('id', $this->show_id)->first(['id', 'title', 'image']);

        $show_id = $this->show_id;
        $users = User::whereIn('id', $this->userIds)
            ->where('verified', 'Verified')->where('admin_status', 'Approved')
            ->with(['shows' => function ($q) use ($show_id) {
                $q->where('show_id', $show_id);
                $q->where('notify', 1);
            }])->get(['id']);

        //Log::info('User Id ::: ' . json_encode($this->userIds));

        $tokenData['android'] = [];
        $tokenData['iphone'] = [];
        foreach ($users as $userItem) {
            if ($userItem->shows->count() > 0) {
                $tokens = PushId::active()->where('user_id', $userItem->id)->get(['token', 'platform']);
                foreach ($tokens as $tokenItem) {
                    $tokenData[$tokenItem->platform][] = $tokenItem->token;
                }
            }
        }

        $tokenData['android'] = array_unique($tokenData['android']);
        $tokenData['iphone'] = array_unique($tokenData['iphone']);

        $tokenData['android'] = array_filter(array_flatten($tokenData['android']));
        $tokenData['iphone'] = array_filter(array_flatten($tokenData['iphone']));

        if (count($tokenData['android']) > 0 || count($tokenData['iphone']) > 0) {
            $episode = $this->data;
            $episode_count = count($episode);
            $ep_id = '';
            $epi_desc = '';
            if ($episode_count == 1) {
                $episodeInfo = Episode::find($episode[0], ['id', 'title', 'description']);
                $mesage = 'New Episode: ' . str_replace('"', "'", $episodeInfo->title);
                $ep_id = $episodeInfo->id;
                $epi_desc = '';
            } else {
                $mesage = 'New Episodes';
            }

            $title = str_replace('"', "'", $show->title);
            $description = $mesage;

            $icon = '';
            $data = [
                'title' => $title,
                'type' => 'SNE',
                'desc' => $mesage,
                'epi_desc' => $epi_desc,
                'id' => $show->id,
                'epi_id' => $ep_id,
                'episode_count' => $episode_count,
                'image' => !empty($show->image) ? $show->getWSImage(600) : '',
                'mediaUrl' => !empty($show->image) ? $show->getWSImage(600) : '',
                'tray_icon' => ''
            ];

            if (count($tokenData['android']) > 0) {
                FireBaseAndroid::sendFireBaseNotification($tokenData['android'], $title, $description, $icon, $data);
            }
            if (count($tokenData['iphone']) > 0) {
                FireBase::sendShowFireBaseNotification($tokenData['iphone'], $title, $description, $icon, $data);
            }
        }
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['push_notification'];
    }
}
