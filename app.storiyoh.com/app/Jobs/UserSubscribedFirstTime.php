<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Classes\Firebase as FirebaseTopic;
use App\Models\PushId;
use App\Models\User;

class UserSubscribedFirstTime implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $userId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id)
    {
        $this->userId = $user_id;        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::where('id', $this->userId)->where('verified', 'Verified')->where('admin_status', 'Approved')->first();
        if ($user) {
            $allShows = $user->subscribers()->wherePivot('notify', 1)->pluck('id')->all();
            if (count($allShows) > 0) {
                $uniqueShows = array_unique($allShows);
                $splitShows = array_chunk($uniqueShows, 10);
                foreach ($splitShows as $showArray) {
                    //\Log::info("Show Array ". json_encode($showArray));
                    //\Log::info("User Id ". $user->id);
                    dispatch(new FirebaseTopicSubscription($showArray, $user->id, 'subscribe_on_first_time'))->onQueue('subscribe-topic');
                }
            }
        }      
    }
}
