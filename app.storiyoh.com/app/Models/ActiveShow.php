<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActiveShow extends Model
{
    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Table name for the model.
     *
     * @var type
     */
    protected $table = 'show_active';

    /**
     * @var array
     */
    protected $fillable = ['show_id', 'itunes_id', 'feed_url', 'no_of_episode', 'cron_run_at', 'content_length', 'which_bucket', 'date_between'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     *
     * @param type $value
     * @return type
     */
    public function show($value)
    {
        $data = Show::find($value, ['title', 'image']);

        $image = empty($data->image) ? '' : $data->getImage(100);

        return [$data->title, $image];
    }
}
