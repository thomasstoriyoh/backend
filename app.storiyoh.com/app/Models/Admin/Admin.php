<?php

namespace App\Models\Admin;

use App\Traits\MakerCheckerManager;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\UserStampManager;
use App\Traits\HasPermission;
use App\Traits\UserActionLogManager;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable, UserStampManager, UserActionLogManager, HasPermission, MakerCheckerManager;
    
    /**
     * Activate Timestamps.
     * 
     * @var boolean
     */
    protected $userstamps = true;
    
    /**
     * Activate Action Logging.
     * 
     * @var boolean
     */
    protected $actionLog = true;
    
    /**
     * The field to use for logging.
     * 
     * @var string
     */
    protected $logField = 'first_name';

    /**
     * Guard to use for the Stamping.
     * 
     * @var string
     */
    protected $userGuard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'username', 'password', 'type',
        'status', 'admin_status', 'last_login', 'ip',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Fields to be treated as carbon instance.
     * 
     * @var array
     */
    protected $dates = ['last_login'];
    
    /**
     * Start userstamp engine.
     *
     * @param array $attributes
     */
    function __construct(array $attributes = array()) {
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        $this->manageMakerChecker();
    }

    /**
    * Is the logged in admin is a super admin.
    *
    * @return boolean
    */
    public function isSuperAdmin()
    {
        return ($this->type == 'SA');
    }

   /**
    * Is the admin active or not.
    *
    * @return boolean 
    */
    public function isActive()
    {
         return ($this->status == 'Active');
    }
    
    /**
    * Is the admin approved or not.
    *
    * @return boolean 
    */
    public function isApproved()
    {
         return ($this->admin_status == 'Approved');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name ." ". $this->last_name;
    }

    public function passwordSecurity()
    {
        return $this->hasOne(PasswordSecurity::class);
    }
}
