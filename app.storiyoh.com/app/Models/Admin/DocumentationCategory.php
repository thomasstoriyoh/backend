<?php

namespace App\Models\Admin;

use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use Illuminate\Database\Eloquent\Model;

class DocumentationCategory extends Model
{
    /**
     * Manage Userstamps and Logs.
     */
    use UserStampManager, UserActionLogManager;

    /**
     * Route created yet.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Table Name.
     *
     * @var string
     */
    protected $table = 'documentation_categories';

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * Fields allowed for mass assignment.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'documentation_category_id', 'status', 'order'];

    /**
     * Blog constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
    }

    /**
     * Relationship to it's own.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function child() {
        return $this->hasMany(DocumentationCategory::class, 'documentation_category_id');
    }

    /**
     * Relationship to it's own.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent() {
        return $this->belongsTo(DocumentationCategory::class, 'documentation_category_id');
    }

    /**
     * Relationship with the pages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pages() {
        return $this->hasMany(DocumentationPages::class);
    }

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

}
