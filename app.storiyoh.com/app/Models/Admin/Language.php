<?php

namespace App\Models\Admin;

use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use UserStampManager, UserActionLogManager;
    /**
     * Route created yet.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Table name for the model.
     *
     * @var string
     */
    protected $table = "languages";

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'locale', 'status'
    ];

    /**
     * Blog constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
    }

    /**
     * Scope for Published.
     *
     * @param $query
     * @return mixed
     */
    public function scopePublished($query) {
        return $query->whereStatus('Published');
    }
}
