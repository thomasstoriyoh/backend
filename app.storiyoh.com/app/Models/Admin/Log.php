<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'logs';

    public $timestamps = true;

    protected $fillable = ['title', 'module_id', 'user_id', 'type', 'item_id'];

    /**
     * Relationship with the Admin.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(Admin::class, 'user_id');
    }

    /**
     * Relationship with the Modules.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module() {
        return $this->belongsTo(Module::class, 'module_id');
    }

//    public function logDetails() {
//        if ( !empty($this->type)) {
//            $type = $this->type . "ed";
//            if ($this->type == 'Create' || $this->type == 'Delete') {
//                $type = $this->type . "d";
//            }
//            $log = "<b>" . $this->user->name . "</b> " . $type . " <b><i>" . $this->title . "</i></b> in <b>";
//            $log .= $this->module->title . "</i>";
//            return $log;
//        }
//        $log = $this->user->name . " taken UNKNOWN action on <i>" . $this->title . "</i> in <b>";
//        $log .= $this->module->title . "</i>";
//        return $log;
//    }
}
