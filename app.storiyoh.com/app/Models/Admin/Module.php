<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserStampManager;

class Module extends Model
{
    /**
     * Table name of the Model.
     *
     * @var string
     */
    protected $table = 'modules';

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'type', 'module_id', 'order', 'model', 'nested', 'table_name', 
        'controller', 'status', 'maker_checker', 'widget', 'has_maker_checker', 'has_details'
    ];

    /**
     * Relationship to it's own.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function child() {
        return $this->hasMany(Module::class, 'module_id');
    }

    /**
     * Relationship to it's own.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent() {
        return $this->belongsTo(Module::class, 'module_id');
    }

    /**
     * Relationship to it's own.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function forwarded() {
        return $this->belongsTo(Module::class, 'forward_to');
    }

    /**
     * Relationship with the Module Entry Views.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function view() {
        return $this->hasMany(ModuleEntryView::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getModelNameAttribute()
    {
        return $this->get('model');
    }
    
    /**
     * Published scope.
     * 
     * @param object $query
     * @return object
     */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

}
