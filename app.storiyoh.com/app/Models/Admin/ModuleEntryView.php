<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ModuleEntryView extends Model
{
    /**
     * Table name of the model.
     *
     * @var string
     */
    protected $table = 'module_entry_views';

    /**
     * Timestamps to manage.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Mass assignable fields.
     *
     * @var array
     */
    protected $fillable = ['view_date', 'module_id', 'item_id', 'count'];

    /**
     * Relationship with the modules.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module() {
        return $this->belongsTo(Module::class);
    }
}
