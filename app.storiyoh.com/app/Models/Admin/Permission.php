<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserStampManager;

class Permission extends Model
{
    
    /**
     * Table name of the model.
     *
     * @var string
     */
    protected $table = "admin_permissions";

    /**
     * Fillable fields for the model.
     *
     * @var array
     */
    protected $fillable = ['module_id', 'admin_id', 'create', 'read', 'update', 'delete'];

    /**
     * Timestamps to be considered.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Permission belongs to many modules.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module() {
        return $this->belongsTo(Module::class, 'module_id');
    }

    /**
     * Permission belongs to many admins.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin() {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}
