<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AudioFile extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'api_id', 'episode_id', 'mp3', 'audiosearch_mp3', 'duration', 'length', 'status', 'order'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function episode()
    {
        return $this->belongsTo(Episode::class);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        if (! empty($this->mp3)) {
            return $this->mp3;
        }

        return $this->audiosearch_mp3;
    }
}
