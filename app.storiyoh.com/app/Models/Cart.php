<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $fillable = [
        'user_id',
        'content_id',
        'type'
    ];


    public static function addItem($user, $request ){

        $content_id = $request->input('content_id');
        $content_type  = $request->input('type');
        $price = 0;

        // switch($content_type ){
        //     case 'show':
        //         $product_type = 1;
        //         break;
            
        //     case 'episode':
        //         $product_type = 2;
        //         break;
            
        //     default:
        //         $product_type = 3;
        //         break;
        // }

        // $order = Order::where([
        //     'user_id' => $user->id, 
        //     'product_id' => $content_id,
        //     'product_type' => $product_type
        // ])->first();
        

        // if(!$order){
        //     $price = $order->net_amount;
        // }

        // whether item exists in user cart
        $cart = static::where([
            'user_id' => $user->id, 
            'content_id' => $content_id,
            'type' => $content_type
        ])->first();

        if($cart){
            return $cart;
        }

        // add item to cart
        $cart = static::create([
            'user_id' => $user->id,
            'content_id' => $content_id,
            'type' => $content_type ,    
            ]);

        return $cart;
    }

}
