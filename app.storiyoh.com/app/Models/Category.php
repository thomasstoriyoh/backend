<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Syncable;

    /**
     * @var array
     */
    protected $fillable = [
        'parent_id', 'title', 'order',
        'created_by', 'updated_by'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
    */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent() {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() {
        return $this->hasMany(Category::class, 'parent_id');
    }

     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shows()
    {
        return $this->belongsToMany(Show::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function users() {
        return $this->belongsToMany(User::class)
            ->withTimestamps()->withPivot(['notify']);
    }
    
    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        $image = config('config.s3_url') . '/showcategory/' . $this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));
        
        return url('/image/' . $token);
    }
}
