<?php

namespace App\Models;

use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Chart extends Model
{
    use UserStampManager, UserActionLogManager;

    /**
     * @var bool
     */
    public static $routeRegistered = true;

    /**
     * @var bool
     */
    public $userstamps = true;

    /**
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * @var bool
     */
    protected $actionLog = true;

    /**
     * @var string
     */
    protected $logField = 'title';

    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->manageUserstamp();
        $this->manageActionLog();
    }

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'date', 'keywords', 'source_url', 'image',
        'featured', 'status', 'order', 'created_by', 'updated_by',
        'post_count', 'repost_count', 'share_external_count', 'share_dm_count'
    ];

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
    */
    public function scopePublished($query)
    {
        return $query->where('status', 'Published');
    }

    /**
     * Featured scope.
     *
     * @param object $query
     * @return object
    */
    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function chart_poadcast()
    {
        return $this->belongsToMany(Show::class)->withPivot('order')
            ->withTimestamps();
    }

    /**
     * Accessor for the Tag ids.
     *
     * @return mixed
     */
    public function getShowIdAttribute()
    {
        return $this->chart_poadcast()->pluck('title', 'id')->all();
    }

    /**
     * Accessor for the Post Date.
     *
     * @return mixed
     */
    public function getDateAttribute($value)
    {
        return empty($value) ? Carbon::now()->format('Y-m-d') : Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        $image = config('config.s3_url') . '/charts/' . $this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

        return url('/image/' . $token);
    }
}
