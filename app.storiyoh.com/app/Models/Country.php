<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MakerCheckerManager;
use App\Traits\UserStampManager;
use App\Traits\UserActionLogManager;

class Country extends Model
{
    use UserStampManager, UserActionLogManager, MakerCheckerManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'countries_iso_code_2', 'countries_iso_code_3', 'order_by'
    ];

    /**
     * Table name for the model.
     *
     * @var type
     */
    protected $table = 'countries';

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = false;

    /**
     * The user guard to use for detecting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = false;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * Country Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        //$this->manageUserstamp();
        //$this->manageActionLog();
        //$this->manageMakerChecker();
    }
}
