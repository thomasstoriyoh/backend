<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CronNewsletter extends Model
{
    protected $table = 'cron_newsletter';


    protected $fillable = ['newsletter_id','user_type','newsletter_type','subject','from_name','from_email','status'];
    
}
