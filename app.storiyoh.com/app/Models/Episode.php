<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Episode extends Model
{
    use Syncable, Searchable;

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAsIndex()
    {
        return 'episodes';
    }

    /**
     * @var array
     */
    protected $fillable = [
        'uuid', 'show_id', 'itunes_id', 'title', 'description', 'image',
        'duration', 'size', 'mp3', 'link', 'date_created', 'date_added',
        'status', 'order', 'created_by', 'updated_by', 'explicit',
        'listen_count', 'post_count', 'repost_count', 'share_external_count',
        'share_dm_count', 'playlist_count', 'queue_count', 'favorite_count',
        'clip_count', 'profile_pins_count', 'group_pins_count'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['show'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(EpisodeTag::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
//    public function audioFiles()
//    {
//        return $this->hasMany(AudioFile::class);
//    }

    /**
     * @return null|string
     */
    public function getAudioLink()
    {
        return $this->mp3 ? $this->mp3 : "";
    }

    /**
     * @return string|null
     */
    public function getDurationText()
    {
        return $this->duration ? gmdate('H:i:s', $this->duration) : "";
    }

    /**
     * @return AudioFile
     */
//    protected function getAudioFile()
//    {
//        return $this->audioFiles()->where(function($q) {
//            $q->orWhereNotNull('mp3');
//            $q->orWhereNotNull('audiosearch_mp3');
//        })->first();
//    }

    /**
     * shows
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function show()
    {
        return $this->belongsTo(Show::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_stream_data()
    {
        return $this->belongsToMany(User::class, 'user_stream_data', 'episode_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_download_data()
    {
        return $this->belongsToMany(User::class, 'user_download_data', 'episode_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_historical_data()
    {
        return $this->belongsToMany(User::class, 'user_historical_data', 'episode_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Relationship with Comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(EpisodeUserComment::class);
    }

    /**
     * Relationship with episode like
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function like()
    {
        return $this->belongsToMany(User::class, 'episode_user_like')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_episode_queue()
    {
        return $this->belongsToMany(User::class, 'user_queue', 'episode_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query)
    {
        return $query->where('status', 'Published');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activities()
    {
        return $this->morphMany(Feed::class, 'typeable');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...
        $customize['id'] = $array['id'];
        $customize['itunes_id'] = @$array['itunes_id'];
        $customize['show_id'] = @$array['show_id'];
        $customize['show_title'] = @$this->show->title;
        $customize['collection_name'] = @$this->show->collection_name;
        $customize['collection_censored_name'] = @$this->show->collection_censored_name;
        $customize['artist_name'] = @$this->show->artist_name;
        $customize['title'] = @$array['title'];
        $customize['description'] = @$array['description'];
        $customize['mp3'] = @$array['mp3'] ? 1 : 0;
        $customize['duration'] = @$array['duration'];
        $customize['categories'] = @implode(',', $this->show->categories()->pluck('title')->all());
        $customize['category_ids'] = json_encode(array_map('trim', $this->show->categories()->pluck('id')->all()));
        $customize['tags'] = @implode(',', $this->tags()->pluck('title')->all());
        $customize['language'] = @$this->show->language;
        $customize['country'] = @$this->show->country;
        $customize['status'] = @$array['status'] ? $array['status'] : 'Published';
        $customize['recency'] = strtotime(@$array['updated_at']);
        $customize['release_date'] = strtotime(@$array['date_added']);
        $customize['date_created'] = @$array['created_at'];

        return $customize;
    }

    /**
     * Get the title.
     *
     * @param  string  $value
     * @return string
     */
    public function getTitleAttribute($value)
    {
        return html_entity_decode($value, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Get the Description.
     *
     * @param  string  $value
     * @return string
     */
    public function getDescriptionAttribute($value)
    {
        return trim(str_replace("\n", '', html_entity_decode(strip_tags($value, '<b><i><ol><li><ul><a><br><blockquote><img>'), ENT_QUOTES, 'UTF-8')));
        //return trim(str_replace("\n", '', html_entity_decode(strip_tags($value), ENT_QUOTES, 'UTF-8')));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors()
    {
        return $this->belongsToMany(User::class, 'episode_author', 'episode_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function narrators()
    {
        return $this->belongsToMany(User::class, 'episode_narrator', 'episode_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function producers()
    {
        return $this->belongsToMany(User::class, 'episode_producer', 'episode_id', 'user_id')
            ->withTimestamps();
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function premium_pricing()
    {
        return $this->hasMany(PremiumPricingEpisode::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function episode_extra_info()
    {
        return $this->hasOne(EpisodeDetail::class);
    }

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        if (! is_null(env('DBLOCAL'))) {
            $image = 'uploads/episodes/' . $this->image;
            return url($image);
        } else {
            $image = config('config.s3_url') . '/episodes/' . $this->image;
            $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

            return url('/image/' . $token);
        }
    }
}
