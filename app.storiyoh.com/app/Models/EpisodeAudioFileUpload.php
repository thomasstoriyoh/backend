<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpisodeAudioFileUpload extends Model
{
    /**
     * @var array
    */
    protected $fillable = [
        'user_id', 'show_id', 'episode_id', 'title', 'duration', 'mp3', 'file_original_name', 'status'
    ];
}
