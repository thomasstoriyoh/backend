<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpisodeDetail extends Model
{
   /**
     * @var array
     */
    protected $fillable = [
        'title', 'user_id', 'show_id', 'episode_id', 'intro', 'language', 'rating', 'country', 'status', 'content_type', 
        'res_description', 'file_original_name', 'shots'
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function episode()
    {
        return $this->belongsTo(Episode::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        if (! is_null(env('DBLOCAL'))) {
            $image = 'uploads/episodes/' . $this->episode->image;
            return url($image);
        } else {
            $image = config('config.s3_url') . '/episodes/' . $this->episode->image;
            $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

            return url('/image/' . $token);
        }
    }

    /**
     * Get episode image
     */
    public function getEpisodeImage($w = null, $h = null)
    {
        if (! is_null(env('DBLOCAL'))) {
            $image = 'uploads/episodes/' . $this->episode->image;
            return url($image);
        } else {
            return $image = config('config.s3_url') . '/episodes/' . $this->episode->image;
        }
    }

    /**
     * Get S3 url for episode shots
     */
    public function getEpisodeShotImage($w = null, $h = null)
    {
        if (!is_null(env('DBLOCAL'))) {
            $image = 'uploads/episodes/shots/' . $this->shots;
            return url($image);
        } else {
            return $image = config('config.s3_url') . '/episodes/shots/' . $this->shots;
        }
    }

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getShotImage($w = null, $h = null)
    {
        if (!is_null(env('DBLOCAL'))) {
            $image = 'uploads/episodes/shots/' . $this->shots;
            return url($image);
        } else {
            $image = config('config.s3_url') . '/episodes/shots/' . $this->shots;
            $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

            return url('/image/' . $token);
        }
    }
}
