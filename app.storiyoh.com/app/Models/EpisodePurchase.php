<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpisodePurchase extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'episode_id', 'user_id', 'purchased_date'
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function episode()
    {
        return $this->belongsTo(Episode::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
