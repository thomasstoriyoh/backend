<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpisodeResource extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'temp_id', 'episode_id', 'res_title1', 'res_file1', 'res_title2', 'res_file2',
        'res_title3', 'res_file3', 'res_title4', 'res_file4', 'res_title5', 'res_file5',
        'res_title6', 'res_file6', 'res_title7', 'res_file7'
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function episode()
    {
        return $this->belongsTo(Episode::class);
    }

    /**
     * @var bool
     */
    public $timestamps = true;
}
