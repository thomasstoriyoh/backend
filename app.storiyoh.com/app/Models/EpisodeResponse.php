<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpisodeResponse extends Model
{
    use ResponseTrait;

    /**
     * @var array
     */
    protected $fillable = ['id', 'show_id', 'data', 'processed', 'processed_at'];

    /**
     * @var bool
     */
    public $timestamps = true;
}
