<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpisodeTag extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * @var bool
     */
    public $timestamps = true;
}
