<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpisodeTemp extends Model
{
    /**
     * Table name
     */
    protected $table = "episode_temp";

   /**
     * @var array
    */
    protected $fillable = [
        'audio_upload_id', 'user_id', 'show_id', 'title', 'intro', 'description', 'country', 'language', 'category_id',
        'rating', 'image', 'mp3', 'duration', 'authors', 'narrators', 'producers', 'aired_date', 'tags', 'pricing', 'status', 
        'res_description', 'file_original_name', 'shots'
    ];
}
