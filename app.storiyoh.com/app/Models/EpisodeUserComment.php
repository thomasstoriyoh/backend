<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EpisodeUserComment extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = "episode_user_comments";
    
    /**
     * Timestamps required.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fillable Fields for tags.
     *
     * @var array
     */
    protected $fillable = ['episode_id', 'user_id', 'comment_id', 'comment', 'approved'];
    
    /**
     * Relationship with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    /**
     * Relationship with Episode.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function episode() {
        return $this->belongsTo(Episode::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activities()
    {
        return $this->morphMany(Feed::class, 'typeable');
    }
}
