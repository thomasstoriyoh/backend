<?php

namespace App\Models;

use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
// use App\Models\Admin\Log;

class Factoid extends Model
{
    use UserStampManager, UserActionLogManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * BlogCategory constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        //$this->manageMakerChecker();
    }

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'episode_id', 'date_appear', 'status'
    ];

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

    /**
     * Save the Log in the Database.
     *
     * @param $model
     * @param $logType
     */
    protected function saveLog($model, $logType) {
        if ($logType != 'Login' && $this->validateModel($model)) {
            $log = new $model->logModel;
            $log->title = substr($model->{$model->getLogField()}, 0, 180);
            $log->module_id = $model->getModuleId();
            $log->item_id = $model->id;
            $log->type = @$logType;
            $log->user_id = @Auth::guard($model->getLogGuard())->user()->id;
            $log->save();
        }
    }

}
