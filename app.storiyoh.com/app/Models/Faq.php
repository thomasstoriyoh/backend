<?php

namespace App\Models;

use App\Traits\MakerCheckerManager;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use UserStampManager, UserActionLogManager, MakerCheckerManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * FaqCategory constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        $this->manageMakerChecker();
    }
    /**
     * @var array
     */
    protected $fillable = [
        'faq_categories_id','title','answer','order','status','admin_status','created_by', 'updated_by'
    ];

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

    /**
     * Approved scope.
     *
     * @param object $query
     * @return object
     */
    public function scopeApproved($query) {
        return $query->where('admin_status', 'Approved');
    }

    public function category() {
        return $this->hasOne('App\Models\Faqcategory','id','faq_categories_id');
    }
}
