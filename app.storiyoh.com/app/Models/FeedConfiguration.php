<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\MakerCheckerManager;
use App\Traits\UserStampManager;
use App\Traits\UserActionLogManager;

class FeedConfiguration extends Model
{
    use UserStampManager, UserActionLogManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = false;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';
    
    public $title = "Feed Configuration";

    /**
     * App Version constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        //$this->manageMakerChecker();
    }

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    
    protected $fillable = [
        'trending', 'trending_shows', 'trending_boards', 'trending_episodes', 'recommended_users'
    ];    
}
