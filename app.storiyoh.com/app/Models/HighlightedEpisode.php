<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use App\Traits\MakerCheckerManager;

class HighlightedEpisode extends Model
{
    use UserStampManager, UserActionLogManager, MakerCheckerManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * @var array
     */
    protected $fillable = ['episode_id', 'order', 'status', 'admin_status', 'created_by', 'updated_by'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = false;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * BlogCategory constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        $this->manageMakerChecker();
    }

    /**
     * shows
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function episode()
    {
        return $this->belongsTo(Episode::class);
    }

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query)
    {
        return $query->where('status', 'Published');
    }

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopeAdminPublished($query)
    {
        return $query->where('admin_status', 'Approved');
    }
}
