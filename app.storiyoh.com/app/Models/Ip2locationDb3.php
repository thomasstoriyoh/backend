<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ip2locationDb3 extends Model
{
    /**
     * Table name
     * 
     * @var boolean
    */
    protected $table = 'ip2location_db3';

    /**
     * Deactivate Timestamps.
     * 
     * @var boolean
    */
    public $timestamps = false;
    
    /**
     * Disable increment
     *     
    */
    public $incrementing = false;

    /**
     * Primary Key
     *     
    */
    protected $primaryKey = 'ip_to';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = ['ip_from', 'ip_to', 'country_code', 'country_name', 'region_name', 'city_name'];
}
