<?php

namespace App\Models;

use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ItunesTopPodcast extends Model
{
    use UserStampManager, UserActionLogManager;
    
    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    
    /**
     * @var array
     */
    protected $fillable = [
        'title', 'rss_url', 'processed', 'status', 'created_by', 'updated_by'
    ];       

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for detecting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    //protected $title = 'Itunes Top Podcast';

    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->manageUserstamp();
        $this->manageActionLog();            
    }    
}
