<?php

namespace App\Models;

//use App\Traits\MakerCheckerManager;
use App\Traits\UserActionLogManager;
//use App\Traits\UserStampManager;

use Illuminate\Database\Eloquent\Model;

class Last_synch extends Model
{
    use UserActionLogManager;

    protected $table = 'last_synch';

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = false;

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = false;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = false;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * BlogCategory constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        //$this->manageUserstamp();
        //$this->manageActionLog();
        //$this->manageMakerChecker();
    }

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'type'
    ];

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
}
