<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManualSync extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['type', 'sync_date', 'sync_count'];

    /**
     * @var bool
     */
    public $timestamps = true;
}
