<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use App\Traits\MakerCheckerManager;

class MarketplaceWebPromotion extends Model
{
    use UserStampManager, UserActionLogManager, MakerCheckerManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * BlogCategory constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        $this->manageMakerChecker();
    }

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'type', 'content_id', 'image', 'order', 'status', 'admin_status', 'created_by', 'updated_by'
    ];

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query)
    {
        return $query->where('status', 'Published');
    }

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopeAdminPublished($query)
    {
        return $query->where('admin_status', 'Approved');
    }

    /**
     * @param $w
     * @param $h
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getImage($w = null, $h = null)
    {
        $image = config('config.s3_url') . '/marketplace_web_promo/' . $this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

        return url('/image/' . $token);
    }
}
