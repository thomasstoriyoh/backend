<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MakerCheckerManager;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;

class Network extends Model
{    
    use UserStampManager, UserActionLogManager, MakerCheckerManager;
    
    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = false;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * BlogCategory constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        //$this->manageMakerChecker();
    }

    /**
     * @var array
     */
    protected $fillable = ['title', 'image', 'featured', 'created_by', 'updated_by'];
    
    /**
     * Published scope.
     * 
     * @param object $query
     * @return object
    */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

    /**
     * Featured scope.
     * 
     * @param object $query
     * @return object
    */
    public function scopeFeatured($query) {
        return $query->where('featured', 1);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shows()
    {
        return $this->hasMany(Show::class);
    }

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        $image = config('config.s3_url') . '/networks/' . $this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

        return url('/image/' . $token);
    }
}
