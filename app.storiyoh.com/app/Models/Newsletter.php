<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\MakerCheckerManager;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;

class Newsletter extends Model
{
    use UserStampManager, UserActionLogManager, MakerCheckerManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * BlogCategory constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        $this->manageMakerChecker();
    }

     


    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'title','newsletter_title','description','image','btn_text','btn_link','trending_podcasts','featured_collections','featured_episodes','essentials','podcast_of_the_week','admin_status','image', 'status', 'order',
    ];

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

    /**
     * Approved scope.
     *
     * @param object $query
     * @return object
     */
    public function scopeApproved($query) {
        return $query->where('admin_status', 'Approved');
    }

    /**
     * @param $w
     * @param $h
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getImage($w, $h)
    {
        return url('uploads/newsletter/thumbs/'.$w.'_'.$h.'_' . $this->image);
    }


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trending_podcasts' => 'array',
        'featured_collections' => 'array',
        'featured_episodes' => 'array',
        'essentials' => 'array',
    ];

    
}
