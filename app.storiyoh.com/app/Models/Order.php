<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_number', 'cust_transaction_id', 'invoice_date', 'user_id', 'purchase_type', 'feature_id',
        'product_type', 'product_id', 'invoice_type', 'country_id', 'billing_city', 'billing_state', 'billing_country',
        'ip_address', 'gateway', 'platform', 'payment_mode', 'transaction_id', 'billing_type', 'currency', 'net_amount',
        'tax_name', 'tax_percent', 'tax_amount', 'invoice_total', 'status', 'additional_status', 'token', 'response',
        'cron_update_date', 'created_by', 'updated_by', 'giftBy', 'gift_message'
    ];

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with Country
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function country() {
        return $this->belongsTo(MarketplaceCountry::class);
    }

    /**
     * Relationship with Sender
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sender() {
        return $this->belongsTo(User::class, 'giftBy');
    }

    /**
     * Relationship with Show
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function show()
    {
        return $this->belongsTo(Show::class, 'product_id');
    }

    /**
     * Relationship with Episode
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function episode()
    {
        return $this->belongsTo(Episode::class, 'product_id');
    }
}
