<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    /**
     * @var bool
     */
    public static $routeRegistered = true;

    protected $table = "boards";
    
    /**
     * @var bool
     */
    public $userstamps = false;
    
    /**
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * @var bool
     */
    protected $actionLog = false;

    /**
     * @var string
     */
    protected $logField = 'title';

    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);            
    }
    
    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'image', 'private', 'featured'
    ];

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user() {
        return $this->belongsTo(User::class);
    }    

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        //$image = public_path('uploads/show_assets/' . $this->image);
        $image = config('config.s3_url')."/boards/".$this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));
        return url('/image/' . $token);
    }

    /**
     * Featured scope.
     *
     * @param object $query
     * @return object
    */
    public function scopeFeatured($query) {
        return $query->where('featured', 1);
    } 
    
    /**
     * Relationship with Episodes
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function episodes() {
        return $this->belongsToMany(Episode::class, 'board_episode', 'board_id', 'episode_id')
            ->withTimestamps();
    }
}
