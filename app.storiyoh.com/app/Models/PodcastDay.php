<?php

namespace App\Models;

use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PodcastDay extends Model
{
    use UserStampManager, UserActionLogManager;
    
    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Resource table name.
     *
     * @var string
     */
    protected $table = "podcast_of_the_day";

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'show_id', 'date', 'status', 'created_by', 'updated_by'
    ];       

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for detecting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->manageUserstamp();
        $this->manageActionLog();            
    }

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

    /**
     * Relationship with Show.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function show() {
        return $this->belongsTo(Show::class);
    }
}
