<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;

class PopularShow extends Model
{
    use UserStampManager, UserActionLogManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Resource table name.
     *
     * @var string
     */
    //protected $table = "podcast_of_the_day";

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'category_id', 'order', 'created_by', 'updated_by'
    ];

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for detecting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->manageUserstamp();
        $this->manageActionLog();
    }

    /**
     * Relationship with Show.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function shows()
    {
        return $this->belongsToMany(Show::class, 'show_popular_show')
            ->withPivot(['category_id', 'sequence'])->withTimestamps();
    }
}
