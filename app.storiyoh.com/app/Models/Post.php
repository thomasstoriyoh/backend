<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * Table name for the model.
     *
     * @var string
     */
    //protected $table = "boards";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'post', 'tagIds', 'status', 'post_id', 'likes_count', 'comment_count',
        'repost_count', 'report_count', 'share_external_count', 'share_dm_count'
    ];

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }    

    /**
     * Relationship with Like and Post
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes()
    {
        return $this->belongsToMany(User::class, 'like_post', 'post_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Relationship with Comment
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(PostComment::class);
    }    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repost_data() {
        return $this->hasMany(Post::class, 'post_id');
    }
}
