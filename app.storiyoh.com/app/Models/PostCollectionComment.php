<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCollectionComment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'comment', 'tagIds', 'status', 'reply_post_id'
    ];

    /**
     * Relationship with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with Post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post() {
        return $this->belongsTo(PostCollection::class, 'post_collection_id');
    }
}
