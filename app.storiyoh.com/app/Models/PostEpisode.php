<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostEpisode extends Model
{
    /**
     * Table name for the model.
     * 
     * @var string
     */
    //protected $table = "boards";    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'episode_id', 'post', 'status', 'post_id', 'likes_count', 'comment_count', 
        'repost_count', 'report_count', 'share_external_count', 'share_dm_count'
    ];

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with Episode
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function episode() {
        return $this->belongsTo(Episode::class);
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes() {
        return $this->belongsToMany(User::class, 'like_post_episode', 'post_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Relationship with Comments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany(PostEpisodeComment::class);
    }

    /**
     * Relationship with Episode Model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post_data()
    {
        return $this->belongsTo(Episode::class, 'episode_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repost_data() {
        return $this->hasMany(PostEpisode::class, 'post_id');
    }
}
