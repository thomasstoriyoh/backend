<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostPlaylist extends Model
{
    /**
     * Table name for the model.
     * 
     * @var string
     */
    //protected $table = "boards";    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'playlist_id', 'post', 'status', 'post_id', 'likes_count', 'comment_count', 
        'repost_count', 'report_count', 'share_external_count', 'share_dm_count'
    ];

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with Playlist
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function playlist() {
        return $this->belongsTo(Board::class);
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes() {
        return $this->belongsToMany(User::class, 'like_post_playlist', 'post_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Relationship with Comments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany(PostPlaylistComment::class);
    }

    /**
     * Relationship with Playlist Model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post_data()
    {
        return $this->belongsTo(Board::class, 'playlist_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repost_data() {
        return $this->hasMany(PostPlaylist::class, 'post_id');
    }
}
