<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostShow extends Model
{
    /**
     * Table name for the model.
     *
     * @var string
     */
    //protected $table = "boards";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'show_id', 'user_id', 'post', 'status', 'post_id', 'likes_count', 'comment_count',
        'repost_count', 'report_count', 'share_external_count', 'share_dm_count'
    ];

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with Show
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function show()
    {
        return $this->belongsTo(Show::class);
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes()
    {
        return $this->belongsToMany(User::class, 'like_post_show', 'post_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Relationship with Comments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(PostShowComment::class);
    }

    /**
     * Relationship with Show Model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post_data()
    {
        return $this->belongsTo(Show::class, 'show_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repost_data() {
        return $this->hasMany(PostShow::class, 'post_id');
    }
}
