<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostSmartPlaylist extends Model
{
    /**
     * Table name for the model.
     * 
     * @var string
     */
    //protected $table = "boards";    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'smart_playlist_id', 'post', 'status', 'post_id', 'likes_count', 'comment_count', 
        'repost_count', 'report_count', 'share_external_count', 'share_dm_count'
    ];

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship with Smart Playlist
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function smart_playlist() {
        return $this->belongsTo(SmartList::class);
    }
    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes() {
        return $this->belongsToMany(User::class, 'like_post_smart_playlist', 'post_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Relationship with Comments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany(PostSmartPlaylistComment::class);
    }

    /**
     * Relationship with Smart Playlist Model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post_data()
    {
        return $this->belongsTo(SmartPlaylist::class, 'smart_playlist_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repost_data() {
        return $this->hasMany(PostSmartPlaylist::class, 'post_id');
    }
}
