<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\UserStampManager;

class PremiumFeatureHistoric extends Model
{
    use UserStampManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = false;

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = false;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    //protected $logField = 'feature_name';
    
    public $title = "Premium Feature Historic";

    /**
     * App Version constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);        
    }

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    
    protected $fillable = [
        'premium_feature_id', 'update_date', 'apple_product_id', 'price_in_inr', 'price_in_usd', 'price_in_aed', 
        'web_price_in_inr', 'web_price_in_usd', 'web_price_in_aed', 'created_by', 'updated_by'
    ];
}
