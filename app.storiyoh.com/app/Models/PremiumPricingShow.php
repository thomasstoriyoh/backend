<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PremiumPricingShow extends Model
{
   /**
     * @var array
     */
    protected $fillable = [
        'show_id', 'country_id', 'currency_id', 'apple_product_id', 'android_product_id', 
        'web_price', 'price', 'markup_price', 'commission_price', 'selling_price'
    ];
}
