<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PremiumPricingShowsHistory extends Model
{
    /**
     * 
     */
    protected $table = "premium_pricing_shows_history";
    
    /**
     * @var array
     */
    protected $fillable = [
        'premium_pricing_id', 'show_id', 'country_id', 'currency_id', 'apple_product_id', 
        'android_product_id', 'web_price', 'price', 'markup_price', 'commission_price', 'selling_price'
    ];
}
