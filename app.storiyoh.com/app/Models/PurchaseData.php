<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tzsk\Crypt\Facade\StrCrypt;

class PurchaseData extends Model
{
    protected $table = "purchase_data";
    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_number', 'invoice_date', 'user_id', 'purchase_type', 'feature_id', 
        'product_type', 'product_id', 'gateway', 'vendor', 'payment_mode', 'transaction_id', 'billing_type',
        'currency', 'invoice_amount', 'status', 'additional_status', 'token', 'response', 'platform', 
        'created_by', 'updated_by'
    ];

    /** */
    // public function setTransactionIDAttribute($value) {
    //     $this->attributes['transaction_id'] = StrCrypt::encrypt($value);
    // }
}
