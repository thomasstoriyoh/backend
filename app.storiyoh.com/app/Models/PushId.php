<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushId extends Model
{
    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'platform','token', 'uuid', 'status',
    ];

    /**
     * Timestamps.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Active Pushes.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query) {
        return $query->where('status', 1);
    }

    /**
     * Relationship with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
