<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushSyncTimestamp extends Model
{
    /**
     * Table name for the model.
     *
     * @var string
     */
    protected $table = "push_sync_timestamp";
    
    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = [
        'last_feed_id'
    ];

    /**
     * Timestamps.
     *
     * @var bool
     */
    public $timestamps = true;
}
