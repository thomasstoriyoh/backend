<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponseCount extends Model
{
    /**
     * @var string 
     */
    protected $connection = 'cmysql';
    
    /**
     * @var array
     */
    protected $fillable = ['type', 'from_count'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @param $query
     * @return ResponseCount|null
     */
    public function scopeShow($query)
    {
        return @$query->where('type', 'Show');
    }

    /**
     * @param $query
     * @return ResponseCount|null
     */
    public function scopeEpisode($query)
    {
        return @$query->where('type', 'Episode');
    }

}
