<?php
namespace App\Models;


trait ResponseTrait
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeUnprocessed($query)
    {
        return $query->where('processed', false);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeProcessed($query)
    {
        return $query->where('processed', true);
    }
}