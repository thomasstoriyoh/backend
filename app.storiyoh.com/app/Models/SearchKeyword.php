<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchKeyword extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['keyword', 'show_id', 'type', 'count'];
    
    /**
     * @var bool
     */
    public $timestamps = true;
}
