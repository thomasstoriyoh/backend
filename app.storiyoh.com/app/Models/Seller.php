<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'email', 'contact_no', 'image', 'overview', 'url', 'media_link_fb', 'media_link_twitter',
        'media_link_linkedin', 'media_link_instagram', 'status'
    ];

    /**
     * Status scope.
     *
     * @param object $query
     * @return object
     */
    public function isEnabled()
    {
        return ($this->status == 'Enabled');
    }

    /**
     * Relationship with registered User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
    * @param null $w
    * @param null $h
    * @return string
    */
    public function getImage($w = null, $h = null)
    {
        if (is_null(env('DBLOCAL'))) {
            $image = config('config.s3_url') . '/sellers/' . $this->image;
            $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));
        } else {
            $image = public_path('uploads/sellers/' . $this->image);
            $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));            
        }

        return url('/image/' . $token);
    }
}
