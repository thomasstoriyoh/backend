<?php

namespace App\Models;

use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Show extends Model
{
    use UserActionLogManager, UserStampManager, Syncable, Searchable;

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAsIndex()
    {
        return 'shows';
    }

    /**
     * @var bool
     */
    public static $routeRegistered = true;

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var bool
     */
    public $userstamps = true;

    /**
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * @var bool
     */
    protected $actionLog = true;

    /**
     * @var string
     */
    protected $logField = 'title';

    /**
     * @var array
     */
    protected $fillable = [
        'itunes_id', 'network_id', 'title', 'language', 'description', 'full_image', 'image', 'feed_url',
        'web_url', 'artist_name', 'collection_name', 'collection_censored_name', 'country', 'status',
        'no_of_episode', 'no_of_subscribers', 'content_length', 'order', 'created_by', 'updated_by',
        'featured', 'last_update_timestamp', 'post_count', 'repost_count', 'share_external_count',
        'share_dm_count', 'playlist_count', 'collection_count', 'profile_pins_count', 'group_pins_count',
        'user_id', 'rating', 'content_type', 'explicit', 'btn_label'
    ];

    /**
     * Show constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
    }

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
    */
    public function scopePublished($query)
    {
        return $query->where('status', 'Published');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /*public function network()
    {
        return $this->belongsTo(Network::class);
    }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function episodes()
    {
        return $this->hasMany(Episode::class);
    }

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        //$image = public_path('uploads/show_assets/' . $this->image);
        $image = config('config.s3_url') . '/shows/' . $this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

        return url('/image/' . $token);
    }

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getWSImage($w = null, $h = null)
    {
        //$image = url('uploads/show_assets/' . $this->image);
        //$image = public_path('uploads/show_assets/' . $this->image);

        $image = config('config.s3_url') . '/shows/' . $this->image;
        //dd($image);
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

        //return config('config.storiyoh_backend_url') . '/image/' . $token;

        return url('/image/' . $token);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscribers()
    {
        return $this->belongsToMany(User::class)
          ->withTimestamps()->withPivot(['notify']);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activities()
    {
        return $this->morphMany(Feed::class, 'typeable');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\Hasone
     */
    public function active()
    {
        return $this->hasOne(ActiveShow::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\Hasone
     */
    public function network()
    {
        return $this->belongsTo(Network::class);
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...
        $customize['id'] = $array['id'];
        $customize['itunes_id'] = @$array['itunes_id'];
        $customize['title'] = @$array['title'];
        $customize['feed_url'] = @$array['feed_url'];
        $customize['artist_name'] = @$array['artist_name'];
        $customize['collection_name'] = @$array['collection_name'];
        $customize['collection_censored_name'] = @$array['collection_censored_name'];
        $customize['description'] = @$array['description'];
        $customize['no_of_episode'] = @$array['no_of_episode'];
        $customize['categories'] = @implode(',', $this->categories()->pluck('title')->all());
        $customize['category_ids'] = json_encode(array_map('trim', $this->categories()->pluck('id')->all()));
        $customize['language'] = @$array['language'];
        $customize['country'] = @$array['country'];
        $customize['status'] = @$array['status'] ? $array['status'] : 'Draft';
        $customize['recency'] = strtotime($array['updated_at']);
        $customize['date_created'] = @$array['created_at'];

        return $customize;
    }

    /**
     * Now fetch the category type list.
     *
     * @return mixed
     */
    public function getCategoryIdAttribute()
    {
        return $this->categories()->pluck('id')->all();
    }

    /**
     * Featured scope.
     *
     * @param object $query
     * @return object
    */
    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    /**
     * Get the title.
     *
     * @param  string  $value
     * @return string
     */
    public function getTitleAttribute($value)
    {
        return html_entity_decode($value, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Get the Description.
     *
     * @param  string  $value
     * @return string
     */
    public function getDescriptionAttribute($value)
    {
        return trim(str_replace("\n", '', html_entity_decode(strip_tags($value), ENT_QUOTES, 'UTF-8')));
    }

    /**
     * This function is use for get all collection
     *
     * @return void
     */
    public function getCollection()
    {
        return $this->belongsToMany(Chart::class)
            ->withTimestamps();
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activities_show()
    {
        return $this->morphMany(ShowNotification::class, 'typeable');
    }

    /**
     * This function is use for get all collection
     *
     * @return void
     */
    public function getSmartList()
    {
        return $this->belongsToMany(SmartPlaylist::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function premium_pricing() {
        return $this->hasMany(PremiumPricingShow::class);
    }

    /**
     * This function is use for get user info
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get Show Image
     */
    public function getSeriesImage($w = null, $h = null)
    {
        if (! is_null(env('DBLOCAL'))) {
            $image = 'uploads/shows/' . $this->image;
            return url($image);
        } else {
            return $image = config('config.s3_url') . '/shows/' . $this->image;            
        }
    }
}
