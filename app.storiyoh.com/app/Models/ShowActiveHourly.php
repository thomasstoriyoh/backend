<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowActiveHourly extends Model
{
    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = false;

    /**
     * Table name for the model.
     *
     * @var type
     */
    protected $table = 'show_active_hourly';
    
    /**
     * @var array
     */
    protected $fillable = ['show_id', 'itunes_id', 'feed_url', 'cron_run_at', 'content_length', 'date_between', 'error', 'has_hub', 'hub_url', 'has_subscribe'];

    /**
     * @var bool
     */
    public $timestamps = true; 
}
