<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowActiveSingleEpisode extends Model
{
    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = false;
    
    /**
     * Table name for the model.
     *
     * @var type
     */
    protected $table = 'show_active_single_episode_temporary';
    
    /**
     * @var array
     */
    protected $fillable = ['show_id', 'itunes_id', 'feed_url', 'cron_run_at', 'no_of_episodes', 'error'];

    /**
     * @var bool
     */
    public $timestamps = true; 

    /**
     * shows
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function show()
    {
        return $this->belongsTo(Show::class);
    }
}
