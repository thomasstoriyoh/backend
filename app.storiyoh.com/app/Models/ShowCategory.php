<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;

class ShowCategory extends Model
{
    use UserStampManager, UserActionLogManager;
    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    protected $table = 'categories';

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';

    /**
     * BlogCategory constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        //$this->manageMakerChecker();
    }

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'image', 'parent_id', 'status'
    ];

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

    /**
     * Save the Log in the Database.
     *
     * @param $model
     * @param $logType
     */

    public function getImage($w = null, $h = null)
    {
        // return url('uploads/showcategory/thumbs/'.$w.'_'.$h.'_' . $this->image);
        $image = config('config.s3_url') . '/showcategory/' . $this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

        return url('/image/' . $token);
    }
}
