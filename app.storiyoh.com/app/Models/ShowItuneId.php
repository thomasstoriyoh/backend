<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowItuneId extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['itune_id', 'processed', 'processed_at'];

    /**
     * @var bool
     */
    public $timestamps = true;
}
