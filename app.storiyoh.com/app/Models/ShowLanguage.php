<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowLanguage extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['short_code', 'title', 'status', 'order', 'created_by', 'updated_by'];
    
    /**
     * @var bool
     */
    public $timestamps = true;
}
