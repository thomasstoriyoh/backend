<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowNotification extends Model
{
    /**
     * Resource table name.
     *
     * @var string
     */
    protected $table = "show_notifications";
    
    /**
     * @var array
     */
    protected $fillable = ['typeable_type', 'typeable_id', 'data', 'type', 'created_at', 'updated_at'];

    /**
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Morphing it to all the modules having Feeds.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function typeable() {
        return $this->morphTo();
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_notification_status() {
        return $this->belongsToMany(User::class, 'show_notification_status', 'notification_id', 'user_id')
        ->withPivot('typeable_id', 'type', 'status')->withTimestamps();
    }
}
