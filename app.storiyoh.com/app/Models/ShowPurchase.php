<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowPurchase extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'show_id', 'user_id', 'purchased_date'
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function show()
    {
        return $this->belongsTo(Show::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
