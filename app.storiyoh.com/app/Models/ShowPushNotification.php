<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowPushNotification extends Model
{
    /**
     * Table name for the model.
     *
     * @var type
     */
    protected $table = 'show_push_notifications';
    
    /**
     * @var array
     */
    protected $fillable = ['show_id', 'userIds', 'data', 'status'];

    /**
     * @var bool
     */
    public $timestamps = true; 
}
