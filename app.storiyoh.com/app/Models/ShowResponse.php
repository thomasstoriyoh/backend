<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowResponse extends Model
{
    use ResponseTrait;

    /**
     * @var array
     */
    protected $fillable = ['id', 'data', 'processed', 'processed_at'];

    /**
     * @var bool
     */
    public $timestamps = true;
}
