<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmartList extends Model
{
    /**
     * @var bool
     */
    public static $routeRegistered = true;

    /**
     * Table name for the model.
     * 
     * @var string
     */
    protected $table = "smart_playlists";
    
    /**
     * @var bool
     */
    public $userstamps = false;
    
    /**
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * @var bool
     */
    protected $actionLog = false;

    /**
     * @var string
     */
    protected $logField = 'title';

    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);            
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'image', 'private', 'featured'
    ];
    
    /**
     * Relationship with Shows
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shows() {
        return $this->belongsToMany(Show::class, 'show_smart_playlist', 'smart_playlist_id', 'show_id')
            ->withTimestamps();
    }
        
    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        //$image = public_path('uploads/show_assets/' . $this->image);
        $image = config('config.s3_url')."/smart_playlists/".$this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));
        return url('/image/' . $token);
    }

     /**
     * Featured scope.
     *
     * @param object $query
     * @return object
    */
    public function scopeFeatured($query) {
        return $query->where('featured', 1);
    }
}
