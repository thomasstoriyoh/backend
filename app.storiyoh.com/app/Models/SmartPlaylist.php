<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmartPlaylist extends Model
{
    /**
     * Table name for the model.
     *
     * @var string
     */
    //protected $table = "smart_playlists";

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    //protected $touches = ['shows'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'image', 'private', 'featured',
        'post_count', 'repost_count', 'share_external_count', 'share_dm_count',
        'followers_count', 'profile_pins_count', 'group_pins_count'
    ];

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany(User::class)
            ->withTimestamps();
    }

    /**
     * Relationship with Categories
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class)
            ->withTimestamps();
    }

    /**
     * Relationship with Shows
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shows()
    {
        return $this->belongsToMany(Show::class)
            ->withTimestamps();
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activities()
    {
        return $this->morphMany(Feed::class, 'typeable');
    }

    /**
     * @param null $w
     * @param null $h
     * @return string
     */
    public function getImage($w = null, $h = null)
    {
        //$image = public_path('uploads/show_assets/' . $this->image);
        $image = config('config.s3_url') . '/smart_playlists/' . $this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

        return url('/image/' . $token);
    }

    /**
     * Relationship with all smartlist listing
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function my_lists()
    {
        return $this->belongsToMany(User::class, 'my_followed_and_own_smartlists', 'smart_playlist_id', 'user_id')
            ->withPivot('board_type')->withTimestamps();
    }

    /**
    * following smart playlist
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    public function following_user()
    {
        return $this->belongsToMany(User::class)
            ->withTimestamps();
    }

    /**
     * Featured scope.
     *
     * @param object $query
     * @return object
    */
    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }
}
