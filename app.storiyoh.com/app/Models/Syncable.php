<?php
namespace App\Models;

use App\Audiosearch\ManualQuota;
use App\Jobs\ManualSyncJob;
use Cache;

trait Syncable
{
    /**
     * @return bool
     */
    public function isSyncable()
    {
        $class = class_basename(__CLASS__);

        return $this->getQuota($class) > $this->getUsedQuota($class);
    }

    /**
     * @return int
     */
    public function availableQuota()
    {
        $class = class_basename(__CLASS__);

        return (int) ($this->getQuota($class) - $this->getUsedQuota($class));
    }

    /**
     * @return bool
     */
    public function isInProgress()
    {
        return $this->isSyncing() OR $this->isProcessing();
    }

    /**
     * @return bool
     */
    public function isSyncing()
    {
        $class = class_basename(__CLASS__);

        return Cache::has('SyncRunning.' . $class);
    }

    /**
     * @return bool
     */
    public function isProcessing()
    {
        $class = class_basename(__CLASS__);

        return Cache::has('ProcessRunning.' . $class);
    }

    /**
     * @param $class
     * @return mixed
     */
    protected function getQuota($class)
    {
        $allowed = new ManualQuota();
        $method = "get" . $class;

        return $allowed->$method();
    }

    /**
     * @param $class
     * @return int
     */
    protected function getUsedQuota($class)
    {
        $today = @date('Y-m-d');
        $sync = ManualSync::where('type', $class)->where('sync_date', $today)->first();
        $used = $sync ? $sync->sync_count : 0;

        return (int) $used;
    }

}