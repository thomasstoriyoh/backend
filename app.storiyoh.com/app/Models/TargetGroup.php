<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\MakerCheckerManager;
use App\Traits\UserStampManager;
use App\Traits\UserActionLogManager;

class TargetGroup extends Model
{
    use UserStampManager, UserActionLogManager, MakerCheckerManager;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;
    
    /**
     * Table name for the model.
     * 
     * @var string
     */
    protected $table = "target_groups";

    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Userstamps to maintain.
     *
     * @var bool
     */
    public $userstamps = true;

    /**
     * The user guard to use for deletcting the logged in user.
     *
     * @var string
     */
    protected $userGuard = 'admin';

    /**
     * Activate the logging.
     *
     * @var bool
     */
    protected $actionLog = true;

    /**
     * The field value to log alongside.
     *
     * @var string
     */
    protected $logField = 'title';
    
    /**
     * Target Group constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = []){
        parent::__construct($attributes);

        $this->manageUserstamp();
        $this->manageActionLog();
        $this->manageMakerChecker();
    }

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    
    protected $fillable = [
        'title', 'country', 'os', 'last_active', 'status', 'order'
    ];

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function scopePublished($query) {
        return $query->where('status', 'Published');
    }

    /**
     * Approved scope.
     *
     * @param object $query
     * @return object
     */
    public function scopeApproved($query) {
        return $query->where('admin_status', 'Approved');
    }
    
    /**
     * Now fetch the country list.
     *
     * @return mixed
     */
    public function getLocationAttribute() {
        
        $options = json_decode($this->country, 1);
        return collect($options)->map(function($i){
            return (int) $i;
        });        
    }
}
