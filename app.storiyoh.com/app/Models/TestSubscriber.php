<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\MakerCheckerManager;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;

class TestSubscriber extends Model
{

	protected $table = 'test_subscribers';
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'email','source','subscription_status',
    ];
}
