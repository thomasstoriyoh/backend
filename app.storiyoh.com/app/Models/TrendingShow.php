<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\MakerCheckerManager;
use App\Traits\UserActionLogManager;
use App\Traits\UserStampManager;

class TrendingShow extends Model
{

	protected $table = 'trending_shows';
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'itunes_id'
    ];
}
