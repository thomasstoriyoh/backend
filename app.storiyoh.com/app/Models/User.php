<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens;

    /**
     * Route created or not.
     *
     * @var boolean
     */
    public static $routeRegistered = true;

    /**
     * Table name for the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider', 'provider_id', 'first_name', 'last_name', 'email', 'image',
        'username', 'password', 'api_token', 'type', 'verified', 'country_id',
        'expiry', 'order', 'admin_status', 'bio', 'discreet', 'last_access_time',
        'city', 'state', 'country', 'oauth_enabled', 'last_access_time_social',
        'app_last_access_time', 'registered_platform', 'password_social', 
        'last_login_type', 'user_type', 'dashboard_status', 'firebase',
        'seller', 'publisher', 'encryption_key', 'sendbird_id', 
        'non_registered_gift', 'developer_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];


    protected static function boot()
    {
        parent::boot();
    
        static::creating(function ($query) {
            // set validation key
            $query->validation_key =  sha1(mt_rand() . mt_rand());
        });
    }

    
    /**
     * Published scope.
     *
     * @param object $query
     * @return object
     */
    public function isVerified()
    {
        return ($this->verified == 'Verified');
    }

    /**
     * Approved scope.
     *
     * @param object $query
     * @return object
     */
    public function isApproved()
    {
        return ($this->admin_status == 'Approved');
    }

    /**
     * Published scope.
     *
     * @param object $query
     * @return object
    */
    public function scopeApproved($query)
    {
        return $query->where('admin_status', 'Approved');
    }

    /**
     * Verified scope.
     *
     * @param object $query
     * @return object
    */
    public function scopeVerified($query)
    {
        return $query->where('verified', 'Verified');
    }

    /**
     *
     * @return type
     */
    public function country_name()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    /**
    * Full name accessor.
    *
    * @return string
    */
    public function getFullNameAttribute()
    {
        return trim($this->first_name . ' ' . $this->last_name);
    }

    /**
    * Notification name accessor. eg. Nilesh N
    *
    * @return string
    */
    public function getNotificationFormatNameAttribute()
    {
        return trim($this->first_name . ' ' . substr($this->last_name, 0, 1));
    }

    /**
     * Relationship with Categories
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class)
            ->withTimestamps()->withPivot('notify');
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function following()
    {
        return $this->belongsToMany(User::class, 'user_follow', 'follower_id', 'following_id')
            ->withTimestamps();
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function follower()
    {
        return $this->belongsToMany(User::class, 'user_follow', 'following_id', 'follower_id')
            ->withTimestamps();
    }

    /**
     * My Boards
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boards()
    {
        return $this->hasMany(Board::class);
    }

    /**
     * following boards
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function following_boards()
    {
        return $this->belongsToMany(Board::class)
            ->withTimestamps();
    }

    /**
     * Relationship with Shows
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shows()
    {
        return $this->belongsToMany(Show::class)
            ->withTimestamps()->withPivot('notify');
    }

    /**
     * Relationship with Subscribers
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subscribers()
    {
        return $this->belongsToMany(Show::class)
            ->withTimestamps()->withPivot('notify');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_stream_data()
    {
        return $this->belongsToMany(Episode::class, 'user_stream_data', 'user_id', 'episode_id')
            ->withPivot(['unique_no', 'discreet'])->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_download_data()
    {
        return $this->belongsToMany(Episode::class, 'user_download_data', 'user_id', 'episode_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_historical_data()
    {
        return $this->belongsToMany(Episode::class, 'user_historical_data', 'user_id', 'episode_id')
            ->withTimestamps();
    }

    /**
     * Relationship with Episode.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comment()
    {
        return $this->belongsTo(EpisodeUserComment::class);
    }

    /**
     * Relationship with episode like
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function like()
    {
        return $this->belongsToMany(Episode::class, 'episode_user_like')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_episode_queue()
    {
        return $this->belongsToMany(Episode::class, 'user_queue', 'user_id', 'episode_id')
            ->withPivot('order')->withTimestamps();
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activities()
    {
        return $this->morphMany(Feed::class, 'typeable');
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_notification_status()
    {
        return $this->belongsToMany(Feed::class, 'user_feed_notifications', 'user_id', 'feed_id')
            ->withPivot('typeable_id', 'type')
            ->withTimestamps();
    }

    /**
     * Relationship with Push.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pushIds()
    {
        return $this->hasMany(PushId::class, 'user_id');
    }

    /**
    * @param null $w
    * @param null $h
    * @return string
    */
    public function getImage($w = null, $h = null)
    {
        //$image = public_path('uploads/users/' .$this->id.'/'. $this->image);
        $image = config('config.s3_url') . '/users/' . $this->id . '/' . $this->image;
        $token = urlencode(base64_encode(json_encode(compact('w', 'h', 'image'))));

        return url('/image/' . $token);
    }

    /**
     * My Smart Playlist
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function smart_playlists()
    {
        return $this->hasMany(SmartPlaylist::class);
    }

    /**
     * following smart playlist
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function following_smart_playlist()
    {
        return $this->belongsToMany(SmartPlaylist::class)
            ->withTimestamps();
    }

    /**
     * Relationship with all smartlist listing
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function my_lists()
    {
        return $this->belongsToMany(SmartPlaylist::class, 'my_followed_and_own_smartlists', 'user_id', 'smart_playlist_id')
            ->withPivot('board_type')->withTimestamps();
    }

    /**
     * Relationship with Social Profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socialIds()
    {
        return $this->hasMany(UserSocial::class, 'user_id');
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_show_notification_status()
    {
        return $this->belongsToMany(ShowNotification::class, 'show_notification_status', 'user_id', 'notification_id')
            ->withPivot('typeable_id', 'type', 'status')
            ->withTimestamps();
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activities_notification()
    {
        return $this->morphMany(UserNotification::class, 'typeable');
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_push_notification_status()
    {
        return $this->belongsToMany(UserNotification::class, 'user_notification_status', 'user_id', 'user_notification_id')
            ->withPivot('typeable_id', 'type', 'status')->withTimestamps();
    }

    /**
     * Override passport function
     * for username and email
     * @param [type] $username
     * @return void
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)->orWhere('email', $username)->first();
    }

    /**
     * Override passport function
     * for password and password_social
     * @param [type] $password
     * @return void
     */
    public function validateForPassportPasswordGrant($password)
    {
        if ($this->last_login_type == 'Social' || $this->last_login_type == 'Guest') {
            return \Hash::check($password, $this->password_social);
        } elseif ($this->last_login_type == 'Not_Verified') {
            return \Hash::check($password, $this->password_social);
        } else {
            return \Hash::check($password, $this->password);
        }

        return null;
    }


    /**
     * Relationship with Subscribers
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function premium_features()
    {
        return $this->belongsToMany(PremiumFeature::class, 'premium_feature_user')
            ->withPivot("premium_feature_start_date", "premium_feature_end_date")
            ->withTimestamps();
    }

    /**
     * Relationship with Seller.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sellerInfo()
    {
        return $this->hasOne(Seller::class);
    }

    /**
     * Relationship with Order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    
    /**
     * Relationship with UserWishlist.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wishlists()
    {
        return $this->hasMany(UserWishlist::class);
    }

    /**
     * Relationship with Podcast.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function premium_shows()
    {
        return $this->hasMany(Show::class);
    }
    
    /**
     * Relationship with Episode.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function premium_episode()
    {
        return $this->hasMany(EpisodeDetail::class);
    }
}
