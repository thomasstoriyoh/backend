<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEmailReset extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = "user_email_resets";
    
    protected $primaryKey = null;
    
    public $incrementing = false;

    /**
     * Timestamps required.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fillable Fields for tags.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'otp', 'value'];
}