<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    /**
     * Resource table name.
     *
     * @var string
     */
    protected $table = "user_notifications";
    
    /**
     * @var array
     */
    protected $fillable = ['typeable_type', 'typeable_id', 'data', 'type'];

    /**
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * Morphing it to all the modules having Feeds.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function typeable() {
        return $this->morphTo();
    }

    /**
     * Relationship with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_notification_status() {
        return $this->belongsToMany(User::class, 'user_notification_status', 'user_notification_id', 'user_id')
            ->withPivot('typeable_id', 'type')->withTimestamps();
    }
}
