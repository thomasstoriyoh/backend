<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    /**
     * Timestamps to maintain.
     *
     * @var bool
     */
    public $timestamps = true;
            
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'provider', 'provider_id',
    ];

    /**
     * Relationship with user
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
