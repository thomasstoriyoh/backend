<?php

namespace App\MyFeedResponse;

use App\Models\Episode;
use App\Traits\Helper;

/**
 * Description of EpisodeHeardFeedResponse
 *
 * @author bcm
 */
class EpisodeHeardFeedResponse extends MyFeedResponseable 
{
    /**
     * 
     * @return type
     */
    public function response() {     
        $episode = Episode::whereIn('id', json_decode($this->feed->data))->first(['id', 'title', 'show_id', 'description', 'mp3', 'duration', 'listen_count', 'explicit']);
        if($episode) {
            return [
                'feed_id' => $this->feed->id,
                'type' => $this->feed->type,
                'name' => @$this->feed->typeable->full_name,
                'username' => @$this->feed->typeable->username,
                "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                'display_text' => "listened to",
                'show_id' => @$episode->show->id,
                'show_name' => trim(str_replace("\n", "", html_entity_decode(@$episode->show->title))),
                'episode_id' => $episode->id,
                'episode_name' => trim(str_replace("\n", "", html_entity_decode($episode->title))),
                'episode_desc' => strip_tags($episode->description),
                'episode_audio' => $episode->getAudioLink(),
                'episode_image' => !empty($episode->show->image) ? $episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                'duration' => $episode->getDurationText(),
                'listen_count' => Helper::shorten_count($episode->listen_count),
                "explicit" => $episode->show->explicit,
                'last_update' => $this->feed->updated_at->diffForHumans(),
                'share_url' => config('config.live_url') . '/episode/' . $episode->id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$episode->title))),               
            ];
        }
    }

}
