<?php
namespace App\MyFeedResponse;

use App\Traits\Helper;
use App\ClassesV3\Feed;
use App\Models\PostShow;
use App\Models\PostEpisode;
use App\Models\PostPlaylist;
use App\Models\PostSmartPlaylist;
use App\Models\PostCollection;
use App\Models\Post;
use App\Models\User;

/**
 * Description of BoardFollowResponse
 *
 * @author bcm
 */

class PostResponse extends MyFeedResponseable {

    public function response() {
        if($this->feed->type == Feed::NEW_SHOW_POST) {
            //try {
                $content = PostShow::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'show_id', 'post', 'likes_count', 'comment_count']);
                if($content) {
                    $like = 'false';
                    if ($content->likes) {                    
                        $like_info = $content->likes()->wherePivot('user_id', $this->current_user->id)->count();
                        if ($like_info) {
                            $like = 'true';
                        }
                    }
    
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'content_id' => $content->id,
                        'name' => @$this->feed->typeable->full_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => "shared a Show",                    
                        'show_id' => $content->show->id,
                        'show_name' => trim(str_replace("\n", "", html_entity_decode($content->show->title))),
                        //'description' => $content->show->description,
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->show->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                        'show_categories' => @implode(", ", $content->show->categories()->pluck('title')->all()),                
                        'show_image' => !empty($content->show->image) ? $content->show->getWSImage(200) : asset('uploads/default/show.png'),
                        'post' => ! empty($content->post) ? $content->post : "",
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_subscribers' => Helper::shorten_count($content->show->no_of_subscribers),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'like' => $like,
                        "explicit" => $content->show->explicit,
                        'share_url' => config('config.live_url') . '/podcasts/' . $content->show->id . '/' . Helper::make_slug(trim(@$content->show->title)),
                    ];
                }                
            //} catch(\Exception $ex) {}            
        } else if($this->feed->type == Feed::NEW_EPISODE_POST) {
            //try {
                $content = PostEpisode::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'episode_id', 'post', 'likes_count', 'comment_count']);
                if($content) {
                    $like = 'false';
                    if ($content->likes) {                    
                        $like_info = $content->likes()->wherePivot('user_id', $this->current_user->id)->count();
                        if ($like_info) {
                            $like = 'true';
                        }
                    }
                    
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'content_id' => $content->id,
                        'name' => @$this->feed->typeable->full_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => "shared an Episode",
                        'episode_id' => $content->episode_id,
                        'episode_name' => html_entity_decode($content->episode->title),
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->episode->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                        'show_id' => $content->episode->show->id,
                        'show_name' => trim(str_replace("\n", "", html_entity_decode($content->episode->show->title))),
                        'show_image' => !empty($content->episode->show->image) ? $content->episode->show->getWSImage(200) : asset('uploads/default/show.png'),
                        'post' => ! empty($content->post) ? $content->post : "",
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'listen_count' => Helper::shorten_count($content->episode->listen_count),
                        "explicit" => $content->episode->explicit,
                        'like' => $like,
                        "explicit" => $content->episode->show->explicit,
                        'episode_audio' => $content->episode->getAudioLink(),
                        'duration' => $content->episode->getDurationText(),
                        'share_url' => config('config.live_url') . '/episode/' . $content->episode_id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$content->episode->title))),
                    ];
                }                
            //} catch(\Exception $ex) {}            
        }  else if($this->feed->type == Feed::NEW_PLAYLIST_POST) {
            //try {
                $content = PostPlaylist::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'playlist_id', 'post', 'likes_count', 'comment_count']);
                if($content) {
                    $like = 'false';
                    if ($content->likes) {                    
                        $like_info = $content->likes()->wherePivot('user_id', $this->current_user->id)->count();
                        if ($like_info) {
                            $like = 'true';
                        }
                    }
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'content_id' => $content->id,
                        'name' => @$this->feed->typeable->full_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => "shared a Playlist",
                        'board_id' => $content->playlist->id,
                        'board_name' => trim(str_replace("\n", "", html_entity_decode($content->playlist->title))),
                        'board_image' => !empty($content->playlist->image) ? $content->playlist->getImage(200) : asset('uploads/default/board.png'),
                        'board_owner_name' => $content->playlist->user->first_name,
                        'board_user_name' => $content->playlist->user->username,
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->playlist->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                        'post' => ! empty($content->post) ? $content->post : "",
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'no_of_followers' => Helper::shorten_count($content->playlist->followers_count),
                        'like' => $like,
                        "explicit" => 0,
                    ];
                }                
            //} catch(\Exception $ex) {}            
        } else if($this->feed->type == Feed::NEW_SMARTPLAYLIST_POST) {
            //try {
                $content = PostSmartPlaylist::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'smart_playlist_id', 'post', 'likes_count', 'comment_count']);
                if($content) {
                    $like = 'false';
                    if ($content->likes) {                    
                        $like_info = $content->likes()->wherePivot('user_id', $this->current_user->id)->count();
                        if ($like_info) {
                            $like = 'true';
                        }
                    }
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'content_id' => $content->id,
                        'name' => @$this->feed->typeable->full_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => "shared a Smart Playlist",
                        'board_id' => $content->smart_playlist->id,
                        'board_name' => trim(str_replace("\n", "", html_entity_decode($content->smart_playlist->title))),
                        'board_image' => !empty($content->smart_playlist->image) ? $content->smart_playlist->getImage(200) : asset('uploads/default/board.png'),
                        'board_owner_name' => $content->smart_playlist->user->first_name,
                        'board_user_name' => $content->smart_playlist->user->username,
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->smart_playlist->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                        'post' => ! empty($content->post) ? $content->post : "",
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'no_of_followers' => Helper::shorten_count($content->smart_playlist->followers_count),
                        'like' => $like,
                        "explicit" => 0,
                    ];
                }                
            //} catch(\Exception $ex) {}            
        } else if($this->feed->type == Feed::NEW_COLLECTION_POST) {
            //try {
                $content = PostCollection::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'collection_id', 'post', 'likes_count', 'comment_count']);
                if($content) {
                    $like = 'false';
                    if ($content->likes) {                    
                        $like_info = $content->likes()->wherePivot('user_id', $this->current_user->id)->count();
                        if ($like_info) {
                            $like = 'true';
                        }
                    }
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'content_id' => $content->id,
                        'name' => @$this->feed->typeable->full_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => "shared a Collection",
                        'collection_id' => $content->collection->id,
                        'collection_name' => trim(str_replace("\n", "", html_entity_decode($content->collection->title))),
                        'collection_image' => !empty($content->collection->image) ? $content->collection->getImage(200) : asset('uploads/default/board.png'),
                        //'description' => $content->collection->description,
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->collection->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                        'post' => ! empty($content->post) ? $content->post : "",
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'like' => $like,
                        "explicit" => 0,
                        'share_url' => config('config.live_url') . '/collections/' . $content->collection->id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$content->collection->title))),
                    ];
                }                
            //} catch(\Exception $ex) {}            
        } else if($this->feed->type == Feed::INDIVIDUAL_POST) {
            $content = Post::whereIn("id", json_decode($this->feed->data))->first(['id', 'user_id', 'post', 'tagIds', 'likes_count', 'comment_count']);
            if($content) {
                $like = 'false';
                if ($content->likes) {                    
                    $like_info = $content->likes()->wherePivot('user_id', $this->current_user->id)->count();
                    if ($like_info) {
                        $like = 'true';
                    }
                }
                //tag code start
                $tagUserData = [];
                if(! is_null($content->tagIds)) {
                    $tagData = User::whereIn('id', json_decode($content->tagIds))->get(['id', 'username', 'first_name', 'image']);
                    foreach($tagData as $item) {
                        $tagUserData[] = [
                            'id' => $item->id, 
                            'username' => $item->username, 
                            'name' => $item->first_name,
                            'image' => !empty($item->image) ? $item->getImage(200) : asset('uploads/default/user.png')
                        ];
                    }            
                }
                return [
                    'feed_id' => $this->feed->id,
                    'type' => $this->feed->type,
                    'content_id' => $content->id,
                    'name' => @$this->feed->typeable->full_name,
                    'username' => @$this->feed->typeable->username,
                    "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'display_text' => "shared a Post",
                    'post' => ! empty($content->post) ? $content->post : "",
                    'tagUserData' => $tagUserData,
                    'last_update' => $this->feed->updated_at->diffForHumans(),
                    'no_of_likes' => Helper::shorten_count($content->likes_count),
                    'no_of_comments' => Helper::shorten_count($content->comment_count),
                    'like' => $like                    
                ];
            }
        }
    }
}
