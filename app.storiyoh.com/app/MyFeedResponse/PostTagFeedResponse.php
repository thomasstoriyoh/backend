<?php
namespace App\MyFeedResponse;

use Carbon\Carbon;
use App\Classes\Feed;
use App\Models\PostShowComment;
use App\Models\PostEpisodeComment;
use App\Models\PostPlaylistComment;
use App\Models\PostSmartPlaylistComment;
use App\Models\PostCollectionComment;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class PostTagFeedResponse extends MyFeedResponseable
{
    //put your code here
    public function response() {
        $read_status = $this->current_user->user_notification_status()->wherePivot('feed_id', $this->feed->id)->count();
        
        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->feed->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        $data = json_decode($this->feed->data, true);
        $postId = $data[0];
        
        if ($this->feed->type == Feed::SHOW_POST_COMMENT_TAG) {
            //try {
                $content = PostShowComment::find(trim($postId, '#'));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_show_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        }
        else if ($this->feed->type == Feed::EPISODE_POST_COMMENT_TAG) {
            //try {
                $content = PostEpisodeComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_episode_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        }
        else if ($this->feed->type == Feed::PLAYLIST_POST_COMMENT_TAG) {
            //try{
                $content = PostPlaylistComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_playlist_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } 
        else if ($this->feed->type == Feed::SMARTPLAYLIST_POST_COMMENT_TAG) {
            //try{
                $content = PostSmartPlaylistComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_smart_playlist_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } 
        else if ($this->feed->type == Feed::COLLECTION_POST_COMMENT_TAG) {
            //try{
                $content = PostCollectionComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_collection_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        }                         
    }
}
