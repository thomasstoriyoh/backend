<?php
namespace App\MyFeedResponse;

use App\Models\SmartPlaylist;
use App\Traits\Helper;

/**
 * Description of SmartFollowResponse
 *
 * @author bcm
 */
class SmartPlaylistFollowResponse extends MyFeedResponseable {    
    public function response() {     
        $board = SmartPlaylist::whereIn('id', json_decode($this->feed->data))->withCount('followers')->first(['id', 'title', 'user_id', 'image', 'description', 'private']);
        if($board) {
            $user_boards = $this->current_user->following_smart_playlist()->pluck('id')->all();
            $follow_status = 'false';
            if (@in_array($board->id, $user_boards)) {
                $follow_status = 'true';
            }
            return [
                'feed_id' => $this->feed->id,
                'type' => $this->feed->type,
                'name' => @$this->feed->typeable->full_name,
                'username' => @$this->feed->typeable->username,
                "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                'display_text' => "started following smart playlist",
                'board_id' => $board->id,
                'private' => $board->private,
                'board_name' => $board->title,
                'board_description' => $board->description,
                'board_image' => !empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
                'board_owner' => $board->user_id == $this->current_user->id ? 1 : 0,
                'board_owner_name' => $board->user->first_name,
                'board_user_name' => $board->user->username,
                'last_update' => $this->feed->updated_at->diffForHumans(),
                'follow_status' => $follow_status,
                'no_of_follower' => Helper::shorten_count($board->followers_count),
            ];
        }
    
    }
}
