<?php
namespace App\NotificationResponse;

use App\Models\Episode;
use Carbon\Carbon;

/**
 * Description of BoardFeedResponse
 *
 * @author bcm
 */
class BoardFeedResponse extends NotificationResponseable {
    
    public function response() {  
        $episode = Episode::whereIn('id', json_decode($this->notification->data))->first(['id', 'show_id', 'title', 'image', 'mp3', 'duration']);

        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id',$this->notification->id)->count();

        // $todayTS = strtotime(Carbon::now()->format("Y-m-d"));
        // $notification_date = strtotime(Carbon::parse($this->notification->updated_at)->format('Y-m-d'));        
        // $lastWeekDate = strtotime(Carbon::now()->subDays(7)->format("Y-m-d"));

        // $time_status = 3;
        // if($todayTS == $notification_date) {
        //     $time_status = 1;
        // } else if($notification_date < $todayTS  && $notification_date >= $lastWeekDate) {
        //     $time_status = 2;
        // }

        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        if($episode) {
            return [
                'id' => $this->notification->id,
                'type' => $this->notification->type,
                'board_id' => $this->notification->typeable->id,
                'board_title' => $this->notification->typeable->title,
                'board_image' => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(200) : asset('uploads/default/board.png'),
                "no_follower" => $this->notification->typeable->followers()->count(),
                'display_text' => "added a new episode ".$episode->title,
                'display_text2' => "added a new episode",
                'show_id' => @$episode->show->id,
                'show_name' => trim(str_replace("\n", "", html_entity_decode(@$episode->show->title))),
                'episode_id' => $episode->id,
                'episode_name' => trim(str_replace("\n", "", html_entity_decode($episode->title))),
                'episode_audio' => $episode->getAudioLink(),
                "duration" => $episode->getDurationText(),
                'last_update' => $this->notification->updated_at->diffForHumans(),
                'read_status' => $read_status,
                'time_status' => $time_status
            ];
        }
    }    
}
