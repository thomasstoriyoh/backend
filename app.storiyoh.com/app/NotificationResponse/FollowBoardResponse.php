<?php
namespace App\NotificationResponse;
use App\Models\Board;
use Carbon\Carbon;

class FollowBoardResponse extends NotificationResponseable
{
    /**
     * 
     * @return array
     */
    public function response() {
     
        $board = Board::whereIn('id', json_decode($this->notification->data))->first(['id', 'title', 'image']);

        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id',$this->notification->id)->count();
        // $todayTS = strtotime(Carbon::now()->format("Y-m-d"));
        // $notification_date = strtotime(Carbon::parse($this->notification->updated_at)->format('Y-m-d'));        
        // $lastWeekDate = strtotime(Carbon::now()->subDays(7)->format("Y-m-d"));

        // $time_status = 3;
        // if($todayTS == $notification_date) {
        //     $time_status = 1;
        // } else if($notification_date < $todayTS  && $notification_date >= $lastWeekDate) {
        //     $time_status = 2;
        // } 

        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        if($board) {
            return [
                'id' => $this->notification->id,
                'type' => $this->notification->type,
                'name' => @$this->notification->typeable->notification_format_name,
                'username' => @$this->notification->typeable->username,
                "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                'display_text' => "started following your playlist ".$board->title,
                'board_id' => $board->id,
                'board_name' => $board->title,
                'board_image' => ! empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
                'last_update' => $this->notification->updated_at->diffForHumans(),
                'read_status' => $read_status,
                'time_status' => $time_status
            ];
        }
    
    }
    
}
