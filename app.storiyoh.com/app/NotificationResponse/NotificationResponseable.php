<?php
namespace App\NotificationResponse;

use App\Models\Feed;

abstract class NotificationResponseable 
{
    protected $notification;
    protected $login_user;
    
    public function __construct(Feed $feed, $user) 
    {
    	$this->login_user = $user;
        $this->notification = $feed;
    }
    
    abstract public function response();
}
