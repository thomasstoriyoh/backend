<?php

namespace App\NotificationResponse;

use App\Models\Episode;
use Carbon\Carbon;
use App\Traits\Helper;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class ShowNewEpisodeFeedResponse extends NotificationResponseable
{
    //put your code here
    public function response()
    {
        $episodes = Episode::published()->withCount('like', 'comments')->whereIn('id', json_decode($this->notification->data))->whereNotNull('show_id')->get(['id', 'title', 'show_id', 'mp3', 'duration']);
        $user_episode_like = $this->login_user->like()->pluck('episode_id')->all();
        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id', $this->notification->id)->count();

        // $todayTS = strtotime(Carbon::now()->format("Y-m-d"));
        // $notification_date = strtotime(Carbon::parse($this->notification->updated_at)->format('Y-m-d'));
        //$lastWeekDate = strtotime(Carbon::now()->subDays(7)->format("Y-m-d"));

        // $time_status = 3;
        // if($todayTS == $notification_date) {
        //     $time_status = 1;
        // } else if($notification_date < $todayTS  && $notification_date >= $lastWeekDate) {
        //     $time_status = 2;
        // }

        $todayTS = Carbon::parse(date('Y-m-d H:i:s'));
        $notification_date = Carbon::parse($this->notification->updated_at);

        $time_status = 3;
        if ($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } elseif ($todayTS->diffInHours($notification_date) > 24 && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        if ($episodes) {
            return [
                'id' => $this->notification->id,
                'type' => $this->notification->type,
                'show_id' => @$episodes->first()->show->id,
                'show_name' => trim(str_replace("\n", '', html_entity_decode(@$episodes->first()->show->title))),
                'show_image' => @$episodes->first()->show->image ? $episodes->first()->show->getWSImage(200) : asset('uploads/default/show.png'),
                'episode_count' => count($episodes),
                'display_text' => count($episodes) > 1 ? count($episodes) . ' new episodes' : $episodes[0]['title'],
                'display_text2' => count($episodes) > 1 ? 'New episodes' : $episodes[0]['title'],
                'episode_data' => $this->episode_list($episodes, $user_episode_like),
                'read_status' => $read_status,
                'time_status' => $time_status
            ];
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $episodes
     * @return void
     */
    protected function episode_list($episodes, $user_episode_like)
    {
        $data = [];
        //if ($episodes->count() == 1) {
        foreach ($episodes as $key => $episode) {
            if ($key == 1) {
                break;
            }
            $like = false;
            $like = @in_array($episode->id, $user_episode_like) ? 'true' : 'false';
            $data[] = [
                    'episode_id' => $episode->id,
                    'episode_name' => html_entity_decode($episode->title),
                    'episode_audio' => $episode->getAudioLink(),
                    'duration' => $episode->getDurationText(),
                    'like' => $like,
                    'like_count' => Helper::shorten_count($episode->like_count),
                    'comment_count' => Helper::shorten_count($episode->comments_count),
                    'display_text' => 'has a new episode ' . $episode->title,
                    'last_update' => $this->notification->updated_at->diffForHumans()
                ];
        }
        //}

        return $data;
    }
}
