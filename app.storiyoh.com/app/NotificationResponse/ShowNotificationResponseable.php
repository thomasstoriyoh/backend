<?php
namespace App\NotificationResponse;

use App\Models\Feed;
use App\Models\ShowNotification;

abstract class ShowNotificationResponseable 
{
    protected $notification;
    protected $login_user;
    
    public function __construct(ShowNotification $notification, $user) 
    {
    	$this->login_user = $user;
        $this->notification = $notification;
    }
    
    abstract public function response();
}
