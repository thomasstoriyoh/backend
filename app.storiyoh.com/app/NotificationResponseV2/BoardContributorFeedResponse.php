<?php
namespace App\NotificationResponseV2;

use App\Models\Episode;
use Carbon\Carbon;
use App\Traits\Helper;

/**
 * Description of BoardFeedResponse
 *
 * @author bcm
 */
class BoardContributorFeedResponse extends NotificationResponseableV2 {
    
    public function response() {
        $episodeInfo = json_decode($this->notification->data); 
        
        $episode = Episode::where('id', $episodeInfo[0])->first(['id', 'show_id', 'title', 'image', 'mp3', 'duration', 'listen_count', 'explicit']);
        
        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id',$this->notification->id)->count();        

        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        if($episode) {
            return [
                'id' => $this->notification->id,
                'type' => $this->notification->type,
                'board_id' => $this->notification->typeable->id,
                'board_title' => $this->notification->typeable->title,
                'board_image' => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(200) : asset('uploads/default/board.png'),
                "no_follower" => $this->notification->typeable->followers()->count(),
                'display_text' => "added a new episode ".$episode->title,
                'display_text2' => "added a new episode",
                'show_id' => @$episode->show->id,
                'show_name' => trim(str_replace("\n", "", html_entity_decode(@$episode->show->title))),
                'episode_id' => $episode->id,
                'episode_name' => trim(str_replace("\n", "", html_entity_decode($episode->title))),
                'episode_audio' => $episode->getAudioLink(),
                "duration" => $episode->getDurationText(),
                'listen_count' => Helper::shorten_count($episode->listen_count), 
                "explicit" => $episode->show->explicit,
                'last_update' => $this->notification->updated_at->diffForHumans(),
                'read_status' => $read_status,
                'time_status' => $time_status
            ];
        }
    }    
}
