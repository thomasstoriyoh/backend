<?php
namespace App\NotificationResponseV2;
use App\Models\Board;
use Carbon\Carbon;
use App\Models\User;

class BoardContributorResponse extends NotificationResponseableV2
{
    /**
     * 
     * @return array
     */
    public function response() {
     
        $board = $this->notification->typeable;

        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id',$this->notification->id)->count();
        
        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        if($board) {
            $userData = User::where('id', $board->user_id)->first(['id', 'first_name', 'image', 'username']);
            return [
                'id' => $this->notification->id,
                'type' => $this->notification->type,
                'name' => @$userData->notification_format_name,
                'username' => @$userData->username,
                "user_photo" => ! empty($userData->image) ? $userData->getImage(100) : asset('uploads/default/user.png'),
                'display_text' => 'added you as a Contributor',
                'board_id' => $board->id,
                'board_name' => $board->title,
                'board_image' => ! empty($board->image) ? $board->getImage(200) : asset('uploads/default/board.png'),
                'last_update' => $this->notification->updated_at->diffForHumans(),
                'read_status' => $read_status,
                'time_status' => $time_status
            ];
        }
    
    }
    
}
