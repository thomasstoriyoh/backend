<?php
namespace App\NotificationResponseV2;

use App\Models\EpisodeUserComment;
use Carbon\Carbon;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class EpisodeCommentFeedResponse extends NotificationResponseableV2
{
    //put your code here
    public function response() {
        
        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id',$this->notification->id)->count();
        
        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        $comment = EpisodeUserComment::find($this->notification->typeable_id, ['user_id', 'episode_id']);
        //dd($comment->user);

        return [
            'id' => $this->notification->id,
            'type' => $this->notification->type,
            'name' => @$comment->user->notification_format_name,
            'username' => @$comment->user->username,
            "user_photo" => ! empty($comment->user->image) ? $comment->user->getImage(100) : asset('uploads/default/user.png'),
            'episode_id' => $comment->episode_id,
            'comment_id' => $this->notification->typeable->id,
            'display_text' => "tagged you in the post",                
            'last_update' => $this->notification->updated_at->diffForHumans(),
            'read_status' => $read_status,
            'time_status' => $time_status
        ];                 
    }
}
