<?php
namespace App\NotificationResponseV2;
use App\Models\User;
use Carbon\Carbon;

class FollowUserResponse extends NotificationResponseableV2
{
    /**
     * 
     * @return array
     */
    public function response() 
    {
        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id',$this->notification->id)->count();
        
        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        return [
            'id' => $this->notification->id,
            'type' => $this->notification->type,
            'name' => @$this->notification->typeable->notification_format_name,
            'username' => @$this->notification->typeable->username,
            "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
            'display_text' => "started following you",
            'last_update' => $this->notification->updated_at->diffForHumans(),
            'read_status' => $read_status,
            'time_status' => $time_status
        ];    
    }    
}
