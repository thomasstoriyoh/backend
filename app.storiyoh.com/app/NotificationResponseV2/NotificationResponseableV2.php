<?php
namespace App\NotificationResponseV2;

use App\Models\Feed;

abstract class NotificationResponseableV2 
{
    protected $notification;
    protected $login_user;
    
    public function __construct(Feed $feed, $user) 
    {
    	$this->login_user = $user;
        $this->notification = $feed;
    }
    
    abstract public function response();
}
