<?php
namespace App\NotificationResponseV2;

use Carbon\Carbon;
use App\Models\PostShow;
use App\ClassesV3\Feed;
use App\Models\PostEpisode;
use App\Models\PostPlaylist;
use App\Models\PostCollection;
use App\Models\PostSmartPlaylist;
use App\Models\Post;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class PostLikeFeedResponse extends NotificationResponseableV2
{
    //put your code here
    public function response() {
        
        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id', $this->notification->id)->count();
        
        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        $data = json_decode($this->notification->data, true);
        $postId = $data[0];
        
        if ($this->notification->type == Feed::SHOW_POST_LIKE) {
            //try {
                $content = PostShow::find(trim($postId, '#'), ['id', 'user_id', 'show_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'show_id' => $content->show_id,
                        'post_id' => trim($postId, '#'),
                        'display_text' => 'liked your post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::EPISODE_POST_LIKE) {
            //try {
                $content = PostEpisode::find(trim($postId, "#"), ['id', 'user_id', 'episode_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'episode_id' => $content->episode_id,
                        'post_id' => trim($postId, '#'),
                        'display_text' => 'liked your post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::PLAYLIST_POST_LIKE) {
            //try{
                $content = PostPlaylist::find(trim($postId, "#"), ['id', 'user_id', 'playlist_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'playlist_id' => $content->playlist_id,
                        'post_id' => trim($postId, '#'),
                        'display_text' => 'liked your post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::SMARTPLAYLIST_POST_LIKE) {
            //try{
                $content = PostSmartPlaylist::find(trim($postId, "#"), ['id', 'user_id', 'smart_playlist_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'smart_playlist_id' => $content->smart_playlist_id,
                        'post_id' => trim($postId, '#'),
                        'display_text' => 'liked your post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::COLLECTION_POST_LIKE) {
            //try{
                $content = PostCollection::find(trim($postId, "#"), ['id', 'user_id', 'collection_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'collection_id' => $content->collection_id,
                        'post_id' => trim($postId, '#'),
                        'display_text' => 'liked your post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::INDIVIDUAL_POST_LIKE) {            
            $content = Post::find(trim($postId, "#"), ['id', 'post_id']);
            if ($content) {
                return [
                    'id' => $this->notification->id,
                    'type' => $this->notification->type,
                    'name' => @$this->notification->typeable->notification_format_name,
                    'username' => @$this->notification->typeable->username,
                    "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'post_id' => $content->id,
                    'display_text' => 'liked your post',
                    'last_update' => $this->notification->updated_at->diffForHumans(),
                    'read_status' => $read_status,
                    'time_status' => $time_status
                ];
            }
        }
    }
}
