<?php
namespace App\NotificationResponseV2;

use Carbon\Carbon;
use App\ClassesV3\Feed;
use App\Models\PostShowComment;
use App\Models\PostEpisodeComment;
use App\Models\PostPlaylistComment;
use App\Models\PostSmartPlaylistComment;
use App\Models\PostCollectionComment;
use App\Models\PostComment;
use App\Models\Post;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class PostTagFeedResponse extends NotificationResponseableV2
{
    //put your code here
    public function response() {
        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id', $this->notification->id)->count();
        
        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        $data = json_decode($this->notification->data, true);
        $postId = $data[0];
        
        if ($this->notification->type == Feed::SHOW_POST_COMMENT_TAG) {
            //try {
                $content = PostShowComment::find(trim($postId, '#'));
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_show_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::EPISODE_POST_COMMENT_TAG) {
            //try {
                $content = PostEpisodeComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_episode_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::PLAYLIST_POST_COMMENT_TAG) {
            //try{
                $content = PostPlaylistComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_playlist_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::SMARTPLAYLIST_POST_COMMENT_TAG) {
            //try{
                $content = PostSmartPlaylistComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_smart_playlist_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::COLLECTION_POST_COMMENT_TAG) {
            //try{
                $content = PostCollectionComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'id' => $this->notification->id,
                        'type' => $this->notification->type,
                        'name' => @$this->notification->typeable->notification_format_name,
                        'username' => @$this->notification->typeable->username,
                        "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_collection_id,
                        'display_text' => 'tagged you in the post',
                        'last_update' => $this->notification->updated_at->diffForHumans(),
                        'read_status' => $read_status,
                        'time_status' => $time_status
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->notification->type == Feed::INDIVIDUAL_POST_COMMENT_TAG) {
            $content = PostComment::find(trim($postId, "#"));
            if ($content) {
                return [
                    'id' => $this->notification->id,
                    'type' => $this->notification->type,
                    'name' => @$this->notification->typeable->notification_format_name,
                    'username' => @$this->notification->typeable->username,
                    "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'comment_id' => trim($postId, '#'),
                    'post_id' => $content->post_id,
                    'display_text' => 'tagged you in the comment',
                    'last_update' => $this->notification->updated_at->diffForHumans(),
                    'read_status' => $read_status,
                    'time_status' => $time_status
                ];
            }
        } else if ($this->notification->type == Feed::INDIVIDUAL_POST_TAG) {
            $content = Post::find(trim($postId, "#"));
            if ($content) {
                return [
                    'id' => $this->notification->id,
                    'type' => $this->notification->type,
                    'name' => @$this->notification->typeable->notification_format_name,
                    'username' => @$this->notification->typeable->username,
                    "user_photo" => ! empty($this->notification->typeable->image) ? $this->notification->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'comment_id' => trim($postId, '#'),
                    'post_id' => $content->id,
                    'display_text' => 'tagged you in the post',
                    'last_update' => $this->notification->updated_at->diffForHumans(),
                    'read_status' => $read_status,
                    'time_status' => $time_status
                ];
            }
        }                        
    }
}
