<?php

namespace App\NotificationResponseV2;

use App\Models\Episode;
use Carbon\Carbon;
use App\Traits\Helper;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class ShowNewEpisodeFeedResponseV3 extends ShowNotificationResponseableV3
{
    //put your code here
    public function response()
    {
        $episodes = Episode::published()->whereIn('id', json_decode($this->notification->data))->whereNotNull('show_id')->get(['id', 'title', 'show_id', 'mp3', 'duration', 'listen_count', 'explicit']);

        $read_status = $this->login_user->user_show_notification_status()->wherePivot('notification_id', $this->notification->id)->count();

        $todayTS = Carbon::parse(date('Y-m-d H:i:s'));
        $notification_date = Carbon::parse($this->notification->updated_at);

        $time_status = 3;
        if ($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } elseif ($todayTS->diffInHours($notification_date) > 24 && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }

        if ($episodes) {
            return [
                'id' => $this->notification->id,
                'type' => $this->notification->type,
                'show_id' => @$episodes->first()->show->id,
                'show_name' => trim(str_replace("\n", '', html_entity_decode(@$episodes->first()->show->title))),
                'show_image' => @$episodes->first()->show->image ? $episodes->first()->show->getWSImage(200) : asset('uploads/default/show.png'),
                'episode_count' => count($episodes),
                'display_text' => count($episodes) > 1 ? count($episodes) . ' new episodes' : $episodes[0]['title'],
                'display_text2' => count($episodes) > 1 ? 'New episodes' : $episodes[0]['title'],
                'episode_data' => $this->episode_list($episodes),
                'read_status' => $read_status,
                'time_status' => $time_status
            ];
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $episodes
     * @return void
     */
    protected function episode_list($episodes)
    {
        $data = [];
        foreach ($episodes as $key => $episode) {
            if ($key == 1) {
                break;
            }
            $data[] = [
                'episode_id' => $episode->id,
                'episode_name' => html_entity_decode($episode->title),
                'episode_audio' => $episode->getAudioLink(),
                'duration' => $episode->getDurationText(),
                'display_text' => 'has a new episode ' . $episode->title,
                'listen_count' => Helper::shorten_count($episode->listen_count),
                'explicit' => $episode->show->explicit,
                'last_update' => Carbon::createFromTimeStamp(strtotime(@$this->notification->updated_at))->diffForHumans()
            ];
        }

        return $data;
    }
}
