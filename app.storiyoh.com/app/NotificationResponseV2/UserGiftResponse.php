<?php
namespace App\NotificationResponseV2;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Order;

class UserGiftResponse extends NotificationResponseableV2
{
    /**
     * 
     * @return array
     */
    public function response() 
    {
        $read_status = $this->login_user->user_notification_status()->wherePivot('feed_id',$this->notification->id)->count();
        
        $todayTS = Carbon::parse(date("Y-m-d H:i:s"));
        $notification_date = Carbon::parse($this->notification->updated_at);        
        
        $time_status = 3;
        if($todayTS->diffInHours($notification_date) <= 24) {
            $time_status = 1;
        } else if($todayTS->diffInHours($notification_date) > 24  && $todayTS->diffInHours($notification_date) <= 168) {
            $time_status = 2;
        }
        $orderData = json_decode($this->notification->data);
        $order = Order::find($orderData[0], ['id', 'user_id', 'giftBy', 'product_id', 'product_type']);
        
        return [
            'id' => $this->notification->id,
            'type' => $this->notification->type,
            'name' => @$order->sender->notification_format_name,
            'username' => @$order->sender->username,
            "user_photo" => ! empty($order->sender->image) ? $order->sender->getImage(100) : asset('uploads/default/user.png'),
            'display_text' => "gifted you",
            'content_id' => $order->product_id,
            'content_type' => $order->product_type == 1 ? "S" : "E",
            'last_update' => $this->notification->updated_at->diffForHumans(),
            'read_status' => $read_status,
            'time_status' => $time_status
        ];    
    }    
}
