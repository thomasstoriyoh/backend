<?php

namespace App\Providers\Admin;

use App\Models\Admin\Language;
use App\Models\Admin\Module;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Route;

class AdminViewComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeMasterView();

        $this->composeHeaderView();

        $this->composeReportTabView();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function composeMasterView()
    {
        view()->composer('admin.layouts.master', function($view){
            list($controller, $action) = explode("@", Route::currentRouteAction());

            $ctrl = class_basename($controller);

            $module = @Module::whereController($ctrl)->first();
            $sidebar_selected_item = @$module->id;
            $sidebar_selected_type = @$module->type;
            $sidebar_selected_panel = @$module->parent->id;
            if(!empty($module->parent->forward_to)) {
                $sidebar_selected_item = $module->parent->id;
                $sidebar_selected_panel = @$module->parent->parent->id;
            }
            $route_required = true;
//            if ($ctrl == 'TradeSchemeController') {
//                $route_required = false;
//                $product = Module::whereSlug('product-listing')->first();
//                if ($product) {
//                    $sidebar_selected_item = $product->id;
//                    $sidebar_selected_type = $product->type;
//                    $sidebar_selected_panel = $product->parent->id;
//                }
//            }
            return $view->with([
                'currentController' => "\\" . $controller,
                'currentMethod' => $action,
                'routeRequired' => $route_required,
                'sidebar_selected_item' => $sidebar_selected_item,
                'sidebar_selected_panel' => $sidebar_selected_panel,
                'sidebar_selected_type' => $sidebar_selected_type,
            ]);
        });
    }

    protected function composeHeaderView()
    {
        $nowS = Carbon::now();
        $nowE = Carbon::now();
        if(!session('ga_start_date_for_analytics')) {
            session()->put('ga_start_date_for_analytics', $nowS->subDays(6)->format("Y-m-d"));
        }
        if(!session('ga_end_date_for_analytics')) {
            session()->put('ga_end_date_for_analytics', $nowE->format("Y-m-d"));
        }

        view()->composer('admin.partials.header', function($view) {
            $all_sidebar_modules = Module::published()
                ->whereModuleId(0)->whereType('Module')->orderBy('order')->latest()
                ->get(['id', 'title', 'slug', 'controller', 'model', 'forward_to']);
            $all_sidebar_configs = Module::published()
                ->whereModuleId(0)->whereType('Configure')->orderBy('order')->latest()
                ->get(['id', 'title', 'slug', 'controller', 'model', 'forward_to']);
            $all_sidebar_reports = Module::published()
                ->whereModuleId(0)->whereType('Report')->orderBy('order')->latest()
                ->get(['id', 'title', 'slug', 'controller', 'model', 'forward_to']);
            $all_sidebar_docs = Module::published()
                ->whereModuleId(0)->whereType('Documentation')->orderBy('order')->latest()
                ->get(['id', 'title', 'slug', 'controller', 'model', 'forward_to']);

            $lang_module = Module::published()->whereSlug('language')->first();
            $all_languages = ['status' => !empty($lang_module)];
            if(! empty($lang_module)) {
                $all_languages['langs'] = Language::published()->orderBy('order')->latest()->get();
            }

            return $view->with(compact('all_sidebar_modules', 'all_sidebar_configs', 'all_sidebar_reports', 'all_sidebar_docs', 'all_languages'));
        });
    }

    protected function composeReportTabView()
    {
        view()->composer('admin.reports.google.tab', function($view) {
            list($controller, $action) = explode("@", Route::currentRouteAction());

            $ctrl = explode('\\', $controller);
            $current_controller = end($ctrl);

            $module = Module::whereController($current_controller)->first();

            $all_tab_reports = Module::published()
                ->whereModuleId($module->module_id)->whereType('Report')->orderBy('order')->latest()
                ->get(['id', 'title', 'slug', 'controller', 'model']);

            return $view->with(compact('all_tab_reports', 'current_controller'));
        });
    }
}
