<?php

namespace App\Providers\Admin;

use App\Traits\GoogleAnalytics;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AnalyticsViewComposerProvider extends ServiceProvider
{
    use GoogleAnalytics;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['admin.reports.google.top_google_chart','admin.index'], function($view) {
            $data = $this->getCommonDataForPeriod(Carbon::createFromFormat("Y-m-d", session('ga_start_date_for_analytics')), Carbon::createFromFormat("Y-m-d", session('ga_end_date_for_analytics')), 'ga:sessions', ["dimensions"=>'ga:date', "sort"=>"ga:date"]);
            $result = $this->formatAnalyticsData($data);
            $analyticsData = $result['data'];
            $total_analytics_count = $result['count'];

            $data = $this->getCommonDataForPeriod(Carbon::createFromFormat("Y-m-d", date('Y-m-d')), Carbon::createFromFormat("Y-m-d", date('Y-m-d')), 'ga:sessions', ["dimensions"=>'ga:date', "sort"=>"ga:date"]);
            $result = $this->formatAnalyticsData($data);
            $today_count = $result['count'];

            return $view->with(compact("analyticsData", 'total_analytics_count', 'today_count'));
        });
    }

    /**
     *
     * Formatted Google Analytics top chart data
     */
    protected  function formatAnalyticsData($data)
    {
        $formatedArr = array();
        $totalCount = 0;
        foreach($data as $item)
        {
            $formatedArr[] = array($item['date']->format("jS M"), (int) $item['sessions']);
            $totalCount += (int) $item['sessions'];
        }
        return ['data' => $formatedArr, 'count' => $totalCount];
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
