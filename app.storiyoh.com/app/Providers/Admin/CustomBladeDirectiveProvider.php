<?php

namespace App\Providers\Admin;

use Blade;
use Illuminate\Support\ServiceProvider;

class CustomBladeDirectiveProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive("CanI", function($expression, $guard = 'admin') {
            return "<?php if (Auth::guard('{$guard}')->check() && Auth::guard('{$guard}')->user()->can({$expression})): ?>";
        });

        Blade::directive("elseCanI", function($expression, $guard = 'admin') {
            return "<?php else if (Auth::guard('{$guard}')->check() && Auth::guard('{$guard}')->user()->can({$expression})): ?>";
        });

        Blade::directive("CantI", function($expression, $guard = 'admin') {
            return "<?php if (! Auth::guard('{$guard}')->check() OR ! Auth::guard('{$guard}')->user()->can({$expression})): ?>";
        });

        Blade::directive("endCantI", function() {
            return "<?php endif; ?>";
        });

        Blade::directive("continue", function() {
            return "<?php continue; ?>";
        });

        Blade::directive("endCanI", function() {
            return "<?php endif; ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
