<?php

namespace App\Providers\Api;

use Response;
use Illuminate\Support\ServiceProvider;
use App\Traits\ResponseFormatBusiness;

class ResponseEncrytionBusinessProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $request = $this->app->make('Illuminate\Http\Request');

        Response::macro('business_api', function($data) {            
            
            $data = ResponseFormatBusiness::encrypt(json_encode($data));
            
            return Response::make($data);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
