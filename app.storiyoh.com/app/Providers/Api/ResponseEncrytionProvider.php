<?php

namespace App\Providers\Api;

use Response;
use Tzsk\Crypt\Facade\StrCrypt;
use Illuminate\Support\ServiceProvider;
use App\Traits\EncryptDecrypt;

class ResponseEncrytionProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $request = $this->app->make('Illuminate\Http\Request');

        Response::macro('api', function($data) use ($request) {
            $request_data = explode('/',$request->path());
            if (in_array('v2', $request_data) && in_array('new_version', $request_data)) {
                $data = EncryptDecrypt::encrypt($data);
            } else if (in_array('v5', $request_data) && in_array('new_version', $request_data)) {                
                $data = urlencode(StrCrypt::encrypt(json_encode($data)));                
            } else if (in_array('v6', $request_data) && in_array('new_version', $request_data)) {                
                $data = urlencode(StrCrypt::encrypt(json_encode($data)));                
            } else if (in_array('v7', $request_data) && in_array('new_version', $request_data)) {
                $data = urlencode(StrCrypt::encrypt(json_encode($data)));
            } else if (in_array('v8', $request_data) && in_array('new_version', $request_data)) {
                $data = urlencode(StrCrypt::encrypt(json_encode($data)));                
            } /*else {
                if ($request->header('response-encrypted', 'No') == 'Yes') {
                    $data = urlencode(StrCrypt::encrypt(json_encode($data)));
                }
            }*/
            return Response::make($data);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
