<?php

namespace App\Providers;

use App\Repositories\DbEpisodeRepository;
use App\Repositories\ElasticSearchRepository;
use App\Repositories\RepositoryInterface;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Schema::defaultStringLength(191);
        \Horizon::auth(function ($request) {
            if (\Auth::guard('admin')->user() == null) {
                echo "Login Failed.";
                exit;
            }
            if (! \Auth::guard('admin')->user()->isSuperAdmin()) {
                echo "Only Super Admin access this page.";
                exit;
            }
            return true;
        });
        Blade::withoutDoubleEncoding();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RepositoryInterface::class, function($app) {
            // This is useful in case we want to turn-off our
            // search cluster or when deploying the search
            // to a live, running application at first.
            // if (!config('services.search.enabled')) {
            //     return new EloquentArticlesRepository();
            // }

             //return new DbEpisodeRepository();

            return new ElasticSearchRepository(
                $app->make(Client::class)
            );  
        });

        $this->bindSearchClient();
    }

    private function bindSearchClient()
    {
        $this->app->bind(Client::class, function ($app) {
            return ClientBuilder::create()
                ->setHosts(['localhost:9200'])
                ->build();
        });
    }
}
