<?php

namespace App\Providers;

use App\Traits\Registrar\ResourceRegistrar;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class RouteResourceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $router = $this->app->make("Illuminate\Routing\Router");
        $this->app->bind("Illuminate\Routing\ResourceRegistrar", function () use ($router) {
            return new ResourceRegistrar($router);
        });
    }
}
