<?php
namespace App\Repositories;

use App\Models\Episode;

/**
* EpisodeRepository Class
*/
class DbEpisodeRepository implements RepositoryInterface
{

    public function search($query = "")
    {
        return Episode::where(function($q) use ($query) {
        	$q->orWhere('title', 'LIKE', "%{$query}%")
        		->orWhere('description', 'LIKE', "%{$query}%");
        })->limit(1000)->get();
    }

}