<?php
namespace App\Repositories;

use App\Models\Episode;
use Elasticsearch\Client;

/**
* EpisodeRepository Class
*/
class ElasticEpisodeRepository implements RepositoryInterface
{

	private $search;

    public function __construct(Client $client) {
        $this->search = $client;
    }

    public function search($request = "")
    {
        $items = $this->searchOnElasticsearch($request);
//dd($items);
        return $this->buildCollection($items);
    }

    private function searchOnElasticsearch($request)
    {
    	$instance = new Episode;
    	$filter = '';

    	/*if($request->has('filter_show'))
    	{
    		$filter = ["term" => ["show_id" => @implode(",",$request->filter_show) ]];
    	}
    	if($request->has('filter_category'))
    	{
    		$filter = ["term" => ["categories" => @implode(",",$request->filter_category) ]];
    	}

    	if($request->has('filter_show') && $request->has('filter_category'))
    	{
    		$filter = [];
    		$filter[] =  ["term" => ["show_id" => @implode(",",$request->filter_show) ]];
    		$filter[] =  ["term" => ["categories" => @implode(",",$request->filter_category) ]];
    	}*/

    	//$filter_param = [[ $filter ]];
//dd($filter_param);
    	$param =  [ 
    				 'index' => $instance->getSearchIndex(),
				     'type' =>  $instance->getSearchType(),
			            'body' => [
							'size' =>1000,
							
					        'query' =>[
				                'bool' => [
					                'must' => [
						                'multi_match' => [
						                	'query' =>". $request->keyword.",
						                	'fields' => ['title','description','tags','categories','networks']
						                ],
					                ],
					                //'filter' => $filter_param,
				               ]
			                ]
			            ]
			        ];
		$params['body']['sort'] = [
									['title' => ['order' => 'asc']],
								  ];   	   
        return $this->search->search($param);
      
    }

    private function buildCollection($items)
    {
        /**
         * The data comes in a structure like this:
         * 
         * [ 
         *      'hits' => [ 
         *          'hits' => [ 
         *              [ '_source' => 1 ], 
         *              [ '_source' => 2 ], 
         *          ]
         *      ] 
         * ]
         * 
         * And we only care about the _source of the documents.
        */
        $hits = array_pluck($items['hits']['hits'], '_source') ?: [];
        
        $sources = array_map(function ($source) {
            // The hydrate method will try to decode this
            // field but ES gives us an array already.
            // $source['tags'] = json_decode($source['tags']);
            // $source['categories'] = json_decode($source['categories']);
            return $source['id'];
        }, $hits);

        $search_episode = [];

        if(sizeof($sources))
        {
	        // We have to convert the results array into Eloquent Models.
	        $search_episode = Episode::with(['show', 'audioFiles'])->whereIn('id',$sources);
	    }


        $final = [
        			"search_episodes" => $search_episode,
        		    //"total" => $items['hits']['total']
        		 ];

        return $final;

    }
}	