<?php
namespace App\Repositories;

use App\Models\Episode;
use App\Models\Show;
use Elasticsearch\Client;

/**
* EpisodeRepository Class
*/
class ElasticSearchRepository implements RepositoryInterface
{

	private $search;

    public function __construct(Client $client) {
        $this->search = $client;
    }

    public function search($request = "")
    {
        $page = $request->page ? $request->page :  1;

        $items = $this->searchOnElasticsearch($request,$page);

//dd($items);
        return $this->buildCollection($items,$page);
    }

    public function show_search($request = "")
    {
        $page = $request->page ? $request->page :  1;

        $items = $this->searchOnShowElasticsearch($request,$page);

        return $this->showbuildCollection($items,$page);
    }

    private function searchOnElasticsearch($request,$page)
    {
        $size = config('config.pagination.episode');

        $from = ($page - 1) * $size;
    	$instance = new Episode;
    	$filter = [];

        try
        {
            if($request->has('keyword') && $request->has('filter_category') && $request->has('filter_show') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["show" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }

            else if($request->has('keyword') && $request->has('filter_category') && $request->has('filter_show'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["show_id" => @implode(",",$request->filter_show) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }
            else if($request->has('keyword') && $request->has('filter_category') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }
            else if($request->has('keyword') && $request->has('filter_show') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["show_id" => @implode(",",$request->filter_show) ]],
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }
            else if($request->has('filter_category') && $request->has('filter_show') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["show" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                          ];
            }
            else if($request->has('keyword') && $request->has('filter_category'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks']
                                                ],
                                            ],
                                         'filter' => [
                                                         ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                         ["term" => ["status" => 'published' ]],
                                                         ["exists" => ["field" => "show_id" ]]
                                                     ],
                                       ]
                                    ]
                                ]
                            ];
            }
            else if($request->has('keyword') && $request->has('filter_show'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["show_id" => @implode(",",$request->filter_show) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }
            else if($request->has('keyword') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'Published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                                ]
                          ];
            }
            else if($request->has('filter_category') && $request->has('filter_show'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["show" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                            ];
            }
            else if($request->has('filter_category') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]] 
                                                    ],
                                       ]
                                    ]
                            ];
            }
            else if($request->has('filter_show') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["show" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }
            else if($request->has('filter_show'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["show" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                            ];
            }
            else if($request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                            ];
            }
            else if($request->has('filter_category'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                       ]
                                    ]
                            ];
            }
            else if($request->has('keyword'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks'],
                                                ],
                                            ],
                                            'filter' => [
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                    ],
                                    ]
                                ]
                              ]
                          ];
            }
            else 
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                           
                                        'filter' => [
                                                        ["term" => ["status" => 'published' ]],
                                                        ["exists" => ["field" => "show_id" ]]
                                                       
                                                    ],
                                       ]
                                    ]
                                ]
                            ];          
            }

          return $this->search->search($param);
        }

        catch(\Exception $e)
        {
            return;
        }
      
    }

    private function searchOnShowElasticsearch($request,$page)
    {
        $size = config('config.pagination.show');

        $from = ($page - 1) * $size;
        $instance = new Show;
        $filter = [];
        try
        {
            if($request->has('keyword') && $request->has('filter_category') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }

            else if($request->has('keyword') && $request->has('filter_category'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["status" => 'published' ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }
            else if($request->has('keyword') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','categories','networks']
                                                ],
                                            ],
                                        'filter' => [
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];
            }
            
            else if($request->has('filter_category') && $request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]]
                                                    ],
                                       ]
                                    ]
                          ];
            }
            else if($request->has('filter_network'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["networks" => @implode(",",$request->filter_network) ]],
                                                        ["term" => ["status" => 'published' ]]
                                                    ],
                                       ]
                                    ]
                            ];
            }
            else if($request->has('filter_category'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'filter' => [
                                                        ["term" => ["categories" => @implode(",",$request->filter_category) ]],
                                                        ["term" => ["status" => 'published' ]]
                                                    ],
                                       ]
                                    ]
                            ];
            }
            else if($request->has('keyword'))
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                            'must' => [
                                                'multi_match' => [
                                                    'query' =>". $request->keyword.",
                                                    'fields' => ['title','description','tags','categories','networks'],
                                                ],
                                            ],
                                            'filter' => [
                                                        ["term" => ["status" => 'published' ]]
                                                    ],
                                    ]
                                ]/*,
                                'sort' => ["title" => ["order" => 'asc']]*/
                              ]
                          ];
            }
            else 
            {
                $param =  [ 
                             'index' => $instance->getSearchIndex(),
                             'type' =>  $instance->getSearchType(),
                                'body' => [
                                    'size' => $size,
                                    'from' => $from,
                                    'query' =>[
                                        'bool' => [
                                        'filter' => [
                                                        ["term" => ["status" => 'published' ]]
                                                    ],
                                       ]
                                    ]
                                ]
                            ];          
            }

          return $this->search->show_search($param);
        }
        catch(\Exception $e)
        {
            //dd($e->getMessage());
            return;
        }
    }

    private function buildCollection($items,$page)
    {

        /**
         * The data comes in a structure like this:
         * 
         * [ 
         *      'hits' => [ 
         *          'hits' => [ 
         *              [ '_source' => 1 ], 
         *              [ '_source' => 2 ], 
         *          ]
         *      ] 
         * ]
         * 
         * And we only care about the _source of the documents.
        */
        $final = [
                    "search_episodes" => [],
                    "total" => 0
                 ];

        if(!empty($items['hits']['hits']))
        {
             $hits = array_pluck($items['hits']['hits'], '_source') ?: [];
        
            $sources = array_map(function ($source) {
                //return $source['id'];
                return $source;
            }, $hits);

            $search_episode = [];

            if(sizeof($sources))
            {
                // We have to convert the results array into Eloquent Models.
               // $search_episode = Episode::with(['show', 'audioFiles'])->whereIn('id',$sources)->get(['id', 'show_id', 'title', 'image']);

                $search_episode =  Episode::with(['show', 'audioFiles'])->hydrate($sources);

            }

            $final = [
                        "search_episodes" => $search_episode,
                        "total" => $items['hits']['total']
                     ];
        }

        return $final;

    }

    private function showbuildCollection($items,$page)
    {

        $final = [
                    "search_show" => [],
                    "total" => 0
                 ];
        
        $search_show = [];
                 
        if(!empty($items['hits']['hits']))
        {
             $hits = array_pluck($items['hits']['hits'], '_source') ?: [];
        
            $sources = array_map(function ($source) {
                return $source['id'];
                //return $source;
            }, $hits);

            

            if(sizeof($sources))
            {

               $source_ids = implode(",",$sources);
               
               // $search_show =  Show::with(['show', 'audioFiles'])->hydrate($sources);
               $search_show =  Show::withCount(['episodes', 'subscribers'])->with('categories')->whereIn('id',$sources)->orderByRaw("FIELD(id,$source_ids)")->get(); 
            }

            return $final = [
                        "search_shows" => $search_show,
                        "total" => $items['hits']['total'],
                        "sources" => $sources
                     ];
        }

        return $final = [
                    "search_shows" => $search_show,
                    "total" => $items['hits']['total'],
                    "sources" => []
                  ];

    }
}	