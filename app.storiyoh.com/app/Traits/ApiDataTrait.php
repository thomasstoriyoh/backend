<?php
namespace App\Traits;

Trait ApiDataTrait {
    
    // Restore User Data 
    
    protected function restore_user_data($user)
    {
        $user_board_data = [];
        $queue_data = [];

        /*$user_boards =  $user->boards()->with('episodes')->withCount('episodes')->get();
        
        $user_board_data = [];
        $user_board_episode = [];

        if(!empty($user_boards))
        {
           foreach($user_boards as $value)
           {    
                $user_board_episode = [];

                foreach($value->episodes as $episode_value)
                {
                    if($episode_value->show_id != '')
                    {
                        $user_board_episode[]  = [
                            "id" => $episode_value->id,
                            "show_id" => $episode_value->show_id,
                            "show_title" => $episode_value->show->title,
                            "episode_title" => trim(str_replace("\n", "", $episode_value->title)),
                            "category" => @implode(", ", $episode_value->categories()->pluck('title')->all()),
                            "image" => ! empty($episode_value->show) ? $episode_value->show->getWSImage(200) : asset('uploads/default/show.png'),
                            "audio_file" => $episode_value->getAudioLink()
                        ];
                    }
                }

                $user_board_data[] = [ 
                    "id" => $value->id,
                    "board_name" => $value->title,
                    "image" => ! empty($value->image) ? $value->getImage(200) : asset('uploads/default/board.png'),
                    "no_of_episodes" => $value->episodes_count,
                    "last_updated" => $value->updated_at->diffForHumans(),
                    "user_board_episode" => $user_board_episode
                ];                
           }
        }

        $user_queue = $user->user_episode_queue()->orderBy('order')->get();
        $queue_data = [];

        if(!empty($user_queue))
        {           
           foreach($user_queue as $value)
           { 
                $queue_data[] = [
                    "id" => $value->id, 
                    "title" => $value->title,
                    "show_id" => ! empty($value->show->id) ? $value->show->id : '',
                    "show_name" => ! empty($value->show->title) ? trim(str_replace("\n", "", $value->show->title)) : '',
                    "image" => ! empty($value->show->image) ? $value->show->getWSImage(200) : asset('uploads/default/show.png'), 
                    "duration" => $value->getDurationText(), 
                    "audio_file" => $value->getAudioLink()
                ];
           }
        }*/

        $data = [
            'user_board_data' => $user_board_data,
            'queue_data' => $queue_data
        ];

        return $data;        
    }
}
