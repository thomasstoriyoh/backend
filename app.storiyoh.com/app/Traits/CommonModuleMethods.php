<?php
namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;

trait CommonModuleMethods {

    /**
     * The forced dimension.
     *
     * @var array
     */
    protected $forceDimension = null;

    /**
     * Width array if fixed.
     *
     * @var string
     */
    protected $thumbsWidth = null;

    /**
     * Height array if fixed.
     *
     * @var string
     */
    protected $thumbsHeight = null;

    /**
     * Assist with locating the view.
     *
     * @param $view
     * @param array $data
     * @param bool $strict
     * @return mixed
     */
    protected function view($view, $data =[], $strict = false) {
        if ($this->getViewRoot() && ! $strict) {
            $view = $this->getViewRoot() . "." . $view;
        }

        if (view()->exists($view)) {
            return view($view, $data);
        }

        return view('errors.404');
    }

    /**
     * Short hand for route names.
     *
     * @param null $route
     * @return string
     */
    private function route($route = null) {
        if (is_null($route)) {
            return $this->rootRoute . ".index";
        }
        return $this->rootRoute . "." . $route;
    }

    /**
     * Upload Image of Module.
     *
     * @param $file
     * @param $filename
     * @param $directory
     * @param null $thumbsDir
     * @return object
     */
    private function uploadModuleImage($file, $filename, $directory, $thumbsDir = null)
    {
        $result = $this->validateUpload($file, $filename, $directory, 'image', $thumbsDir);
        if ( !$result->status) {
            return $result;
        }

        $filename .= "." . $file->getClientOriginalExtension();

        $image = $file->move($directory, $filename);
        $thumbs = [];

        if ( !is_null($thumbsDir)) {
            $dimensions = $this->getWidthHeightFormat($this->thumbsWidth, $this->thumbsHeight);

            foreach ($dimensions as $dimension) {
                $thumb_name = (int) $dimension->width . "_" . (int) $dimension->height . "_" . $filename;
                Image::make($image)->fit((int) $dimension->width, (int) $dimension->height)->save($directory . "/" . $thumbsDir . "/" . $thumb_name);
                $thumbs[(int) $dimension->width . "-" . (int) $dimension->height] = $thumb_name;
            }
        }
        return (object) ['status' => true, 'filename' => $filename, 'thumbs' => $thumbs];
    }

    /**
     * Upload Cropped Module Image.
     *
     * @param $file
     * @param $filename
     * @param $directory
     * @param null $thumbsDir
     * @return object
     */
    private function uploadModuleImageWithChords($file, $filename, $directory, $thumbsDir = null) {
        $result = $this->validateUpload($file, $filename, $directory, 'image', $thumbsDir);
        if ( !$result->status) {
            return $result;
        }

        $filename_ext = $filename . "." . $file->getClientOriginalExtension();

        $image = $file->move($directory, $filename_ext);
        $thumbs = [];

        if ( !is_null($thumbsDir)) {

            if (!empty($this->crop_chords)) {
                $thumb_name = (int) $this->crop_chords->width . "_" . (int) $this->crop_chords->height . "_" . $filename_ext;
                Image::make($image)->crop((int) $this->crop_chords->w, (int) $this->crop_chords->h, (int) $this->crop_chords->x, (int) $this->crop_chords->y)
                    ->fit((int) $this->crop_chords->width, (int) $this->crop_chords->height)->save($directory . "/" . $thumbsDir . "/" . $thumb_name);
            }

            $dimensions = $this->getWidthHeightFormat($this->thumbsWidth, $this->thumbsHeight);
            foreach ($dimensions as $dimension) {

                $thumb_name = $dimension->width . "_" . $dimension->height . "_" . $filename_ext;
                Image::make($image)->fit((int) $dimension->width, (int) $dimension->height)->save($directory . "/" . $thumbsDir . "/" . $thumb_name);
                $thumbs[(int) $dimension->width . "-" . (int) $dimension->height] = $thumb_name;
            }
        }
        return (object) ['status' => true, 'filename' => $filename_ext, 'thumbs' => $thumbs];
    }

    /**
     * Upload Files of Module.
     *
     * @param $file
     * @param $filename
     * @param $directory
     * @return object
     */
    private function uploadModuleFile($file, $filename, $directory) {
        $result = $this->validateUpload($file, $filename, $directory, 'file');
        if ( !$result->status) {
            return $result;
        }

        $filename .= "." . $file->getClientOriginalExtension();
        $file->move($directory, $filename);
        return (object) ['status' => true, 'filename' => $filename];
    }

    /**
     * Validate Upload File before upload.
     *
     * @param $file
     * @param $filename
     * @param $directory
     * @param $type
     * @param null $thumbDir
     * @return object
     */
    private function validateUpload($file, $filename, $directory, $type, $thumbDir = null) {
        if ( !$file->isValid()) {
            return (object) ['status' => false, 'error' => 'ERROR'];
        }

        if (!is_dir($directory) OR !is_writable($directory)) {
            return (object) ['status' => false, 'error' => 'DIRECTORY'];
        }

        preg_match("/^([\\w-_.]+)$/", $filename, $match_name);
        if (empty($match_name)) {
            return (object) ['status' => false, 'error' => 'FILENAME'];
        }

        if ($type == 'image') {
            preg_match("/^image\\/(.*)$/", $file->getMimeType(), $match_mime);
            if (empty($match_mime)) {
                return (object) ['status' => false, 'error' => 'NOT_IMAGE'];
            }
        }

        $extensions = @$this->allowedFileExtensions;

        if ( !empty($extensions)) {
            if ( !in_array(strtolower($file->getClientOriginalExtension()), $extensions)) {
                return (object) ['status' => false, 'error' => 'EXTENSION_SUPPORT'];
            }
        }

        if ( !is_null($thumbDir)) {
            if (!is_dir($directory . "/" . $thumbDir) OR !is_writable($directory . "/" . $thumbDir)) {
                return (object) ['status' => false, 'error' => 'THUMB_DIRECTORY'];
            }
        }
        return (object) ['status' => true];
    }

    /**
     * Process Uploaded file errors.
     *
     * @param $response
     * @return $this|bool
     */
    private function processUploadError($request, $response, $ajax = false) {
        if (! $response->status) {
            switch ($response->error) {
                case 'DIRECTORY':
                    $message = 'Upload Directory does not exist or not writable';
                    break;

                case 'THUMB_DIRECTORY':
                    $message = 'Upload Thumb Directory does not exist or not writable';
                    break;

                case 'FILENAME':
                    $message = 'Filename can only contain Letters, Numbers, Dashes & Underscores.';
                    break;

                case 'NOT_IMAGE':
                    $message = 'Please upload an image only.';
                    break;

                case 'EXTENSION_SUPPORT':
                    $message = 'Unsupported file format / Extension.';
                    break;

                default :
                    $message = 'Some Error Occured during Upload.';
                    break;
            }

            if (! $ajax) {
                return redirect()->back()->withInput($request->all())
                    ->withErrors(['image' => $message]);
            }
            return ['status' => $response->status, 'message' => $message];
        }
        return true;
    }

    /**
     * Get formatted Image width & height.
     *
     * @param $width
     * @param $height
     * @return array
     */
    private function getWidthHeightFormat($width, $height) {
        $dimensions = [];

        if (!empty($this->forceDimension)) {
            return $this->forceDimension = $this->getForcedDimension();
        }

        if (! is_null($width)) {
            if ( is_array($width)) {
                if ( is_array($height)) {
                    foreach ($width as $key => $value) {
                        $dimensions[] = (object) ["width" => $value, "height" => $height[$key]];
                    }
                    return (object) $dimensions;
                } else if ( typeOf($height) != 'object') {
                    foreach ($height as $value) {
                        $dimensions[] = (object) ["width" => $value, "height" => $height];
                    }
                } else {
                    return $dimensions;
                }
            } else if ( typeOf($width) != 'object') {
                if ( is_array($height)) {
                    foreach ($height as $key => $value) {
                        $dimensions[] = (object) ["width" => $width, "height" => $value];
                    }
                    return $dimensions;
                } else if ( typeOf($height) != 'object') {
                    $dimensions[] = (object) ["width" => $width, "height" => $height];
                    return $dimensions;
                }
            } else {
                return $dimensions;
            }
        }

        return $dimensions;
    }

    /**
     * Publish item.
     *
     * @param $object
     * @return \Illuminate\Http\RedirectResponse
     */
    private function publishModule($object) {
        $object->status = "Published";
        $object->setEventType('Published');
        $object->save();
        return redirect()->back();
    }

    /**
     * Publish item.
     *
     * @param $object
     * @return \Illuminate\Http\RedirectResponse
     */
    private function unpublishModule($object) {
        $object->status = "Draft";
        $object->setEventType('Drafted');
        $object->save();
        return redirect()->back();
    }

    /**
     * Approve item.
     *
     * @param $object
     * @return \Illuminate\Http\RedirectResponse
     */
    private function approveModule($object) {
        $object->admin_status = "Approved";
        $object->setEventType('Approved');
        $object->save();
        return redirect()->back();
    }

    /**
     * Approve item.
     *
     * @param $object
     * @return \Illuminate\Http\RedirectResponse
     */
    private function unapproveModule($object) {
        $object->admin_status = "Pending";
        $object->setEventType('Unapproved');
        $object->save();
        return redirect()->back();
    }

    /**
     * Get the view paath.
     *
     * @return boolean
     */
    protected function getViewRoot() {
        if (property_exists($this, 'viewRoot')) {
            if (!empty($this->viewRoot)) {
                return $this->viewRoot;
            }
        }
        return false;
    }

    /**
     * Delete an image or file.
     *
     * @param $filename
     * @param $directory
     * @param null $thumbsDir
     * @return bool
     */
    private function deleteImageOrFile($filename, $directory, $thumbsDir = null) {
        @unlink(public_path($directory) . "/" . $filename);

        if ( !empty($thumbsDir)) {
            $dimensions = $this->getWidthHeightFormat($this->thumbsWidth, $this->thumbsHeight);
            foreach ($dimensions as $dimension) {
                @unlink(public_path($directory . "/" . $thumbsDir) . "/" . $dimension->width . "_" . $dimension->height ."_" . $filename);
            }
        }
        return true;
    }

    /**
     * Get the forced dimention.
     *
     * @return array
     */
    private function getForcedDimension() {
        $dimension = [];
        foreach ($this->forceDimension as $value) {
            if (gettype($value) != 'object') {
                $dimension[] = (object) $value;
            } else {
                $dimension[] = $value;
            }
        }
        return $dimension;
    }


    /**
     * Parse the order tree.
     *
     * @param type $order
     * @return array
     */
    protected function parseOrder($order) {
        $sequence = [];
        foreach ($order as $node) {
            $sequence[] = $node['id'];

            if (!empty($node['children'])) {
                $return_value = $this->parseOrder($node['children']);
                foreach ($return_value as $item) {
                    $sequence[] = $item;
                }
                //$sequence[] = $return_value;
            }
        }
        return $sequence;
    }


    protected function parseEditorBody($body) {
        $mimes = [
            'bmp'	=>	array('image/bmp', 'image/x-bmp', 'image/x-bitmap', 'image/x-xbitmap',
                'image/x-win-bitmap', 'image/x-windows-bmp', 'image/ms-bmp', 'image/x-ms-bmp',
                'application/bmp', 'application/x-bmp', 'application/x-win-bitmap'),
            'gif'	=>	array('image/gif'),
            'jpeg'	=>	array('image/jpeg', 'image/pjpeg'),
            'jpg'	=>	array('image/jpeg', 'image/pjpeg'),
            'jpe'	=>	array('image/jpeg', 'image/pjpeg'),
            'jp2'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
            'j2k'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
            'jpf'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
            'jpg2'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
            'jpx'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
            'jpm'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
            'mj2'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
            'mjp2'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
            'png'	=>	array('image/png',  'image/x-png'),
            'tiff'	=>	array('image/tiff'),
            'tif'	=>	array('image/tiff'),
        ];
        $re = '/<img src="data:image\/([a-z]+);base64,([\w\W][^\s]+)"/';
        preg_match_all($re, $body, $matches);

        if (empty($matches[1])) {
            return $body;
        }

        foreach ($matches[1] as $key => $match) {
            $extension = $match;
            foreach ($mimes as $ext => $mime) {
                if (in_array('image/'.$match, $mime)) {
                    $extension = $ext;
                    break;
                }
            }

            $filename = date('YmdHis').rand(100000, 999999).".".$extension;
            file_put_contents(public_path('uploads/summernote/'.$filename), base64_decode($matches[2][$key]));
            $img = '<img src="'.asset('uploads/summernote/'.$filename).'"';

            $body = str_replace($matches[0][$key], $img, $body);
        }

        return $body;
    }

    /**
     * For uploading images...
     *
     * @param Request $request
     * @return array
     */
    public static function imageUploadonS3(Request $request, $folder_path) {
        if ($request->hasFile('file')) {
            $allowedfileExtension = ['jpeg','png','jpg','gif','svg'];
            $extension = $request->file->getClientOriginalExtension();            
            $filename = "";
            if(config('config.upload_platform') != "local") {
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    $filename = date('YmdHis').rand(100000, 999999). "." . $extension;                    
                    
                    //upload image on S3 Bucket
                    $image_name = $folder_path. '/' . $filename;                    
                    Storage::disk('s3')->put($image_name, file_get_contents($request->file));
                }
            }

            $uploaded_files[] = (object) ['status' => true, 'filename' => $filename, 'thumbs' => []];
        
            return $uploaded_files;
        }

        $uploaded_files[] = (object) ['status' => false, 'filename' => "", 'error' => 'Please upload Image.'];

        return $uploaded_files;
    }
}
