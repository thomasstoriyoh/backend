<?php
namespace App\Traits;

/**
 * Description of Response
 *
 * @author Neetendra
 */
trait EncryptDecrypt {

    /**
     * Encryption Code
     */
    public static function encrypt($data)
    {
        $data = openssl_encrypt(json_encode($data), 'aes-256-cbc', config('config.website.encrypted_key'), 0, config('config.website.encrypted_iv'));
        return urlencode($data);
    }

    /**
     * Decryption Code
     * @return string
     */
    public static function decrypt($data)
    {
        $data = openssl_decrypt($data, 'aes-256-cbc', config('config.website.encrypted_key'), 0, config('config.website.encrypted_iv'));        
        return $data;
    }
}