<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

/**
 * Description of FireBase for Android
 *
 * @author Neetendra
 */
trait FireBaseAndroid
{
    public static function sendFireBaseNotification($token, $title, $message, $icon = '', $data)
    {
        //$token = array_unique($token);
        $fields = [
            'registration_ids' => $token,
            'data' => $data
        ];
        //dd(json_encode($fields));
        return self::sendPushNotification($fields, $token);
    }

    /*
    * This function will make the actuall curl request to firebase server
    * and then the message is sent
    */
    public static function sendPushNotification($fields, $token = [])
    {
        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';

        //building headers for the request
        $headers = [
            'Authorization: key=' . config('config.push_notification'),
            'Content-Type: application/json'
        ];

        //Initializing curl to open a connection
        $ch = curl_init();

        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);

        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);

        //adding headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //adding the fields in json format
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        //finally executing the curl request
        $result = curl_exec($ch);

        DB::table('push_log')->insert(
            ['text1' => json_encode($fields), 'text2' => $result, 'text3' => json_encode($token), 'queue_date' => date('Y-m-d H:i:s'), 'platform' => 'android']
        );

        //\Log::info("Result".$result);

        //dd($result);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }

        //Now close the connection
        curl_close($ch);

        //and return the result
        return $result;
    }
}
