<?php

namespace App\Traits;

use Illuminate\Support\Facades\Request;

/**
 * Trait Generate Passport Token
 *
 * @package App\Traits
 */
trait GeneratePassportToken
{
    /**
     * Generate Access token
     */
    public static function generateAccessToken($request, $username, $password)
    {
        $params = [
            'grant_type' => 'password',
            'client_id' => config('config.client_id'),
            'client_secret' => config('config.client_secret'),
            'scope' => '',
            'username' => $username,
            'password' => $password,
        ];
        
        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        $response = \Route::dispatch($proxy);

        return json_decode((string) $response->getContent(), true);
    }

    /**
     * Refresh Access token
     */
    public static function refreshAccessToken($request)
    {
        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id' => config('config.client_id'),
            'client_secret' => config('config.client_secret'),
            'scope' => '',
        ];

        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        $response = \Route::dispatch($proxy);

        return json_decode((string) $response->getContent(), true);
    }
}
