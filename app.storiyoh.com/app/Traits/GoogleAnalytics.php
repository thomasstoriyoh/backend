<?php
namespace App\Traits;

use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Collection;
use Spatie\Analytics\Period;
//use Spatie\Analytics\Analytics;
use Analytics;
trait GoogleAnalytics {

    /**
     * Call the query method for the common info
     *
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $metrics
     * @param array $others
     * @param string $groupBy
     * @return mixed
     */
    public function getCommonDataForPeriod(DateTime $startDate, DateTime $endDate, $metrics, $others = [], $groupBy = 'date')
    {        
        //$answer = Analytics::performQuery($startDate, $endDate, $metrics, $others);        
        $answer = Analytics::performQuery(Period::create($startDate, $endDate), $metrics, $others); 
        if (is_null($answer->rows)) {
            return new Collection([]);
        }

        $visitorData = [];
        foreach ($answer->rows as $dateRow) {
            $visitorData[] = [$groupBy => Carbon::createFromFormat(($groupBy == 'yearMonth' ? 'Ym' : 'Ymd'), $dateRow[0]),'sessions'=>$dateRow[1]];
        }

        return new Collection($visitorData);
    }
}