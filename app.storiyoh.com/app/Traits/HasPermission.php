<?php
namespace App\Traits;


use App\Models\Admin\Module;
use App\Models\Admin\Permission;

trait HasPermission
{
    /**
     * Admin has many permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    protected function permissions()
    {
        return $this->hasMany(Permission::class, 'admin_id');
    }

    /**
     * Add Permissions to the user.
     *
     * @param array $permissions
     * @return array|bool
     */
    public function addPermissions(array $permissions)
    {
        $permissions = $this->parsePermissions($permissions);
        $validate = $this->validatePermissions($permissions);
        if (! empty($validate)) {
            $result = [];
            foreach ($validate as $key => $item) {
                $result[] = $this->addPermission($key, $item);
            }
            return $result;
        }

        return false;
    }

    /**
     * Add a single permission.
     *
     * @param $key
     * @param $items
     * @return Permission|bool
     */
    public function addPermission($key, $items) {
        $module = Module::whereSlug($key)->first(['id']);
        $permission = Permission::whereAdminId($this->id)->whereModuleId($module->id)->first();
        if (! $permission) {
            $permission = new Permission();

            $permission->admin_id = $this->id;
            $permission->module_id = $module->id;
        }
        
        $permission->create = false;
        $permission->read = false;
        $permission->update = false;
        $permission->delete = false;
        
        if (!empty($items)) {
            foreach ($items as $item) {
                $permission->$item = true;
            }
        }
        if (in_array('create', $items) OR in_array('update', $items) OR in_array('delete', $items)) {
            $permission->read = true;
        }

        $permission = $permission->save();

        return $permission;
    }

    /**
     * Sync Existing permissions...
     *
     * @param array $permissions
     * @return array|bool
     */
    public function syncPermissions(array $permissions) {
        $permissions = $this->parsePermissions($permissions);
        $validate = $this->validatePermissions($permissions);
        $existing = $this->getPermissions(true);

        $permissions =  array_merge($existing, $validate);
        $difference = array_diff(array_keys($existing), array_keys($validate));
        foreach ($difference as $item) {
            $permissions[$item] = [

            ];
        }
        $result = [];
        foreach ($permissions as $key => $permission) {
            $result[] = $this->addPermission($key, $permission);
        }
        return $result;
    }

    /**
     * Parse Permissions.
     *
     * @param $permissions
     * @return array
     */
    protected function parsePermissions($permissions)
    {
        $formatted = [];
        foreach ($permissions as $permission) {
            list($action, $module) = explode(".", $permission);
            $formatted[$module][] = $this->parsepermission($permission);
        }

        return $formatted;
    }

    /**
     * Parse permission to array format.
     *
     * @param $permission
     * @return string
     */
    protected function parsePermission($permission) {
        list($action, $module) = explode(".", $permission);
        if ($action == 'add') { $action = 'create'; }
        if ($action == 'store') { $action = 'create'; }
        if ($action == 'edit') { $action = 'update'; }
        if ($action == 'show') { $action = 'read'; }
        if ($action == 'view') { $action = 'read'; }
        if ($action == 'destroy') { $action = 'delete'; }
        return $action;
    }

    /**
     * Crosscheck permissions with database.
     *
     * @param $permissions
     * @return array|bool
     */
    protected function validatePermissions($permissions) {
        $slugs = array_keys($permissions);
        $modules = Module::whereIn('slug', $slugs)->get(['id', 'slug']);
        if ($modules->count()) {
            $module_slugs = [];
            foreach ($modules as $module) {
                $module_slugs[$module->slug] = $permissions[$module->slug];
            }
            return $module_slugs;
        } else {
            return [];
        }
    }

    /**
     * Can a user do the set of permissions?
     *
     * @param $permissions
     * @param string $operator
     * @return bool
     */
    public function can($permissions, $operator = 'AND') {
        if (is_array($permissions)) {
            return $this->hasPermissions($permissions, $operator);
        }
        if (is_string($permissions)) {
            return $this->hasPermissions([$permissions], $operator);
        }
        return false;
    }

    /**
     * Does the user has set of permissions?
     *
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission, $operator = 'AND') {
        if (is_string($permission)) {
            $permission = [$permission];
        }

        return is_array($permission) ? $this->hasPermissions($permission, $operator) : false;
    }

    /**
     * Does the user has set of permissions?
     *
     * @param $permissions
     * @param string $operator
     * @return bool
     */
    public function hasPermissions($permissions, $operator = 'AND') {
        if ($this->isSuperAdmin()) {
            return true;
        }
        
        $permissions = $this->parentPermission($permissions);
        $permissions = $this->parsePermissions($permissions);
        $validate = $this->validatePermissions($permissions);
        
        if ($validate) {
            $assigned = $this->getPermissions();
            
            foreach ($validate as $key => $item) {
                if (strtoupper($operator) == 'OR') {
                    if (in_array($key, array_keys($assigned))) {
                        foreach ($item as $i) {
                            if ($assigned[$key][$i]) {
                                return true;
                            }
                        }
                    }
                } else if (strtoupper($operator == 'AND')) {
                    if (in_array($key, array_keys($assigned))) {
                        $bool = true;
                        foreach ($item as $i) {
                            if (empty($assigned[$key][$i])) {
                                $bool = false;
                            }
                        }
                        return $bool;
                    } else {
                        return false;
                    }
                }
            }

        }

        return false;
    }
    
    /**
     * Find permission format for the parent.
     * 
     * @param array $permissions
     * @return array
     */
    protected function parentPermission($permissions) {
        $parent_permissions = [];
        foreach ($permissions as $permission) {
            list($action, $slug) = explode('.', $permission);
            $module = Module::whereSlug($slug)->first();
            if (@$module->module_id == 0) {
                $parent_permissions[] = $permission;
            }else{
                $return_array = $this->parentPermission([$action . '.' . $module->parent->slug]);
                $parent_permissions[] = array_shift($return_array);
            }
        }
        
        return $parent_permissions;
    }

    /**
     * Get all the permissions associated with the user.
     *
     * @return array
     */
    public function getPermissions($strict = false) {
        $permissions = $this->permissions;
        $permission_array = [];
        foreach ($permissions as $permission) {
            if (!$strict) {
                
                $actions = [];
                $methods = config('permission');
                foreach ($methods as $key => $method) {
                    foreach ($method as $met) {
                        $actions[$met] = $permission->$key;
                    }
                }
            }else {
                $actions = [
                    'create' => $permission->create ? true : false,
                    'read' => $permission->read ? true : false,
                    'update' => $permission->update ? true : false,
                    'delete' => $permission->delete ? true : false,
                ];
            }
            $permission_array[$permission->module->slug] = $actions;
        }
        return $permission_array;
    }
}