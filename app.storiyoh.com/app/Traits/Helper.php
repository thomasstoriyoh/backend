<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Traits;

use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Models\SearchKeyword;

/**
 * Description of Helper
 *
 * @author Neetendra
 */
class Helper {
    //put your code here
    
    /**
     * function for increase view count.
     *
     * @return mixed
     */
    public static function increaseVisitCount($class_basename, $id) {
        $module = \App\Models\Admin\Module::whereController($class_basename)->first();
        $date = \Carbon\Carbon::now()->format('Y-m-d');
        $viewEntry = \App\Models\Admin\ModuleEntryView::whereModuleId($module->id)->whereItemId($id)
            ->whereViewDate($date)->first();
        //return dd($viewEntry);
        if ($viewEntry) {
            $viewEntry->count += 1;
            \App\Models\Admin\ModuleEntryView::whereModuleId($module->id)->whereItemId($id)
                ->whereViewDate($date)->update(['count' => $viewEntry->count]);
        } else {
            \App\Models\Admin\ModuleEntryView::create(['module_id' => $module->id, 'item_id' => $id, 'view_date' => $date, 'count' => 1]);
        }
    }
    
    /**
     * function for sent SMS.
     *
     * @return mixed
     */
    
    public static function sendSMS($mobileno, $message) {
        
        // Textlocal account details
        $username = "";
        $hash = "";

        // Message details
        //$numbers = array(918123456789, 918987654321);
        //$numbers = implode(',', $numbers);

        $numbers = "91".$mobileno;
        $sender = urlencode('PLANFD');
        $message = rawurlencode($message);

        // Prepare data for POST request
        $data = array('username'=>$username, 'hash'=>$hash, 'numbers'=>$numbers, "sender"=>$sender, "message"=>$message);
        //print_r($data);

        // Send the POST request with cURL
        $ch = curl_init('http://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //$response = curl_exec($ch);
        $response = true;
        //curl_close($ch);

        // Process your response here
        return $response;
    }
    
    /**
     * function for sent mail.
     *
     * @return mixed
     */
    public static function sendAllEmail($mailData) {
        Mail::send($mailData['file_path'], compact('mailData'), function ($message) use ($mailData) {
            $message->from($mailData['from_email'], $mailData['from_name']);
            $message->to($mailData['to_email'], $mailData['to_name'])->subject($mailData['subject']);
            if ($mailData['filename']) {
                $message->attach(public_path($mailData['filename']));
            }
        }); 
    }
    
    /**
     * 
     * @param type $n
     * @param type $precision
     * @return type
     */
    public static function shorten_count($n,$precision = 1)
    {
        if ($n < 900) 
        {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } 
        else if ($n < 900000) 
        {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } 
        else if ($n < 900000000)
        {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } 
        else if ($n < 900000000000) 
        {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } 
        else 
        {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) 
        {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }
        return $n_format . $suffix;
    }

    /**
     * Undocumented function
     *
     * @param [type] $itune_id
     * @param [type] $image
     * @return void
     */
    public static function createImage ($itune_id, $image)
    {
        $image_name = "";

        if ($itune_id && $image) {
            try{
                // $img = Image::make($image)->encode('jpg');
                // $image_name = $itune_id.".jpg";                
                // $file = public_path('uploads/show_assets/' . $image_name);
                // finally we save the image as a new file
                // $img->save($file);

                //$img = Image::make($image)->encode('jpg');
                $img = $image;
                $image_name = $itune_id.".jpg";
                $t = Storage::disk('s3')->put("shows/".$image_name, file_get_contents($img));                
            } catch (\Exception $e) {
                return $image_name;
            }           
        }
        
        return $image_name;
    }

    /**
     * This function is use for create slug
     *
     * @param [type] $string
     * @return void
     */
    public static function make_slug($text) {        
        
        // remove unwanted characters
        $text = preg_replace('/\s+/u', '-', strtolower(trim($text)));
        
        // trim
        $text = trim($text, '-');

        //Remove special character
        $text = self::remove_accent($text);
    
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        //Remove character (-) from start and end
        $text = trim($text, "-");
            
        // lowercase
        $text = strtolower($text);
    
        if (empty($text)) {
            return 'n-a';
        }
    
        return $text;
    }

    public static function remove_accent($str)
    {
        $a = array(',', '"', '(', ')', '[', ']', ':', '®', '!', '`', '~', '=', '.', '?', '#', '@', '$', '&', '^', "'", '%', '/', '|', '\\', "’", '°', '“', '”');
        $b = array('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
        return str_replace($a, $b, $str);
    }

    public static function trending_data($noofrecords = 10) {
        $date = \Carbon\Carbon::today()->subDays(10);
        $trendingData = SearchKeyword::orderBy('count', 'DESC')->where('updated_at', '>=', date($date))
                ->take($noofrecords)->get(['id', 'keyword', 'type', 'show_id', 'count']);

        //dump($trendingData->toArray());

        $data = [];
        foreach ($trendingData as $item) {
            $link = '#';
            $show_id = '';
            if ($item->type == 'Show') {
                $link = self::make_slug($item->keyword) . '/episodes/' . $item->show_id;
                $show_id = $item->show_id;
            } else {
                $link = strtolower($item->keyword);
            }
            $data[] = [
                'id' => $item->id,
                'title' => $item->keyword,
                'search_keyword' => $item->keyword,
                'type' => $item->type,
                'link' => $link,
                'show_id' => $show_id,
                //'count' => $item->count,
            ];
        }

        return $data;
    }

    /**
     * Remove Special Emoticon
     *
     * @param [type] $string
     * @return void
     */
    public static function remove_emoji($string) {
        // Match Emoticons
        $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clear_string = preg_replace($regex_emoticons, '', $string);

        // Match Miscellaneous Symbols and Pictographs
        $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clear_string = preg_replace($regex_symbols, '', $clear_string);

        // Match Transport And Map Symbols
        $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clear_string = preg_replace($regex_transport, '', $clear_string);

        // Match Miscellaneous Symbols
        $regex_misc = '/[\x{2600}-\x{26FF}]/u';
        $clear_string = preg_replace($regex_misc, '', $clear_string);

        // Match Dingbats
        $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
        $clear_string = preg_replace($regex_dingbats, '', $clear_string);

        return $clear_string;
    }

    /**
     * This function is use for update image name
     *
     * @param [type] $itune_id
     * @param [type] $image
     * @return void
     */
    public static function createPodcastImage ($itune_id, $image)
    {
        $image_name = "";

        if ($itune_id && $image) {
            try{
                // $img = Image::make($image)->encode('jpg');
                // $image_name = $itune_id.".jpg";                
                // $file = public_path('uploads/show_assets/' . $image_name);
                // finally we save the image as a new file
                // $img->save($file);

                //$img = Image::make($image)->encode('jpg');
                $img = $image;
                //$image_name = $itune_id.".jpg";
                $image_name = date('YmdHis').rand(100000, 999999).".jpg";               
                $t = Storage::disk('s3')->put("shows/".$image_name, file_get_contents($img));                
            } catch (\Exception $e) {
                return $image_name;
            }           
        }
        
        return $image_name;
    }
}
