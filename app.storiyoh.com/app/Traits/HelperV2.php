<?php
namespace App\Traits;

use Aws\S3\S3Client;
use App\Models\ShowPurchase;
use App\Models\EpisodePurchase;
use Request;
use Bcm\Paytm\BcmPaytmWallet;
use Illuminate\Support\Facades\Mail;

/**
 * Description of Response
 *
 * @author Neetendra
 */
trait HelperV2 {

    /**
     * This function is use for create signed url
     */
    public function getSignedUrl($audio_file_name)
    {        
        //Instantiate an Amazon S3 client.
        $s3Client = new S3Client([
            'version' => 'latest',
            'region'  => 'ap-south-1'
        ]);

        //Creating a presigned URL
        $cmd = $s3Client->getCommand('GetObject', [
            'Bucket' => config('config.s3_bucket_name'),
            'Key' => $audio_file_name,
            //'ResponseContentDisposition' => 'attachment'
        ]);
        
        $request = $s3Client->createPresignedRequest($cmd, '+5 minutes');
        
        // Get the actual presigned-url
        return (string)$request->getUri();        
    }

    /**
    * This function check if Series / Episode purchsed by the user or not
    */
    public function check_if_item_purchase_by_user($content_user_id, $current_user, $contentId, $type, $parent_id = null, $selling_price = null)
    {
        //This code add for free
        // $status = "yes";
        // return $status;        
        if ($content_user_id == $current_user) {
            return "yes";
        }
        $status = "no";
        if ($type == "series") {
            $purchased_status = ShowPurchase::where('show_id', $contentId)->where('user_id', $current_user)->count();
            if ($purchased_status) {
                $status = "yes";
                return $status;
            }
        } else if ($type == "episode") {
            $purchased_show_status = ShowPurchase::where('show_id', $parent_id)->where('user_id', $current_user)->count();
            //dd($purchased_show_status, $current_user, $contentId);
            if ($purchased_show_status == 0) {
                $purchased_status = EpisodePurchase::where('episode_id', $contentId)->where('user_id', $current_user)->count();
                if ($purchased_status) {
                    $status = "yes";
                    return $status;
                }
            }
            if ($purchased_show_status) {
                $status = "yes";
                return $status;
            }
        }
        //check selling price for add status free for purchase
        if (in_array($selling_price, ['0', '0.0', '0.00', '0.000'])) {
            $status = "free";
            return $status;
        }

        return $status;
    }

    /**
     * This function is use to get paytm checksum
     */
    public function getCheckSum($request, $invoice_no, $cust_transaction_id, $selling_price) {        
        try {
            $request['ORDER_ID'] = $invoice_no;
            $request['CUST_ID'] = $cust_transaction_id;
            $request['TXN_AMOUNT'] = $selling_price;
            $request['CALLBACK_URL'] = 'N';
        
            $request_parameter = $request->except('lang', 'content_id', 'product_type', 'platform', 'payment_mode', 'giftBy', 'gift_message');
            $request = new \Illuminate\Http\Request($request_parameter);
                    
            $paytm = new BcmPaytmWallet($request);

            $paytmData = $paytm->generateChecksum();

            return $paytmData;
        } catch (\Exception $ex) {           
            return false;            
        }
    }

    /**
     * This function return total in words
     * 
     */
    public static function numberToWords(float $number, $vendor)
    {
        $wrd = $vendor == 'Paypal' ? 'Dollar' : 'Rupees';
        $wrds =  $vendor == 'Paypal' ? 'Cents' : 'Paise';
        $no = (int) floor($number);
        $point = (int) round(($number - $no) * 100);
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array(
            '0' => '', '1' => 'One', '2' => 'Two',
            '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
            '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
            '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
            '13' => 'Thirteen', '14' => 'Fourteen',
            '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
            '18' => 'Eighteen', '19' => 'Nineteen', '20' => 'Twenty',
            '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
            '60' => 'Sixty', '70' => 'Seventy',
            '80' => 'Eighty', '90' => 'Ninety'
        );
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;

            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str[] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);


        if (floor($point / 10) * 10 <= 10) {
            $points = ($point) ?  $words[$point] : '';
        } else {
            $points = ($point) ?
                "" . $words[floor($point / 10) * 10] . " " .
                $words[$point = $point % 10] : '';
        }

        if ($points != '') {
            return $wrd . " " . $result . ' and ' . $points . " " . $wrds . " Only";
        } else {
            return $wrd . " " . $result . " Only";
        }
    }

    /**
     * function for sent mail.
     *
     * @return mixed
     */
    public static function sendAllEmail($mailData) {
        Mail::send($mailData['file_path'], compact('mailData'), function ($message) use ($mailData) {
            $message->from($mailData['from_email'], $mailData['from_name']);
            $message->to($mailData['to_email'], $mailData['to_name'])->subject($mailData['subject']);
            if ($mailData['filename']) {
                $message->attach($mailData['filename']);
            }
        }); 
    }

    /**
     * This function is check if current user is seller or not
     */
    public static function isSeller($user) {
        if($user->seller == 1) {
            return ['status' => true, 'code' => 200, 'message' => ""];            
        }
        return ['status' => false, 'code' => 200, 'message' => "Sorry, you are not a seller."];
    }
    
    /**
     * This function check if Series / Episode purchsed by the user or not
     */
    public function check_if_item_purchase_by_user_for_gift($content_user_id, $current_user, $contentId, $type, $parent_id = null)
    {
        //$status = "yes";
        //return $status;
        $status = "no";
        if($type == "series") {
            $purchased_status = ShowPurchase::where('show_id', $contentId)->where('user_id', $current_user)->count();
            if($purchased_status) {
                $status = "yes";
            }
        } else if($type == "episode") {
            $purchased_show_status = ShowPurchase::where('show_id', $parent_id)->where('user_id', $current_user)->count();
            if ($purchased_show_status == 0) {
                $purchased_status = EpisodePurchase::where('episode_id', $contentId)->where('user_id', $current_user)->count();
                if($purchased_status) {
                    $status = "yes";
                }
            }
            if($purchased_show_status) {
                $status = "yes";
            }
        }
        return $status;
    }

    /**
     * This function is use for 
     * verify paytabs transaction
     */
    public function verifyPaytabsTransaction($transaction_id, $order_id)
    {
        $params = [
            "merchant_email" => config('config.paytab.PAYTAB_EMAIL'),
            "secret_key" => config('config.paytab.PAYTAB_SECRET_KEY'),
            "transaction_id" => $transaction_id,
            "order_id" => $order_id,
        ];

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.paytabs.com/apiv2/verify_payment_transaction",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $params,
            //CURLOPT_HTTPHEADER => $requested_headers
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, 1);
    }

    /**
     * This function is check if current user is verified or not
     */
    public static function isUserVerified($user)
    {
        if ($user->user_type == 0) {
            return ["status" => false, "message" => "Guest user can not proceed this."];
        }
        if ($user->verified != "Verified") {
            return ["status" => false, "message" => "Your account is not verified. Please verify your account."];
        }
        if ($user->admin_status != "Approved") {
            return ["status" => false, "message" => "Your account is disabled by the admin. Please contact support team."];
        }
        return ["status" => true, "message" => ""];
    }

    /**
     * This function is use for
     * mixpanel tracking
     * 
     */
    public function trackMixpanelEvent(\App\Models\Order $order)
    {
        try {
            $developerId = "";
            if (is_null($order->giftBy)) {
                $developerId = !is_null($order->user->developer_id) ? $order->user->developer_id : "";
            } else {
                $developerId = !is_null($order->sender->developer_id) ? $order->sender->developer_id : "";
            }

            //Check if purchase data is episode
            if ($order->product_type == 2 || $order->product_type == 3) {
                $episode_id = $order->episode->id;
                $episode_name = $order->episode->title;
                $episode_category = implode(", ", $order->episode->categories->pluck('title')->all());
                $duration = $order->episode->getDurationText();
                $rating = !is_null($order->episode->episode_extra_info->rating) ? $order->episode->episode_extra_info->rating : "";
                $language = !is_null($order->episode->episode_extra_info->language) ? $order->episode->episode_extra_info->language : "";
                if ($language) {
                    $language = isset(config('config.languages')[$language]) ? config('config.languages')[$language] : "";
                }
                $description = !is_null($order->episode->description) ? $order->episode->description : "";
                $show_name = $order->episode->show->title;
                $currency = $order->currency;
                $purchased_value = $order->invoice_total;

                $mp = \Mixpanel::getInstance(config('config.mixpanel_token'));

                if (!empty($developerId)) {
                    $mp->identify($developerId);
                }

                // track an event
                $array = [
                    "Episode Name" => $episode_name,
                    "Episode Category" => $episode_category,
                    "Duration" => $duration,
                    "Rating" => $rating,
                    "Episode ID" => $episode_id,
                    "Language" => $language,
                    "Description" => $description,
                    "Show Name" => $show_name,
                    "Currency" => $currency,
                    "Purchased Value" => $purchased_value
                ];
                $mp->track("Purchase Episode", $array);
            }
        } catch (\Exception $ex) {
            return true;
        }

        return true;
    }

    /**
     * This function is use for 
     * verify paytab transaction
     */
    public function verifyPaytabsTransactionV2($transaction_id, $order_id, $currency)
    {
        $secret_email = ($currency == "USD") ? config('config.paytab.PAYTAB_USD_EMAIL') : config('config.paytab.PAYTAB_EMAIL');
        $secret_key = ($currency == "USD") ? config('config.paytab.PAYTAB_USD_SECRET_KEY') : config('config.paytab.PAYTAB_SECRET_KEY');

        $params = [
            "merchant_email" => $secret_email,
            "secret_key" => $secret_key,
            "transaction_id" => $transaction_id,
            "order_id" => $order_id,
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.paytabs.com/apiv2/verify_payment_transaction",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $params,
            //CURLOPT_HTTPHEADER => $requested_headers
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, 1);
    }
}
