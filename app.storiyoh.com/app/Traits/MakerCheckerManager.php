<?php
namespace App\Traits;


use App\Models\Admin\Module;
use Illuminate\Support\Facades\Auth;

trait MakerCheckerManager
{
    /**
     * Manage the Maker Checker functionality.
     */
    protected function manageMakerChecker() {
        static::creating(function($model) {
            $admin_status = $model->getMakerChekerStatus('Created');

            if ($admin_status['status']) {
                $model->admin_status = $admin_status['action'];
            }
        });

        static::updating(function($model) {
            $admin_status = $model->getMakerChekerStatus('Updated');

            if ($admin_status['status']) {
                $model->admin_status = $admin_status['action'];
            }
        });
    }

    /**
     * Get the maker checker status.
     *
     * @return string
     */
    public function getMakerChekerStatus($action) {
        $model_array = explode("\\", __CLASS__);
        $module = Module::whereModel(end($model_array))->first(['maker_checker']);

        if ($module) {
            /**
             * If maker checker is OFF then by default everything will be APPROVED.
             */
            if ($module->maker_checker == 'Off') {
                return ['status' => true, 'action' => 'Approved'];
            }
        } else {
            /**
             * If module not found then APPROVED.
             */
            return ['status' => true, 'action' => 'Approved'];
        }

        $event = $this->getLogStampType($action);
        /**
         * During Reorder and Login
         */
        if (in_array($event, ['Reordered', 'Login', 'Approved', 'Unapproved'])) {
            return ['status' => false];
        }

        if (Auth::guard($this->getUserGuardForMakerChecker())->user()->isSuperAdmin()) {
            if (in_array($event, ['Deactivated', 'Draft'])) {
                return ['status' => true, 'action' => 'Pending'];
            }
            return ['status' => true, 'action' => 'Approved'];
        }else{
            if ($event == 'Published') {
                return ['status' => false];
            }
            return ['status' => true, 'action' => 'Pending'];
        }
    }

    /**
     * Gets the Event Type.
     *
     * @param $event
     * @return mixed
     */
    protected function getLogStampType($event) {
        return property_exists($this, 'logType') ? (empty($this->logType) ? $event : $this->logType) : $event;
    }

    /**
     * User guard for maker checker.
     *
     * @return null
     */
    private function getUserGuardForMakerChecker()
    {
        return property_exists($this, 'userGuard') ? $this->userGuard : null;
    }
}