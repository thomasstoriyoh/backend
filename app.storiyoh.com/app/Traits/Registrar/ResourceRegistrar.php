<?php
namespace App\Traits\Registrar;

use Illuminate\Routing\ResourceRegistrar as ResourceRegistrarMaster;
use Illuminate\Routing\Router;
/**
 * Temporary Route Registrar class.
 *
 * @author KMA / DEV Team.
 */
class ResourceRegistrar extends ResourceRegistrarMaster
{
	/**
     * The custom actions for a resourceful controller.
     *
     * @var array
     */
    protected $resourceDefaults = [
            'index', 'ajaxIndex', 'publish', 'draft', 'approve', 'unapprove', 'delete', 'reorder', 'postReorder',
            'deleteImage', 'gallery', 'postGallery', 'caption',
            'upload', 'create', 'store', 'show', 'edit', 'update', 'destroy',
	];

	/**
	 * Route Registrar Constructor.
	 * @param \Illuminate\Routing\Router $router
	 */
	function __construct(Router $router)
	{
		parent::__construct($router);
	}

	/**
     * Add the ajaxIndex method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceAjaxIndex($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/ajax';

        $action = $this->getResourceAction($name, $controller, 'ajaxIndex', $options);

        return $this->router->any($uri, $action);
    }

	/**
     * Add the publish method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourcePublish($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/publish';

        $action = $this->getResourceAction($name, $controller, 'publish', $options);

        return $this->router->match(['PUT', 'PATCH'], $uri, $action);
    }

	/**
     * Add the draft method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceDraft($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/draft';

        $action = $this->getResourceAction($name, $controller, 'draft', $options);

        return $this->router->match(['PUT', 'PATCH'], $uri, $action);
    }

	/**
     * Add the approve method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceApprove($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/approve';

        $action = $this->getResourceAction($name, $controller, 'approve', $options);

        return $this->router->match(['PUT', 'PATCH'], $uri, $action);
    }

	/**
     * Add the unapprove method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceUnapprove($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/unapprove';

        $action = $this->getResourceAction($name, $controller, 'unapprove', $options);

        return $this->router->match(['PUT', 'PATCH'], $uri, $action);
    }
    
    /**
     * Add the delete method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceDelete($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/delete';

        $action = $this->getResourceAction($name, $controller, 'delete', $options);

        return $this->router->delete($uri, $action);
    }
    
    /**
     * Add the reorder method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceReorder($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/reorder';

        $action = $this->getResourceAction($name, $controller, 'reorder', $options);

        return $this->router->get($uri, $action);
    }
    
    /**
     * Add the put reorder method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourcePostReorder($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/reorder';

        $action = $this->getResourceAction($name, $controller, 'postReorder', $options);

        return $this->router->match(['PUT', 'PATCH'], $uri, $action);
    }
    
    /**
     * Add the upload method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceUpload($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/upload';

        $action = $this->getResourceAction($name, $controller, 'upload', $options);

        return $this->router->any($uri, $action);
    }

    /**
     * Add the upload method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceCaption($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/caption';

        $action = $this->getResourceAction($name, $controller, 'caption', $options);

        return $this->router->any($uri, $action);
    }
    
    /**
     * Add the deleteImage method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceDeleteImage($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/{'.$base.'}/delete_image';

        $action = $this->getResourceAction($name, $controller, 'deleteImage', $options);

        return $this->router->delete($uri, $action);
    }
    
    /**
     * Add the gallery method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourceGallery($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/{'.$base.'}/gallery';

        $action = $this->getResourceAction($name, $controller, 'gallery', $options);

        return $this->router->get($uri, $action);
    }
    
    /**
     * Add the postGallery method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function addResourcePostGallery($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/{'.$base.'}/gallery';

        $action = $this->getResourceAction($name, $controller, 'postGallery', $options);

        return $this->router->post($uri, $action);
    }
    
}
