<?php
namespace App\Traits;

use Illuminate\Http\Request;

/**
 * Description of Response
 *
 * @author Neetendra
 */
trait ResponseFormat {
    
    #####Added By Datta#######
    /**
     * success response method.
     */
    public function sendResponse($response, $message="")
    {
        // return response()->json([
        //     'status' => true,
        //     'mesage' => $message,
        //     'data' => $response
        // ], 200);
        $response = ['status' => true, 'mesage' => $message, 'data' => $response];
        return response()->api($response)->setStatusCode(200);
    }
    /**
     * return error response.
     */
    public function sendValidationError($error = [], $message = "The given data was invalid")
    {
        // $response = [
        //     'status' => false,
        //     'data'    => (object) array(),
        //     'message' => $message,
        // ];
        
        // return response()->json($response, 422);
        $response = ['status' => false, 'data' => (object) array(), 'message' => $message];
        return response()->api($response)->setStatusCode(422);
    }
    
    /**
     * success response but empty data.
     */
    public function sendEmptyResponse($message = 'No content available') {
        // $response = [
        //     'status' => false,
        //     'data'    => [],
        //     'message' => $message,
        // ];
        
        // return response()->json($response, 204);
        $response = ['status' => false, 'data' => [], 'message' => $message];
        return response()->api($response)->setStatusCode(204);
    }
    #############END############
    
    /**
    * success response method.
    */
    public function sendErrorResponse($response, $code)
    {        
        //return response()->json($response, $code);
        return response()->api($response)->setStatusCode($code);
    }

    /**
     * This function is use for authetication exception
     *
     * @param [type] $response
     * @param [type] $code
     * @return void
     */
    public function sendAuthErrorResponse($response, $code)
    {        
        //return response()->json($response, $code);
        return response()->api($response)->setStatusCode($code);
    }

    /**
     * send Exception with custome message
     * @return Array
     */
    public function sendExceptionError($code = 404, $service = null)
    {
        if (isset($service)) {
            $serviceArr = explode('/', $service);
            $serviceCount = count($serviceArr);
            $service = $serviceArr[$serviceCount - 1];
            if (is_numeric($service)) {
                $service = $serviceArr[$serviceCount - 2];
            }
        }

        $messageArr = [
            //401 => 'Token Authentication error from '.$service,
            401 => '401:1 Token authentication error.',
            403 => 'You are authenticated but do not have access to what you are trying to do',
            404 => 'The resource you are requesting does not exist',
            405 => 'The request type is not allowed',
            422 => 'The request and the format is valid, however the request was unable to process. For instance when sent data does not pass validation tests.',
            429 => 'API limit exceeded',
            500 => 'An error occured on the server which was not the consumers fault.',
            // 6000 => 'Id missing for'.$service,
            // 6000 => 'Response unavailable for'.$service,
        ];

        $response = [
            'status'  =>false,
            'message' => @$messageArr[$code],
            'code'    => $code
        ];

        //return response()->json($response, $code);
        return response()->api($response)->setStatusCode($code);
    }

    /**
     * This function is use for locale language
     */
    public function getLocale() {
        
        $request = request();

        return  ! empty($request->lang) ?  $request->lang : \App::getLocale();        
    }
}
