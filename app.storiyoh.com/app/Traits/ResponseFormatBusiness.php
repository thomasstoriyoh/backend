<?php
namespace App\Traits;
use Auth;

use Illuminate\Http\Request;

/**
 * Description of Response
 *
 * @author Neetendra
 */
trait ResponseFormatBusiness {

    /**
    * success response method.
    */
    public function sendResponse($response, $message="")
    {       
        $response = ['status' => true, 'mesage' => $message, 'data' => $response];
        return response()->business_api($response)->setStatusCode(200);
    }
    /**
     * return error response.
     */
    public function sendValidationError($error = [], $message = "The given data was invalid")
    {        
        $response = ['status' => false, 'data' => (object) array(), 'message' => $message];
        return response()->business_api($response)->setStatusCode(422);
    }

    /**
       * success response but empty data.
    */
    public function sendEmptyResponse($message = 'No content available') 
    {
        $response = ['status' => false, 'data' => [], 'message' => $message];
        return response()->business_api($response)->setStatusCode(204);
    }

    /**
    * error response method.
    */
    public function sendErrorResponse($response, $code)
    {        
        return response()->business_api($response)->setStatusCode($code);
    }

    /**
     * This function is use for authetication exception
     *
     * @param [type] $response
     * @param [type] $code
     * @return void
     */
    public function sendAuthErrorResponse($response, $code)
    {
        return response()->business_api($response)->setStatusCode($code);
    }

    /**
     * send Exception with custome message
     * @return Array
     */
    public function sendExceptionError($code = 404, $service = null)
    {
        if (isset($service)) {
            $serviceArr = explode('/', $service);
            $serviceCount = count($serviceArr);
            $service = $serviceArr[$serviceCount - 1];
            if (is_numeric($service)) {
                $service = $serviceArr[$serviceCount - 2];
            }
        }

        $messageArr = [
            401 => '401:1 Token authentication error.',
            403 => 'You are authenticated but do not have access to what you are trying to do',
            404 => 'The resource you are requesting does not exist',
            405 => 'The request type is not allowed',
            422 => 'The request and the format is valid, however the request was unable to process. For instance when sent data does not pass validation tests.',
            429 => 'API limit exceeded for '.$service,
            500 => 'An error occured on the server which was not the consumers fault.',
            // 6000 => 'Id missing for'.$service,
            // 6000 => 'Response unavailable for'.$service,
        ];

        $response = [
            'status'  =>false,
            'message' => $messageArr[$code],
            'code'    => $code
        ];
        
        return response()->business_api($response)->setStatusCode($code);        
    }

    /**
     * This function is use for locale language
     */
    public function getLocale()
    {        
        $request = request();
        return  ! empty($request->lang) ?  $request->lang : \App::getLocale();        
    }

    /**
     * Encryption Code
     */
    public static function encrypt($data)
    {
       $user = Auth::guard('api')->user();
       if (! is_null($user)) {           
           return openssl_encrypt($data, 'aes-256-cbc', $user->encryption_key, 0, config('config.business_iv'));
           $data = urlencode($data);
       }   
       return $data;
     }

     /**
      * Decryption Code
      * @return string
      */
    public static function decrypt($data)
    {
        $user = Auth::guard('api')->user();
        if(! is_null($user)) {
            return openssl_decrypt($data, 'aes-256-cbc', $user->encryption_key, 0, config('config.business_iv'));
        }
        return $data;
    }

    /**
     * 
     * @param type $n
     * @param type $precision
     * @return type
     */
    public static function shorten_count($n,$precision = 1)
    {
        if ($n < 900) 
        {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } 
        else if ($n < 900000) 
        {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } 
        else if ($n < 900000000)
        {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } 
        else if ($n < 900000000000) 
        {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } 
        else 
        {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) 
        {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }
        return $n_format . $suffix;
    }
}