<?php

namespace App\Traits;

use App\Observers\ElasticSearchObserver;

/**
* Searchable
*/
trait Searchable
{
	public static function bootSearchable()
	{
		static::observe(ElasticSearchObserver::class);
	}

	public function getSearchIndex()
    {
        return $this->getTable();
    }

    public function getSearchType()
    {
        if (property_exists($this, 'useSearchType')) {
            return $this->useSearchType;
        }

        return $this->getTable();
    }

    public function toSearchArray()
    {
        // By having a custom method that transforms the model
        // to a searchable array allows us to customize the
        // data that's going to be searchable per model.

        $customize = $this->toArray();

        if($this->getSearchType() != 'shows')
        {
            // for episode
            $customize['tags'] = json_encode($this->tags()->pluck('title')->all());  
        }
     
        $customize['categories'] = json_encode($this->categories()->pluck('title', 'id')->all());

        $customize['networks'] = !empty($this->show->network) ? json_encode($this->show->network()->pluck('title')->all()) : "";
        
        return $customize;
      
        // return $this->toArray();
    }
}