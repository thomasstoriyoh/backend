<?php

namespace App\Traits;

use App\Models\Admin\Module;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Log;

/**
 * Description of UserActionLogManager
 *
 * @author bcm
 */
trait UserActionLogManager {
    /**
     * Default Log Type to use.
     *
     * @var null
     */
    protected $logType = null;

    /**
     * Default Log Class.
     * @var Log
     */
    protected $logModel = Log::class;

    /**
     * Action Log Listner.
     */
    protected function manageActionLog() {
        if ($this->loggable() && $this->getLogModel() && $this->getLogField()) {
            static::created(function($model) {
                $this->saveLog($model, $model->getLogType('Created'));
                return false;
            });

            static::updated(function($model) {
                $this->saveLog($model, $model->getLogType('Updated'));
                return false;
            });

            static::deleted(function($model) {
                $this->saveLog($model, $model->getLogType('Deleted'));
                return false;
            });
        }
    }

    /**
     * Save the Log in the Database.
     *
     * @param $model
     * @param $logType
     */
    protected function saveLog($model, $logType) {
        if ($logType != 'Login' && $this->validateModel($model)) {
            $log = new $model->logModel;
            $log->title = $model->{$model->getLogField()};
            $log->module_id = $model->getModuleId();
            $log->item_id = $model->id;
            $log->type = @$logType;
            $log->user_id = @Auth::guard($model->getLogGuard())->user()->id;
            $log->save();
        }
    }

    /**
     * @param $model
     * @return bool
     */
    protected function validateModel($model)
    {
        $attr = [
            'module_id' => $model->getModuleId(),
            'item_id' => $model->id,
            'user_id' => @Auth::guard($model->getLogGuard())->user()->id
        ];

        $validator = \Validator::make($attr, [
            'module_id' => 'required',
            'item_id' => 'required',
            'user_id' => 'required'
        ]);

        return $validator->passes();
    }

    protected function getModuleId() {
        $class_array = explode("\\", __CLASS__);
        $class = Module::whereModel(end($class_array))->first(['id']);
        return !empty($class) ? $class->id : null;
    }

    /**
     * Gets the LogType.
     *
     * @param $event
     * @return mixed
     */
    protected function getLogType($event) {
        return property_exists($this, 'logType') ? (empty($this->logType) ? $event : $this->logType) : $event;
    }

    /**
     * Detects if this class is loggable.
     *
     * @return bool
     */
    protected function loggable() {
        return property_exists($this, 'actionLog') ? $this->actionLog : false;
    }

    /**
     * Name of the Log Model Class.
     *
     * @return bool
     */
    protected function getLogModel() {
        if(property_exists($this, 'logModel')) {
            return is_subclass_of($this->logModel, Model::class) ? $this->logModel : false;
        }
        return false;
    }

    /**
     * Log Title Field
     * @return null
     */
    protected function getLogField() {
        return property_exists($this, 'logField') ? $this->logField : null;
    }


    /**
     * Get the Guard.
     *
     * @return null
     */
    protected function getLogGuard() {
        return property_exists($this, 'userGuard') ? $this->userGuard : null;
    }

    /**
     * Set the Log Type.
     *
     * @param $event
     */
    public function setEventType($event) {
        $this->logType = $event;
    }
}
