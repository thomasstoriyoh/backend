<?php
namespace App\Traits;

use App\Models\Admin\Module;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

/**
 * Description of UserStampManager
 *
 * @author bcm
 */
trait UserStampManager {

    /**
     * The fields to be treated as userstamps.
     *
     * @var array
     */
    protected $users = [];

    /**
     * Created By Field Name.
     */
    protected static $CREATED_BY = 'created_by';

    /**
     * Updated By Field Name.
     */
    protected static $UPDATED_BY = 'updated_by';

    /**
     * Userstamp Manager Listner.
     */
    protected function manageUserstamp() {
        if($this->userStampable()) {
            static::creating(function($model) {
                $model->created_by = @Auth::guard($model->getUserGuard())->user()->id;
                $model->updated_by = @Auth::guard($model->getUserGuard())->user()->id;
            });

            static::updating(function($model) {
                if ($model->getLogTypeForStamp('Updated') != 'Login') {
                    $model->updated_by = @Auth::guard($model->getUserGuard())->user()->id;
                }
            });
        }
    }

    /**
     * Returns userstamps as Users.
     *
     * @param $value
     * @return mixed
     */
    public function asUser($value) {
        if ($value instanceof Authenticatable) {
            return $value;
        }

        return $this->userFromId($value);
    }

    /**
     * Returns User from ID.
     *
     * @param $id
     * @return mixed
     */
    protected function userFromId($id) {
        $user = config("auth.providers." . config("auth.guards." . $this->getUserGuard() . ".provider") . ".model");
        return $user::find($id) ? $user::find($id) : false;
    }

    /**
     * Created By Relationship.
     *
     * @return mixed
     */
    public function creator() {
        return $this->belongsTo('App\Admin', self::$CREATED_BY);
    }

    /**
     * Updated By Relationship.
     *
     * @return mixed
     */
    public function updator() {
        return $this->belongsTo('App\Admin', self::$UPDATED_BY);
    }

    /**
     * Get the User Fields.
     *
     * @return array
     */
    protected function getUsers() {
        $defaults = [self::$CREATED_BY, self::$UPDATED_BY];
        return $this->userStampable() ? array_merge($this->users, $defaults) : $this->users;
    }

    /**
     * Get the Guard.
     *
     * @return null
     */
    protected function getUserGuard() {
        return property_exists($this, 'userGuard') ? $this->userGuard : null;
    }

    /**
     * @return bool
     */
    protected function userStampable() {
        return property_exists($this, 'userstamps') ? $this->userstamps : false;
    }
    
    /**
     * Set the log stamp type.
     * 
     * @param $event
     */
    protected function setLogStampType($event) {
        $this->logType = $event;
    }

    /**
     * Gets the Event Type.
     *
     * @param $event
     * @return mixed
     */
    protected function getLogTypeForStamp($event) {
        return property_exists($this, 'logType') ? (empty($this->logType) ? $event : $this->logType) : $event;
    }
}
