<?php
namespace App\UserFeedResponse;

use Carbon\Carbon;
use App\ClassesV3\Feed;
use App\Models\PostShowComment;
use App\Models\PostEpisodeComment;
use App\Models\PostPlaylistComment;
use App\Models\PostSmartPlaylistComment;
use App\Models\PostCollectionComment;
use App\Traits\Helper;
use App\Models\PostComment;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class PostCommentFeedResponse extends UserFeedResponseable
{
    //put your code here
    public function response() {                
        $data = json_decode($this->feed->data, true);
        $postId = $data[0];        
        if ($this->feed->type == Feed::SHOW_POST_COMMENT) {
            //try {
                $content = PostShowComment::find(trim($postId, '#'));
                if($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'comment on post',
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_show_id,
                        'post' => ! empty($content->comment) ? $content->comment : "",
                        'show_post' => ! empty($content->post->post) ? $content->post->post : "",
                        'show_id' => $content->post->show_id,
                        'show_name' => trim(str_replace("\n", "", html_entity_decode($content->post->show->title))),
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->post->show->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                        'show_categories' => @implode(", ", $content->post->show->categories()->pluck('title')->all()),                
                        'show_image' => !empty($content->post->show->image) ? $content->post->show->getWSImage(200) : asset('uploads/default/show.png'),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_subscribers' => Helper::shorten_count($content->post->show->no_of_subscribers),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        "explicit" => $content->post->show->explicit,
                        'share_url' => config('config.live_url') . '/podcasts/' . $content->post->show_id . '/' . Helper::make_slug(trim(@$content->post->show->title)),
                    ];
                }
                
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::EPISODE_POST_COMMENT) {
            //try {
                $content = PostEpisodeComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'comment on post',
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_episode_id,
                        'post' => ! empty($content->comment) ? $content->comment : "",
                        'episode_post' => ! empty($content->post->post) ? $content->post->post : "",
                        'episode_id' => $content->post->episode_id,
                        'episode_name' => html_entity_decode($content->post->episode->title),
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->post->episode->description, '<br>'), ENT_QUOTES, 'UTF-8'))),                        
                        'show_id' => $content->post->episode->show->id,
                        'show_name' => trim(str_replace("\n", "", html_entity_decode($content->post->episode->show->title))),
                        'show_image' => !empty($content->post->episode->show->image) ? $content->post->episode->show->getWSImage(200) : asset('uploads/default/show.png'),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'listen_count' => Helper::shorten_count($content->post->episode->listen_count),
                        "explicit" => $content->post->episode->show->explicit,

                        'episode_audio' => $content->post->episode->getAudioLink(),
                        'duration' => $content->post->episode->getDurationText(),
                        'share_url' => config('config.live_url') . '/episode/' . $content->post->episode_id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$content->post->episode->title))),
                    ];
                }
            //} catch(\Exception $ex){}
        }else if ($this->feed->type == Feed::PLAYLIST_POST_COMMENT) {
            //try{
                $content = PostPlaylistComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'comment on post',
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_playlist_id,                        
                        'post' => ! empty($content->comment) ? $content->comment : "",
                        'playlist_post' => ! empty($content->post->post) ? $content->post->post : "",
                        'board_id' => $content->post->playlist_id,
                        'board_name' => trim(str_replace("\n", "", html_entity_decode($content->post->playlist->title))),
                        'board_image' => !empty($content->post->playlist->image) ? $content->post->playlist->getImage(200) : asset('uploads/default/board.png'),
                        'board_owner_name' => $content->post->playlist->user->first_name,
                        'board_user_name' => $content->post->playlist->user->username,
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->post->playlist->description, '<br>'), ENT_QUOTES, 'UTF-8'))),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->post->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->post->comment_count),
                        'no_of_followers' => Helper::shorten_count($content->post->playlist->followers_count),                        
                        "explicit" => 0
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::SMARTPLAYLIST_POST_COMMENT) {
            //try{
                $content = PostSmartPlaylistComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'comment on post',
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_smart_playlist_id,                        
                        'post' => ! empty($content->comment) ? $content->comment : "",
                        'smart_playlist_post' => ! empty($content->post->post) ? $content->post->post : "",
                        'board_id' => $content->post->smart_playlist_id,
                        'board_name' => trim(str_replace("\n", "", html_entity_decode($content->post->smart_playlist->title))),
                        'board_image' => !empty($content->post->smart_playlist->image) ? $content->post->smart_playlist->getImage(200) : asset('uploads/default/board.png'),
                        'board_owner_name' => $content->post->smart_playlist->user->first_name,
                        'board_user_name' => $content->post->smart_playlist->user->username,
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->post->smart_playlist->description, '<br>'), ENT_QUOTES, 'UTF-8'))),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->post->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->post->comment_count),
                        'no_of_followers' => Helper::shorten_count($content->post->smart_playlist->followers_count),
                        "explicit" => 0
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::COLLECTION_POST_COMMENT) {
            //try{
                $content = PostCollectionComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'comment on post',
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_collection_id,                        
                        'post' => ! empty($content->comment) ? $content->comment : "",
                        'collection_post' => ! empty($content->post->post) ? $content->post->post : "",
                        'collection_id' => $content->post->collection_id,
                        'collection_name' => trim(str_replace("\n", "", html_entity_decode($content->post->collection->title))),
                        'collection_image' => !empty($content->post->collection->image) ? $content->post->collection->getImage(200) : asset('uploads/default/board.png'),
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->post->collection->description, '<br>'), ENT_QUOTES, 'UTF-8'))),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->post->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->post->comment_count),
                        "explicit" => 0,
                        'share_url' => config('config.live_url') . '/collections/' . $content->post->collection_id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$content->post->collection->title))),
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::INDIVIDUAL_POST_COMMENT) {
            $content = PostComment::find(trim($postId, "#"));
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'comment on post',
                        'comment_id' => trim($postId, '#'),
                        'post_id' => $content->post_id,                        
                        'post' => ! empty($content->comment) ? $content->comment : "",
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->post->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->post->comment_count),
                    ];
                }
        }               
    }
}
