<?php
namespace App\UserFeedResponse;

use Carbon\Carbon;
use App\Models\PostShow;
use App\ClassesV3\Feed;
use App\Models\PostEpisode;
use App\Models\PostPlaylist;
use App\Models\PostCollection;
use App\Models\PostSmartPlaylist;
use App\Traits\Helper;
use App\Models\Post;

/**
 * Description of ShowNewEpisodeFeedResponse
 *
 * @author bcm
 */
class PostLikeFeedResponse extends UserFeedResponseable
{
    //put your code here
    public function response() {        
        $data = json_decode($this->feed->data, true);
        $postId = $data[0];        
        if ($this->feed->type == Feed::SHOW_POST_LIKE) {
            //try {
                $content = PostShow::find(trim($postId, '#'), ['id', 'user_id', 'show_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'liked post',                        
                        'post_id' => trim($postId, '#'),
                        'post' => ! empty($content->post) ? $content->post : "",
                        'show_id' => $content->show_id,
                        'show_name' => trim(str_replace("\n", "", html_entity_decode($content->show->title))),
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->show->description, '<br>'), ENT_QUOTES, 'UTF-8'))),
                        'show_categories' => @implode(", ", $content->show->categories()->pluck('title')->all()),                
                        'show_image' => !empty($content->show->image) ? $content->show->getWSImage(200) : asset('uploads/default/show.png'),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_subscribers' => Helper::shorten_count($content->show->no_of_subscribers),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        "explicit" => $content->show->explicit,
                        'share_url' => config('config.live_url') . '/podcasts/' . $content->show_id . '/' . Helper::make_slug(trim(@$content->show->title)),
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::EPISODE_POST_LIKE) {
            //try {
                $content = PostEpisode::find(trim($postId, "#"), ['id', 'user_id', 'episode_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {                    
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'liked post',                        
                        'post_id' => trim($postId, '#'),                        
                        'post' => ! empty($content->post) ? $content->post : "",
                        'episode_id' => $content->episode_id,
                        'episode_name' => html_entity_decode($content->episode->title),
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->episode->description, '<br>'), ENT_QUOTES, 'UTF-8'))),                        
                        'show_id' => $content->episode->show->id,
                        'show_name' => trim(str_replace("\n", "", html_entity_decode($content->episode->show->title))),
                        'show_image' => !empty($content->episode->show->image) ? $content->episode->show->getWSImage(200) : asset('uploads/default/show.png'),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'listen_count' => Helper::shorten_count($content->episode->listen_count),
                        "explicit" => $content->episode->show->explicit,
                        'episode_audio' => $content->episode->getAudioLink(),
                        'duration' => $content->episode->getDurationText(),
                        'share_url' => config('config.live_url') . '/episode/' . $content->episode_id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$content->episode->title))),
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::PLAYLIST_POST_LIKE) {
            //try{
                $content = PostPlaylist::find(trim($postId, "#"), ['id', 'user_id', 'playlist_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {                    
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'liked your post',                        
                        'post_id' => trim($postId, '#'),
                        'post' => ! empty($content->post) ? $content->post : "",
                        'board_id' => $content->playlist_id,
                        'board_name' => trim(str_replace("\n", "", html_entity_decode($content->playlist->title))),
                        'board_image' => !empty($content->playlist->image) ? $content->playlist->getImage(200) : asset('uploads/default/board.png'),
                        'board_owner_name' => $content->playlist->user->first_name,
                        'board_user_name' => $content->playlist->user->username,
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->playlist->description, '<br>'), ENT_QUOTES, 'UTF-8'))),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'no_of_followers' => Helper::shorten_count($content->playlist->followers_count),                        
                        "explicit" => 0
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::SMARTPLAYLIST_POST_LIKE) {
            //try{
                $content = PostSmartPlaylist::find(trim($postId, "#"), ['id', 'user_id', 'smart_playlist_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'liked post',                        
                        'post_id' => trim($postId, '#'),                                                                        
                        'post' => ! empty($content->post) ? $content->post : "",
                        'board_id' => $content->smart_playlist_id,
                        'board_name' => trim(str_replace("\n", "", html_entity_decode($content->smart_playlist->title))),
                        'board_image' => !empty($content->smart_playlist->image) ? $content->smart_playlist->getImage(200) : asset('uploads/default/board.png'),
                        'board_owner_name' => $content->smart_playlist->user->first_name,
                        'board_user_name' => $content->smart_playlist->user->username,
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->smart_playlist->description, '<br>'), ENT_QUOTES, 'UTF-8'))),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        'no_of_followers' => Helper::shorten_count($content->smart_playlist->followers_count),
                        "explicit" => 0
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::COLLECTION_POST_LIKE) {
            //try{
                $content = PostCollection::find(trim($postId, "#"), ['id', 'user_id', 'collection_id', 'post', 'likes_count', 'comment_count']);
                if ($content) {
                    return [
                        'feed_id' => $this->feed->id,
                        'type' => $this->feed->type,
                        'name' => @$this->feed->typeable->notification_format_name,
                        'username' => @$this->feed->typeable->username,                        
                        "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                        'display_text' => 'liked post',                        
                        'post_id' => trim($postId, '#'),
                        'post' => ! empty($content->post) ? $content->post : "",
                        'collection_id' => $content->collection_id,
                        'collection_name' => trim(str_replace("\n", "", html_entity_decode($content->collection->title))),
                        'collection_image' => !empty($content->collection->image) ? $content->collection->getImage(200) : asset('uploads/default/board.png'),
                        'description' => trim(str_replace("<br>", "\n", html_entity_decode(strip_tags($content->collection->description, '<br>'), ENT_QUOTES, 'UTF-8'))),                        
                        'last_update' => $this->feed->updated_at->diffForHumans(),
                        'no_of_likes' => Helper::shorten_count($content->likes_count),
                        'no_of_comments' => Helper::shorten_count($content->comment_count),
                        "explicit" => 0,
                        'share_url' => config('config.live_url') . '/collections/' . $content->collection_id . '/' . Helper::make_slug(trim(str_replace("\n", '', @$content->collection->title))),
                    ];
                }
            //} catch(\Exception $ex){}
        } else if ($this->feed->type == Feed::INDIVIDUAL_POST_LIKE) {
            $content = Post::find(trim($postId, "#"), ['id', 'user_id', 'post', 'likes_count', 'comment_count']);
            if ($content) {
                return [
                    'feed_id' => $this->feed->id,
                    'type' => $this->feed->type,
                    'name' => @$this->feed->typeable->notification_format_name,
                    'username' => @$this->feed->typeable->username,                        
                    "user_photo" => ! empty($this->feed->typeable->image) ? $this->feed->typeable->getImage(100) : asset('uploads/default/user.png'),
                    'display_text' => 'liked post',                        
                    'post_id' => trim($postId, '#'),
                    'post' => ! empty($content->post) ? $content->post : "",
                    'last_update' => $this->feed->updated_at->diffForHumans(),
                    'no_of_likes' => Helper::shorten_count($content->likes_count),
                    'no_of_comments' => Helper::shorten_count($content->comment_count),
                ];
            }
        }
    }
}
