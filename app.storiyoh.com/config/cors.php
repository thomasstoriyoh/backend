<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Laravel CORS
    |--------------------------------------------------------------------------
    |
    | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
    | to accept any value.
    |
    */

    'supportsCredentials' => false,
    //'allowedOrigins' => ['https://insights.storiyoh.com', 'https://www2.storiyoh.com', 'https://www.storiyoh.com', 'https://wwwsandbox.storiyoh.com', 'https://business.storiyoh.com', 'https://storiyoh.com'],
    'allowedOrigins' => ['*'],
    'allowedOriginsPatterns' => [],
    'allowedHeaders' => [
        'api-key', 'content-type', 'request-encrypted', 'response-encrypted', 
        'authorization', 'cache-control', 'x-requested-with'
    ],
    //'allowedHeaders' => ['*'],
    'allowedMethods' => ['GET', 'POST', 'OPTIONS'],
    'exposedHeaders' => [],
    'maxAge' => 0,
];
