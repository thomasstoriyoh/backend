<?php
return [
    'create' => [
        'create',
        'add',
        'store',
        'upload',
    ],
    'update' => [
        'edit',
        'update',
        'modify',
        'publish',
        'draft',
        'reorder',
        'postReorder',
        'approve',
        'unapprove',
        'upload',
        'postGallery',
        'deleteImage',
        'download_sales_csv'
    ],
    'delete' => [
        'delete',
        'destroy',
    ],
    'read' => [
        'read',
        'view',
        'show',
        'index',
        'ajaxIndex',
        'gallery',
    ],
];
