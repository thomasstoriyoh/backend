<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1577059702384653',
        'client_secret' => 'd743f634d248ec02ea4d348f2c4db7b5',
        'redirect' => 'https://app.storiyoh.com/auth/facebook/callback',
    ],

    'google' => [
        'client_id' => '525680950876-5b1b0e6iaf2mqlkldu0uubc53j32plor.apps.googleusercontent.com',
        'client_secret' => 'HVvHfArKQ9rYZLG-hyj5DFGX',
        'redirect' => 'https://app.storiyoh.com/auth/google/callback',
    ],
    
    'twitter' => [
        'client_id' => 'jgDoQcA0EVnlixb1CGXLbo9Pe',
        'client_secret' => 'Kub54N4Y6cwgVH0EGB59COgHTjZqkAfSXHGa6v0r0e0QTxk3cm',
        'redirect' => 'https://app.storiyoh.com/auth/twitter/callback',
    ]

];
