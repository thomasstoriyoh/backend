<?php

return [
    'driver'          => 'google',
    'google'          => [
        'apikey' => 'AIzaSyDKlb-Ym3IHclyDyny79Ucp1iQesmpsysI',//config("config.g_plus"),
    ],
    'bitly'           => [
        'username' => env('URL_SHORTENER_BITLY_USERNAME', ''),
        'password' => env('URL_SHORTENER_BITLY_PASSWORD', ''),
    ],
    'connect_timeout' => 2,
    'timeout'         => 2,
];
