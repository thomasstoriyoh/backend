<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_episodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->integer('episode_id')->unsigned();            
            $table->foreign('episode_id')->references('id')
                ->on('episodes')->onDelete('cascade');

            $table->integer('user_id')->unsigned();            
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            
            $table->text('post')->nullable();
            $table->enum('status', ['Published', 'Draft'])->default('Published');

            $table->bigInteger('post_id')->default(0);
            
            $table->bigInteger('likes_count')->default(0);
            $table->bigInteger('comment_count')->default(0);
            $table->bigInteger('repost_count')->default(0);
            $table->bigInteger('report_count')->default(0);
            $table->bigInteger('share_external_count')->default(0);
            $table->bigInteger('share_dm_count')->default(0);            
                                                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_episodes');
    }
}
