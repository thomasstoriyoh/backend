<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostPlaylistCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_playlist_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('post_playlist_id')->unsigned();            
            $table->foreign('post_playlist_id')->references('id')
                ->on('post_playlists')->onDelete('cascade');

            $table->integer('user_id')->unsigned();            
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            
            $table->text('comment')->nullable();
            $table->enum('status', ['Published', 'Draft'])->default('Published');

            $table->bigInteger('reply_post_id')->default(0);                                    
                                                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_playlist_comments');
    }
}
