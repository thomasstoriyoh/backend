<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNotificationStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_notification_status', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            
            $table->bigInteger('user_notification_id')->unsigned();
            $table->foreign('user_notification_id')->references('id')
                ->on('user_notifications')->onDelete('cascade');

            $table->integer('typeable_id')->default(0);

            $table->string('type')->nullable()->default("");

            $table->tinyInteger('status');

            $table->timestamps();

            $table->index(['typeable_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_notification_status');
    }
}
