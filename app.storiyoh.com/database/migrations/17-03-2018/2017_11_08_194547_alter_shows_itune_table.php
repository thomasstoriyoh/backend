<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShowsItuneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shows', function (Blueprint $table) {
            $table->unsignedBigInteger('itunes_id')->after('id')->unique()->index()->nullable();
            $table->string('language')->after('title')->nullable();
            $table->longtext('artist_name')->after('feed_url')->nullable();
            $table->string('collection_name')->after('artist_name')->nullable();
            $table->string('collection_censored_name')->after('collection_name')->nullable();
            $table->string('country')->after('collection_censored_name')->nullable();
            $table->integer('no_of_episode')->after('country')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shows', function (Blueprint $table) {
            $table->dropColumn('itunes_id');
            $table->dropColumn('language');
            $table->dropColumn('artist_name');
            $table->dropColumn('collection_name');
            $table->dropColumn('collection_censored_name');
            $table->dropColumn('country');
        });
    }
}
