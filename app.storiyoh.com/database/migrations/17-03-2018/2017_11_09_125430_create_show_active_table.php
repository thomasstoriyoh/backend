<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowActiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_active', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedInteger('show_id')->unique()->index();
            $table->foreign('show_id')->references('id')
                ->on('shows')->onDelete('cascade');
            
            $table->unsignedBigInteger('itunes_id')->unique()->index();
            $table->text('feed_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('show_active');
    }
}
