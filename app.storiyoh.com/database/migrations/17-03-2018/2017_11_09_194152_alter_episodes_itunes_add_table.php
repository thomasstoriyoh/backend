<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEpisodesItunesAddTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('episodes', function (Blueprint $table) {
            $table->string('uuid')->after('id')->nullable();
            $table->unsignedBigInteger('size')->after('duration')->nullable();
            $table->text('mp3')->after('size')->nullable();
            $table->text('link')->after('mp3')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('episodes', function (Blueprint $table) {
            $table->dropColumn('uuid');
            $table->dropColumn('size');
            $table->dropColumn('mp3');
            $table->dropColumn('link');
        });
    }
}
