<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletters', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();
            $table->string('newsletter_title')->nullable();

            $table->text('description')->nullable();            
            $table->string('image')->nullable();
            $table->string('btn_text')->nullable();
            $table->string('btn_link')->nullable();

            $table->string('trending_podcasts')->nullable(); 
            $table->string('featured_collections')->nullable(); 
            $table->string('featured_episodes')->nullable(); 
            $table->string('essentials')->nullable();
            $table->string('podcast_of_the_week')->nullable(); 

            $table->unsignedInteger('order')->default(0);
            $table->enum('status', ['Published', 'Draft'])->default('Draft');
            $table->enum('admin_status', ['Approved', 'Pending'])->default('Pending');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletters');
    }
}
