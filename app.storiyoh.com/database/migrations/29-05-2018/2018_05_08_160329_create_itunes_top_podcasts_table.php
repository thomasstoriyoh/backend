<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItunesTopPodcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itunes_top_podcasts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('rss_url')->nullable();

            $table->boolean('processed')->default(false);
            
            $table->enum('status', ['Published', 'Draft'])->default('Draft');
            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itunes_top_podcasts');
    }
}
