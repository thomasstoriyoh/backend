<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title', '50');
            $table->string('slug', '60')->unique();
            $table->enum('type', ['Module', 'Report', 'Configure', 'Documentation'])->default('Module');
            $table->integer('module_id')->default(0);
            $table->boolean('nested')->default(false);
            $table->string('model', 50)->nullable();
            $table->string('controller', 50)->nullable();
            $table->string('table_name')->nullable();
            $table->integer('forward_to')->unsigned()->nullable();
            $table->integer('order')->default(0);
            $table->enum('maker_checker', ['On', 'Off'])->default('On');
            $table->enum('has_details', ['Yes', 'No'])->default('No');
            $table->boolean('has_maker_checker')->default(false);
            $table->enum('status', ['Published', 'Draft'])->default("Draft");
            $table->boolean('widget')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules');
    }
}
