<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('module_id');
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('user_id');
            $table->string('title');
            $table->enum('type', [
                'Created', 
                'Updated', 
                'Deleted', 
                'Published', 
                'Drafted',
                'Overwritten',
                'Featured',
                'Unfeatured',
                'Rearranged',
                'Approved',
                'Unapproved',
                'Default Image',
                'Image Caption',
                'Activated',
                'Deactivated',
                'Reordered'
            ])->nullable();
            $table->timestamps();

            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}
