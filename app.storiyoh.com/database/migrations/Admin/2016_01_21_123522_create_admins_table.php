<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 25);
            $table->string('last_name', 25)->nullable();
            $table->string('email', 100)->unique();
            $table->string('username', 50)->unique();
            $table->string('password', 60);
            $table->enum('type', ['SA', 'A', 'S', 'O'])->default('A');
            $table->enum('status', ["Active", "Inactive"])->default("Inactive");
            $table->enum('admin_status', ["Approved", "Pending"])->default("Pending");
            $table->timestamp('last_login')->nullable();
            $table->string('ip', 25)->nullable();
            $table->rememberToken();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admins');
    }
}
