<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleEntryViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_entry_views', function (Blueprint $table) {
            $table->date('view_date');
            $table->integer('module_id')->unsigned();
            $table->foreign('module_id')->references('id')
                ->on('modules')->onDelete('cascade');
            $table->integer('item_id')->default(0);
            $table->integer('count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_entry_views');
    }
}
