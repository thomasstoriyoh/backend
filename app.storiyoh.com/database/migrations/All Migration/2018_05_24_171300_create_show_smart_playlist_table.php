<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowSmartPlaylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_smart_playlist', function (Blueprint $table) {
            $table->integer('show_id')->unsigned();
            $table->foreign('show_id')->references('id')
                ->on('shows')->onDelete('cascade');
            
            $table->integer('smart_playlist_id')->unsigned();
            $table->foreign('smart_playlist_id')->references('id')
                ->on('smart_playlists')->onDelete('cascade');
            
            $table->integer('order')->unsigned()->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('show_smart_playlist');
    }
}
