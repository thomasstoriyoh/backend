<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyFollowedAndOwnSmartlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_followed_and_own_smartlists', function (Blueprint $table) {
            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            
            $table->integer('smart_playlist_id')->unsigned();
            $table->foreign('smart_playlist_id')->references('id')
                ->on('smart_playlists')->onDelete('cascade');

            $table->boolean('board_type');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_followed_and_own_smartlists');
    }
}
