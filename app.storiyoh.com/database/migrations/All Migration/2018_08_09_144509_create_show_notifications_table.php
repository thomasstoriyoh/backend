<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('typeable_type');
            $table->integer('typeable_id');            
            $table->string('data');
            $table->enum('type', ["", "Show Update"])->default("");
            $table->timestamps();

            $table->index(['typeable_type', 'typeable_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('show_notifications');
    }
}
