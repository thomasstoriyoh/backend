<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowActiveSingleEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_active_single_episodes', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->unsignedInteger('show_id')->unique()->index();
            $table->foreign('show_id')->references('id')
                ->on('shows')->onDelete('cascade');
            $table->unsignedBigInteger('itunes_id')->unique()->index();
            $table->text('feed_url')->nullable();
            $table->date('cron_run_at')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('show_active_single_episodes');
    }
}
