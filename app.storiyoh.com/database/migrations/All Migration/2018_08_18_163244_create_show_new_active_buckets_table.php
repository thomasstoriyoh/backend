<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowNewActiveBucketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_new_active_buckets', function (Blueprint $table) {
            
            $table->bigIncrements('id');            
            
            $table->unsignedInteger('show_id')->unique()->index();
            $table->foreign('show_id')->references('id')
                ->on('shows')->onDelete('cascade');
            
            $table->unsignedBigInteger('itunes_id')->unique()->index();
        
            $table->text('feed_url')->nullable();
        
            $table->date('cron_run_at')->nullable();
        
            $table->integer('no_of_episodes')->default(0);
        
            $table->tinyInteger('error')->default(0);
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('show_new_active_buckets');
    }
}
