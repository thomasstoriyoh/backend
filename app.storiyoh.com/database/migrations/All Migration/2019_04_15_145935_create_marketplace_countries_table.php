<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketplaceCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();

            $table->integer('currency_id')->default(0);
            // $table->foreign('currency_id')->references('id')->on('marketplace_currencies')->onDelete('cascade');

            $table->unsignedInteger('order')->default(0);
            $table->enum('status', ['Published', 'Draft'])->default('Draft');
            $table->enum('admin_status', ['Approved', 'Pending'])->default('Pending');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_countries');
    }
}
