<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIp2locationDb3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip2location_db3', function (Blueprint $table) {
            $table->integer('ip_from')->unsigned();
            $table->integer('ip_to')->unsigned();
            $table->char('country_code', 2)->nullable();
            $table->string('country_name', 64)->nullable();
            $table->string('region_name', 128)->nullable();
            $table->string('city_name', 128)->nullable();
            $table->primary('ip_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip2location_db3');
    }
}
