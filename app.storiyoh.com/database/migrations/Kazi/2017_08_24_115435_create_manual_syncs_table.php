<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualSyncsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_syncs', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['Category', 'Network', 'Show', 'Episode'])->nullable();
            $table->date('sync_date');
            $table->unsignedInteger('sync_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual_syncs');
    }
}
