<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $query = "INSERT INTO tbl_admins ("
                . "first_name, last_name, email, "
                . "username, password, type, status, admin_status, "
                . "created_by, updated_by, created_at, updated_at"
                . ") VALUES ("
                . ":first_name, :last_name, :email, "
                . ":username, :password, :type, :status, :admin_status, "
                . ":created_by, :updated_by, :created_at, :updated_at"
                . ")";
        
        DB::insert($query, [
            'first_name' => 'BCM',
            'last_name' => 'Team',
            'email' => 'dev.team@bcat.in',
            'username' => 'admin',
            'password' => Hash::make('123456'),
            'type' => 'SA',
            'status' => 'Active',
            'admin_status' => 'Approved',
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
