<?php

use App\Models\ResponseCount;
use Illuminate\Database\Seeder;

class ResponseCountSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Show', 'Episode'];
        foreach ($types as $type) {
            ResponseCount::firstOrCreate(compact('type'));
        }
    }
}
