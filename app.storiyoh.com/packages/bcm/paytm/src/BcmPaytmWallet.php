<?php

namespace Bcm\Paytm;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
require __DIR__.'/../lib/encdec_paytm.php';

class BcmPaytmWallet {

    protected $request;
    protected $response;
    protected $txn_url;
    protected $txn_status_url;

    protected $merchant_key;
	protected $merchant_id;
	protected $merchant_website;
	protected $industry_type;
    protected $channel;
	protected $domain;
	
    public function __construct(Request $request) {
        $this->request = $request;
        // if (env('APP_ENV','local') == 'production') {
		// 	$this->domain = 'securegw.paytm.in';
		// }else{
		// 	$this->domain = 'securegw-stage.paytm.in';
		// }
        $this->domain = config('config.paytm.PAYTM_DOMAIN');
        $this->paytm_txn_url = 'https://'.$this->domain.'/theia/processTransaction';
		$this->paytm_txn_status_url = 'https://'.$this->domain.'/merchant-status/getTxnStatus';

		$this->merchant_key = config('config.paytm.PAYTM_MERCHANT_KEY');//'As&zqed7At8mok7u';
		$this->merchant_id = config('config.paytm.PAYTM_MERCHANT_ID');//'UEWoqo70908506490459';
		$this->merchant_website  = config('config.paytm.PAYTM_MERCHANT_WEBSITE'); //'APPSTAGING';
		$this->industry_type = 'Retail92';
		$this->channel = 'WAP';
		
		
    }

    public function generateChecksum(){
		if($this->request->CALLBACK_URL == 'N' ) {
			$this->request['CALLBACK_URL'] = 'https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID='.$this->request->ORDER_ID;
		}
		$this->request['MID']  = $this->merchant_id;
		$this->request['WEBSITE']  = $this->merchant_website;
		$this->request['INDUSTRY_TYPE_ID']  = $this->industry_type;
		$this->request['CHANNEL_ID']  = $this->channel;
		$this->request['PAYMENT_MODE_ONLY'] = 'YES';
		$this->request['PAYMENT_TYPE_ID']  = 'NB';

		$checksum = getChecksumFromArray((array)$this->request->all(), $this->merchant_key);
		return [ 
			'MID' => $this->merchant_id,
			'CHECKSUMHASH' => $checksum, 
			'ORDER_ID' => $this->request->get('ORDER_ID'),
			'CUST_ID' => $this->request->get('CUST_ID'),
			'TXN_AMOUNT' => $this->request->get('TXN_AMOUNT'),
			'CALLBACK_URL' => $this->request->get('CALLBACK_URL'),
			'INDUSTRY_TYPE_ID' => $this->request->get('INDUSTRY_TYPE_ID'),
			'WEBSITE' => $this->request->get('WEBSITE'),
			'CHANNEL_ID' => $this->request->get('CHANNEL_ID'),
			'PAYMENT_TYPE_ID' => $this->request->get('PAYMENT_TYPE_ID'),
			'PAYMENT_MODE_ONLY' => $this->request->get('PAYMENT_MODE_ONLY') 
		];
	}

	public function verifyChecksum() {
		
		$paramList = array();
		$paramList["CURRENCY"] = $this->request->get('CURRENCY');
		$paramList["GATEWAYNAME"] = $this->request->get('GATEWAYNAME');
		$paramList["RESPMSG"] = $this->request->get('RESPMSG');
		$paramList["BANKNAME"] = $this->request->get('BANKNAME');
		$paramList["PAYMENTMODE"] = $this->request->get('PAYMENTMODE');
		$paramList["MID"] = $this->request->get('MID');
		$paramList["RESPCODE"] = $this->request->get('RESPCODE');
		$paramList["TXNID"] = $this->request->get('TXNID');
		$paramList["TXNAMOUNT"] = $this->request->get('TXNAMOUNT');
		$paramList["ORDERID"] = $this->request->get('ORDERID');
		$paramList["STATUS"] = $this->request->get('STATUS');
		$paramList["BANKTXNID"] = $this->request->get('BANKTXNID');
		$paramList["TXNDATE"] = $this->request->get('TXNDATE');
		$CHECKSUMHASH = $this->request->get('CHECKSUMHASH');

		if(verifychecksum_e($paramList, $this->merchant_key, $CHECKSUMHASH) == "TRUE"){
		    return $this->response = $this->request->all();
		}

        throw new \Exception('Invalid checksum');
	}

}
?>