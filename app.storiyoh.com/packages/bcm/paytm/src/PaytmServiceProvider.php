<?php

namespace Bcm\Paytm;

use Illuminate\Support\ServiceProvider;

use Carbon\Carbon;

class PaytmServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Bcm\Paytm\BcmPaytmWallet', function ($app) {
                return new BcmPaytmWallet($app);
        });
    }

}
