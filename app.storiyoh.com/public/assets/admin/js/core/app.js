$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': module_actions.csrf_token,
    }
});

var module = {
    publish: function(ids) {
        return this.ajax(module_actions.publish, {ids: ids}, 'PUT');
    },

    draft: function(ids) {
        return this.ajax(module_actions.draft, {ids: ids}, 'PUT');
    },
    
    approve: function(ids) {
        return this.ajax(module_actions.approve, {ids: ids}, 'PUT');
    },

    unapprove: function(ids) {
        return this.ajax(module_actions.unapprove, {ids: ids}, 'PUT');
    },

    delete: function(ids) {
        return this.ajax(module_actions.delete, {ids: ids}, 'DELETE');
    },

    ajax: function(url, data, type) {
        return $.ajax({
            url : url,
            data: data,
            type: type,
            dataType: 'json',
        }).fail(function(data) {
            console.log(data.responseText);
        });
    },

    notify: function(message, type) {
        if (typeof type == undefined) {
            type = 'alert';
        }
        noty({
            text: message,
            type: type,
            dismissQueue: true,
            theme: 'agileUI',
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            layout: 'top',
            timeout: 2000,
            closeWith: ['click', 'backdrop'],
        });
    },
    
    confirm: function(message, callback) {
        swal({
            title: "Please Confirm",
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
        }, function(isConfirm) {
            callback(isConfirm);
        });
    },
    
    counter: function(target, limit, type) {
        if (type === undefined) type = 'char';
        target.parent().parent().find('.fm-label').append('<label class="counter-result float-right"></label>');
        target.counter({
            goal: limit,
            type: type,
            count: 'up',
            append: false,
            target: target.parent().parent().find('label.counter-result'),
            msg: type == 'char' ? '/ '+limit +' )' : '/ '+limit+' )',
        });
    },

    getSelectedIds: function(table) {

        var ids = [];
        for(i = 0; i < table.rows('.tr-selected').data().length; i++) {
            ids.push(table.rows('.tr-selected').data()[i].id);
        }

        return ids;
    },
    
    formSubmit: function (clear) {
        if (clear === undefined) {
            $("a.plupload_start").click();
            return false;
        }
        
        return true;
    },
    
    deleteImage: function(data) {
        if (data === undefined) {
            data = 'Cover';
        }
        return this.ajax(module_actions.delete_image, data, 'DELETE');
    },

    loadImageCropFunction: function (crop_size, uploader) {
        var crop_options = {
            boxWidth: $(window).width()*6.8/10,
            aspectRatio: crop_size[0]/crop_size[1],
            minSize: crop_size,
            onSelect: setChords
        };

        var jcrop_api;
        var $current_image = '';
        var chords;
        var $modal_div = $("#modal-crop-image");
        var existing_img_id;

        $(document).on('click', 'a.crop-image', function(event) {
            var $img = $(this).parent().parent().parent().parent().parent().find('img');
            var src = $img.attr('src');
            existing_img_id = $img.attr('existing-id');

            $modal_div.find('img').attr('src', src);
            $modal_div.modal('show');
            $current_image = $img.attr('target-id');

            $modal_div.find('img').Jcrop(crop_options, function () {
                jcrop_api = this;
                if (crop_options.minSize) {
                    jcrop_api.setOptions();
                }
            });

            event.stopPropagation();
        });

        $modal_div.on('hidden.bs.modal', function(e){
            jcrop_api.destroy();
            $("#modal-crop-image").find('img').attr('src', '');//.attr('style', '');
            e.stopPropagation();
        });

        function setChords(c) {
            chords = $.extend(true, {}, c, {width: crop_size[0], height: crop_size[1]});
        }

        $(document).on("click", "button.set-crop", function () {
            var new_options = {};
            new_options[$current_image] = chords;

            uploader.setOption('multipart_params', $.extend(true, {}, uploader.getOption('multipart_params'), new_options));
            $("#modal-crop-image").modal('hide');

            if(typeof item_exists !== undefined) {
                if (typeof multiple_items === undefined) {
                    if (! item_exists) {
                        $input = $('<input>').attr('type', 'hidden').attr('name', 'edit_chords').val(JSON.stringify(chords));
                        $(".crop-panel").find('div.example-box-wrapper').append($input);
                    }
                } else {
                    console.log(existing_img_id);
                    var many_chords = $.extend(true, {}, chords, {
                        item_id : existing_img_id
                    });
                    $input = $('<input>').attr('type', 'hidden').attr('name', 'edit_chords[]').val(JSON.stringify(many_chords));
                    console.log(JSON.stringify(many_chords));
                    $(".crop-panel").find('div.example-box-wrapper').append($input);
                }
            }

        });

        $(document).on("click", "a.remove-image", function () {
            var $img = $(this).parent().parent().parent().parent().parent().find('img');
            uploader.removeFile(uploader.getFile($img.attr('target-id')));
            return false;
        });

    }
};

$(function(){
    if (module.slug_format !== undefined) {
        $node = $("<span>").addClass('input-helper').addClass('slug-helper');
        $('input[name="slug"]').parent().append($node);
    }
    $(document).on('keyup', 'input[name="title"], input[name="slug"]', function() {
        var slug = $(this).val().replace(/[\s\'\"\W]+/g, '-').toLowerCase();
        var slug_helper = '';
        if (module.slug_format !== undefined) {
            if ($.trim(slug) != "") {
                slug_helper = module.slug_format.replace(':slug', slug);
            }
        }
        $('input[name="slug"]').val(slug);
        $('input[name="slug"]').parent().find('.slug-helper').html(slug_helper);
        $('input[name="slug"]').trigger('change');
    });
    $(document).on('change keyup', 'input, select, textarea, div[contenteditable]', function(){
        $(".loading-button").button('normal');
    });
});