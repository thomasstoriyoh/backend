(function ( $ ) {
    $.fn.bcmTable = function(options) {
        if (options === undefined) {
            var options = {};
        }
        
        var defaults = {
            dom: '<"datatable-header"><"datatable-scroll"t>'+
                    '<"datatable-footer"<"row"<"col-md-2"l><"col-md-3"i><"col-md-3"<"clearfix"f>><"col-md-4"p>>>',
            processing: true,
            serverSide: true,
            responsive: true,
            ajax : {
                url: module_actions.json,
                data: function(d) {
                    d.custom_filter = $("#module_filter").val();
                }
            },
            order:[
                [0, 'ASC']
            ],
            language: {
                lengthMenu: '_MENU_ per page'
            }
        };
        
        var settings = $.extend({}, defaults, options, true);
        var table = this.DataTable(settings);
        
        $('.dataTables_filter input').attr("placeholder", "Search...");
        return table;
    };
    
    
}( jQuery ));

$(document).on( 'click', '#listing-table tbody tr', function () {
    $(this).toggleClass('tr-selected');
} );