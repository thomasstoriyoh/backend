(function ( $ ) {
    $.fn.bcmUpload = function(options) {
        if (options === undefined) {
            var options = {};
        }

        var methods = {
            displayImageForCropping: function (fr, file) {
                $node = $('<div class="col-lg-3 col-md-4 col-sm-6">'+
                    '<div class="thumbnail-box-wrapper">'+
                    '<div class="thumbnail-box">'+
                    '<div class="thumb-content">'+
                    '<div class="center-vertical">'+
                    '<div class="center-content">'+
                    '<div class="thumb-btn animated zoomIn">'+
                    '<a href="javascript:void(0)" class="btn btn-md btn-round btn-info crop-image" title="Crop">' +
                    '<i class="glyph-icon icon-crop"></i></a> '+
                    '<a href="javascript:void(0)" class="btn btn-md btn-round btn-danger remove-image" title="Remove">' +
                    '<i class="glyph-icon icon-remove"></i></a>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="thumb-overlay bg-gray"></div>'+
                    '<img src="" alt="">'+
                    '</div>'+
                    '</div>'+
                    '</div>');
                $node.find('img').attr('src', fr.result).attr('target-id', file.id);
                $(".crop-panel").show().find('div.example-box-wrapper').append($node);
            },

            checkForDimensionAndCropping: function (up, files) {
                $method = this;
                $.each(files, function(i, file) {
                    var regex = /image\/(.*)/;
                    if (regex.test(file.type)) {
                        var fr = new FileReader;
                        fr.onload = function() {
                            var img = new Image;
                            img.onload = function() {
                                if (up.settings.min_dimention.height > this.height || up.settings.min_dimention.width > this.width) {
                                    module.notify('Please select image with more than '+
                                        up.settings.min_dimention.width+' x '+up.settings.min_dimention.height+
                                        ' dimension.', 'error');
                                    up.removeFile(file);
                                    img = null;
                                } else if (up.settings.enable_crop) {
                                    $method.displayImageForCropping(fr, file);
                                    img = null;
                                }
                            };
                            img.src = fr.result;
                        };
                        fr.readAsDataURL(file.getNative());
                    }
                });
            },
            removeImageThumbnail: function (file) {
                $("img[target-id='"+file.id+"']").parent().parent().parent().remove();
                $("#modal-crop-image").find('img').attr('src', '');
            }
        };
        
        var defaults = {
            runtimes: 'html5,flash,silverlight,html4',
            url: module_actions.upload,
            chunk_size: '8Mb',
            unique_names: true,
            filters: {
                max_file_size: '8Mb',
            },
            max_files: 1,
            required: false,
            enable_crop: false,
            min_dimention: null,
            multipart_params: {resource: 'File'},
            headers: {
                'X-CSRF-TOKEN': module_actions.csrf_token
            },
            init: {
                Init: function (options) {
                    $("a.plupload_start").hide();
                },

                FilesAdded: function(up, files) {

                    var maxfiles = up.settings.max_files;
                    if(up.files.length > maxfiles ) {
                        up.splice(maxfiles);
                        module.notify('You can only select ' + maxfiles + ' file(s)', 'error');
                    }

                    if (up.files.length === maxfiles) {
                        $(up.settings.browse_button).hide();
                    }
                    
                    if (up.settings.min_dimention !== null) {
                        methods.checkForDimensionAndCropping(up, files);
                    }
                },

                FilesRemoved: function(up, files) {
                    var maxfiles = up.settings.max_files;
                    if(up.files.length < maxfiles ) {
                        $(up.settings.browse_button).show();

                        if (up.settings.enable_crop) {
                            $("div.crop-panel").hide();
                        }
                    }
                    if (up.settings.enable_crop) {
                        $.each(files, function (i, file) {
                            methods.removeImageThumbnail(file);
                        });
                    }
                },

                FileUploaded: function(up, file, response) {
                    var data  = $.parseJSON(response.response)[0];

                    if (data.status !== undefined && data.status) {
                        var input_name = up.settings.input_name == undefined ? 'images[]' : up.settings.input_name;
                        $node = $('<input>').attr('name', input_name).attr('type', 'hidden').val(data.filename);
                        $node.appendTo($("div.file-uploader-helper"));
                    }else{
                        console.log(data);
                    }
                },

                Error: function(up, error) {
                    module.notify(error.message, 'error');
                }
            }
        };
        
        var settings = $.extend(true, {}, defaults, options);
        this.pluploadQueue(settings);
        return this.pluploadQueue();
        
    };
}( jQuery ));

