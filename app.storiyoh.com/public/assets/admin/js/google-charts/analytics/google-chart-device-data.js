//****************************************** OS Donut Chart Code Start **********************
google.charts.setOnLoadCallback(drawOSDonutChart);
function drawOSDonutChart() {
	var data = google.visualization.arrayToDataTable(osviewdata);
    
    var options = {
        pieHole: 0.4,
        legend: {position: 'bottom'},
        title: '',
        colors: ['#5F9EA0','#D2691E','#6495ED','#008B8B','#B8860B', '#50B432','#058DC7','#8A2BE2','#9932CC','#9400D3','#00BFFF','#008000'],
        pieSliceBorderColor: 'transparent',
        chartArea: {
            left: 14,
            top: 14,
            width: "90%",
            height: "90%"
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('osDonutChart'));
    chart.draw(data, options);
}
//****************************************** OS Donut Chart Code Start **********************

//****************************************** OS Version Donut Chart Code Start **********************
google.charts.setOnLoadCallback(drawOSVersionDonutChart);
function drawOSVersionDonutChart() {
	var data = google.visualization.arrayToDataTable(osversionviewdata);
    
    var options = {
        pieHole: 0.4,
        legend: {position: 'bottom'},
        title:'',
        colors: ['#5F9EA0','#D2691E','#6495ED','#008B8B','#B8860B', '#50B432','#058DC7','#8A2BE2','#9932CC','#9400D3','#00BFFF','#008000'],
        pieSliceBorderColor: 'transparent',
        chartArea: {
            left: 14,
            top: 14,
            width: "90%",
            height: "90%"
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('osVersionDonutChart'));
    chart.draw(data, options);
}
//****************************************** OS Version Donut Chart Code End **********************

//****************************************** Brand Donut Chart Code Start **********************
google.charts.setOnLoadCallback(drawBrandDonutChart);
function drawBrandDonutChart() {
	var data = google.visualization.arrayToDataTable(brandviewdata);
    
    var options = {
        pieHole: 0.4,
        legend: {position: 'bottom'},
        title:'',
        colors: ['#5F9EA0','#D2691E','#6495ED','#008B8B','#B8860B', '#50B432','#058DC7','#8A2BE2','#9932CC','#9400D3','#00BFFF','#008000'],
        pieSliceBorderColor: 'transparent',
        chartArea: {
            left: 14,
            top: 14,
            width: "90%",
            height: "90%"
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('brandDonutChart'));
    chart.draw(data, options);
}
//****************************************** Brand Donut Chart Code End **********************

//****************************************** Model Donut Chart Code Start **********************
google.charts.setOnLoadCallback(drawModelDonutChart);
function drawModelDonutChart() {
	var data = google.visualization.arrayToDataTable(modelviewdata);
    
    var options = {
        pieHole: 0.4,
        legend: {position: 'bottom'},
        title:'',
        colors: ['#50B432','#058DC7','#8A2BE2','#5F9EA0','#D2691E','#6495ED','#008B8B','#B8860B', '#9932CC','#9400D3','#00BFFF','#008000'],
        pieSliceBorderColor: 'transparent',
        chartArea: {
            left: 14,
            top: 14,
            width: "90%",
            height: "90%"
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('modelDonutChart'));
    chart.draw(data, options);
}
//****************************************** Model Donut Chart Code End **********************

$(window).resize(function() {
    drawOSDonutChart();
    drawOSVersionDonutChart();
    drawBrandDonutChart();
    drawModelDonutChart();
});