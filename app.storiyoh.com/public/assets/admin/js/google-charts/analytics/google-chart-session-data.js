//****************************************** New Session Code Start **********************
google.charts.setOnLoadCallback(drawNewSessionsChart);

function drawNewSessionsChart() {
    var data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Date');
	data.addColumn('number', 'New Sessions (%)');
	
	data.addRows(percentnewsessionsdata);
    var options = {
        backgroundColor: '#fff',
        chartArea: {
            backgroundColor: '#fff'
        },
        legend: {
            position: 'none'
        }
    };
    
    var chart = new google.charts.Line(document.getElementById('newSessionsChart'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}
//****************************************** New Session Code End **********************

//****************************************** Avg Session Duration Code Start **********************
google.charts.setOnLoadCallback(drawAvgSessionDurationChart);

function drawAvgSessionDurationChart() {
    var data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Date');
	data.addColumn('number', 'Avg. Duration (Sec.)');

    data.addRows(avgsessiondurationdata);
    
    var options = {
        backgroundColor: '#fff',
        chartArea: {
            backgroundColor: '#fff'
        },
        legend: {
            position: 'none'
        }
    };
    
    var chart = new google.charts.Line(document.getElementById('avgSessionDurationChart'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}
//****************************************** Avg Session Duration Code End **********************

//****************************************** Bounces Code Start **********************
google.charts.setOnLoadCallback(drawBouncesChart);

function drawBouncesChart() {
    var data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Date');
	data.addColumn('number', 'Bounces');

    data.addRows(bouncesdata);
    
    var options = {
        backgroundColor: '#fff',
        chartArea: {
            backgroundColor: '#fff'
        },
        legend: {
            position: 'none'
        }
    };
    
    var chart = new google.charts.Line(document.getElementById('bouncesChart'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}
//****************************************** Bounces Code End **********************

//****************************************** Bounce Rate Code Start **********************
google.charts.setOnLoadCallback(drawBounceRateChart);

function drawBounceRateChart() {
    var data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Date');
	data.addColumn('number', 'Bounce Rate (%)');

    data.addRows(bounceratedata);
    
    var options = {
        backgroundColor: '#fff',
        chartArea: {
            backgroundColor: '#fff'
        },
        legend: {
            position: 'none'
        }
    };
    
    var chart = new google.charts.Line(document.getElementById('bounceRateChart'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}
//****************************************** Bounce Rate Code End **********************

$(window).resize(function() {
    drawNewSessionsChart();
    drawAvgSessionDurationChart();
    drawBouncesChart();
    drawBounceRateChart();
});