//******************************************Draw Total Page views Chart Code Start **********************
google.charts.setOnLoadCallback(drawTotalPageviewsChart);

function drawTotalPageviewsChart() {
    var data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Date');
	data.addColumn('number', 'Page Views');

    data.addRows(pageviewsdata);
    
     var options = {
        backgroundColor: '#fff',
        chartArea: {
            backgroundColor: '#fff'
        },
        legend: {
            position: 'none'
        }
    };
    
    var chart = new google.charts.Line(document.getElementById('totalPageviewsChart'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}
//******************************************Draw Total Page views Chart Code End **********************

//******************************************Draw active User Chart Code Start **********************
google.charts.setOnLoadCallback(drawactiveUserChart);

function drawactiveUserChart() {
    var data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Date');
	data.addColumn('number', 'Active Users');

    data.addRows(userdata);
    
     var options = {
        backgroundColor: '#fff',
        chartArea: {
            backgroundColor: '#fff'
        },
        legend: {
            position: 'none'
        }
    };
    
    var chart = new google.charts.Line(document.getElementById('activeUserChart'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}
//******************************************Draw active User Chart Code End **********************


//******************************************Draw New Users Chart Code Start **********************
//drawChart4
google.charts.setOnLoadCallback(drawNewUserChart);

function drawNewUserChart() {
    var data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Date');
	data.addColumn('number', 'New Users');

    data.addRows(newuserdata);
    
     var options = {
        backgroundColor: '#fff',
        chartArea: {
            backgroundColor: '#fff'
        },
        legend: {
            position: 'none'
        }
    };
    
    var chart = new google.charts.Line(document.getElementById('newUserChart'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}
//******************************************Draw New Users Chart Code End **********************

//drawChart5
google.charts.setOnLoadCallback(drawUserTypesDonutChart);

function drawUserTypesDonutChart() {
    var data = google.visualization.arrayToDataTable(newvisitordata);

    var options = {
        pieHole: 0.4,
        legend: {
            position: 'bottom'
        },
        colors: ['#50B432', '#058DC7'],
        pieSliceBorderColor: 'transparent',
        chartArea: {
            left: 14,
            top: 14,
            width: "90%",
            height: "90%"
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('userTypesDonutChart'));
    chart.draw(data, options);
}

$(window).resize(function() {
    drawTotalPageviewsChart();
    drawactiveUserChart();
    drawNewUserChart();
    drawUserTypesDonutChart();
});