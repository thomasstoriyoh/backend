google.charts.load('current', {
    'packages': ['line', 'corechart']
});

//Draw Top Chart
google.charts.setOnLoadCallback(drawTopChart);

function drawTopChart()
{
    var data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Date');
	data.addColumn('number', 'Sessions');

    data.addRows(sessionData);
   
    var options = {
        backgroundColor: '#fff',
        chartArea: {
            backgroundColor: '#fff'
        },
        legend: {
            position: 'none'
        }
    };
    var chart = new google.charts.Line(document.getElementById('drawTopChart'));
    chart.draw(data, google.charts.Line.convertOptions(options));
}

$(window).resize(function() {
    drawTopChart();   
});