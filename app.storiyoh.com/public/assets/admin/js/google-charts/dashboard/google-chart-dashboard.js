//drawChart2
// var k = 1;
// var $chart_functions = [];
// $.each(module_view_data, function (i, module_data) {
//     if(Number(module_data.length) > 0) {
//         $(function(){
//             $chart_functions[k] = function () {
//                 var data = new google.visualization.DataTable();
//                 data.addColumn('string', 'Date');
//                 data.addColumn('number', 'Views');
//                 data.addRows(module_data);
//                 var options = {
//                     backgroundColor: 'transparent',
//                     chartArea: {
//                         backgroundColor: 'transparent'
//                     },
//                     legend: {
//                         position: 'none'
//                     },
//                     colors: ["#fff"],
//                     vAxis: {
//                         baselineColor: 'transparent',
//                         gridlines: {
//                             color: "transparent"
//                         },
//                         textStyle: {
//                             color: '#FFF'
//                         }
//                     },
//                     hAxis: {
//                         textStyle: {
//                             color: '#FFF'
//                         }
//                     }
//                 };
//                 var chart = new google.charts.Line(document.getElementById('drawChart'+k));
//                 chart.draw(data, google.charts.Line.convertOptions(options));
//
//                 k++;
//             }
//
//             google.charts.setOnLoadCallback($chart_functions[k]);
//         });
//     }
// });


// google.charts.setOnLoadCallback(drawChart2);
//
// function drawChart2() {
//     var data = new google.visualization.DataTable();
//     data.addColumn('string', 'Date');
//     data.addColumn('number', 'Views');
//     data.addRows(module_data2);
//     var options = {
//         backgroundColor: 'transparent',
//         chartArea: {
//             backgroundColor: 'transparent'
//         },
//         legend: {
//             position: 'none'
//         },
//         colors: ["#fff"],
//         vAxis: {
//             baselineColor: 'transparent',
//             gridlines: {
//                 color: "transparent"
//             },
//             textStyle: {
//                 color: '#FFF'
//             }
//         },
//         hAxis: {
//             textStyle: {
//                 color: '#FFF'
//             }
//         }
//     };
//     var chart = new google.charts.Line(document.getElementById('drawChart2'));
//     chart.draw(data, google.charts.Line.convertOptions(options));
// }
//
// //drawChart3
// google.charts.setOnLoadCallback(drawChart3);
//
// function drawChart3() {
//     var data = new google.visualization.DataTable();
//     data.addColumn('number', 'Day');
//     data.addColumn('number', 'Guardians of the Galaxy');
//     data.addRows([
//         [1, 37.8],
//         [2, 30.9],
//         [3, 25.4],
//         [4, 11.7],
//         [5, 11.9],
//         [6, 8.8],
//         [7, 7.6],
//         [8, 12.3],
//         [9, 16.9],
//         [10, 12.8],
//         [11, 5.3],
//         [12, 6.6],
//         [13, 4.8],
//         [14, 4.2]
//     ]);
//     var options = {
//         backgroundColor: 'transparent',
//         chartArea: {
//             backgroundColor: 'transparent'
//         },
//         legend: {
//             position: 'none'
//         },
//         colors: ["#fff"],
//         vAxis: {
//             baselineColor: 'transparent',
//             gridlines: {
//                 color: "transparent"
//             },
//             textStyle: {
//                 color: '#FFF'
//             }
//         },
//         hAxis: {
//             textStyle: {
//                 color: '#FFF'
//             }
//         }
//     };
//     var chart = new google.charts.Line(document.getElementById('drawChart3'));
//     chart.draw(data, google.charts.Line.convertOptions(options));
// }
//
// //drawChart4
// google.charts.setOnLoadCallback(drawChart4);
//
// function drawChart4() {
//     var data = new google.visualization.DataTable();
//     data.addColumn('number', 'Day');
//     data.addColumn('number', 'Guardians of the Galaxy');
//     data.addRows([
//         [1, 37.8],
//         [2, 30.9],
//         [3, 25.4],
//         [4, 11.7],
//         [5, 11.9],
//         [6, 8.8],
//         [7, 7.6],
//         [8, 12.3],
//         [9, 16.9],
//         [10, 12.8],
//         [11, 5.3],
//         [12, 6.6],
//         [13, 4.8],
//         [14, 4.2]
//     ]);
//     var options = {
//         backgroundColor: 'transparent',
//         chartArea: {
//             backgroundColor: 'transparent'
//         },
//         legend: {
//             position: 'none'
//         },
//         colors: ["#fff"],
//         vAxis: {
//             baselineColor: 'transparent',
//             gridlines: {
//                 color: "transparent"
//             },
//             textStyle: {
//                 color: '#FFF'
//             }
//         },
//         hAxis: {
//             textStyle: {
//                 color: '#FFF'
//             }
//         }
//     };
//     var chart = new google.charts.Line(document.getElementById('drawChart4'));
//     chart.draw(data, google.charts.Line.convertOptions(options));
// }

//Dashboard Module Chart Code Start
google.charts.setOnLoadCallback(drawDashboardModuleChart);

function drawDashboardModuleChart() {
	var data = google.visualization.arrayToDataTable(dashboardModuleData);
    var options = {
        pieHole: 0.4,
        legend: {position: 'none'},
        colors: ['#3366cc','#990099','#109618','#ff9900','#dc3912'],
        pieSliceBorderColor: 'transparent',
        chartArea: {
            left: 14,
            top: 14,
            width: "90%",
            height: "90%"
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('drawChart5'));
    chart.draw(data, options);
}
// //Dashboard Module Chart Code End

$(window).resize(function() {
    drawDashboardModuleChart();
});
