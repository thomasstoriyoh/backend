@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $admin->exists ? 'Update' : 'Create' }} CMS User</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Configure</a></li>
            <li>{{ $admin->exists ? 'Update' : 'Create' }} Admin</li>
        </ul>
    </div>
    <p>Delight UI comes packed with a lot of options.</p>
</div>
{!! Form::model($admin, [
    'method' => $admin->exists ? 'PUT' : 'POST',
    'action' => $admin->exists ? 
        ['Admin\Core\AdminController@update', $admin->id] : 'Admin\Core\AdminController@store',
    'class' => 'form bordered-row',
    'autocomplete' => 'off',
    'data-parsley-validate' => "",
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Basic Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>First Name</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('first_name', null, [
                                'class' => "form-control",
                                'placeholder' => "First Name",
                                'required' => true,
                                'pattern' => '/^[a-zA-Z\s]+$/',
                                'data-parsley-required-message' => "Please enter admin first name.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Last name</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('last_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Last Name",
                                'required' => true,
                                'pattern' => '/^[a-zA-Z\s]+$/',
                                'data-parsley-required-message' => "Please enter admin last name.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Email</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::email('email', null, [
                                'class' => "form-control",
                                'placeholder' => "Email Address",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter admin email.",
                                'data-parsley-email-message' => "Please enter a valid email.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Username</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('username', null, [
                                'class' => "form-control",
                                'placeholder' => "Username",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter admin username.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Password</label>
                        </div>
                        <?php
                            $attributes = [
                                'class' => "form-control",
                                'id' => 'password',
                                'placeholder' => "Password",
                            ];
                            
                            if(! $admin->exists) {
                                $attributes = array_merge($attributes, [
                                    'required' => true,
                                    'data-parsley-required-message' => "Please enter admin Password",
                                ]);
                            }
                        ?>
                        <div class="fm-control">
                            {!! Form::password('password', $attributes) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Confirm Password</label>
                        </div>
                        <?php
                            $attributes = [
                                'class' => "form-control",
                                'placeholder' => "Confirm Password",
                            ];
                            
                            if(! $admin->exists) {
                                $attributes = array_merge($attributes, [
                                    'required' => true,
                                    'data-parsley-equalto' => '#password',
                                    'data-parsley-required-message' => "Please confirm admin Password",
                                    'data-parsley-equalto-message' => 'Confirm password should be same as Password',
                                ]);
                            }
                        ?>
                        <div class="fm-control">
                            {!! Form::password('password_confirmation', $attributes) !!}
                        </div>
                    </div>
                </div>
                {{--<div class="col-xs-12 col-sm-12 col-md-4">--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="fm-label">--}}
                            {{--<label>Role</label>--}}
                            {{--<div class="fm-tooltip">--}}
                                {{--<i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Role of the Admin"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="fm-control">--}}
                            {{--{!! Form::select('type', $role_list, null, [--}}
                                {{--'class' => "form-control chosen-select",--}}
                            {{--]) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            <div class="fm-label">
                <label>Access Control</label>
                <div class="fm-tooltip">
                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Give permissions to this user in module level."></i>
                </div>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="cl-box-wrapper">
                <div class="module-permission-box">
                    <div class="cl-box-title">
                        Modules
                    </div>
                    @foreach($modules as $module)
                        @include('admin.core.admin.permission')
                    @endforeach
                </div>

                <div class="config-permission-box">
                    <div class="cl-box-title">
                        Configurations
                    </div>
                    @foreach($configs as $module)
                        @unless($module->slug == 'language')
                            @include('admin.core.admin.permission')
                        @endunless
                    @endforeach
                </div>

                <div class="report-permission-box">
                    <div class="cl-box-title">
                        Reports
                    </div>
                    @foreach($reports as $module)
                        @include('admin.core.admin.permission')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            User Status
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Active or Inactive"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $admin->status == 'Active' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $admin->status == 'Active' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\AdminController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
$(function(){
    $(".bootstrap-switch.bootstrap-switch-medium").css('width', '200px');
    
    change_input_state();
    $("select[name='nested']").change(function() {
        change_input_state();
    });
    
    function change_input_state() {
        var value = $("select[name='nested']").val();
        if(value == '1') {
            $("input[name='table_name']").removeAttr('readonly');
            $("input[name='model']").removeAttr('readonly');
            $("input[name='controller']").removeAttr('readonly');
        }
        if(value == '') {
            $("input[name='table_name']").val('').attr('readonly', true);
            $("input[name='model']").val('').attr('readonly', true);
            $("input[name='controller']").val('').attr('readonly', true);
        }
    }
    
    $(document).on('keyup', 'input[name="title"]', function() {
        $('input[name="slug"]').val($(this).val().replace(/[\s\'\"\W]+/g, '-').toLowerCase());
    });
    
    var nested = ($.trim($("select[name='nested']").val()) != "");
    window.ParsleyValidator.addValidator('conditionalValue', function(value, requirements) {
        if(requirements[1] == $(requirements[0]).val() && value == '') {
            return false;
        }
        var regex = /^[a-zA-Z_]+$/;
        if (value != '' && ! regex.test(value)) {
            return false;
        }
        return true;
    }, 32).addMessage('en', 'conditionalValue', 'Field is required and should be Letters only.');
    
    $(document).on('change keyup', 'input, select, textarea', function(){
        $(".loading-button").button('normal');
    });

    $(document).on('change', '.permission-create, .permission-update, .permission-delete', function(){
        if(! $(this).parent().hasClass('active')) {
            $(this).parent().parent().find(".permission-read").prop('checked', true);
            $(this).parent().parent().find(".permission-read").parent().addClass('active');
        }
    });

//    changeAdminType();
//    $(document).on("change", "select[name='type']", function () {
//        changeAdminType();
//    });
//
//    function changeAdminType() {
//        var value = $("select[name='type']").val();
//        var $config_box = $(".config-permission-box");
//        var $report_box = $(".report-permission-box");
//        var $module_box = $(".module-permission-box");
//
//        if (value == 'S' || value == 'O') { // Then it is a Sales Team / Operations team.
//            $config_box.find('input[type="checkbox"]').each(function (index, el) {
//                $(el).prop('checked', false);
//                $(el).parent().removeClass('active');
//            });
//            $config_box.hide();
//
//            $report_box.find('input[type="checkbox"]').each(function (index, el) {
//                $(el).prop('checked', false);
//                $(el).parent().removeClass('active');
//            });
//            $report_box.hide();
//
//            $module_box.find('input[type="checkbox"]').each(function (index, el) {
//                var id = $(el).attr('id');
//                if (id != 'create-requested-itinerary' && id != 'read-requested-itinerary' &&
//                        id != 'update-requested-itinerary' && id != 'delete-requested-itinerary') {
//                    $(el).prop('checked', false);
//                    $(el).parent().removeClass('active');
//                    $(el).parents('.cl-box-main').hide();
//                }
//            });
//
//            // Deselect all permissions Except Itinerary Request.
//
//        } else {
//            $config_box.show();
//            $report_box.show();
//            $module_box.find('.cl-box-main').show();
//        }
//    }
});
</script>
@stop
