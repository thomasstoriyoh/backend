<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Content Management System </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/admin/css/admin.css') }}?v=1" charset="utf-8">
    <link rel="stylesheet" href="{{ url('assets/admin/css/overwrite.css') }}">
    <!-- Overwrite Css -->
    <style type="text/css">
        li.search-field {width:100% !important;}
        .select2-container {width:100% !important;}
        input.select2-search__field {width:100% !important;}

        #menuLeft{visibility:hidden;}
        .pace-done #menuLeft{visibility:visible;}
        body{display: none;}
        .pace > * {display: block;}
    </style>

    <!-- JS Core -->
    <script type="text/javascript" src="{{ url('assets/admin/js/admin-head.js') }}"></script>
    <script type="text/javascript">
        var api;
        $(function() {
            $('nav#menuLeft').mmenu({
                extensions: ['effect-slide-menu', 'pageshadow', 'theme-dark'],
                searchfield: true,
                counters: true,
                navbar: {
                    title: 'Dashboard'
                },
                navbars: [{
                    position: 'top',
                    content: ['searchfield']
                }, {
                    position: 'top',
                    content: [
                        'prev',
                        'title'
                    ]
                }]
            });
            
            api = $('nav#menuLeft').data('mmenu');
            
        });
        
        $(window).load(function() {
            $("body").fadeIn(700);
        });
    </script>

    @yield('css')
</head>
<body>
<div id="sb-site">
    <div id="page-wrapper">            
        <div id="page-content-wrapper">
            <div id="page-content">
                @include('admin.partials.header-2fa')                
                <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Two Factor Authentication</div>
                                        <div class="panel-body">
                                            @if ($errors->any())
                                                @foreach ($errors->all() as $error)
                                                    <div class="alert alert-danger">
                                                        {{ $error }}                                                
                                                    </div>
                                                    <br />
                                                @endforeach
                                            @endif
                                            <p>Two factor authentication (2FA) strengthens access security by requiring two methods (also referred to as factors) to verify your identity. Two factor authentication protects against phishing, social engineering and password brute force attacks and secures your logins from attackers exploiting weak or stolen credentials.</p>
                                            <p>&nbsp;</p>                                            
                                            <strong>Enter the pin from Google Authenticator Enable 2FA</strong><br/><br/>
                                            <form class="form-horizontal" action="{{ route('2faVerify') }}" method="POST">
                                                {{ csrf_field() }}
                                                <div class="form-group{{ $errors->has('one_time_password-code') ? ' has-error' : '' }}">
                                                    <label for="one_time_password" class="col-md-4 control-label">One Time Password</label>
                                                    <div class="col-md-6">
                                                        <input name="one_time_password" class="form-control"  type="text" required/>
                                                    </div>
                                                </div>                                                
                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-4">
                                                    <button class="btn btn-primary" type="submit">Authenticate</button>
                                                </div>
                                            </div>
                                        </form>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ url('assets/admin/js/admin.js') }}"></script>
</body>
</html>