@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>CMS Users</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Configure</a></li>
            <li>Admin</li>
        </ul>
    </div>
    <p>Delight UI comes packed with a lot of options.</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            CMS User Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.admin')
                    <a class="btn btn-default" href="{{ action('Admin\Core\AdminController@create') }}">ADD USER</a>
                @endCanI
                @CanI(['edit.admin', 'delete.admin'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('edit.admin')
                            <li><a href="javascript:void(0)" class="publish">Activate</a></li>
                            <li><a href="javascript:void(0)" class="draft">Deactivate</a></li>
                        @endCanI
                        {{-- @if(Auth::guard('admin')->user()->isSuperAdmin() && $module_details->maker_checker == 'On')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="approve">Approve</a></li>
                            <li><a href="javascript:void(0)" class="unapprove">Unapprove</a></li>
                        @endif --}}
                        @CanI('delete.admin')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="delete">Delete</a></li>
                        @endCanI
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
                <div class="rdtChosen">  
                    <select name="" multiple data-placeholder="Filter Options..." 
                            class="chosen-select form-control" id="module_filter">
                        <optgroup label="Status">
                            <option value="status.Active">Active</option>
                            <option value="status.Inactive">Inactive</option>
                        </optgroup>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="example-box-wrapper">
            <div class="table-responsive">
                <table id="listing-table"
                       class="table table-striped table-bordered responsive no-wrap"
                       cellspacing="0" width="100%"></table>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    
    var table = $('#listing-table').bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false },
            { title: "First Name", data: "first_name" },
            { title: "Last Name", data: "last_name" },
            { title: "Email", data: "email"  },
            { title: "Username", data: "username" },
            { title: "Status", data: "status" },
            { title: "Admin Status", data: "admin_status" },
            { title: "Last Active", data: "last_login" },
            @CanI('edit.admin')
            { title: "Options", data: "options", sortable: false }
            @endCanI
        ]
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.publish", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some users to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to activate these users?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.publish(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Users activated successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.draft", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some users to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to deactivate these users?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.draft(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Users deactivated successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.delete", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some users to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to delete these users?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.delete(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Users deleted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    @if(Auth::guard('admin')->user()->isSuperAdmin() && $module_details->maker_checker == 'On')
    $(document).on("click", "a.approve", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some users to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to approve these user(s)?', function(isConfirm){
            if(isConfirm){
                var ajax = module.approve(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Users approved successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.unapprove", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some users to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to unapprove these user(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.unapprove(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Users unapproved successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    @endif
});
</script>
@stop