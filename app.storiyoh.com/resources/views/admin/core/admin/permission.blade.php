<div class="cl-box-main clearfix">
    <div class="cl-module">{{ $module->title }}</div>
    <div class="cl-permission">
        <div class="btn-group" data-toggle="buttons">
            <a href="#" class="btn btn-default {{ $admin->can('create.' . $module->slug) ? 'active':'' }}">
                <input type="checkbox" name="create[]" id="create-{{ $module->slug }}" class="permission-create"
                       value="{{ $module->id }}" {{ $admin->can('create.' . $module->slug) ? 'checked':'' }}>
                Create
            </a>
            <a href="#" class="btn btn-default {{ $admin->can('read.' . $module->slug) ? 'active':'' }}">
                <input type="checkbox" name="read[]" id="read-{{ $module->slug }}" class="permission-read"
                       value="{{ $module->id }}" {{ $admin->can('read.' . $module->slug) ? 'checked':'' }}>
                Read
            </a>
            <a href="#" class="btn btn-default {{ $admin->can('update.' . $module->slug) ? 'active':'' }}">
                <input type="checkbox" name="update[]" id="update-{{ $module->slug }}" class="permission-update"
                       value="{{ $module->id }}" {{ $admin->can('update.' . $module->slug) ? 'checked':'' }}>
                Update
            </a>
            <a href="#" class="btn btn-default {{ $admin->can('delete.' . $module->slug) ? 'active':'' }}">
                <input type="checkbox" name="delete[]" id="delete-{{ $module->slug }}" class="permission-delete"
                       value="{{ $module->id }}" {{ $admin->can('delete.' . $module->slug) ? 'checked':'' }}>
                Delete
            </a>
        </div>
    </div>
</div>