@extends('admin.layouts.master')

@section('content')
    <div id="page-title">
        <h2>My Profile</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li><a href="{{ action('Admin\Core\DashboardController@index') }}">Catalyst WCMS</a></li>
                <li><a href="#menuLeft">Configure</a></li>
                <li>My Profile</li>
            </ul>
        </div>
    </div>
    {!! Form::model($admin, [
        'method' => 'PUT',
        'action' => ['Admin\Core\AdminController@postProfile', $admin->id],
        'class' => 'form bordered-row',
        'autocomplete' => 'off',
        'data-parsley-validate' => "",
    ]) !!}
    <div class="panel">
        <div class="panel-body">
            @if (!empty($errors->all()))
                <div class="row">
                    @foreach ($errors->all() as $error)
                        <div class="col-md-6">
                            <div class="alert alert-danger mrg10B">
                                <p>{{ @$error }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            <div class="example-box-wrapper">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>First Name</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::text('first_name', null, [
                                    'class' => "form-control",
                                    'placeholder' => "First Name",
                                    'required' => true,
                                    'pattern' => '/^[a-zA-Z\s]+$/',
                                    'data-parsley-required-message' => "Please enter admin first name.",
                                    'data-parsley-pattern-message' => "Can only contain Letters.",
                                ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Last name</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::text('last_name', null, [
                                    'class' => "form-control",
                                    'placeholder' => "Last Name",
                                    'required' => true,
                                    'pattern' => '/^[a-zA-Z\s]+$/',
                                    'data-parsley-required-message' => "Please enter admin last name.",
                                    'data-parsley-pattern-message' => "Can only contain Letters.",
                                ]) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Email</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::email('email', null, [
                                    'class' => "form-control",
                                    'placeholder' => "Email Address",
                                    'required' => true,
                                    'data-parsley-required-message' => "Please enter admin email.",
                                    'data-parsley-email-message' => "Please enter a valid email.",
                                ]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Username</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::text('username', null, [
                                    'class' => "form-control",
                                    'placeholder' => "Username",
                                    'required' => true,
                                    'readonly' => true,
                                    'data-parsley-required-message' => "Please enter admin username.",
                                ]) !!}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Old Password</label>
                            </div>
                            <?php
                            $attributes = [
                                    'class' => "form-control",
                                    'id' => 'old_password',
                                    'placeholder' => "Old Password",
                            ];

                            if(! $admin->exists) {
                                $attributes = array_merge($attributes, [
                                        'required' => true,
                                        'data-parsley-required-message' => "Please enter old Password.",
                                ]);
                            }
                            ?>
                            <div class="fm-control">
                                {!! Form::password('old_password', $attributes) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>New Password</label>
                            </div>
                            <?php
                            $attributes = [
                                    'class' => "form-control",
                                    'id' => 'password',
                                    'placeholder' => "Password",
                            ];

                            if(! $admin->exists) {
                                $attributes = array_merge($attributes, [
                                        'required' => true,
                                        'data-parsley-required-message' => "Please enter admin Password.",
                                ]);
                            }
                            ?>
                            <div class="fm-control">
                                {!! Form::password('password', $attributes) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Confirm Password</label>
                            </div>
                            <?php
                            $attributes = [
                                    'class' => "form-control",
                                    'placeholder' => "Confirm Password",
                            ];

                            if(! $admin->exists) {
                                $attributes = array_merge($attributes, [
                                        'required' => true,
                                        'data-parsley-equalto' => '#password',
                                        'data-parsley-required-message' => "Please confirm admin Password",
                                        'data-parsley-equalto-message' => 'Confirm password should be same as Password',
                                ]);
                            }
                            ?>
                            <div class="fm-control">
                                {!! Form::password('password_confirmation', $attributes) !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 fm-submit">
                    <button type="submit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('/catalyst/2fa') }}">Google 2FA Settings</a>
                </div>
                <div class="col-xs-6 fm-cancel text-right">
                    <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('js')
    <script type="text/javascript">
        $(function(){
            $(".bootstrap-switch.bootstrap-switch-medium").css('width', '200px');

            change_input_state();
            $("select[name='nested']").change(function() {
                change_input_state();
            });

            function change_input_state() {
                var value = $("select[name='nested']").val();
                if(value == '1') {
                    $("input[name='table_name']").removeAttr('readonly');
                    $("input[name='model']").removeAttr('readonly');
                    $("input[name='controller']").removeAttr('readonly');
                }
                if(value == '') {
                    $("input[name='table_name']").val('').attr('readonly', true);
                    $("input[name='model']").val('').attr('readonly', true);
                    $("input[name='controller']").val('').attr('readonly', true);
                }
            }

            $(document).on('keyup', 'input[name="title"]', function() {
                $('input[name="slug"]').val($(this).val().replace(/[\s\'\"\W]+/g, '-').toLowerCase());
            });

            var nested = ($.trim($("select[name='nested']").val()) != "");
            window.ParsleyValidator.addValidator('conditionalValue', function(value, requirements) {
                if(requirements[1] == $(requirements[0]).val() && value == '') {
                    return false;
                }
                var regex = /^[a-zA-Z_]+$/;
                if (value != '' && ! regex.test(value)) {
                    return false;
                }
                return true;
            }, 32).addMessage('en', 'conditionalValue', 'Field is required and should be Letters only.');

            $(document).on('change keyup', 'input, select, textarea', function(){
                $(".loading-button").button('normal');
            });
        });
    </script>
@stop
