@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $app_version->exists ? 'Update' : 'Create' }} App Version</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\Core\AppVersionController@index') }}">App Version</a></li>
            <li>{{ $app_version->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $app_version->exists ? 'Update' : 'Create' }} App Version</p>
</div>

{!! Form::model($app_version, [
    'method' => $app_version->exists ? 'PUT' : 'POST',
    'action' => $app_version->exists ?
        ['Admin\Core\AppVersionController@update', $app_version->id] : 'Admin\Core\AppVersionController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Platform : <strong>{{ ucwords($app_version->platform) }}</strong>
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter title."                                
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Version</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('version', null, [
                                'class' => "form-control",
                                'placeholder' => "Version",
                                'required' => true,
                                'pattern' => '/^[0-9\.]+$/',
                                'data-parsley-required-message' => "Please enter version.",                                
                                'data-parsley-pattern-message' => "Can only contain Number.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Force Update</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Yes or No"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('force_update', ['No' => 'No', 'Yes' => 'Yes'], null,[
                                'class' => 'chosen-select form-control',
                                'required' => true,
                                'data-parsley-required-message' => "Please select update type.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Description</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('description', null, [
                                'class' => "form-control textarea-no-resize",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter description.",
                                'placeholder' => "Description",
                                'rows' => 6
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>

@if(!empty($app_version->image))
<div class="panel image-panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Existing Image
        </h3>
        <div class="example-box-wrapper">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="thumbnail-box-wrapper">
                    <div class="thumbnail-box">
                        <div class="thumb-content">
                            <div class="center-vertical">
                                <div class="center-content">
                                    <div class="thumb-btn animated zoomIn">
                                        <a href="javascript:void(0)" class="btn btn-md btn-round btn-danger delete-image" title=""><i class="glyph-icon icon-remove"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="thumb-overlay bg-gray"></div>
                        <img src="{{ asset('uploads/version/' . $app_version->image) }}" alt="" style="display: none;">
                        <img src="{{ asset('uploads/version/thumbs/640_480_' . $app_version->image) }}" alt="">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endif

@include('admin.partials.crop_upload', ['status' => empty($app_version->image), 'tooltip' => 'Min Dimension: 640x480'])

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="button" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\AppVersionController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
  <script type="text/javascript">    
    var item_exists = {{ $app_version->image ? 'false' : 'true' }};
    
    $(document).ready(function(){
        
        $upQueue = $(".file-uploader");
        var crop_size = [640, 480];

        var uploader = $upQueue.bcmUpload({
            filters: { // Mime Types to be allowed to select.
                mime_types: [{
                    title: "Image files",
                    extensions: "jpg,gif,png,jpeg"
                }]
            },
            enable_crop: false, // Is cropping required or not? That's it. Cropping will be there for you.
            max_files: 1, // How many uploads allowed?
            required: item_exists, // Is the image upload mandatory?
            multipart_params: {resource: 'Image'}, // You can set it to your choice. You have to check for it in the controller to upload accordingly.
            min_dimention: { // This section is only required for image resources. It will be ignored for other files even if you set it.
                width: crop_size[0],
                height: crop_size[1]
            }
        });

        var uploaded = false;
        uploader.bind('UploadComplete', function (up, files) { // What happens when all the uploads are done.
            uploaded = true;
            $("#moduleForm").submit();
        });

        $('#moduleForm').submit(function() {
            if (uploader.files.length > 0 && uploaded == false) {
                uploader.start();

                return false;
            } else if (uploader.getOption('required') && uploader.files.length < 1) {
                module.notify('Please select some image(s) to proceed', 'error');

                return false;
            } else {
                return true;
            }
        });

    //    module.loadImageCropFunction(crop_size, uploader);

        /**
         * Upload section end.
         */

        $("a.delete-image").click(function() {
            module.confirm('Are you sure you want to delete this image? ', function(isConfirm) {
                if (isConfirm) {
                    var ajax = module.ajax('{{ action("Admin\\Core\\AppVersionController@deleteImage", $app_version->id) }}', {
                        type: 'Cover'
                    }, 'DELETE');

                    ajax.done(function(data){
                        if(data.status) {
                            $("div.image-panel").toggle();
                            uploader.setOption('required', true);
                        }else{
                            module.notify(data.message, 'error');
                        }
                    });
                }
            });
        });
        
        $( "#moduleSubmit" ).click(function( event ) {
            $("#moduleForm").parsley();
            // Validate form with parsley.
            $("#moduleForm").parsley().validate();

            if($("#moduleForm").parsley().isValid()) {
                module.confirm('You are about to release a new version of the app. Are you sure you want to proceed?', function(isConfirm){
                    if (isConfirm) {
                        $( "#moduleForm" ).submit();
                    }
                    $("#moduleSubmit").button('reset');
                });
            }
        });
    });
</script>  
@stop