@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>App Version</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li>App Version</li>
        </ul>
    </div>
    <p>App Version Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            App Version Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.app_version')
                    @if($count_value == 0)
                        <a class="btn btn-default" href="{{ action('Admin\Core\AppVersionController@create') }}">ADD ITEM</a>
                    @endif
                @endCanI
                @CanI(['update.app_version', 'delete.app_version'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('update.app_version')
                            <li><a href="javascript:void(0)" class="notification">Send Notification</a></li>
                        @endCanI                        
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
<!--                <div class="rdtChosen">  
                    <select name="" multiple data-placeholder="Filter options..." 
                            class="chosen-select form-control" id="module_filter">
                        <optgroup label="Status">
                            <option value="status.Published">Published</option>
                            <option value="status.Draft">Draft</option>
                        </optgroup>
                        @if($module_details->maker_checker == 'On')
                        <optgroup label="Approval Status">
                            <option value="admin_status.Approved">Approved</option>
                            <option value="admin_status.Pending">Pending</option>
                        </optgroup>
                        @endif
                    </select>
                </div>-->
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    var table = $("#listing-table").bcmTable({
         dom: '<"datatable-header"><"datatable-scroll"t>'+
               '<"datatable-footer"<"row"<"col-md-2"l><"col-md-3"i><"col-md-3"<"clearfix">><"col-md-4"p>>>',
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Platform", data: "platform", sortable: false},
            { title: "Version", data: "version", sortable: false},
            { title: "Force Update", data: "force_update", sortable: false},            
            @CanI('edit.app_version')
                { title: "Options", data: "options", sortable: false}
            @endCanI
        ]
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.notification", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some item to perform the action.', 'error');
            return false;
        }
        
        if(ids.length > 1) {
            module.notify('Notification must be processed one at a time.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to send notification?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\Core\AppVersionController@notification") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('User notified successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });        
});
</script>
@stop