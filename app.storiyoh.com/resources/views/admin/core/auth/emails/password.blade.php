Hi {{ $user->first_name }} {{ $user->last_name }} <br><br> We received a passsword retrieval request for your Content Management System.
Click here to reset your password:
<a href="{{ $link = url('catalyst/password/reset', $token).'?email='.urlencode($user->email) }}"> {{ $link }} </a>

<br><br> Thanks for using Brand Catalyst Media CMS Solutions <br>
<br> Regards,<br> The Brand Catalyst Media Team <br> www.brandcatmedia.com