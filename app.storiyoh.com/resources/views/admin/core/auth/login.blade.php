@extends('admin.layouts.login')

@section('css')
<style type="text/css">

    html,body {
        height: 100%;
        background: #fff;
    }

</style>
@stop

@section('content')
<div class="center-vertical">
    <div class="center-content">
		<span id="fullscreen-btn" class="hide"></span>
		{!! Form::open([
			'action' => "Admin\Auth\LoginController@login",
			'method' => 'POST',
			'class' => "col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin",
			'id' => "login-validation",
			'autocomplete' => 'off',
			'data-parsley-validate' => "",
		]) !!}
            <h3 class="text-center pad25B font-gray text-transform-upr font-size-23">Catalyst</h3>
            <div id="login-form" class="content-box bg-default">
                <div class="content-box-wrapper pad20A">
                    <img class="mrg25B center-margin display-block" src="{{ url('assets/admin/images/logo.png') }}" alt="">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-envelope-o"></i>
                            </span>
                            {!! Form::text('username', null, [
                                'class' => 'form-control',
                                'placeholder' => 'Username',
                                'required' => true,
                                'data-parsley-required-message' => "Please enter your Username",
                            ]) !!}

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            {!! Form::password('password', [
                                    'class' => 'form-control',
                                    'placeholder' => 'Password',
                                    'required' => true,
                                    'data-parsley-required-message' => "Please enter your Password"
                            ]) !!}
                        </div>
                    </div>
					<div class="form-group">
                        <div class="checkbox checkbox-info" style="height: 20px;">
                            <label>
                                <input type="checkbox" id="loginCheckbox1" class="custom-checkbox" name="remember">
                                Remember me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" data-loading-text="Please wait..." data-normal-text="Login" class="btn btn-block loading-button btn-primary">Login</button>
                    </div>

                    <div class="form-group text-center">
                        <a href="#" class="switch-button" switch-target="#login-forgot" switch-parent="#login-form" title="Recover password">Forgot your password?</a>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}

            {!! Form::open([
                    'url' => 'catalyst/password/email',
                    'method' => 'POST',
                    'class' => "col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin",
                    'data-parsley-validate' => "",
            ]) !!}

            <div id="login-forgot" class="content-box bg-default" style="display: none;">
                <div class="content-box-wrapper pad20A">

                    <div class="form-group">
                        <label for="forgot_email">Email address / Username:</label>
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-envelope-o"></i>
                            </span>
                            {!! Form::text('email', null, [
                                'class' => 'form-control',
                                'id' => 'forgot_email',
                                'placeholder' => 'Enter email / Username',
                                'required' => true,
                                'data-parsley-required-message' => "Please enter your Email/Username"
                            ]) !!}

                        </div>
                    </div>
                </div>
                <div class="button-pane text-center">
                    {{--<button type="submit" class="btn btn-md btn-primary">Recover Password</button>--}}
                    <button type="submit" data-loading-text="Please wait..." data-normal-text="Recover Password" class="btn btn-md loading-button btn-primary">Recover Password</button>
                    <a href="" class="btn btn-md switch-button" switch-target="#login-form" switch-parent="#login-forgot" title="Cancel">Cancel</a>
                </div>
            </div>

            {!! Form::close() !!}

    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(function() {
	/**
	 * Email field Error Messages.
	 */
	@if($errors->has('username'))
	    module.notify("{{ $errors->first('username') }}", 'error');
	@endif

	/**
	 * Password field Error Messages.
	 */
	@if($errors->has('password'))
	    module.notify("{{ $errors->first('password') }}", 'error');
	@endif

	/**
	 * Forgot Password success Messages.
	 */
	@if(session('status'))
	    module.notify("{{ session('status') }}", 'success');
	@endif

	/**
	 * Email field Error Messages.
	 */
	@if($errors->has('email'))
	    module.notify("{{ $errors->first('email') }}", 'error');
	@endif

    $('.loading-button').click(function() {
        var btn = $(this)
        btn.button('loading');
    });
});
</script>
@stop
