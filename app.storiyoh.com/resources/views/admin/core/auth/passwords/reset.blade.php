@extends('admin.layouts.login')

@section('css')
<style type="text/css">

    html,body {
        height: 100%;
        background: #fff;
    }

</style>
@stop

@section('content')
<div class="center-vertical">
    <div class="center-content">
		<span id="fullscreen-btn" class="hide"></span>
		{!! Form::open([
			'url' => "catalyst/password/reset" ,
			'method' => 'POST',
			'class' => "col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin",
			'id' => "login-validation",
			'autocomplete' => 'off',
			'data-parsley-validate' => "",
		]) !!}
            <h3 class="text-center pad25B font-gray text-transform-upr font-size-23">Catalyst <span class="opacity-80">WCMS</span></h3>
            <div id="login-form" class="content-box bg-default">
				{!! Form::hidden('token', $token) !!}

                <div class="content-box-wrapper pad20A">
                    <img class="mrg25B center-margin radius-all-100 display-block" src="{{ asset('assets/admin/images/logo.png') }}" alt="">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-envelope-o"></i>
                            </span>
                            {!! Form::email('email', $email, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Email',
                                    'required' => true,
                                    'data-parsley-required-message' => "Please enter your Email",
                            ]) !!}

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            {!! Form::password('password', [
                                    'class' => 'form-control',
                                    'placeholder' => 'Choose your Password',
                                    'required' => true,
                                    'id' => 'password',
                                    'data-parsley-required-message' => "Please enter your New Password"
                            ]) !!}
                        </div>
                    </div>
					<div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            {!! Form::password('password_confirmation', [
                                    'class' => 'form-control',
                                    'placeholder' => 'Confirm your Password',
                                    'required' => true,
                                    'data-parsley-equalto' => '#password',
                                    'data-parsley-required-message' => "Please confirm your Password",
                                    'data-parsley-equalto-message' => 'Confirm password should be same as New Password',
                            ]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">Reset Password</button>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}

    </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('assets/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
<script type="text/javascript">
$(function() {
	/**
	 * Email field Error Messages.
	 */
	@if($errors->has('email'))
	    module.notify("{{ $errors->first('email') }}", 'error');
	@endif
});
</script>
@stop
