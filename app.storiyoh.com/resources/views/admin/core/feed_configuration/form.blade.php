@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $feed_configuration->exists ? 'Update' : 'Create' }} App Version</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\Core\FeedConfigurationController@index') }}">App Version</a></li>
            <li>{{ $feed_configuration->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $feed_configuration->exists ? 'Update' : 'Create' }} App Version</p>
</div>
{{ dd($feed_configuration) }}

{!! Form::model($feed_configuration, [
    'method' => $feed_configuration->exists ? 'PUT' : 'POST',
    'action' => $feed_configuration->exists ?
        ['Admin\Core\FeedConfigurationController@update', $feed_configuration->id] : 'Admin\Core\FeedConfigurationController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter title."                                
                            ]) !!}
                        </div>
                    </div>
                </div>                                
            </div>                        
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="button" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\FeedConfigurationController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
   
@stop