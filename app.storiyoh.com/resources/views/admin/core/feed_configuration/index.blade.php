@extends('admin.layouts.master')

@section('content')

<div id="page-title">
    <h2>Configuration</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li class="blueText">Configuration</li>
            <li>Update</li>
        </ul>
    </div>
    <p>Configuration</p>
</div>

{!! Form::model($feed_configuration, [
    'method' => 'POST',
    'action' => ['Admin\Core\FeedConfigurationController@store'],
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}

<div class="panel">
    <div class="panel-body">
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Trending Shows</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Most Subscribed over x Period"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('trending_shows', null, [
                                'class' => "form-control",
                                'placeholder' => "value for trending shows",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter value."                                
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Trending Boards</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Most Followers over x Period"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('trending_boards', null, [
                                'class' => "form-control",
                                'placeholder' => "value for trending boards",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter value."                                
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Trending Episodes</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Most Listens over x Period"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('trending_episodes', null, [
                                'class' => "form-control",
                                'placeholder' => "value for trending episodes",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter value."                                
                            ]) !!}
                        </div>
                    </div>
                </div>
                <!-- Enable / Disable -->
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Recommended Users</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Enable / Disable"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('recommended_users', ['' => 'Please select' , '1' => 'Enabled', '0' => 'Disabled'], null,[
                                'class' => 'chosen-select form-control',
                                'required' => true,
                                'data-parsley-required-message' => "Please select Recommended Users status",
                            ]) !!}
                        </div>
                    </div>
                </div>                               
            </div>                        
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="button" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\DashboardController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
  <script type="text/javascript">    
    $(document).ready(function(){
        $( "#moduleSubmit" ).click(function( event ) {
            $("#moduleForm").parsley();
            // Validate form with parsley.
            $("#moduleForm").parsley().validate();

            if($("#moduleForm").parsley().isValid()) {
                module.confirm('Are you sure you want to proceed?', function(isConfirm){
                    if (isConfirm) {
                        $( "#moduleForm" ).submit();
                    }
                    $("#moduleSubmit").button('reset');
                });
            }
        });
    });
</script> 
@stop