@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $language->exists ? 'Update' : 'Create' }} Language</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Configure</a></li>
            <li>{{ $language->exists ? 'Update' : 'Create' }} Language</li>
        </ul>
    </div>
    <p>Delight UI comes packed with a lot of options.</p>
</div>
{!! Form::model($language, [
    'method' => $language->exists ? 'PUT' : 'POST',
    'action' => $language->exists ?
        ['Admin\Core\LanguageController@update', $language->id] : 'Admin\Core\LanguageController@store',
    'class' => 'form bordered-row',
    'autocomplete' => 'off',
    'data-parsley-validate' => "",
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Language Title</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Language title eg. English",
                                'required' => true,
                                'pattern' => '/^[a-zA-Z\s]+$/',
                                'data-parsley-required-message' => "Please enter language title.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Locale</label>
                        </div>
                        <div class="fm-control">
                            <?php
                                $attributes = [
                                        'class' => "form-control",
                                        'placeholder' => "Language locale eg. en",
                                        'required' => true,
                                        'pattern' => '/^[a-zA-Z\s]+$/',
                                        'data-parsley-required-message' => "Please enter language locale.",
                                        'data-parsley-pattern-message' => "Can only contain Letters.",
                                ];
                                if($language->exists) {
                                    $attributes['readonly'] = '';
                                }
                            ?>
                            {!! Form::text('locale', null, $attributes) !!}
                        </div>
                    </div>
                </div>
            </div>
            

        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $language->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $language->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\LanguageController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
$(function(){
    $(document).on('change keyup', 'input, select, textarea', function(){
        $(".loading-button").button('normal');
    });
});
</script>
@stop
