@extends('admin.layouts.master')

@section('content')
    <div id="page-title">
        <h2>Language</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
                <li><a href="#menuLeft">Configure</a></li>
                <li>Language</li>
            </ul>
        </div>
    </div>

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Language Listing
            </h3>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    @CanI('create.language')
                    <a class="btn btn-default" href="{{ action('Admin\Core\LanguageController@create') }}">Add Language</a>
                    @endCanI
                    @CanI(['edit.language', 'delete.language'], 'OR')
                    <a class="btn btn-default" href="{{ action('Admin\Core\LanguageController@show', $language->id) }}">Manage</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            @CanI('edit.admin')
                            <li><a href="javascript:void(0)" class="publish">Publish</a></li>
                            <li><a href="javascript:void(0)" class="draft">Draft</a></li>
                            <li><a href="{{ action('Admin\Core\LanguageController@reorder') }}">Reorder</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="default">Default</a></li>
                            @endCanI
                            @CanI('delete.admin')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="delete">Delete</a></li>
                            @endCanI
                        </ul>
                    </div>
                    @endCanI
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <!--Chosen Select-->
                    <div class="rdtChosen">
                        <select name="" multiple data-placeholder="Filter Options..."
                                class="chosen-select form-control" id="module_filter">
                            <optgroup label="Status">
                                <option value="status.Published">Published</option>
                                <option value="status.Draft">Draft</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>

            <div class="example-box-wrapper">
                <div class="table-responsive">
                    <table id="listing-table"
                           class="table table-striped table-bordered responsive no-wrap"
                           cellspacing="0" width="100%"></table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(function(){

            var table = $('#listing-table').bcmTable({
                columns: [
                    { title: "#", data: "order", sortable: false },
                    { title: "Language", data: "title" },
                    { title: "Locale", data: "locale" },
                    { title: "Default", data: "default"  },
                    { title: "Status", data: "status" },
                    @CanI('edit.language')
                    { title: "Options", data: "options", sortable: false }
                    @endCanI
                ]
            });

            $("#module_filter").change(function(){
                table.ajax.reload();
            });

            $(document).on("click", "a.publish", function(){
                var ids = module.getSelectedIds(table);

                if(ids.length < 1) {
                    module.notify('Please select some languages to perform the action.', 'error');
                    return false;
                }

                module.confirm('Are you sure you want to publish these language(s)?', function(isConfirm){
                    if (isConfirm) {
                        var ajax = module.publish(ids);
                        ajax.done(function(data) {
                            if (data.status) {
                                module.notify('Language(s) published successfully.', 'success');
                            }else{
                                module.notify(data.message, 'error');
                            }
                            table.ajax.reload();
                        });
                    }
                });
            });

            $(document).on("click", "a.draft", function(){
                var ids = module.getSelectedIds(table);

                if(ids.length < 1) {
                    module.notify('Please select some languages to perform the action.', 'error');
                    return false;
                }

                module.confirm('Are you sure you want to draft these language(s)?', function(isConfirm){
                    if(isConfirm) {
                        var ajax = module.draft(ids);
                        ajax.done(function(data) {
                            if (data.status) {
                                module.notify('Language(s) drafted successfully.', 'success');
                            }else{
                                module.notify(data.message, 'error');
                            }
                            table.ajax.reload();
                        });
                    }
                });
            });

            $(document).on("click", "a.delete", function(){
                var ids = module.getSelectedIds(table);

                if(ids.length < 1) {
                    module.notify('Please select some languages to perform the action.', 'error');
                    return false;
                }

                module.confirm('Are you sure you want to delete these language(s)?', function(isConfirm){
                    if (isConfirm) {
                        var ajax = module.delete(ids);
                        ajax.done(function(data) {
                            if (data.status) {
                                module.notify('Language(s) deleted successfully.', 'success');
                            }else{
                                module.notify(data.message, 'error');
                            }
                            table.ajax.reload();
                        });
                    }
                });
            });

            $(document).on("click", "a.default", function(){
                var ids = module.getSelectedIds(table);

                if(ids.length < 1 || ids.length > 1) {
                    module.notify('Please select one languages to perform the action.', 'error');
                    return false;
                }

                module.confirm('Are you sure you want to make default these language?', function(isConfirm){
                    if (isConfirm) {
                        var ajax = module.approve(ids);
                        ajax.done(function(data) {
                            if (data.status) {
                                module.notify('Language made default successfully.', 'success');
                            }else{
                                module.notify(data.message, 'error');
                            }
                            table.ajax.reload();
                        });
                    }
                });
            });

        });
    </script>
@stop