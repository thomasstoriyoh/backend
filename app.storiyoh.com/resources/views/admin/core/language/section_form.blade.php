@extends('admin.layouts.master')

@section('content')
    <div id="page-title">
        <h2>Add Section</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
                <li><a href="#menuLeft">Configure</a></li>
                <li><a href="{{ action('Admin\Core\LanguageController@index') }}">Language</a></li>
                <li>Add Section</li>
            </ul>
        </div>
    </div>
    {!! Form::open([
        'method' => 'POST',
        'action' => ['Admin\Core\LanguageController@storeSection'],
        'class' => 'form bordered-row',
        'autocomplete' => 'off',
        'data-parsley-validate' => "",
    ]) !!}
    <div class="panel">
        <div class="panel-body">
            @if (!empty($errors->all()))
                <div class="row">
                    @foreach ($errors->all() as $error)
                        <div class="col-md-6">
                            <div class="alert alert-danger mrg10B">
                                <p>{{ @$error }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            <div class="example-box-wrapper">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Section Title</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::text('title', null, [
                                    'class' => "form-control",
                                    'placeholder' => "Language Section title eg. Header",
                                    'required' => true,
                                    'pattern' => '/^[a-zA-Z\s]+$/',
                                    'data-parsley-required-message' => "Please enter language section title.",
                                    'data-parsley-pattern-message' => "Can only contain Letters.",
                                ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Slug</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::text('slug', null, [
                                    'class' => "form-control",
                                    'required' => true,
                                    'pattern' => '/^[a-z\-]+$/',
                                    'data-parsley-required-message' => "Please enter language section.",
                                    'data-parsley-pattern-message' => "Can only contain Small Letters.",
                                ]) !!}
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 fm-submit">
                    <button type="submit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
                </div>
                <div class="col-xs-6 fm-cancel text-right">
                    <a href="{{ action('Admin\Core\LanguageController@index') }}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('js')
    <script type="text/javascript">
        $(function(){
            $(document).on('change keyup', 'input, select, textarea', function(){
                $(".loading-button").button('normal');
            });
        });
    </script>
@stop
