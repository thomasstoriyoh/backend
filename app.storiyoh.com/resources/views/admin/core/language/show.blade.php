@extends('admin.layouts.master')

@section('content')
    <div id="page-title">
        <h2>{{ $language->title }}</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
                <li><a href="#menuLeft">Configure</a></li>
                <li><a href="{{ action('Admin\Core\LanguageController@index') }}">Language</a></li>
                <li>{{ $language->title }}</li>
            </ul>
        </div>
    </div>

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Section Listing
            </h3>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a class="btn btn-default" href="{{ action('Admin\Core\LanguageController@createSection') }}">Add Section</a>
                    <a class="btn btn-default delete-section" href="javascript:void(0)">Delete</a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">

                </div>
            </div>

            <div class="example-box-wrapper">
                <div class="table-responsive">
                    <table id="listing-table"
                           class="table table-striped table-bordered responsive no-wrap"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Slug</th>
                            <th>Section</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($sections as $slug => $section)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $slug }}</td>
                                <td>{{ $section }}</td>
                                <td>
                                    <div class="dropdown">
                                        <a href="#" title="Options" class="user-profile clearfix" data-toggle="dropdown">
                                            <span class="glyph-icon icon-align-justify"></span>
                                            <i class="glyph-icon icon-angle-down"></i>
                                        </a>
                                        <div class="dropdown-menu float-right">
                                            <ul class="reset-ul mrg5B">
                                                <li>
                                                    <a href="{{ action('Admin\Core\LanguageController@words', $slug) }}">
                                                        <i class="glyph-icon icon-edit"></i> Show Words
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(function(){

            var table = $("#listing-table").DataTable({
                dom: '<"datatable-header"><"datatable-scroll"t>'+
                '<"datatable-footer"<"row"<"col-md-3"l><"col-md-3"i><"col-md-3"<"clearfix"f>><"col-md-3"p>>>',
            });
            $('.dataTables_filter input').attr("placeholder", "Search...");

            $(document).on('click', 'a.delete-section', function () {
                var ids = [];
                for(i = 0; i < table.rows('.tr-selected').data().length; i++) {
                    ids.push(table.rows('.tr-selected').data()[i][1]);
                }

                if (ids.length < 1) {
                    module.notify('Please select at least one section to perform this action.', 'error');
                    return false;
                }

                module.confirm('Are you sure you want to delete this section?', function(isConfirm){
                    if (isConfirm) {
                        var ajax = module.unapprove(ids);
                        ajax.done(function(data) {
                            if (data.status) {
                                module.notify('Section(s) deleted successfully.', 'success');
                                table.rows('.tr-selected').remove().draw();
                            }else{
                                module.notify(data.message, 'error');
                            }
                        });
                    }
                });
            });

        });
    </script>
@stop