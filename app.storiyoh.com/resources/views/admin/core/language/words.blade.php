@extends('admin.layouts.master')

@section('content')
    <div id="page-title">
        <h2>Words</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
                <li><a href="#menuLeft">Configure</a></li>
                <li><a href="{{ action('Admin\Core\LanguageController@index') }}">Language</a></li>
                <li><a href="{{ action('Admin\Core\LanguageController@show', 1) }}">English</a></li>
                <li>{{ ucwords(str_replace('-', ' ', $section)) }}</li>
            </ul>
        </div>
    </div>

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Words Listing
            </h3>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <a class="btn btn-default" href="#add_word_modal" data-toggle="modal">Add Word</a>
                    <a class="btn btn-default delete-words" href="javascript:void(0)">Delete</a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">

                </div>
            </div>

            <div class="example-box-wrapper">
                <div class="table-responsive">
                    <table id="listing-table"
                           class="table table-striped table-bordered responsive no-wrap"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Slug ( Prefix: {{ $section }} )</th>
                            @foreach($languages as $language)
                            <th>{{ $language->title }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>

                        <?php $put_row = false; ?>
                        @if(session('slug'))
                        <?php
                        $put_row = !array_key_exists(session('slug'), $words);
                        ?>
                        @endif

                        @foreach($words as $slug => $word)
                            <tr>
                                <td>{{ $slug }}</td>
                                @foreach($languages as $language)
                                    <?php
                                        $params = ['locale' => $language->locale];
                                    ?>
                                    <td>
                                        <a href="#" class="xeditable-lang" data-name="{{ $section }}" data-type="text" data-params="{{ json_encode($params) }}" data-pk="{{ $slug }}" data-title="Enter translation">{{ translate($section.'.'.$slug, $language->locale) }}</a>
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        @if($put_row)
                            <tr>
                                <td>{{ session('slug') }}</td>
                                @foreach($languages as $language)
                                    <?php
                                        $params = ['locale' => $language->locale];
                                        $title = $language->locale == 'en' ? session('title') : '';
                                    ?>
                                    <td>
                                        <a href="#" class="xeditable-lang" data-name="{{ $section }}" data-type="text" data-params="{{ json_encode($params) }}" data-pk="{{ session('slug') }}" data-title="Enter translation">{{ $title }}</a>
                                    </td>
                                @endforeach
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="add_word_modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Word</h4>
                </div>
                {!! Form::open([
                    'method' => 'POST',
                    'action' => ['Admin\Core\LanguageController@storeWord', $section],
                    'class' => 'form bordered-row',
                    'autocomplete' => 'off',
                    'data-parsley-validate' => "",
                ]) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fm-label">
                                    <label>Word</label>
                                </div>
                                <div class="fm-control">
                                    {!! Form::text('title', null, [
                                        'class' => "form-control",
                                        'placeholder' => "Language word",
                                        'required' => true,
                                        'pattern' => '/^[\w\s]+$/',
                                        'data-parsley-required-message' => "Please enter language word.",
                                        'data-parsley-pattern-message' => "Can only contain Word Characters.",
                                    ]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fm-label">
                                    <label>Slug</label>
                                </div>
                                <div class="fm-control">
                                    {!! Form::text('slug', null, [
                                        'class' => "form-control",
                                        'placeholder' => "Language word slug.",
                                        'required' => true,
                                        'pattern' => '/^[a-z\-]+$/',
                                        'data-parsley-required-message' => "Please enter language word slug.",
                                        'data-parsley-pattern-message' => "Can only contain Small Letters.",
                                    ]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Word</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(function(){

            var table = $("#listing-table").DataTable({
                dom: '<"datatable-header"><"datatable-scroll"t>'+
                '<"datatable-footer"<"row"<"col-md-3"l><"col-md-3"i><"col-md-3"<"clearfix"f>><"col-md-3"p>>>',
            });
            $('.dataTables_filter input').attr("placeholder", "Search...");

            $(".xeditable-lang").editable({
                mode: 'popup',
                url: module_actions.caption,
            });

            $(document).on('click', 'a.delete-words', function () {
                var ids = [];
                for(i = 0; i < table.rows('.tr-selected').data().length; i++) {
                    ids.push(table.rows('.tr-selected').data()[i][0]);
                }

                var data = {
                    slugs: ids
                };

                if (ids.length < 1) {
                    module.notify('Please select at least one item to perform this action.', 'error');
                    return false;
                }

                module.confirm('Are you sure you want to delete these word(s)?', function (isConfirm) {
                    if (isConfirm) {
                        module.ajax("{{ action('Admin\Core\LanguageController@deleteImage', $section) }}", data, 'DELETE').done(function (response) {
                            if (response.status) {
                                module.notify('Language(s) drafted successfully.', 'success');
                                table.rows('.tr-selected').remove().draw();
                            }else{
                                module.notify(response.message, 'error');
                            }
                        });
                    }
                });
            });


        });
    </script>
@stop