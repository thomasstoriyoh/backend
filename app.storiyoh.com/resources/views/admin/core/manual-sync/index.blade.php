@extends('admin.layouts.master')

@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        td.processing span.loading {display: block}
        td.processing a[data-sync] {display: none}
        td span.loading {display: none}
        td a[data-sync] {display: block}
    </style>
@stop

@section('content')
    <div id="page-title">
        <h2>Manual Sync</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
                <li><a href="#menuLeft">Modules</a></li>
                <li>Manual Sync</li>
            </ul>
        </div>
    </div>

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Sync Manually
            </h3>

            <div class="table-responsive">
                <table id="listing-table"
                       class="table table-striped table-bordered responsive no-wrap"
                       cellspacing="0" width="100%">

                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Component</th>
                        <th>Available Quota</th>
                        <th>Sync</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ class_basename($item) }}</td>
                            <td>{{ $item->availableQuota() }}</td>
                            <td class="{{ $item->isInProgress() ? 'processing' : '' }}">
{{--                                @if($item->isSyncable())--}}
                                    <span class="loading">
                                        <i class="fa fa-refresh fa-lg fa-spin"></i> In Progress...
                                    </span>
                                    <a href="javascript:void(0);" data-sync="{{ class_basename($item) }}">
                                        <i class="fa fa-refresh fa-lg"></i> Sync Now...
                                    </a>
                                {{--@endif--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(function () {
            var phases = {}, quotas = {}, $sync = $("[data-sync]");
            $sync.each(function (i, el) {
                phases[$(el).data('sync')] = $(el).parent().hasClass('processing');
            });

            populateLoaders();

            var interval = null;
//            startMonitoring();

            function startMonitoring() {
                interval = setInterval(function () {
                    module.ajax(module_actions.publish, {}, 'PUT').done(function (res) {
                        phases = res.data.phases;
                        quotas = res.data.quotas;
                        populateLoaders();
                        reduceQuotas();
                    })
                }, 1000);
            }

            function populateLoaders() {
                var processing = false;
                for (var phase in phases) {
                    var $target = $("[data-sync='"+phase+"']").parent();
                    if (phases[phase]) {
                        $target.addClass('processing');
                        processing = true;
                    } else {
                        $target.removeClass('processing');
                    }
                }

                clearMonitoring();
                if (processing) {
                    setTimeout(function () {
                        startMonitoring();
                    }, 2000);
                }
            }

            function clearMonitoring() {
                if (interval) {
                    clearInterval(interval);
                }
            }

            function reduceQuotas() {
                for (var quota in quotas) {
                    var $target = $("[data-sync='"+quota+"']").parent();

                    $target.prev().text(quotas[quota]);
                }
            }

            $sync.click(function(e) {
                e.stopPropagation();
                var data = { type : $(this).data('sync') };

                module.ajax(module_actions.json, data, 'GET').done(function (res) {
                    var type = res.status ? 'success' : 'error';
                    module.notify(res.message, type);
                    phases[data.type] = true;
                    populateLoaders();
                }).fail(function (res) {
                    module.notify("Something went wrong, Please try again after some time.", "error");
                });
            });
        });
    </script>
@stop
