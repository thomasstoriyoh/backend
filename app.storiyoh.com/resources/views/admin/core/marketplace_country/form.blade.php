@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $marketplace_country->exists ? 'Update' : 'Create' }} Countries</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\Core\MarketplaceCountryController@index') }}">Marketplace Countries</a></li>
            <li>{{ $marketplace_country->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $marketplace_country->exists ? 'Update' : 'Create' }} Marketplace Countries</p>
</div>

{!! Form::model($marketplace_country, [
    'method' => $marketplace_country->exists ? 'PUT' : 'POST',
    'action' => $marketplace_country->exists ?
        ['Admin\Core\MarketplaceCountryController@update', $marketplace_country->id] : 'Admin\Core\MarketplaceCountryController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">        
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Country</label>
                        </div>
                        <div class="fm-control">                            
                            @if($marketplace_country->exists)
                                <b><i>{{ $marketplace_country->title }}</i></b>
                            @else
                                {!! Form::select('title', ['' => 'Select...'] + $country_list, null,[
                                    'class' => 'chosen-select form-control',
                                    'required' => true
                                ]) !!}
                            @endif
                            
                        </div>
                    </div>
                </div>                
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Currency</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Country Currency"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('currency_id', ['' => 'Select...'] + $currency_list, null,[
                                'class' => 'chosen-select form-control'                              
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Markup Price</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Markup Price in Percent"></i>
                            </div>
                        </div>
                        <div class="fm-control">                            
                            {!! Form::text('markup_price', null, [
                                'class' => "form-control",
                                'placeholder' => "Markup Price",
                                'required' => true,
                                'pattern' => '/^[0-9\.]+$/',
                                'data-parsley-required-message' => "Please enter markup price.",
                                'data-parsley-pattern-message' => "Can only contain number.",                                
                            ]) !!}                                                        
                        </div>
                    </div>
                </div>                
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Commission Price</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Commission Price in Percent"></i>
                            </div>
                        </div>
                        <div class="fm-control">                            
                            {!! Form::text('commission_price', null, [
                                'class' => "form-control",
                                'placeholder' => "Commission Price",
                                'required' => true,
                                'pattern' => '/^[0-9\.]+$/',
                                'data-parsley-required-message' => "Please enter commission price.", 
                                'data-parsley-pattern-message' => "Can only contain number.",                               
                            ]) !!}                                                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Tax Name</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Tax Name"></i>
                            </div>
                        </div>
                        <div class="fm-control">                            
                            {!! Form::text('tax_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Tax Name",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter tax name."
                            ]) !!}                                                        
                        </div>
                    </div>
                </div>                
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Tax</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Tax in Percent"></i>
                            </div>
                        </div>
                        <div class="fm-control">                            
                            {!! Form::text('tax_price', null, [
                                'class' => "form-control",
                                'placeholder' => "Tax",
                                'required' => true,
                                'pattern' => '/^[0-9\.]+$/',
                                'data-parsley-required-message' => "Please enter tax.", 
                                'data-parsley-pattern-message' => "Can only contain number.",                               
                            ]) !!}                                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $marketplace_country->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $marketplace_country->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>                
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\MarketplaceCountryController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
  <script type="text/javascript">    
    $(function(){
        module.counter($('input[name="title"]'), 50);
    });
</script>  
@stop