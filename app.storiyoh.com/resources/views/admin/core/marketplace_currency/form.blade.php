@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $marketplace_currency->exists ? 'Update' : 'Create' }} Currency</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\Core\MarketplaceCurrencyController@index') }}">Marketplace Currencies</a></li>
            <li>{{ $marketplace_currency->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $marketplace_currency->exists ? 'Update' : 'Create' }} Marketplace Currency</p>
</div>

{!! Form::model($marketplace_currency, [
    'method' => $marketplace_currency->exists ? 'PUT' : 'POST',
    'action' => $marketplace_currency->exists ?
        ['Admin\Core\MarketplaceCurrencyController@update', $marketplace_currency->id] : 'Admin\Core\MarketplaceCurrencyController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">        
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Currency</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Currency Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter name of Currency."                                
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $marketplace_currency->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $marketplace_currency->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>                
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\MarketplaceCurrencyController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
  <script type="text/javascript">    
    $(function(){
        module.counter($('input[name="title"]'), 50);
    });
</script>  
@stop