@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $module->exists ? 'Update' : 'Create' }}  Module</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Configure</a></li>
            <li>{{ $module->exists ? 'Update' : 'Create' }} Module</li>
        </ul>
    </div>
    <p>Delight UI comes packed with a lot of options.</p>
</div>

{!! Form::model($module, [
    'method' => $module->exists ? 'PUT' : 'POST',
    'action' => $module->exists ? 
        ['Admin\Core\ModuleController@update', $module->id] : 'Admin\Core\ModuleController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'data-parsley-validate' => "",
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Basic Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Module Title</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Module Tile"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Module Title",
                                'required' => true,
                                'pattern' => '/^[a-zA-Z\s\&\/]+$/',
                                'data-parsley-required-message' => "Please enter module title.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Module Slug</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Unique Module Slug"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('slug', null, [
                                'class' => "form-control",
                                'placeholder' => "Module Slug",
                                'required' => true,
                                'pattern' => '/^[a-zA-Z\-]+$/',
                                'data-parsley-required-message' => "Please enter module slug.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
            $types = ['Module', 'Report', 'Configure', 'Documentation'];
            $type_array = ['' => 'Choose...'];
            foreach ($types as $type) {
                $type_array[$type] = $type;
            }
            ?>
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Module Type</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Module Type"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('type', $type_array, 'Module', [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter module type.",
                                'id' => "type",
                            ]) !!}
                        </div>
                    </div>
                </div>
                
                <?php
                $module_array = ['0' => 'Self'];
                foreach ($modules as $module_item) {
                    if ($module_item->id == $module->id) {
                        continue;
                    }
                    $module_array[$module_item->id] = $module_item->title;
                }
                ?>
                
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Module Parent</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Module Parent"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('module_id', $module_array, null, [
                                'class' => "form-control chosen-select",
                                'id' => "module_id",
                            ]) !!}
                        </div>
                    </div>
                </div>
                
                {{--<div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Maker Checker</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Has admin_status field in the table"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('has_maker_checker', ["No", "Yes"], null, [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'data-parsley-required-message' => "Please select Maker Checker type.",
                                'id' => "has_maker_checker",
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Has Details</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Has any details page for this module."></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('has_details', ["No"=>"No", "Yes"=>"Yes"], null, [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'data-parsley-required-message' => "Please select Module Details type.",
                                'id' => "has_details",
                            ]) !!}
                        </div>
                    </div>
                </div> --}}
            </div>
            @if(! $module->exists)
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Controller Required</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Controller Required"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('nested', [
                                '' => 'No',
                                '1' => 'Yes',
                            ], null, [
                                'class' => "form-control chosen-select",
                                'id' => "nested",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Table Name</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Unique Table Name"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('table_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Module Table Name",
                                'readonly' => true,
                                'data-parsley-validate-if-empty data-parsley-conditionalValue' => '["[name=\"nested\"] option:selected", "1"]',
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Model Name</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Unique Model Name"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('model', null, [
                                'class' => "form-control",
                                'placeholder' => "Model Name",
                                'readonly' => true,
                                'data-parsley-validate-if-empty data-parsley-conditionalValue' => '["[name=\"nested\"] option:selected", "1"]',
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Controller Name</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Unique Controller Name"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('controller', null, [
                                'class' => "form-control",
                                'placeholder' => "Controller Name",
                                'readonly' => true,
                                'data-parsley-validate-if-empty data-parsley-conditionalValue' => '["[name=\"nested\"] option:selected", "1"]',
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            @endif
            
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $module->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $module->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\ModuleController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
$(function(){
    $(".bootstrap-switch.bootstrap-switch-medium").css('width', '200px');
    
    change_input_state();
    $("select[name='nested']").change(function() {
        change_input_state();
    });
    
    function change_input_state() {
        var value = $("select[name='nested']").val();
        if(value == '1') {
            $("input[name='table_name']").removeAttr('readonly');
            $("input[name='model']").removeAttr('readonly');
            $("input[name='controller']").removeAttr('readonly');
        }
        if(value == '') {
            $("input[name='table_name']").val('').attr('readonly', true);
            $("input[name='model']").val('').attr('readonly', true);
            $("input[name='controller']").val('').attr('readonly', true);
        }
    }
    
    $(document).on('keyup', 'input[name="title"]', function() {
        $('input[name="slug"]').val($(this).val().replace(/[\s\'\"\W]+/g, '-').toLowerCase());
    });
    
    var nested = ($.trim($("select[name='nested']").val()) != "");
    window.ParsleyValidator.addValidator('conditionalValue', function(value, requirements) {
        if(requirements[1] == $(requirements[0]).val() && value == '') {
            return false;
        }
        var regex = /^[a-zA-Z_]+$/;
        if (value != '' && ! regex.test(value)) {
            return false;
        }
        return true;
    }, 32).addMessage('en', 'conditionalValue', 'Field is required and should be Letters only.');
    
    $(document).on('change keyup', 'input, select, textarea', function(){
        $(".loading-button").button('normal');
    });
});
</script>
@stop