@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Modules</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Configure</a></li>
            <li><a href="{{ action('Admin\Core\ModuleController@index') }}">Modules</a></li>
            <li>Reorder</li>
        </ul>
    </div>
    <p>Delight UI comes packed with a lot of options.</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Module Reorder
        </h3>
        <div class="dd cg-dd" id="reorder">
            <ol class="dd-list">
                @foreach($modules as $item)
                <li class="dd-item" data-id="{{ $item->id }}">
                    <div class="dd-handle">{{ $item->title }}</div>
                    @if($item->child->toArray())
                        @include('admin.partials.sort', ['items' => $item->child()->orderBy('order')->latest()->get()])
                    @endif
                </li>
                @endforeach
                
            </ol>
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" data-loading-text="Please wait..." data-normal-text="Submit" id="sort_trigger" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\ModuleController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/admin/js/nestable.js') }}"></script>
<script type="text/javascript">
$(function(){
    $('#reorder').nestable().on('change', function(e){
        
    });
    
    $("#sort_trigger").click(function(){
        var data = {order : $('.dd').nestable('serialize')};
        var ajax = module.ajax('{{ action("Admin\\Core\\ModuleController@postReorder") }}', data, 'PUT');
        ajax.done(function(data) {
            if (data.status) {
                module.notify('Items reordered successfully.', 'success');
                setTimeout(function(){
                    window.location.href = '{{ action("Admin\\Core\\ModuleController@index") }}';
                }, 1500);
            }else{
                module.notify(data.message, 'error');
            }
        });
    });
//    var list = document.getElementById('reorder');
//    Sortable.create(list);
});
</script>
@stop
