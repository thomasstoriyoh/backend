@extends('admin.layouts.master')

@section('css')
<style>
    .datatable-scroll {height: auto;}
</style>
@stop

@section('content')
<div id="page-title">
    <h2>Settings</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Configure</a></li>
            <li>Settings</li>
        </ul>
    </div>
    <p>Delight UI comes packed with a lot of options.</p>
</div>
{!! Form::open([
    'action' => 'Admin\Core\ModuleController@postSettings',
    'method' => 'PUT',
    'class' => 'form',
    'id' => 'ModuleForm'
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Maker Checker Module Listing
        </h3>
        
        <div class="example-box-wrapper">
            <div class="table-responsive">
                <table id="listing-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Module</th>
                            <th width='20%'>Maker/Cheker</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($modules as $key => $module)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $module->title }}</td>
                            <td>
                                <input type="checkbox" name="mc_status" class="input-switch-alt" id="status-{{ $module->id }}" value="{{ $module->id }}" {{ $module->maker_checker == 'On' ? 'checked' : '' }}>
                                <input type="hidden" class="status" name="maker_checker[]" value="{{ $module->maker_checker == 'On' ? $module->id : '' }}">
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Dashboard Widget Option (Max. 3)
        </h3>

        <div class="example-box-wrapper">
            <div class="table-responsive">
                <select class="chosen-select-settings form-control" multiple data-placeholder="Dashboard Modules..." name="widget_modules[]">
                    @foreach($modules as $key => $module)
                        @if($module->slug == 'admin' OR $module->has_details != 'Yes')
                            @continue;
                        @endif
                        <option value="{{ $module->id }}" {{ $module->widget ? 'selected': '' }}>{{ $module->title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" data-loading-text="Please wait..." data-normal-text="Submit Changes" class="btn loading-button btn-primary">Submit Changes</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Core\DashboardController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script>
$(function(){

    $(".chosen-select-settings").chosen({
        max_selected_options: 3
    });

    $("#listing-table").DataTable({
        dom: '<"datatable-header"><t>'+
                    '<"datatable-footer">',
        columns: [
            {sortable: false},
            {},
            {sortable: false},
        ],
        order: [
            [0, 'asc']
        ],
        lengthMenu: [[-1], ['All']]
    });
    
    
    $(document).on("change", "input.input-switch-alt", function(){
        var obj = $(this);
        if (obj.attr('checked') === undefined) {
            module.confirm('Are you sure you want to disable maker/checker for this module? All pending items in the module will be approved by default.', function(isConfirm){
                if (! isConfirm) {
                    obj.attr('checked', true);
                    obj.next('div.switch-toggle').trigger('click');
                }
            });
        }
    });
    
    api.openPanel($("#mmenu-main-Configure"));
    api.setSelected($("li#mmenu-item-link-module"));
});
</script>
@stop
