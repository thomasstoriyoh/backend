@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Admin Log</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Configure</a></li>
            <li>Admin Log</li>
        </ul>
    </div>
    <p>Admin Log Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Admin Log
        </h3>
        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    var table = $("#listing-table").bcmTable({
        dom: '<"datatable-header"><"datatable-scroll"t>'+
        '<"datatable-footer"<"row"<"col-md-3"l><"col-md-3"i><"col-md-6"p>>>',
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Section", data: "title", sortable: false},
            { title: "User", data: "user", sortable: false},
            { title: "Item", data: "item", sortable: false},
            { title: "Action", data: "action", sortable: false},
            { title: "Date", data: "date", sortable: false},
        ],
        language: {lengthMenu: '_MENU_ per page'},
        "bFilter": false
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });
    
    api.openPanel($("#mmenu-main-Configure"));
    api.setSelected($("li#mmenu-item-link-log"));
});
</script>
@stop