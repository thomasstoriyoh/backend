@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Documentation Category</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\Documentation\DocumentationCategoryController@index') }}">Documentation Category</a></li>
            <li>{{ $category->exists ? 'Edit' : 'Add' }}</li>
        </ul>
    </div>
    <p>{{ $category->exists ? 'Edit' : 'Add' }} Blog Category</p>
</div>

{!! Form::model($category, [
    'method' => $category->exists ? 'PUT' : 'POST',
    'action' => $category->exists ?
        ['Admin\Documentation\DocumentationCategoryController@update', $category->id] : 'Admin\Documentation\DocumentationCategoryController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Basic Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter title.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Slug</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Unique URL Slug"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('slug', null, [
                                'class' => "form-control",
                                'placeholder' => "Slug",
                                'required' => true,
                                'pattern' => '/^[a-zA-Z0-9\-]+$/',
                                'data-parsley-required-message' => "Please enter slug.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Parent</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Parent Category"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('documentation_category_id', array_merge(['0' => 'Root'], $categories),
                            null, [
                                'class' => "form-control chosen-select",
                            ]) !!}
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $category->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $category->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\Documentation\DocumentationCategoryController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop
