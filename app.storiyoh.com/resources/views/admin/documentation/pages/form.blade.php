@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Documentation Pages</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
            <li class="blueText">Documentation</li>
            <li><a href="{{ action('Admin\Documentation\DocumentationPagesController@index') }}">Documentation Pages</a></li>
            <li>{{ $pages->exists ? 'Edit' : 'Add' }}</li>
        </ul>
    </div>
</div>

{!! Form::model($pages, [
    'method' => $pages->exists ? 'PUT' : 'POST',
    'action' => $pages->exists ?
        ['Admin\Documentation\DocumentationPagesController@update', $pages->id] :
        'Admin\Documentation\DocumentationPagesController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Basic Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter title.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Category</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('documentation_category_id', $category_list, null, [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter title.",
                            ]) !!}
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Description
        </h3>
        <div class="example-box-wrapper">
            {!! Form::textarea('description', null, [
                'class' => 'wysiwyg-editor-doc',
            ]) !!}
        </div>
    </div>
</div>

<div class="panel attachment-panel {{ empty($pages->attachment) ? '' : 'hide' }}">
    <div class="panel-body">
        <h3 class="title-hero">
            Attachment
        </h3>
        <div class="example-box-wrapper">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <span class="btn btn-primary btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    {!! Form::file('attachment_file') !!}
                </span>
                <div class="clearfix"></div>
                <span class="fileinput-filename"></span>
                <a href="#" class="close fileinput-exists mrg15T mrg10L" data-dismiss="fileinput" style="float: none">&times;</a>
            </div>
        </div>
    </div>
</div>

<div class="panel attachment-panel {{ empty($pages->attachment) ? 'hide' : '' }}">
    <div class="panel-body">
        <h3 class="title-hero">
            Existing Attachment
        </h3>
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-md-3">
                    <span>{{ $pages->attachment }}</span> <a class="close delete-attachment">&times;</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $pages->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $pages->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\BlogController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">;
$(function(){

    module.counter($('input[name="title"]'), 50);

    $('.wysiwyg-editor-doc').summernote({
        height: 250,
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['para', ['ol', 'ul']],
            ['table', ['table']],
            ['insert', ['link', 'picture']],
            ['misc', ['fullscreen', 'codeview']]
        ]
    });

    $("a.delete-attachment").click(function() {
        module.confirm('Are you sure you want to delete this attachment? ', function(isConfirm) {
            if (isConfirm) {
                var ajax = module.ajax('{{ action("Admin\\Documentation\\DocumentationPagesController@deleteImage", $pages->id) }}', {
                    type: 'File',
                }, 'DELETE');

                ajax.done(function(data){
                    if(data.status) {
                        $("div.attachment-panel").toggleClass('hide');
                    }else{
                        module.notify(data.message, 'error');
                    }
                });
            }
        });

    });
});
</script>
@stop