@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Blog</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\BlogController@index') }}">Blog</a></li>
            <li><a href="{{ action('Admin\BlogController@show', $blog->id) }}">{{ $blog->title }}</a></li>
            <li>Gallery</li>
        </ul>
    </div>
    <p>Blog Details</p>

    <div class="dt-tab">
        <div class="example-box-wrapper">
            <ul class="nav-responsive nav nav-tabs">
                <li class="active"><a href="javascript:void(0);">Details</a></li>
                <li><a href="{{ action('Admin\BlogController@gallery', $blog->id) }}">Gallery</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">Detail Page</h3>
        <div class="example-box-wrapper">

            <div class="row">
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Title</label>
                        </div>
                        <div class="cg-content">
                            {{ $blog->title }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>URL Slug</label>
                        </div>
                        <div class="cg-content">
                            {{ $blog->slug }}
                        </div>
                    </div>
                </div>
            </div>

            @unless(empty($blog->short_description))
            <div class="row">
                <div class="col-sm-12">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Short Description</label>
                        </div>
                        <div class="cg-content">
                            {{ $blog->short_description }}
                        </div>
                    </div>
                </div>
            </div>
            @endunless

            <div class="row">
                <div class="col-sm-12">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Description</label>
                        </div>
                        <div class="cg-content">
                            {!! $blog->description !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Publish Status</label>
                        </div>
                        <div class="cg-content">
                            {{ $blog->status }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Approval Status</label>
                        </div>
                        <div class="cg-content">
                            {{ $blog->admin_status }}
                        </div>
                    </div>
                </div>
            </div>

            @unless(empty($blog->category_names) && empty($blog->tag_names))
            <div class="row">
                @unless(empty($blog->category_names))
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Categories</label>
                        </div>
                        <div class="cg-content">
                            {{ implode(", ", $blog->category_names) }}
                        </div>
                    </div>
                </div>
                @endunless
                @unless(empty($blog->tag_names))
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Tags</label>
                        </div>
                        <div class="cg-content">
                            {{ implode(", ", $blog->tag_names) }}
                        </div>
                    </div>
                </div>
                @endunless
            </div>
            @endunless

        </div>
    </div>
</div>

@unless(empty($blog->youtube_id) && empty($blog->image))
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Image & Video
        </h3>
        <div class="example-box-wrapper">
            <div class="row">
                @unless(empty($blog->youtube_id))
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Youtube Video</label>
                        </div>
                        <div class="cg-content">
                            <iframe src="http://www.youtube.com/embed/{{  $blog->youtube_id }}?autoplay=0" frameborder="0" width="100%" style="height: 320px;"></iframe>
                        </div>
                    </div>
                </div>
                @endunless
                @unless($blog->image)
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Image</label>
                        </div>
                        <div class="cg-content">
                            <img src="{{ asset('uploads/blog/thumbs/640_360_'.$blog->image) }}" class="img-responsive">
                        </div>
                    </div>
                </div>
                @endunless
            </div>
        </div>
    </div>
</div>
@endunless

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Meta Information
        </h3>
        <div class="example-box-wrapper">

            <div class="row">
                <div class="col-sm-12">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Meta Title</label>
                        </div>
                        <div class="cg-content">
                            {{ $blog->meta_title }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Meta Keywords</label>
                        </div>
                        <div class="cg-content">
                            {{ $blog->meta_keyword }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Meta Description</label>
                        </div>
                        <div class="cg-content">
                            {{ $blog->meta_description }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Visit History
        </h3>
        <div class="example-box-wrapper">
        <table class="table table-bordered responsive no-wrap" id="listing-table">
            <thead class="cf">
            <tr>
                <th>Date</th>
                <th>Visit Count</th>
            </tr>
            </thead>
            <tbody>
            @foreach($blog->view_entries as $view)
            <tr>
                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $view->view_date)->format('jS M Y') }}</td>
                <td>{{ $view->count }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#listing-table').DataTable({
                dom: '<"datatable-header"><"datatable-scroll"t>'+
                '<"datatable-footer"<"row"<"col-sm-3"l><"col-sm-3"i><"col-sm-3"<"clearfix"f>><"col-sm-3"p>>>',
                language: {lengthMenu: '_MENU_ per page'},
                responsive: true,
                "info":     false,
                "bFilter": false,
                "bInfo": false
            });
        });
    </script>
@stop
