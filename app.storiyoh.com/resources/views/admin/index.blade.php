@extends("admin.layouts.master")

@section("content")
<!--Page Title-->
<div id="page-title">
    <h2>Dashboard</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li class="blueText">Catalyst WCMS</li>
            <li>Dashboard</li>
        </ul>
    </div>
    <p>Delight UI comes packed with a lot of options</p>
</div>

@include("admin.reports.google.top_google_chart")

@unless(count($module_view_data) < 1)
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
                Module Views History
            </h3>
        <div class="example-box-wrapper">
            <div class="row">
                <?php
                    $chart_index = 1;
                    $color_array = ['', 'bg-blue-alt', 'bg-green', 'bg-orange'];
                ?>
                @foreach($module_view_data as $module_name => $module_array_data)
                <div class="col-md-{{ 12/count($module_view_data) }}">
                    <div class="dashboard-box {{ $color_array[$chart_index] }}">
                        <div class="content-wrapper">
                            <div id="drawChart{{ $chart_index }}" style="width:100%;height:180px">
                                <div class="loading-stick" style="position:absolute;margin-top: 60px;width:90%;text-align:center;">
                                    <div class="stick bg-white"></div>
                                    <div class="stick bg-white"></div>
                                    <div class="stick bg-white"></div>
                                    <h1>Please Wait...</h1>
                                </div>
                            </div>
                        </div>
                        <div class="button-pane">
                            <div class="size-md float-left heading">
                                {{ $module_name }}
                            </div>
                        </div>
                    </div>
                </div>
                <?php $chart_index++; ?>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endunless

@if(Auth::guard('admin')->user()->type == "SA")
    <div class="row">
        <div class="col-md-4">
            <div class="content-box">
                <h3 class="content-box-header bg-default">Content Statistics</h3>
                <div class="content-box-wrapper pad0A">
                    <div class="mrg20A">
                        <div class="row">
                            <div class="text-center pad0A">
                                <div style="width:276px;height:276px;display:inline-block;">
                                    <div id="drawChart5" style="width:100%;height:100%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table remove-background cs-table">
                        <tbody>
                            @foreach($dashboardModule as $key=>$item)
                            @if($key > 0)
                            <tr>
                                <td class="text-left">
                                    <div class="badge mrg10L badge-small mrg5R bg{{$key}}"></div>
                                    {{$item[0]}}
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel">
                <div class="panel-body">
                    <h3 class="title-hero">
                        Recent Activity
                        <a href="{{ action('Admin\Core\LogController@index') }}" class="pull-right">View More</a>
                    </h3>
                    <div class="example-box-wrapper">
                        <div class="table-responsive">
                            <table id="listing-table" class="table table-striped table-bordered responsive nowrap" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Section</th>
                                    <th>User</th>
                                    <th>Item</th>
                                    <th>Action</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($logs_data as $key=>$data)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $data->module->title}}</td>
                                        <td>{{ $data->user->first_name}} {{ $data->user->last_name}}</td>
                                        <td>{{ $data->title }}</td>
                                        <td>{{ $data->type }}</td>
                                        <td>{{ $data->created_at->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif	
@stop

@section("js")
<!-- Google Chart Library -->
<script>var dashboardModuleData = {!! json_encode($dashboardModule) !!}; var module_view_data = {!! json_encode($module_view_data) !!}</script>
 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/analytics/top-chart-data.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/dashboard/google-chart-dashboard.js') }}"></script>

<!-- Bootstrap Daterangepicker -->
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker-demo.js') }}"></script>

<script type="text/javascript">
    <?php $key = 0; ?>
    @foreach($module_view_data as $small_data)
        @if(!empty($small_data))
            var module_data{{ $key+1 }} = {!! json_encode($small_data) !!};
            function drawChart{{ $key+1 }} () {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Date');
                data.addColumn('number', 'Views');
                data.addRows(module_data{{ $key+1 }});
                var options = {
                    backgroundColor: 'transparent',
                    chartArea: {
                        backgroundColor: 'transparent'
                    },
                    legend: {
                        position: 'none'
                    },
                    colors: ["#fff"],
                    vAxis: {
                        baselineColor: 'transparent',
                        gridlines: {
                            color: "transparent"
                        },
                        textStyle: {
                            color: '#FFF'
                        }
                    },
                    hAxis: {
                        textStyle: {
                            color: '#FFF'
                        }
                    }
                };
                var chart = new google.charts.Line(document.getElementById('drawChart{{ $key+1 }}'));
                chart.draw(data, google.charts.Line.convertOptions(options));
            }
            google.charts.setOnLoadCallback(drawChart{{ $key+1 }});

            $(window).resize(function () {
                drawChart{{ $key+1 }}();
            });
        <?php $key++; ?>
        @endif
    @endforeach
</script>
<script>
$(document).ready(function() {
    $('#listing-table').DataTable({
    	dom: '<"datatable-header"><"datatable-scroll"t>'+
                    '<"datatable-footer"<"row"<"col-md-3"l><"col-md-3"i><"col-md-3"<"clearfix"f>><"col-md-3"p>>>',
        language: {lengthMenu: '_MENU_ per page'},
        responsive: true,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "bFilter": false, 
        "bInfo": false
    });
});
</script>
@stop