<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Content Management System </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/admin/css/admin-login.css') }}?v=2.0.1" charset="utf-8">
    <link rel="stylesheet" href="{{ url('assets/admin/css/overwrite.css') }}?v=2.0.1">
    <!-- JS Core -->
    <script type="text/javascript" src="{{ url('assets/admin/js/admin-login.js') }}?v=2.0.1"></script>
    
    @yield('css')
</head>
<body>

@yield('content')
<script type="text/javascript">
    var module_actions = {};
</script>
<script type="text/javascript" src="{{ url("assets/admin/js/core/app.js") }}?v=2.0.1"></script>
@yield('js')
</body>
</html>
