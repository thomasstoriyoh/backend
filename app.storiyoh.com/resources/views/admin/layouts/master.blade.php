<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Content Management System </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/admin/css/admin.css') }}?v=1" charset="utf-8">
    <link rel="stylesheet" href="{{ url('assets/admin/css/overwrite.css') }}">
    <!-- Overwrite Css -->
    <style type="text/css">
        li.search-field {width:100% !important;}
        .select2-container {width:100% !important;}
        input.select2-search__field {width:100% !important;}

        #menuLeft{visibility:hidden;}
        .pace-done #menuLeft{visibility:visible;}
        body{display: none;}
        .pace > * {display: block;}
    </style>

    <!-- JS Core -->
    <script type="text/javascript" src="{{ url('assets/admin/js/admin-head.js') }}"></script>
    <script type="text/javascript">
        var api;
        $(function() {
            $('nav#menuLeft').mmenu({
                extensions: ['effect-slide-menu', 'pageshadow', 'theme-dark'],
                searchfield: true,
                counters: true,
                navbar: {
                    title: 'Dashboard'
                },
                navbars: [{
                    position: 'top',
                    content: ['searchfield']
                }, {
                    position: 'top',
                    content: [
                        'prev',
                        'title'
                    ]
                }]
            });
            
            api = $('nav#menuLeft').data('mmenu');
            @if($sidebar_selected_panel)
                api.openPanel($("#mmenu-item-panel-{{ $sidebar_selected_panel }}"));
            @elseif($sidebar_selected_type)
                api.openPanel($("#mmenu-main-{{ $sidebar_selected_type }}"));
            @endif
            @if($sidebar_selected_item)
                api.setSelected($("li#mmenu-item-link-{{ $sidebar_selected_item }}"));
            @endif
        });
        
        $(window).load(function() {
            $("body").fadeIn(700);
        });
    </script>

    @yield('css')
</head>
<body>
    <div id="sb-site">
    <div id="page-wrapper">
        
        <div id="page-content-wrapper">
            <div id="page-content">
                @include('admin.partials.header')
                
                @yield('content')
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript" src="{{ url('assets/admin/js/admin.js') }}"></script>
<script type="text/javascript">
    /* Input switch */

    $(function() { "use strict";
//        $('.input-switch').bootstrapSwitch();
    });

    /* Textarea autoresize */

    $(function() { "use strict";
        $('.textarea-autosize').autosize();
    });

    $(function(){
        $(".select-2-tags").select2({
            tags: true,
            placeholder: "Type anything or select form the list",
            createTag: function (params) {
                var term = $.trim(params.term);

                if (term === '') {
                    return null;
                }

                return {
                    id: '*#$*#$'+term,
                    text: term,
                    newTag: true // add additional parameters
                }
            }
        });
    });

    /* Datepicker bootstrap */

    $(function() { "use strict";
        $('.bootstrap-datepicker').bsdatepicker({
            format: 'yyyy-mm-dd'
        });
    });

$(function() {
    $(".chosen-select").chosen();

    $(".chosen-search").append('<i class="glyph-icon icon-search"></i>');
    $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');

});

    /* Responsive tabs */
    $(function() {
        "use strict";
        $('.nav-responsive').tabdrop();
    });

    /* WYSIWYG editor */
    $(function() { "use strict";
        $('.wysiwyg-editor').summernote({
            height: 250,
            toolbar: [
                ['style', ['bold', 'underline']],
                ['para', ['ol', 'ul', 'link', 'picture', 'fullscreen', 'codeview']]
            ]
        });
    });
</script>
    @yield('preJS')
<!--Custom Part by: Kazi-->
<script type="text/javascript">
@if($routeRequired)
    var module_actions = {
        json: '{{ action("{$currentController}@ajaxIndex") }}',
        publish: '{{ action("{$currentController}@publish") }}',
        draft: '{{ action("{$currentController}@draft") }}',
        delete: '{{ action("{$currentController}@delete") }}',
        approve: '{{ action("{$currentController}@approve") }}',
        unapprove: '{{ action("{$currentController}@unapprove") }}',
        upload: '{{ action("{$currentController}@upload")  }}',
        caption: '{{ action("{$currentController}@caption")  }}',
        csrf_token: '{{ csrf_token() }}',
    };
@endif

$(function(){

    $(window).load(function(){
        @if(session('success'))
            module.notify("{{ session('success') }}", 'success');
        @endif
        @if(session('error'))
            module.notify("{{ session('error') }}", 'error');
        @endif
    });

    $(".xeditable-field").editable({
        mode: 'inline',
        url: module_actions.caption
    });
});

</script>

<script type="text/javascript" src="{{ url('assets/admin/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/admin/js/core/bcmTable.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/admin/js/core/bcmUpload.js') }}"></script>
@yield('js')
</body>
</html>
