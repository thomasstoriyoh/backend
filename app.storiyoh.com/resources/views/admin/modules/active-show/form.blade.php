@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $active_show->exists ? 'Update' : 'Create' }} Active Show</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\ActiveShowController@index') }}">Active Shows</a></li>
            <li>{{ $active_show->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $active_show->exists ? 'Update' : 'Create' }} Active Show</p>
</div>

{!! Form::model($active_show, [
    'method' => $active_show->exists ? 'PUT' : 'POST',
    'action' => $active_show->exists ?
        ['Admin\ActiveShowController@update', $active_show->id] : 'Admin\ActiveShowController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Shows</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="All Shows"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('show_id', [], null, [
                                'class' => "form-control select-2-show",
                                'multiple' => false,
                                'required' => true,
                                'data-parsley-required-message' => "Please select show.",
                                'data-parsley-errors-container'=> "#show_err_id",
                                'id' => "show_id",
                                'style' => ''
                            ]) !!}
                        </div>
                        <div id="show_err_id"></div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\ActiveShowController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    $(function() {
        $(".select-2-show").select2({
            placeholder: 'Search for a show',
            ajax: {
                url: "{{ action("Admin\\ActiveShowController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });
</script>
@stop