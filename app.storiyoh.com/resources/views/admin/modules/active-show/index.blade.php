@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Active Shows</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li>Active Shows</li>
        </ul>
    </div>
    <p>Active Show Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Active Show Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.active-shows')
                    <a class="btn btn-default" href="{{ action('Admin\ActiveShowController@create') }}">ADD ITEM</a>
                @endCanI
                @CanI(['update.category', 'delete.category'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('delete.category')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="delete">Delete</a></li>
                        @endCanI
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    var table = $("#listing-table").bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Image", data: "image", sortable: false},
            { title: "Show", data: "show_id", sortable: false},            
            { title: "Added Date", data: "created_at", sortable: false}            
        ]
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.delete", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to delete these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.delete(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items deleted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });        
});
</script>
@stop