@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $chart->exists ? 'Update' : 'Create' }} Collections</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\ChartController@index') }}">Collections</a></li>
            <li>{{ $chart->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $chart->exists ? 'Update' : 'Create' }} Collection</p>
</div>

{!! Form::model($chart, [
    'method' => $chart->exists ? 'PUT' : 'POST',
    'action' => $chart->exists ?
        ['Admin\ChartController@update', $chart->id] : 'Admin\ChartController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter title."                                
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Date</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('date', null, [
                                'class' => "form-control bootstrap-datepicker",
                                'placeholder' => "Date",
                                'required' => true,
                                'data-date-format' => "yyyy-mm-dd",
                            ]) !!}
                        </div>
                    </div>
                </div>                
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Source (URL)</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="URL that this will point to."></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('source_url', null, [
                                'class' => "form-control",
                                'placeholder' => "Link",                                
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Keywords</label>
                                <div class="fm-tooltip">
                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Related Keywords for the chart."></i>
                                </div>
                            </div>
                            <div class="fm-control">
                                <select  name="keywords[]" id="keywords" class="select-2-show form-control" multiple data-placeholder="Select multiple" required data-parsley-required-message="Please select keyword." data-parsley-errors-container="#keyword_err_id">
                                    @foreach($tag_options as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                </select> 

                                {{--  <select name="keywords[]" id="keywords" class="select-2-tags form-control" multiple data-placeholder="Select multiple" required data-parsley-required-message="Please select keyword." data-parsley-errors-container="#keyword_err_id">
                                    @foreach($tag_options as $key => $module)
                                        <option value="{{ $module }}" {{ in_array($module, $keywordArray) ? 'selected': '' }}>{{ $module }}</option>
                                    @endforeach
                                </select>                                  --}}
                            </div>
                            <div id="keyword_err_id"></div>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Description</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('description', null, [
                                'class' => "form-control textarea-no-resize",
                                'placeholder' => "Description",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter description.",
                                'rows' => 6
                            ]) !!}
                        </div>
                    </div>
                </div>	
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">Podcasts</h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">                                       
                    <div class="fm-control">
                        <div class="fm-control">
                            <select  name="show_id[]" id="show_id" class="select-2-show form-control" multiple data-placeholder="Select multiple" data-parsley-required-message="Please select podcast." data-parsley-errors-container="#show_err_id">
                                @foreach($podcastArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                            </select>                                
                        </div>                                                
                    </div>
                    <div id="show_err_id"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.partials.crop_upload', ['title' => 'CSV', 'status' => true, 'tooltip' => ''])

@if(!empty($chart->image))
    <div class="panel image-panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Existing Image
            </h3>
            <div class="example-box-wrapper">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <img src="{{ $chart->getImage(200) }}" alt="" style="box-shadow:0 10px 16px 0 #CCC">                
                </div>            
            </div>
        </div>
    </div>
@endif

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $chart->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $chart->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\ChartController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    $(function() {
        $("#show_id").select2({
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\ActiveShowController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });

        $("#keywords").select2({
            placeholder: 'Search for a keyword',
            ajax: {
                url: "{{ action("Admin\\ChartController@search_keyword") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });
</script>
<script type="text/javascript">
    var up = {
        data_csv: {
            object: null,
            uploaded: false
        }
    };
    /**
    * Uploader start.
    * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
    * If uploading resource is not image then it will ignore the crop size.
    *
    */

   $upQueue = $(".file-uploader");

   var uploader = $upQueue.bcmUpload({
       filters: { // Mime Types to be allowed to select.
           mime_types: [{
               title: "CSV files",
               extensions: "csv"
           }]
       },
       max_files: 1, // How many uploads allowed?
       input_name: 'data_csv[]',
       required: false, // Is the image upload mandatory?
       multipart_params: {resource: 'File'} // You can set it to your choice. You have to check for it in the controller to upload accordingly.
   });

    up.data_csv.object = uploader;

    uploader.bind('UploadComplete', function (upl, files) { // What happens when all the uploads are done.
         up.data_csv.uploaded = true;
         $("#moduleForm").submit();
    });

    $("#moduleForm").submit(function () {
     var $obj = $(this);     
     if (up.data_csv.object.files.length > 0 && up.data_csv.uploaded == false) {
         up.data_csv.object.start();
         return false;
     } else {
        return true;
     }
   });
</script>
@stop