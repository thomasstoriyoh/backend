@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $discovermasthead->exists ? 'Update' : 'Create' }} Discover Masthead</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\DiscoverMastheadController@index') }}">Discover Masthead</a></li>
            <li>{{ $discovermasthead->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $discovermasthead->exists ? 'Update' : 'Create' }} Discover Masthead</p>
</div>

{!! Form::model($discovermasthead, [
    'method' => $discovermasthead->exists ? 'PUT' : 'POST',
    'action' => $discovermasthead->exists ?
        ['Admin\DiscoverMastheadController@update', $discovermasthead->id] : 'Admin\DiscoverMastheadController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter title."                                
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 apppage">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Type</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Podcast, Playlist, Smart Playlist, Collection"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('type', ['' => 'Select' ,'Podcast' => 'Podcast', 'Playlist' => 'Playlist', 'Smart Playlist' => 'Smart Playlist', 'Collection' => 'Collection'], null,[
                                'class' => 'chosen-select form-control',
                                'id' => 'select_apppage',
                                'required' => true,
                                'data-parsley-required-message' => "Please select type.",
                            ]) !!}
                        </div>
                    </div>
                </div>

                
                
            </div>
            
        </div>
       <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Podcast List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Podcast List"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="show_id" id="podcast" class="select-2-show form-control" data-placeholder="Select multiple" data-parsley-required-message="Please select podcast." data-parsley-errors-container="#show_err_id">
                                    @foreach($podcastArray as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="show_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Episode List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Episode List"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="episode_id" id="episode" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Episode." data-parsley-errors-container="#episode_err_id">
                                    @foreach($episode as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="episode_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 playlist_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Playlist List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Playlist List"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="playlist_id" id="playlist" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Playlist." data-parsley-errors-container="#playlist_err_id">
                                    @foreach($playlist as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="playlist_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 smart_playlist_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Smart Playlist List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Smart Playlist List"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="smart_playlist_id" id="smart_playlist" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Smart Playlist." data-parsley-errors-container="#smart_playlist_err_id">
                                    @foreach($smartplaylist as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach   
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="smart_playlist_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 collection_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Collection List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Collection List"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="collection_id" id="collection" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Collection." data-parsley-errors-container="#collection_err_id">
                                    @foreach($collection as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="collection_err_id"></div>
                    </div>
                </div>
            </div>                          
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $discovermasthead->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $discovermasthead->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\DiscoverMastheadController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')


<script type="text/javascript">
    $(function() {
        $("#podcast").select2({
            allowClear: true,
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\DiscoverMastheadController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    $(document).on("change", "#podcast", function(){
        episode(this.value);
        episode_reset(); 
    });

    function episode_reset(){
        $("#episode").val(null).trigger("change");
    }

    @if($discovermasthead->exists)
        $('#podcast').trigger('change');
        $('#episode').val('{{key($episode)}}').trigger('change');
        // console.log('{{$discovermasthead->episode_id}}')
    @endif

    function episode(value) {
        $("#episode").select2({
            allowClear: true,
            placeholder: 'Search for a episode',
            ajax: {
                url: "{{ action("Admin\\DiscoverMastheadController@search_episode") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    select_type: value,
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 0                        
        });
    }

    // playlist
    $(function() {
        $("#playlist").select2({
            allowClear: true,
            placeholder: 'Search for a playlist',
            ajax: {
                url: "{{ action("Admin\\DiscoverMastheadController@search_playlist") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    // smart_playlist
    $(function() {
        $("#smart_playlist").select2({
            allowClear: true,
            placeholder: 'Search for a smart playlist',
            ajax: {
                url: "{{ action("Admin\\DiscoverMastheadController@search_smart_playlist") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    // collection
    $(function() {
        $("#collection").select2({
            allowClear: true,
            placeholder: 'Search for a collection',
            ajax: {
                url: "{{ action("Admin\\DiscoverMastheadController@search_collection") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    // select event
    $(document).on("change", "#select_apppage", function(){
        select_event(this.value);
    });

    $(document).ready(function(){
        var value = $('#select_apppage').val();
        // console.log(value)
        select_event(value);
        // episode_reset();
    });

    function select_event(value)
    {
        if (value == 'Podcast') {
            $('.podcast_data').show();
                $("#podcast").attr('data-parsley-required', 'true').parsley();
            // $('#external').hide();
            //     $("#externallink").removeAttr('data-parsley-required').parsley().destroy();

            $('.playlist_data').hide();
                $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
            $('.smart_playlist_data').hide();
                $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
            $('.collection_data').hide();
                $("#collection").removeAttr('data-parsley-required').parsley().destroy();
        }
        else if (value == 'Playlist') {
            $('.playlist_data').show();
                $("#playlist").attr('data-parsley-required', 'true').parsley();
            // $('#external').hide();
            //     $("#externallink").removeAttr('data-parsley-required').parsley().destroy();

            $('.podcast_data').hide();
                $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
            $('.smart_playlist_data').hide();
                $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
            $('.collection_data').hide();
                $("#collection").removeAttr('data-parsley-required').parsley().destroy();
        }
        else if (value == 'Smart Playlist') {
            $('.smart_playlist_data').show();
                $("#smart_playlist").attr('data-parsley-required', 'true').parsley();
            // $('#external').hide();
            //     $("#externallink").removeAttr('data-parsley-required').parsley().destroy();

            $('.podcast_data').hide();
                $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
            $('.playlist_data').hide();
                $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
            $('.collection_data').hide();
                $("#collection").removeAttr('data-parsley-required').parsley().destroy();
        }
        else if (value == 'Collection') {
            $('.podcast_data').hide();
                $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
            // $('#external').hide();
            //     $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
            $('.playlist_data').hide();
                $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
            $('.smart_playlist_data').hide();
                $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
            $('.collection_data').show();
                $("#collection").attr('data-parsley-required', 'true').parsley();
        }
        else{
            // $('#external').hide();
            //     $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
            // $('.apppage').hide();
            //     $("#select_apppage").removeAttr('data-parsley-required').parsley().destroy();
            $('.podcast_data').hide();
                $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
            $('.playlist_data').hide();
                $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
            $('.smart_playlist_data').hide();
                $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
            $('.collection_data').hide();
                $("#collection").removeAttr('data-parsley-required').parsley().destroy();
        }
    }

</script>


<script type="text/javascript">
var item_exists = {{ $discovermasthead->exists ? 'false' : 'true' }};
$(function(){

    module.counter($('input[name="title"]'), 50);

    /**
     * Uploader start.
     * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
     * If uploading resource is not image then it will ignore the crop size.
     *
     */

    $upQueue = $(".file-uploader");
    var crop_size = [640, 480];

    
    
    $('#moduleForm').submit(function() {
        if (uploader.files.length > 0 && uploaded == false) {
            uploader.start();

            return false;
        } else if (uploader.getOption('required') && uploader.files.length < 1) {
            module.notify('Please select some image(s) to proceed', 'error');

            return false;
        } else {
            return true;
        }
    });

//    module.loadImageCropFunction(crop_size, uploader);

    /**
     * Upload section end.
     */
 
});
</script>
@stop