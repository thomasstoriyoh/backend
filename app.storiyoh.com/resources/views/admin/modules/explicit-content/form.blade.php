@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $explicit_content->exists ? 'Update' : 'Create' }} Explicit Content</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\ExplicitContentController@index') }}">Explicit Content</a></li>
            <li>{{ $explicit_content->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $explicit_content->exists ? 'Update' : 'Create' }} Explicit Content</p>
</div>

{!! Form::model($explicit_content, [
    'method' => $explicit_content->exists ? 'PUT' : 'POST',
    'action' => $explicit_content->exists ?
        ['Admin\ExplicitContentController@update', $explicit_content->id] : 'Admin\ExplicitContentController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter title.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>




<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $explicit_content->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $explicit_content->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\ExplicitContentController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
var item_exists = {{ $explicit_content->exists ? 'false' : 'true' }};
$(function(){

    module.counter($('input[name="title"]'), 50);

    /**
     * Uploader start.
     * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
     * If uploading resource is not image then it will ignore the crop size.
     *
     */

    $upQueue = $(".file-uploader");
    var crop_size = [640, 480];

    var uploader = $upQueue.bcmUpload({
        filters: { // Mime Types to be allowed to select.
            mime_types: [{
                title: "Image files",
                extensions: "jpg,gif,png,jpeg"
            }]
        },
        enable_crop: false, // Is cropping required or not? That's it. Cropping will be there for you.
        max_files: 1, // How many uploads allowed?
        required: item_exists, // Is the image upload mandatory?
        multipart_params: {resource: 'Image'}, // You can set it to your choice. You have to check for it in the controller to upload accordingly.
        min_dimention: { // This section is only required for image resources. It will be ignored for other files even if you set it.
            width: crop_size[0],
            height: crop_size[1]
        }
    });

    var uploaded = false;
    uploader.bind('UploadComplete', function (up, files) { // What happens when all the uploads are done.
        uploaded = true;
        $("#moduleForm").submit();
    });
    
    $('#moduleForm').submit(function() {
        if (uploader.files.length > 0 && uploaded == false) {
            uploader.start();

            return false;
        } else if (uploader.getOption('required') && uploader.files.length < 1) {
            module.notify('Please select some image(s) to proceed', 'error');

            return false;
        } else {
            return true;
        }
    });

//    module.loadImageCropFunction(crop_size, uploader);

    /**
     * Upload section end.
     */

    
});
</script>
@stop