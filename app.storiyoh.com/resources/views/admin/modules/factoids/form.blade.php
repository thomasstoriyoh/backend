@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $factoid->exists ? 'Update' : 'Create' }} Factoids</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\FactoidsController@index') }}">Factoids</a></li>
            <li>{{ $factoid->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $factoid->exists ? 'Update' : 'Create' }} Factoids</p>
</div>

{!! Form::model($factoid, [
    'method' => $factoid->exists ? 'PUT' : 'POST',
    'action' => $factoid->exists ?
        ['Admin\FactoidsController@update', $factoid->id] : 'Admin\FactoidsController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Factoid</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Factoid"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            
                            {!! Form::textarea('title', null, [
                                'class' => "form-control textarea-no-resize",
                                'placeholder' => "Factoid",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter Factoid.",
                                'data-parsley-minlength' => '[3]',
                                
                                'rows' => 3
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Publish Date</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('date_appear', null, [
                                'class' => "form-control bootstrap-datepicker",
                                'placeholder' => "Publish Date",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter Publish Date",
                                'data-date-format' => "yyyy-mm-dd",
                            ]) !!}
                        </div>
                    </div>
                </div>

                
                
            </div>
            
        </div>
       <div class="example-box-wrapper">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Podcast List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Podcast List"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="" id="podcast" class="select-2-show form-control" data-placeholder="Select" data-parsley-required-message="Please select podcast." data-parsley-errors-container="#show_err_id" >
                                    @foreach($podcastArray as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="show_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Episode List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Episode List"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="episode_id" id="episode" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Episode." data-parsley-errors-container="#episode_err_id" >
                                    @foreach($episode as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="episode_err_id"></div>
                    </div>
                </div>
                                           
            </div>                            
        </div>
    </div>
</div>




<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $factoid->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $factoid->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\FactoidsController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')


<script type="text/javascript">
    $(function() {
        $("#podcast").select2({
            allowClear: true,
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\FactoidsController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    $(document).on("change", "#podcast", function(){
        episode(this.value);
        episode_reset(); 
    });

    function episode_reset(){
        $("#episode").val(null).trigger("change");
    }

    @if($factoid->exists)
        $('#podcast').trigger('change');
        $('#episode').val('{{$factoid->episode_id}}').trigger('change');
    @endif

    function episode(value) {
        $("#episode").select2({
            allowClear: true,
            placeholder: 'Search for a episode',
            ajax: {
                url: "{{ action("Admin\\FactoidsController@search_episode") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    select_type: value,
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 0                        
        });
    }

</script>


<script type="text/javascript">
var item_exists = {{ $factoid->exists ? 'false' : 'true' }};
$(function(){

    module.counter($('input[name="title"]'), 50);

    /**
     * Uploader start.
     * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
     * If uploading resource is not image then it will ignore the crop size.
     *
     */

    $upQueue = $(".file-uploader");
    var crop_size = [640, 480];

    var uploader = $upQueue.bcmUpload({
        filters: { // Mime Types to be allowed to select.
            mime_types: [{
                title: "Image files",
                extensions: "jpg,gif,png,jpeg"
            }]
        },
        enable_crop: false, // Is cropping required or not? That's it. Cropping will be there for you.
        max_files: 1, // How many uploads allowed?
        required: item_exists, // Is the image upload mandatory?
        multipart_params: {resource: 'Image'}, // You can set it to your choice. You have to check for it in the controller to upload accordingly.
        min_dimention: { // This section is only required for image resources. It will be ignored for other files even if you set it.
            width: crop_size[0],
            height: crop_size[1]
        }
    });

    var uploaded = false;
    uploader.bind('UploadComplete', function (up, files) { // What happens when all the uploads are done.
        uploaded = true;
        $("#moduleForm").submit();
    });
    
    $('#moduleForm').submit(function() {
        if (uploader.files.length > 0 && uploaded == false) {
            uploader.start();

            return false;
        } else if (uploader.getOption('required') && uploader.files.length < 1) {
            module.notify('Please select some image(s) to proceed', 'error');

            return false;
        } else {
            return true;
        }
    });

//    module.loadImageCropFunction(crop_size, uploader);

    /**
     * Upload section end.
     */

    
});

</script>
@stop