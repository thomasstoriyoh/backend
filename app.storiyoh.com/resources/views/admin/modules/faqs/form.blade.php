@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $faq->exists ? 'Update' : 'Create' }} FAQ</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\FaqController@index') }}">FAQ</a></li>
            <li>{{ $faq->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $faq->exists ? 'Update' : 'Create' }} FAQ Category</p>
</div>

{!! Form::model($faq, [
    'method' => $faq->exists ? 'PUT' : 'POST',
    'action' => $faq->exists ?
        ['Admin\FaqController@update', $faq->id] : 'Admin\FaqController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Question</label>
                            <div class="fm-tooltip">
                                <!-- <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i> -->
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Question",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter title.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
                 <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Category</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('faq_categories_id', ['' => 'Select...'] + @$category_list, null, [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'data-parsley-required-message' => "Please select Category.",
                                'data-parsley-errors-container'=> "#cat_err_id",
                                'id' => "faq_categories_id",
                            ]) !!}
                        </div>
                        <div id="cat_err_id"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 answer">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Answer</label>
                            <!-- <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div> -->
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('answer', null, [
                                'class' => "wysiwyg-editor form-control",
                                'placeholder' => "Answer",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div> 
          
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $faq->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $faq->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\FaqController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
var item_exists = {{ $faq->exists ? 'false' : 'true' }};
$(function(){

    module.counter($('input[name="title"]'), 100);

    /**
     * Uploader start.
     * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
     * If uploading resource is not image then it will ignore the crop size.
     *
     */

    

//    module.loadImageCropFunction(crop_size, uploader);

    /**
     * Upload section end.
     */


});
</script>
@stop