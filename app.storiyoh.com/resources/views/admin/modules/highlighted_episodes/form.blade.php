@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $highlighted_episode->exists ? 'Update' : 'Create' }} Marketplace Highlighted Episode</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="#" class="active">Marketplace</a></li>
            <li><a href="{{ action('Admin\HighlightedEpisodeController@index') }}">Highlighted Episode</a></li>
            <li>{{ $highlighted_episode->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $highlighted_episode->exists ? 'Update' : 'Create' }} Marketplace Highlighted Episode</p>
</div>

{!! Form::model($highlighted_episode, [
    'method' => $highlighted_episode->exists ? 'PUT' : 'POST',
    'action' => $highlighted_episode->exists ?
        ['Admin\HighlightedEpisodeController@update', $highlighted_episode->id] : 'Admin\HighlightedEpisodeController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 apppage">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Type</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Podcast, Episode, Standalone Episode"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('type', ['' => 'Select' ,'Podcast' => 'Series', 'Standalone' => 'Standalone Episode'], null,[
                                'class' => 'chosen-select form-control',
                                'id' => 'select_apppage',
                                'required' => true,
                                'data-parsley-required-message' => "Please select type.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Series List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Series List"></i>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="fm-control">
                                <select  name="show_id" id="podcast" class="select-2-show form-control" data-placeholder="Select..." data-parsley-required-message="Please select series." data-parsley-errors-container="#show_err_id">
                                @foreach($podcastArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                                </select>                                
                            </div>                                                
                        </div>
                        <div id="show_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 apppage episode_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Episode List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Episode List"></i>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="fm-control">
                                <select  name="episode_id" id="episode" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Episode." data-parsley-errors-container="#episode_err_id">
                                @foreach($episode as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                                </select>                                
                            </div>                                                
                        </div>
                        <div id="episode_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 standalone_episode">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Episode</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Standalone Episode List"></i>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="fm-control">
                                <select name="standaloneepisode_id" id="standaloneepisode" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select standalone episode." data-parsley-errors-container="#standaloneepisode_err_id">
                                @foreach($standalone as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                                </select>                                
                            </div>                                                
                        </div>
                        <div id="standaloneepisode_err_id"></div>
                    </div>
                </div>                                
            </div>                          
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $highlighted_episode->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $highlighted_episode->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\HighlightedEpisodeController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    $(function() {
        $("#podcast").select2({
            allowClear: true,
            placeholder: 'Search for a series',
            ajax: {
                url: "{{ action("Admin\\HighlightedEpisodeController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    $(document).on("change", "#podcast", function(){
        episode(this.value);
        episode_reset(); 
    });

    function episode_reset(){
        $("#episode").val(null).trigger("change");
    }

    @if($highlighted_episode->exists)
        $('#podcast').trigger('change');
        $('#episode').val('{{key($episode)}}').trigger('change');
    @endif

    function episode(value) {
        $("#episode").select2({
            allowClear: true,
            placeholder: 'Search for a episode',
            ajax: {
                url: "{{ action("Admin\\HighlightedEpisodeController@search_episode") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    select_type: value,
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 0                        
        });
    }

    // playlist
    $(function() {
        $("#standaloneepisode").select2({
            allowClear: true,
            placeholder: 'Search for a playlist',
            ajax: {
                url: "{{ action("Admin\\HighlightedEpisodeController@search_standalone_episode") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    // select event
    $(document).on("change", "#select_apppage", function(){
        select_event(this.value);
    });

    $(document).ready(function(){
        var value = $('#select_apppage').val();
        select_event(value);
    });

    function select_event(value)
    {
        if (value == 'Podcast') {
            $('.podcast_data').show();
            $("#podcast").attr('data-parsley-required', 'true').parsley();

            $('.episode_data').show();
            $("#episode").attr('data-parsley-required', 'true').parsley();

            $('.standalone_episode').hide();
            $("#standaloneepisode").removeAttr('data-parsley-required').parsley().destroy();

        } else if (value == 'Standalone') {
            $('.standalone_episode').show();
            $("#standaloneepisode").attr('data-parsley-required', 'true').parsley();
            
            $('.podcast_data').hide();
            $("#podcast").removeAttr('data-parsley-required').parsley().destroy(); 

            $('.episode_data').hide();
            $("#episode").removeAttr('data-parsley-required').parsley().destroy(); 

        } else {
            $('.podcast_data').hide();
            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();

            $('.episode_data').hide();
            $("#episode").removeAttr('data-parsley-required').parsley().destroy();

            $('.standalone_episode').hide();
            $("#standaloneepisode").removeAttr('data-parsley-required').parsley().destroy();            
        }
    }
</script>
@stop