@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Highlighted Episode</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="#" class="active">Marketplace</a></li>
            <li>Highlighted Episode</li>
        </ul>
    </div>
    <p>Marketplace Highlighted Episode Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Highlighted Episode Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.highlighted-episodes')
                    <a class="btn btn-default" href="{{ action('Admin\HighlightedEpisodeController@create') }}">ADD ITEM</a>
                @endCanI
                @CanI(['update.highlighted-episodes', 'delete.highlighted-episodes'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('update.highlighted-episodes')
                            <li><a href="javascript:void(0)" class="publish">Publish</a></li>
                            <li><a href="javascript:void(0)" class="draft">Draft</a></li>
                            <li><a href="{{ action('Admin\HighlightedEpisodeController@reorder') }}">Reorder</a></li>
                        @endCanI
                        @if(Auth::guard('admin')->user()->isSuperAdmin() && $module_details->maker_checker == 'On')
                            {{--<li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="approve">Approve</a></li>
                            <li><a href="javascript:void(0)" class="unapprove">Unapprove</a></li>--}}
                        @endif
                        @CanI('delete.highlighted-episodes')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="delete">Delete</a></li>
                        @endCanI
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
                <div class="rdtChosen">  
                    <select name="" multiple data-placeholder="Filter options..." 
                            class="chosen-select form-control" id="module_filter">
                        <optgroup label="Status">
                            <option value="status.Published">Published</option>
                            <option value="status.Draft">Draft</option>
                        </optgroup>
                        @if($module_details->maker_checker == 'On')
                        {{--<optgroup label="Approval Status">
                            <option value="admin_status.Approved">Approved</option>
                            <option value="admin_status.Pending">Pending</option>
                        </optgroup>--}}
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    var table = $("#listing-table").bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Title", data: "title"},
            { title: "Type", data: "type", sortable: false},
            { title: "Status", data: "status", sortable: false},
            @CanI('edit.promotion')
                // { title: "Options", data: "options", sortable: false}
            @endCanI
        ]
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.publish", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to publish these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.publish(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items published successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.draft", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to draft these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.draft(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items drafted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.delete", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to delete these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.delete(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items deleted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
});
</script>
@stop