@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $itunes_top_podcast->exists ? 'Update' : 'Create' }} iTunes Top Podcasts</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\ItunesTopPodcastController@index') }}">iTunes Top Podcasts</a></li>
            <li>{{ $itunes_top_podcast->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $itunes_top_podcast->exists ? 'Update' : 'Create' }} iTunes Top Podcasts</p>
</div>

{!! Form::model($itunes_top_podcast, [
    'method' => $itunes_top_podcast->exists ? 'PUT' : 'POST',
    'action' => $itunes_top_podcast->exists ?
        ['Admin\ItunesTopPodcastController@update', $itunes_top_podcast->id] : 'Admin\ItunesTopPodcastController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group"> 
                        <div class="fm-label">
                            <label>Country</label>                            
                        </div>                                      
                        <div class="fm-control">
                            <div class="fm-control">
                                {!! Form::select('title', ["" => "Select Country"] + $countryArray , null, [
                                    'class' => "form-control chosen-select",
                                    'id' => "title",
                                    'required' => true,
                                    'data-parsley-required-message' => "Please select country from list.",
                                ]) !!}
                            </div>                                                
                        </div>
                        <div id="show_err_id"></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>iTunes RSS URL</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('rss_url', null, [
                                'class' => "form-control",
                                'placeholder' => "RSS URL",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter url."                                
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>                            
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $itunes_top_podcast->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $itunes_top_podcast->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\ItunesTopPodcastController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    $(function() {
               
    });
</script>
@stop