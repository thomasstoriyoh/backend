@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $marketplace_promotion->exists ? 'Update' : 'Create' }} Marketplace Promotion</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="#" class="active">Marketplace</a></li>
            <li><a href="{{ action('Admin\MarketplacePromotionController@index') }}">Promotion</a></li>
            <li>{{ $marketplace_promotion->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $marketplace_promotion->exists ? 'Update' : 'Create' }} Marketplace Promotion</p>
</div>

{!! Form::model($marketplace_promotion, [
    'method' => $marketplace_promotion->exists ? 'PUT' : 'POST',
    'action' => $marketplace_promotion->exists ?
        ['Admin\MarketplacePromotionController@update', $marketplace_promotion->id] : 'Admin\MarketplacePromotionController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter title."                                
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 apppage">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Type</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Podcast, Episode, Standalone Episode"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('type', ['' => 'Select' ,'Podcast' => 'Series', 'Standalone' => 'Standalone Episode'], null,[
                                'class' => 'chosen-select form-control',
                                'id' => 'select_apppage',
                                'required' => true,
                                'data-parsley-required-message' => "Please select type.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Series List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Series List"></i>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="fm-control">
                                <select  name="show_id" id="podcast" class="select-2-show form-control" data-placeholder="Select..." data-parsley-required-message="Please select series." data-parsley-errors-container="#show_err_id">
                                @foreach($podcastArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                                </select>                                
                            </div>                                                
                        </div>
                        <div id="show_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Episode List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Episode List"></i>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="fm-control">
                                <select  name="episode_id" id="episode" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Episode." data-parsley-errors-container="#episode_err_id">
                                @foreach($episode as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                                </select>                                
                            </div>                                                
                        </div>
                        <div id="episode_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 standalone_episode">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Episode</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Standalone Episode List"></i>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="fm-control">
                                <select name="standaloneepisode_id" id="standaloneepisode" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select standalone episode." data-parsley-errors-container="#standaloneepisode_err_id">
                                @foreach($standalone as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                                </select>                                
                            </div>                                                
                        </div>
                        <div id="standaloneepisode_err_id"></div>
                    </div>
                </div>                                
            </div>                          
        </div>
    </div>
</div>

@if(!empty($marketplace_promotion->image))
    <div class="panel image-panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Existing Image
            </h3>
            <div class="example-box-wrapper">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbnail-box-wrapper">
                        <div class="thumbnail-box">
                            <div class="thumb-content">
                                <div class="center-vertical">
                                    <div class="center-content">
                                        <div class="thumb-btn animated zoomIn">
                                            <a href="javascript:void(0)" class="btn btn-md btn-round btn-danger delete-image" title=""><i class="glyph-icon icon-remove"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="thumb-overlay bg-gray"></div>
                            <img src="{{ $marketplace_promotion->getImage(200) }}" alt="" style="box-shadow:0 10px 16px 0 #CCC">
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endif

@include('admin.partials.crop_upload', ['status' => empty($marketplace_promotion->image), 'tooltip' => 'Image size will be 1200 x 675'])

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $marketplace_promotion->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $marketplace_promotion->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\MarketplacePromotionController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    $(function() {
        $("#podcast").select2({
            allowClear: true,
            placeholder: 'Search for a series',
            ajax: {
                url: "{{ action("Admin\\MarketplacePromotionController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    $(document).on("change", "#podcast", function(){
        episode(this.value);
        episode_reset(); 
    });

    function episode_reset(){
        $("#episode").val(null).trigger("change");
    }

    @if($marketplace_promotion->exists)
        $('#podcast').trigger('change');
        $('#episode').val('{{key($episode)}}').trigger('change');
    @endif

    function episode(value) {
        $("#episode").select2({
            allowClear: true,
            placeholder: 'Search for a episode',
            ajax: {
                url: "{{ action("Admin\\MarketplacePromotionController@search_episode") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    select_type: value,
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 0                        
        });
    }

    // playlist
    $(function() {
        $("#standaloneepisode").select2({
            allowClear: true,
            placeholder: 'Search for a playlist',
            ajax: {
                url: "{{ action("Admin\\MarketplacePromotionController@search_standalone_episode") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });

    // select event
    $(document).on("change", "#select_apppage", function(){
        select_event(this.value);
    });

    $(document).ready(function(){
        var value = $('#select_apppage').val();
        select_event(value);
    });

    function select_event(value)
    {
        if (value == 'Podcast') {
            $('.podcast_data').show();
            $("#podcast").attr('data-parsley-required', 'true').parsley();

            $('.standalone_episode').hide();
            $("#standaloneepisode").removeAttr('data-parsley-required').parsley().destroy();

        } else if (value == 'Standalone') {
            $('.standalone_episode').show();
            $("#standaloneepisode").attr('data-parsley-required', 'true').parsley();
            
            $('.podcast_data').hide();
            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();            
        } else {
            $('.podcast_data').hide();
            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();

            $('.standalone_episode').hide();
            $("#standaloneepisode").removeAttr('data-parsley-required').parsley().destroy();            
        }
    }

    $(function() {        
        module.counter($('input[name="title"]'), 100);

        /**
         * Uploader start.
         * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
         * If uploading resource is not image then it will ignore the crop size.
         *
         */
        var item_exists = {{ $marketplace_promotion->exists ? 'false' : 'true' }};
        $upQueue = $(".file-uploader");
        var crop_size = [1200, 675];

        var uploader = $upQueue.bcmUpload({
            filters: { // Mime Types to be allowed to select.
                mime_types: [{
                    title: "Image files",
                    extensions: "jpg,gif,png,jpeg"
                }]
            },
            enable_crop: false, // Is cropping required or not? That's it. Cropping will be there for you.
            max_files: 1, // How many uploads allowed?
            required: item_exists, // Is the image upload mandatory?
            multipart_params: {resource: 'Image'}, // You can set it to your choice. You have to check for it in the controller to upload accordingly.
            min_dimention: { // This section is only required for image resources. It will be ignored for other files even if you set it.
                width: crop_size[0],
                height: crop_size[1]
            }
        });

        var uploaded = false;
        uploader.bind('UploadComplete', function (up, files) { // What happens when all the uploads are done.
            uploaded = true;
            $("#moduleForm").submit();
        });
        
        $('#moduleForm').submit(function() {
            if (uploader.files.length > 0 && uploaded == false) {
                uploader.start();
                return false;
            } else if (uploader.getOption('required') && uploader.files.length < 1) {
                module.notify('Please select some image(s) to proceed', 'error');
                return false;
            } else {
                return true;
            }
        });

    /**
     * Upload section end.
     */
    @if($marketplace_promotion->exists)
        $("a.delete-image").click(function() {
            module.confirm('Are you sure you want to delete this image? ', function(isConfirm) {
                if (isConfirm) {
                    var ajax = module.ajax('{{ action("Admin\\MarketplacePromotionController@deleteImage", $marketplace_promotion->id) }}', {
                        type: 'Cover'
                    }, 'DELETE');
                    
                    ajax.done(function(data){
                        if(data.status) {
                            $("div.image-panel").toggle();
                            uploader.setOption('required', true);
                        }else{
                            module.notify(data.message, 'error');
                        }
                    });
                }
            });    
        });
    @endif
});
</script>
@stop