@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $masthead->exists ? 'Update' : 'Create' }} App Banner</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\MastheadController@index') }}">App Banner</a></li>
            <li>{{ $masthead->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $masthead->exists ? 'Update' : 'Create' }} App Banner</p>
</div>

{!! Form::model($masthead, [
    'method' => $masthead->exists ? 'PUT' : 'POST',
    'action' => $masthead->exists ?
        ['Admin\MastheadController@update', $masthead->id] : 'Admin\MastheadController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter title."                                
                            ]) !!}
                        </div>
                    </div>
                </div>
            
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Link Type</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="External or App Page"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('type', ['' => 'Please select' , 'App Page' => 'App Page', 'External' => 'External'], null,[
                                'class' => 'chosen-select form-control',
                                'id' => 'appmasthead',
                                'required' => true,
                                'data-parsley-required-message' => "Please select link type.",
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6" id="external">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>URL</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="URL that this will point to."></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('link', null, [
                                'class' => "form-control",
                                'placeholder' => "External Link",
                                'id' => 'externallink',
                                'required' => false,
                                'data-parsley-required-message' => "Please enter a url.",
                                'data-parsley-url-message' => "Please enter a valid url.",
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 apppage">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>App Page</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="External or Internal"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('content_type', ['' => 'Select' ,'Podcast' => 'Podcast', 'Playlist' => 'Playlist', 'Smart Playlist' => 'Smart Playlist', 'Collection' => 'Collection'], null,[
                                'class' => 'chosen-select form-control',
                                'id' => 'select_apppage',
                                'data-parsley-required-message' => "Please select link type.",
                            ]) !!}
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Podcast List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="External or Internal"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="show_id" id="podcast" class="select-2-show form-control" data-placeholder="Select multiple" data-parsley-required-message="Please select podcast." data-parsley-errors-container="#show_err_id">
                                    @foreach($podcastArray as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="show_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 apppage podcast_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Episode List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="External or Internal"></i>
                            </div>
                        </div>
                        <div class="form-group">                                                                   
                            <div class="fm-control">
                                <select  name="episode_id" id="episode" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Episode." data-parsley-errors-container="#episode_err_id">
                                    @foreach($episode as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                </select>                               
                            </div>                                                
                            </div>
                        <div id="episode_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 playlist_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Playlist List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="External or Internal"></i>
                            </div>
                        </div>
                        <div class="form-group">                                                                   
                                <div class="fm-control">
                                    <select  name="playlist_id" id="playlist" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Playlist." data-parsley-errors-container="#playlist_err_id">
                                        @foreach($playlist as $key => $module)
                                            <option value="{{ $key }}" selected>{{ $module }}</option>
                                        @endforeach
                                    </select>                                
                                </div>                                                
                            </div>
                            <div id="playlist_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 smart_playlist_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Smart Playlist List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="External or Internal"></i>
                            </div>
                        </div>
                        <div class="form-group">                                       
                            
                                <div class="fm-control">
                                    <select  name="smart_playlist_id" id="smart_playlist" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Smart Playlist." data-parsley-errors-container="#smart_playlist_err_id">
                                        @foreach($smartplaylist as $key => $module)
                                            <option value="{{ $key }}" selected>{{ $module }}</option>
                                        @endforeach   
                                        </select>                                
                                </div>                                                
                            </div>
                            <div id="smart_playlist_err_id"></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 collection_data">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Collection List</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="External or Internal"></i>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="fm-control">
                                <select  name="collection_id" id="collection" class="select-2-show form-control" data-placeholder="Select " data-parsley-required-message="Please select Collection." data-parsley-errors-container="#collection_err_id">
                                    @foreach($collection as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                </select>                                
                            </div>                                                
                        </div>
                        <div id="collection_err_id"></div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

@if(!empty($masthead->image))
<div class="panel image-panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Existing Image
        </h3>
        <div class="example-box-wrapper">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="thumbnail-box-wrapper">
                    <div class="thumbnail-box">
                        <div class="thumb-content">
                            <div class="center-vertical">
                                <div class="center-content">
                                    <div class="thumb-btn animated zoomIn">
                                        <a href="javascript:void(0)" class="btn btn-md btn-round btn-danger delete-image" title=""><i class="glyph-icon icon-remove"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="thumb-overlay bg-gray"></div>
                        <img src="{{ $masthead->getImage(200) }}" alt="" style="box-shadow:0 10px 16px 0 #CCC">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endif

@include('admin.partials.crop_upload', ['status' => empty($masthead->image), 'tooltip' => 'Image size will be 1200 x 600'])

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $masthead->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $masthead->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\MastheadController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    var item_exists = {{ $masthead->exists ? 'false' : 'true' }};
    $(function(){
    
        module.counter($('input[name="title"]'), 50);
    
        /**
         * Uploader start.
         * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
         * If uploading resource is not image then it will ignore the crop size.
         *
         */
    
        $upQueue = $(".file-uploader");
        var crop_size = [1200, 600];
    
        var uploader = $upQueue.bcmUpload({
            filters: { // Mime Types to be allowed to select.
                mime_types: [{
                    title: "Image files",
                    extensions: "jpg,gif,png,jpeg"
                }]
            },
            enable_crop: false, // Is cropping required or not? That's it. Cropping will be there for you.
            max_files: 1, // How many uploads allowed?
            required: item_exists, // Is the image upload mandatory?
            multipart_params: {resource: 'Image'}, // You can set it to your choice. You have to check for it in the controller to upload accordingly.
            min_dimention: { // This section is only required for image resources. It will be ignored for other files even if you set it.
                width: crop_size[0],
                height: crop_size[1]
            }
        });
    
        var uploaded = false;
        uploader.bind('UploadComplete', function (up, files) { // What happens when all the uploads are done.
            uploaded = true;
            $("#moduleForm").submit();
        });
        
        $('#moduleForm').submit(function() {
            if (uploader.files.length > 0 && uploaded == false) {
                uploader.start();
    
                return false;
            } else if (uploader.getOption('required') && uploader.files.length < 1) {
                module.notify('Please select some image(s) to proceed', 'error');
    
                return false;
            } else {
                return true;
            }
        });
    
    //    module.loadImageCropFunction(crop_size, uploader);
    
        /**
         * Upload section end.
         */
        @if($masthead->exists)
            $("a.delete-image").click(function() {
                module.confirm('Are you sure you want to delete this image? ', function(isConfirm) {
                    if (isConfirm) {
                        var ajax = module.ajax('{{ action("Admin\\MastheadController@deleteImage", $masthead->id) }}', {
                            type: 'Cover'
                        }, 'DELETE');
                        
                        ajax.done(function(data){
                            if(data.status) {
                                $("div.image-panel").toggle();
                                uploader.setOption('required', true);
                            }else{
                                module.notify(data.message, 'error');
                            }
                        });
                    }
                });
                
            });
        @endif
    });
    
    $(function() {
        $("#podcast").select2({
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\ActiveShowController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });
    
    // playlist
    $(function() {
        $("#playlist").select2({
            placeholder: 'Search for a playlist',
            ajax: {
                url: "{{ action("Admin\\MastheadController@search_playlist") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });
    
    // smart_playlist
    $(function() {
        $("#smart_playlist").select2({
            placeholder: 'Search for a smart playlist',
            ajax: {
                url: "{{ action("Admin\\MastheadController@search_smart_playlist") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });
    
    // collection
    $(function() {
        $("#collection").select2({
            placeholder: 'Search for a collection',
            ajax: {
                url: "{{ action("Admin\\MastheadController@search_collection") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });
    });
    
    function episode(value) {
        $("#episode").select2({
            placeholder: 'Search for a episode',
            ajax: {
                url: "{{ action("Admin\\MastheadController@search_episode") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    select_type: value,
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 0                        
        });
    }
    
    $(document).on("change", "#podcast", function(){        
       episode(this.value); 
    });
    
    $(document).ready(function(){
        var value = $('#podcast').val();
        if (value != '') {
            episode(value); 
        }
        
    });
    
    
    @if($masthead->exists)
        
        $(document).ready(function(){
            var value = $('#appmasthead').val();
            // alert(value)
            if (value == "External")
            {
                $('#external').show();
                    $("#externallink").attr('data-parsley-required', 'true').parsley();
                $('.apppage').hide();
                    $("#select_apppage").removeAttr('data-parsley-required').parsley().destroy();
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();  
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if(value == "App Page")
            {
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                $('.apppage').show();
                    $("#select_apppage").attr('data-parsley-required', 'true').parsley();
                
                var apppage = $('#select_apppage').val();
    
                    if (apppage == "Collection")
                    {
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').show();
                            $("#collection").attr('data-parsley-required', 'true').parsley();
                    }
    
                    else if (apppage == "Podcast")
                    {
                        $('.podcast_data').show();
                            $("#podcast").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else if (apppage == "Playlist")
                    {
                        $('.playlist_data').show();
                            $("#playlist").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else if (apppage == "Smart Playlist")
                    {
                        $('.smart_playlist_data').show();
                            $("#smart_playlist").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
    
                // $('.podcast_data').hide();
            }
            else {
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                $('.apppage').hide();
                    $("#select_apppage").removeAttr('data-parsley-required').parsley().destroy();
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
        });
    
        $(document).on("change", "#appmasthead", function(){        
            var linktype = $('#appmasthead').val();
            if (linktype == "External")
            {
                $('#external').show();
                $('.apppage').hide();  
                    $('.podcast_data').hide();
                        $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                    $('.playlist_data').hide();
                        $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                    $('.smart_playlist_data').hide();
                        $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                    $('.collection_data').hide();
                        $("#collection").removeAttr('data-parsley-required').parsley().destroy();
    
                $("#externallink").attr('data-parsley-required', 'true').parsley();
                $("#select_apppage").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if(linktype == "App Page")
            {
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.apppage').show();
                    $("#select_apppage").attr('data-parsley-required', 'true').parsley();
                var apppage = $('#select_apppage').val();
                    // alert(select_type);
                    if (apppage == "Podcast")
                    {
                        $('.podcast_data').show();
                            $("#podcast").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else if (apppage == "Playlist")
                    {
                        $('.playlist_data').show();
                            $("#playlist").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else if (apppage == "Smart Playlist")
                    {
                        $('.smart_playlist_data').show();
                            $("#smart_playlist").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else if (apppage == "Collection")
                    {
                        $('.collection_data').show();
                            $("#collection").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else{
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
            }
            else {
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                $('.apppage').hide();
                    $("#select_apppage").removeAttr('data-parsley-required').parsley().destroy();
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
        });
    
        $(document).on("change", "#select_apppage", function(){        
            var apppage = $('#select_apppage').val();
            // alert(select_type);
            if (apppage == "Podcast")
            {
                $('.podcast_data').show();
                    $("#podcast").attr('data-parsley-required', 'true').parsley();
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if (apppage == "Playlist")
            {
                $('.playlist_data').show();
                    $("#playlist").attr('data-parsley-required', 'true').parsley();
    
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                // $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if (apppage == "Smart Playlist")
            {
                $('.smart_playlist_data').show();
                    $("#smart_playlist").attr('data-parsley-required', 'true').parsley();
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if (apppage == "Collection")
            {
                $('.collection_data').show();
                    $("#collection").attr('data-parsley-required', 'true').parsley();
    
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
            }       
            else
            {
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
                
        });
    
        
    @else
        $(document).ready(function(){
            var value = $('#appmasthead').val();
            // alert(value)
            if (value == "External")
            {
                $('#external').show();
                $('.apppage').hide();
                $('.podcast_data').hide();
                $('.playlist_data').hide();
                $('.smart_playlist_data').hide();
                $('.collection_data').hide();  
            }
            else if(value == "App Page")
            {
                $('#external').hide();
                $('.apppage').show();
                
                var apppage = $('#select_apppage').val();
    
                    if (apppage == "Collection")
                    {
                        $('.podcast_data').hide();
                        $('#external').hide();
                        $('.playlist_data').hide();
                        $('.smart_playlist_data').hide();
                        $('.collection_data').show();
                    }
    
                    else if (apppage == "Podcast")
                    {
                        $('.podcast_data').show();
                        $('#external').hide();
                        $('.playlist_data').hide();
                        $('.smart_playlist_data').hide();
                        $('.collection_data').hide();
                    }
                    else if (apppage == "Playlist")
                    {
                        $('.playlist_data').show();
                        $('#external').hide();
                        $('.podcast_data').hide();
                        $('.smart_playlist_data').hide();
                        $('.collection_data').hide();
                    }
                    else if (apppage == "Smart Playlist")
                    {
                        $('.smart_playlist_data').show();
                        $('#external').hide();
                        $('.podcast_data').hide();
                        $('.playlist_data').hide();
                        $('.collection_data').hide();
                    }
    
                // $('.podcast_data').hide();
            }
            else {
                $('#external').hide();
                $('.apppage').hide();
                $('.podcast_data').hide();
                $('.playlist_data').hide();
                $('.smart_playlist_data').hide();
                $('.collection_data').hide();
            }
        });
    
        $(document).on("change", "#appmasthead", function(){        
            var linktype = $('#appmasthead').val();
            if (linktype == "External")
            {
                $('#external').show();
                $('.apppage').hide();  
                    $('.podcast_data').hide();
                        $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                    $('.playlist_data').hide();
                        $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                    $('.smart_playlist_data').hide();
                        $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                    $('.collection_data').hide();
                        $("#collection").removeAttr('data-parsley-required').parsley().destroy();
    
                $("#externallink").attr('data-parsley-required', 'true').parsley();
                $("#select_apppage").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if(linktype == "App Page")
            {
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.apppage').show();
                    $("#select_apppage").attr('data-parsley-required', 'true').parsley();
    
                var apppage = $('#select_apppage').val();
                    // alert(select_type);
                    if (apppage == "Podcast")
                    {
                        $('.podcast_data').show();
                            $("#podcast").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else if (apppage == "Playlist")
                    {
                        $('.playlist_data').show();
                            $("#playlist").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else if (apppage == "Smart Playlist")
                    {
                        $('.smart_playlist_data').show();
                            $("#smart_playlist").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else if (apppage == "Collection")
                    {
                        $('.collection_data').show();
                            $("#collection").attr('data-parsley-required', 'true').parsley();
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                    }
                    else{
                        $('#external').hide();
                            $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                        $('.podcast_data').hide();
                            $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                        $('.playlist_data').hide();
                            $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.smart_playlist_data').hide();
                            $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                        $('.collection_data').hide();
                            $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                    }
            }
            else {
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                $('.apppage').hide();
                    $("#select_apppage").removeAttr('data-parsley-required').parsley().destroy();
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
        });
    
        $(document).on("change", "#select_apppage", function(){        
            var apppage = $('#select_apppage').val();
            // alert(select_type);
            if (apppage == "Podcast")
            {
                $('.podcast_data').show();
                    $("#podcast").attr('data-parsley-required', 'true').parsley();
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if (apppage == "Playlist")
            {
                $('.playlist_data').show();
                    $("#playlist").attr('data-parsley-required', 'true').parsley();
    
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
                // $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if (apppage == "Smart Playlist")
            {
                $('.smart_playlist_data').show();
                    $("#smart_playlist").attr('data-parsley-required', 'true').parsley();
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
    
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }
            else if (apppage == "Collection")
            {
                $('.collection_data').show();
                    $("#collection").attr('data-parsley-required', 'true').parsley();
    
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
            }       
            else
            {
                $('#external').hide();
                    $("#externallink").removeAttr('data-parsley-required').parsley().destroy();
                $('.podcast_data').hide();
                    $("#podcast").removeAttr('data-parsley-required').parsley().destroy();
                $('.playlist_data').hide();
                    $("#playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.smart_playlist_data').hide();
                    $("#smart_playlist").removeAttr('data-parsley-required').parsley().destroy();
                $('.collection_data').hide();
                    $("#collection").removeAttr('data-parsley-required').parsley().destroy();
            }                
        });
    @endif    
    </script>
@stop