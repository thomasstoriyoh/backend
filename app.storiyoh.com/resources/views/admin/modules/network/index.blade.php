@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Networks</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li>Networks</li>
        </ul>
    </div>
    <p>Networks Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Networks Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.networks')
                    <a class="btn btn-default" href="{{ action('Admin\NetworkController@create') }}">ADD ITEM</a>
                @endCanI
                @CanI(['update.networks', 'delete.networks'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('update.networks')
                            <li><a href="javascript:void(0)" class="publish">Publish</a></li>
                            <li><a href="javascript:void(0)" class="draft">Draft</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="feature">Featured</a></li>
                            <li><a href="javascript:void(0)" class="unfeature">Unfeatured</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ action('Admin\NetworkController@reorder') }}">Reorder</a></li>
                        @endCanI                        
                        @CanI('delete.networks')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="delete">Delete</a></li>
                        @endCanI
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
                <div class="rdtChosen">  
                    <select name="" multiple data-placeholder="Filter options..." 
                            class="chosen-select form-control" id="module_filter">
                        <optgroup label="Status">
                            <option value="status.Published">Published</option>
                            <option value="status.Draft">Draft</option>
                            <option value="featured.1">Featured</option>
                        </optgroup>                        
                    </select>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    var table = $("#listing-table").bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Title", data: "title"},
            { title: "Image", data: "image"},
            { title: "Featured", data: "featured", sortable: true},
            { title: "No of Podcasts", data: "number_of_podcast"},
            { title: "Status", data: "status"},            
            @CanI('edit.networks')
                { title: "Options", data: "options", sortable: false}
            @endCanI
        ]
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.publish", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to publish these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.publish(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items published successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.draft", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to draft these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.draft(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items drafted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.delete", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to delete these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.delete(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items deleted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    @if(Auth::guard('admin')->user()->isSuperAdmin() && $module_details->maker_checker == 'On')
        $(document).on("click", "a.approve", function(){
            var ids = module.getSelectedIds(table);
            
            if(ids.length < 1) {
                module.notify('Please select some items to perform the action.', 'error');
                return false;
            }
            
            module.confirm('Are you sure you want to approve these item(s)?', function(isConfirm){
                if(isConfirm){
                    var ajax = module.approve(ids);
                    ajax.done(function(data) {
                        if (data.status) {
                            module.notify('Item(s) approved successfully.', 'success');
                        }else{
                            module.notify(data.message, 'error');
                        }
                        table.ajax.reload();
                    });
                }
            });
        });
        
        $(document).on("click", "a.unapprove", function(){
            var ids = module.getSelectedIds(table);
            
            if(ids.length < 1) {
                module.notify('Please select some items to perform the action.', 'error');
                return false;
            }
            
            module.confirm('Are you sure you want to unapprove these item(s)?', function(isConfirm){
                if(isConfirm) {
                    var ajax = module.unapprove(ids);
                    ajax.done(function(data) {
                        if (data.status) {
                            module.notify('Item(s) unapproved successfully.', 'success');
                        }else{
                            module.notify(data.message, 'error');
                        }
                        table.ajax.reload();
                    });
                }
            });
        });
    @endif

    $(document).on("click", "a.feature", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        if(ids.length > 10) {
            module.notify('You can not feature more than 10 items.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to feature these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\NetworkController@featured") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) featured successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.unfeature", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to unfeature these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\NetworkController@unfeatured") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) unfeatured successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
});
</script>
@stop