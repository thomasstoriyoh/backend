@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $newsletter->exists ? 'Update' : 'Create' }} Newsletter</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\NewsletterController@index') }}">Newsletter</a></li>
            <li>{{ $newsletter->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $newsletter->exists ? 'Update' : 'Create' }} Newsletter</p>
</div>

{!! Form::model($newsletter, [
    'method' => $newsletter->exists ? 'PUT' : 'POST',
    'action' => $newsletter->exists ?
        ['Admin\NewsletterController@update', $newsletter->id] : 'Admin\NewsletterController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title (Internal)</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter title.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Newsletter Title</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Newsletter Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('newsletter_title', null, [
                                'class' => "form-control",
                                'placeholder' => "Newsletter Title",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter newsletter title.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Newsletter Description</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Newsletter Description contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('description', null, [
                                'class' => "form-control",
                                'placeholder' => "Newsletter Description",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
					
					<div class="form-group">
                        <div class="fm-label">
                            <label>Button Text</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Button Text contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('btn_text', null, [
                                'class' => "form-control",
                                'placeholder' => "Button Text",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter btn_text.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Button Link</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Button Link that this will point to."></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('btn_link', null, [
                                'class' => "form-control",
                                'placeholder' => "Button Link",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter a button link.",
                                'data-parsley-url-message' => "Please enter a valid button link.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>

	                   
        </div>
    </div>
</div>



<div class="panel">
    <div class="panel-body">
        <div class="example-box-wrapper">

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group"> 
            		<div class="fm-label">
                        <label>Trending Podcast</label>
                    </div>                                      
                    <div class="fm-control">
                        <div class="fm-control">
                            <select  name="trending_podcasts[]" id="trending_podcasts" class="select-2-show form-control" multiple data-placeholder="Select multiple" required data-parsley-required-message="Please select podcast." data-parsley-errors-container="#trending_podcasts_err_id">
                                @foreach($podcastArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                            </select>                                
                        </div>                                                
                    </div>
                    <div id="trending_podcasts_err_id"></div>
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group"> 
            		<div class="fm-label">
                        <label>Featured Collections</label>
                    </div>                                      
                    <div class="fm-control">
                        <div class="fm-control">
                            <select  name="featured_collections[]" id="featured_collections" class="select-2-show form-control" multiple data-placeholder="Select multiple" required data-parsley-required-message="Please select podcast." data-parsley-errors-container="#featured_collections_err_id">
                                @foreach($chartsArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                            </select>                                
                        </div>                                                
                    </div>
                    <div id="featured_collections_err_id"></div>
                </div>
            </div>


             <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">  
                	<div class="fm-label">
                        <label>Select Podcast for Featured Episodes</label>
                    </div>                                     
                    <div class="fm-control">
                        <div class="fm-control">
                            <select  name="show_id[]" id="show_id" class="select-2-show form-control" multiple data-placeholder="Select multiple" data-parsley-required-message="Please select podcast." data-parsley-errors-container="#show_err_id">
                                @foreach($showArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                            </select>                                
                        </div>                                                
                    </div>
                    <div id="show_err_id"></div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">  
                	<div class="fm-label">
                        <label>Featured Episodes</label>
                    </div>                                     
                    <div class="fm-control">
                        <div class="fm-control">
                            <select  name="featured_episodes[]" id="featured_episodes" class="form-control chosen-select-limit4" multiple data-placeholder="Select multiple" required data-parsley-required-message="Please select episodes." data-parsley-errors-container="#featured_episodes_err_id">
                                @foreach($episodesArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                            </select>                                
                        </div>                                                
                    </div>
                    <div id="featured_episodes_err_id"></div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group"> 
                    <div class="fm-label">
                        <label>Essentials</label>
                    </div>                                      
                    <div class="fm-control">
                        <div class="fm-control">
                            <select  name="essentials[]" id="essentials" class="select-2-show form-control" multiple data-placeholder="Select multiple" required data-parsley-required-message="Please select podcast." data-parsley-errors-container="#essentials_err_id">
                                @foreach($essentialsArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                            </select>                                
                        </div>                                                
                    </div>
                    <div id="essentials_err_id"></div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">  
                	<div class="fm-label">
                        <label>Podcast of the Week</label>
                    </div>                                     
                    <div class="fm-control">
                        <div class="fm-control">
                            <select  name="podcast_of_the_week" id="podcast_of_the_week" class="select-2-show form-control" data-placeholder="Select single" required data-parsley-required-message="Please select episodes." data-parsley-errors-container="#podcast_of_the_week_err_id">
                                @foreach($podcastweekArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                            </select>                                
                        </div>                                                
                    </div>
                    <div id="podcast_of_the_week_err_id"></div>
                </div>
            </div>

        </div>
    </div>
</div>


@if(!empty($newsletter->image))
<div class="panel image-panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Existing Image
        </h3>
        <div class="example-box-wrapper">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="thumbnail-box-wrapper">
                    <div class="thumbnail-box">
                        <div class="thumb-content">
                            <div class="center-vertical">
                                <div class="center-content">
                                    <div class="thumb-btn animated zoomIn">
                                        <a href="javascript:void(0)" class="btn btn-md btn-round btn-info crop-image" title=""><i class="glyph-icon icon-crop"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-md btn-round btn-danger delete-image" title=""><i class="glyph-icon icon-remove"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="thumb-overlay bg-gray"></div>
                        <img src="{{ asset('uploads/newsletter/' . $newsletter->image) }}" alt="" style="display: none;">
                        <img src="{{ asset('uploads/newsletter/thumbs/640_480_' . $newsletter->image) }}" alt="">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endif
<!-- 
@include('admin.partials.crop_upload', ['status' => empty($newsletter->image),'title' => 'Logo' ])
 -->
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $newsletter->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $newsletter->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\NewsletterController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">



    $(function() {

        $("#trending_podcasts").select2({
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\NewsletterController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2,
            maximumSelectionLength: 4                        
        });


        $("#featured_collections").select2({
            placeholder: 'Search for a charts',
            ajax: {
                url: "{{ action("Admin\\NewsletterController@search_charts") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2,
            maximumSelectionLength: 2                          
        });


        $("#show_id").select2({
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\NewsletterController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });

        $("#show_id").on('change',function(){
        	
        	var showid = $("#show_id").val();

        	if($.trim((showid) != "")){
        		var ajax = $.ajax({
                    url : "{{ action("Admin\\NewsletterController@search_episodes") }}",
                    type : 'GET',
                    data : {showid : showid},
                });

        	    ajax.done(function(data){        	    	
        	    	$('#featured_episodes').empty();

        	   		$('#featured_episodes').append($("<option value=''></option>"));
                    $.each(data.results, function(key, element) {

                     $('#featured_episodes').append($("<option value='" + element.id +"'>" + element.text + "</option>"));
                     });
                     $('#featured_episodes').trigger("chosen:updated");
        	    });

        	}
        });

        $(".chosen-select-limit4").chosen({ max_selected_options: 4,search_contains: true });
        

        $("#podcast_of_the_week").select2({
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\NewsletterController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });


        $("#essentials").select2({
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\NewsletterController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2,
            maximumSelectionLength: 4                        
        });



    });

var item_exists = {{ $newsletter->exists ? 'false' : 'true' }};
$(function(){

    module.counter($('input[name="title"]'), 50);

    /**
     * Uploader start.
     * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
     * If uploading resource is not image then it will ignore the crop size.
     *
     */

    $upQueue = $(".file-uploader");
    var crop_size = [640, 480];

    var uploader = $upQueue.bcmUpload({
        filters: { // Mime Types to be allowed to select.
            mime_types: [{
                title: "Image files",
                extensions: "jpg,gif,png,jpeg"
            }]
        },
        enable_crop: false, // Is cropping required or not? That's it. Cropping will be there for you.
        max_files: 1, // How many uploads allowed?
        required: item_exists, // Is the image upload mandatory?
        multipart_params: {resource: 'Image'}, // You can set it to your choice. You have to check for it in the controller to upload accordingly.
        min_dimention: { // This section is only required for image resources. It will be ignored for other files even if you set it.
            width: crop_size[0],
            height: crop_size[1]
        }
    });

    var uploaded = false;
    uploader.bind('UploadComplete', function (up, files) { // What happens when all the uploads are done.
        uploaded = true;
        $("#moduleForm").submit();
    });
    
    $('#moduleForm').submit(function() {
        if (uploader.files.length > 0 && uploaded == false) {
            uploader.start();

            return false;
        } else if (uploader.getOption('required') && uploader.files.length < 1) {
            //module.notify('Please select some image(s) to proceed', 'error');

            return true;
        } else {
            return true;
        }
    });

//    module.loadImageCropFunction(crop_size, uploader);

    /**
     * Upload section end.
     */
    @if($newsletter->exists)
        $("a.delete-image").click(function() {
            module.confirm('Are you sure you want to delete this image? ', function(isConfirm) {
                if (isConfirm) {
                    var ajax = module.ajax('{{ action("Admin\\NewsletterController@deleteImage", $newsletter->id) }}', {
                        type: 'Cover'
                    }, 'DELETE');
                    
                    ajax.done(function(data){
                        if(data.status) {
                            $("div.image-panel").toggle();
                            uploader.setOption('required', true);
                        }else{
                            module.notify(data.message, 'error');
                        }
                    });
                }
            });        
        });
    @endif
});
</script>
@stop