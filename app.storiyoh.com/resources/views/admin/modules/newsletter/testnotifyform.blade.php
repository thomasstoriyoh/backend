@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $newsletter->exists ? 'Update' : 'Create' }} Newsletter</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\NewsletterController@index') }}">Newsletter</a></li>
            <li>Notify</li>
        </ul>
    </div>
    <p>{{ $newsletter->exists ? 'Update' : 'Create' }} Newsletter</p>
</div>

{!! Form::model($newsletter, [
    'method' => 'PUT',
    'action' => ['Admin\NewsletterController@testnotify', $newsletter->id],
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Subject</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Subject contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('subject', null, [
                                'class' => "form-control",
                                'placeholder' => "Subject",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter subject.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
					
					<div class="form-group">
                        <div class="fm-label">
                            <label>From Name</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="From Name contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('from_name', 'Storiyoh', [
                                'class' => "form-control",
                                'placeholder' => "From Name",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter from name.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>From Email</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="From Email this will contain only email."></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::email('from_email', 'info@storiyoh.com', [
                                'class' => "form-control",
                                'placeholder' => "From Email",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter a from email.",
                                'data-parsley-url-message' => "Please enter a valid from email.",
                            ]) !!}
                        </div>
                    </div>
                </div>

                <input type="hidden" name="user_type" value="test">

            </div>
	                   
        </div>
    </div>
</div>




<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\NewsletterController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">



$(function(){

    module.counter($('input[name="title"]'), 50);   


            $("#user_type").select2({});

});
</script>
@stop