@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>User Playlist</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li>User Playlist</li>
        </ul>
    </div>
    <p>User Playlist Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            User Playlist Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.active-shows')
                    {{-- <a class="btn btn-default" href="{{ action('Admin\ActiveShowController@create') }}">ADD ITEM</a> --}}
                @endCanI
                @CanI(['update.playlist', 'delete.playlist'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('delete.playlist')                            
                            <li><a href="javascript:void(0)" class="feature">Featured</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="unfeature">Unfeatured</a></li>
                        @endCanI
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
                <div class="rdtChosen">  
                    <select name="" multiple data-placeholder="Filter options..." 
                            class="chosen-select form-control" id="module_filter">
                        <optgroup label="Status">
                            <option value="featured.1">Featured</option>
                        </optgroup>                                                
                    </select>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    var table = $("#listing-table").bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Title", data: "title", sortable: true},
            { title: "No. of Episodes", data: "no_of_episodes", sortable: false},
            { title: "Created By", data: "user_id", sortable: false},
            { title: "Image", data: "image", sortable: false},            
            { title: "Featured", data: "featured", sortable: true}, 
            { title: "Created Date", data: "created_at", sortable: true},
        ]
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.feature", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        if(ids.length > 5) {
            module.notify('You can not feature more than 5 items.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to feature these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\PlaylistController@featured") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) featured successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.unfeature", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to unfeature these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\PlaylistController@unfeatured") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) unfeatured successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });        
});
</script>
@stop