@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $podcast_of_the_day->exists ? 'Update' : 'Create' }} Podcast of the Day</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\PodcastDayController@index') }}">Podcast of the Day</a></li>
            <li>{{ $podcast_of_the_day->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $podcast_of_the_day->exists ? 'Update' : 'Create' }} Podcast of the Day</p>
</div>

{!! Form::model($podcast_of_the_day, [
    'method' => $podcast_of_the_day->exists ? 'PUT' : 'POST',
    'action' => $podcast_of_the_day->exists ?
        ['Admin\PodcastDayController@update', $podcast_of_the_day->id] : 'Admin\PodcastDayController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group"> 
                        <div class="fm-label">
                            <label>Podcast Name / Itunes Id</label>                            
                        </div>                                      
                        <div class="fm-control">
                            <div class="fm-control">
                                <select  name="show_id" id="show_id"
                                 class="select-2-show form-control" 
                                 data-placeholder="Select Podcast" 
                                 required 
                                 data-parsley-required-message="Please select podcast." 
                                 data-parsley-errors-container="#show_err_id">
                                    @foreach($podcastArray as $key => $module)
                                        <option value="{{ $key }}" selected>{{ $module }}</option>
                                    @endforeach
                                </select>                                
                            </div>                                                
                        </div>
                        <div id="show_err_id"></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Date</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('date', null, [
                                'class' => "form-control bootstrap-datepicker",
                                'placeholder' => "Date",
                                'required' => true,
                                'data-date-format' => "yyyy-mm-dd",
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>                            
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $podcast_of_the_day->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $podcast_of_the_day->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\PodcastDayController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    $(function() {
        $("#show_id").select2({
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\ActiveShowController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });        
    });
</script>
@stop