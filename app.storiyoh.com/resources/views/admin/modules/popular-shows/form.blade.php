@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $popular_show->exists ? 'Update' : 'Create' }} Popular Show</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\PopularShowController@index') }}">Popular Shows</a></li>
            <li>{{ $popular_show->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $popular_show->exists ? 'Update' : 'Create' }} Popular Shows</p>
</div>

{!! Form::model($popular_show, [
    'method' => $popular_show->exists ? 'PUT' : 'POST',
    'action' => $popular_show->exists ?
        ['Admin\PopularShowController@update', $popular_show->id] : 'Admin\PopularShowController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                   <div class="form-group">
                       <div class="fm-label">
                           <label>Category</label>
                       </div>
                       <div class="fm-control">
                           {!! Form::select('category_id', @$category_list, null, [
                               'class' => "form-control chosen-select",
                               'required' => true,
                               'data-parsley-required-message' => "Please select Category.",
                               'data-parsley-errors-container'=> "#cat_err_id",
                               'id' => "category_id",
                           ]) !!}
                       </div>
                       <div id="cat_err_id"></div>
                   </div>
               </div>
           </div>                                   
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">Podcasts</h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">                                       
                    <div class="fm-control">
                        <div class="fm-control">
                            <select name="show_id[]" id="show_id" class="select-2-show form-control" multiple data-placeholder="Select multiple" data-parsley-required-message="Please select podcast." data-parsley-errors-container="#show_err_id">
                                @foreach($podcastArray as $key => $module)
                                    <option value="{{ $key }}" selected>{{ $module }}</option>
                                @endforeach
                            </select>                                
                        </div>                                                
                    </div>
                    <div id="show_err_id"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.partials.crop_upload', ['title' => 'CSV', 'status' => true, 'tooltip' => ''])

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\PopularShowController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    $(function() {        
        $("#show_id").select2({
            placeholder: 'Search for a podcast',
            ajax: {
                url: "{{ action("Admin\\PopularShowController@search_movies") }}",                
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term, // search term
                    cat_id: $('#categories_id').val(), // search term
                    page: params.page
                  };
                },
                processResults: function (data, page) {
                  // parse the results into the format expected by Select2.
                  // since we are using custom formatting functions we do not need to
                  // alter the remote JSON data
                  return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 2                        
        });        
    });
</script>
<script type="text/javascript">
    var up = {
        data_csv: {
            object: null,
            uploaded: false
        }
    };
    /**
    * Uploader start.
    * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
    * If uploading resource is not image then it will ignore the crop size.
    *
    */

   $upQueue = $(".file-uploader");

   var uploader = $upQueue.bcmUpload({
       filters: { // Mime Types to be allowed to select.
           mime_types: [{
               title: "CSV files",
               extensions: "csv"
           }]
       },
       max_files: 1, // How many uploads allowed?
       input_name: 'data_csv[]',
       required: false, // Is the image upload mandatory?
       multipart_params: {resource: 'File'} // You can set it to your choice. You have to check for it in the controller to upload accordingly.
   });

    up.data_csv.object = uploader;

    uploader.bind('UploadComplete', function (upl, files) { // What happens when all the uploads are done.
         up.data_csv.uploaded = true;
         $("#moduleForm").submit();
    });

    $("#moduleForm").submit(function () {
     var $obj = $(this);     
     if (up.data_csv.object.files.length > 0 && up.data_csv.uploaded == false) {
         up.data_csv.object.start();
         return false;
     } else {
        return true;
     }
   });
</script>
@stop