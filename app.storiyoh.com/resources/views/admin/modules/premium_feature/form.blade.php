@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $premium_feature->exists ? 'Update' : 'Create' }} Premium Feature</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\PremiumFeatureController@index') }}">Premium Feature</a></li>
            <li>{{ $premium_feature->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $premium_feature->exists ? 'Update' : 'Create' }} Premium Feature</p>
</div>

{!! Form::model($premium_feature, [
    'method' => $premium_feature->exists ? 'PUT' : 'POST',
    'action' => $premium_feature->exists ?
        ['Admin\PremiumFeatureController@update', $premium_feature->id] : 'Admin\PremiumFeatureController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all()))        
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Feature Name</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Name of the feature"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('feature_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Feature Name",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter Feature Name.",                                
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Description</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Short description of the feature"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('feature_description', null, [
                                'class' => "form-control textarea-no-resize",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter Description.",
                                'placeholder' => "Description",
                                'rows' => 6
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>                       
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            {!! Form::checkbox('availability_ios', 1) !!} Available on iOS
            {{-- <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Itunes Product Id"></i>
            </div> --}}
        </h3>
        <div class="example-box-wrapper">
            <div class="form-group">
                <div class="fm-label">
                    <label>Apple Product ID</label>
                </div>
                <div class="fm-control">
                    {!! Form::text('apple_product_id', null, [
                        'class' => "form-control",
                        'placeholder' => "Apple Product ID"
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            {!! Form::checkbox('availability_android', 1) !!} Available on Android
            {{-- <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Price of the feature."></i>
            </div> --}}
        </h3>
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group"> 
                        <div class="fm-label">
                            <label>Price in INR</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('price_in_inr', null, [
                                'class' => "form-control",
                                'placeholder' => "Price in INR",
                                'pattern' => '/^[0-9\.]+$/', 
                                'data-parsley-pattern-message' => "Can only contain number and dot.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Price in USD</label>
                        </div>                        
                        <div class="fm-control">
                            {!! Form::text('price_in_usd', null, [
                                'class' => "form-control",
                                'placeholder' => "Price in USD",
                                'pattern' => '/^[0-9\.]+$/', 
                                'data-parsley-pattern-message' => "Can only contain number and dot.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group"> 
                        <div class="fm-label">
                            <label>Price in AED</label>
                        </div>                       
                        <div class="fm-control">
                            {!! Form::text('price_in_aed', null, [
                                'class' => "form-control",
                                'placeholder' => "Price in AED",
                                'pattern' => '/^[0-9\.]+$/', 
                                'data-parsley-pattern-message' => "Can only contain number and dot.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            {!! Form::checkbox('availability_web', 1) !!} Available on Web
            {{-- <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Price of the feature."></i>
            </div> --}}
        </h3>
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Price in INR</label>
                        </div>                        
                        <div class="fm-control">
                            {!! Form::text('web_price_in_inr', null, [
                                'class' => "form-control",
                                'placeholder' => "Price in INR",
                                'pattern' => '/^[0-9\.]+$/', 
                                'data-parsley-pattern-message' => "Can only contain number and dot.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Price in USD</label>
                        </div>                        
                        <div class="fm-control">
                            {!! Form::text('web_price_in_usd', null, [
                                'class' => "form-control",
                                'placeholder' => "Price in USD",
                                'pattern' => '/^[0-9\.]+$/', 
                                'data-parsley-pattern-message' => "Can only contain number and dot.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group"> 
                        <div class="fm-label">
                            <label>Price in AED</label>
                        </div>                       
                        <div class="fm-control">
                            {!! Form::text('web_price_in_aed', null, [
                                'class' => "form-control",
                                'placeholder' => "Price in AED",
                                'pattern' => '/^[0-9\.]+$/', 
                                'data-parsley-pattern-message' => "Can only contain number and dot.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $premium_feature->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $premium_feature->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\PremiumFeatureController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
$(function(){
    module.counter($('input[name="feature_name"]'), 100);
    module.counter($('textarea[name="feature_description"]'), 200);

    function availability() {
        let value = $('input[name=availability]:checked').val();
        if(value == 'undefined') {
            return;
        }
        if(value == 'iphone') {
            //document.getElementById('google_product_id_div').style.display = 'none';
            document.getElementById('apple_product_id_div').style.display = 'block';            
        } else if(value == 'android') {
            document.getElementById('apple_product_id_div').style.display = 'none'; 
            //document.getElementById('google_product_id_div').style.display = 'block';            
        } else if(value == 'web') {
            //document.getElementById('google_product_id_div').style.display = 'none';
            document.getElementById('apple_product_id_div').style.display = 'none'; 
        }
    }
    
    $("input[name='availability']").change(function(){
        availability();
    });

    availability();    
});
</script>
@stop