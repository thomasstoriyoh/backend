@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $push_notification->exists ? 'Update' : 'Create' }} Notification</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li class="blueText">Modules</li>
            <li><a href="{{ action('Admin\PushNotificationController@index') }}">Notification</a></li>
            <li>{{ $push_notification->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $push_notification->exists ? 'Update' : 'Create' }} Notification</p>
</div>

{!! Form::model($push_notification, [
    'method' => $push_notification->exists ? 'PUT' : 'POST',
    'action' => $push_notification->exists ?
        ['Admin\PushNotificationController@update', $push_notification->id] : 'Admin\PushNotificationController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all()))
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Title</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter notification title."
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Destination</label>
                        </div>                        
                        <div class="fm-control">
                            {!! Form::select('destination', $notification_type, null, [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'id' => "notification_type",
                                'data-parsley-required-message' => "Please select Destination Type.",
                                'data-parsley-errors-container'=> "#notification_type_err_id",
                            ]) !!}
                        </div>
                        <div id="notification_type_err_id"></div>
                    </div>
                </div>
            </div>
            
            <div class="row">                
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Target Group</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('target_group_id', ['' => 'Select...', 'add-target-group' => '+ Add New Target Group'] + $target_list, null, [
                                'class' => "form-control chosen-select",
                                'id' => 'target_group_id'
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Message</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Max 200 character"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('message', null, [
                                'class' => "form-control textarea-no-resize",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter message.",
                                'placeholder' => "Message",
                                'rows' => 6
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>

            <div class="row" id="app_id" style="display: none;">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>App Page Type</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('app_page', $ape_page_type, null, [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'id' => "app_page",
                                'data-parsley-required-message' => "Please select App Page Type.",
                                'data-parsley-errors-container'=> "#ape_page_type_err_id",
                            ]) !!}
                        </div>
                        <div id="ape_page_type_err_id"></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6" id="search_query_div">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Search Query</label>
                        </div>
                        <div class="fm-control">
                           {!! Form::text('search_query', null, [
                                'class' => "form-control",
                                'placeholder' => "Search Query",
                                'data-parsley-required-message' => "Please enter query tag.",                                
                            ]) !!} 
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-6" id="feed_div" style="display: none;">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Feed Type</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('feed_page', $ape_feed_type, null, [
                                'class' => "form-control chosen-select",
                                'id' => "feed_page"
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>

            <div class="row" id="url_id" style="display: ">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Target URL</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('url', null, [
                                'class' => "form-control",
                                'placeholder' => "Target URL",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter target url.",
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $push_notification->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $push_notification->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\PushNotificationController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@include('admin.modules.push-notification.modal')

@section('js')
<script type="text/javascript">
    $(function(){
        module.counter($('input[name="title"]'), 100);
        module.counter($('textarea[name="message"]'), 200);
    });

    var $app_block = $("#app_id"), $url_block = $("#url_id"), $module_form = $("#moduleForm"), 
        $search_query_div = $("#search_query_div"), $feed_div = $("#feed_div");

    function showNotificationDiv(content_type) {
        
        var type = $("#notification_type").val();
        
        if(type === "URL") {
            resetAPP(content_type);
            $app_block.hide();
            $url_block.show();
        } else if(type === "App") {
            resetURL(content_type);
            $url_block.hide();
            $app_block.show();
        } else {
            resetAll(content_type);            
            $app_block.hide();
            $url_block.hide();
        }
    }

    var $url = $url_block.find('input[name="url"]'), 
        $app_page = $app_block.find('select[name="app_page"]'), 
        $feed_page = $app_block.find('select[name="feed_page"]'),
        $search_query = $app_block.find('input[name="search_query"]');

    function resetURL(content_type) {

        $url.val('');
        $url.removeAttr('required');
        
        if ($("#app_page").val() == "Search" && $("#notification_type").val() == "App") {
            $search_query.attr('required', true);
        }
        
        if(content_type == 0) {
            $app_page.val('Search');
            $app_page.trigger('chosen:updated');        
        }

        $module_form.parsley().destroy();
        $module_form.parsley();
    }

    function resetAPP(content_type) {

        $url.attr('required', true);
        $app_page.removeAttr('required');

        $feed_div.hide();

        //if(content_type == 0) {
            $search_query.val('');
            $search_query.removeAttr('required'); 
            
            //$search_query.attr('required', true);
            
            $feed_page.val('Network');
            $feed_page.trigger('chosen:updated');
        //}
        
        $search_query_div.show();
        
        $module_form.parsley().destroy();
        $module_form.parsley();
    }
    
    function resetAll() {
        
        $url.val('');
        $url.removeAttr('required');
        
        $app_page.val('Search');
        $app_page.trigger('chosen:updated');        
        
        $search_query.val('');
        $search_query.removeAttr('required'); 
        
        $module_form.parsley().destroy();
        $module_form.parsley();
    }


    var content_type = {{ $push_notification->exists ? 1 : 0 }};
    //alert(content_type);

    $("#notification_type").change(function () {
        showNotificationDiv(content_type);
    });

    showNotificationDiv(content_type);

    function app_page_type_div() {
        
        //alert($("#notification_type").val());

        if ($("#app_page").val() == "Feed") {
            $search_query_div.hide();

            $search_query.removeAttr('required');

            $module_form.parsley().destroy();
            $module_form.parsley();

            $feed_div.show();
        } else if ($("#app_page").val() == "Search") {
            
            $feed_div.hide();
            //alert($("#notification_type").val());
            if($("#notification_type").val() == "App") {
                $search_query.attr('required', true);    
            }

            $feed_page.val('Network');
            $feed_page.trigger('chosen:updated');

            $module_form.parsley().destroy();
            $module_form.parsley();

            $search_query_div.show();        
        } else {
            $search_query_div.hide();
            $feed_div.hide();
        }
    }

    $("#app_page").change(function () {    
        app_page_type_div();
    });

    app_page_type_div();
    
    
    $(document).on("change", "select[name='target_group_id']", function() {
        if ($(this).val() == 'add-target-group') {
            var $modal = $("#modalForm");
            $modal.parsley().reset();
            $("#add_modal").modal('show');
            $(this).val('');
            $("select[name='target_group_id']").trigger("chosen:updated");
        }
    });

    $("#modalForm").submit(function () {
        
        var $obj = $(this);

        var formData = {
            title: $obj.find('input[name="target_title"]').val(),
            location: $obj.find('select[name="location[]"]').val(),
            os: $obj.find('select[name="os"]').val(),
            last_active: $obj.find('select[name="last_active"]').val(),            
            status: 1
        };

        if(formData.title != '') {
            $.ajax({
                url : "{{ action('Admin\TargetGroupController@store') }}",
                type: 'POST',
                data: formData,
                dataType: 'json'
            }).done(function(data) {
                if(data.status) {
                    console.log(data.status);
                    //var option = '<option value="'+ data.data.id +'">'+ data.data.title +'</option>';
                    $('#target_group_id').append($('<option value="'+ data.data.id +'">'+ data.data.title +'</option>'));
                    $('#target_group_id').trigger("chosen:updated");
                    
                    $("#add_modal").modal('hide');
                    $("#modalForm").parsley().reset();                
                }else{
                    module.notify(data.message, "error");
                }
            }).fail(function (data) {
                module.notify("Some error occurred during adding new target group.", "error");
                console.log(data.responseText);
            });
        }
        return false;
    });
</script>
@stop
