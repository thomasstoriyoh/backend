@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Notification</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li class="blueText">Modules</li>
            <li>Notification</li>
        </ul>
    </div>
    <p>Notification Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Notification Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.push-notification')
                    <a class="btn btn-default" href="{{ action('Admin\PushNotificationController@create') }}">ADD ITEM</a>
                @endCanI
                @CanI(['update.push-notification', 'delete.push-notification'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('update.push-notification')
                            <li><a href="javascript:void(0)" class="publish">Publish</a></li>
                            <li><a href="javascript:void(0)" class="draft">Draft</a></li>
                        @endCanI
                        @if(Auth::guard('admin')->user()->isSuperAdmin() && $module_details->maker_checker == 'On')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="approve">Approve</a></li>
                            <li><a href="javascript:void(0)" class="unapprove">Unapprove</a></li>
                        @endif
                        @CanI('update.push-notification')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="notification">Send Notification</a></li>
                        @endCanI
                        @CanI('delete.push-notification')
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="delete">Delete</a></li>
                        @endCanI
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
                <div class="rdtChosen">
                    <select name="" multiple data-placeholder="Filter options..."
                            class="chosen-select form-control" id="module_filter">
                        <optgroup label="Status">
                            <option value="status.Published">Published</option>
                            <option value="status.Draft">Draft</option>
                        </optgroup>

                        @if($module_details->maker_checker == 'On')
                        <optgroup label="Approval Status">
                            <option value="admin_status.Approved">Approved</option>
                            <option value="admin_status.Pending">Pending</option>
                        </optgroup>
                        @endif

                        <optgroup label="Target Group">
                            @foreach($target_list as $type)
                                <option value="target_group_id.{{ $type->id }}">{{ $type->title }}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    var table = $("#listing-table").bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Title", data: "title"},
            { title: "Destination", data: "destination"},
            { title: "Target", data: "target_group_id"},
            { title: "App Page", data: "app_page"},
            { title: "Status", data: "status"},
            { title: "Approval Status", data: "admin_status"},
            @CanI('edit.push-notification')
                { title: "Options", data: "options", sortable: false}
            @endCanI
        ]
    });

    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.publish", function() {
        var ids = module.getSelectedIds(table);

        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }

        module.confirm('Are you sure you want to publish these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.publish(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items published successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });

    $(document).on("click", "a.draft", function(){
        var ids = module.getSelectedIds(table);

        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }

        module.confirm('Are you sure you want to draft these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.draft(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items drafted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });

    $(document).on("click", "a.notification", function() {
        var ids = module.getSelectedIds(table);

        if(ids.length < 1) {
            module.notify('Please select some item to perform the action.', 'error');
            return false;
        }

        if(ids.length > 1) {
            module.notify('Notification must be processed one at a time.', 'error');
            return false;
        }

        module.confirm('Are you sure you want to send notification?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\PushNotificationController@notification") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('User notified successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });

    $(document).on("click", "a.delete", function(){
        var ids = module.getSelectedIds(table);

        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }

        module.confirm('Are you sure you want to delete these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.delete(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items deleted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });

    @if(Auth::guard('admin')->user()->isSuperAdmin() && $module_details->maker_checker == 'On')
    $(document).on("click", "a.approve", function(){
        var ids = module.getSelectedIds(table);

        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }

        module.confirm('Are you sure you want to approve these item(s)?', function(isConfirm){
            if(isConfirm){
                var ajax = module.approve(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) approved successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });

    $(document).on("click", "a.unapprove", function(){
        var ids = module.getSelectedIds(table);

        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }

        module.confirm('Are you sure you want to unapprove these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.unapprove(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) unapproved successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    @endif
});
</script>
@stop
