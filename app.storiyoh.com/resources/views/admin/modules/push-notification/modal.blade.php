<div class="modal fade" tabindex="-1" role="dialog" id="add_modal" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Target Group</h4>
            </div>
            {!! Form::open([
                'method' => 'POST',
                'url' => '',
                'id' => 'modalForm',
                'class' => 'form bordered-row',
                'autocomplete' => 'off',
                'data-parsley-validate' => "",
            ]) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Title</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::text('target_title', null, [
                                    'class' => "form-control",
                                    'placeholder' => "Title",
                                    'required' => true,
                                    'data-parsley-required-message' => "Please enter Group Name.",                                    
                                ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Location</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::select('location[]', $country_array, null, [
                                    'class' => "form-control chosen-select",
                                    'multiple' => true,
                                    'id' => "location",
                                ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Operating System</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::select('os', $os_array, null, [
                                    'class' => "form-control chosen-select",                                
                                    'id' => "os",
                                ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="fm-label">
                                <label>Last Active</label>
                            </div>
                            <div class="fm-control">
                                {!! Form::select('last_active', $last_active, null, [
                                    'class' => "form-control chosen-select",                                
                                    'id' => "last_active",
                                ]) !!}
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>