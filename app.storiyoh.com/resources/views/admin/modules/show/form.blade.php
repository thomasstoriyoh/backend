@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $show->exists ? 'Update' : 'Create' }} Show</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\ShowController@index') }}">Show</a></li>
            <li>{{ $show->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $show->exists ? 'Update' : 'Create' }} Masthead</p>
</div>

{!! Form::model($show, [
    'method' => $show->exists ? 'PUT' : 'POST',
    'action' => $show->exists ?
        ['Admin\ShowController@update', $show->id] : 'Admin\ShowController@add_show',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Podcast Title</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title of the podcast"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Podcast Title",
                                'id' => 'title',
                                'required' => true,
                                'data-parsley-required-message' => "Please enter podcast title.",
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Category</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Podcast Category"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('category_id[]', $categories, null, [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'multiple' => true,
                                'data-parsley-required-message' => "Please select category.",
                                'data-parsley-errors-container'=> "#category_id_err_id",
                                'id' => "category_id",
                            ]) !!}
                        </div>
                        <div id="category_id_err_id"></div>
                    </div>
                </div>
            </div>

            <div class="row">                               
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Collection Name</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Collection Name"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('collection_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Collection Name",
                                'id' => 'collection_name',
                                'required' => true,
                                'data-parsley-required-message' => "Please enter collection name.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Censored Name</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Censored Name"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('collection_censored_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Censored Name",
                                'id' => 'collection_censored_name',
                                'required' => true,
                                'data-parsley-required-message' => "Please enter censored name.",
                            ]) !!}
                        </div>
                    </div>
                </div> 
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Artist Name</label> 
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Multiple artist name with comma seperated"></i>
                            </div>                           
                        </div>
                        <div class="fm-control">
                            {!! Form::text('artist_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Artist Name",
                                'id' => 'artist_name',
                                'required' => true,
                                'data-parsley-required-message' => "Please enter artist name.",
                            ]) !!}
                        </div>
                    </div>
                </div>                               
            </div>

            <div class="row">                               
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Network</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Podcast Network"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('network_id', ["" => "Select Network"] + $network_list, null, [
                                'class' => "form-control chosen-select",
                                'id' => "network_id",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Language</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Podcast Language"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('language', ["" => "Select Language"] + $language_list, null, [
                                'class' => "form-control chosen-select",
                                'id' => "language",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Country</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Country"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('country', ["" => "Select Country"] + $country_list, null, [
                                'class' => "form-control chosen-select",
                                'id' => "country",
                                'required' => true,
                                'data-parsley-required-message' => "Please select Country.",
                                'data-parsley-errors-container'=> "#country_err_id",
                            ]) !!}
                        </div>
                        <div id="country_err_id"></div>
                    </div>
                </div>                              
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Description</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('description', null, [
                                'class' => "form-control textarea-no-resize",
                                'placeholder' => "Description",
                                'rows' => 6                                
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Feed URL</label> 
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Feed URL"></i>
                            </div>                           
                        </div>
                        <div class="fm-control">
                            {!! Form::text('feed_url', null, [
                                'class' => "form-control",
                                'placeholder' => "Feed URL",
                                'id' => 'feed_url',
                                'required' => true,
                                'data-parsley-required-message' => "Please enter feed url.",
                            ]) !!}
                        </div>
                    </div>
                </div>                
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Website URL</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Website URL"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('web_url', null, [
                                'class' => "form-control",
                                'placeholder' => "Website URL",
                                'id' => 'web_url'
                            ]) !!}
                        </div>
                    </div>
                </div>                                               
            </div>            
        </div>
    </div>
</div>
@if(!empty($show->image))
    <div class="panel image-panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Existing Image
            </h3>
            <div class="example-box-wrapper">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbnail-box-wrapper">
                        <div class="thumbnail-box">
                            <div class="thumb-content">
                                <div class="center-vertical">
                                    <div class="center-content">
                                        <div class="thumb-btn animated zoomIn">
                                            <a href="javascript:void(0)" class="btn btn-md btn-round btn-danger delete-image" title=""><i class="glyph-icon icon-remove"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="thumb-overlay bg-gray"></div>
                            <img src="{{ $show->getImage(200) }}" alt="">
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endif

@include('admin.partials.crop_upload', ['title' => 'Image', 'tooltip' => 'Minimum Image size should be 600 x 600', 'status' => empty($show->image)])

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\ShowController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    var item_exists = {{ $show->image ? 'false' : 'true' }};
    $(function(){
        /**
         * Uploader start.
         * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
         * If uploading resource is not image then it will ignore the crop size.
         *
         */
    
        $upQueue = $(".file-uploader");
        var crop_size = [600, 600];
    
        var uploader = $upQueue.bcmUpload({
            filters: { // Mime Types to be allowed to select.
                mime_types: [{
                    title: "Image files",
                    extensions: "jpg,gif,png,jpeg"
                }]
            },
            enable_crop: false, // Is cropping required or not? That's it. Cropping will be there for you.
            max_files: 1, // How many uploads allowed?
            required: item_exists, // Is the image upload mandatory?
            multipart_params: {resource: 'Image'}, // You can set it to your choice. You have to check for it in the controller to upload accordingly.
            min_dimention: { // This section is only required for image resources. It will be ignored for other files even if you set it.
                width: crop_size[0],
                height: crop_size[1]
            }
        });
    
        var uploaded = false;
        uploader.bind('UploadComplete', function (up, files) { // What happens when all the uploads are done.
            uploaded = true;
            $("#moduleForm").submit();
        });
        
        $('#moduleForm').submit(function() {
            if (uploader.files.length > 0 && uploaded == false) {
                uploader.start();

                return false;
            } else if (uploader.getOption('required') && uploader.files.length < 1) {
                module.notify('Please select some image(s) to proceed', 'error');

                return false;
            } else {
                return true;
            }
        });
    
        /**
         * Upload section end.
         */
        @if($show->exists)
            $("a.delete-image").click(function() {
                module.confirm('Are you sure you want to delete this image? ', function(isConfirm) {
                    if (isConfirm) {
                        var ajax = module.ajax('{{ action("Admin\\ShowController@deleteImage", $show->id) }}', {
                            type: 'Cover'
                        }, 'DELETE');
                        
                        ajax.done(function(data){
                            if(data.status) {
                                $("div.image-panel").toggle();
                                uploader.setOption('required', true);
                            }else{
                                module.notify(data.message, 'error');
                            }
                        });
                    }
                });                
            });
        @endif
    });
</script>
@stop