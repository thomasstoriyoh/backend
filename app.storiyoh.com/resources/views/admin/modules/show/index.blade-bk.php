@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Show</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li>Show</li>
        </ul>
    </div>
    <p>Show Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Show Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.shows')
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Create <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="{{ action('Admin\ShowController@create') }}">Add Podcast</a></li>
                        <li><a href="{{ action('Admin\ShowController@search_podcast_data') }}">Search Podcast</a></li>
                    </ul>
                {{--  <a class="btn btn-default" href="{{ action('Admin\ShowController@create') }}">ADD ITEM</a>  --}}
                @endCanI
                @CanI(['update.shows', 'delete.shows'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('update.shows')
                            <li><a href="javascript:void(0)" class="publish">Publish</a></li>
                            <li><a href="javascript:void(0)" class="draft">Draft</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="feature">Featured</a></li>
                            <li><a href="javascript:void(0)" class="unfeature">Unfeatured</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#add_csv_modal" data-toggle="modal">Upload CSV</a></li>
                        @endCanI                        
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
                <div class="rdtChosen">  
                    <select name="" multiple data-placeholder="Filter options..." 
                            class="chosen-select form-control" id="module_filter">
                        <optgroup label="Status">
                            <option value="status.Published">Published</option>
                            <option value="status.Draft">Draft</option>
                        </optgroup>

                        <optgroup label="Status">
                            <option value="featured.1">Featured</option>                            
                        </optgroup>
                        
                        <optgroup label="Category">
                        @foreach($category_list as $category)
                            <option value="category_id.{{ $category->id }}">{{ $category->title }}</option>
                        @endforeach
                    </optgroup>
                    </select>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>

@include('admin.partials.csv_modal')

@stop

@section('js')
<script type="text/javascript">
var table;
$(function(){
        table = $("#listing-table").bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Title", data: "title"},
            { title: "Image", data: "image", sortable: false},
            { title: "Categories", data: "categories", sortable: false},
            { title: "Episodes", data: "episodes", sortable: false},
            { title: "Featured", data: "featured", sortable: false},
            { title: "Active", data: "active", sortable: false},
            { title: "Status", data: "status", sortable: false},
            { title: "Options", data: "options", sortable: false}
        ]
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.publish", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }

        if(ids.length > 1) {
            module.notify('Podcast must be processed one at a time.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to publish this podcast?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.publish(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Podcast published successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.draft", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }

        if(ids.length > 1) {
            module.notify('Podcast must be processed one at a time.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to draft this podcast? If you draft this podcast, all episodes from this podcast will be drafted.', function(isConfirm){
            if (isConfirm) {
                var ajax = module.draft(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Podcast drafted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.fetch", function() {
        alert(1);
        {{--
        var ids = module.getSelectedIds(table);
        module.confirm('Are you sure you want to active these items?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\ShowController@activeStatus") }}', {"ids":ids, "type":'Yes'}, 'PATCH');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items status changes successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });--}}
    });
    
    {{--
    $(document).on("click", "a.active", function() {
        var ids = module.getSelectedIds(table);

        module.confirm('Are you sure you want to active these items?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\ShowController@activeStatus") }}', {"ids":ids, "type":'Yes'}, 'PATCH');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items status changes successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
        
    $(document).on("click", "a.inactive", function() {
        var ids = module.getSelectedIds(table);

        module.confirm('Are you sure you want to in-active these items?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\ShowController@activeStatus") }}', {"ids":ids, "type":'No'}, 'PATCH');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items status changes successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    --}}

    $(document).on("click", "a.feature", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        if(ids.length > 5) {
            module.notify('You can not feature more than 5 items.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to feature these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\ShowController@featured") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) featured successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.unfeature", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to unfeature these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\ShowController@unfeatured") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) unfeatured successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
});

function fetch_episode(id) {
    module.confirm('Are you sure you want to fetched all episodes?', function(isConfirm) {
        if(isConfirm) {
            var ajax = module.ajax('{{ action("Admin\ShowController@fetch_episodes") }}', {"show_id":id}, 'GET');
            ajax.done(function(data) {
                if (data.status) {
                    module.notify(data.message, 'success');
                }else{
                    module.notify(data.message, 'error');
                }                
            });
        }
    });    
}

function update_show(show_id, itune_id) {
    module.confirm('Are you sure you want to update show info?', function(isConfirm) {
        if(isConfirm) {
            var ajax = module.ajax('{{ action("Admin\ShowController@update_shows") }}', {"show_id" : show_id, "itune_id" : itune_id}, 'GET');
            ajax.done(function(data) {
                if (data.status) {
                    module.notify(data.message, 'success');
                }else{
                    module.notify(data.message, 'error');
                }                
            });
        }
    });    
}

function remove_duplicate_episode(id) {
    module.confirm('Are you sure?', function(isConfirm) {
        if(isConfirm) {
            var ajax = module.ajax('{{ action("Admin\ShowController@remove_duplicate_episode") }}', {"show_id":id}, 'GET');
            ajax.done(function(data) {
                if (data.status) {
                    module.notify(data.message, 'success');
                }else{
                    module.notify(data.message, 'error');
                }                
            });
        }
    });    
}
</script>
<script type="text/javascript">
    var up = {
        data_csv: {
            object: null,
            uploaded: false
        }
    };
    /**
    * Uploader start.
    * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
    * If uploading resource is not image then it will ignore the crop size.
    *
    */

   $upQueue = $(".file-uploader");

   var uploader = $upQueue.bcmUpload({
       filters: { // Mime Types to be allowed to select.
           mime_types: [{
               title: "CSV files",
               extensions: "csv"
           }]
       },
       max_files: 1, // How many uploads allowed?
       input_name: 'data_csv[]',
       required: true, // Is the image upload mandatory?
       multipart_params: {resource: 'File'} // You can set it to your choice. You have to check for it in the controller to upload accordingly.
   });

    up.data_csv.object = uploader;

    uploader.bind('UploadComplete', function (upl, files) { // What happens when all the uploads are done.
         up.data_csv.uploaded = true;
         $("#modalCsvForm").submit();
    });

    $("#modalCsvForm").submit(function () {
     var $obj = $(this);     
     if (up.data_csv.object.files.length > 0 && up.data_csv.uploaded == false) {
         up.data_csv.object.start();
         return false;
     } else {
         //alert(up.data_csv.object.files.length);
        if(up.data_csv.object.files.length == 0) {
            return false;
        }
        $("#add_csv_modal").modal('hide');
        //$("#modalCsvForm").parsley().reset();         
        //up.data_csv.object.splice();
        //table.ajax.reload();
        module.notify("CSV has been uploaded successfully.", "success");
        setTimeout(function(){
            window.location.href = '{{ action("Admin\\ShowController@index") }}';
        }, 1200);
     }
   });
</script>
@stop