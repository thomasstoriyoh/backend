@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $show->exists ? 'Update' : 'Create' }} Show</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\ShowController@index') }}">Show</a></li>
            <li>{{ $show->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $show->exists ? 'Update' : 'Create' }} Masthead</p>
</div>

{!! Form::model($show, [
    'method' => $show->exists ? 'PUT' : 'POST',
    'action' => $show->exists ?
        ['Admin\ShowController@update', $show->id] : 'Admin\ShowController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-5">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Keyword</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title of the show, Random keywords"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('keyword', null, [
                                'class' => "form-control",
                                'placeholder' => "keyword",
                                'id' => 'keyword'
                            ]) !!}
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-5">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>iTunes ID</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="iTunes id of the show"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('itunes_id', null, [
                                'class' => "form-control",
                                'placeholder' => "iTunes ID",
                                'id' => 'itunes_id'
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-2">
                    <div class="form-group" style="padding-top: 24px;">
                        <div class="fm-control">
                            <button type="button" id="searchButton" data-normal-text="Search"  class="btn btn-primary btn-block">Search</button>
                        </div>
                    </div>
                </div>                
            </div>            
        </div>
    </div>
</div>

<div id="searchBody">
    <div class="panel">
        <div class="panel-body">
            Podcast Listing will come here.
        </div>
    </div>
</div>

{!! Form::close() !!}

@stop

@section('js')
<script type="text/javascript">
    function searchPodcast() {
        
        if($('#keyword').val() == "" && $('#itunes_id').val() == "") {
           module.notify('Please enter keyword or itunes id.', 'error');
           return false;
        }
        
        $("#searchButton").button('loading');
        $("#searchBody").empty().html('<div class="panel"><div class="panel-body">Search Listing will come here.</div></div>')
        
        var ajax = module.ajax('{{ action("Admin\\ShowController@searchPodcast") }}', {
            search_key: $('#keyword').val()+'##'+$('#itunes_id').val(),
        }, 'GET');
        
        ajax.done(function(data){
            if(data.status) {
                $("#searchBody").html(data.data);
            }else{
                module.notify(data.message, 'error');
            }
            
            $("#searchButton").button("reset");
        });
    }
    
    $("#searchButton").click(function() {        
        searchPodcast();                    
    });
</script>
@stop