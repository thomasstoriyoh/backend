<?php
    $itunesData = session()->get('Itunes.Search');
    //dd($itunesData['data']->results);
    if($itunesData['status']) {
?>
@if(count($itunesData['data']->results))
<div class="panel">
    <div class="panel-body">
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-sm-10">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Show Name</label>
                        </div>                        
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Active Show?</label>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-body">
        @foreach($itunesData['data']->results as $item)
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-sm-10">
                    <div class="cg-group">
                        <div class="cg-content">
                            <input type="checkbox" name="ituneIDs[]" id="ituneID_{{ $item->collectionId }}" value="{{ $item->collectionId }}"> {{ $item->collectionName }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="cg-group">
                        <div class="cg-content">
                            <input type="checkbox" name="active[]" id="active_{{ $item->collectionId }}" value="{{ $item->collectionId }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\ShowController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
@else
    <div class="panel">
        <div class="panel-body">
            No Podcast Found.
        </div>
    </div>
@endif
<?php } else {?>
<div class="panel">
    <div class="panel-body">
        No Podcast Found.
    </div>
</div>
<?php } ?>
