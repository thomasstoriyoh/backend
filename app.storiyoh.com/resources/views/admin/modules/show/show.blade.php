@extends('admin.layouts.master')

@section('content')
    <div id="page-title">
        <h2>Show</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
                <li><a href="#menuLeft">Modules</a></li>
                <li><a href="{{ action('Admin\ShowController@index') }}">Show</a></li>
                <li>{{ $show->title }}</li>
            </ul>
        </div>
    </div>

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Show Details <!--<a onclick="fetch_episodes_via_local(<?php echo $show->id?>)">Fetch Episodes via Local</a>-->
            </h3>
            <div class="example-box-wrapper">
                <div class="row">                    
                    <div class="col-sm-4">
                        <div class="cg-group">
                            <div class="cg-label">
                                <label>Title</label>
                            </div>
                            <div class="cg-content">
                                {{ $show->title }}
                            </div>
                        </div>
                    </div>
                    @if($show->language)
                        <div class="col-sm-2">
                            <div class="cg-group">
                                <div class="cg-label">
                                    <label>Language</label>
                                </div>
                                <div class="cg-content">
                                    {{ @$show->language }}
                                </div>
                            </div>
                        </div>
                    @endif
                    
                    @if($show->country)
                        <div class="col-sm-2">
                            <div class="cg-group">
                                <div class="cg-label">
                                    <label>Country</label>
                                </div>
                                <div class="cg-content">
                                    {{ $show->country}}
                                </div>
                            </div>
                        </div>                    
                    @endif
                    
                    @if(@$show->network->title)
                        <div class="col-sm-4">
                            <div class="cg-group">
                                <div class="cg-label">
                                    <label>Network</label>
                                </div>
                                <div class="cg-content">
                                    {{ @$show->network->title}}
                                </div>
                            </div>
                        </div>                    
                    @endif
                </div>
                @if($show->categories()->count())
                    <div class="row">                    
                        <div class="col-sm-12">
                            <div class="cg-group">
                                <div class="cg-label">
                                    <label>Categories</label>
                                </div>
                                <div class="cg-content">
                                    {{ $show->categories()->pluck('title')->implode(', ') }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                
                @if($show->artist_name)
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="cg-group">
                                <div class="cg-label">
                                    <label>Artist</label>
                                </div>
                                <div class="cg-content">
                                    {{ $show->artist_name}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                
                @unless(empty($show->image))                    
                    <div class="col-sm-12">
                        <div class="cg-group">
                            <div class="cg-label">
                                <label>Image</label>
                            </div>
                            <div class="cg-content">
                                <img src="{{ $show->getImage(200) }}" alt="{{ $show->title }}" style="box-shadow:0 10px 16px 0 #CCC">
                            </div>
                        </div>
                    </div>
                @endunless
                                                
                @if($show->description)
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="cg-group">
                                <div class="cg-label">
                                    <label>Description</label>
                                </div>
                                <div class="cg-content">
                                    {!! $show->description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">Episodes</h3>
            <div class="table-responsive">
                <table id="listing-table"
                       class="table table-striped table-bordered responsive no-wrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Duration</th>
                        <th>Published On</th>
                        <th>Listen</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($show->episodes()->published()->orderBy('date_created', 'DESC')->get() as $key => $episode)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $episode->id }}</td>
                                <td>{{ $episode->title }}</td>
                                <td>{{ $episode->getDurationText() }}</td>
                                <td>{{ \Carbon\Carbon::parse($episode->date_created)->format('jS M Y') }}</td>
                                <td><a href="{{ $episode->mp3 }}" target="_blank">Play</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $('#listing-table').DataTable({
            language: {lengthMenu: '_MENU_ per page'}
        });
        $('.dataTables_filter input').attr("placeholder", "Search...");

        function fetch_episodes_via_local(id) {
        module.confirm('Are you sure you want to fetched all episodes?', function(isConfirm) {
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\ShowController@fetch_episodes_via_local") }}', {"show_id":id}, 'GET');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify(data.message, 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }                
                });
            }
        });    
    }
    </script>
@endsection
