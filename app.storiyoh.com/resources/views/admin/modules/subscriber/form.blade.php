@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $subscriber->exists ? 'Update' : 'Create' }} Subscriber</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\SubscriberController@index') }}">Subscriber</a></li>
            <li>{{ $subscriber->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $subscriber->exists ? 'Update' : 'Create' }} Subscriber</p>
</div>

{!! Form::model($subscriber, [
    'method' => $subscriber->exists ? 'PUT' : 'POST',
    'action' => $subscriber->exists ?
        ['Admin\SubscriberController@update', $subscriber->id] : 'Admin\SubscriberController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Email</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('email', null, [
                                'class' => "form-control",
                                'placeholder' => "Email",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter email."                                
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\SubscriberController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
    
</script>
@stop