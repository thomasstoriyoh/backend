@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Subscriber</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li>Subscriber</li>
        </ul>
    </div>
    <p>Subscriber Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Subscriber Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.subscriber')
                    <a class="btn btn-default" href="{{ action('Admin\SubscriberController@create') }}">ADD ITEM</a>
                @endCanI
                @CanI(['update.subscriber', 'delete.subscriber'], 'OR')
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @CanI('update.subscriber')
                            <li><a href="javascript:void(0)" class="active">Active</a></li>
                            <li><a href="javascript:void(0)" class="inactive">Inactive</a></li>
                            <li role="separator" class="divider"></li>                            
                        @endCanI
                        @CanI('delete.subscriber')
                            <li><a href="javascript:void(0)" class="delete">Delete</a></li>
                        @endCanI
                    </ul>
                </div>
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
                <div class="rdtChosen">  
                    <select name="" multiple data-placeholder="Filter options..." 
                            class="chosen-select form-control" id="module_filter">
                        <optgroup label="Status">
                            <option value="subscription_status.1">Active</option>
                            <option value="subscription_status.0">Inactive</option>                            
                        </optgroup>
                    </select>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                class="table table-striped table-bordered responsive no-wrap"
                cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
    var table = $("#listing-table").bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Email", data: "email", sortable: false},
            { title: "Status", data: "subscription_status", sortable: true}            
        ]
    });
    
    $("#module_filter").change(function(){
        table.ajax.reload();
    });

    $(document).on("click", "a.delete", function(){
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to delete these items?', function(isConfirm){
            if (isConfirm) {
                var ajax = module.delete(ids);
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Items deleted successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });

    $(document).on("click", "a.active", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
                
        module.confirm('Are you sure you want to active these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\SubscriberController@active") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) active successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
    
    $(document).on("click", "a.inactive", function() {
        var ids = module.getSelectedIds(table);
        
        if(ids.length < 1) {
            module.notify('Please select some items to perform the action.', 'error');
            return false;
        }
        
        module.confirm('Are you sure you want to inactive these item(s)?', function(isConfirm){
            if(isConfirm) {
                var ajax = module.ajax('{{ action("Admin\SubscriberController@inactive") }}', {"ids":ids}, 'PUT');
                ajax.done(function(data) {
                    if (data.status) {
                        module.notify('Item(s) inactive successfully.', 'success');
                    }else{
                        module.notify(data.message, 'error');
                    }
                    table.ajax.reload();
                });
            }
        });
    });
});
</script>
@stop