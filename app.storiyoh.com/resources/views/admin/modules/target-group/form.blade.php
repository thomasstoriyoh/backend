@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $target_group->exists ? 'Update' : 'Create' }} Target Group</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li class="blueText">Modules</li>
            <li><a href="{{ action('Admin\TargetGroupController@index') }}">Target Group</a></li>
            <li>{{ $target_group->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $target_group->exists ? 'Update' : 'Create' }} Target Group</p>
</div>

{!! Form::model($target_group, [
    'method' => $target_group->exists ? 'PUT' : 'POST',
    'action' => $target_group->exists ?
        ['Admin\TargetGroupController@update', $target_group->id] : 'Admin\TargetGroupController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Basic Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Group Name</label>                            
                        </div>
                        <div class="fm-control">
                            {!! Form::text('title', null, [
                                'class' => "form-control",
                                'placeholder' => "Group Name",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter Group Name.",                                
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Location</label>
                        </div>
                        <?php
//                            $value = null;
//                            if(!empty($target_group->country)){
//                                $value = json_decode($target_group->country);
//                            }
                            //dd($value, $country_array);
                        ?>
                        <div class="fm-control">
                            {!! Form::select('location[]', $country_array, null, [
                                'class' => "form-control chosen-select",
                                'multiple' => true,
                                'id' => "location",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Operating System</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('os', $os_array, null, [
                                'class' => "form-control chosen-select",                                
                                'id' => "os",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Last Active</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('last_active', $last_active, null, [
                                'class' => "form-control chosen-select",                                
                                'id' => "last_active",
                            ]) !!}
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>
@if(!empty($target_group->image))
<div class="panel image-panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Existing Image
        </h3>
        <div class="example-box-wrapper">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="thumbnail-box-wrapper">
                    <div class="thumbnail-box">
                        <div class="thumb-content">
                            <div class="center-vertical">
                                <div class="center-content">
                                    <div class="thumb-btn animated zoomIn">
                                        <a href="javascript:void(0)" class="btn btn-md btn-round btn-danger delete-image" title=""><i class="glyph-icon icon-remove"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="thumb-overlay bg-gray"></div>
                        <img src="{{ asset('uploads/product/brand/thumbs/640_480_' . $target_group->image) }}" alt="">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endif

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Publish Setting
            <div class="fm-tooltip">
                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Publish or Draft"></i>
            </div>
        </h3>
        <div class="example-box-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="fm-label">
                    </div>
                    <div class="fm-control">
                        <input type="checkbox" name="status" class="input-switch-alt" id="status" value="1" {{ $target_group->status == 'Published' ? 'checked' : '' }}>
                        <input type="hidden" class="status" name="status" value="{{ $target_group->status == 'Published' ? '1' : '' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\TargetGroupController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')

@stop