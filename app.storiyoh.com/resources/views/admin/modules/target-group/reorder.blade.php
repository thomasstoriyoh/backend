@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Product Brand Reorder</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url(config('app.admin_url')) }}">Catalyst WCMS</a></li>
            <li class="blueText">Modules</li>
            <li><a href="{{ action('Admin\VehicleTypeController@index') }}">Product Brand</a></li>
            <li>Reorder</li>
        </ul>
    </div>
    <p>Reorder Vehicle Type</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Reorder Items
        </h3>
        <div class="dd cg-dd" id="reorder">
            <ol class="dd-list">
                @foreach($items as $item)
                <li class="dd-item" data-id="{{ $item->id }}">
                    <div class="dd-handle">{{ $item->title }}</div>
                </li>
                @endforeach
                
            </ol>
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" data-loading-text="Please wait..." data-normal-text="Submit" id="sort_trigger" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\VehicleTypeController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('assets/widgets/nestable/nestable.js') }}"></script>
<script type="text/javascript">
$(function(){
    $('#reorder').nestable();
    
    $("#sort_trigger").click(function(){
        var data = {order : $('.dd').nestable('serialize')};
        var ajax = module.ajax('{{ action("Admin\\VehicleTypeController@postReorder") }}', data, 'PUT');
        ajax.done(function(data) {
            if (data.status) {
                module.notify('Items reordered successfully.', 'success');
                setTimeout(function(){
                    window.location.href = '{{ action("Admin\\VehicleTypeController@index") }}';
                }, 1200);
            }else{
                module.notify(data.message, 'error');
            }
        });
    });
});
</script>
@stop