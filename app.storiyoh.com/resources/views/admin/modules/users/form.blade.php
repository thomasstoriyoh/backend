@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>{{ $user->exists ? 'Update' : 'Create' }} User</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li><a href="{{ action('Admin\UserController@index') }}">User</a></li>
            <li>{{ $user->exists ? 'Update' : 'Create' }}</li>
        </ul>
    </div>
    <p>{{ $user->exists ? 'Update' : 'Create' }} User</p>
</div>

{!! Form::model($user, [
    'method' => $user->exists ? 'PUT' : 'POST',
    'action' => $user->exists ?
        ['Admin\UserController@update', $user->id] : 'Admin\UserController@store',
    'class' => 'form-horizontal bordered-row',
    'autocomplete' => 'off',
    'id' => 'moduleForm',
    'data-parsley-validate' => "",
    'files' => true,
]) !!}
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Information
        </h3>
        @if (!empty($errors->all())) 
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="col-md-6">
                <div class="alert alert-danger mrg10B">
                    <p>{{ @$error }}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="example-box-wrapper">           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>First name</label>
                            <div class="fm-tooltip">
                                <!-- <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i> -->
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('first_name', null, [
                                'class' => "form-control",
                                'placeholder' => "First name",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter first name.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Last name</label>
                            <div class="fm-tooltip">
                                <!-- <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i> -->
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('last_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Last name",
                                'required' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter last name.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Username</label>
                            <div class="fm-tooltip">
                                <!-- <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Title contain only alphanumeric,space, comma, dot, -, %, @, !, $, &quot;, ', *, ( ), &"></i> -->
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('user_name', null, [
                                'class' => "form-control",
                                'placeholder' => "Last name",
                                'required' => true,
                                'readonly' => true,
                                'pattern' => '/[\w\-\.\,\s%@!$"\'\*\(\)&]+$/',
                                'data-parsley-required-message' => "Please enter last name.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                  <!-- <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Gender</label>
                            <div class="fm-tooltip">
                             
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('gender', ['Male' => 'Male','Female' => 'Female'], null, [
                                'class' => "form-control chosen-select",
                                'required' => true,
                                'data-parsley-required-message' => "Please select gender.",
                                'data-parsley-errors-container'=> "#gender_err",
                                'id' => "faq_categories_id",
                            ]) !!}
                        </div>
                        <span id="gender_err"></span>
                    </div>
                </div> -->
            </div>
           
            <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Email</label>
                        </div>
                        <div class="fm-control">
                             {!! Form::text('email', null, [
                                'class' => "form-control",
                                'placeholder' => "Last name",
                                'required' => true,
                                'readonly' => true,
                            ]) !!}
                        </div>
                       
                    </div>
                </div>
                 <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Password</label>
                            <div class="fm-tooltip">
                                <!-- <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title=""
                                   data-original-title="Confirm Password of the Outlet"></i> -->
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::password('password', [
                                'class' => "form-control",
                                'placeholder' => "Password",
                            ]) !!}
                        </div>
                    </div>
                </div>
                  <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Confirm Password</label>
                            <div class="fm-tooltip">
                                <!-- <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title=""
                                   data-original-title="Confirm Password of the Outlet"></i> -->
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::password('password_confirmation', [
                                'class' => "form-control",
                                'placeholder' => "Confirm Password",
                                'data-parsley-equalto' => 'input[name="password"]',
                                'data-parsley-equalto-message' => "Confirmation password does not match.",
                            ]) !!}
                        </div>
                    </div>
                </div>
              
                <!-- <div class="col-xs-12 col-sm-12 col-md-3">
                  <div class="form-group">
                      <div class="fm-label">
                          <label>DOB</label>
                      </div>
                      <div class="fm-control">
                          {!! Form::text('dob', @$user->dob , [
                              'class' => "form-control bootstrap-datepicker",
                              'placeholder' => "Date",
                              'required' => true,
                              'readonly' => true,
                              
                          ]) !!}
                      </div> 
                  </div>
              </div> -->
            </div> 
            <!-- <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Address</label>
                            <div class="fm-tooltip">
                               
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('address', null, [
                                'class' => "form-control",
                                'placeholder' => "Address",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter address.",
                            ]) !!}
                        </div>
                    </div>
                </div> 
            </div> -->
            <!-- <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Phone</label>
                        </div>
                        <div class="fm-control">
                             {!! Form::text('phone', null, [
                                'class' => "form-control",
                                'placeholder' => "Phone",
                                'required' => true,
                                'data-parsley-type' => "digits",
                                'data-parsley-maxlength' => "15" 
                            ]) !!}
                        </div>
                       
                    </div>
                </div>
               
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Landmark</label>
                            <div class="fm-tooltip">
                           
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('landmark', null, [
                                'class' => "form-control",
                                'placeholder' => "Landmark",
                              
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>City</label>
                        </div>
                        <div class="fm-control">
                             {!! Form::text('city', null, [
                                'class' => "form-control",
                                'placeholder' => "City",
                                'required' => true,
                                'data-parsley-maxlength' => "15" 
                            ]) !!}
                        </div>
                       
                    </div>
            </div>
            </div> -->
            <!-- <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4">
                  <div class="form-group">
                      <div class="fm-label">
                          <label>Zipcode</label>
                      </div>
                      <div class="fm-control">
                          {!! Form::text('zipcode', @$user->zipcode , [
                              'class' => "form-control",
                              'placeholder' => "Zipcode",
                              'required' => true,
                              'data-parsley-required-message' => "Please select dob.",
                          ]) !!}
                      </div>
                  </div>
              </div>
             
            <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>State</label>
                        </div>
                        <div class="fm-control">
                             {!! Form::text('state', null, [
                                'class' => "form-control",
                                'placeholder' => "State",
                                'required' => true,
                                'data-parsley-maxlength' => "15" 
                            ]) !!}
                        </div>
                       
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Country</label>
                            <div class="fm-tooltip">
                              
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('country', @$country_list, null, [
                                'class' => "form-control chosen-select",
                                'multiple' => false,
                                'data-parsley-errors-messages-disabled'=>true,
                                'data-parsley-required-message' => "Please select country.",
                                'data-parsley-errors-container'=> "#country",
                                'id' => "country",
                            ]) !!}
                        </div>
                        <div id="country"></div>
                    </div>
            </div>
            </div> -->
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6 fm-submit">
                <button type="submit" id="moduleSubmit" data-loading-text="Please wait..." data-normal-text="Submit" class="btn loading-button btn-primary">Submit</button>
            </div>
            <div class="col-xs-6 fm-cancel text-right">
                <a href="{{ action('Admin\UserController@index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script type="text/javascript">
var item_exists = {{ $user->exists ? 'false' : 'true' }};
$(function(){

   // module.counter($('input[name="title"]'), 100);

    /**
     * Uploader start.
     * This is a universal uploader just set the $upQueue. and Crop Size i.e. minimum allowed image size.
     * If uploading resource is not image then it will ignore the crop size.
     *
     */

    

//    module.loadImageCropFunction(crop_size, uploader);

    /**
     * Upload section end.
     */


});
</script>
@stop