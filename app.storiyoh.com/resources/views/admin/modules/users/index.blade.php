@extends('admin.layouts.master')

@section('content')

<div id="page-title">
    <h2>User</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li>User</li>
        </ul>
    </div>
    <p>User Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            User Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.users')
                    <!-- <a class="btn btn-default" href="{{ action('Admin\FaqController@create') }}">ADD ITEM</a> -->
                @endCanI
                @CanI(['update.users', 'delete.users'], 'OR')
                    @if(Auth::guard('admin')->user()->isSuperAdmin())
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                @CanI('update.users')
                                    <li><a href="javascript:void(0)" class="publish">Verified</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="javascript:void(0)" class="draft">Disabled</a></li>
                                <!--      
                                    <li role="separator" class="divider"></li>
                                    <li><a href="javascript:void(0)" class="syncdata_android">Android Sync</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="javascript:void(0)" class="syncdata_ios">IOS Sync</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="javascript:void(0)" class="syncdata">Both Sync</a></li>
                                    -->
                                    <li role="separator" class="divider"></li>
                                    <li><a href="javascript:void(0)" class="enb_seller">Enable Seller</a></li>
                                    <li><a href="javascript:void(0)" class="dis_seller">Disabled Seller</a></li>

                                    <li role="separator" class="divider"></li>
                                
                                    <!-- <li><a href="javascript:void(0)" class="publish">Publish</a></li>
                                    <li><a href="javascript:void(0)" class="draft">Draft</a></li>
                                    <li><a href="{{ action('Admin\UserController@reorder') }}">Reorder</a></li> -->
                                    <li><a href="{{ action('Admin\UserController@downloadCsv') }}" data-toggle="modal">Download CSV</a></li>
                                @endCanI
                                @if(Auth::guard('admin')->user()->isSuperAdmin() && $module_details->maker_checker == 'On')
                                    <!-- <li role="separator" class="divider"></li> -->
                                    {{--  <li><a href="javascript:void(0)" class="approve">Activate</a></li>
                                    <li><a href="javascript:void(0)" class="unapprove">Deactivate</a></li>  --}}
                                @endif
                                @CanI('delete.users')
                                    {{--  <li role="separator" class="divider"></li>
                                    <li><a href="javascript:void(0)" class="delete">Delete</a></li>  --}}
                                @endCanI
                            </ul>
                        </div>
                    @endif
                @endCanI
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <!--Chosen Select-->
                <div class="rdtChosen">  
                    <select name="" multiple data-placeholder="Filter options..." 
                            class="chosen-select form-control" id="module_filter">
                        <!-- <optgroup label="Status">
                            <option value="status.Published">Active</option>
                            <option value="status.Draft">De-active</option>
                        </optgroup> -->
                        @if($module_details->maker_checker == 'On')
                            <optgroup label="User Status">
                                <option value="admin_status.Approved">Active</option>
                                <option value="admin_status.Pending">Inactive</option>
                            </optgroup>
                        @endif
                        <optgroup label="Register Type">
                            <option value="provider.Direct">Direct</option>
                            <option value="provider.Facebook">Facebook</option>
                            <option value="provider.Google">Google</option>
                            <option value="provider.Twitter">Twitter</option>
                        </optgroup>
                        <optgroup label="Type">
                            <option value="user_type.0">Guest</option>
                            <option value="user_type.1">Registered</option>
                        </optgroup>
                        <optgroup label="Seller">
                            <option value="seller.1">Seller</option>
                        </optgroup>                         
                    </select>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
    $(function(){
        var table = $("#listing-table").bcmTable({
            columns: [
                { title: "#", data: "order", sortable: false},
                { title: "Full Name", data: "full_name", sortable: false},
                { title: "Email", data: "email"},
                { title: "Username", data: "user_name"},
                { title: "Type", data: "type"},
                { title: "Provider", data: "provider"},
                { title: "Platform", data: "platform"},
                { title: "Status", data: "verified"},
                @CanI('edit.users')
                { title: "Options", data: "options", sortable: false}
                @endCanI
            ]
        });
        
        $("#module_filter").change(function(){ 
            table.ajax.reload();
        });
    
        $(document).on("click", "a.publish", function(){
            var ids = module.getSelectedIds(table);
            
            if(ids.length < 1) {
                module.notify('Please select some items to perform the action.', 'error');
                return false;
            }
            
            module.confirm('Are you sure you want to verified these items?', function(isConfirm){
                if (isConfirm) {
                    var ajax = module.publish(ids);
                    ajax.done(function(data) {
                        if (data.status) {
                            module.notify('Items verified successfully.', 'success');
                        }else{
                            module.notify(data.message, 'error');
                        }
                        table.ajax.reload();
                    });
                }
            });
        });
    
        $(document).on("click", "a.draft", function(){
            var ids = module.getSelectedIds(table);
            
            if(ids.length < 1) {
                module.notify('Please select some items to perform the action.', 'error');
                return false;
            }
            
            module.confirm('Are you sure you want to disabled these items?', function(isConfirm){
                if (isConfirm) {
                    var ajax = module.draft(ids);
                    ajax.done(function(data) {
                        if (data.status) {
                            module.notify('Items disabled successfully.', 'success');
                        }else{
                            module.notify(data.message, 'error');
                        }
                        table.ajax.reload();
                    });
                }
            });
        });

        $(document).on("click", "a.syncdata_android", function(){
            module.confirm('Are you sure you want to sync these items?', function(isConfirm){
                if (isConfirm) {
                    jQuery.ajax({
                    url: "{{ url('catalyst/module/users/syncdata_android') }}",
                    method: 'post',
                    data: {
                        // name: "test"
                    },
                    success: function(result){
                        if (result.status) {
                            module.notify(result.message, 'success');
                        }else{
                            module.notify(result.message, 'error');
                        }
                        //module.notify(result);
                        //console.log(result);
                        //alert(result);
                    }});
                }
            });
        });
        
        $(document).on("click", "a.syncdata_ios", function(){
            module.confirm('Are you sure you want to sync these items?', function(isConfirm){
                if (isConfirm) {
                    jQuery.ajax({
                    url: "{{ url('catalyst/module/users/syncdata_ios') }}",
                    method: 'post',
                    data: {
                        // name: "test"
                    },
                    success: function(result){
                        if (result.status) {
                            module.notify(result.message, 'success');
                        }else{
                            module.notify(result.message, 'error');
                        }
                        //console.log(result);
                        //alert(result);
                    }});
                }
            });
        });
        
        $(document).on("click", "a.syncdata", function(){
            module.confirm('Are you sure you want to sync these items?', function(isConfirm){
                if (isConfirm) {
                    jQuery.ajax({
                    url: "{{ url('catalyst/module/users/syncdata') }}",
                    method: 'post',
                    data: {
                        // name: "test"
                    },
                    success: function(result){
                        if (result.status) {
                            module.notify(result.message, 'success');
                        }else{
                            module.notify(result.message, 'error');
                        }
                        //console.log(result);
                        //alert(result);
                    }});
                }
            });
        });

        $(document).on("click", "a.enb_seller", function() {
            var ids = module.getSelectedIds(table);
            
            if(ids.length < 1) {
                module.notify('Please select some items to perform the action.', 'error');
                return false;
            }            
            
            module.confirm('Are you sure you want to enable these item(s)?', function(isConfirm){
                if(isConfirm) {
                    var ajax = module.ajax('{{ action("Admin\UserController@enabled_seller") }}', {"ids":ids}, 'PUT');
                    ajax.done(function(data) {
                        if (data.status) {
                            module.notify('Item(s) enabled successfully.', 'success');
                        }else{
                            module.notify(data.message, 'error');
                        }
                        table.ajax.reload();
                    });
                }
            });
        });
        
        $(document).on("click", "a.dis_seller", function() {
            var ids = module.getSelectedIds(table);
            
            if(ids.length < 1) {
                module.notify('Please select some items to perform the action.', 'error');
                return false;
            }
            
            module.confirm('Are you sure you want to disable these item(s)?', function(isConfirm){
                if(isConfirm) {
                    var ajax = module.ajax('{{ action("Admin\UserController@disabled_seller") }}', {"ids":ids}, 'PUT');
                    ajax.done(function(data) {
                        if (data.status) {
                            module.notify('Item(s) disabled successfully.', 'success');
                        }else{
                            module.notify(data.message, 'error');
                        }
                        table.ajax.reload();
                    });
                }
            });
        });
    });
</script>
@stop