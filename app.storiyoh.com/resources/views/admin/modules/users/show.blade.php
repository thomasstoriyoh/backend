@extends('admin.layouts.master')
@section('content')
<div id="page-title">
    <h2>User Details</h2>
    <!--Breadcrumb--> 
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li> 
            <li class="blueText">Modules</li>
            <li><a href="{{ action('Admin\UserController@index') }}">User</a></li>
            <li>Details</li>
        </ul>
    </div>
</div> 
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">Basic Information</h3>
        <div class="example-box-wrapper">
            <div class="row">
             <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Provider</label>
                        </div>
                        <div class="cg-content">
                            {{ $user->provider }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Full Name</label>
                        </div>
                        <div class="cg-content">
                            {{ $user->first_name }} {{ $user->last_name }}
                        </div>
                    </div>
                </div>
               
                
                <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Email Address</label>
                        </div>
                        <div class="cg-content">
                             {{ $user->email }} 
                        </div>
                    </div>
                </div>
               <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Username</label>
                        </div>
                        <div class="cg-content">
                            {{ $user->username }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Type</label>
                        </div>
                        <div class="cg-content">
                            {{ $user->type }}
                        </div>
                    </div>
                </div>
                
            </div>
            

        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">Storiyoh Information</h3>
        <div class="example-box-wrapper">
            <div class="row">
             <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Number of Boards</label>
                        </div>
                        <div class="cg-content">
                            {{ $user->boards()->count() }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Number of Followed Boards</label>
                        </div>
                        <div class="cg-content">
                            {{ $user->following_boards()->count() }}
                        </div>
                    </div>
                </div>
                  <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Number of Followers</label>
                        </div>
                        <div class="cg-content">
                            {{ $user->follower()->count() }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="cg-group">
                        <div class="cg-label">
                            <label>Number of Users Followed</label>
                        </div>
                        <div class="cg-content">
                            {{ $user->following()->count() }}
                        </div>
                    </div>
                </div>
              
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            
            <div class="col-xs-6 fm-cancel text-left">
                <a href="{{ action('Admin\UserController@index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
</div>
@stop

