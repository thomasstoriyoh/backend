<div class="panel {{ empty($class) ? 'image-panel' : 'image-panel-'.$class }}"
        {{ $status ? '' : 'style=display:none;' }}>
    <div class="panel-body">
        <h3 class="title-hero">
            Upload {{ empty($title) ? 'Image' : $title }}
            @if(! empty($tooltip))
                <div class="fm-tooltip">
                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title=""
                       data-original-title="{{ $tooltip }}"></i>
                </div>
            @endif
        </h3>
        <div class="example-box-wrapper">
            <div class="file-uploader" {{ empty($class) ? '' : 'id=file-uploader-'.$class }}></div>
        </div>
        <div class="file-uploader-helper"></div>
    </div>
</div>

<div class="panel crop-panel" style="display: none;">
    <div class="panel-body">
        <h3 class="title-hero">
            Crop Images
        </h3>
        <div class="example-box-wrapper">


        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-crop-image">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Crop Image</h4>
            </div>
            <div class="modal-body">
                <img src="" class="jcrop-preview-big" alt="Example" style="width: 200px;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary set-crop">Set Crop</button>
            </div>
        </div>
    </div>
</div>