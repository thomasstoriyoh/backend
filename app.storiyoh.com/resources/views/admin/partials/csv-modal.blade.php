<?php $modal_class = empty($class) ? '' : '_'.$class; ?>
<div class="modal fade" tabindex="-1" role="dialog" id="csv_modal{{ $modal_class }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload CSV</h4>
            </div>
            {!! Form::open([
                'method' => 'POST',
                'url' => '',
                'id' => 'modalCsvForm'.$modal_class,
                'class' => 'form bordered-row',
                'autocomplete' => 'off',
                'data-parsley-validate' => "",
                'onsubmit' => 'return false;'
            ]) !!}
            <div class="modal-body">
                @include('admin.partials.upload-modal', ['status' => true, 'prefix' => 'CSV', 'tooltip' => ''])

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Upload CSV</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>