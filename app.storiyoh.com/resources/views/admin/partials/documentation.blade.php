<ul id="{{ $panel_id }}">
    @foreach($sidebar_modules as $sidebar_module)
        @if(empty($sidebar_module->child->count()) && !empty($sidebar_module->controller))
            @CanI('view.'.$sidebar_module->slug)
            <li id="mmenu-item-link-{{ $sidebar_module->id }}">
                @if(class_exists('App\Http\Controllers\Admin\Documentation\\' . $sidebar_module->controller))
                    <a href="{{ @action('Admin\\Documentation\\' . $sidebar_module->controller . '@index') }}">
                        {{ $sidebar_module->title }}
                    </a>
                @else
                    <span>{{ $sidebar_module->title }}</span>
                @endif
            </li>
            @endCanI
        @else
            <li><span>{{ $sidebar_module->title }}</span>
                @if(!empty($sidebar_module->child->count()))
                    <?php $panel_id = 'mmenu-item-panel-'.$sidebar_module->id; ?>
                    @include('admin.partials.documentation', ['sidebar_modules' => $sidebar_module->child()->orderBy('order')->latest()->get(), 'panel_id' => $panel_id])
                    @else
                    <span>{{ $sidebar_module->title }}</span>
                @endif
            </li>
        @endif
    @endforeach
</ul>