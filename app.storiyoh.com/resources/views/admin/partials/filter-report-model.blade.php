    <div class="modal fade" tabindex="-1" role="dialog" id="apply_filter">
        <div class="modal-dialog modal-lg">
             <div class="modal-content">
                    <div class="modal-header"> User Report Filters
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                {!! Form::open([
                    'method' => 'POST',
                    'url' => '',
                    'id' => 'advanced_filter_form',
                    'class' => 'form bordered-row',
                    'autocomplete' => 'off',
                    'data-parsley-validate' => "",
                    'onsubmit' => 'return false;'
                ]) !!} 
                <div class="modal-body">
                    <div class="example-box-wrapper">
                        <div class="row">
                           <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <div class="fm-label">
                                        <label>Source</label>
                                        <div class="fm-tooltip">
                                            <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title=""
                                               data-original-title="Filter By Source"></i>
                                        </div>
                                    </div>
                                    <div class="fm-control">
                                        {!! Form::select('source', ['All' => 'All', 'Direct' => 'Direct', 'Facebook' => 'Facebook', 'Google' => 'Google', 'Twitter' => 'Twitter'], null, [
                                            'class' => "form-control chosen-select",
                                            'multiple' => false,
                                            'id' => "source",
                                        ]) !!}
                                    </div>
                                </div>
                            </div> 
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <div class="fm-label">
                                        <label>Type</label>
                                        <div class="fm-tooltip">
                                            <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title=""
                                               data-original-title="Filter By Registration Type"></i>
                                        </div>
                                    </div>
                                    <div class="fm-control">
                                        {!! Form::select('user_type', ['All' => 'All', '0' => 'Guest', '1' => 'Registered'], null, [
                                            'class' => "form-control chosen-select",
                                            'multiple' => false,
                                            'id' => "user_type",
                                        ]) !!}
                                    </div>
                                </div>
                            </div>                            
                         </div>
                    </div>
                    <div class="example-box-wrapper">
                        <div class="row">
                           <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <div class="fm-label">
                                        <label>Registration Date</label>
                                        <div class="fm-tooltip">
                                            <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title=""
                                               data-original-title="Filter By Registration Date"></i>
                                        </div>
                                    </div>
                                    <div class="fm-control">
                                        {!! Form::text('daterange', null, [
                                            'class' => "form-control ",
                                            'placeholder' => "YYYY-MM-DD",
                                            'data-start-date' => @date('Y-m-d'),
                                            'data-parsley-errors-container'=> "#start_date_err_id",
                                            'id'=>'daterangepicker',
                                        ]) !!}
                                    </div>
                                    <div id="start_date_err_id"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <div class="fm-label">
                                        <label>Status</label>
                                        <div class="fm-tooltip">
                                            <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title=""
                                               data-original-title="Filter By Current User Status"></i>
                                        </div>
                                    </div>
                                      <div class="fm-control">
                                        {!! Form::select('verified', ['All' => 'All', 'Verified' => 'Verified', 'Un-verified' => 'Unverified', 'Disabled' => 'Disabled'], 'All', [
                                            'class' => "form-control chosen-select",
                                            'multiple' => false,
                                            'id' => "status",
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button id="clearFilter" type="button" class="btn btn-default" >Clear Filters</button>
                  <button type="submit" id="moduleSubmit" class="btn btn-primary">Filter</button>
              </div>
               {!! Form::close() !!}  

         </div>
        </div>
    </div>

   <!--  <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 fm-submit">
                    <button type="submit" id="moduleSubmit" class="btn btn-primary">Generate</button>
                </div>
                
            </div>
        </div>
    </div> -->
  

<!-- Bootstrap Daterangepicker -->
