<div class="modal fade" tabindex="-1" role="dialog" id="apply_filter">
    <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">Report Filters
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            {!! Form::open([
                'method' => 'POST',
                'url' => '',
                'id' => 'advanced_filter_form',
                'class' => 'form bordered-row',
                'autocomplete' => 'off',
                'data-parsley-validate' => "",
                'onsubmit' => 'return false;'
            ]) !!} 
            <div class="modal-body">
                <div class="example-box-wrapper">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="fm-label">
                                    <label>Date Range</label>
                                    <div class="fm-tooltip">
                                        <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title=""
                                            data-original-title="Filter By Registration Date"></i>
                                    </div>
                                </div>
                                <div class="fm-control">
                                    {!! Form::text('daterange', $start_date . " to " . $end_date, [
                                        'class' => "form-control ",
                                        'placeholder' => "YYYY-MM-DD",
                                        'data-start-date' => @date('Y-m-d'),
                                        'data-parsley-errors-container'=> "#start_date_err_id",
                                        'id'=>'daterangepicker',
                                    ]) !!}
                                </div>
                                <div id="start_date_err_id"></div>
                            </div>
                        </div>                            
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="clearFilter" type="button" class="btn btn-default" >Clear Filters</button>
                <button type="submit" id="moduleSubmit" class="btn btn-primary">Filter</button>
            </div>
            {!! Form::close() !!}  

        </div>
    </div>
</div>
