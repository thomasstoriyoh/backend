    <!--Page Header-->
    <div id="page-header">
        <div id="header-nav-left">            
            <div class="user-account-btn dropdown">
                <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                    <img width="28" src="{{ asset('assets/admin/images/logo.png') }}" alt="Profile image">
                    <span>{{ Auth::guard('admin')->user()->first_name . " " . Auth::guard('admin')->user()->last_name }}</span>
                    <i class="glyph-icon icon-angle-down"></i>
                </a>
                <div class="dropdown-menu float-right">
                    <div class="box-sm">
                        <ul class="reset-ul mrg5B">
                            <li>
                                @if(Auth::guard('admin')->user()->isSuperAdmin())
                                <a href="{{ action('Admin\Core\AdminController@profile') }}">My Profile</a>
                                @endif
                                <a href="#">Last Login: {{ Auth::guard('admin')->user()->last_login ? Auth::guard('admin')->user()->last_login->diffForHumans() : '' }}</a>
                                <a href="#">Last Login From: {{ Auth::guard('admin')->user()->ip }}</a>
                            </li>
                        </ul>
                        <div class="button-pane button-pane-alt pad5L pad5R text-center">
                            <a href="{{ url("catalyst/logout") }}" class="btn btn-flat display-block font-normal btn-danger">
                                <i class="glyph-icon icon-power-off"></i> Logout
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="header-nav-right">            
            <i class="glyph-icon tooltip-button menu-icon icon-bars" title="" aria-describedby="tooltip102192"></i>            
        </div>
        <div class="header-nav-title">
            Catalyst
        </div>
    </div>
    
