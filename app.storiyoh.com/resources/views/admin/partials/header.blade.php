<!--Mmenu Left-->
    <nav id="menuLeft">
        <ul>
            <li><a href="{{ url('/catalyst') }}">Dashboard</a></li>
            <!--Modules-->
            <?php $show_module = false; ?>
            @if ($all_sidebar_modules->count())
                @foreach($all_sidebar_modules as $sidebar_module)
                    @CanI('view.'.$sidebar_module->slug)
                        <?php $show_module = true; ?>
                    @endCanI
                @endforeach
                
                @if($show_module)
                <li><span>Modules</span>
                    <ul id="mmenu-main-Module">
                        @foreach($all_sidebar_modules as $sidebar_module)
                            @if(empty($sidebar_module->child->count()) && !empty($sidebar_module->controller))
                                @CanI('view.'.$sidebar_module->slug)
                                    <li id="mmenu-item-link-{{ $sidebar_module->id }}">
                                        <?php $model = "App\\Models\\" . $sidebar_module->model; ?>
                                        <a href="{{ !empty($model::$routeRegistered) ? action('Admin\\' . $sidebar_module->controller . '@index') : 'javascript:void(0)' }}">
                                            {{ $sidebar_module->title }}
                                        </a>
                                    </li>
                                @endCanI
                            @elseif(!empty($sidebar_module->forward_to))
                                @CanI('view.'.$sidebar_module->slug)
                                <li id="mmenu-item-link-{{ $sidebar_module->id }}">

                                    <?php $model = "App\\Models\\" . $sidebar_module->forwarded->model; ?>
                                    <a href="{{ !empty($model::$routeRegistered) ? action('Admin\\' . $sidebar_module->forwarded->controller . '@index') : 'javascript:void(0)' }}">
                                        {{ $sidebar_module->title }}
                                    </a>
                                </li>
                                @endCanI
                            @else
                                <li><span>{{ $sidebar_module->title }}</span>
                                    @if(!empty($sidebar_module->child->count()))
                                        <?php $panel_id = 'mmenu-item-panel-'.$sidebar_module->id; ?>
                                        @include('admin.partials.sidebar', ['sidebar_modules' => $sidebar_module->child()->published()->orderBy('order')->latest()->get(), 'panel_id' => $panel_id])
                                    @endif
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                @endif
            @endif
            
            <!--Configs-->
            <?php $show_config = false; ?>
            @if ($all_sidebar_configs->count())
                @foreach($all_sidebar_configs as $sidebar_module)
                    @CanI('view.'.$sidebar_module->slug)
                        <?php $show_config = true; ?>
                    @endCanI
                @endforeach
                
                @if($show_config)
                <li><span>Configuration</span>
                    <ul id="mmenu-main-Configure">
                        @foreach($all_sidebar_configs as $sidebar_module)
                            @CanI('view.'.$sidebar_module->slug)
                                <li id="mmenu-item-link-{{ $sidebar_module->id }}">
                                    @if(class_exists('App\Http\Controllers\Admin\Core\\' . $sidebar_module->controller))
                                        <a href="{{ @action('Admin\\Core\\' . $sidebar_module->controller . '@index') }}">
                                            {{ $sidebar_module->title }}
                                        </a>
                                    @else
                                        <span>{{ $sidebar_module->title }}</span>
                                    @endif
                                </li>
                            @endCanI
                        @endforeach
                        @if(Auth::guard('admin')->user()->isSuperAdmin())
                            {{-- <li id="mmenu-item-link-module">
                                <a href="{{ @action('Admin\Core\ModuleController@settings') }}">
                                    Settings
                                </a>
                            </li> --}}
                            <li id="mmenu-item-link-log">
                                <a href="{{ @action('Admin\Core\LogController@index') }}">
                                    Admin Log
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
                @endif
            @endif

            <!--Reports-->
            <?php $show_reports = false; ?>
            @if ($all_sidebar_reports->count())
                @foreach($all_sidebar_reports as $sidebar_module)
                    @CanI('view.'.$sidebar_module->slug)
                    <?php $show_reports = true; ?>
                    @endCanI
                @endforeach
                <?php ?>
                @if($show_reports)
                    <li><span>Reports</span>
                        <ul id="mmenu-main-Report">
                            @foreach($all_sidebar_reports as $sidebar_module)
                                @if(empty($sidebar_module->child->count()) && !empty($sidebar_module->controller))
                                    @CanI('view.'.$sidebar_module->slug)
                                    <li id="mmenu-item-link-{{ $sidebar_module->id }}">
                                        @if($sidebar_module->slug == 'google-reports')
                                            @if(class_exists('App\Http\Controllers\Admin\Reports\Google\\' . $sidebar_module->controller))
                                                <a href="{{ @action('Admin\\Reports\\Google\\' . $sidebar_module->controller . '@index') }}">
                                                    {{ $sidebar_module->title }}
                                                </a>
                                            @else
                                                <span>{{ $sidebar_module->title }}</span>
                                            @endif
                                        @elseif($sidebar_module->slug == 'user-reports')
                                            @if(class_exists('App\Http\Controllers\Admin\Reports\Member\\' . $sidebar_module->controller))
                                                <a href="{{ @action('Admin\\Reports\\Member\\' . $sidebar_module->controller . '@index') }}">
                                                    {{ $sidebar_module->title }}
                                                </a>
                                            @else
                                                <span>{{ $sidebar_module->title }}</span>
                                            @endif
                                        @elseif($sidebar_module->slug == 'sales-reports')
                                            @if(class_exists('App\Http\Controllers\Admin\Reports\Sales\\' . $sidebar_module->controller))
                                                <a href="{{ @action('Admin\\Reports\\Sales\\' . $sidebar_module->controller . '@index') }}">
                                                    {{ $sidebar_module->title }}
                                                </a>
                                            @else
                                                <span>{{ $sidebar_module->title }}</span>
                                            @endif
                                        @endif
                                    </li>
                                    @endCanI
                                @else
                                    <li><span>{{ $sidebar_module->title }}</span>
                                        @if(!empty($sidebar_module->child->count()))
                                            <?php $panel_id = 'mmenu-item-panel-'.$sidebar_module->id; ?>
                                            @include('admin.partials.reports', ['sidebar_modules' => $sidebar_module->child()->orderBy('order')->latest()->get(), 'panel_id' => $panel_id])
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @endif
                <?php ?>
            @endif

            <!--Reports-->
            <?php $show_docs = false; ?>
            @if ($all_sidebar_docs->count())
                @foreach($all_sidebar_docs as $sidebar_module)
                    @CanI('view.'.$sidebar_module->slug)
                    <?php $show_docs = true; ?>
                    @endCanI
                @endforeach

                @if($show_docs)
                    <li><span>Documentation</span>
                        <ul id="mmenu-main-Documentation">
                            @foreach($all_sidebar_docs as $sidebar_module)
                                @if(empty($sidebar_module->child->count()) && !empty($sidebar_module->controller))
                                    @CanI('view.'.$sidebar_module->slug)
                                    <li id="mmenu-item-link-{{ $sidebar_module->id }}">
                                        @if(class_exists('App\Http\Controllers\Admin\Documentation\\' . $sidebar_module->controller))
                                            <a href="{{ @action('Admin\\Documentation\\' . $sidebar_module->controller . '@index') }}">
                                                {{ $sidebar_module->title }}
                                            </a>
                                        @else
                                            <span>{{ $sidebar_module->title }}</span>
                                        @endif
                                    </li>
                                    @endCanI
                                @else
                                    <li><span>{{ $sidebar_module->title }}</span>
                                        @if(!empty($sidebar_module->child->count()))
                                            <?php $panel_id = 'mmenu-item-panel-'.$sidebar_module->id; ?>
                                            @include('admin.partials.documentation', ['sidebar_modules' => $sidebar_module->child()->orderBy('order')->latest()->get(), 'panel_id' => $panel_id])
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endif
            @if(Auth::guard('admin')->user()->type == "SA")
                <li><a href="{{ url('/horizon') }}" target="_blank">Horizon</a></li>
                <li><a href="{{ url('/log-viewer') }}" target="_blank">Log Viewer</a></li>
            @endif
        </ul>
    </nav>
    
    <!--Page Header-->
    <div id="page-header">
        <div id="header-nav-left">
            @if($all_languages['status'])
                <div class="language-btn dropdown">
                    <a href="#" class="clearfix" data-toggle="dropdown">
                        <span>{{ session('current_language') ? strtoupper(session('current_language')) : strtoupper($all_languages['langs'][0]->locale) }}</span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-right">
                        <ul class="reset-ul mrg5B">
                            <li>
                                @foreach($all_languages['langs'] as $langs)
                                    <a href="{{ action('Admin\Core\DashboardController@show', $langs->locale) }}">{{ $langs->title }} ( {{ strtoupper($langs->locale) }} )</a>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            @endif
            <div class="user-account-btn dropdown">
                <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                    <img width="28" src="{{ asset('assets/admin/images/logo.png') }}" alt="Profile image">
                    <span>{{ Auth::guard('admin')->user()->first_name . " " . Auth::guard('admin')->user()->last_name }}</span>
                    <i class="glyph-icon icon-angle-down"></i>
                </a>
                <div class="dropdown-menu float-right">
                    <div class="box-sm">
                        <ul class="reset-ul mrg5B">
                            <li>
                                @if(Auth::guard('admin')->user()->isSuperAdmin())
                                <a href="{{ action('Admin\Core\AdminController@profile') }}">My Profile</a>
                                @endif
                                <a href="#">Last Login: {{ Auth::guard('admin')->user()->last_login ? Auth::guard('admin')->user()->last_login->diffForHumans() : '' }}</a>
                                <a href="#">Last Login From: {{ Auth::guard('admin')->user()->ip }}</a>
                            </li>
                        </ul>
                        <div class="button-pane button-pane-alt pad5L pad5R text-center">
                            <a href="{{ url("catalyst/logout") }}" class="btn btn-flat display-block font-normal btn-danger">
                                <i class="glyph-icon icon-power-off"></i> Logout
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="header-nav-right">
            <a href="#menuLeft">
                <i class="glyph-icon tooltip-button menu-icon icon-bars" title="" aria-describedby="tooltip102192"></i>
            </a>
        </div>
        <div class="header-nav-title">
            Catalyst
        </div>
    </div>
    
