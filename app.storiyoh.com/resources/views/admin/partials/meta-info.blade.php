<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Meta Information
        </h3>
        <div class="example-box-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Meta Title</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('meta_title', null, [
                                'class' => "form-control",
                                'placeholder' => "Meta Title",
                                'required' => true,
                                'data-parsley-required-message' => "Please enter meta title.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>URL Slug</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Unique URL Slug"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::text('slug', null, [
                                'class' => "form-control",
                                'placeholder' => "Slug",
                                'required' => true,
                                'pattern' => '/^[a-zA-Z0-9\-]+$/',
                                'data-parsley-required-message' => "Please enter slug.",
                                'data-parsley-pattern-message' => "Can only contain Letters.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Meta Keywords</label>
                            <div class="fm-tooltip">
                                <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Please use comma(,) for multiple keywords"></i>
                            </div>
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('meta_keyword', null, [
                                'class' => "form-control textarea-no-resize",
                                'placeholder' => "Short Keywords",
                                'required' => true,
                                'rows' => 6,
                                'data-parsley-required-message' => "Please enter meta keyword.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Meta Description</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::textarea('meta_description', null, [
                                'class' => "form-control textarea-no-resize",
                                'placeholder' => "Meta Description",
                                'required' => true,
                                'rows' => 6,
                                'data-parsley-required-message' => "Please enter meta description.",
                                'data-parsley-minlength' => '[3]',
                                'data-parsley-minlength-message' => "Min. 3 characters required.",
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>