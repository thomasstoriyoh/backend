<ul id="{{ $panel_id }}">
    @foreach($sidebar_modules as $sidebar_module)
        @if(empty($sidebar_module->child->count()) && !empty($sidebar_module->controller))
            <li id="mmenu-item-link-{{ $sidebar_module->id }}">
                <?php $model = "App\Models\\" . $sidebar_module->model; ?>
                    <a href="{{ !empty($model::$routeRegistered) ? action('Admin\\' . $sidebar_module->controller . '@index') : 'javascript:void(0)' }}">
                    {{ $sidebar_module->title }}
                </a>
            </li>
        @else
            <li><span>{{ $sidebar_module->title }}</span>
                @if(!empty($sidebar_module->child->count()))
                    <?php $panel_id = 'mmenu-item-panel-'.$sidebar_module->id; ?>
                    @include('admin.partials.sidebar', ['sidebar_modules' => $sidebar_module->child, 'panel_id' => $panel_id])
                @endif
            </li>
        @endif
    @endforeach
</ul>