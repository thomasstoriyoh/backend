<div id="page-title">
                    <h2>Forms elements</h2>
                    <p>Components for creating great user experiences.</p>

                </div>

                <!--Code Am-->
                <div class="panel">
                    <div class="panel-body">
                        <h3 class="title-hero">
                            Elements
                        </h3>
                        <div class="example-box-wrapper">
                            <form class="form-horizontal bordered-row">

                                <!--Input text feilds-->                
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" class="form-control" id="" placeholder="Example placeholder...">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Chosen select-->                
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Chosen select</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Chosen select"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <select name="" class="chosen-select">
                                                    <optgroup label="Option group 1">
                                                        <option>Option 1</option>
                                                        <option>Option 2</option>
                                                        <option>Option 3</option>
                                                        <option>Option 4</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 2">
                                                        <option>Option 5</option>
                                                        <option>Option 6</option>
                                                        <option>Option 7</option>
                                                        <option>Option 8</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 3">
                                                        <option>Option 9</option>
                                                        <option>Option 10</option>
                                                        <option>Option 11</option>
                                                        <option>Option 12</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Chosen select</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Chosen select"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <select name="" class="chosen-select">
                                                    <optgroup label="Option group 1">
                                                        <option>Option 1</option>
                                                        <option>Option 2</option>
                                                        <option>Option 3</option>
                                                        <option>Option 4</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 2">
                                                        <option>Option 5</option>
                                                        <option>Option 6</option>
                                                        <option>Option 7</option>
                                                        <option>Option 8</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 3">
                                                        <option>Option 9</option>
                                                        <option>Option 10</option>
                                                        <option>Option 11</option>
                                                        <option>Option 12</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Chosen select</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Chosen select"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <select name="" class="chosen-select">
                                                    <optgroup label="Option group 1">
                                                        <option>Option 1</option>
                                                        <option>Option 2</option>
                                                        <option>Option 3</option>
                                                        <option>Option 4</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 2">
                                                        <option>Option 5</option>
                                                        <option>Option 6</option>
                                                        <option>Option 7</option>
                                                        <option>Option 8</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 3">
                                                        <option>Option 9</option>
                                                        <option>Option 10</option>
                                                        <option>Option 11</option>
                                                        <option>Option 12</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Chosen select</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Chosen select"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <select name="" class="chosen-select">
                                                    <optgroup label="Option group 1">
                                                        <option>Option 1</option>
                                                        <option>Option 2</option>
                                                        <option>Option 3</option>
                                                        <option>Option 4</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 2">
                                                        <option>Option 5</option>
                                                        <option>Option 6</option>
                                                        <option>Option 7</option>
                                                        <option>Option 8</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 3">
                                                        <option>Option 9</option>
                                                        <option>Option 10</option>
                                                        <option>Option 11</option>
                                                        <option>Option 12</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!--Chosen multi select-->                
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Chosen multi select</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Chosen multi select"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <select name="" multiple data-placeholder="Click to see available options..." class="chosen-select">
                                                    <optgroup label="Option group 1">
                                                        <option>Option 1</option>
                                                        <option>Option 2</option>
                                                        <option>Option 3</option>
                                                        <option>Option 4</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 2">
                                                        <option>Option 5</option>
                                                        <option>Option 6</option>
                                                        <option>Option 7</option>
                                                        <option>Option 8</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 3">
                                                        <option>Option 9</option>
                                                        <option>Option 10</option>
                                                        <option>Option 11</option>
                                                        <option>Option 12</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Chosen multi select</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Chosen multi select"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <select name="" multiple data-placeholder="Click to see available options..." class="chosen-select">
                                                    <optgroup label="Option group 1">
                                                        <option>Option 1</option>
                                                        <option>Option 2</option>
                                                        <option>Option 3</option>
                                                        <option>Option 4</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 2">
                                                        <option>Option 5</option>
                                                        <option>Option 6</option>
                                                        <option>Option 7</option>
                                                        <option>Option 8</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 3">
                                                        <option>Option 9</option>
                                                        <option>Option 10</option>
                                                        <option>Option 11</option>
                                                        <option>Option 12</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Chosen multi select</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Chosen multi select"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <select name="" multiple data-placeholder="Click to see available options..." class="chosen-select">
                                                    <optgroup label="Option group 1">
                                                        <option>Option 1</option>
                                                        <option>Option 2</option>
                                                        <option>Option 3</option>
                                                        <option>Option 4</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 2">
                                                        <option>Option 5</option>
                                                        <option>Option 6</option>
                                                        <option>Option 7</option>
                                                        <option>Option 8</option>
                                                    </optgroup>
                                                    <optgroup label="Option group 3">
                                                        <option>Option 9</option>
                                                        <option>Option 10</option>
                                                        <option>Option 11</option>
                                                        <option>Option 12</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Multi select list-->                
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Multi select list</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Multi select list"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <select multiple class="multi-select" name="">
                                                    <option>Dallas Cowboys</option>
                                                    <option>New York Giants</option>
                                                    <option>Philadelphia Eagles</option>
                                                    <option>Washington Redskins</option>
                                                    <option>Chicago Bears</option>
                                                    <option>Detroit Lions</option>
                                                    <option>Green Bay Packers</option>
                                                    <option>Minnesota Vikings</option>
                                                    <option>Atlanta Falcons</option>
                                                    <option>Carolina Panthers</option>
                                                    <option>New Orleans Saints</option>
                                                    <option>Tampa Bay Buccaneers</option>
                                                    <option>Arizona Cardinals</option>
                                                    <option>St. Louis Rams</option>
                                                    <option>San Francisco 49ers</option>
                                                    <option>Seattle Seahawks</option>
                                                    <option>Buffalo Bills</option>
                                                    <option>Miami Dolphins</option>
                                                    <option>New England Patriots</option>
                                                    <option>New York Jets</option>
                                                    <option>Baltimore Ravens</option>
                                                    <option>Cincinnati Bengals</option>
                                                    <option>Cleveland Browns</option>
                                                    <option>Pittsburgh Steelers</option>
                                                    <option>Houston Texans</option>
                                                    <option>Indianapolis Colts</option>
                                                    <option>Jacksonville Jaguars</option>
                                                    <option>Tennessee Titans</option>
                                                    <option>Denver Broncos</option>
                                                    <option>Kansas City Chiefs</option>
                                                    <option>Oakland Raiders</option>
                                                    <option>San Diego Chargers</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!--Custom checkbox info color-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Custom checkbox</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Custom checkbox"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <div class="fm-radio-inline-block">
                                                    <div class="checkbox checkbox-info">
                                                        <label>
                                                            <input type="checkbox" id="inlineCheckbox114" checked class="custom-checkbox">
                                                            Checkbox info
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="fm-radio-inline-block">
                                                    <div class="checkbox checkbox-info">
                                                        <label>
                                                            <input type="checkbox" id="inlineCheckbox114" class="custom-checkbox">
                                                            Checkbox info
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="fm-radio-inline-block">
                                                    <div class="checkbox checkbox-info">
                                                        <label>
                                                            <input type="checkbox" id="inlineCheckbox114" class="custom-checkbox">
                                                            Checkbox info
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Custom radio info color-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Custom radio</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Custom radio"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <div class="fm-radio-inline-block">
                                                    <div class="radio radio-info">
                                                        <label>
                                                            <input type="radio" id="inlineRadio114" checked name="example-radio" class="custom-radio">
                                                            Radio info
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="fm-radio-inline-block">
                                                    <div class="radio radio-info">
                                                        <label>
                                                            <input type="radio" id="inlineRadio114" name="example-radio" class="custom-radio">
                                                            Radio info
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="fm-radio-inline-block">
                                                    <div class="radio radio-info">
                                                        <label>
                                                            <input type="radio" id="inlineRadio114" name="example-radio" class="custom-radio">
                                                            Radio info
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Input switch alternate-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input switch alternate</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input switch alternate"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="checkbox" class="input-switch-alt">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Textarea auto resize-->                
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Textarea auto resize</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Textarea auto resize"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <textarea name="" rows="3" class="form-control textarea-autosize"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Autocomplete-->                
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Autocomplete</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Autocomplete"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input type="text" name="" placeholder="Start typing to see results..." class="form-control autocomplete-input">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Touchspin alternate-->                
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Touchspin alternate</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Touchspin alternate"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <input id="touchspin-demo-4" class="form-control" type="text" value="" name="touchspin-demo-4">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <h3 class="title-hero">
                            Input groups
                        </h3>
                        <div class="example-box-wrapper">
                            <form class="form-horizontal bordered-row">

                                <!--Input group icon-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>Input group icon</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="Input group icon"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-black">
                                                        <i class="glyph-icon icon-lock"></i>
                                                    </span>
                                                    <input type="text" class="form-control" placeholder="Username">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <h3 class="title-hero">
                            File upload
                        </h3>
                        <div class="example-box-wrapper">
                            <form class="form-horizontal bordered-row">

                                <!--File button-->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <div class="fm-label">
                                                <label>File button</label>
                                                <div class="fm-tooltip">
                                                    <i class="glyph-icon tooltip-button demo-icon icon-info-circle" title="" data-original-title="File button"></i>
                                                </div>
                                            </div>
                                            <div class="fm-control">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="...">
                                                    </span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                
                            </form>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body text-center">
                       <button class="btn btn-primary">Submit</button>
                    </div>
                </div>