<ol class="dd-list">
    @foreach($items as $item)
    <li class="dd-item" data-id="{{ $item->id }}">
        <div class="dd-handle">{{ $item->title }}</div>
        @if($item->child->toArray())
            @include('admin.partials.sort', ['items' => $item->child()->orderBy('order')->latest()->get()])
        @endif
    </li>
    @endforeach
</ol>