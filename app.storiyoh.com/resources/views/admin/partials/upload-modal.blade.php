<div class="example-box-wrapper">
    <div class="file-uploader" {{ empty($class) ? '' : 'id=file-uploader-'.$class }}></div>
</div>
<div class="file-uploader-helper"></div>