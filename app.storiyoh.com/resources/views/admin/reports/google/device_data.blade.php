@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Google Analytics Report</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Reports</a></li>
            <li class="blueText">Google Analytics Report</li>
            <li>Device Info</li>
        </ul>
    </div>
    @include("admin.reports.google.tab")
    <!-- <p>Delight UI comes packed with a lot of options</p> -->
</div>

@include("admin.reports.google.top_google_chart")
<div class="row">
    <div class="col-md-6">
        <div class="dashboard-box bg-white mrgB20">
            <div class="text-center pad0A">
                <div style="width:100%;height:370px;display:inline-block;">
                    <div id="osDonutChart" style="width:100%;height:100%;"></div>
                </div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Operating System
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="dashboard-box bg-white mrgB20">
            <div class="text-center pad0A">
                <div style="width:100%;height:370px;display:inline-block;">
                    <div id="osVersionDonutChart" style="width:100%;height:100%;"></div>
                </div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Operating System Version
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="dashboard-box bg-white mrgB20">
            <div class="text-center pad0A">
                <div style="width:100%;height:370px;display:inline-block;">
                    <div id="brandDonutChart" style="width:100%;height:100%;"></div>
                </div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Mobile Brands 
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="dashboard-box bg-white mrgB20">
            <div class="text-center pad0A">
                <div style="width:100%;height:370px;display:inline-block;">
                    <div id="modelDonutChart" style="width:100%;height:100%;"></div>
                </div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Mobile Model
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script>
	var osviewdata, osversionviewdata, brandviewdata,modelviewdata ="";
	osviewdata = {!! json_encode($osData) !!};
	osversionviewdata = {!! json_encode($osVersionData) !!};
	brandviewdata = {!! json_encode($brandData) !!};
	modelviewdata = {!! json_encode($modelData) !!};
</script>

<!-- Google Chart Library -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/analytics/top-chart-data.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/analytics/google-chart-device-data.js') }}"></script>

<!-- Bootstrap Daterangepicker -->
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker-demo.js') }}"></script>

<!-- Tabdrop Responsive -->
{{--<script type="text/javascript" src="{{ asset('assets/widgets/tabs/tabs-responsive.js') }}"></script>--}}
<script type="text/javascript">
    /* Responsive tabs */
    $(function() {
        "use strict";
        $('.nav-responsive').tabdrop();
    });
</script>
@stop