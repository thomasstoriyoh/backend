@extends('admin.layouts.master')

@section('content')

<div id="page-title">
    <h2>Google Analytics Report</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Reports</a></li>
            <li class="blueText">Google Analytics Report</li>
            <li>Geo Location</li>
        </ul>
    </div>
    @include("admin.reports.google.tab")
    <!-- <p>Delight UI comes packed with a lot of options</p> -->
</div>

@include("admin.reports.google.top_google_chart")

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            {{ ucwords($place_type) }}
        </h3>
        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
	</div>
</div>

@stop

@section('js')
<script>
var dataSet = {!! json_encode($analyticsData) !!};
var place_type = '{{ $place_type }}';
$(document).ready(function() {
    $('#listing-table').DataTable({
    	dom: '<"datatable-header"><"datatable-scroll"t>'+
                    '<"datatable-footer"<"row"<"col-md-3"l><"col-md-3"i><"col-md-3"<"clearfix"f>><"col-md-3"p>>>',
        data: dataSet,
        columns: [
            { title: "{{ ucwords($place_type) }}"},
            { title: "Sessions"},
            @if($place_type !='networkdomain'){ title: "Options", sortable: false} @endif
        ],
        language: {
          lengthMenu: '_MENU_ per page'
        },
        @if($place_type !='networkdomain')
        "columnDefs": [{
		    "targets": 2,
		    "data": "download_link",
		    "render": function (data, type, row, meta) {
		    	console.log(row[0]);
		      return '<a href="?type='+place_type+'&value='+row[0]+'">View More</a>';
    		}
  		}]
 		@endif
    });
    $('.dataTables_filter input').attr("placeholder", "Search...");
});
</script>

<!-- Google Chart Library -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/analytics/top-chart-data.js') }}"></script>

<!-- Bootstrap Daterangepicker -->
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker-demo.js') }}"></script>

<!-- Tabdrop Responsive -->
{{--<script type="text/javascript" src="{{ asset('assets/widgets/tabs/tabs-responsive.js') }}"></script>--}}
<script type="text/javascript">
    /* Responsive tabs */
    $(function() {
        "use strict";
        $('.nav-responsive').tabdrop();
    });
</script>
@stop