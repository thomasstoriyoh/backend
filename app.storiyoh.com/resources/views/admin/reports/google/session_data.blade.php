@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Google Analytics Report</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Reports</a></li>
            <li class="blueText">Google Analytics Report</li>
            <li>Session Data</li>
        </ul>
    </div>
    @include("admin.reports.google.tab")
    <!-- <p>Delight UI comes packed with a lot of options</p> -->
</div>

@include("admin.reports.google.top_google_chart")

<div class="row">
    <div class="col-md-6">
        <div class="dashboard-box bg-white mrgB20">
            <div class="content-wrapper">
                <div id="newSessionsChart" style="width:100%;height:180px"></div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    New Sessions
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="dashboard-box bg-white mrgB20">
            <div class="content-wrapper">
                <div id="avgSessionDurationChart" style="width:100%;height:180px"></div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Avg. Session Duration
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-md-6">
        <div class="dashboard-box bg-white mrgB20">
            <div class="content-wrapper">
                <div id="bouncesChart" style="width:100%;height:180px"></div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Bounces
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="dashboard-box bg-white mrgB20">
            <div class="content-wrapper">
                <div id="bounceRateChart" style="width:100%;height:180px"></div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Bounce Rate
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
	var percentnewsessionsdata, bouncesdata, bounceratedata, avgsessiondurationdata="";
	percentnewsessionsdata = {!! json_encode($percentNewSessionsData) !!};
	avgsessiondurationdata = {!! json_encode($avgSessionDurationData) !!};
	bouncesdata = {!! json_encode($bouncesData) !!};
	bounceratedata = {!! json_encode($bounceRateData) !!};
</script>

<!-- Google Chart Library -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/analytics/top-chart-data.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/analytics/google-chart-session-data.js') }}"></script>

<!-- Bootstrap Daterangepicker -->
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker-demo.js') }}"></script>

<!-- Tabdrop Responsive -->
{{--<script type="text/javascript" src="{{ asset('assets/widgets/tabs/tabs-responsive.js') }}"></script>--}}
<script type="text/javascript">
    /* Responsive tabs */
    $(function() {
        "use strict";
        $('.nav-responsive').tabdrop();
    });
</script>
@stop