<div class="dt-tab">
    <div class="example-box-wrapper">
        <ul class="nav-responsive nav nav-tabs" style="font-weight: bold;">
        	@foreach($all_tab_reports as $sidebar_module)
                @if(empty($sidebar_module->child->count()) && !empty($sidebar_module->controller))
                    @CanI('view.'.$sidebar_module->slug)
                    <li {{ $sidebar_module->controller == $current_controller ? 'class=active' : '' }}>
                        @if(class_exists('App\Http\Controllers\Admin\Reports\Google\\' . $sidebar_module->controller))
                            <a href="{{ @action('Admin\\Reports\Google\\' . $sidebar_module->controller . '@index') }}">
                                {{ $sidebar_module->title }}
                            </a>
                            @else
                            {{ $sidebar_module->title }}
                        @endif
                    </li>
                    @endCanI
                @endif
            @endforeach
        </ul>
    </div>
</div>

<script type="text/javascript">

    $(window).resize(function () {
        setTimeout(function () {
            handleDropdown();
        }, 550);
    });

$(window).load(function () {
    setTimeout(function () {
        $(window).trigger('resize');
    }, 500);
});


function handleDropdown() {
    if ($.trim($('.dt-tab .dropdown-menu').html()) == "") {
        $('.dt-tab .dropdown-menu').parent().hide();
    }else{
        $('.dt-tab .dropdown-menu').parent().show();
        $('.dt-tab .dropdown-menu').css('margin-left', '-90px');
    }
}

</script>