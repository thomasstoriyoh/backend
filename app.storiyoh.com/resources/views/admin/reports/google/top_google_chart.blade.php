<div class="row">
    <div class="col-md-12">
        <div class="dashboard-box dashboard-box-chart bg-white content-box">
            <div class="content-wrapper">
                <div class="dc1-header row">
                    <div class="col-sm-6 dc1-title">
                        <small class="font-size-14">Total Visits: </small> <span class="font-blue">{{ $total_analytics_count }}</span>
                        <small class="font-size-14 mrg10L">Today: </small> <span class="font-blue">{{ $today_count }}</span>
                    </div>
                    <div class="col-sm-6 dc1-date">
                        <div id="daterangepicker-dashboard" class="btn btn-primary" title="" data-toggle="dropdown">
                            <i class="glyph-icon icon-calendar"></i>
                            <span></span>
                            <b class="caret"></b>
                        </div>
                    </div>
                </div>
                <div id="drawTopChart" style="width:100% !important;height:400px">
                    <div style="margin-top:190px" class="chart-loading glyph-icon remove-border demo-icon icon-spin-5 icon-spin" title=""></div>
                </div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left">
                    Sessions
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	var sessionData = {!! json_encode($analyticsData) !!};
	$(function() {
	    $('#daterangepicker-dashboard').daterangepicker({
	        format: 'YYYY-MM-DD',
	        startDate: moment('{{ session("ga_start_date_for_analytics")}}'),
            endDate: moment('{{ session("ga_end_date_for_analytics")}}'),
            maxDate: moment(),
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'small bg-green',
            cancelClass: 'small ui-state-default',
            separator: ' to '
	    },
        function(start, end) {
            $('#daterangepicker-dashboard span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            module.ajax("{{ action('Admin\Core\DashboardController@ajaxIndex') }}", {
            	start:start.format('YYYY-MM-DD'),
            	end:end.format('YYYY-MM-DD')
            }).done(function(e){
            	location.reload();
            });
        });
	    $('#daterangepicker-dashboard span').html(moment('{{ session("ga_start_date_for_analytics")}}').format('MMMM D, YYYY') + ' - ' + moment('{{ session("ga_end_date_for_analytics")}}').format('MMMM D, YYYY'));
	});

    $(window).load(function () {
        $(".chart-loading").css('margin-left', parseInt(($("#drawTopChart").width())/2) - 10 + 'px');
    });
</script>