@extends('admin.layouts.master')

@section('content')
<div id="page-title">
    <h2>Google Analytics Report</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Reports</a></li>
            <li class="blueText">Google Analytics Report</li>
            <li>User Data</li>
        </ul>
    </div>
    @include("admin.reports.google.tab")
    <!-- <p>Delight UI comes packed with a lot of options</p> -->
</div>

@include("admin.reports.google.top_google_chart")
<div class="row">
    <div class="col-md-4">
        <div class="dashboard-box bg-white mrgB20">
            <div class="content-wrapper">
                <div id="totalPageviewsChart" style="width:100%;height:180px"></div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Total Pageviews
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="dashboard-box bg-white mrgB20">
            <div class="content-wrapper">
                <div id="activeUserChart" style="width:100%;height:180px"></div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    Active Users
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="dashboard-box bg-white mrgB20">
            <div class="content-wrapper">
                <div id="newUserChart" style="width:100%;height:180px"></div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    New Users
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="dashboard-box bg-white mrgB20 pad15A">
            <div class="text-center pad0A">
                <div style="width:100%;height:320px;display:inline-block;">
                    <div id="userTypesDonutChart" style="width:100%;height:100%;"></div>
                </div>
            </div>
            <div class="button-pane">
                <div class="size-md float-left heading">
                    User Type
                </div>
            </div>
        </div>
    </div>   
</div>

@stop

@section('js')
<script>
	var pageviewsdata, userdata, newuserdata, newvisitordata, returningvisitordata="";
	pageviewsdata = {!! json_encode($analyticsTotalPageviewsData) !!};
	userdata = {!! json_encode($analyticsActiveUsersData) !!};
	newuserdata = {!! json_encode($analyticsNewUsersData) !!};
	newvisitordata = {!! json_encode($newVisitorData) !!};
</script>

<!-- Google Chart Library -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/analytics/top-chart-data.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/google-charts/analytics/google-chart-user-data.js') }}"></script>

<!-- Bootstrap Daterangepicker -->
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/date-range-picker/daterangepicker-demo.js') }}"></script>

<!-- Tabdrop Responsive -->
{{--<script type="text/javascript" src="{{ asset('assets/widgets/tabs/tabs-responsive.js') }}"></script>--}}
<script type="text/javascript">
    /* Responsive tabs */
    $(function() {
        "use strict";
        $('.nav-responsive').tabdrop();
    });
</script>
@stop