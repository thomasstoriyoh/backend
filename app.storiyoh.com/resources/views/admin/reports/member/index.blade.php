@extends('admin.layouts.master')

@section('content')

<div id="page-title">
    <h2>User Report</h2>
    <!--Breadcrumb-->
    <div class="breadcrumb">
        <ul class="clearfix">
            <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
            <li><a href="#menuLeft">Modules</a></li>
            <li>User Report</li>
        </ul>
    </div>
    <p>User Listing</p>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            User Listing
        </h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                @CanI('create.user-reports')
                   <!--  <a class="btn btn-default" href="{{ action('Admin\FaqController@create') }}">ADD ITEM</a> -->
                    <a href="#filter_modal" class="btn btn-default show-model">Filters</a>
                    <a href="javascript:void(0);" id="reset-filter" class="btn btn-default reset-filter" style="display: none;">Reset Filters</a>
                @endCanI

            </div>
                 <div class="col-xs-12 col-sm-12 col-md-6" style="text-align:right">
                    <div class="rdtChosen">  
                         @CanI('create.user-reports')
                            <a id="download_csv" class="btn btn-default csv" href="{{ action('Admin\Reports\Member\UserReportController@download_csv') }}" data-toggle="modal">Download CSV</a>
                            {{-- <a id="download_csv2" class="btn btn-default csv" href="{{ action('Admin\Reports\Member\UserReportController@downloadUserCsv', ['start_date=2018-07-01&end_date=2018-07-31']) }}" data-toggle="modal">Download User Count</a> --}}
                         @endCanI
                    </div>
                </div>
        </div>
        <div class="table-responsive">
            <table id="listing-table"
                   class="table table-striped table-bordered responsive no-wrap"
                   cellspacing="0" width="100%"></table>
        </div>
    </div>
</div>

@include('admin.partials.filter-report-model')
@stop

@section('js')
<script type="text/javascript">

 var table;
 var csvUrl = "{{ action('Admin\\Reports\\Member\\UserReportController@download_csv') }}";
 //var csvUrl2 = "{{ action('Admin\\Reports\\Member\\UserReportController@downloadUserCsv') }}";


$(function(){
    var table = $("#listing-table").bcmTable({
        columns: [
            { title: "#", data: "order", sortable: false},
            { title: "Full Name", data: "full_name", sortable: false},
            // { title: "Last Name", data: "last_name"},
            { title: "Email", data: "email"},
            { title: "Username", data: "user_name"},            
            { title: "Type", data: "user_type"},
            { title: "Provider", data: "provider"},
            { title: "Verified", data: "verified"},            
            { title: "Last Login", data: "last_login"},
            @CanI('edit.users')
            { title: "Options", data: "options", sortable: false}
            @endCanI
        ],
        ajax : {
            url: module_actions.json,
            data: function(d) {
                d.advanced_filters = $("#advanced_filter_form").serialize();
            }
        },
        bFilter: false
    });
    
    $("#module_filter").change(function(){ 
        table.ajax.reload();
    });

    $(".show-model").click(function(){
        $('#apply_filter').modal('show');
    });

    $("#advanced_filter_form").submit(function() {
        table.ajax.reload();
        changeCsvUrl();
        $("#apply_filter").modal('hide');
    });

    function changeCsvUrl() {
        var urlLink = $.param(table.ajax.params());
        $("#download_csv").attr("href", csvUrl+"?"+urlLink);
    }

    $('#daterangepicker').daterangepicker({
        format: 'YYYY-MM-DD',
        startDate: moment('{{ $start_date}}'),
        endDate: moment('{{ $end_date }}'),
        maxDate: moment(),
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'small bg-green',
        cancelClass: 'small ui-state-default',
        separator: ' to '
    },
    function(start, end) {
        $('#daterangepicker').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));           
    }); 
    
    $(".csv2").click(function(){
        //$("#download_csv2").attr("href"+ csvUrl + '?start_date=2018-07-01 00:00:01&end_date=2018-07-31 23:59:59');
    });
});
</script>
<script type="text/javascript" src="{{ url('assets/admin/js/date-range-picker/moment.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/admin/js/date-range-picker/daterangepicker.js?v=3') }}"></script>
<script type="text/javascript" src="{{ url('assets/admin/js/date-range-picker/daterangepicker-demo.js') }}"></script>

<script type="text/javascript">
    var $source_list = $('select[name="source[]"]');
    $source_list.change(function(){
        var $obj = $(this);
        if($obj.val()){
            if($.inArray('All',$obj.val()) >= 0){
                $source_list.val('All');
            }
        }

            $source_list.trigger('chosen:updated');
    });

    $("#clearFilter").on("click",function(){
        $("#daterangepicker").val("");
        $("#source").val("All").trigger("chosen:updated");
        $("#user_type").val("All").trigger("chosen:updated");
        $("#status").val("All").trigger("chosen:updated");

    });

    $(".reset-filter").on("click",function(){
        $("#daterangepicker").val("");
        $("#source").val("All").trigger("chosen:updated");
        $("#user_type").val("All").trigger("chosen:updated");
        $("#status").val("All").trigger("chosen:updated");
        $("#reset-filter").hide();
        $("#moduleSubmit").submit();
    }); 

    $("#moduleSubmit").on('click',function(){
        if(($("#daterangepicker").val() != "") || ($("#source").val() != 'All') || ($("#status").val() != "Both"))
        {
            $("#reset-filter").show();
        }
        else
        {
            $("#reset-filter").hide();
        }
    });
    </script>
@stop

