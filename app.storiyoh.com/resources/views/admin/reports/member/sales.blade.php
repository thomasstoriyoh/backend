@extends('admin.layouts.master')

@section('content')
    <!--Page Title-->
    <div id="page-title">
        <h2>Sales Report</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li class="blueText">Catalyst WCMS</li>
                <li>Sales Report</li>
            </ul>
        </div>
    </div>
    <div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="title-hero">
                    Filters
                    <a href="javascript:void(0);" class="pull-right" id="clear-all">Clear All</a>
                </div>
                <div class="example-box-wrapper">
                    <div class="form-group">
                        <div class="fm-label">
                            <label>Report of</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('report_type', $report_type, null, [
                                'class' => 'form-control chosen-select',
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Company</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('company[]', $companies, null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Categories</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('category_id[]', [], null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Brands</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('product_id[]', [], null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Regional Head</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('regional_head[]', [], null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Area Head</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('area_head[]', [], null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Sales Executive</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('sales_executive[]', [], null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Distributors</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('distributor_id[]', [], null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Licence Type</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('licence_types[]', $licence_types, null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Outlet Type</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('outlet_types[]', [], null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="fm-label">
                            <label>Outlets</label>
                        </div>
                        <div class="fm-control">
                            {!! Form::select('outlet_id[]', [], null, [
                                'class' => 'form-control chosen-select',
                                'multiple' => true,
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-block generate-report">GENERATE</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="panel">
            <div class="panel-body" style="padding: 15px 20px 0px 20px;">
                <div class="title-hero" style="margin:-15px -20px 0px;">
                    <span style="font-size: 18px;line-height: 35px;">Chart</span>
                    <div id="daterangepicker-report" class="btn btn-primary pull-right" data-toggle="dropdown">
                        <i class="glyph-icon icon-calendar"></i>
                        <span></span>
                        <b class="caret"></b>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="title-hero">
                    Overall Sale / Case

                    <div class="pull-right">
                        <div class="pull-left pad10R">Sales</div>
                        <div class="pull-left">
                            <input type="checkbox" class="input-switch-alt" id="overall_check" />
                        </div>
                        <div class="pull-left pad10L">Cases</div>
                    </div>
                </div>

            </div>
            <div class="example-box-wrapper" id="overall-chart" style="height:400px;">
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="title-hero">
                    Company Sale / Case

                    <div class="pull-right">
                        <div class="pull-left pad10R">Sales</div>
                        <div class="pull-left">
                            <input type="checkbox" class="input-switch-alt" id="company_check" />
                        </div>
                        <div class="pull-left pad10L">Cases</div>
                    </div>
                </div>
            </div>
            <div class="example-box-wrapper" id="company-chart" style="height:400px;">
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="title-hero">
                    Category Sale / Case

                    <div class="pull-right">
                        <div class="pull-left pad10R">Sales</div>
                        <div class="pull-left">
                            <input type="checkbox" class="input-switch-alt" id="category_check" />
                        </div>
                        <div class="pull-left pad10L">Cases</div>
                    </div>
                </div>
            </div>
            <div class="example-box-wrapper" id="category-chart" style="height:400px;">
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="title-hero">
                    Product Sale / Case

                    <div class="pull-right">
                        <div class="pull-left pad10R">Sales</div>
                        <div class="pull-left">
                            <input type="checkbox" class="input-switch-alt" id="product_check" />
                        </div>
                        <div class="pull-left pad10L">Cases</div>
                    </div>
                </div>
            </div>
            <div class="example-box-wrapper" id="product-chart" style="height:400px;">
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="title-hero">
                    Distributor Sale / Case

                    <div class="pull-right">
                        <div class="pull-left pad10R">Sales</div>
                        <div class="pull-left">
                            <input type="checkbox" class="input-switch-alt" id="distributor_check" />
                        </div>
                        <div class="pull-left pad10L">Cases</div>
                    </div>
                </div>
            </div>
            <div class="example-box-wrapper" id="distributor-chart" style="height:400px;">
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="title-hero">
                    Licence Type Sale / Case

                    <div class="pull-right">
                        <div class="pull-left pad10R">Sales</div>
                        <div class="pull-left">
                            <input type="checkbox" class="input-switch-alt" id="licence_check" />
                        </div>
                        <div class="pull-left pad10L">Cases</div>
                    </div>
                </div>
            </div>
            <div class="example-box-wrapper" id="licence-chart" style="height:400px;">
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="title-hero">
                    Outlet Type Sale / Case

                    <div class="pull-right">
                        <div class="pull-left pad10R">Sales</div>
                        <div class="pull-left">
                            <input type="checkbox" class="input-switch-alt" id="type_check" />
                        </div>
                        <div class="pull-left pad10L">Cases</div>
                    </div>
                </div>
            </div>
            <div class="example-box-wrapper" id="type-chart" style="height:400px;">
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="title-hero">
                    Outlet Sale / Case

                    <div class="pull-right">
                        <div class="pull-left pad10R">Sales</div>
                        <div class="pull-left">
                            <input type="checkbox" class="input-switch-alt" id="outlet_check" />
                        </div>
                        <div class="pull-left pad10L">Cases</div>
                    </div>
                </div>
            </div>
            <div class="example-box-wrapper" id="outlet-chart" style="height:400px;">
            </div>
        </div>
        <div class="panel">
            <div class="panel-body text-center" id="data-not-found">
                <h3>No Data Found</h3>
            </div>
        </div>
    </div>
    </div>
@stop

@section('js')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="{{ asset('assets/widgets/daterangepicker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/widgets/daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript">
        var company_rh = {!! json_encode($regional_heads) !!};
        var licence_ot = {!! json_encode($outlet_types) !!};
        $(function () {
            var filters = {
                start_date : '{{ @date("Y-m-d", strtotime("-6 Months")) }}',
                end_date : '{{ @date("Y-m-d") }}'
            };
            $('#daterangepicker-report').daterangepicker({
                    format: 'YYYY-MM-DD',
                    startDate: moment('{{ @date("Y-m-d", strtotime("-6 Months")) }}'),
                    endDate: moment('{{ @date("Y-m-d") }}'),
                    maxDate: moment(),
                    opens: 'left',
                    buttonClasses: ['btn btn-default'],
                    applyClass: 'small bg-green',
                    cancelClass: 'small ui-state-default',
                    separator: ' to '
                },
                function(start, end) {
                    var format = start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY');
                    $('#daterangepicker-report span').html(format);

                    filters.start_date = start.format('YYYY-MM-DD');
                    filters.end_date = end.format('YYYY-MM-DD');

                    Report.getFilteredData();
                });
            $('#daterangepicker-report span')
                .html(moment('{{ @date("Y-m-d", strtotime("-6 Months")) }}').format('MMMM D, YYYY') +
                    ' - ' + moment('{{ @date("Y-m-d") }}').format('MMMM D, YYYY'));

            var $company = $("select[name='company[]']"),
                $regional_head = $("select[name='regional_head[]']"),
                $area_head = $("select[name='area_head[]']"),
                $sales_executive = $("select[name='sales_executive[]']"),
                $brands = $("select[name='product_id[]']"),
                $categories = $("select[name='category_id[]']"),
                $distributors = $("select[name='distributor_id[]']"),
                $licence_types = $("select[name='licence_types[]']"),
                $outlet_types = $("select[name='outlet_types[]']"),
                $outlets = $("select[name='outlet_id[]']"),
                $report_type = $("select[name='report_type']"),
                $generate = $("button.generate-report");

            var Report = {
                response: {},
                changeCompanySelect : function () {
                    var options = '', ids = $company.val();
                    if (ids) {
                        $.each(ids, function (k, id) {
                            if (company_rh[id] !== undefined) {
                                $.each(company_rh[id], function (i, val) {
                                    options += '<option value="'+ val.id +'">'+ val.full_name +'</option>';
                                });
                            }
                        });
                    }

                    $regional_head.html(options).trigger("chosen:updated").trigger("change");
                    this._loadCategories(ids);
                    this._loadDistributors(ids);

                },
                changeRegionalHeadSelect : function () {
                    var ids = $regional_head.val(), options = '', company = $company.val();
                    $area_head.html(options).trigger("chosen:updated").trigger("change");
                    if (ids) {
                        $.ajax({
                            url : "{{ action('Admin\AjaxController@areaHeads') }}",
                            method : 'POST',
                            data : {ids : ids, company : company},
                            dataType : 'json'
                        }).done(function (response) {
                            if (response.status) {
                                $.each(response.data, function (index, el) {
                                    var name = el.first_name +' '+ el.last_name;
                                    options += '<option value="'+ el.id +'">'+ name +'</option>';
                                });
                            } else {
                                module.notify(response.message, "warning");
                            }

                            $area_head.html(options).trigger("chosen:updated").trigger("change");
                        });
                    }
                },
                changeAreaHeadSelect : function () {
                    var ids = $area_head.val(), options = '', company = $company.val();
                    $sales_executive.html(options).trigger("chosen:updated").trigger("change");
                    if (ids) {
                        $.ajax({
                            url : "{{ action('Admin\AjaxController@salesExecutives') }}",
                            method : 'POST',
                            data : {ids : ids, company : company},
                            dataType : 'json'
                        }).done(function (response) {
                            if (response.status) {
                                $.each(response.data, function (index, el) {
                                    var name = el.first_name +' '+ el.last_name;
                                    options += '<option value="'+ el.id +'">'+ name +'</option>';
                                });
                            } else {
                                module.notify(response.message, "warning");
                            }

                            $sales_executive.html(options).trigger("chosen:updated").trigger("change");
                        });
                    }
                },
                changeSalesExecutiveSelect : function () {
                    this._loadOutlets();
                },
                changeCategorySelect : function () {
                    var ids = $categories.val(), options = '', company = $company.val();
                    $brands.html(options).trigger("chosen:updated").trigger("change");
                    $.ajax({
                        url : "{{ action('Admin\AjaxController@companyProducts') }}",
                        method : 'POST',
                        data : {company : company, category_id : ids},
                        dataType : 'json'
                    }).done(function (response) {
                        if (response.status) {
                            $.each(response.data, function (index, el) {
                                options += '<option value="'+ el.id +'">'+ el.title +'</option>';
                            });
                        } else {
                            module.notify(response.message, "warning");
                        }

                        $brands.html(options).trigger("chosen:updated").trigger("change");
                    });
                },
                changeLicenceTypeSelect : function () {
                    var options = '', ids = $licence_types.val();
                    if (ids) {
                        $.each(ids, function (k, id) {
                            if (licence_ot[id] !== undefined) {
                                $.each(licence_ot[id], function (i, val) {
                                    options += '<option value="'+ val.id +'">'+ val.title +'</option>';
                                });
                            }
                        });
                    }

                    $outlet_types.html(options).trigger("chosen:updated").trigger("change");
                },
                changeOutletTypeSelect : function () {
                    this._loadOutlets();
                },
                changeDistributorSelect : function () {
                    this._loadOutlets();
                },
                getFilteredData : function () {
                    var vm = this;
                    $.ajax({
                        url : module_actions.json,
                        data : this._getFilterData(),
                        type : 'GET',
                        dataType : 'json'
                    }).done(function(res) {
                        vm.response = res;
                        drawAllSaleChart();
                    });
                },
                _getFilterData : function () {
                    return Object.assign({}, filters, {
                        company_ids : $company.val(),
                        regional_head_ids : $regional_head.val(),
                        area_head_ids : $area_head.val(),
                        sales_ids : $sales_executive.val(),
                        category_ids : $categories.val(),
                        product_ids : $brands.val(),
                        distributor_ids : $distributors.val(),
                        licence_type_ids : $licence_types.val(),
                        outlet_type_ids : $outlet_types.val(),
                        outlet_ids : $outlets.val(),
                        report_type : $report_type.val()
                    });
                },
                _loadCategories : function (company) {
                    var options = '';
                    $.ajax({
                        url : "{{ action('Admin\AjaxController@companyCategories') }}",
                        method : 'POST',
                        data : {company : company},
                        dataType : 'json'
                    }).done(function (response) {
                        if (response.status) {
                            $.each(response.data, function (index, el) {
                                options += '<option value="'+ el.id +'">'+ el.title +'</option>';
                            });
                        } else {
                            module.notify(response.message, "warning");
                        }

                        $categories.html(options).trigger("chosen:updated").trigger("change");
                    });
                },
                _loadDistributors : function (company) {
                    var options = '';
                    $.ajax({
                        url : "{{ action('Admin\AjaxController@companyDistributors') }}",
                        method : 'POST',
                        data : {company : company},
                        dataType : 'json'
                    }).done(function (response) {
                        if (response.status) {
                            $.each(response.data, function (index, el) {
                                options += '<option value="'+ el.id +'">'+ el.company +'</option>';
                            });
                        } else {
                            module.notify(response.message, "warning");
                        }

                        $distributors.html(options).trigger("chosen:updated").trigger("change");
                    });
                },
                _loadOutlets : function () {
                    var options = '';
                    $outlets.html(options).trigger("chosen:updated").trigger("change");
                    $.ajax({
                        url : "{{ action('Admin\AjaxController@outlets') }}",
                        method : 'POST',
                        data : {
                            regional_heads : $regional_head.val(),
                            area_heads : $area_head.val(),
                            sales_executives : $sales_executive.val(),
                            distributors : $distributors.val(),
                            licence_types : $licence_types.val(),
                            outlet_types : $outlet_types.val()
                        },
                        dataType : 'json'
                    }).done(function (response) {
                        if (response.status) {
                            $.each(response.data, function (index, el) {
                                options += '<option value="'+ el.id +'">'+ el.company +'</option>';
                            });
                        } else {
                            module.notify(response.message, "warning");
                        }

                        $outlets.html(options).trigger("chosen:updated").trigger("change");
                    });
                }
            };

            $company.change(function () {
                Report.changeCompanySelect();
            });

            $regional_head.change(function () {
                Report.changeRegionalHeadSelect();
            });

            $area_head.change(function () {
                Report.changeAreaHeadSelect();
            });

            $sales_executive.change(function () {
                Report.changeSalesExecutiveSelect();
            });

            $categories.change(function () {
                Report.changeCategorySelect();
            });

            $licence_types.change(function () {
                Report.changeLicenceTypeSelect();
            });

            $outlet_types.change(function () {
                Report.changeOutletTypeSelect();
            });

            $distributors.change(function () {
                Report.changeDistributorSelect();
            });

            $("#clear-all").click(function () {
                $report_type.val('Overall Sale').trigger('chosen:updated');
                $company.val('').trigger('chosen:updated').trigger('change');
                $licence_types.val('').trigger('chosen:updated').trigger('change');

                setTimeout(Report.getFilteredData.bind(Report), 200);
            });

            google.charts.load('visualization', '1.0', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(Report.getFilteredData.bind(Report));
            $generate.click(function () {
                Report.getFilteredData();
            });

            function drawAllSaleChart() {
                drawOverallChart();
                drawCompanyChart();
                drawCategoryChart();
                drawProductChart();
                drawDistributorChart();
                drawLicenceChart();
                drawTypeChart();
                drawOutletChart();
            }

            var $overall_check = $("#overall_check"),
                $company_check = $("#company_check"),
                $category_check = $("#category_check"),
                $product_check = $("#product_check"),
                $distributor_check = $("#distributor_check"),
                $licence_check = $("#licence_check"),
                $type_check = $("#type_check"),
                $outlet_check = $("#outlet_check");

            function drawOverallChart() {
                var json = Report.response.overall_sale;
                if ($overall_check.attr("checked")) {
                    json = Report.response.overall_case;
                }
                $("#overall-chart").parents('.panel').show();
                $("#data-not-found").parents('.panel').hide();
                if (json.length <= 1) {
                    $("#overall-chart").parents('.panel').hide();
                    $("#data-not-found").parents('.panel').show();
                    return false;
                }
                var ch = getChartOptions(json);

                var chart = new google.visualization.ColumnChart(
                    document.getElementById('overall-chart'));

                chart.draw(ch.data, ch.options);
            }
            $overall_check.change(function() {
                drawOverallChart();
            });

            function drawCompanyChart() {
                var json = Report.response.company_sale;
                if ($company_check.attr("checked")) {
                    json = Report.response.company_case;
                }
                $("#company-chart").parents('.panel').show();
                if (json.length <= 1) {
                    $("#company-chart").parents('.panel').hide();//.html('<h4>No Data Found.</h4>');
                    return false;
                }
                var ch = getChartOptions(json, true);

                var chart = new google.visualization.ColumnChart(
                    document.getElementById('company-chart'));

                chart.draw(ch.data, ch.options);
            }
            $company_check.change(function() {
                drawCompanyChart();
            });

            function drawCategoryChart() {
                var json = Report.response.category_sale;
                if ($category_check.attr("checked")) {
                    json = Report.response.category_case;
                }
                $("#category-chart").parents('.panel').show();
                if (json.length <= 1) {
                    $("#category-chart").parents('.panel').hide();//.html('<h4>No Data Found.</h4>');
                    return false;
                }
                var ch = getChartOptions(json, true);

                var chart = new google.visualization.ColumnChart(
                    document.getElementById('category-chart'));

                chart.draw(ch.data, ch.options);
            }
            $category_check.change(function() {
                drawCategoryChart();
            });

            function drawProductChart() {
                var json = Report.response.product_sale;
                if ($product_check.attr("checked")) {
                    json = Report.response.product_case;
                }
                $("#product-chart").parents('.panel').show();
                if (json.length <= 1) {
                    $("#product-chart").parents('.panel').hide();//.html('<h4>No Data Found.</h4>');
                    return false;
                }
                var ch = getChartOptions(json, true);

                var chart = new google.visualization.ColumnChart(
                    document.getElementById('product-chart'));

                chart.draw(ch.data, ch.options);
            }
            $product_check.change(function() {
                drawProductChart();
            });

            function drawDistributorChart() {
                var json = Report.response.distributor_sale;
                if ($distributor_check.attr("checked")) {
                    json = Report.response.distributor_case;
                }
                $("#distributor-chart").parents('.panel').show();
                if (json.length <= 1) {
                    $("#distributor-chart").parents('.panel').hide();//.html('<h4>No Data Found.</h4>');
                    return false;
                }
                var ch = getChartOptions(json, true);

                var chart = new google.visualization.ColumnChart(
                    document.getElementById('distributor-chart'));

                chart.draw(ch.data, ch.options);
            }
            $distributor_check.change(function() {
                drawDistributorChart();
            });

            function drawLicenceChart() {
                var json = Report.response.licence_sale;
                if ($licence_check.attr("checked")) {
                    json = Report.response.licence_case;
                }
                $("#licence-chart").parents('.panel').show();
                if (json.length <= 1) {
                    $("#licence-chart").parents('.panel').hide();//.html('<h4>No Data Found.</h4>');
                    return false;
                }
                var ch = getChartOptions(json, true);

                var chart = new google.visualization.ColumnChart(
                    document.getElementById('licence-chart'));

                chart.draw(ch.data, ch.options);
            }
            $licence_check.change(function() {
                drawLicenceChart();
            });

            function drawTypeChart() {
                var json = Report.response.type_sale;
                if ($type_check.attr("checked")) {
                    json = Report.response.type_case;
                }
                $("#type-chart").parents('.panel').show();
                if (json.length <= 1) {
                    $("#type-chart").parents('.panel').hide();//.html('<h4>No Data Found.</h4>');
                    return false;
                }
                var ch = getChartOptions(json, true);

                var chart = new google.visualization.ColumnChart(
                    document.getElementById('type-chart'));

                chart.draw(ch.data, ch.options);
            }
            $type_check.change(function() {
                drawTypeChart();
            });

            function drawOutletChart() {
                var json = Report.response.outlet_sale;
                if ($outlet_check.attr("checked")) {
                    json = Report.response.outlet_case;
                }
                $("#outlet-chart").parents('.panel').show();
                if (json.length <= 1) {
                    $("#outlet-chart").parents('.panel').hide();//.html('<h4>No Data Found.</h4>');
                    return false;
                }
                var ch = getChartOptions(json, true);

                var chart = new google.visualization.ColumnChart(
                    document.getElementById('outlet-chart'));

                chart.draw(ch.data, ch.options);
            }
            $outlet_check.change(function() {
                drawOutletChart();
            });

            function getChartOptions(json, stack) {
                var stacked = stack || false;
                var data = google.visualization.arrayToDataTable(json);

                var view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                    {
                        role: "annotation"
                    }]);

                var options = {
                    height: 366,
                    legend: { position: "none" },
                    chartArea:{ width: '85%', height: '80%'},
                    hAxis: {
                        textStyle: {
                            fontSize: '10'
                        }
                    },
                    vAxis: {
                        textStyle: {
                            fontSize: '10'
                        }
                    },
                    isStacked: stacked
                };

                return { data: data, options: options };
            }


        });
    </script>
@stop