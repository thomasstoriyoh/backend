@extends('admin.layouts.master')

@section('content')

    <div id="page-title">
        <h2>Sales Report</h2>
        <!--Breadcrumb-->
        <div class="breadcrumb">
            <ul class="clearfix">
                <li><a href="{{ url('/catalyst') }}">Catalyst WCMS</a></li>
                <li><a href="#menuLeft">Modules</a></li>
                <li>Sales Report</li>
            </ul>
        </div>
        <p>Sales Listing</p>
    </div>

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Sales Listing
            </h3>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    @CanI('create.sales-reports')
                        <a href="#filter_modal" class="btn btn-default show-model">Filters</a>
                        <a href="javascript:void(0);" id="reset-filter" class="btn btn-default reset-filter" style="display: none;">Reset Filters</a>
                    @endCanI

                </div>
                <div class="col-xs-12 col-sm-12 col-md-6" style="text-align:right">
                    <div class="rdtChosen">  
                        @CanI('create.sales-reports')
                            <a id="download_csv" class="btn btn-default csv" href="{{ action('Admin\Reports\Sales\SalesReportController@download_sales_csv') }}" data-toggle="modal">Download CSV</a>                            
                        @endCanI
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="listing-table"
                    class="table table-striped table-bordered responsive no-wrap"
                    cellspacing="0" width="100%"></table>
            </div>
        </div>
    </div>
    @include('admin.partials.filter-sales-report-model')
@stop

@section('js')
    <script type="text/javascript">
        var table;
        var csvUrl = "{{ action('Admin\\Reports\\Sales\\SalesReportController@download_sales_csv') }}";

        $(function(){
            var table = $("#listing-table").bcmTable({
                columns: [
                    { title: "#", data: "order", sortable: false},
                    { title: "Full Name", data: "full_name", sortable: false},
                    { title: "Email", data: "email", sortable: false},
                    { title: "Invoice No.", data: "invoice_number", sortable: false},            
                    { title: "Invoice Date", data: "invoice_date", sortable: false},
                    { title: "Product", data: "product", sortable: false},
                    { title: "Type", data: "product_type", sortable: false},            
                    { title: "Currency", data: "currency", sortable: false},
                    { title: "Total", data: "invoice_total", sortable: false},
                    @CanI('edit.sales-reports')
                        // { title: "Options", data: "options", sortable: false}
                    @endCanI
                ],
                ajax : {
                    url: module_actions.json,
                    data: function(d) {
                        d.advanced_filters = $("#advanced_filter_form").serialize();
                    }
                },
                bFilter: false
            });
            
            $("#module_filter").change(function(){ 
                table.ajax.reload();
            });

            $(".show-model").click(function(){
                $('#apply_filter').modal('show');
            });

            $("#advanced_filter_form").submit(function() {
                table.ajax.reload();
                changeCsvUrl();
                $("#apply_filter").modal('hide');
            });

            function changeCsvUrl() {            
                var urlLink = $.param(table.ajax.params());
                $("#download_csv").attr("href", csvUrl+"?"+urlLink);            
            }

            $('#daterangepicker').daterangepicker({
                format: 'YYYY-MM-DD',
                startDate: moment('{{ $start_date}}'),
                endDate: moment('{{ $end_date }}'),
                maxDate: moment(),
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'small bg-green',
                cancelClass: 'small ui-state-default',
                separator: ' to '
            },
            function(start, end) {
                $('#daterangepicker').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));           
            });
        });
    </script>
    <script type="text/javascript" src="{{ url('assets/admin/js/date-range-picker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/admin/js/date-range-picker/daterangepicker.js?v=3') }}"></script>
    <script type="text/javascript" src="{{ url('assets/admin/js/date-range-picker/daterangepicker-demo.js') }}"></script>

    <script type="text/javascript">
        $("#clearFilter").on("click",function(){
            $("#daterangepicker").val("");
        });

        $(".reset-filter").on("click",function(){
            $("#daterangepicker").val("");
            $("#reset-filter").hide();
            $("#moduleSubmit").submit();
        }); 

        $("#moduleSubmit").on('click',function(){
            if ($("#daterangepicker").val() != "")
            {
                $("#reset-filter").show();
            }
            else
            {
                $("#reset-filter").hide();
            }
        });
    </script>
@stop

