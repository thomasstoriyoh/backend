<!doctype html>
<html>

<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
</head>

<body>
    <div class="f1-page-wrapper legal-page">
        <div class="f1-page">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">

                    <div class="uk-width-medium-3-4">
                        <div class="f1-info episode-overview">
                            <h1 class="heading uk-text-uppercase">Privacy Policy</h1>
                            <div class="content">
                                <p>
                                    <b>Information about Us and the need for Privacy Policy:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>The domain name storiyoh.com is owned by Storiyoh Private Limited, a private company incorporated under the Companies Act, 2013 bearing CIN U74999KL2017FTC050463 and having its registered office at Villa No. 12, Skyline, Greenwoods, Indira Junction, Kakkanad Kochi Ernakulam, Kerala 682030, India (“Storiyoh” or “Us” or “We” or “Our”). </li>
                                        <li>We are concerned about the privacy of the users accessing Our Platform(s) (“You” or “Your” or “Yourself”). </li>
                                        <li>Your privacy is important to Us and We have therefore provided this Privacy Policy (“Policy”) to familiarize You with the manner in which We collect, use and disclose Your Personal Information collected through the Platform(s). </li>
                                        <li>When You access or use Our Platform(s), We collect and store Your Personal Information which is provided by You from time to time during the course of Your use or access of Platform(s).</li>
                                        <li>The purpose of doing so is to improve the experience of the Platform(s) and that of the services offered through the Platform(s) and to provide You with a safe, efficient, smooth and customized experience. We may collect such Personal Information from You that We consider necessary for achieving this purpose. </li>
                                    </ul>
                                </p>

                                <p>
                                    <b>Prospective Amendments:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>We reserve the right, at Our sole discretion, to change, modify, add or remove portions of this Policy (“Changes”). You are requested to check this Policy periodically for Changes as Your use of the Platform(s) is subject to the most current version of this Policy and Your continued use of the Platform(s) following such Changes, will mean that You accept and agree to the Changes. </li>
                                        <li>By mere access or use of the Platform(s), You expressly consent to Our Policy. </li>
                                        <li>This Privacy Policy is to be read concurrently with Our Terms of Use, Community Guidelines and Copyright Policy.</li>
                                    </ul>
                                </p>

                                <p>
                                    <b>General Definitions:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>“Personal Information” shall mean such information disclosed by or obtained from any user of the Platform(s) including You and includes name, age, any other personal details capable of identifying a person and any other information about the user and/or Yourself (including any literary or artistic works) that the user and/or You willingly submits to the Platform(s).</li>
                                        <li>“Information" shall mean any information You provide to Us or other users of the Platform(s) including without limitation information provided by You, in the registration process, in the feedback area, bulletin board and messages or through any e-mail feature and shall include User Content and Your Personal Information. </li>
                                    </ul>
                                </p>

                                <p>
                                    <b>Collection of Information:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>In general, You can browse the Platform(s) without telling Us who You are or revealing any Personal Information about Yourself. Once You give Us Your Personal Information, You are not anonymous to Us. You always have the option to not provide information by choosing not to use a particular service or feature on the Platform(s).</li>
                                        <li>You may need to provide Us Your Personal Information during the course of using some of Our services and/or while registering with Us. </li>
                                        <li>
                                            We also collect Information which You provide to Us directly. This may include (non-exhaustive list):
                                            <ul>
                                                <li>Your username, password and e-mail address when You register with Storiyoh;</li>
                                                <li>Profile information that You provide for Your user profile including Your name and profile picture. This information allows Us to help You or others be "found" on Storiyoh.</li>
                                                <li>Content (e.g., comments, reviews, feedbacks and other materials) that You post on the Platform(s) (“User Content”).</li>
                                                <li>Communications between You and Us. </li>
                                            </ul>
                                        </li>
                                        <li>We also collect associated metadata which is the technical data that is associated with User Content. You can add or may have metadata added to Your User Content including “#” or “@” (to mark keywords when You post), geotag (to mark Your location). This makes Your User Content more searchable by others and more interactive. We may run contests, special offers or other events or activities ("Events") on the Platform(s). If You do not want to participate in an Event, do not use the particular metadata associated with that Event.</li>
                                        <li>Further, We also collate location Information which is collected by Us after You enable the necessary permissions on Your device from which You access Our Platform(s). </li>
                                        <li>We may automatically track certain Information about You based upon Your behavior, acts, subscriptions and preferences on Our Platform(s). We use this information for the purposes of internal research on Our users' demographics, interests, and behavior to better understand, protect and provide Our users a better experience on Our Platform(s). </li>
                                        <li>We may include certain technical Information with respect to Your device. This Information may include the date and time of visiting the Platform(s), Your IP address and the type of browser being used.</li>
                                        <li>In an effort to make Our Platform(s) effective, certain information may be collected each time You visit the Platform(s). Such information may be stored in server logs. These encrypted statistics do not identify You personally, but provide Us the information regarding Your visit to the Platform(s) and the type of user who is accessing Our Platform(s) and certain browsing activities by You. This data may include (but shall not be limited to): IP address of Your server from where the Platform(s) is being accessed, the type of, the operating system of Your device, the pages You last visited before visiting Our Platform(s), the duration of Your stay on Our Platform(s), the date and time of Your access. These data(s) are used by Storiyoh to understand the use and number of users visiting the Platform(s). This anonymous information is collected through the use of pixel tags.</li>
                                        <li>The ‘User Profile’ page allows You to share more information about Yourself with other users. It is entirely Your discretion as to what You share here. Information saved in the ‘User Profile’ page will be publicly accessible. You can remove or edit any information at any time.</li>
                                    </ul>
                                </p>

                                <p>
                                    <b>Use of Your Information:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>You are solely responsible for Your Information, and in accordance with certain features of the Platform(s), We may only act as a passive conduit for Your online distribution and publication of Your Information. </li>
                                        <li>
                                            In addition to some of the specific uses of Information, We may use Your Information to (non-exhaustive list):
                                            <ul>
                                                <li>help You efficiently access Your User Content after You sign in;</li>
                                                <li>remember Information so You will not have to re-enter it during Your next visit to the Platform(s);</li>
                                                <li>provide personalized content and information to You and others, which could include online ads or other forms of marketing through newsletters, e-mails or any other methods;</li>
                                                <li>provide, improve, test, and monitor the effectiveness of Our service;</li>
                                                <li>monitor metrics such as total number of visitors, traffic, and demographic patterns</li>
                                                <li>diagnose or fix technology problems;</li>
                                                <li>automatically update Our App on Your device;</li>
                                            </ul>
                                        </li>
                                        <li>
                                            Personal Information provided by You is used for the following proposes, including but not limited to:
                                            <ul>
                                                <li>to facilitate Your use of the Platform(s) and to help You address Your problems with the Platform(s) including addressing any technical problems and providing any customer related services;</li>
                                                <li>to provide You with information about Our services and to send You information, materials, and offers;</li>
                                                <li>to personalize Your experience on the Platform(s) by presenting advertising, products and offers tailored for You;</li>
                                                <li>for proper administration and protection of the integrity of the Platform(s); and</li>
                                                <li>to respond to judicial process and provide information to law enforcement agencies or in connection with an investigation on matters related to public safety, as permitted by law.</li>
                                            </ul>
                                        </li>
                                        <li>
                                            We shall also share Your Information with third-parties on need to know basis under reasonable confidentiality terms, wherever applicable, for any of the following purposes including but not limited to:
                                            <ul>
                                                <li>We may share Your Information and information from tools like cookies, log files, and device identifiers and location data, with third-party organizations that help Us provide services to You ("Service Providers"). </li>
                                                <li>We may share Your Information to respond to judicial process and provide information to law enforcement agencies or in connection with an investigation on matters related to public safety, as permitted by law.</li>
                                                <li>We may also share certain Information with third-party advertising partners. This information would allow third-parties to, among other things, deliver targeted advertisements that they believe will be of most interest to You.</li>
                                                <li>We may remove parts of data that can identify You and share anonymized data with other parties. We may also combine Your Information with other information in a way that it is no longer associated with You and share that aggregated information.</li>
                                            </ul>
                                        </li>
                                        <li>Any information or content that You voluntarily disclose on the Platform(s), such as User Content, becomes available to the public, as controlled by any applicable privacy settings that You set. To change Your privacy settings on the Platform(s), please change Your profile setting. Once You have shared User Content or made it public, that User Content may be re-shared by others.</li>
                                        <li>We may share any Information with Our other affiliates to help detect and prevent identity theft, fraud and other potentially illegal acts; correlate related or multiple accounts to prevent abuse of Our Platform(s) and/or Services; and to facilitate joint or co-branded services that You request where such services are provided by more than one corporate entity. </li>
                                        <li>Some of the information related to the Platform(s) and Your visit to the Platform(s) may be shared with Our sponsors, investors, advertisers, developers, strategic business partners and associates in order to enhance and grow Our business and the Platform(s).</li>
                                        <li>If We sell or otherwise transfer part or the whole of Storiyoh or Our assets to another organization (e.g., in the course of a transaction like a merger, acquisition, bankruptcy, dissolution, liquidation), Your Information may be among the items sold or transferred. We will reasonably ensure that the buyer or transferee will have to honor the commitments We have made in this Policy.</li>
                                        <li>We may disclose Your Information to the extent such disclosure is reasonably necessary to enforce Our Terms of Use or Our other policies and in order to respond to claims of any other user or third-party.</li>
                                    </ul>
                                </p>

                                <p>
                                    <b>Storage and Processing:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>Due to the existing environment, We cannot ensure that all of Your Information will never be disclosed in ways not otherwise described in this Policy. Therefore, although We use industry standard practices to protect Your privacy, We do not promise, and You should not expect, that Your Information will always remain private.</li>
                                        <li>Your Information may be stored and processed in India or any other country in which We or Our Service Providers or third-parties, as referred in this Policy, maintain facilities.</li>
                                        <li>We use commercially reasonable safeguards to help keep the information collected through the Platform(s) secure and take reasonable steps (such as requesting a unique password) to verify Your identity before granting You access to Your account. However, We cannot ensure the security of any information You transmit on the Platform(s) or guarantee that information on the Platform(s) may not be accessed, disclosed, altered, or destroyed.</li>
                                        <li>When You access Our Platform(s), We offer the use of a secure server. The secure server software (SSL) encrypts all information You put in before it is sent to Us. </li>
                                    </ul>
                                </p>

                                <p>
                                    <b>Cookies and Cache:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>We use data collection devices such as "cookies" and “cache” (collectively, “Cookies”) on certain pages of the Platform(s) to help analyse Our web page flow, measure promotional effectiveness, and promote trust and safety. We offer certain features that are only available through the use of Cookies. </li>
                                        <li>We place both permanent and temporary Cookies in Your device. Most Cookies are "session cookies," implying that they are automatically deleted from Your device at the end of a session.</li>
                                        <li>You can stop Cookies from being downloaded on Your device from the Platform(s) at any time by selecting the appropriate settings in Your browser. You can also delete Your browser Cookies or disable them entirely. The browsers will tell You how to change Your browser settings to notify You when a Cookie is being set or updated, or to restrict or block certain types or all Cookies. In case You decline to download Cookies from Our Platform(s) You may not be able to use certain features on the Platform(s), but this may significantly impact Your experience with the Platform(s) and may make parts of the Platform(s) non-functional or inaccessible. We recommend that You leave them turned on.</li>
                                        <li>Additionally, You may encounter Cookies or other similar devices on certain pages of the Platform(s) that are placed by third parties. We work with third parties to support the Platform(s) by serving advertisements or providing services, such as providing users with the ability to comment on content, allowing users to share content, customizing content based on usage, or tracking aggregate site usage statistics (including actions taken on the Platform(s), and dates and times You access the Platform(s)). These third parties use their Cookies to collect information on various websites. We do not control the use of Cookies by third parties nor are We in any way responsible for the same.</li>
                                    </ul>
                                </p>

                                <p>
                                    <b>Advertising:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>We may use third-party advertising companies to serve ads when You visit Our Platform(s). These companies may use Information not including Your Personal Information, about Your visits to Platform(s) and other websites in order to provide advertisements about goods and services of interest to You. </li>
                                        <li>Please be aware that You may click on a sponsored external link at Your own risk and We cannot be held liable for any damages or implications caused by visiting any external links mentioned.</li>
                                        <li>The Platform(s) or third parties may provide links to other World Wide Web sites or resources. Since We have no control over such Sites and resources, You acknowledge and agree that We are not responsible for the availability of such external sites or resources, and do not endorse and are not responsible or liable for any content, advertising, products or other materials on or available from such sites or resources. You further acknowledge and agree that We shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such site or resource.</li>
                                    </ul>
                                </p>

                                <p>
                                    <b>Duration of maintaining and using Your Information:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>Following deactivation of Your account, We may retain certain Information (including Your Personal Information and User Content) for a commercially reasonable time for backup, archival, and/or audit purposes. We will remove Your public posts from view and/or dissociate them from Your user profile, but We may retain information about You for the purposes authorized under this Policy unless prohibited by law. </li>
                                        <li>We may, at our discretion, either delete Your Personal Information or de-identify it so that it is anonymous and not attributed to Your identity. For example, We may retain information to prevent, investigate, or identify possible wrongdoing in connection with Our services or to comply with legal obligations.</li>
                                        <li>If You remove Information that You posted to the Platform(s), copies may remain viewable in cached and archived pages of the Platform(s), or if other users or third parties using the Storiyoh API have copied or saved that information.</li>
                                    </ul>
                                </p>

                                <p>
                                    <b>Miscellaneous:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>We do not knowingly collect or solicit any information from minors or knowingly allow such persons to access or register for the services on Our Platform(s). In the event that We learn that We have collected Personal Information from a minor without parental consent, We will delete that information as quickly as possible.</li>
                                        <li>In accordance with Information Technology Act 2000 and rules made there under, the name and contact details of the Grievance Officer are provided below:</li>
                                    </ul>
                                    <b>Name: Grievance Officer - Rahul Nair</b>
                                    <br>
                                    <b>E-mail id: <a href="mailto:email-legal@storiyoh.com">email-legal@storiyoh.com</a></b>
                                </p>

                                <p>
                                    <b>Contact Us:</b>
                                </p>
                                <p>
                                    <ul>
                                        <li>We encourage You to contact Us in case You have any queries regarding this Policy and/or You wish to report an infringement, or otherwise at <a href="mailto:legal@storiyoh.com">legal@storiyoh.com</a>.</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>