<!doctype html>
<html>

<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
</head>

<body>
    <div class="f1-page-wrapper about-page">

        <div class="f1-page">

            <div class="uk-container uk-container-center" id="waypoint">
                <div class="f1-info shows-overview">
                    <h3 class="heading uk-text-uppercase">About Us</h3>
                    <div class="content">
                        <p>Media and communication, in all its guises - audio or video, pictures or texts, movies or music, short-form or long-form, blogs or books, digital or analog - is as much about a collective, connected experience as it is about the content. Imagine reading books, listening to music or watching movies without any connection to the outside world. How would that feel? We would end up in a bubble and will never know what others think about the same things we are consuming. We may not see an alternative viewpoint. Regardless of the medium, we want to feel connected to not only the art but also to the artist and to other art lovers. After all, the word communication comes from the Latin word ‘communicare’ which means ‘to share’. It is the shared, common, collective experiences that make media a powerful tool for deliberation and change. </p>
                        <p>Podcasts constitute a relatively new medium, but it already reached hundreds of millions of people. The on-demand nature of podcasts and the relatively low barriers to content creation make it a medium with potential to reach billions of people. However, podcast creation, distribution and consumption are all isolated and fragmented experiences right now. We are on a mission to simplify it, unify it, and make it a socially meaningful experience for everyone. We want to build a new community for people who love the spoken word; a community of creators and listeners. For us, such a community is what is most important, and we will use technology to create, manage and to help it thrive. In doing so we are, however, steadfast in our commitment to certain principles.</p>
                    </div>

                    <div class="uk-grid uk-text-center">
                        <div class="uk-width-medium-1-3">
                            <div class="mt40">
                                <img src="{{ asset('assets/frontend') }}/images/addictive.png" alt="">
                            </div>
                            <div class="heading uk-margin-top uk-text-uppercase">
                                We are not out to create an ‘addictive’ product.
                            </div>
                            <div class="content uk-margin-top">
                                Addictions of any kind are bad because it demeans our sense of self-control. Our endeavor will be to design and develop products that people love, enjoy and find meaningful rather than churning products that people can’t stop using. We are not interested in hacking human psychology. Period.
                            </div>
                        </div>
                        <div class="uk-width-medium-1-3">
                            <div class="mt40">
                                <img src="{{ asset('assets/frontend') }}/images/personalisation.png" alt="">
                            </div>
                            <div class="heading uk-margin-top uk-text-uppercase">
                                Personalisation is great but only up to a point.
                            </div>
                            <div class="content uk-margin-top">
                                Over-personalisation simply leads to all of us living in a bubble; suspended from reality. So, while we love to recommend content to you - and we will - that is based on your likes, we will also give you opportunities to hear viewpoints that you wouldn’t typically come across going purely by your likes.
                            </div>
                        </div>
                        <div class="uk-width-medium-1-3">
                            <div class="mt40">
                                <img src="{{ asset('assets/frontend') }}/images/scale.png" alt="">
                            </div>
                            <div class="heading uk-margin-top uk-text-uppercase">
                                Scale is a matter of principle for us.
                            </div>
                            <div class="content uk-margin-top">
                                Only through scale can the benefits of podcasting reach billions of people around the world. In this spirit, we are committed to supporting creators and consumers, and supporting greater diversity in content - not just across formats and genres but also across languages and dialects. Scale will help podcasting achieve sustainability and its potential.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>