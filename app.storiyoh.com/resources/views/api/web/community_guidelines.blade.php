<!doctype html>
<html>
<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
</head>
<body>
    <div class="f1-page-wrapper legal-page">
        <div class="f1-page">
            <div class="uk-container uk-container-center">
                <div class="uk-grid" data-uk-grid-margin>                    
                    <div class="uk-width-medium-3-4">
                        <div class="f1-info episode-overview">
                            <h1 class="heading uk-text-uppercase">Community Guidelines</h1>
                            <div class="content">
                                <p>
                                  <b>Information about Us and the need for Community Guidelines:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>The domain name storiyoh.com is owned by Storiyoh Private Limited, a private company incorporated under the Companies Act, 2013 bearing CIN U74999KL2017FTC050463 and having its registered office at Villa No. 12, Skyline, Greenwoods, Indira Junction, Kakkanad Kochi Ernakulam, Kerala 682030, India (“Storiyoh” or “Us” or “We” or “Our”). </li>
                                    <li>These Community Guidelines (“Guidelines”) include important information about Our expectations from You while using Our Platform(s) and are meant to guide the users of Our Platform(s) (“You” or “Your” or “Contributing Users”), about the type of Content and/or User Generated Content, which You upload on Our Platform(s) and/or distribute through Our Platform(s).</li>
                                    <li>We are a social platform and Our core purpose is to expand Your capabilities and freedom, as We endeavor to create a community where everyone is able to discover what they love and share it with others, which means showing You ideas that are relevant, interesting and personal to You, and to the extent possible, making sure You do not see anything that is inappropriate or spam.</li>
                                    <li>To achieve this goal, We request You to participate on the Platform(s) in a way that promotes a friendly and positive experience for Our community at Storiyoh. </li>
                                  </ul>
                                </p>
                                <p>
                                  <b>Prospective Amendments:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>We reserve the right, at Our sole discretion, to change, modify, add or remove portions of these Guidelines (“Changes”). You are requested to check these Guidelines periodically for Changes as Your use of the Platform(s) is subject to the most current version of these Guidelines and Your continued use of the Platform(s) following such Changes, will mean that You accept and agree to the Changes. </li>
                                    <li>By mere access or use of the Platform(s), You expressly consent to these Guidelines. </li>
                                    <li>These Community Guidelines are to be read concurrently with Our Terms of Use, Privacy Policy and Copyright Policy.</li>
                                  </ul>
                                </p>
                                <p>
                                  <b>General Definitions:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>“Content” includes, without limitation, any information, data, text, logos, photographs, video, audio, animations, written posts, text, articles, comments, software, scripts, graphics, themes and interactive features generated, underlying source code, provided or otherwise made accessible on or through the Platform(s) including User Generated Content. </li>
                                    <li>“IPR” shall mean all copyrights (including without limitation the exclusive right to reproduce, distribute copies of, display and perform the copyrighted work and to prepare derivative works), copyright registrations and applications, trademark rights (including, without limitation, registrations and applications), patent rights (whether registered or unregistered), trade names or business names, registered and unregistered design rights, mask-work rights, trade secrets, moral rights, author’s rights, rights in packaging, goodwill and other intellectual property rights, and all renewals and extension thereof and all rights or forms of protection of a similar nature or having an equivalent or similar effect to any of the above, which may subsist anywhere in the world, regardless of whether any of such rights arise under the laws of India or any other state, country or jurisdiction.</li>
                                    <li>“User Generated Content” shall mean any content generated by the Contributing Users and aggregated on the Platform(s) including but not limited to messages and/or opinions and/or feedbacks or distributed and/or made available via RSS.</li>
                                  </ul>
                                </p>
                                <p>
                                  <b>General Safeguards:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>We are an open, social platform, made up of a vibrant group of people with differing views, opinions, and cultural backgrounds. We support freedom of speech and the fundamental right for individuals to express themselves, and expect everyone else to do the same.</li>
                                    <li>We recognize that criticism can be a valuable tool for helping each other improve. However, it is important to remember that behind each episode of content is a person. Criticism should always focus on the Content, and not on the individual who created it. You may share Your opinion with others, but in a constructive way. </li>
                                    <li>Before uploading and/or distributing any Content, always ask yourself whether You would appreciate receiving the same from others. If not, do not upload and/or distribute it.</li>
                                    <li>You are welcome to share Your opinions and beliefs with the rest of the Storiyoh community, even if they might be controversial, or challenge the status-quo unless it causes any harm to a party.</li>
                                    <li>Uploading and/or distributing Content for the sole purpose of trolling or disrupting the community is not permitted.</li>
                                    <li>You may question the beliefs and opinions of others as long as it is relevant and done in a respectful and non-threatening manner.</li>
                                    <li>To make sure We collectively work towards building the highest-quality Content that is useful to everyone, it would do good if You are mindful of the length and/or size of Your comments for the sake of brevity and clarity. </li>
                                    <li>Any Content You provide or distribute on Our Platform(s) shall be deemed to be non-confidential and We shall be free to use such information on unrestricted basis and as and when deemed appropriate by Us.</li>
                                  </ul>
                                </p>
                                <p>
                                  <b>Community Guidelines:</b>
                                  <br>
                                  These Guidelines are elaborated as follows (non-exhaustive):
                                </p>
                                <p>
                                  <ul>
                                    <li>
                                      In addition to the user obligations and other obligations laid under the Terms of Use, You shall ensure that the Content You upload on and/or distribute through Our Platform(s) should not, at any time:
                                      <ul>
                                        <li>Defame, abuse, harass, threaten or otherwise violate the legal rights of any third-parties;</li>
                                        <li>Be inappropriate, profane, abusive, libelous defamatory, vulgar, infringing, obscene, indecent, harassing, threatening, or inflammatory or otherwise objectionable or contains any such objectionable language or encourages such use;</li>
                                        <li>Contain viruses, corrupted files, or any other similar software or programs that may damage the operation of the Platform(s) or cause any harm;</li>
                                        <li>Create or transmit unwanted electronic communications such as “spam,” to other users or members of the Platform(s) or otherwise interfere with other users’ or members’ enjoyment of the Platform(s).</li>
                                        <li>Is capable of conducting surveys, contests, pyramid schemes or chain letters, without Our consent;</li>
                                        <li>Falsifies or deletes any author attributions, legal or other proper disclosures or proprietary designations or labels of the origin or source of Content that is already uploaded;</li>
                                        <li>Violates the Terms of Use including but not limited to any applicable Privacy Policy of the Platform(s).</li>
                                        <li>Includes any offensive comments that are connected to race, cultural ethnicity, religious beliefs, national origin, age, sexual orientation, gender identity, disability or physical handicap or defames, ridicules, mocks, disparages, threatens, harasses, intimidates or abuses anyone; </li>
                                        <li>Promotes violence or describes how to perform a violent act or promotes terrorism.</li>
                                        <li>Contains targeted insults and/or coordinated efforts against any particular user or group(s) of users and mocking their opinions and/or indulges or promotes name-calling or attack people based on whether You agree with them or indicates a personal vendetta against user or group(s) of users.</li>
                                        <li>Is grossly offensive to the community, including blatant expressions for bigotry, prejudice and hatred.</li>
                                        <li>Exploits or promotes the exploitation of any person or entity and specially minors.</li>
                                        <li>Results in reverse engineering, reverse compiling or otherwise deriving the underlying source code or structure or sequence of any Platform(s), solution or technology or sequence of individual passwords of subscriber sites (or pages contained therein).</li>
                                        <li>Is uploaded or distributed in anticipation of and/or as a result of accepting or soliciting a kickback, monetary or otherwise.</li>
                                      </ul>
                                    </li>
                                  </ul>
                                </p>
                                <p>
                                  <b>Original Content:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>You should upload Content that is Your original work or such Content that You are authorized to upload and/or broadcast and/or share on the Platform(s). </li>
                                    <li>Content that involves replicas, derivative creations, or performances of others' copyrighted content may violate another person’s IPR.</li>
                                    <li>We encourage You to assess Your Content for adherence to applicable IPR laws, including fair use, and to secure all appropriate rights if needed, before sharing the Content on the Platform(s). </li>
                                  </ul>
                                </p>
                                <p>
                                  <b>Lawful Content:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>Content, which is contrary to law, has no place on Our Platform(s). </li>
                                    <li>You must respect all applicable local, national, and international laws including IPR laws and all Our policies while using Our Platform(s) and uploading any Contents on or distributing any Contents through Our Platforms. </li>
                                    <li>Any Content or activity featuring, encouraging, or soliciting illegal activities is prohibited. In order to protect the community and maintain positivity, please do not post any content that is contrary to law. </li>
                                    <li>Distributing any Content shared by Us and/or another user of the Platform(s) that You know, or reasonably should know, is illegal if such distribution has been specifically prohibited.</li>
                                    <li>Please refrain from uploading and/or distributing Content that sells or promotes any services that are unlawful in the location at which the Content is posted or received.</li>
                                  </ul>
                                </p>
                                <p>
                                  <b>Impersonation and right to privacy:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>Impersonation is strictly prohibited. </li>
                                    <li>Activities by You which contribute to impersonation, such as copying a user's channel layout, using a similar username or posing as another person will be considered harassment by You.</li>
                                    <li>Your user profile is Your identity on Our Platform(s). You're welcome to use a screen name but please refrain from using inappropriate names, bios, or profile images.</li>
                                    <li>Everyone has the right to privacy, no matter who they are. We request You to ensure that You do not share personal details including personal information of another person (or entity) or any information that may be used to track, contact or impersonate that individual (or entity), without their permission. This includes recordings of private conversations, photographs, phone numbers, and home or email addresses.</li>
                                  </ul>
                                </p>

                                <p>
                                  <b>Spamming and related activities:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>
                                      We aim for Our Platform(s) to be a spam-free place. We therefore encourage You to refrain from activities that include (but are not limited to) uploading and/or distributing:
                                      <ul>
                                        <li>large amounts of repetitive, unwanted messages or user reports</li>
                                        <li>unauthorized advertisements</li>
                                        <li>Contents that encourage phishing</li>
                                        <li>Contents with an intent to defraud others</li>
                                        <li>Contents spreading malware or viruses</li>
                                        <li>Contents promoting misinformation, disinformation, and malinformation (such as feigning distress, posting misleading metadata, or intentional mis-categorization of content)</li>
                                        <li>Contents with an intent to tamper (such as artificially inflating follow or live viewer stats).</li>
                                      </ul>
                                    </li>
                                    <li>Please refrain from uploading, distributing or promoting Content that has been promoted through the sending of spam or mail fraud schemes, or pages that promote or condone the sending of spam.</li>
                                    <li>Please avoid sending of bulk email, not originating from Our servers soliciting services relating to Storiyoh or un-related services, and mass distributing it to unknown recipients on Our servers and/or Platform(s).</li>
                                  </ul>
                                </p>
                                <p>The Community Guidelines, as elaborated under this Section are generic in nature and are non-exhaustive. Your actions might fall into cases that are not specifically laid down under these Guidelines but they may still be considered a violation of these Guidelines, if deemed so by Us at Storiyoh.</p>
                                <p>
                                  <b>Reporting and Action:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>As a user of the Platform(s) it is possible that You might come across Content that You disagree with or find distasteful from time to time. </li>
                                    <li>If that is the case, the best thing You can do is to ignore that Content, and to block those users who share it, to avoid it in the future. </li>
                                    <li>However, You can choose to report such Contents that You believe violate these Guidelines if You encounter any such Content by specifying the location of the Content and the reason You believe it violates these Guidelines or is distasteful to You. </li>
                                    <li>We have a dedicated team who will review all reports objectively, and remove any Content that violates these Guidelines, Terms of Use or applicable laws.</li>
                                    <li>If We adjudge that a particular Content is infringing and in violation of Our Guidelines or Terms of Use, whether reported by another user or found by Us through any other means, directly and/or indirectly, We will send You (“Infringing User”) a written intimation.</li>
                                    <li>The Infringing User may, on receipt of such intimation, on his own, remove the infringing Content from the Platform(s), if the option to do so is enabled on the Platform(s) or contact Us for such removal.</li>
                                    <li>
                                      On non-response from the Infringing User and/or if We are unable to reach the Infringing User, We may implement either one or a combination of the following precautions (“Precautions”):
                                      <ul>
                                        <li>remove the Content believed to be infringed from the Platform(s) permanently;</li>
                                        <li>Suspend the account of the Infringing User;</li>
                                        <li>Terminate the account of the Infringing User;</li>
                                        <li>Report the infringement to appropriate authorities.</li>
                                      </ul>
                                    </li>
                                    <li>If the Infringing User is not in agreement with such Precautions implemented against him, he can contact Us and We shall try to resolve the matter.</li>
                                    <li>
                                      Please Note:
                                      <ul>
                                        <li>In general, We do not restrict You from uploading or distributing any type of Content. However, what We strongly stand against is if the Content is distasteful, offensive, derogatory or harms any third-party.</li>
                                        <li>We understand that there is no definite method of adjudging if a particular Content specifically violates these Guidelines and to what extent will such violation be tolerated. Any Content may be viewed as a violation by one party while another party may think differently. We at Storiyoh, therefore consider this as a matter of Our prerogative and shall take the necessary safeguards as We deem fit if We, in Our sole discretion, find the Content to be in violation of Our Terms of Use and/or these Guidelines.</li>
                                      </ul>
                                    </li>
                                  </ul>
                                </p>

                                <p>
                                  <b>Warranties:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>You represent and warrant that You own or otherwise control all of the rights to the Content that You post, upload or distribute on or through the Platform(s) and that, as at the date that the Content is uploaded or distributed by You: (i) the Content is accurate; (ii) use of the Content does not breach any applicable policies or Guidelines and will not cause injury to any person or entity (including that the Content or material is not defamatory); (iii) the Content is lawful. </li>
                                    <li>We do not control or endorse the content, messages or information found in Your Content or in any external linked sites that may be linked to or from the Platform(s) and, therefore, We specifically disclaim any responsibility with regard thereto.</li>
                                    <li>The ideas, feedbacks and other material(s) and Contents uploaded by You are yours alone. We do not endorse Your views and provide no warranties with respect to its accuracy or correctness. We shall not be held liable for the contents of such ideas, feedback and other material(s) contained in the Content uploaded and/or distributed on Our Platform(s).</li>
                                  </ul>
                                </p>

                                <p>
                                  <b>Indemnity:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>You agree that You are solely responsible to Us and to any third party for any breach of Your obligations under the Terms of Use and these Guidelines and for the consequences of any such breach.</li>
                                    <li>You shall always indemnify and keep Us indemnified in case of any claims including but not limited to third party claims, claims related to infringement, breach of Your warranties and breach of any of Our policies with respect to the ideas, feedback, content and other material(s) uploaded on and/or distributed by You through Our Platform(s).</li>
                                  </ul>
                                </p>

                                <p>
                                  <b>Contact Us:</b>
                                </p>
                                <p>
                                  <ul>
                                    <li>We encourage You to contact Us in case You have any queries regarding these Guidelines and/or You wish to report an infringement, or otherwise at legal@storiyoh.com. </li>
                                  </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>

</html>