<!doctype html>
<html>
<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
</head>
<body>
    <div class="f1-page-wrapper legal-page">

        <div class="f1-page">
            <div class="uk-container uk-container-center">
                <div class="uk-grid" data-uk-grid-margin>

                    <!-- Sub Menu Component for Legal Pages Code Start -->
                    <legal-sub-nav :subnav="4"></legal-sub-nav>
                    <!-- Sub Menu Component for Legal Pages Code End -->

                    <div class="uk-width-medium-3-4">
                        <div class="f1-info episode-overview">
                            <h1 class="heading uk-text-uppercase">Copyright Policy</h1>
                            <div class="content">
                              <p>
                                <b>Information about Us and the need for Copyright Policy:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>The domain name storiyoh.com is owned by Storiyoh Private Limited, a private company incorporated under the Companies Act, 2013 bearing CIN U74999KL2017FTC050463 and having its registered office at Villa No. 12, Skyline, Greenwoods, Indira Junction, Kakkanad Kochi Ernakulam, Kerala 682030, India (“Storiyoh” or “Us” or “We” or “Our”). </li>
                                  <li>We respect the IP rights of the users accessing Our Platform(s) (“You” or “Your” or “Yourself”) and urge You to do the same and have therefore provided this Copyright Policy (“Policy”). </li>
                                  <li>We are committed to removing infringing services and Contents from Our Platform(s). To facilitate this, We have put in place an infringement verification process, as laid down in this Policy (“Infringement Verification Process” or “Process”) so that IP rights owners could easily report Contents on Our Platform(s) that infringe their rights. </li>
                                  <li>If We receive proper notification of IP infringement, along with supporting documents, Our response to such notices will include removing or disabling access to material and/or Content claimed to be the subject of infringing activity. </li>
                                </ul>
                              </p>
                              <p>
                                <b>Prospective Amendments:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>We reserve the right, at Our sole discretion, to change, modify, add or remove portions of this Policy (“Changes”). You are requested to check this Policy periodically for Changes as Your use of the Platform(s) is subject to the most current version of this Policy and Your continued use of the Platform(s) following such Changes, will mean that You accept and agree to the Changes. </li>
                                  <li>By mere access or use of the Platform(s), You expressly consent to this Policy. </li>
                                  <li>This Copyright Policy is to be read concurrently with Our Terms of Use, Privacy Policy and Community Guidelines. </li>
                                </ul>
                              </p>
                              <p>
                                <b>General Definitions:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>“Content” includes, without limitation, any information, data, text, logos, photographs, video, audio, animations, written posts, text, articles, comments, software, scripts, graphics, themes and interactive features generated, underlying source code, provided or otherwise made accessible on or through the Platform(s) including User Generated Content. </li>
                                  <li>“IP” shall mean all copyrights (including without limitation the exclusive right to reproduce, distribute copies of, display and perform the copyrighted work and to prepare derivative works), copyright registrations and applications, trademark rights (including, without limitation, registrations and applications), patent rights (whether registered or unregistered), trade names or business names, registered and unregistered design rights, mask-work rights, trade secrets, moral rights, author’s rights, rights in packaging, goodwill and other intellectual property rights, and all renewals and extension thereof and all rights or forms of protection of a similar nature or having an equivalent or similar effect to any of the above, which may subsist anywhere in the world, regardless of whether any of such rights arise under the laws of India or any other state, country or jurisdiction.</li>
                                  <li>“User Generated Content” shall mean any content generated by the Users and aggregated on the Platform(s) including but not limited to messages and/or opinions and/or feedbacks or distributed and/or made available via RSS.</li>
                                </ul>
                              </p>
                              <p>
                                <b>How to report infringement:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>If You, in good faith, believe that Your IP rights have been infringed by any of the Content on Our Platform(s), You may follow the process given under this Policy.</li>
                                  <li>
                                    We require that the IP owner or the authorized agent of such IP owner, provides the following minimum information and/or detail to Us on <a href="mailto:legal@storiyoh.com">legal@storiyoh.com</a>. A sample of such e-mail is enclosed as Annexure 1:
                                    <ul>
                                      <li>Identification or description of the IP protected Content that has been infringed.</li>
                                      <li>Clear identification or description of where the Content that is claimed to be infringing is located on the Platform(s) with adequate particulars podcast id/links of infringing Contents.
                                        <br>
                                        (Note: We are unable to process requests which do not specify exact podcast links or URLs. Please do not provide links to browser pages or links of search queries as these pages are dynamic and their contents change with time).
                                      </li>
                                      <li>Your identification details.</li>
                                      <li>If You are not the IP owner, then details of the owner of the IP rights and a statement by You that You are authorized to act on the IP owner's behalf.</li>
                                      <li>Proof of ownership of IP by the IP owner. </li>
                                      <li>A statement by You that You believe that the use of the Content complained of on the Platform(s), is not authorized to be on the Platform(s) by the IP owner, its agent, or the law.</li>
                                    </ul>
                                  </li>
                                </ul>
                              </p>
                              <p>
                                <b>The Infringement Verification Process:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>We have in place the Infringement Verification Process so that IP owners can easily report Content that they believe, infringes their rights. It is in Our interest to ensure that infringing Contents are removed from our Platform(s), as they erode the trust You place in Us.</li>
                                  <li>
                                    You can make use of Our Process:
                                    <ul>
                                      <li>If You are an IP owner (or such owner’s authorized agent) and want to report the issue of an infringing Content; </li>
                                      <li>If Your User Generated Content was removed through the Process, and You believe that Your User Generated Content was removed in error;</li>
                                      <li>If You are a vigilant user and wish to report an infringing Content.</li>
                                    </ul>
                                  </li>
                                  <li>This Process works to ensure that Contents on the Platform(s) do not infringe upon the copyright, trademark or other IP rights of third parties. We encourage Our users to be participants in this Process. </li>
                                </ul>
                              </p>
                              <p>
                                <b>Procedure after submission of infringement claim:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>On receiving any infringement claim through Our Process, We shall process the claim and notify the user who has uploaded such infringing Content (“Infringing User”) (if identifiable by Us through reasonable efforts) and such Infringing User shall be sent an intimation detailing such infringement claim.</li>
                                  <li>The Infringing User may on receipt of such intimation, on his own, remove the infringing Content from the Platform(s), if the option to do so is enabled on the Platform(s) or contact Us for such removal.</li>
                                  <li>The Infringing User may connect with Us to give him the details of the IP owner so that the Infringing User may directly connect with such IP owner.</li>
                                  <li>
                                    On non-response from the Infringing User and/or if We are unable to reach the Infringing User, We may implement either one or a combination of the precautions (“Precautions”):
                                    <ul>
                                      <li>remove the Content believed to be infringed from the Platform(s) permanently;</li>
                                      <li>Suspend the account of the Infringing User;</li>
                                      <li>Terminate the account of the Infringing User;</li>
                                      <li>Report the infringement to appropriate authorities.</li>
                                    </ul>
                                  </li>
                                  <li>If the Infringing User is not in agreement with such Precautions implemented against him, he can contact Us, and We shall try to resolve the matter.</li>
                                </ul>
                              </p>
                              <p>
                                <b>Warranties:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>We do not control or endorse the content, messages or information found in Your Content or in any external linked sites that may be linked to or from the Platform(s) and, therefore, We specifically disclaim any responsibility with regard thereto.</li>
                                  <li>The ideas, feedbacks and other material(s) and Contents uploaded on the Platform(s) by Our users are theirs alone. We do not endorse their views and provide no warranties with respect to its accuracy or correctness. We shall not be held liable for the contents of such ideas, feedback and other material(s) contained in the Content uploaded and/or distributed on Our Platform(s).</li>
                                </ul>
                              </p>
                              <p>
                                <b>Legal Advice:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>We do not and cannot verify if anybody has the right to upload and/or distribute the Contents and neither can We verify the Content uploaded and/or distributed by You. </li>
                                  <li>We cannot provide legal advice on what You should upload on the Platform(s) and/or distribute through the Platform(s). However, if You have questions or concerns regarding Your rights, You may wish to seek legal advice from a specialist. If You are unsure about the Content You wish to upload and/or distribute, We advise You refrain from uploading and/or distributing it. </li>
                                  <li>Uploading and/or distributing Content that infringes a third party’s IP rights, without permission is a violation of the law and If You have already uploaded and/or distributed such Content, You should remove it, if the option to do so is enabled on the Platform(s) or contact Us for such removal.</li>
                                </ul>
                              </p>
                              <p>
                                <b>Indemnity:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>You agree that You are solely responsible to Us and to any third party for any breach of Your obligations under the Terms of Use and this Policy and for the consequences of any such breach.</li>
                                  <li>You shall always indemnify and keep Us indemnified in case of any claims including but not limited to third party claims, claims related to infringement and breach of any of Our policies or Terms of Use or with respect to the ideas, feedback, content and other material(s) uploaded on and/or distributed by You through Our Platform(s).</li>
                                </ul>
                              </p>
                              <p>
                                <b>Contact Us:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>We encourage You to contact Us in case You have any queries regarding this Policy and/or You wish to report an infringement, or otherwise at <a href="mailto:legal@storiyoh.com">legal@storiyoh.com</a>.</li>
                                </ul>
                              </p>
                            </div>
                            <div class="content">
                              <br>
                              <br>
                              <br>
                              <div class="">
                                <b>Annexure 1 <br>
                                Notice of Infringement</b>
                              </div>
                              <br>
                              To
                              <br>
                              <br>
                              <br>
                              <p>
                                <b>Storiyoh Private Limited</b>
                                <br>
                                <br>
                                I, [NAME], resident of [ADDRESS] do solemnly and sincerely declare as follows:
                              </p>
                              <p>I am the owner of certain intellectual property rights ("IP Owner"),</p>
                              <p>OR:</p>
                              <p>I am, an authorized representative of [NAME OF THE IP OWNER], the IP Owner, </p>
                              <p>and I believe in good faith that the contents or materials identified in Schedule 1 enclosed hereto are not authorized by me/the IP Owner or the law and therefore infringe my/the IP Owner's rights. </p>
                              <p>Please expeditiously remove or disable access to the material or contents claimed to be infringing.</p>
                              <p>I may be contacted at:</p>
                              <p>Name:</p>
                              <p>Title & Company:	</p>
                              <p>Address:</p>
                              <p>Email (correspondence):</p>
                              <p>Telephone/Fax: </p>
                              <p>The details and proof provided by me with respect to the ownership of IP in Schedule 2 is true and accurate and is competent to be admissible as evidence in any court of law.</p>
                              <p>I make this declaration conscientiously believing it to be true and correct.</p>
                              <p>Declared by </p>
                              <p>on [date] at [place]</p>
                              <p>Truthfully,</p>
                              <p>Signature</p>
                            </div>
                            <div class="content">
                              <br>
                              <br>
                              <br>
                              <br>
                              <p>
                                <b>Schedule 1 to Notice of Infringement:</b>
                              </p>
                              <p>
                                <ul>
                                  <li>List of allegedly infringing Content(s), or materials;</li>
                                  <li>Its location on the Platform(s);</li>
                                  <li>Details of the person who has uploaded such infringing Content(s) (if identifiable);</li>
                                  <li>
                                    Select the most appropriate reason (non-exhaustive list specifying examples of infringement).
                                    <br>
                                    <br>
                                    Trademark-infringement
                                    <br>
                                    <br>
                                    <ul>
                                      <li>Trademark- description infringement</li>
                                      <li>Content (s) has unlawful comparison to trademark owner's brand or product</li>
                                      <li>Content (s) contains unlawful use of trademark owner's logo</li>
                                    </ul>
                                    <br>
                                    Copyright-item infringement
                                    <br>
                                    <br>
                                    <ul>
                                      <li>Software is being offered without any license or in violation of a license</li>
                                      <li>Content (s) is a bootleg recording</li>
                                      <li>Content (s) is an unlawful copy </li>
                                      <li>Content (s) is unlawful duplication of material</li>
                                      <li>Content (s) is an unlawful copy of other copyrighted work </li>
                                    </ul>
                                    <br>
                                    Copyright-listing Content infringement
                                    <br>
                                    <br>
                                    <ul>
                                      <li>Content (s) comprises unauthorized copy of copyrighted text</li>
                                      <li>Content (s) comprises unauthorized copy of copyrighted image</li>
                                      <li>Content (s) comprises unauthorized copy of copyrighted image and text</li>
                                    </ul>
                                  </li>
                                </ul>
                              </p>
                              <br>
                              <p><b>Schedule 2 to Notice of Infringement:</b></p>
                              <p>Please provide the proof of ownership of IP as We cannot process unsupported claims.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>

</html>