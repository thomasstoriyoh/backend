<!doctype html>
<html>
<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
</head>

<body>
    <div class="f1-page-wrapper legal-page">
        <div class="f1-page">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">

                    <div class="uk-width-medium-3-4">
                        <div class="f1-info episode-overview">
                            <h1 class="heading uk-text-uppercase">Legal disclaimer</h1>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam mauris turpis, ut tristique ligula iaculis eu. Donec ut mi nulla. Maecenas blandit vel enim nec pulvinar. Curabitur nec nulla sed magna malesuada blandit ut vel nisl. Sed tincidunt libero mauris, quis suscipit felis auctor non. Duis elementum lacinia dui at feugiat. Morbi luctus molestie massa, ac rhoncus arcu luctus maximus. Sed vel arcu quis lectus scelerisque semper.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam mauris turpis, ut tristique ligula iaculis eu. Donec ut mi nulla. Maecenas blandit vel enim nec pulvinar. Curabitur nec nulla sed magna malesuada blandit ut vel nisl. Sed tincidunt libero mauris, quis suscipit felis auctor non. Duis elementum lacinia dui at feugiat. Morbi luctus molestie massa, ac rhoncus arcu luctus maximus. Sed vel arcu quis lectus scelerisque semper.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam mauris turpis, ut tristique ligula iaculis eu. Donec ut mi nulla. Maecenas blandit vel enim nec pulvinar. Curabitur nec nulla sed magna malesuada blandit ut vel nisl. Sed tincidunt libero mauris, quis suscipit felis auctor non. Duis elementum lacinia dui at feugiat. Morbi luctus molestie massa, ac rhoncus arcu luctus maximus. Sed vel arcu quis lectus scelerisque semper.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam mauris turpis, ut tristique ligula iaculis eu. Donec ut mi nulla. Maecenas blandit vel enim nec pulvinar. Curabitur nec nulla sed magna malesuada blandit ut vel nisl. Sed tincidunt libero mauris, quis suscipit felis auctor non. Duis elementum lacinia dui at feugiat. Morbi luctus molestie massa, ac rhoncus arcu luctus maximus. Sed vel arcu quis lectus scelerisque semper.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam mauris turpis, ut tristique ligula iaculis eu. Donec ut mi nulla. Maecenas blandit vel enim nec pulvinar. Curabitur nec nulla sed magna malesuada blandit ut vel nisl. Sed tincidunt libero mauris, quis suscipit felis auctor non. Duis elementum lacinia dui at feugiat. Morbi luctus molestie massa, ac rhoncus arcu luctus maximus. Sed vel arcu quis lectus scelerisque semper.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>