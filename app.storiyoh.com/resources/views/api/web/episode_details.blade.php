<!doctype html>
<html>
<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    {{--  <meta name="viewport" content="initial-scale=1, minimum-scale=1, maximum-scale=3.0, user-scalable=1" />  --}}
    {{--  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=6.0, minimum-scale=1, user-scalable=yes"/>  --}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
    <style>
        .content ul li {
            padding-bottom: 10px !important;
        }
        .legal-page .f1-page {
            padding: 0px 0;
        }
        img{
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .dark-mode{
            background: #000;
            color: #FFF;            
        }
        em {
            color: #000 !important;
            font-weight: 400 !important;
        }
    </style>
</head>

<body>
    <?php
        $dark_mode = "";
        if(! is_null($view_mode)){ 
            if($view_mode == "darkMode") {
                $dark_mode = "dark-mode";
            }
        }        
    ?>
    <div class="f1-page-wrapper legal-page {{ $dark_mode }}">
        <div class="f1-page">
            <div class="uk-container uk-container-center">
                <div class="uk-grid uk-grid-collapse">
                    <div class="uk-width-medium-3-4">
                        <div class="f1-info episode-overview">
                            <div class="content">
                                {!! $episode !!}
                            </div>
                            <div style="margin-bottom: 75px;">&nbsp;</div>                
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
