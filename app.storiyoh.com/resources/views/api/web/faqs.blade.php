<!doctype html>
<html>

<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
    <style>
    </style>
</head>

<body>
    <?php
        $dark_mode = "";
        $concat_url = "";
        if(! is_null($view_mode)){ 
            if($view_mode == "darkMode") {
                $dark_mode = "dark-mode";
                $concat_url = "/darkMode";
            }
        }        
    ?>
    <div class="f1-page-wrapper about-page">
        <div class="f1-page">
            <div class="acordion cat">                
                @foreach($faq as $key => $item)                    
                    <div class="acordion-section">
                        <a href="{{ url('api/faqs/details/') ."/". $key . $concat_url}}">
                            <h3 class="acordion-title">{{ $item }}</h3>
                        </a>
                    </div>                    
                @endforeach                                
            </div>
        </div>
    </div>
</body>
</html>