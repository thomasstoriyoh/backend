<!doctype html>
<html>

<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
    <style>
    </style>
</head>

<body>
    <?php
        $dark_mode = "";
        if(! is_null($view_mode)){ 
            if($view_mode == "darkMode") {
                $dark_mode = "dark-mode";                
            }
        }        
    ?>
    <div class="f1-page-wrapper about-page">

        <div class="f1-page">
            <h2 class="acordion-main-title"><a href="{{ url('api/faqs/') }}"><img src="{{ asset('assets/frontend') }}/images/acc-arrow_2.jpg" alt=""></a> &nbsp;&nbsp; {{ $faqCategory->title }}</h2>
            <div class="acordion">                
                @foreach($data as $item)
                <div class="acordion-section">
                    <h3 class="acordion-title">{{ $item->title }}</h3>
                    <div class="acordion-content">
                        {!! $item->answer !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
        $(".acordion-section:first").addClass("acordion-section-active");
        $(".acordion-section:first .acordion-title").addClass("acordion-title-active");
        $(".acordion-title").click(function() {
            $(".acordion-title").removeClass("acordion-title-active");
            $(this).addClass("acordion-title-active");
            $(".acordion-content").stop().slideUp();
            $(this).next(".acordion-content").stop().slideDown();
            $(".acordion-section").removeClass("acordion-section-active");
            $(this).parent(".acordion-section").addClass("acordion-section-active");
        });
    </script>
</body>

</html>