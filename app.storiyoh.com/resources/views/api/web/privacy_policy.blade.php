<!doctype html>
<html>
<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
    <style type="text/css">
        h1 {
            font-size: 14px;
            font-weight: 600;
            color: #1d68f1;
            margin: 0;
            line-height: 1.428;
            text-transform: uppercase;
        }
        h2 {
            font-size: 14px;
            font-weight: 700;
            margin: 0;
            line-height: 1.428
        }
        .new-legal-sec h1,
            .new-legal-sec .uk-h1 {
            font-size: 16px;
            font-weight: 600;
            color: #1d68f1;
            line-height: 1.3;
            margin-bottom: 16px;
            margin-top: 30px;
        }
        .new-legal-sec ol, .new-legal-sec ul {
            padding-left: 40px;
        }
        .new-legal-sec p, .new-legal-sec ul, .new-legal-sec ol, .new-legal-sec li {
            margin: 10px 0;
        }
        .new-legal-sec .nw-number-list {
            list-style: none;
            margin: 10px 0 0;
        }
        .new-legal-sec .nw-number-list > li {
            position: relative;
        }
        .new-legal-sec .nw-number-list > li:before {
            content: attr(list-number);
            font-size: 14px;
            position: absolute;
            left: -40px;
            top: 0px;
            width: 36px;
            text-align: right;
        }
    </style>
</head>
<body>
    <div class="f1-page-wrapper legal-page">
        <div class="f1-page">
            <div class="uk-container uk-container-center">
                <div class="new-legal-sec">
                    <h1>Privacy Policy</h1>
                    <p>Last updated on 29th May 2019</p>
                    <p>Welcome to the App, Storiyoh and/or Website, <a class="uk-link-text" target="_blank" href="https://www.storiyoh.com/">www.storiyoh.com</a> (collectively <strong>“Platform”</strong>) owned and hosted by Storiyoh DMCC. By visiting the Platform and/or using the service provided by Us to You (<strong>“Service”</strong>), You are deemed to have accepted the terms relating to privacy of information shared by You through the Platform or by using the Service. This Policy is part of our Terms of Use. Capitalized terms used here but not defined in this Policy have the meaning given to them in our Terms of Use (<a class="uk-link-text" href="https://www.storiyoh.com/legal/terms-of-use">https://www.storiyoh.com/terms-of-use</a>).</p>

                    <ol class="nw-number-list">
                        <li list-number="1">
                            <strong>Some terms used in this Policy explained:</strong>

                            <ul class="nw-number-list">
                                <li list-number="1.1">
                                    <strong>“Information"</strong> shall mean any information You provide to us or other users of the Platform(s) including information provided by You, in the registration process, in the feedback area, bulletin board and messages or through any e-mail feature and shall include User Content and Your Personal Information.
                                </li>

                                <li list-number="1.2"> <strong>“Personal Information” </strong> shall mean information disclosed by or obtained from any user of the Platform(s) or Service including name, age, any other personal details capable of identifying a person and any other information about the user (including any literary or artistic works) that the user willingly submits to the Platform(s) or while using the Service.
                                </li>
                            </ul>
                        </li>

                        <li list-number="2">
                            <strong>Information about Us and the need for Privacy Policy:</strong>

                            <ul class="nw-number-list">
                                <li list-number="2.1">The domain name storiyoh.com is owned by Storiyoh DMCC, a limited liability company registered with Dubai Multi Commodities Centre Authority having its registered address at Unit No.157, DMCC Business Centre, Level No. PF, Gold Tower (AU), Dubai, United Arab Emirates. </li>
                                <li list-number="2.2">We are concerned about the privacy of the users accessing Our Platform or availing the Service (referred to as <strong>“You”</strong>).
                                </li>

                                <li list-number="2.3">
                                    Your privacy is important to Us and We have therefore provided this Privacy Policy (<strong>“Policy”</strong>) to familiarize You with the manner in which We collect, use and disclose Your Personal Information collected through the Platform(s) and/or by availing the Service.

                                </li>
                                <li list-number="2.4">
                                    When You access or use Our Platform/Service, We collect and store Your Personal Information which is provided by You from time to time during the course of Your use or access of Platform/Service.

                                </li>

                                <li list-number="2.5">
                                    We do this to improve the experience of the Platform(s) and Services and to provide You with a safe, efficient, smooth and customized experience. We may collect such Personal Information from You that We consider necessary for achieving this purpose.

                                </li>
                            </ul>
                        </li>

                        <li list-number="3">
                            <strong>Changes to the Policy:</strong>

                            <p>We may revise the Policy from time to time and will make the revised Policy available through the Platform with the date of the latest revision. Your continued use of the Platform/Service after the revised Policy has become effective indicates that you have read, understood and agreed to the current version of the Policy. This Privacy Policy is to be read concurrently with our Terms of Use (<a class="uk-link-text" href="https://www.storiyoh.com/legal/terms-of-use">https://www.storiyoh.com/terms-of-use</a>), including the Community Guidelines and Copyright Policy, which are part of the Terms of Use.
                            </p>

                        </li>

                        <li list-number="4">
                            <strong>Collection of Information:</strong>

                            <ul class="nw-number-list">
                                <li list-number="4.1">You can browse the Platform(s) without telling Us who You are or revealing any Personal Information about Yourself. If You give Us Your Personal Information, then, You are not anonymous to Us. You always have the option to not provide information by choosing not to use a particular service or feature on the Platform(s).
                                </li>
                                <li list-number="4.2">You may need to provide Us Your Personal Information during the course of using some of Our Services or while registering with Us. </li>
                                <li list-number="4.3">We also collect Information which You provide to Us directly. This may include:

                                    <ol type="i">
                                        <li>Your username, password and e-mail address when You register with Storiyoh;</li>
                                        <li>Profile information that You provide for Your user profile including Your name and profile picture. This information allows Us to help You or others be "found" on Storiyoh.</li>
                                        <li>Content You post on the Platform(s) <strong>(“User Content”)</strong>
                                        <li>Communications between You and Us. </li>
                                    </ol>


                                </li>

                                <li list-number="4.4">
                                    We also collect associated metadata which is the technical data that is associated with User Content. You can add or may have metadata added to Your User Content including “#” or “@” (to mark keywords when You post) or geotag (to mark Your location). This makes Your User Content more searchable by others and more interactive. We may run contests, special offers or other events or activities ("Events") on the Platform(s). If You do not want to participate in an Event, do not use the particular metadata associated with that Event.
                                </li>


                                <li list-number="4.5">
                                    We also collate location Information which is collected by Us only after You enable the necessary permissions on Your device from which You access Our Platform(s).
                                </li>

                                <li list-number="4.6">
                                    We may automatically track certain Information about You based on Your behavior, acts, subscriptions and preferences on Our Platform(s).

                                </li>

                                <li list-number="4.7">
                                    We may collect certain technical Information with respect to Your device like the date and time of visiting the Platform, Your IP address and the type of browser being used.

                                </li>

                                <li list-number="4.8">
                                    Some information may be stored in Our server logs. These encrypted statistics do not identify You personally, but provide Us the information regarding Your visit to the Platform(s) and the type of user who is accessing Our Platform and certain browsing activities by You. This data may include the pages You last visited before visiting Our Platform(s), the duration of Your stay on Our Platform(s) etc.
                                </li>

                                <li list-number="4.9">
                                    It is entirely at Your discretion as to what You share in Your User Profile. Information saved in the ‘User Profile’ page will be publicly accessible. You can remove or edit any information at any time.
                                </li>

                                <li list-number="4.10">
                                    Other automatic information that We collect include account information, purchase or redemption information, information about your interactions with other businesses etc.
                                </li>


                            </ul>
                        </li>

                        <li list-number="5">
                            <strong>Use of Your Information:</strong>

                            <ul class="nw-number-list">
                                <li list-number="5.1">
                                    We may only act as a passive conduit for Your online distribution and publication of Your Information.

                                </li>
                                <li list-number="5.2">
                                    We may use Your Information to:
                                    <ul class="nw-number-list">
                                        <li list-number="a)">help You efficiently access Your User Content after You sign in;</li>
                                        <li list-number="b)">remember Information so You will not have to re-enter it during Your next visit to the Platform(s);
                                        </li>
                                        <li list-number="c)">provide personalized content and information to You and others, which could include online ads or other forms of marketing through newsletters, e-mails or any other methods;</li>
                                        <li list-number="d)">provide, improve, test, and monitor the effectiveness of Our Platform/Service;
                                        </li>
                                        <li list-number="e)">monitor metrics such as total number of visitors, traffic, and demographic patterns</li>
                                        <li list-number="f)">diagnose or fix technology problems;</li>
                                        <li list-number="g)">automatically update Our App on Your device;</li>

                                    </ul>
                                </li>
                                <li list-number="5.3">
                                    Personal Information provided by You is used for the following purposes:
                                    <ul class="nw-number-list">
                                        <li list-number="a)">to facilitate Your use of the Platform/Service and to help You address Your problems with the Platform/Service;
                                        </li>
                                        <li list-number="b)">to provide You with information about Our Platform/Service;

                                        </li>
                                        <li list-number="c)">to personalize Your experience on the Platform;
                                        </li>
                                        <li list-number="d)">for proper administration and protection of the integrity of the Platform; and

                                        </li>
                                        <li list-number="e)">to provide information as per any legal requirement
                                        </li>

                                    </ul>
                                </li>
                                <li list-number="5.4">User shall have a choice to listen to an episode discretely without the knowledge of other users in the Platform. User may also create a private playlist which will not be shared with other users of the Platform. However, these will be tracked by Us for providing personalized Services to You.</li>
                                <li list-number="5.5">
                                    We use automatically collected information for the purposes of internal research on Our users to better understand, protect and provide Our users a better experience on Our Platform(s).

                                </li>
                                <li list-number="5.6">We use Your Information to take and handle orders, deliver products and services, process payments, and communicate with you about orders, products, services, and promotional offers.
                                </li>
                                <li list-number="5.7">Any information or content that You voluntarily disclose on the Platform(s), such as User Content, becomes available to the public, as controlled by any applicable privacy settings that You set. To change Your privacy settings on the Platform(s), please change Your profile setting. Once You have shared User Content or made it public, that User Content may be re-shared by others.</li>
                                <li list-number="5.8">We may share any Information with Our other affiliates to help detect and prevent identity theft, fraud and other potentially illegal acts and to facilitate joint or co-branded services where such services are provided by more than one corporate entity.
                                </li>
                                <li list-number="5.9">Some of the information related to the Platform(s) and Your visit to the Platform(s) may be shared with Our associates in order to enhance the Platform(s).
                                </li>
                                <li list-number="5.10">If We sell or otherwise transfer part or the whole of Storiyoh or Our assets to another organization, Your Information may be among the items sold or transferred for the sole purpose of continuing the operation of the Service, and only if the recipient of the Personal Information commits to a privacy policy that has terms substantially consistent with this Policy.
                                </li>

                                <li list-number="5.11">
                                    We may disclose Your Information to the extent such disclosure is reasonably necessary to enforce Our Terms of Use or Our other policies and in order to respond to claims of any other user or third-party.

                                </li>
                                <li list-number="5.12">
                                    Storiyoh wishes to inform you that our intention is to use User Generated Content primarily to publish, distribute, display and give access to these on the Platform to improve Content discovery and to build a community feel on the Platform. Storiyoh will also provide access to User Generated Content to other Users primarily to help other Users to discover and evaluate Content on the Platform. Storiyoh does not intend to sell Your User Generated Content in any manner.


                                </li>


                            </ul>
                        </li>

                        <li list-number="6">
                            <strong>Data Security:</strong>

                            <ul class="nw-number-list">
                                <li list-number="6.1">Due to the existing environment, We cannot ensure that all of Your Information will never be disclosed in ways not otherwise described in this Policy. Therefore, although We use industry standard practices to protect Your privacy, no method of transmission over the Internet, or method of electronic storage, is hundred percent secure. If You believe your Personal Information has been compromised, please contact Us at the details set forth in Contact Us section in this Policy. If we learn of a security systems breach, we will inform You and the authorities of the occurrence of the breach in accordance with applicable law.
                                </li>
                                <li list-number="6.2">Your Information may be stored and processed in India or any other country in which We or Our service providers or third-parties, as referred in this Policy, maintain facilities.
                                </li>
                                <li list-number="6.3">We use commercially reasonable safeguards to help keep the Information collected through the Platform(s) secure and take reasonable steps (such as requesting a unique password) to verify Your identity before granting You access to Your account. However, We cannot ensure the security of any Information You transmit on the Platform(s)or guarantee that Information on the Platform(s) may not be accessed, disclosed, altered, or destroyed.
                                </li>
                                <li list-number="6.4">When You access Our Platform(s), We offer the use of a secure server. The secure server software (SSL) encrypts all Information You put in before it is sent to Us.
                                </li>

                            </ul>
                        </li>

                        <li list-number="7">
                            <strong>Cookies and Cache:</strong>

                            <ul class="nw-number-list">
                                <li list-number="7.1">We use data collection devices such as "cookies" and “cache” (collectively, <strong>“Cookies” </strong>) on certain pages of the Platform(s) to help analyse Our web page flow, measure promotional effectiveness, and promote trust and safety. We offer certain features that are only available through the use of Cookies.
                                </li>
                                <li list-number="7.2">We place both permanent and temporary Cookies in Your device. Most Cookies are "session cookies," which means they are automatically deleted from Your device at the end of a session.
                                </li>
                                <li list-number="7.3">You can stop Cookies from being downloaded on Your device from the Platform(s) at any time by selecting the appropriate settings in Your browser. You can also delete Your browser Cookies or disable them entirely. The browsers will tell You how to change Your browser settings to notify You when a Cookie is being set or updated, or to restrict or block certain types or all Cookies. In case You decline to download Cookies from Our Platform(s)You may not be able to use certain features on the Platform(s), but this may significantly impact Your experience with the Platform(s) and may make parts of the Platform(s) non-functional or inaccessible. We recommend that You leave them turned on.</li>
                                <li list-number="7.4">
                                    You may encounter Cookies or other similar devices on certain pages of the Platform(s) that are placed by third parties. We work with third parties to support the Platform(s) by serving advertisements or providing services like commenting on content, allowing users to share content, customizing content based on usage etc. These third parties use their Cookies to collect information on various websites. We do not control the use of Cookies by third parties nor are We in any way responsible for the same.

                                </li>

                            </ul>
                        </li>

                        <li list-number="8">
                            <strong>Duration of maintaining and using Your Information:</strong>

                            <ul class="nw-number-list">
                                <li list-number="8.1">Following deactivation of Your account, We may retain certain Information (including Your Personal Information and User Content) for a reasonable time for backup, archival, and/or audit purposes. We will remove Your public posts from view and/or dissociate them from Your user profile, but We may retain Information about You for the purposes authorized under this Policy unless prohibited by law.
                                </li>
                                <li list-number="8.2">We may, at our discretion, either delete Your Personal Information or de-identify it so that it is anonymous and not attributed to Your identity. For example, We may retain Information to prevent, investigate, or identify possible wrongdoing in connection with Our services or to comply with legal obligations.
                                </li>
                                <li list-number="8.3">User shall have the right to request access to and rectification or erasure of Personal Information or restriction of processing concerning You or to object to processing as well as the right to information/data portability by connecting to Us at <a href="mailto:legal@storiyoh.com">legal@storiyoh.com</a>
                                </li>
                            </ul>
                        </li>

                        <li list-number="9">
                            <strong>Miscellaneous</strong>

                            <ul class="nw-number-list">
                                <li list-number="9.1">
                                    We do not knowingly collect or solicit any Information from minors or knowingly allow such persons to access or register for the services on Our Platform(s). In the event that We learn that We have collected Personal Information from a minor without parental consent, We will delete that Information as quickly as possible.

                                </li>
                                <li list-number="9.2">The name and contact details of the Grievance Officer, should you wish to report any grievances under this Privacy Policy are provided below:
                                    Name: Rahul Radhakrishnan Nair
                                    E-mail id: <a href="mailto:legal@storiyoh.com">legal@storiyoh.com</a>
                                </li>

                            </ul>
                        </li>

                        <li list-number="10">
                            <strong>Contact Us:
                            </strong>

                            <p>We encourage You to contact Us in case You have any queries regarding this Policy and/or wish to report an infringement, breach or otherwise at <a href="mailto:legal@storiyoh.com">legal@storiyoh.com</a>.
                            </p>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</body>
</html>