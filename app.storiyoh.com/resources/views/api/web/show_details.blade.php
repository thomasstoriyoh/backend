<!doctype html>
<html>
<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--  <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />  --}}
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
    <style>
        .content ul li {
            padding-bottom: 10px !important;
        }
        .legal-page .f1-page {
            padding: 0px 0;
        }
        img{
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .dark-mode{
            background: #000;
            color: #FFF;            
        }
        em {
            color: #000 !important;
            font-weight: 400 !important;
        }
    </style>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.12/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.12/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.12/js/uikit-icons.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
</head>

<body>
    <?php
        $dark_mode = "";
        if(! is_null($view_mode)){ 
            if($view_mode == "darkMode") {
                $dark_mode = "dark-mode";
            }
        }        
    ?>
<div class="f1-page-wrapper legal-page {{ $dark_mode }}">
        <div class="f1-page">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-medium-3-4">
                        <div class="f1-info episode-overview">
                            <div class="content">
                                {!!$show!!}
                            </div>
                            <div style="margin-bottom: 75px;">&nbsp;</div>                            
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('img').each(function ()
            {
                var currentImage = $(this);
                currentImage.wrap("<div uk-lightbox><a href='" + currentImage.attr("src") + "' </a></div>");
            });
        });
    </script>                
</body>
</html>