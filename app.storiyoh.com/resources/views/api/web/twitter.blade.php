<!doctype html>
<html>

<head>
    <title>storiyoh</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/uikit.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend') }}/css/main.css">
    <style>
        .f1-page-wrapper{height:100vh;}
    </style>
</head>

<body>
    <div class="f1-page-wrapper">

        <div class="center-vertical">
            <div class="center-content uk-text-center">
                <img src="{{ asset('assets/frontend') }}/images/icons8-twitter.png" alt="twitter">
                <p>Loading...</p>
            </div>
        </div>

    </div>

</body>

</html>