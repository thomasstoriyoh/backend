<!DOCTYPE html>
<html>
  <head>
    <title>User verification</title>
  </head>
  <body>
    @if($user)
    <h2>Welcome to the Storiyoh, {{ $user->first_name }}</h2>
    <p>{{ $status }}</p>
    @else
    <p>{{ $warning }}</p>
    @endif
  </body>
</html>