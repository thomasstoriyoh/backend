<?php
    $newsletter = $mailData['data']['newsletter'];
    $user = $mailData['data']['user'];
    $trending_podcasts = $mailData['data']['trending_podcasts'];
    $featured_collections = $mailData['data']['featured_collections'];
    $featured_episodes = $mailData['data']['featured_episodes'];
    $essentials = $mailData['data']['essentials'];
    $podcast_of_the_week = $mailData['data']['podcast_of_the_week'];

    $urldata = [
        'id' => $user->id,
        'email' => $user->email,
    ];

    $token = urlencode(encrypt(json_encode($urldata)));
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->

    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
    <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->

    <!-- CSS Reset : BEGIN -->
    <style>
        /* What it does: Remove spaces around the email design added by some email clients. */

        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */

        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        /* What it does: Stops email clients resizing small text. */

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */

        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */

        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */

        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }

        table table table {
            table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */

        *[x-apple-data-detectors],
        /* iOS */

        .x-gmail-data-detectors,
        /* Gmail */

        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */

        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */

        img.g-img+div {
            display: none !important;
        }

        /* What it does: Prevents underlining the button text in Windows 10 */

        .button-link {
            text-decoration: none !important;
        }

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */

        /* Create one of these media queries for each additional viewport size you'd like to fix */

        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */

        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
            .email-container {
                min-width: 320px !important;
            }
        }

        /* iPhone 6, 6S, 7, 8, and X */

        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            .email-container {
                min-width: 375px !important;
            }
        }

        /* iPhone 6+, 7+, and 8+ */

        @media only screen and (min-device-width: 414px) {
            .email-container {
                min-width: 414px !important;
            }
        }
    </style>
    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style>
        /* What it does: Hover styles for buttons */

        /*.button-td,
        .button-a {
            transition: all 100ms ease-in;
        }

        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }*/

        a, a:hover, a:active, a:visited {
            color: unset;
            text-decoration: none;
            outline: 0;
        }

        /* Media Queries */

        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }

            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

            /* What it does: Adjust typography on small screens to improve readability */
            .email-container p {
                font-size: 17px !important;
            }
        }
    </style>
    <!-- Progressive Enhancements : END -->

    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

</head>

<body width="100%" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #FFFFFF; text-align: left;">
        <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#222222">
    <tr>
    <td>
    <![endif]-->

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: arial,sans-serif;">
            
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!-- Create white space after the desired preview text so email clients don’t pull other distracting text into the inbox preview. Extend as necessary. -->
        <!-- Preview Text Spacing Hack : BEGIN -->
        <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: arial,sans-serif;">
            
        </div>
        <!-- Preview Text Spacing Hack : END -->

        <!-- Email Header : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
            <tr>
                <td>
                    <a href="{{config('config.live_url')}}" title="">
                        <div style="padding: 35px 0; text-align: center; background-image:url({{url('assets/admin/images/bg-header.jpg')}}); background-repeat: no-repeat; background-position: center; background-size: cover;">
                        </div>
                    </a>
                </td>
            </tr>
        </table>
        <!-- Email Header : END -->

        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">

            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#f8f8f8" style="padding: 40px 20px 20px; text-align: center;">
                    <h1 style="margin: 0; font-family: arial,sans-serif; font-size: 20px; line-height: 125%; color: #000000; font-weight: normal;text-transform: uppercase;">{{ $newsletter->newsletter_title  }}</h1>
                </td>
            </tr>
            <tr>
                <td bgcolor="#f8f8f8" style="padding: 0 20px 40px; font-family: arial,sans-serif; font-size: 14px; line-height: 140%; color: #555555; text-align: center;">
                    <div style="font-family: Arial,sans-serif; font-size: 14px;">{!! $newsletter->description !!}</div>
                </td>
            </tr>
            <tr>
                <td bgcolor="#f8f8f8" style="padding: 0 20px 40px; font-family: arial,sans-serif; font-size: 15px; line-height: 140%; color: #555555; text-align: center">
                    <!-- Button : BEGIN -->
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
                        <tr>
                            <td style="border-radius: 50px; background: transparent; text-align: center;" class="button-td">
                                <a href="{{ $newsletter->btn_link }}" style="background: #1363f5; border: 0; padding: 11px 30px; font-family: arial,sans-serif; font-size: 12px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold; text-transform: uppercase;" class="button-a">
                                  <span style="color:#ffffff;" class="button-link">{{$newsletter->btn_text}}</span>
                              </a>
                            </td>
                        </tr>
                    </table>
                    <!-- Button : END -->
                </td>
            </tr>
            <!-- 1 Column Text + Button : END -->



            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 20px 2px; text-align: center;">
                    <h1 style="margin: 0; font-family: arial,sans-serif; font-size: 20px; line-height: 125%; color: #000000; font-weight: normal; text-transform: uppercase;">featured podcasts</h1>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding: 0 20px 10px; font-family: arial,sans-serif; font-size: 12px; line-height: 140%; color: #656565; text-align: center;">
                    <div style="font-family: Arial,sans-serif; font-size: 12px;">New and Noteworthy</div>
                </td>
            </tr>
            <!-- 1 Column Text + Button : END -->

            <!-- 4 Even Columns : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" align="center" valign="top" style="padding: 10px 10px 20px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                        @foreach($trending_podcasts as $value)

                            <!-- Column : BEGIN -->
                            <td width="20%" class="stack-column-center" valign="top">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                
                                    <tr>
                                        <td style="padding: 10px 10px 2px; text-align: center">
                                            <a href="{{config('config.live_url')}}/podcasts/{{$value->id}}/{{str_slug(trim($value->title))}}"> 
                                                <img src="{{ $value->getWSImage(127,127) }}" width="127" height="127" alt="" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: arial,sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Arial,sans-serif; font-size: 14px; line-height: 140%; color: #1363f5; padding: 0 10px 25px; text-align: left;" class="center-on-narrow">
                                            <a href="{{config('config.live_url')}}/podcasts/{{$value->id}}/{{str_slug(trim($value->title))}}"> 
                                                <div style="font-family: Arial,sans-serif; font-size: 16px;">{{$value->title}}</div>
                                            </a>
                                        </td>
                                    </tr>
                                
                                </table>
                            </td>
                            <!-- Column : END -->
                            @endforeach
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- 4 Even Columns : END -->




            
            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#f8f8f8" style="padding: 40px 20px 2px; text-align: center;">
                    <h1 style="margin: 0; font-family: arial,sans-serif; font-size: 20px; line-height: 125%; color: #000000; font-weight: normal; text-transform: uppercase;">featured Collections</h1>
                </td>
            </tr>
            <tr>
                <td bgcolor="#f8f8f8" style="padding: 0 20px; font-family: arial,sans-serif; font-size: 12px; line-height: 140%; color: #656565; text-align: center;">
                    <div style="font-family: Arial,sans-serif; font-size: 12px;">Hand-Picked By Interests</div>
                </td>
            </tr>
            <!-- 1 Column Text + Button : END -->

            <!-- 2 Even Columns : BEGIN -->
            <tr>
                <td bgcolor="#f8f8f8" align="center" valign="top" style="padding: 0px 10px 40px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            @foreach($featured_collections as $value)
                            <!-- Column : BEGIN -->
                            <td class="stack-column-center" style="padding: 30px 0 0">
                                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <!-- Column : BEGIN -->
                                        <td width="40.68%" class="stack-column-center" valign="top">
                                            <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td dir="ltr" valign="top" style="padding: 3px 10px;">
                                                    <a href="{{config('config.live_url')}}/collections/{{$value->id}}/{{str_slug(trim($value->title))}}">
                                                        <img src="{{$value->getImage(98)}}" width="98" height="98" alt="" border="0" class="center-on-narrow" style="height: auto; background: #dddddd; font-family: arial,sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                                    </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <!-- Column : END -->
                                        <!-- Column : BEGIN -->
                                        <td width="59.31%" class="stack-column-center" valign="top">
                                            <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td dir="ltr" valign="top" style="font-family: arial,sans-serif; font-size: 14px; line-height: 140%; color: #373737; padding: 3px 10px; text-align: left;" class="center-on-narrow">
                                                        <h2 style="margin: 0 0 4px 0; font-family: arial,sans-serif; font-size: 16px; font-weight: normal; line-height: 125%; color: #1363f5; font-weight: regular;">
                                                            <a href="{{config('config.live_url')}}/collections/{{$value->id}}/{{str_slug(trim($value->title))}}">
                                                                {{$value['title']}}
                                                            </a>
                                                        </h2>
                                                        <div style="font-family: arial,sans-serif; font-size: 12px;">{{ $value->chart_poadcast()->count() }} Podcasts</div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <!-- Column : END -->
                                    </tr>
                                </table>
                            </td>
                            <!-- Column : END -->
                            @endforeach
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- 2 Even Columns : END -->





            <!-- 1 Column Text -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 20px 2px; text-align: center;">
                    <h1 style="margin: 0; font-family: arial,sans-serif; font-size: 20px; line-height: 125%; color: #000000; font-weight: normal; text-transform: uppercase;">featured episodes</h1>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding: 0 20px; font-family: arial,sans-serif; font-size: 12px; line-height: 140%; color: #656565; text-align: center;">
                    <div style="font-family: Arial,sans-serif; font-size: 12px;">Must Listens</div>
                </td>
            </tr>

            <tr>
                <td bgcolor="#ffffff" style="padding: 0 0 40px; text-align: center;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">

                        @foreach($featured_episodes as $value)
                        <tr>
                            <td bgcolor="#ffffff" dir="ltr" align="center" valign="top" width="100%" style="padding: 32px 10px 0;">
                                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <!-- Column : BEGIN -->
                                            <td width="15.17%" class="stack-column-center" valign="top">
                                                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td dir="ltr" valign="top" style="padding: 3px 10px;">
                                                                <a href="{{config('config.live_url')}}/episode/{{$value->id}}/{{str_slug(trim($value->title))}}" style="color: #000000;">
                                                                    <img src="{{ $value->show->getWSImage(68,68) }}" width="68" height="68" alt="" border="0" class="center-on-narrow" style="height: auto; background: #dddddd; font-family: arial,sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <!-- Column : END -->
                                            <!-- Column : BEGIN -->
                                            <td width="84.82%" class="stack-column-center" valign="top">
                                                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td dir="ltr" valign="top" style="font-family: arial,sans-serif; font-size: 12px; line-height: 140%; color: #848282; padding: 3px 10px; text-align: left;" class="center-on-narrow">
                                                                <h2 style="margin: 0 0 5px 0; font-family: arial,sans-serif; font-size: 16px; font-weight: normal; line-height: 125%; color: #000000; font-weight: regular;">
                                                                <a href="{{config('config.live_url')}}/episode/{{$value->id}}/{{str_slug(trim($value->title))}}" >
                                                                    {{ $value->title}}
                                                                </a>
                                                                </h2>
                                                                <div style="font-family: arial,sans-serif; font-size: 12px;">
                                                                    <span>{{ Carbon\Carbon::parse($value->created_at)->format('jS F Y') }}</span>
                                                                    <strong>by</strong>
                                                                    <span style="color: #1363f5;text-transform: uppercase; font-size: 11px">
                                                                    <a href="{{config('config.live_url')}}/podcasts/{{$value->show->id}}/{{str_slug(trim($value->show->title))}}"> 
                                                                    {{$value->show->title}}
                                                                    </a>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <!-- Column : END -->
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        @endforeach
 

                    </table>
                </td>
            </tr>

            <!-- 1 Column Text -->





            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#f8f8f8" style="padding: 40px 20px 2px; text-align: center;">
                    <h1 style="margin: 0; font-family: arial,sans-serif; font-size: 20px; line-height: 125%; color: #000000; font-weight: normal; text-transform: uppercase;">ESSENTIALS</h1>
                </td>
            </tr>
            <tr>
                <td bgcolor="#f8f8f8" style="padding: 0 20px 10px; font-family: arial,sans-serif; font-size: 12px; line-height: 140%; color: #656565; text-align: center;">
                    <div style="font-family: Arial,sans-serif; font-size: 12px;">News, Sport, Culture, Lifestyle</div>
                </td>
            </tr>
            <!-- 1 Column Text + Button : END -->

            <!-- 4 Even Columns : BEGIN -->
            <tr>
                <td bgcolor="#f8f8f8" align="center" valign="top" style="padding: 10px 10px 20px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                             @foreach($essentials as $value)
                            <!-- Column : BEGIN -->
                            <td width="20%" class="stack-column-center" valign="top">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="padding: 10px 10px 2px; text-align: center">
                                        <a href="{{config('config.live_url')}}/podcasts/{{$value->id}}/{{str_slug(trim($value->title))}}"> 
                                            <img src="{{ $value->getWSImage(127,127) }}" width="127" height="127" alt="" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: arial,sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Arial,sans-serif; font-size: 14px; line-height: 140%; color: #1363f5; padding: 0 10px 25px; text-align: left;" class="center-on-narrow">
                                                <div style="font-family: Arial,sans-serif; font-size: 16px;">
                                                <a href="{{config('config.live_url')}}/podcasts/{{$value->id}}/{{str_slug(trim($value->title))}}"> 
                                                {{$value->title}}
                                                </a>
                                                </div>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <!-- Column : END -->
                            @endforeach
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- 4 Even Columns : END -->





            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 20px 20px; text-align: center;">
                    <h1 style="margin: 0; font-family: arial,sans-serif; font-size: 20px; line-height: 125%; color: #000000; font-weight: normal; text-transform: uppercase;">Editor's Pick of The Week</h1>
                </td>
            </tr>
            <!-- 1 Column Text + Button : END -->

            <!-- Thumbnail Left, Text Right : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" dir="ltr" align="center" valign="top" width="100%" style="padding: 10px 10px 40px;">
                    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <!-- Column : BEGIN -->
                            <td width="41.89%" class="stack-column-center" valign="top">
                                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td dir="ltr" valign="top" style="padding: 3px 10px;">
                                        <a href="{{config('config.live_url')}}/podcasts/{{$podcast_of_the_week->id}}/{{str_slug(trim($podcast_of_the_week->title))}}">
                                            <img src="{{$podcast_of_the_week->getWSImage(223,223)}}" width="223" height="223" alt="" border="0" class="center-on-narrow" style="height: auto; background: #dddddd; font-family: arial,sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                        </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <!-- Column : END -->
                            <!-- Column : BEGIN -->
                            <td width="58.10%" class="stack-column-center" valign="top">
                                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td dir="ltr" valign="top" style="font-family: arial,sans-serif; font-size: 14px; line-height: 140%; color: #373737; padding: 3px 10px; text-align: left;" class="center-on-narrow">
                                            <h2 style="margin: 0 0 20px 0; font-family: arial,sans-serif; font-size: 16px; font-weight: normal; line-height: 125%; color: #1363f5; font-weight: regular;">
                                            <a href="{{config('config.live_url')}}/podcasts/{{$podcast_of_the_week->id}}/{{str_slug(trim($podcast_of_the_week->title))}}" >
                                                {{ $podcast_of_the_week->title }}
                                            </a>
                                            </h2>
                                            <div style="margin: 0 0 30px 0; font-family: arial,sans-serif; font-size: 14px;">{!! str_limit($podcast_of_the_week->description, 100, '...')  !!}</div>
                                            <!-- Button : BEGIN -->
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="float:left;">
                                                <tr>
                                                    <td style="border-radius: 50px; background: transparent; text-align: center;" class="button-td">
                                                        <a href="{{config('config.live_url')}}/podcasts/{{$podcast_of_the_week->id}}/{{str_slug(trim($podcast_of_the_week->title))}}" style="background: #1363f5; border: 0; padding: 11px 30px; font-family: arial,sans-serif; font-size: 12px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold; text-transform: uppercase;" class="button-a">
                                                          <span style="color:#ffffff;" class="button-link">Read More</span>
                                                      </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- Button : END -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <!-- Column : END -->
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Thumbnail Left, Text Right : END -->

        </table>
        <!-- Email Body : END -->

        <!-- Email Footer : BEGIN -->
        <table role="presentation" bgcolor="#4562bb" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px; font-family: arial,sans-serif; color: #ffffff; font-size: 12px; line-height: 140%;">
            <tr>
            @if($user->verified)
                <td style="padding: 25px 20px 0; width: 100%; font-family: arial,sans-serif; font-size: 12px; line-height: 140%; text-align: left; color: #ffffff;" class="x-gmail-data-detectors">
                    <div style="font-family: arial,sans-serif; font-size: 12px;">This newsletter was sent to you because you have signed up as a user on our platform. If you believe that you should not be receiving this notification then <a href="mailto:tech@storiyoh.com" style="    cursor: pointer !important;">contact us.</a> </div>
                    <hr style="margin: 30px 0 20px; border: 1px solid #637bc4;">
                </td>
            @else
                <td style="padding: 25px 20px 0; width: 100%; font-family: arial,sans-serif; font-size: 12px; line-height: 140%; text-align: left; color: #ffffff;" class="x-gmail-data-detectors">
                    <div style="font-family: arial,sans-serif; font-size: 12px;">This newsletter was sent to you because you have subscribe on our platform. If you believe that you should not be receiving this notification then <a href="http://app.storiyoh.com/unsubscribe/{{$token}}" style="    cursor: pointer !important;"> unsubscribe. </a></div>
                    <hr style="margin: 30px 0 20px; border: 1px solid #637bc4;">
                </td>
            @endif
            </tr>
            <tr>
                <td dir="ltr" align="center" valign="top" width="100%" style="padding: 0 20px 15px;">
                    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <!-- Column : BEGIN -->
                                <td width="79%" class="stack-column-center" valign="top">
                                    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td dir="ltr" valign="top" align="left">
                                                    <div style="margin: 10px 0;font-family: arial,sans-serif; font-size: 12px;">Copyright © 2018 Storiyoh All Rights Reserved</div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <!-- Column : END -->
                                <!-- Column : BEGIN -->
                                <td width="21%" class="stack-column-center" valign="top">
                                    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td dir="ltr" valign="top" style="font-family: arial,sans-serif; font-size: 12px; line-height: 140%; color: #848282;text-align: left;">
                                                        <div class="mailer-social" style="margin: 10px 0">
                                                          <a href="{{ config('config.facebook_link') }}" target="_blank" style="vertical-align:middle;display:inline-block;margin-right:10px;">
                                                              <img src="{{url('assets/admin/images/facebook.png')}}" alt="" width="10" height="19">
                                                          </a>
                                                          <a href="{{ config('config.twitter_link') }}" target="_blank" style="vertical-align:middle;display:inline-block;margin-right:10px;">
                                                              <img src="{{url('assets/admin/images/twitter.png')}}" alt="" width="23" height="18">
                                                          </a>
                                                          <a href="{{ config('config.linkedin_link') }}" target="_blank" style="vertical-align:middle;display:inline-block;margin-right:10px;">
                                                              <img src="{{url('assets/admin/images/linkedin.png')}}" alt="" width="20" height="20">
                                                          </a>
                                                          <a href="{{ config('config.instagram_link') }}" target="_blank" style="vertical-align:middle;display:inline-block">
                                                              <img src="{{url('assets/admin/images/instagram.png')}}" alt="" width="20" height="20">
                                                          </a>
                                                      </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <!-- Column : END -->
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <!-- Email Footer : END -->

        <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
    </center>
</body>

</html>