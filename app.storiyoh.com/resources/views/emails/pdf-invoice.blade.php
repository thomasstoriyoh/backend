<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Storiyoh</title>
    <style>
    body{
        font-family: Helvetica, Arial, sans-serif;
        font-size:14px;
        color: black;
    }
    </style>
</head>
<body>
    <?php //dd($invoice_data) ?>
    <table style="width: 100%;" >
        <tr>
            <td style="width:100%; text-align:center; vertical-align: top; border-bottom:1px solid #eaeaea; padding:0px 0px 20px 0px;"><img src="https://www.storiyoh.com/images/logo.png" alt="Storiyoh Logo" /></td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%; margin-bottom:20px; margin-top:20px;">
                    <tr>
                        <td style="width:100%; float: left; vertical-align: top; line-height:20px;">
                            <span style="font-size:20px; font-weight: bold;">Storiyoh DMCC, Unit 157</span><br>
                            DMCC Business Centre,<br>
                            Level PF, Gold Tower<br>
                            Dubai, United Arab Emirates
                            {{-- <br>GSTIN : GSTIN123<br> --}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%; margin-bottom:40px;">
                    <tr>
                        <td style="width:50%; float: left; vertical-align: top; line-height:20px;">
                            <span style="font-size:20px; font-weight: bold;">Billing To</span><br>
                                {{ $invoice_data['user_billing_name'] }}<br>
                                {{ $invoice_data['user_billing_email'] }}
                        </td>
                        <td style="width:49%; float: right; vertical-align: top; text-align: right">
                            <table style="width: 100%;" >
                                <tr>
                                    <td style="padding-bottom: 20px; text-align:right;">
                                        <span style="font-size:20px; font-weight: bold;">Tax Invoice</span><br>
                                        #{{ $invoice_data['invoice_number'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;">
                                        <span style="font-size:20px; font-weight: bold;">Invoice Date</span><br>
                                        {{ \Carbon\Carbon::parse($invoice_data['invoice_date'])->format('jS F Y')}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr>
                        <td style="background: #000; padding:20px;">
                            <table style="width: 100%;" >
                                <tr>
                                    <td style="width:1%; color:#FFF; font-weight: bold; font-size:18px;"># </td>
                                    <td style="width:70%; color:#FFF; font-weight: bold; font-size:18px;">Item & Description</td>
                                    <td style="width:28%; text-align: right; color:#FFF; font-weight: bold; font-size:18px;">Amount</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:10px 20px;">
                            <table style="width: 100%;" >
                                <tr>
                                    {{-- <td style="width:1%; color:#000; font-weight: bold; font-size:16px;"></td> --}}
                                    <td style="width:70%; color:#000;  font-size:16px;">{{ $invoice_data['content_title'] }}</td>
                                    <td style="width:30%; text-align: right; color:#000; font-weight: bold; font-size:16px;">{{ $invoice_data['currency'] }} {{ floatval($invoice_data['invoice_total']) }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @if($invoice_data['gift_by'])
                        <tr>
                            <td style="padding-top:0px; padding-bottom:10px; padding-left:40px;">
                                <table style="width: 100%;" >
                                    <tr>
                                        <td style="width:100%; color:#000; font-size:16px; colspan=2"><i>(Gifted to {{ $invoice_data['gift_by'] }})</i></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td style="background:#f0eeef; height:1px; width:100%;"></td>
                    </tr>
                    @if(floatval($invoice_data['tax_percent']))
                        <tr>
                            <td style="padding:10px 20px;">
                                <table style="width: 100%;" >
                                    <tr>
                                        {{-- <td style="width:1%; color:#000; font-weight: bold; font-size:16px;"></td> --}}
                                        <td style="width:100%; text-align: right; color:#000;  font-size:16px;">Inclusive of {{ $invoice_data['tax_title'] }} at <span style="font-weight: bold;">{{ floatval($invoice_data['tax_percent']) }}%</span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="background:#f0eeef; height:1px; width:100%;"></td>
                        </tr>
                    @endif                    
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                @if($invoice_data['currency'] == "INR")
                                    <tr>
                                        <td style="width:70%; font-size:18px; padding:10px 0px 0px 20px; vertical-align:top;">                                        
                                            <b>Total In Words : </b> {{ \App\Traits\HelperV2::numberToWords($invoice_data['invoice_total'], $invoice_data['gateway'])}}                                        
                                        </td>
                                        <td style="width:30%; text-align: right; vertical-align: top;">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td style="padding:10px 17px 10px 0px; font-size: 16px;  text-align:right;">Subtotal {{ $invoice_data['currency'] }} <span style="font-weight: bold;">{{ floatval($invoice_data['net_amount']) }}</span></td>
                                                </tr>
                                                @if(floatval($invoice_data['tax_percent']))
                                                    <tr>
                                                        <td style="padding:0px 17px 10px 0px; font-size: 16px;  text-align:right;">{{ $invoice_data['tax_title'] }} charged at {{ floatval($invoice_data['tax_percent']) }}% {{ $invoice_data['currency'] }} <span style="font-weight: bold;">{{ floatval($invoice_data['tax_amount']) }}</span></td>
                                                    </tr>                                            
                                                @endif
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background:#f0eeef; height:1px; width:100%;" colspan="2"></td>
                                    </tr> 
                                @endif                               
                                <tr>
                                    <td  colspan="2" style="padding:10px 20px 10px 0px; font-size: 20px; text-align:right;">Total <span style="font-weight: bold;">{{ $invoice_data['currency'] }} {{ floatval($invoice_data['invoice_total']) }}</span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="background:#f0eeef; height:1px; width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="padding:20px; font-size: 16px;">Please Note: All payments are made in the name of Storiyoh DMCC. This is a computer generated invoce and it does not require a signature. For any queries, please contact us at <a href="mailto:customerservice@storiyoh.com" style="text-decoration: none;">customerservice@storiyoh.com</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</body>
</html>