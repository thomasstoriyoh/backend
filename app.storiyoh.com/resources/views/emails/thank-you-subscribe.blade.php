<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><html xmlns="http://www.w3.org/1999/xhtml" class="ie-browser" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><![endif]-->
<!--[if !IE]><!-->
<html style="margin: 0;padding: 0;" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->

<head>
    <!--[if gte mso 9]><xml>
     <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
     </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <title>Storiyoh</title>
    <style type="text/css" id="media-query">
        body {
            margin: 0;
            padding: 0;
        }

        table,
        tr,
        td {
            vertical-align: top;
            border-collapse: collapse;
        }

        .ie-browser table,
        .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }

        [owa] .img-container div,
        [owa] .img-container button {
            display: block !important;
        }

        [owa] .fullwidth button {
            width: 100% !important;
        }

        .ie-browser .col,
        [owa] .block-grid .col {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }

        .ie-browser .num12,
        .ie-browser .block-grid,
        [owa] .num12,
        [owa] .block-grid {
            width: 600px !important;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .ie-browser .mixed-two-up .num4,
        [owa] .mixed-two-up .num4 {
            width: 200px !important;
        }

        .ie-browser .mixed-two-up .num8,
        [owa] .mixed-two-up .num8 {
            width: 400px !important;
        }

        .ie-browser .block-grid.two-up .col,
        [owa] .block-grid.two-up .col {
            width: 300px !important;
        }

        .ie-browser .block-grid.three-up .col,
        [owa] .block-grid.three-up .col {
            width: 200px !important;
        }

        .ie-browser .block-grid.four-up .col,
        [owa] .block-grid.four-up .col {
            width: 150px !important;
        }

        .ie-browser .block-grid.five-up .col,
        [owa] .block-grid.five-up .col {
            width: 120px !important;
        }

        .ie-browser .block-grid.six-up .col,
        [owa] .block-grid.six-up .col {
            width: 100px !important;
        }

        .ie-browser .block-grid.seven-up .col,
        [owa] .block-grid.seven-up .col {
            width: 85px !important;
        }

        .ie-browser .block-grid.eight-up .col,
        [owa] .block-grid.eight-up .col {
            width: 75px !important;
        }

        .ie-browser .block-grid.nine-up .col,
        [owa] .block-grid.nine-up .col {
            width: 66px !important;
        }

        .ie-browser .block-grid.ten-up .col,
        [owa] .block-grid.ten-up .col {
            width: 60px !important;
        }

        .ie-browser .block-grid.eleven-up .col,
        [owa] .block-grid.eleven-up .col {
            width: 54px !important;
        }

        .ie-browser .block-grid.twelve-up .col,
        [owa] .block-grid.twelve-up .col {
            width: 50px !important;
        }

        @media only screen and (min-width: 620px) {
            .block-grid {
                width: 600px !important;
            }
            .block-grid .col {
                display: table-cell;
                Float: none !important;
                vertical-align: top;
            }
            .block-grid .col.num12 {
                width: 600px !important;
            }
            .block-grid.mixed-two-up .col.num4 {
                width: 200px !important;
            }
            .block-grid.mixed-two-up .col.num8 {
                width: 400px !important;
            }
            .block-grid.two-up .col {
                width: 300px !important;
            }
            .block-grid.three-up .col {
                width: 200px !important;
            }
            .block-grid.four-up .col {
                width: 150px !important;
            }
            .block-grid.five-up .col {
                width: 120px !important;
            }
            .block-grid.six-up .col {
                width: 100px !important;
            }
            .block-grid.seven-up .col {
                width: 85px !important;
            }
            .block-grid.eight-up .col {
                width: 75px !important;
            }
            .block-grid.nine-up .col {
                width: 66px !important;
            }
            .block-grid.ten-up .col {
                width: 60px !important;
            }
            .block-grid.eleven-up .col {
                width: 54px !important;
            }
            .block-grid.twelve-up .col {
                width: 50px !important;
            }
            .block-grid .col .mailer-social {
                text-align: right;
            }
        }

        @media (max-width: 620px) {
            .block-grid,
            .col {
                min-width: 320px !important;
                max-width: 100% !important;
            }
            .block-grid {
                width: calc(100% - 40px) !important;
            }
            .col {
                width: 100% !important;
            }
            .col>div {
                margin: 0 auto;
            }
            img.fullwidth {
                max-width: 100% !important;
            }
        }
    </style>
</head>
<!--[if mso]>
<body class="mso-container" style="background-color:#FFFFFF;">
<![endif]-->
<!--[if !mso]><!-->

<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
    <!--<![endif]-->
    <div class="nl-container" style="min-width: 320px;Margin: 0 auto;background-color: #FFFFFF">
        <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #FFFFFF;"><![endif]-->

        <!-- Header Start -->
        <div style="background-color:transparent;">
            <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;width: 600px;width: calc(29000% - 179200px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #fff;" class="block-grid ">
                <div style="border-collapse: collapse;display: table;width: 100%;">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 600px;"><tr class="layout-full-width" style="background-color:#0f3f93;"><![endif]-->

                    <!--[if (mso)|(IE)]><td align="center" width="600" style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:15px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                    <div class="col num12" style="min-width: 320px;max-width: 600px;width: 600px;width: calc(28000% - 167400px);background-color: #fff;">
                        <div style="background-color: transparent; width: 100% !important;">
                            <!--[if (!mso)&(!IE)]><!-->
                            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:17px; padding-bottom:17px; padding-right: 0px; padding-left: 0px;">
                                <!--<![endif]-->

                                <div align="center" class="img-container center" style="padding-right: 0px;  padding-left: 0px;">
                                    <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px;" align="center"><![endif]-->
                                    <img class="center" align="center" border="0" src="{{ asset('assets/frontend') }}/images/emailer/logo.png" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 112px" width="112">
                                    <!--[if mso]></td></tr></table><![endif]-->
                                </div>


                                <!--[if (!mso)&(!IE)]><!-->
                            </div>
                            <!--<![endif]-->
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                </div>
            </div>
        </div>
        <!-- Header End -->

        <div style="background-color:transparent;">
            <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;width: 600px;width: calc(29000% - 179200px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #363636;" class="block-grid ">
                <div style="border-collapse: collapse;display: table;width: 100%;">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 600px;"><tr class="layout-full-width" style="background-color:#363636;"><![endif]-->

                    <!--[if (mso)|(IE)]><td align="center" width="560" style=" width:560px; padding-right: 30px; padding-left: 30px; padding-top:30px; padding-bottom:30px; border-top: 20px solid #FFFFFF; border-left: 20px solid #FFFFFF; border-bottom: 20px solid #FFFFFF; border-right: 20px solid #FFFFFF;" valign="top"><![endif]-->
                    <div class="col num12" style="min-width: 320px;max-width: 600px;width: 560px;width: calc(28000% - 167400px);background-color: #363636;">
                        <div style="background-color: transparent; width: 100% !important;">
                            <!--[if (!mso)&(!IE)]><!-->
                            <div style="border-top: 20px solid #FFFFFF; border-left: 20px solid #FFFFFF; border-bottom: 20px solid #FFFFFF; border-right: 20px solid #FFFFFF; padding-top:50px; padding-bottom:50px; padding-right: 30px; padding-left: 30px;">
                                <!--<![endif]-->


                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="font-size:12px;line-height:18px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center"><span style="font-size: 28px; line-height: 42px; color: rgb(0, 174, 239);">Welcome!</span></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center"><em><span style="color: rgb(255, 255, 255); font-size: 14px; line-height: 21px;">We’re excited to have you with us</span></em></p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->

                                <!--[if (!mso)&(!IE)]><!-->
                            </div>
                            <!--<![endif]-->
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                </div>
            </div>
        </div>

        <div style="background-color:transparent;">
            <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;width: 600px;width: calc(29000% - 179200px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
                <div style="border-collapse: collapse;display: table;width: 100%;">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 600px;"><tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                    <!--[if (mso)|(IE)]><td align="center" width="600" style=" width:600px; padding-right: 30px; padding-left: 30px; padding-top:5px; padding-bottom:25px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                    <div class="col num12" style="min-width: 320px;max-width: 600px;width: 600px;width: calc(28000% - 167400px);background-color: transparent;">
                        <div style="background-color: transparent; width: 100% !important;">
                            <!--[if (!mso)&(!IE)]><!-->
                            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:25px; padding-right: 30px; padding-left: 30px;">
                                <!--<![endif]-->

                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="font-size:12px;line-height:18px;color:#000000;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;">Dear <strong>User</strong></span></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;">&nbsp;</span><br></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;">Welcome to the community!</span></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;">&nbsp;</span><br></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;">We’re excited to have you with us. your Email Id has been added to our mailing list.</span></span></p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->

                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="font-size:12px;line-height:18px;color:#000000;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;">Feel free to <strong><span style="color: rgb(0, 174, 239); font-size: 14px; line-height: 21px;"><a href="mailto:tech@storiyoh.com" style="color:#00aeef;text-decoration:none;">contact us</a></span></strong> if you have any queries</span></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;">&nbsp;</span><br></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;">Best wishes</span></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><strong><span style="font-size: 14px; line-height: 21px;">{{ config('config.emailer.sender_name') }}</span></strong></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 14px; line-height: 21px;"><a href="http://{{ config('config.www') }}" style="color:#00aeef;text-decoration:none;">{{ config('config.www') }}</a></span></p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->

                                <!--[if (!mso)&(!IE)]><!-->
                            </div>
                            <!--<![endif]-->
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                </div>
            </div>
        </div>

        <!-- Footer Start -->
        <div style="background-color:transparent;">
            <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;width: 600px;width: calc(29000% - 179200px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f4f4f4;" class="block-grid ">
                <div style="border-collapse: collapse;display: table;width: 100%;">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 600px;"><tr class="layout-full-width" style="background-color:#f4f4f4;"><![endif]-->

                    <!--[if (mso)|(IE)]><td align="center" width="600" style=" width:600px; padding-right: 30px; padding-left: 30px; padding-top:20px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                    <div class="col num12" style="min-width: 320px;max-width: 600px;width: 600px;width: calc(28000% - 167400px);background-color: #f4f4f4;">
                        <div style="background-color: transparent; width: 100% !important;">
                            <!--[if (!mso)&(!IE)]><!-->
                            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:20px; padding-bottom:5px; padding-right: 30px; padding-left: 30px;">
                                <!--<![endif]-->

                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="font-size:12px;line-height:18px;color:#000;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="font-size: 12px; line-height: 18px; color:#777575;">This account notification was sent to you because you have signed up as a user on our platform. If you believe that you should not be receiving this notification, please <strong><a href="mailto:tech@storiyoh.com" style="color:#777575;text-decoration:none;">contact us</a></strong>.</span></p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->

                                <div style="padding-right: 10px; padding-left: 10px; padding-top: 15px; padding-bottom: 10px;">
                                    <!--[if (mso)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px;padding-left: 10px; padding-top: 15px; padding-bottom: 10px;"><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                                    <div align="center">
                                        <div style="border-top: 1px solid #BBBBBB; width:100%; line-height:0px;">&nbsp;</div>
                                    </div>
                                    <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                                </div>

                                <!--[if (!mso)&(!IE)]><!-->
                            </div>
                            <!--<![endif]-->
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                </div>
            </div>
        </div>

        <div style="background-color:transparent;">
            <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;width: 600px;width: calc(29000% - 179200px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f4f4f4;" class="block-grid two-up">
                <div style="border-collapse: collapse;display: table;width: 100%;">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 600px;"><tr class="layout-full-width" style="background-color:#f4f4f4;"><![endif]-->

                    <!--[if (mso)|(IE)]><td align="center" width="300" style=" width:300px; padding-right: 30px; padding-left: 30px; padding-top:5px; padding-bottom:20px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                    <div class="col num6" style="Float: left;max-width: 320px;min-width: 300px;width: 300px;width: calc(12300px - 2000%);background-color: #f4f4f4;">
                        <div style="background-color: transparent; width: 100% !important;">
                            <!--[if (!mso)&(!IE)]><!-->
                            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 30px; padding-left: 30px;">
                                <!--<![endif]-->

                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="font-size:12px;line-height:18px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="color: #777575; font-size: 12px; line-height: 18px;"><strong><span style="font-size: 12px; line-height: 18px;"><span style="font-size: 12px; line-height: 18px;" id="_mce_caret" data-mce-bogus="true"><span style="background-color: rgb(255, 255, 255); font-size: 12px; line-height: 18px;">﻿</span></span>{{ config('config.emailer.address_line1') }}</span>
                                            </strong>
                                            </span>
                                        </p>
                                        <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 11px; line-height: 16px; color: #777575;">{{ config('config.emailer.address_line2') }}</span></p>
                                        <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 11px; line-height: 16px; color: #777575;">{{ config('config.emailer.address_line3') }}</span></p>
                                        <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 11px; line-height: 16px; color: #777575;">{{ config('config.emailer.address_line4') }}</span></p>
                                        <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 11px; line-height: 16px; color: #777575;">{{ config('config.emailer.address_line5') }}</span></p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->

                                <!--[if (!mso)&(!IE)]><!-->
                            </div>
                            <!--<![endif]-->
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td><td align="center" width="300" style=" width:300px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                    <div class="col num6" style="Float: left;max-width: 320px;min-width: 300px;width: 300px;width: calc(12300px - 2000%);background-color: #f4f4f4;">
                        <div style="background-color: transparent; width: 100% !important;">
                            <!--[if (!mso)&(!IE)]><!-->
                            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:20px; padding-right: 30px; padding-left: 30px;">
                                <!--<![endif]-->

                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <div class="mailer-social">
                                        <a href="{{ config('config.facebook_link') }}" target="_blank" style="vertical-align:middle;display:inline-block;margin-right:10px;">
                                          <img src="{{ asset('assets/frontend') }}/images/emailer/facebook.png" alt="" width="10" height="19">
                                        </a>
                                        <a href="{{ config('config.twitter_link') }}" target="_blank" style="vertical-align:middle;display:inline-block;margin-right:10px;">
                                          <img src="{{ asset('assets/frontend') }}/images/emailer/twitter.png" alt="" width="23" height="18">
                                      </a>
                                        <a href="{{ config('config.linkedin_link') }}" target="_blank" style="vertical-align:middle;display:inline-block;margin-right:10px;">
                                          <img src="{{ asset('assets/frontend') }}/images/emailer/linkedin.png" alt="" width="20" height="20">
                                      </a>
                                        <a href="{{ config('config.instagram_link') }}" target="_blank" style="vertical-align:middle;display:inline-block">
                                          <img src="{{ asset('assets/frontend') }}/images/emailer/instagram.png" alt="" width="20" height="20">
                                      </a>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->

                                <!--[if (!mso)&(!IE)]><!-->
                            </div>
                            <!--<![endif]-->
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                </div>
            </div>
        </div>

        <div style="background-color:transparent;">
            <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;width: 600px;width: calc(29000% - 179200px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ccc;" class="block-grid ">
                <div style="border-collapse: collapse;display: table;width: 100%;">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 600px;"><tr class="layout-full-width" style="background-color:#000000;"><![endif]-->

                    <!--[if (mso)|(IE)]><td align="center" width="600" style=" width:600px; padding-right: 30px; padding-left: 30px; padding-top:10px; padding-bottom:10px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                    <div class="col num12" style="min-width: 320px;max-width: 600px;width: 600px;width: calc(28000% - 167400px);background-color: #f4f4f4;">
                        <div style="background-color: transparent; width: 100% !important;">
                            <!--[if (!mso)&(!IE)]><!-->
                            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:10px; padding-right: 30px; padding-left: 30px;">
                                <!--<![endif]-->
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="font-size:12px;line-height:14px;color:#FFF;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 14px;line-height: 17px;text-align: center"><span style="font-size: 11px; line-height: 13px; color: #777575;">Copyright &copy; {{ date("Y") }} <a href="http://{{ config('config.www') }}" style="color:#777575;text-decoration:none;"><strong>{{ config('config.emailer.company_name') }}</strong></a>. All Rights Reserved</span></p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->

                                <!--[if (!mso)&(!IE)]><!-->
                            </div>
                            <!--<![endif]-->
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                </div>
            </div>
        </div>
        <!-- Footer End -->

        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
    </div>
</body>
</html>