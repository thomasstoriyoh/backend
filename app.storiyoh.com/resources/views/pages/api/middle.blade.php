<?php
    $scheme = 'storiyoh';
    $ios_id = '';
    $android_package = 'com.storiyoh.app';
    $auto = false;

    // No trailing slash after path, conform to http://x-callback-url.com/specifications/
    //$REQUEST_URI = preg_replace('@/(?:\?|$)@', '', $_SERVER['REQUEST_URI']);

    // Detection
    $HTTP_USER_AGENT = strtolower($_SERVER['HTTP_USER_AGENT']);
    $android = (bool) strpos($HTTP_USER_AGENT, 'android');

    $iphone = !$android && ((bool) strpos($HTTP_USER_AGENT, 'iphone') || (bool) strpos($HTTP_USER_AGENT, 'ipod'));
    $ipad = !$android && !$iphone && (bool) strpos($HTTP_USER_AGENT, 'ipad');
    $ios = $iphone || $ipad;
    $mobile = $android || $ios;

    // Install
    $ios_install = 'http://itunes.apple.com/app/id' . $ios_id;
    $android_install = 'http://play.google.com/store/apps/details?id=' . $android_package;

    $paramAPP = $item['id']."#$$#".$type;
    $shareType = "Share";

    $ENCODED = $paramAPP;

    //dd($ios, $scheme . ':/?validation='.$ENCODED);
    //Open
    if ($ios) {
        $openLINK = $scheme . ':/?validation='.$ENCODED;
    }
    if ($android) {
        $openLINK = 'intent:/?validation='.$ENCODED.'#Intent;package=' . $android_package . ';scheme=' . $scheme . ';end;';
    }
    else {
        $openLINK = $scheme . ':/?validation='.$ENCODED;
    }    
?>
<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Storiyoh</title>
    <style type="text/css">
        body {margin:0; padding:0;}
        *,*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
        .header{padding:25px 0;text-align:center;border-bottom:1px solid #d2d2d2;}
        .header img{max-width:237px;height:auto;display:block;margin:0 auto;}
        .product-container{}
        .product-box{text-align:center;}
        .product-box .product-img{padding:40px 0 30px;max-width:230px;display:block;margin:0 auto;}
        .product-box .product-desc{font-size:20px;color:#61665f;padding:0 0 30px;max-width:300px;margin:0 auto;}
        .product-box .product-price span{font-size:24px;color:#121312;}
        .product-box .product-price span+span{font-size:18px;color:#838383;}
        .footer{width:100%; height:auto; float:left; position:fixed; bottom:0px;padding: 0;}
        .submit_button {border: 1px solid #3288bb; color: #FFF; display: block; font-size: 14px; padding: 10px 0; text-align: left; transition: all 0.3s ease 0s; width: 100%; text-transform: uppercase;font-family: 'Raleway', sans-serif;background: #3288bb;text-align: center;cursor: pointer;}
    </style>
</head>
<body>
    <?php $image_path = asset('assets/frontend'); ?>
    <div class="header">
        <img src="{{ $image_path }}/images/emailer/logo.png" alt="logo">
    </div>
    <div class="product-container">
        <div class="product-box">            
            <img src="{{ $item['image'] }}" alt="{{ $item['title'] }}" class="product-img">                
            <div class="product-desc">
              {{ $item['title'] }}
            </div>
                <div class="product-price">
                    {{-- {{ $item['desc'] }} --}}
                </div>
        </div>
    </div>
    <div class="footer">
        <a href="{{ $openLINK }}" style="text-decoration: none">
            <div class="submit_button">open in app</div>
        </a>
    </div>
</body>

</html>