**Networks:**

Request: `/networks`

Response: `see: networks.json`
```json
[
    {
        "id" : 1,
        "name" : "ABC"
    }
]
```

**Categories:**

Request: `/categories`

Response: `see: categories.json`
```json
[
    {
        "id" : 5,
        "name" : "Technology",
        "parent_id" : 2 // OR null
    }
]
```

**Topics:**

Request: `/topics`

Response: `see: topics.json`
```json
[
    "Sports"
]
```

**Search Shows:**

Request: `/search/shows/Sports`

Response: `see: search-shows.json`
```json
{
    "query" : "Sports",
    "total_results" : 361,
    "page" : 1,
    "results_per_page" : 10,
    "results" : [
        {
            "id": 5959,
            "title": "FL72 Podcast - Sky Sports",
            "network": {
              "id": 228,
              "name": "Sky Sports"
            },
            "categories": [{
              "id": "2",
              "parent_id": null,
              "name": "Sports & Recreation"
            }],
            "description": "Full Description",
            "hosts": [],
            "buzz_score": "0.0",
            "image_files": [{
              "id": 168995,
              "original_file_url": "file.jpg",
              "storage_id": 24871,
              "imageable_id": 5959,
              "imageable_type": "Collection"
            }]
        }
    ]
}
```

**Show Details:**

Request: `/shows/5959`

Response: `see: view-show.json`
```json
{
  "id": 5959,
  "title": "FL72 Podcast - Sky Sports",
  "description": "Full Description",
  "network": "#", // Confusion Here...
  "itunes_id": 569447563,
  "categories": ["2"],
  "buzz_score": "0.0",
  "image_files": [{
    "url": {
      "full": "full-image.jpg",
      "thumb": "thumb-image.jpg"
    }
  }],
  "number_of_episodes": 41,
  "episode_ids": [469929, 460921, '...']
}
```

**Related Shows:**

Request: `/shows/5959/related`

Response: `see: related-shows.json`
```json
[
   {
       "id": 5959,
       "title": "FL72 Podcast - Sky Sports",
       "network": {
         "id": 228,
         "name": "Sky Sports"
       },
       "categories": [{
         "id": "2",
         "parent_id": null,
         "name": "Sports & Recreation"
       }],
       "description": "Full Description",
       "hosts": [],
       "buzz_score": "0.0",
       "image_files": [{
         "id": 168995,
         "original_file_url": "file.jpg",
         "storage_id": 24871,
         "imageable_id": 5959,
         "imageable_type": "Collection"
       }]
   }
]
```

**Search Episodes:**

Request: `/search/episodes/Sports`

Response: `see: search-episodes.json`
```json
{
     "query": "Sports",
     "total_results": 85902,
     "page": 1,
     "results_per_page": 10,
     "results": [{
         "id": 391376,
         "title": "BangTheBook Sports Betting Talk Radio Podcast March 23",
         "description": "Full Episode Description",
         "date_created": "2017-03-23",
         "show_id": 16245,
         "show_title": "BangTheBook.Com - Sports Betting Radio",
         "tags": ["Sports"],
         "duration": 4049,
         "itunes_episode": 383026606,
         "categories": [{
           "id": 22,
           "parent_id": 2,
           "name": "Professional",
           "parent_name": "Sports & Recreation"
         }, {
           "id": 2,
           "parent_id": null,
           "name": "Sports & Recreation",
           "parent_name": null
         }],
         "audio_files": [{
           "id": 388948,
           "mp3": "original.mp3",
           "audiosearch_mp3": "cloudfront.mp3",
           "duration": "01:07:29",
           "url_title": "bangthebook-sports-betting-talk-radio-podcast-march-23",
           "listenlen": "long"
         }],
         "image_urls": {
           "full": "episode-full.jpg",
           "thumb": "episode-thumb.jpg"
         },
         "show_image_urls": {
           "full": "show-full.jpg",
           "thumb": "show-thumb.jpg"
         },
         "contributors": [{
           "role": "host",
           "slug": "adam",
           "name": "Adam",
           "person_id": 128609
         }],
         "buzz_score": "0.69328",
         "highlights": [],
         "entities": [{
           "name": "Golden Nugget",
           "category": "entity",
           "score": 0,
           "is_confirmed": true,
           "timestamps": null
         }]
     }]
}
```

**Episode Details:**

Request: `/episodes/391376`

Response: `see: view-episode.json`
```json

```