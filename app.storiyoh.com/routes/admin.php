<?php
//Route::auth();
Route::group(['prefix' => 'module', 'middleware' => ['auth.admin:admin', 'apc', '2fa']], function () {
    Route::resource('masthead', 'MastheadController');

    Route::match(['PUT', 'PATCH'], 'show/featured', 'ShowController@featured');
    Route::match(['PUT', 'PATCH'], 'show/unfeatured', 'ShowController@unfeatured');
    Route::any('search_podcast_data', 'ShowController@search_podcast_data');
    Route::any('add_show', 'ShowController@add_show');
    Route::any('update_shows', 'ShowController@update_shows');
    Route::any('fetch_episodes', 'ShowController@fetch_episodes');
    Route::any('search_podcast', 'ShowController@searchPodcast');
    Route::resource('show', 'ShowController');
    Route::resource('category', 'CategoryController');

    Route::resource('faq', 'FaqController');

    Route::get('users/download_csv', 'UserController@downloadCsv');
    Route::resource('users', 'UserController');

    Route::resource('user-reports', 'Reports\Member\UserReportController');
    Route::get('download_csv', 'Reports\Member\UserReportController@download_csv');

    Route::resource('target-groups', 'TargetGroupController');
    Route::resource('push-notifications', 'PushNotificationController');
    Route::match(['PUT', 'PATCH'], 'notification', 'PushNotificationController@notification');

    Route::get('active-shows/search_movies', 'ActiveShowController@search_movies');
    Route::resource('active-shows', 'ActiveShowController');

    Route::match(['PUT', 'PATCH'], 'charts/featured', 'ChartController@featured');
    Route::match(['PUT', 'PATCH'], 'charts/unfeatured', 'ChartController@unfeatured');
    Route::match(['PUT', 'PATCH'], 'charts/reorder_podcast/reorder', 'ChartController@postPodcastReorder');
    Route::get('charts/reorder_podcast/{chart}', 'ChartController@reorder_podcast');
    Route::get('search_keyword', 'ChartController@search_keyword');
    Route::resource('charts', 'ChartController');

    Route::resource('podcast_of_the_day', 'PodcastDayController');

    Route::get('newsletter/search_movies', 'NewsletterController@search_movies');
    Route::get('newsletter/search_charts', 'NewsletterController@search_charts');
    Route::get('newsletter/search_episodes', 'NewsletterController@search_episodes');

    // Test Notiy
    Route::get('newsletter/{newsletter}/testnotify', 'NewsletterController@testnotify');
    Route::put('newsletter/{newsletter}/testnotify', 'NewsletterController@posttestnotify');
    // Notify
    Route::get('newsletter/{newsletter}/notify', 'NewsletterController@notify');
    Route::put('newsletter/{newsletter}/notify', 'NewsletterController@postnotify');
    Route::resource('newsletter', 'NewsletterController');

    Route::match(['PUT', 'PATCH'], 'popular_show/reorder_podcast/reorder', 'PopularShowController@postPodcastReorder');
    Route::get('popular_show/reorder_podcast/{popular_show}', 'PopularShowController@reorder_podcast');
    Route::get('search_movies', 'PopularShowController@search_movies');
    Route::resource('popular_show', 'PopularShowController');

    Route::match(['PUT', 'PATCH'], 'networks/featured', 'NetworkController@featured');
    Route::match(['PUT', 'PATCH'], 'networks/unfeatured', 'NetworkController@unfeatured');
    Route::match(['PUT', 'PATCH'], 'networks/podcasts/{network}', 'NetworkController@postUpdatePodcast');
    Route::get('networks/podcasts/{network}', 'NetworkController@addedPodcast');
    Route::resource('networks', 'NetworkController');

    Route::match(['PUT', 'PATCH'], 'subscriber/active', 'SubscriberController@active');
    Route::match(['PUT', 'PATCH'], 'subscriber/inactive', 'SubscriberController@inactive');
    Route::resource('subscriber', 'SubscriberController');

    Route::resource('itunes-top-podcasts', 'ItunesTopPodcastController');

    Route::match(['PUT', 'PATCH'], 'playlist/featured', 'PlaylistController@featured');
    Route::match(['PUT', 'PATCH'], 'playlist/unfeatured', 'PlaylistController@unfeatured');
    Route::resource('playlist', 'PlaylistController');

    Route::match(['PUT', 'PATCH'], 'smart_playlist/featured', 'SmartPlaylistController@featured');
    Route::match(['PUT', 'PATCH'], 'smart_playlist/unfeatured', 'SmartPlaylistController@unfeatured');
    Route::resource('smart_playlist', 'SmartPlaylistController');
    Route::resource('banner', 'BannerController');

    // Syncdata route
    Route::post('users/syncdata_android', 'UserController@syncdata_android');
    Route::post('users/syncdata_ios', 'UserController@syncdata_ios');
    Route::post('users/syncdata', 'UserController@syncdata');

    Route::resource('explicit_content', 'ExplicitContentController');

    Route::get('active-episode/search_episode', 'MastheadController@search_episode');
    Route::get('active-playlist/search_playlist', 'MastheadController@search_playlist');
    Route::get('active-smart_playlist/search_smart_playlist', 'MastheadController@search_smart_playlist');
    Route::get('active-collection/search_collection', 'MastheadController@search_collection');

    Route::get('factoid/search_movies', 'FactoidsController@search_movies');
    Route::get('factoid/search_episode', 'FactoidsController@search_episode');
    Route::resource('factoid', 'FactoidsController');

    //Duplicate Episode
    Route::any('remove_duplicate_episode', 'ShowController@remove_duplicate_episode');

    //Premium Feature Price
    Route::resource('premium_feature', 'PremiumFeatureController');

    // discovermasthead
    Route::get('discovermasthead/search_movies', 'DiscoverMastheadController@search_movies');
    Route::get('discovermasthead/search_episode', 'DiscoverMastheadController@search_episode');
    Route::get('discovermasthead/search_playlist', 'DiscoverMastheadController@search_playlist');
    Route::get('discovermasthead/search_smart_playlist', 'DiscoverMastheadController@search_smart_playlist');
    Route::get('discovermasthead/search_collection', 'DiscoverMastheadController@search_collection');
    Route::resource('discovermasthead', 'DiscoverMastheadController');

    // Show Categories
    Route::resource('showcategory', 'ShowCategoryController');

    Route::get('/download_csv2', 'Reports\Member\UserReportController@downloadUserCsv');

    /** Fetched Episode from Local xml file */
    Route::any('fetch_episodes_via_local', 'ShowController@fetch_episodes_via_local');

    // Series
    //Route::resource('series', 'SeriesController');
    //Route::get('series/tag/search', 'SeriesController@search_tag');

    //Seller
    Route::match(['PUT', 'PATCH'], 'users/seller/enabled', 'UserController@enabled_seller');
    Route::match(['PUT', 'PATCH'], 'users/seller/disabled', 'UserController@disabled_seller');

    //Marketplace Promotion
    Route::get('marketplace_promotions/search_movies', 'MarketplacePromotionController@search_movies');
    Route::get('marketplace_promotions/search_episode', 'MarketplacePromotionController@search_episode');
    Route::get('marketplace_promotions/search_standalone_episode', 'MarketplacePromotionController@search_standalone_episode');
    Route::resource('marketplace_promotions', 'MarketplacePromotionController');

    //Marketplace Web Promotion
    Route::get('marketplace_web_promotions/search_movies', 'MarketplaceWebPromotionController@search_movies');
    Route::get('marketplace_web_promotions/search_episode', 'MarketplaceWebPromotionController@search_episode');
    Route::get('marketplace_web_promotions/search_standalone_episode', 'MarketplaceWebPromotionController@search_standalone_episode');
    Route::resource('marketplace_web_promotions', 'MarketplaceWebPromotionController');

    //Sales Report
    Route::resource('sales-reports', 'Reports\Sales\SalesReportController');
    Route::get('download_sales_csv', 'Reports\Sales\SalesReportController@download_sales_csv');

    //Highlighted Episodes
    Route::get('highlighted_episodes/search_movies', 'HighlightedEpisodeController@search_movies');
    Route::get('highlighted_episodes/search_episode', 'HighlightedEpisodeController@search_episode');
    Route::get('highlighted_episodes/search_standalone_episode', 'HighlightedEpisodeController@search_standalone_episode');
    Route::resource('highlighted-episodes', 'HighlightedEpisodeController');
});
