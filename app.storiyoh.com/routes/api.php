<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

/*************************************** Business Version 1 Code Start ********************************/
/**
* Here all the API routes for Business Apis
*/
include_once __DIR__ . '/api_business_v1.php';

/*************************************** New Version 1 Code Start ********************************/
/**
* Here all the API routes for new version V1
*/
include_once __DIR__ . '/api_new_version_v1.php'; // we are using this version only for Storiyoh Insights

/*************************************** New Version 2 Code Start ********************************/
/**
 * Here all the API routes for new version V2
 * 
 */
include_once __DIR__ . '/api_new_version_v2.php'; // we are using this version only for website

/*************************************** New Version 3 Code Start ********************************/
/**
* Here all the API routes for new version V3
*/
// include_once __DIR__ . '/api_new_version_v3.php';

/*************************************** New Version 4 Code Start ********************************/
/**
* Here all the API routes for new version V4
*/
// include_once __DIR__ . '/api_new_version_v4.php';

/*************************************** New Version 5 Code Start ********************************/
/**
 * Here all the API routes for new version V5
 */
include_once __DIR__ . '/api_new_version_v5.php';

/*************************************** New Version 6 Code Start ********************************/
/**
 * Here all the API routes for new version V6
 */
include_once __DIR__ . '/api_new_version_v6.php';

/*************************************** New Version 7 Code Start ********************************/
/**
 * Here all the API routes for new version V7
 */
include_once __DIR__ . '/api_new_version_v7.php';

/*************************************** New Version 8 Code Start ********************************/
/**
 * Here all the API routes for new version V8
 * Seperated Paytabs for USD and AES
 * Storiyoh Shots
 * Premium Search
 */
include_once __DIR__ . '/api_new_version_v8.php';