<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes v1 for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*************************************** Without Login Routes ********************************/
Route::group(['prefix' => 'business/v1/active', 'namespace' => 'business\V1', 'middleware' => 'throttle:100,1'], function () {
    //Login Routes
    Route::post('/auth/login', 'Auth\AuthController@login');
    Route::post('/auth/social/login', 'Auth\AuthController@provider_login');

    /* Forget and Reset Password Routes*/
    Route::post('/auth/forgot_password', 'Auth\PasswordController@forgot');
    Route::post('/auth/forgot_password/verify/email', 'Auth\PasswordController@verifyForgotEmailOtp');
    Route::post('/auth/reset_password', 'Auth\PasswordController@reset_password');

    /** Refresh Token */
    Route::post('/auth/refresh_token', 'Auth\AuthController@refresh_token');

    /** Update User Info */
    Route::post('/account/update_encryption_key', 'Auth\AuthController@update_encryption_key');
});

/*************************************** Common Routes ********************************/
Route::group(['prefix' => 'business/v1/active', 'namespace' => 'business\V1', 'middleware' => ['throttle:100,1', 'business_crypt']], function () {
    Route::post('/categories', 'CommonController@categories');
    Route::post('/languages', 'CommonController@languages');
    Route::post('/country', 'CommonController@country');
    Route::post('/ratings', 'CommonController@ratings');
    Route::post('/tags', 'CommonController@tags');
    Route::post('/episode_tags', 'CommonController@episode_tags');
});

/*************************************** After Login Routes ********************************/
Route::group(['prefix' => 'business/v1/active', 'namespace' => 'business\V1', 'middleware' => ['auth.business_api', 'business_crypt', 'throttle:rate_limit,1']], function () {
    //Route::group(['prefix' => 'business/v1/active', 'namespace' => 'business\V1', 'middleware' => ['auth.api', 'throttle:60,1']], function () {

    //Basic Profile.
    Route::post('/account', 'MyAccountController@index');

    //User Listing.
    Route::post('/account/users', 'MyAccountController@users');

    //Create or Update Seller profile
    Route::post('/account/update', 'MyAccountController@update');

    //Create or Upload Seller Image
    Route::post('/account/upload_image', 'MyAccountController@update_image');

    //Logout
    Route::any('/account/logout', 'MyAccountController@logout');

    /** Marketplace Country List */
    Route::post('/get_market_place_country_list', 'MyAccountController@getMarketplaceCountryList');

    //Series routes
    Route::post('/series/dashboard', 'SeriesController@mySeriesDashboard');
    Route::post('/series/my_series', 'SeriesController@mySeries');
    Route::post('/series/my_series_listing', 'SeriesController@mySeriesListing');

    Route::post('/series/create', 'SeriesController@addSeries');
    Route::post('/series/edit', 'SeriesController@editSeries');
    Route::post('/series/get_series_price', 'SeriesController@getSeriesPricing');
    Route::post('/series/update_series_price', 'SeriesController@updateSeriesPricing');
    Route::post('/series/show', 'SeriesController@viewSeries');
    Route::post('/series/episodes', 'SeriesController@viewSeriesEpisodes');    

    Route::post('/series/episode/create', 'SeriesController@addSeriesEpisode');

    Route::post('/episode/file_upload', 'SeriesController@file_upload');

    Route::post('/series/published', 'SeriesController@publishedSeries');
    Route::post('/series/unpublished', 'SeriesController@unpublishedSeries');

    Route::post('/standalone/episode/create', 'SeriesController@addStandaloneEpisode');
    Route::post('/standalone/episodes', 'SeriesController@standaloneEpisodes');
    Route::post('/episode/edit', 'SeriesController@editSeriesEpisode');
    Route::post('/episode/standalone/edit', 'SeriesController@editStandaloneEpisode');

    Route::post('/episode/get_episode_price', 'SeriesController@getEpisodePricing');
    Route::post('/episode/update_episode_price', 'SeriesController@updateEpisodePricing');
    Route::post('/episode/show', 'SeriesController@viewEpisode');

    Route::post('/series/episode/published_from_temp', 'SeriesController@publishedEpisodefromTemp');
    Route::post('/episode/published', 'SeriesController@publishedEpisodeData');
    Route::post('/episode/unpublished', 'SeriesController@unpublishedEpisodeData');

    Route::post('/series/price_calculation', 'SeriesController@priceCalculation');

    Route::post('/episode/episode_audio', 'SeriesController@episodeAudioData');

    Route::post('/content/file_upload', 'SeriesController@fileImageUpload');
    Route::post('/content/remove_file', 'SeriesController@removeImageFile');

    Route::post('/episode/delete', 'SeriesController@removeAudioFile');

    Route::post('/content/remove_file_on_edit', 'SeriesController@removeImageFileonEdit');
    Route::post('/episode/remove_audio_file', 'SeriesController@removeEpisodeAudioFile');
    Route::post('/episode/file_upload_on_edit', 'SeriesController@file_upload_on_edit');
    Route::post('/episode/single_signedin_url', 'SeriesController@single_signedin_url');

    //Search User
    Route::post('/search_users', 'MyAccountController@searchUsers');

    //Dashboard Routes
    Route::post('/sales_graph', 'SalesController@sales_graph');
    Route::post('/top_selling', 'SalesController@top_selling');
    Route::post('/top_download', 'SalesController@top_download');
    Route::post('/top_listened', 'SalesController@top_listened');
    Route::post('/recent_upload', 'SalesController@recent_upload');
    Route::post('/sales_graph_test', 'SalesController@sales_graph_testing');

    //Resources Files
    //Route::post('/episode/upload_episode_resources_for_add', 'SeriesController@uploadEpisodeResourceDataforAdd');
    Route::post('/episode/upload_episode_resources', 'SeriesController@uploadEpisodeResourceData');
    Route::post('/episode/remove_episode_resources', 'SeriesController@removeEpisodeResourceData');

    //Added tag
    Route::post('/tag/add', 'SeriesController@tagAdded');

    //Series Episode Reorder Listing
    Route::post('/series/reorder_episodes', 'SeriesController@viewSeriesEpisodesforReorder');
    Route::post('/series/episode/order', 'SeriesController@orderSeriesEpisodes');
});
