<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes v1 for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*** Stiriyoh Insights Routes*/
Route::group(['prefix' => 'new_version/v1/web/active', 'namespace' => 'new_version\Insights', 'middleware' => ['cors', 'throttle:100,1']], function () {
    Route::post('/total_register_user', 'StoriyohInsights@users_count');
    Route::post('/podcasts/subscribed_show', 'StoriyohInsights@podcasts_subscribed_show');
    Route::post('/podcasts/subscribed_networks', 'StoriyohInsights@podcast_subscribe_networks');
    Route::post('/podcasts/subscribed_categories', 'StoriyohInsights@podcast_subscribe_categories');
    Route::post('/podcasts/shows_in_playlist', 'StoriyohInsights@podcast_shows_in_playlist');
    Route::post('/episode/listened', 'StoriyohInsights@episode_listened');
    Route::post('/episode/playlist', 'StoriyohInsights@episode_playlist');
    Route::post('/episode/downloaded', 'StoriyohInsights@episode_downloaded');
    Route::post('dashboard/linegraph', 'StoriyohInsights@dashboard_linegraph');
    Route::post('dashboard/bargraph', 'StoriyohInsights@dashboard_bargraph');
    Route::post('dashboard/doughnut_graph', 'StoriyohInsights@dashboard_doughnut_graph');
    Route::post('dashboard/top_listing_table', 'StoriyohInsights@dashboard_top_listing_table');
    Route::post('dashboard/bargraph', 'StoriyohInsights@dashboard_bargraph');
    Route::post('dashboard/doughnut_graph', 'StoriyohInsights@dashboard_doughnut_graph');
    Route::post('dashboard/top_listing_table', 'StoriyohInsights@dashboard_top_listing_table');
});
