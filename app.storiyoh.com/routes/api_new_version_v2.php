<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes v1 for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*************************************** Before Login Website Routes ********************************/
Route::group(['prefix' => 'new_version/v2/web/active', 'namespace' => 'new_version\Website', 'middleware' => ['cors', 'throttle:100,1']], function () {
    //Homepage Section
    Route::post('/home', 'WebViewController@home');
    Route::post('/banner_clicks', 'WebViewController@banner_clicks');

    //show section
    Route::post('/show/show', 'WebViewController@show');
    Route::post('/show/collection_list', 'WebViewController@show_lists');
    Route::post('/show/episodes', 'WebViewController@show_episodes');
    //episode
    Route::post('/episode/show', 'WebViewController@episode_show');
    Route::post('/episode/listeners', 'WebViewController@episode_listeners');
    //trending playlist
    Route::post('/trending/playlists', 'WebViewController@trending_playlist');
    Route::post('/trending/playlists/detail', 'WebViewController@trending_playlist_detail');
    Route::post('/trending/playlists/episodes', 'WebViewController@trending_playlist_episodes');
    Route::post('/trending/playlists/followers', 'WebViewController@trending_playlist_follwers');

    //trending Smart playlist
    Route::post('/trending/smart_playlists', 'WebViewController@trending_smartplaylist');
    Route::post('/trending/smart_playlists/detail', 'WebViewController@trending_smartplaylist_detail');
    Route::post('/trending/smart_playlists/episodes', 'WebViewController@trending_smartplaylist_episodes');
    Route::post('/trending/smart_playlists/followers', 'WebViewController@trending_smartplaylist_follwers');

    //collection
    Route::post('/collections', 'WebViewController@collection');
    Route::post('/collections/show', 'WebViewController@collection_show');
    
    //trending playlist
    Route::post('/playlists', 'WebViewController@playlist');

    //trending Smart playlist
    Route::post('/smart_playlists', 'WebViewController@smartplaylist');

    //discover
    Route::group(['prefix'=>'discover'], function(){
        Route::post('masthead','WebViewController@discover_mathead');
        Route::post('pod_of_the_day','WebViewController@pod_of_the_day');
        Route::post('popular_shows','WebViewController@popular_shows');
        Route::post('networks', 'WebViewController@networks');
        Route::post('networks/shows', 'WebViewController@network_shows');
        Route::post('categories', 'WebViewController@categories');
        Route::post('categories/popular_shows', 'WebViewController@categories_shows');
    });
    // Newsletter Subscriber
    Route::post('/subscribe', 'CommonController@subscribe');
    //player
    Route::post('/sourcefile', 'WebViewController@sourceFile');

    //search
    Route::post('/show/auto_search', 'WebViewController@auto_search');
    Route::post('/filter_search', 'CommonController@filter_search');

    Route::post('/search/podcasts', 'WebViewController@showSearch');
    Route::post('/search/episodes', 'WebViewController@episodeSearch');
    Route::post('/search/playlists', 'WebViewController@playlistSearch');
    Route::post('/search/smartplaylists', 'WebViewController@smartplaylistSearch');
    Route::post('/search/users', 'WebViewController@userSearch');

    //contact
    Route::post('/contact', 'CommonController@contact');
    Route::post('/ip2location/get_country', 'WebViewController@getCountry');

    //Marketplace Apis
    Route::post('/marketplace', 'WebViewController@marketplace');
    Route::post('/marketplace/masthead', 'WebViewController@marketplace_masthead');
    Route::post('/marketplace/recently_added_shows', 'WebViewController@recently_added_shows');
    Route::post('/marketplace/recently_added_episodes', 'WebViewController@recently_added_episodes');
    Route::post('/home/premium_free_episode', 'WebViewController@premium_free_episode');
    Route::post('/episode/sourcepremiumfile', 'WebViewController@sourcePremiumFile');
    Route::post('/episode/cast_crew', 'WebViewController@episode_cast_crew');
    Route::post('/show/cast_crew', 'WebViewController@show_cast_crew');

    //Standalone listing
    Route::post('/marketplace/standalone_episodes_shots', 'WebViewController@standalone_episodes_shots');
    Route::post('/marketplace/standalone_episodes', 'WebViewController@standalone_episodes');
});

    /*************************************** Without Login Routes ********************************/
    Route::group(['prefix' => 'new_version/v2/web/active', 'namespace' => 'new_version\Website', 'middleware' => 'throttle:60,1'], function () {
        
        // Authentication Routes.
        //Route::post('/auth/login', 'Auth\AuthController@login');
        //Route::post('/auth/login_by_id', 'Auth\AuthController@login_by_id');

        /* Registration Routes*/    
        Route::post('/auth/register', 'Auth\RegisterController@register');
        //Route::any('/auth/update/username', 'Auth\AuthController@update_username');
        Route::post('/auth/resend/otp', 'Auth\AuthController@send_email_otp');

        /* Verify Routes*/
        Route::post('/auth/verify/email', 'Auth\AuthController@verifyEmailOtp');

        /* Login Routes */
        Route::post('/auth/login', 'Auth\WebLoginController@login'); 
        //Route::post('/auth/web/login', 'Auth\WebLoginController@login'); 

        /* Social Login Routes */
        Route::post('/auth/social/login', 'Auth\AuthController@provider_login');

        /* Forget and Reset Password Routes*/
        //Route::post('/auth/forgot_password', 'Auth\PasswordController@forgot');
        //Route::post('/auth/forgot_password/verify/email', 'Auth\PasswordController@verifyForgotEmailOtp');
        //Route::post('/auth/reset_password', 'Auth\PasswordController@reset_password');

        /* Web */
        Route::post('/auth/forgot_password', 'Auth\WebPasswordController@forgot');
        Route::post('/auth/forgot_password/verify/email', 'Auth\WebPasswordController@verifyForgotEmailOtp');
        //Route::get('/auth/reset-password/{token}/{email}', 'Auth\WebPasswordController@verifypasswordlink');
        Route::post('/auth/reset_password', 'Auth\WebPasswordController@reset_password');

        /* Common Cotroller */
        Route::post('/country', 'CommonController@country');
        Route::post('/app_version', 'CommonController@appVersion');

        /* All Category / Auto Complete */
        Route::post('/categories', 'CommonController@categories');

        /* All Networks / Auto Complete */
        Route::post('/networks', 'CommonController@networks');

        /* All Languages */
        Route::post('/languages', 'CommonController@languages');

        /** Faqs **/
        Route::any('/faqs_with_categories', 'WebViewController@faqs_with_categories');
        Route::any('/faqs_category', 'WebViewController@faqs_category');
        Route::any('/faqs', 'WebViewController@faqs');

        /* All Networks / Auto Complete */
        Route::post('/filter_search', 'CommonController@filter_search');

        /** Check Email Exist or Not */
        Route::post('/auth/checkEmail', 'Auth\AuthController@check_email_exist');

        Route::post('/user_queues', 'CommonController@user_queues');

        /** Refresh Token */
        Route::post('/auth/refresh_token', 'Auth\AuthController@refresh_token');

        /** Social Login V2 */
        //Route::post('/auth/provider_login_v2', 'Auth\AuthController@provider_login_v2');
        Route::post('/auth/provider_login_v2', 'Auth\WebLoginController@provider_login_v2');

        /** Guest User */
        Route::post('/auth/guest_register', 'Auth\AuthController@guest_register');
    });

    // auth.api
    Route::group(['namespace' => 'Common', 'middleware' => ['auth.api', 'throttle:rate_limit,1']], function () {

    Route::get('/cart', 'CartController@index');
    Route::post('/cart', 'CartController@create');
    Route::post('/cart/remove', 'CartController@destroy');
    Route::post('/cart/removeall', 'CartController@removeAll');

});// auth.api
