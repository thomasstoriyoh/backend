<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes v1 for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*** Website Routes*/
Route::group(['prefix' => 'new_version/v5/web/active', 'namespace' => 'new_version\V5', 'middleware' => ['cors', 'throttle:100,1']], function () {
    //Homepage Section
    Route::post('/home', 'WebViewController@home');
    Route::post('/banner_clicks', 'WebViewController@banner_clicks');
    //end

    //show section
    Route::post('/show/show', 'WebViewController@show');
    Route::post('/show/collection_list', 'WebViewController@show_lists');
    Route::post('/show/episodes', 'WebViewController@show_episodes');
    //episode
    Route::post('/episode/show', 'WebViewController@episode_show');
    Route::post('/episode/listeners', 'WebViewController@episode_listeners');
    //trending playlist
    Route::post('/trending/playlists', 'WebViewController@trending_playlist');
    Route::post('/trending/playlists/detail', 'WebViewController@trending_playlist_detail');
    Route::post('/trending/playlists/episodes', 'WebViewController@trending_playlist_episodes');
    Route::post('/trending/playlists/followers', 'WebViewController@trending_playlist_follwers');

    //trending Smart playlist
    Route::post('/trending/smart_playlists', 'WebViewController@trending_smartplaylist');
    Route::post('/trending/smart_playlists/detail', 'WebViewController@trending_smartplaylist_detail');
    Route::post('/trending/smart_playlists/episodes', 'WebViewController@trending_smartplaylist_episodes');
    Route::post('/trending/smart_playlists/followers', 'WebViewController@trending_smartplaylist_follwers');

    //collection
    Route::post('/collections', 'WebViewController@collection');
    Route::post('/collections/show', 'WebViewController@collection_show');

    //trending playlist
    Route::post('/playlists', 'WebViewController@playlist');

    //trending Smart playlist
    Route::post('/smart_playlists', 'WebViewController@smartplaylist');

    //discover
    Route::group(['prefix' => 'discover'], function () {
        Route::post('masthead', 'WebViewController@discover_mathead');
        Route::post('pod_of_the_day', 'WebViewController@pod_of_the_day');
        Route::post('popular_shows', 'WebViewController@popular_shows');
        Route::post('networks', 'WebViewController@networks');
        Route::post('networks/shows', 'WebViewController@network_shows');
        Route::post('categories', 'WebViewController@categories');
        Route::post('categories/popular_shows', 'WebViewController@categories_shows');
    });
    // Newsletter Subscriber
    Route::post('/subscribe', 'CommonController@subscribe');
    //player
    Route::post('/sourcefile', 'WebViewController@sourceFile');

    //search
    Route::post('/show/auto_search', 'WebViewController@auto_search');
    Route::post('/filter_search', 'CommonController@filter_search');

    Route::post('/search/podcasts', 'WebViewController@showSearch');
    Route::post('/search/episodes', 'WebViewController@episodeSearch');
    Route::post('/search/playlists', 'WebViewController@playlistSearch');
    Route::post('/search/smartplaylists', 'WebViewController@smartplaylistSearch');
    Route::post('/search/users', 'WebViewController@userSearch');

    //contact
    Route::post('/contact', 'CommonController@contact');
    Route::post('/ip2location/get_country', 'WebViewController@getCountry');

    //Storiyoh Insights
    Route::post('/total_register_user', 'StoriyohInsights@users_count');
    Route::post('/podcasts/subscribed_show', 'StoriyohInsights@podcasts_subscribed_show');
    Route::post('/podcasts/subscribed_networks', 'StoriyohInsights@podcast_subscribe_networks');
    Route::post('/podcasts/subscribed_categories', 'StoriyohInsights@podcast_subscribe_categories');
    Route::post('/podcasts/shows_in_playlist', 'StoriyohInsights@podcast_shows_in_playlist');
    Route::post('/episode/listened', 'StoriyohInsights@episode_listened');
    Route::post('/episode/playlist', 'StoriyohInsights@episode_playlist');
    Route::post('/episode/downloaded', 'StoriyohInsights@episode_downloaded');
    Route::post('dashboard/linegraph', 'StoriyohInsights@dashboard_linegraph');
    Route::post('dashboard/bargraph', 'StoriyohInsights@dashboard_bargraph');
    Route::post('dashboard/doughnut_graph', 'StoriyohInsights@dashboard_doughnut_graph');
    Route::post('dashboard/top_listing_table', 'StoriyohInsights@dashboard_top_listing_table');
    Route::post('dashboard/bargraph', 'StoriyohInsights@dashboard_bargraph');
    Route::post('dashboard/doughnut_graph', 'StoriyohInsights@dashboard_doughnut_graph');
    Route::post('dashboard/top_listing_table', 'StoriyohInsights@dashboard_top_listing_table');
    //End
});

/*************************************** Without Login Routes ********************************/
Route::group(['prefix' => 'new_version/v5/active', 'namespace' => 'new_version\V5', 'middleware' => 'throttle:100,1'], function () {
    //Route::any('/test', 'ApiTestController@index');

    // Authentication Routes.
    Route::post('/auth/login', 'Auth\AuthController@login');
    Route::post('/auth/login_by_id', 'Auth\AuthController@login_by_id');

    /* Registration Routes*/
    Route::post('/auth/register', 'Auth\AuthController@register');
    //Route::any('/auth/update/username', 'Auth\AuthController@update_username');
    Route::post('/auth/resend/otp', 'Auth\AuthController@send_email_otp');

    /* Verify Routes*/
    Route::post('/auth/verify/email', 'Auth\AuthController@verifyEmailOtp');

    /* Login Routes */
    Route::post('/auth/login', 'Auth\AuthController@login');

    /* Social Login Routes */
    Route::post('/auth/social/login', 'Auth\AuthController@provider_login');

    /* Forget and Reset Password Routes*/
    Route::post('/auth/forgot_password', 'Auth\PasswordController@forgot');
    Route::post('/auth/forgot_password/verify/email', 'Auth\PasswordController@verifyForgotEmailOtp');
    Route::post('/auth/reset_password', 'Auth\PasswordController@reset_password');

    /* Common Cotroller */
    Route::post('/country', 'CommonController@country');
    Route::post('/app_version', 'CommonController@appVersion');

    /* All Category / Auto Complete */
    Route::post('/categories', 'CommonController@categories');

    /* All Networks / Auto Complete */
    Route::post('/networks', 'CommonController@networks');

    /* All Languages */
    Route::post('/languages', 'CommonController@languages');

    /** Faqs **/
    Route::any('/faqs_with_categories', 'WebViewController@faqs_with_categories');
    Route::any('/faqs_category', 'WebViewController@faqs_category');
    Route::any('/faqs', 'WebViewController@faqs');

    /* All Networks / Auto Complete */
    Route::post('/filter_search', 'CommonController@filter_search');

    /** Check Email Exist or Not */
    Route::post('/auth/checkEmail', 'Auth\AuthController@check_email_exist');

    Route::post('/user_queues', 'CommonController@user_queues');

    /** Refresh Token */
    Route::post('/auth/refresh_token', 'Auth\AuthController@refresh_token');

    /** Social Login V2 */
    Route::post('/auth/provider_login_v2', 'Auth\AuthController@provider_login_v2');

    /** Guest User */
    Route::post('/auth/guest_register', 'Auth\AuthController@guest_register');
});

/** My Account Section **/
//Route::group(['prefix' => 'v3.2.1/active', 'middleware' => ['auth.api', 'throttle:rate_limit,1']], function () {
Route::group(['prefix' => 'new_version/v5/active', 'namespace' => 'new_version\V5', 'middleware' => ['auth.api', 'throttle:60,1']], function () {

    /* Register Device Token */
    Route::post('/device/register', 'CommonController@device_register');

    // Basic Profile.
    Route::any('/account', 'MyAccountController@index');
    Route::post('/check_username', 'MyAccountController@checkUsername');
    Route::any('/account/edit', 'MyAccountController@update');
    Route::any('/account/edit_image', 'MyAccountController@update_image');
    Route::any('/account/password_reset', 'MyAccountController@resetPassword');
    Route::any('/account/username', 'MyAccountController@resetUserName');

    Route::any('/account/in_app_purchase', 'MyAccountController@in_app_purchase');

    Route::any('/account/set/username', 'MyAccountController@update_username');
    Route::post('/account/get_my_interest', 'MyAccountController@getMyInterest');
    Route::post('/account/set_my_interest', 'MyAccountController@setMyInterest');
    Route::post('/account/get_my_shows', 'MyAccountController@getMyShows');
    Route::post('/account/my_subscribe_shows', 'MyAccountController@getMySubShows');
    Route::post('/account/my_connections', 'MyAccountController@my_connections');
    Route::post('/account/get_my_categories', 'MyAccountController@getMyCategories');

    Route::any('/account/category/setting', 'MyAccountController@category_notification_setting');
    Route::any('/account/subscribe_show/setting', 'MyAccountController@show_notification_setting');

    // Change Email
    Route::any('/account/email_reset', 'MyAccountController@emailReset');
    Route::any('/account/verify_email', 'MyAccountController@verifyEmailOtp');

    Route::any('/account/privacy', 'MyAccountController@setPrivacyStatus');

    // My Boards Routes
    Route::post('/account/myboards', 'BoardController@myboards');
    Route::post('/account/boards_with_paginate', 'BoardController@boards_with_paginate');
    Route::post('/account/boards_lists', 'BoardController@board_listing');
    Route::post('/account/board/add_board', 'BoardController@add_board');
    Route::post('/account/board/show', 'BoardController@show');
    Route::post('/board/show_without_episodes', 'BoardController@show_without_episodes');
    Route::post('/account/board/edit_board', 'BoardController@edit_board');
    Route::post('/account/board/remove_board', 'BoardController@remove_board');
    Route::post('/account/board/add_episode', 'BoardController@add_episode');
    Route::post('/account/board/remove_episode', 'BoardController@remove_episode');
    Route::post('/account/board/my_followed_boards', 'BoardController@my_followed_boards');
    Route::post('/account/board/my_network_boards', 'BoardController@my_network_boards');
    Route::post('/board/followed_users', 'BoardController@followed_board_user');
    Route::post('/account/board/add_episode_into_multiple_boards', 'BoardController@add_episode_into_multiple_boards');

    // Smart Playlist
    Route::post('/smart_playlist/dashboard', 'SmartPlaylistController@dashboard');
    Route::post('/smart_playlist', 'SmartPlaylistController@index');
    Route::post('/my_smart_playlist', 'SmartPlaylistController@my_smart_playlist');
    Route::post('/smart_playlist/show', 'SmartPlaylistController@show');
    Route::post('/smart_playlist/episodes', 'SmartPlaylistController@smart_playlist_episodes');

    Route::post('/smart_playlist/add_smart_playlist', 'SmartPlaylistController@add_smart_playlist');
    Route::post('/smart_playlist/edit_smart_playlist', 'SmartPlaylistController@edit_smart_playlist');
    Route::post('/smart_playlist/remove_smart_playlist', 'SmartPlaylistController@remove_smart_playlist');

    Route::post('/smart_playlist/add_show_smart_playlist', 'SmartPlaylistController@add_show_smart_playlist');
    Route::post('/smart_playlist/add_show_into_mutiple_smart_playlist', 'SmartPlaylistController@add_show_into_mutiple_smart_playlist');
    Route::post('/smart_playlist/remove_show_smart_playlist', 'SmartPlaylistController@remove_show_smart_playlist');
    Route::post('/smart_playlist_lists', 'SmartPlaylistController@smart_playlist_listing');
    Route::post('/smart_playlist/followed_users', 'SmartPlaylistController@followed_board_user');
    Route::post('/smart_playlist/my_followed_boards', 'SmartPlaylistController@my_followed_smart_playlist');
    Route::post('/smart_playlist/follow_smartlist', 'SmartPlaylistController@follow_smartlist');
    Route::post('/smart_playlist/unfollow_smartlist', 'SmartPlaylistController@unfollow_smartlist');
    Route::post('/smart_playlist/my_network_smartplaylist', 'SmartPlaylistController@my_network_smartlist');

    // Dashboard Routes
    //Route::any('/dashboard/set_recommended_category', 'DashboardController@setRecommendedCategory');
    Route::post('/dashboard/get_recommended_categories', 'DashboardController@getRecommendedCategories');
    Route::post('/dashboard/get_recommended_shows', 'DashboardController@getRecommendedShows');
    Route::post('/dashboard/get_recommened_users', 'DashboardController@getRecommendedUsers');
    Route::post('/dashboard/set_recommended_users', 'DashboardController@follow_user');
    //Route::post('/dashboard/set_recommended_shows', 'DashboardController@subscribe_show');
    Route::post('/dashboard/set_recommendedations', 'DashboardController@setRecommendations');

    // Board Routes
    Route::post('/boards', 'BoardController@index');
    Route::post('/board/show', 'BoardController@show');
    Route::post('/board/episodes', 'BoardController@board_episodes');
    Route::post('/board/episodes_with_paginate', 'BoardController@episodes_with_paginate');

    //Feeds
    Route::any('/feed/networks', 'FeedController@networks');
    Route::any('/feed/networks_feed', 'FeedController@network_feed');
    Route::post('/feed/for_you', 'FeedController@feed_for_you');
    Route::post('/feed/for_you_latest', 'FeedController@feed_for_you_old');
    Route::post('/feed/dashboard_trending', 'FeedController@dashboard_trending');
    Route::post('/feed/trending/boards', 'FeedController@trending_board');
    Route::post('/feed/trending/episodes', 'FeedController@trending_episode');
    Route::post('/feed/recommended/shows', 'FeedController@recommended_shows');
    Route::post('/feed/recommended/shows_latest', 'FeedController@recommended_shows_old');
    Route::post('/feed/recommended/users', 'FeedController@recommended_users');
    Route::post('/feed/recommended/boards', 'FeedController@recommended_boards');
    Route::post('/feed/recommended/episodes', 'FeedController@recommended_episodes');
    Route::post('/feed/recommended/episodes_latest', 'FeedController@recommended_episodes_old');

    //User Activity
    Route::post('/account/follow_board', 'UserActivityController@follow_board');
    Route::post('/account/unfollow_board', 'UserActivityController@unfollow_board');
    Route::post('/account/follow_user', 'UserActivityController@follow_user');
    Route::post('/account/unfollow_user', 'UserActivityController@unfollow_user');
    Route::post('/account/subscribe_show', 'UserActivityController@subscribe_show');
    Route::post('/account/unsubscribe_show', 'UserActivityController@unsubscribe_show');
    Route::post('/track/stream/data', 'UserActivityController@user_stream_data');
    Route::post('/episode/download_link', 'EpisodeController@getEpisodeDownloadLink');
    Route::post('/track/download/data', 'UserActivityController@user_download_data');

    //Route::post('/track/historical/data', 'UserActivityController@user_historical_data');

    // Users Routes
    Route::post('/user/connection', 'UserActivityController@user_connection');
    Route::post('/user/boards', 'UserActivityController@user_boards');
    Route::post('/user/show', 'UserActivityController@show');
    Route::post('/users', 'UserActivityController@users');

    // Shows Routes
    Route::post('/shows', 'ShowController@index');
    Route::post('/show/show', 'ShowController@show');
    Route::post('/show/episodes', 'ShowController@episodes');
    //Route::post('/shows/elastic_search', 'ShowController@show_search');

    // Episode Routes
    Route::post('/episodes', 'EpisodeController@index');
    Route::post('/episode/show', 'EpisodeController@show');
    //Route::post('/episode/like', 'EpisodeController@episode_like');
    //Route::post('/episode/comments', 'EpisodeController@allComments');
    //Route::post('/episode/add_comment', 'EpisodeController@add_comment');
    //Route::post('/episode/remove_comment', 'EpisodeController@remove_comment');
    Route::any('/episode/download/list', 'EpisodeController@episode_download_list');
    Route::post('/episode/queue', 'EpisodeController@episode_queue');
    Route::post('/episode/queue/action', 'EpisodeController@episode_queue_action');
    Route::post('/episode/queue/delete', 'EpisodeController@remove_queue');
    Route::post('/episode/queue/reorder', 'EpisodeController@queue_reorder');
    Route::post('/episode/download_link', 'EpisodeController@getEpisodeDownloadLink');
    Route::post('/episode/share', 'EpisodeController@getEpisodeShareLink');
    //Route::post('/episode/comment/page_no', 'EpisodeController@commentPageNumber');
    Route::post('/episode/duration', 'EpisodeController@update_episode_duration');
    //Route::post('/episodes/elastic_search', 'EpisodeController@search');


    //Discover
    Route::post('/discover/dashboard', 'DiscoverController@discover_dashboard_data');
    Route::post('/discover/dashboard/boards', 'DiscoverController@discover_dashboard_data_for_boards');
    Route::post('/discover/dashboard/categories', 'DiscoverController@discover_dashboard_data_for_categories');
    //Route::post('/discover/shows', 'DiscoverController@shows');
    Route::post('/discover/shows/more', 'DiscoverController@show_viewmore');
    Route::post('/discover/boards', 'BoardController@index');
    Route::post('/discover/boards/more', 'DiscoverController@boards_viewmore');

    //Search Section
    Route::post('/boards/search', 'BoardController@index');
    Route::post('/shows/search', 'ShowController@search');
    Route::post('/episodes/search', 'EpisodeController@search');
    Route::post('/users/search', 'UserActivityController@users');
    Route::post('/smart_playlist/search', 'SmartPlaylistController@index');

    //Notification
    Route::any('/notification', 'NotificationController@notification');
    Route::any('/show_notification', 'NotificationController@show_notification');
    Route::post('/notification/read_status', 'NotificationController@set_notification_status');
    Route::any('/feed_show_notification', 'NotificationController@feed_show_notification');

    // Logout Device
    Route::any('/device/logout', 'MyAccountController@logout_device');

    //New Discover Trending Section
    Route::post('/discover/trending_shows', 'DiscoverController@discover_trending_shows');
    Route::post('/discover/trending_shows/more', 'DiscoverController@discover_trending_shows_view_more');
    Route::post('/discover/trending_boards', 'DiscoverController@discover_trending_boards');
    Route::post('/discover/trending_boards/more', 'DiscoverController@discover_trending_boards_view_more');
    Route::post('/discover/trending_episodes', 'DiscoverController@discover_trending_episodes');
    Route::post('/discover/trending_episodes/more', 'DiscoverController@discover_trending_episodes_view_more');
    Route::post('/discover/popular_user', 'DiscoverController@discover_popular_users');
    Route::post('/discover/popular_user/more', 'DiscoverController@discover_popular_users_view_more');

    //New Discover New Section
    Route::post('/discover/new_shows', 'DiscoverController@discover_new_shows');
    Route::post('/discover/new_shows/more', 'DiscoverController@discover_new_shows_view_more');
    Route::post('/discover/new_updated_boards', 'DiscoverController@discover_new_updated_boards');
    Route::post('/discover/new_updated_boards/more', 'DiscoverController@discover_new_updated_boards_view_more');
    Route::post('/discover/new_recent_users', 'DiscoverController@discover_new_recent_users');
    Route::post('/discover/new_recent_users/more', 'DiscoverController@discover_new_recent_users_view_more');

    // Discover V3 Changes
    Route::post('/discover/featured_shows', 'DiscoverController@discover_dashboard_featured_show_data');
    Route::post('/discover/popular_shows', 'DiscoverController@discover_dashboard_popular_shows_data');
    Route::post('/discover/popular_shows/more', 'DiscoverController@discover_dashboard_popular_shows_data_more');

    Route::post('/discover/categories', 'DiscoverController@discover_dashboard_data_for_categories');

    Route::post('/discover/featured_collections', 'DiscoverController@discover_featured_collection');
    Route::post('/discover/new_collections', 'DiscoverController@discover_dashboard_new_collection');
    Route::post('/discover/people_with_common_interests', 'DiscoverController@people_with_common_interests');

    Route::post('/discover/featured_playlist', 'DiscoverController@discover_dashboard_featured_playlist_data');
    Route::post('/discover/popular_playlist', 'DiscoverController@discover_popular_boards');
    Route::post('/discover/popular_playlist/more', 'DiscoverController@discover_popular_boards_view_more');

    //Collections
    Route::post('/collections', 'CollectionController@index');
    Route::post('/collection/show', 'CollectionController@show');
    Route::post('/collection/share', 'CollectionController@getCollectionShareLink');

    /** Networs Listing */
    Route::post('/network/show', 'DiscoverController@networks');
    Route::post('/discover/networks', 'DiscoverController@discover_network');

    /** Notification count */
    Route::post('/notification/user_notification_count', 'NotificationController@user_notification_count');
    Route::post('/account/update_last_access_time', 'MyAccountController@update_last_access_time');
    Route::post('/discover/featured_networks', 'DiscoverController@discover_featured_network');

    /** Discover Smart Playlist */
    Route::post('/discover/featured_smart_playlist', 'DiscoverController@discover_dashboard_featured_smart_playlist_data');
    Route::post('/discover/new_updated_smart_playlists', 'DiscoverController@discover_new_updated_smart_playlists');
    Route::post('/discover/new_updated_smart_playlists/more', 'DiscoverController@discover_new_updated_smart_playlists_view_more');
    Route::post('/discover/popular_smart_playlist', 'DiscoverController@discover_popular_smart_playlists');
    Route::post('/discover/popular_smart_playlist/more', 'DiscoverController@discover_popular_smart_playlists_view_more');

    /** Recommended user for Feed */
    Route::post('/feed/recommended/facebook_users', 'FeedController@recommended_fb_users');
    Route::post('/feed/recommended/twitter_users', 'FeedController@recommended_twitter_users');

    /** Import OPML File */
    Route::any('/account/import_opml', 'MyAccountController@import_opml');
    Route::any('/account/subscribe_multiple_show', 'MyAccountController@subscribe_multiple_show');

    Route::post('/smart_playlist/shows', 'SmartPlaylistController@smart_playlist_shows');
    Route::post('/smart_playlist/subscribe_shows', 'SmartPlaylistController@getMySubShowsforSmartPlaylist');

    Route::post('/episode/listen_history', 'UserActivityController@user_listen_history');
    Route::post('/episode/remove_listen_history', 'UserActivityController@remove_user_listen_history');

    Route::post('/show/auto_search', 'ShowController@auto_search');

    /* New Show Notification */
    Route::any('/show_notification_v2', 'NotificationController@show_notification_v2');
    Route::any('/feed_show_notification_v2', 'NotificationController@feed_show_notification_v2');
    Route::post('/notification/read_status_v2', 'NotificationController@set_notification_status_v2');
    Route::post('/notification/user_notification_count_v2', 'NotificationController@user_notification_count_v2');

    Route::post('/podcast_of_the_day', 'ShowController@home_page_content_podcast_of_the_day');

    /**  */
    Route::post('/episode/conections', 'EpisodeController@episode_conections');
    Route::post('/show/subscribers', 'ShowController@show_subscribers');
    Route::post('/show/collection_list', 'ShowController@show_lists');

    /* New Code Added 01/09/2018 */
    /* Share Post Routes */
    Route::post('/share_post/added', 'PostController@share_post_added');
    Route::post('/share_post/delete', 'PostController@share_post_delete');
    Route::post('/share_post/repost', 'PostController@share_post_repost');
    Route::post('/share_post/like', 'PostController@share_post_like');
    Route::post('/share_post/unlike', 'PostController@share_post_unlike');
    Route::post('/share_post/comments', 'PostController@share_post_comments');
    Route::post('/share_post/comment/added', 'PostController@share_post_comment_added');
    Route::post('/share_post/comment/delete', 'PostController@share_post_comment_delete');
    Route::post('/share_post/comment/show', 'PostController@share_post_comment_details');

    /** Discover Categories */
    Route::post('/categories/popular_shows', 'ShowController@popular_categories');

    /** My Updates */
    Route::post('/my_feeds', 'MyFeedController@my_network_feed');
    Route::post('/my_feeds/delete', 'MyFeedController@delete_feed');

    /** Feed Masthead */
    Route::post('/feed/masthead', 'FeedController@feed_mastheads');
    Route::post('/notification/user_notification_podcast_social_count', 'NotificationController@user_notification_podcast_social_count');

    /* User Connection with Pagination **/
    Route::post('/user/user_connection_with_pagination', 'UserActivityController@user_connection_with_pagination');
    Route::post('/account/my_connections_with_pagination', 'MyAccountController@my_connections_with_pagination');

    //Recommended Users (similar interest / different interest)
    Route::post('recommended_users/similar_interests', 'UserActivityController@recommended_users_similar_interests');
    Route::post('recommended_users/different_interests', 'UserActivityController@recommended_users_different_interests');

    /* New Routes for recommended user v2 */
    Route::post('/feed/recommended/v2/users', 'FeedController@recommended_users_v2');
    Route::post('/feed/recommended/v2/facebook_users', 'FeedController@recommended_fb_users_v2');
    Route::post('/feed/recommended/v2/twitter_users', 'FeedController@recommended_twitter_users_v2');

    /** User Updates */
    Route::post('/user/feeds', 'UserFeedController@user_network_feed');
    Route::post('/user/smart_playlist', 'UserActivityController@user_smart_playlist');
    Route::post('/user/user_subscribe_shows', 'UserActivityController@user_subscribe_shows');

    /** Contributors Code */
    Route::post('/account/board/contributors', 'BoardController@contributor_listing');
    Route::post('/account/board/adding_contributors', 'BoardController@adding_contributors');
    Route::post('/account/board/remove_contributor', 'BoardController@remove_contributor');
    Route::post('/account/board/my_followers', 'MyAccountController@my_followers');
    Route::post('/board/followed_playlist_users', 'BoardController@followed_playlist_users');

    Route::post('/share_post/show', 'PostController@share_post_details');

    Route::any('/notification_for_test', 'NotificationController@notification_for_test');
    Route::any('/episode_queue_listing', 'EpisodeController@episode_queue_listing');

    // Factoids
    Route::post('factoids/factoids_of_the_day', 'FeedController@factoids_of_the_day');

    // Premium Feature
    Route::post('/premium_feature/show', 'InAppContentController@premium_feature_info');
    Route::post('/user_premium_features', 'InAppContentController@user_premium_features');
    Route::post('/inapp_purchase/validate', 'InAppContentController@purchase_validate');

    // Paypal Integration
    Route::post('/paypal/paypal_approval_url_for_feature', 'PaypalController@paypal_approval_url_for_feature')->name('create-feature');

    // Paytm Integration
    #Route::post('/paytm/create_checksum', 'PaytmController@createChecksum')->name('create.paytm.checksum');
    #Route::post('/paytm/verify_checksum', 'PaytmController@verifyChecksum')->name('verify.paytm.checksum');
    // Unsubscribe Topic
    # Route::any('/account/unsubscribe_topic', 'MyAccountController@unsubscribe_topic');

    // IP2Location
    Route::post('/ip2location/get_location', 'IP2LocationController@get_location');

    // Get User Id
    Route::any('/account/getUserId', 'MyAccountController@getUserId');

    // Community Feeds
    Route::any('/feed/community', 'FeedController@community_feeds');

    // Individual Post 
    Route::post('/share_individual_post/added', 'PostController@share_individual_post_added');
    Route::post('/share_individual_post/show', 'PostController@share_individual_post_details');
    Route::post('/share_individual_post/like', 'PostController@share_individual_post_like');
    Route::post('/share_individual_post/delete', 'PostController@share_individual_post_delete');
    // Route::post('/share_post/repost', 'PostController@share_post_repost');    

    /** Post Comment */
    Route::post('/share_individual_post/comments', 'PostController@share_individual_post_comments');
    Route::post('/share_individual_post/comment/added', 'PostController@share_individual_post_comment_added');
    Route::post('/share_individual_post/comment/delete', 'PostController@share_individual_post_comment_delete');
    Route::post('/share_individual_post/comment/show', 'PostController@share_individual_post_comment_details');

    //Following list
    Route::post('/account/my_following_users', 'MyAccountController@my_following_users');

    //My Purchasses
    Route::post('account/my_purchases', 'MyAccountController@my_purchases');
    Route::post('account/purchase/show', 'MyAccountController@purchase_show');

    /** Wishlist Webservices */
    Route::post('/account/my_wishlists', 'MyAccountController@my_wishlist');
    Route::post('/account/wishlist/added', 'MyAccountController@add_wishlist');
    Route::post('/account/wishlist/delete', 'MyAccountController@delete_wishlist');

    ####Latest discover API#####
    //discover
    Route::group(['prefix' => 'discover'], function () {
        Route::post('masthead', 'DiscoverController@discover_mathead');
        Route::post('pod_of_the_day', 'DiscoverController@pod_of_the_day');
        Route::post('popular_shows', 'DiscoverController@popular_shows');
        Route::post('networks', 'DiscoverController@networks');
        Route::post('networks/shows', 'DiscoverController@network_shows');
        Route::post('categories', 'DiscoverController@categories');
        Route::post('categories/popular_shows', 'DiscoverController@categories_shows');

        Route::post('playlists', 'BoardController@playlists');
        Route::post('smartplaylists', 'SmartPlaylistController@smartPlaylists');
    });

    //Marketplace Api start here
    Route::group(['prefix' => 'marketplace'], function () {
        Route::post('check_if_marketplace_available_for_country', 'MarketPlaceController@check_if_marketplace_available_for_country');
        Route::post('dashboard/promo_banner', 'MarketPlaceController@promo_banner');
        Route::post('dashboard/new_series', 'MarketPlaceController@new_series');
        Route::post('dashboard/updated_series', 'MarketPlaceController@updated_series');
        Route::post('dashboard/new_episodes', 'MarketPlaceController@new_episodes');

        Route::post('series/show', 'MarketPlaceController@show_details');
        Route::post('series/show/episodes', 'MarketPlaceController@show_episodes');
        Route::post('series/cast_crew', 'MarketPlaceController@show_cast_crew');

        Route::post('series/episode/show', 'MarketPlaceController@episode_details');
        Route::post('series/episode/cast_crew', 'MarketPlaceController@episode_cast_crew');
        Route::post('series/episode/single_signedin_url', 'MarketPlaceController@single_signedin_url');

        //Webservices for Seller
        Route::post('seller/profile', 'MarketPlaceController@seller_profile');
        Route::post('seller/series', 'MarketPlaceController@seller_series');
        Route::post('seller/episodes', 'MarketPlaceController@seller_episodes');


        // Paytm Integration
        Route::post('/paytm/create_checksum', 'PaytmController@create_checksum')->name('create.paytm.checksum');
        Route::post('/paytm/verify_checksum', 'PaytmController@verify_checksum')->name('verify.paytm.checksum');

        // Gift Section Routes
        Route::post('check_recipient_gift_status', 'MarketPlaceController@check_recipient_gift_status');
        Route::post('/paytm/create_checksum_for_gift', 'PaytmController@create_checksum_for_gift')->name('create.paytm.checksum_gift');
        Route::post('/paytm/verify_checksum_for_gift', 'PaytmController@verify_checksum_for_gift')->name('verify.paytm.checksum_gift');

        //Create Single Signed Url for Resaurces
        Route::post('series/episode/single_signedin_url_resaurces', 'MarketPlaceController@single_signedin_url_resaurces');

        // Razorpay Integration
        Route::post('/series/episode/create_order', 'RazorpayController@free_order');
        Route::post('/razorpay/create_order', 'RazorpayController@create_order');
        Route::post('/razorpay/verify_order', 'RazorpayController@verify_order');
        Route::post('/razorpay/create_order_for_gift', 'RazorpayController@create_order_for_gift');
        Route::post('/razorpay/verify_order_for_gift', 'RazorpayController@verify_order_for_gift');

        //Paytabs Integration
        Route::post('/paytabs/get_user_address_info', 'PayTabController@get_user_address_info');
        Route::post('/paytabs/update_user_address_info', 'PayTabController@update_user_address_info');
        Route::post('/paytabs/create_order', 'PayTabController@create_order');
        Route::post('/paytabs/verify_order', 'PayTabController@verify_order');
        Route::post('/paytabs/create_order_for_gift', 'PayTabController@create_order_for_gift');
        Route::post('/paytabs/verify_order_for_gift', 'PayTabController@verify_order_for_gift');
    });
    //Marketplace Api end here

    //Sendbird Chat Api start here
    Route::group(['prefix' => 'chat'], function () {
        Route::post('/update_sendbird_id_to_user', 'ChatController@update_sendbird_id_to_user');
        Route::post('/user_listings', 'ChatController@sendbird_user_listings');
    });
    //Sendbird Chat Api end here
});
