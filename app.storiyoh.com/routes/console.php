<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

// * * * * * php /workspace/kazi/RND/audiosearch/artisan schedule:run >> /dev/null 2>&1
Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('test:cron', function () {
    Illuminate\Support\Facades\Log::info("Log: ". @date('Y-m-d H:i:s'));
})->describe('Display an inspiring quote');
