<?php
Route::auth();
Route::get('/logout', 'Auth\LoginController@logout');

Route::match(['PUT', 'PATCH'], 'module/settings', 'Core\ModuleController@postSettings');
Route::get('module/settings', 'Core\ModuleController@settings')->middleware('2fa');;
Route::resource('module', 'Core\ModuleController')->middleware('2fa');

Route::resource('/', 'Core\DashboardController')->middleware('2fa');
Route::get('/language/change/{locale}', 'Core\DashboardController@show')->middleware('2fa');

/** 2FA Settings */
Route::get('/2fa', 'Core\PasswordSecurityController@show2faForm')->middleware(['auth.admin:admin', '2fa']);
Route::post('/generate2faSecret', 'Core\PasswordSecurityController@generate2faSecret')->name('generate2faSecret');
Route::post('/2fa', 'Core\PasswordSecurityController@enable2fa')->name('enable2fa');
Route::post('/disable2fa', 'Core\PasswordSecurityController@disable2fa')->name('disable2fa');

Route::post('/2faVerify', function () {
    return redirect(request()->session()->get('_previous')['url']);
})->name('2faVerify')->middleware('2fa');

Route::group(['middleware' => ['auth.admin:admin', 'apc', '2fa']], function () {
    Route::get('profile', 'Core\AdminController@profile');
    Route::match(['PUT', 'PATCH'], 'profile', 'Core\AdminController@postProfile');
    Route::resource('configure/admin', 'Core\AdminController');

    Route::resource('configure/log', 'Core\LogController');

    Route::group(['prefix' => 'reports/google/data'], function () {
        Route::resource('user', 'Reports\Google\GoogleAnalyticsUserData');
        Route::resource('session', 'Reports\Google\GoogleAnalyticsSessionData');
        Route::resource('device', 'Reports\Google\GoogleAnalyticsDeviceData');
        Route::resource('geo', 'Reports\Google\GoogleAnalyticsGeoData');
        Route::resource('behavior', 'Reports\Google\GoogleAnalyticsBehaviorData');
    });

    Route::group(['prefix' => 'documentation'], function () {
        Route::resource('category', 'Documentation\DocumentationCategoryController');
        Route::resource('pages', 'Documentation\DocumentationPagesController');
    });

    /**
     * These are optional Routes.
     */
    Route::resource('reports/member', 'Reports\Member\MemberReportController');

    Route::get('configure/language/section/create', 'Core\LanguageController@createSection');
    Route::post('configure/language/section', 'Core\LanguageController@storeSection');
    Route::get('configure/language/section/{section}/words', 'Core\LanguageController@words');
    Route::post('configure/language/section/word/{section}', 'Core\LanguageController@storeWord');
    Route::resource('configure/language', 'Core\LanguageController');

    /**
     * App Version
     */
    Route::resource('configure/app_version', 'Core\AppVersionController');
    Route::match(['PUT', 'PATCH'], 'notification', 'Core\AppVersionController@notification');

    /* Manual Sync */
    Route::resource('configure/manual_sync', 'Core\ManualSyncController');

    /**
     * Feed Configuration
     */
    Route::resource('configure/feed_configuration', 'Core\FeedConfigurationController');
    
    /**
     * Market Countries
     * 
    */
    Route::resource('configure/marketplace_countries', 'Core\MarketplaceCountryController');
    
    /**
     * Market Countries
     * 
    */
    Route::resource('configure/marketplace_currencies', 'Core\MarketplaceCurrencyController');
});
