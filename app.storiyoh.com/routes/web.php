<?php
Route::get('/image/{token}', 'OptimizeImageController@index');

use App\Models\Episode;
use App\Models\Show;
use App\Models\ShowNotification;
use App\Models\Feed;
use Carbon\Carbon;

Route::get('/', function () {
    //$show= Show::where('id',546987)->first();
    //$show= Show::where('id',546986)->first();
    //$show->unsearchable();
    //dd($show);    
    return redirect('https://www.storiyoh.com/');
    exit;
});

Route::get('/welcome', function () {
    return redirect('https://www.storiyoh.com/');
    exit;
});

Route::get('/home', function () {
    return redirect('/catalyst');
    exit;
});


Route::any('/e/{content}', 'EpisodeController@url_scheme');
Route::any('/s/{content}', 'EpisodeController@url_scheme');

/** Static Pages **/
Route::any('/api/about_us', 'Api\WebViewController@about_us');
Route::any('/api/about_us/{view_mode}', 'Api\WebViewController@about_us');

Route::any('/api/disclaimer', 'Api\WebViewController@disclaimer');
Route::any('/api/disclaimer/{view_mode}', 'Api\WebViewController@disclaimer');

Route::any('/api/privacy_policy', 'Api\WebViewController@privacy_policy');
Route::any('/api/privacy_policy/{view_mode}', 'Api\WebViewController@privacy_policy');

Route::any('/api/terms_of_use', 'Api\WebViewController@terms_of_use');
Route::any('/api/terms_of_use/{view_mode}', 'Api\WebViewController@terms_of_use');

Route::any('/api/attributions', 'Api\WebViewController@attributions');
Route::any('/api/attributions/{view_mode}', 'Api\WebViewController@attributions');

Route::any('/api/faqs', 'Api\WebViewController@faqs');
Route::any('/api/faqs/{view_mode}', 'Api\WebViewController@faqs');

Route::any('/api/faqs/details/{faq_id}', 'Api\WebViewController@faqs_details');
Route::any('/api/faqs/details/{faq_id}/{view_mode}', 'Api\WebViewController@faqs_details');

Route::any('/api/copyright_policy', 'Api\WebViewController@copyright_policy');
Route::any('/api/copyright_policy/{view_mode}', 'Api\WebViewController@copyright_policy');

Route::any('/api/community_guidelines', 'Api\WebViewController@community_guidelines');
Route::any('/api/community_guidelines/{view_mode}', 'Api\WebViewController@community_guidelines');

Route::any('/api/twitter_callback', 'Api\WebViewController@twitter_callback');

#Route::get('/pubsub/podcast_subscribe', 'PubSubController@podcast_subscribe');
#Route::get('/pubsub/subscribe', 'PubSubController@subscribe');
#Route::get('/pubsub/unsubscribe', 'PubSubController@unsubscribe');
#Route::any('pubsub/pull_and_push_rss_data', 'PubSubController@pull_and_push_rss_data');

Route::any('/api/episode/{epi_id}', 'Api\WebViewController@episode_details');
Route::any('/api/episode/{epi_id}/{view_mode}', 'Api\WebViewController@episode_details');

//Route::any('/unsubscribe/{token?}', 'Api\V3_2\WebViewController@unsubscribe');
    
Route::any('/api/episode/resources/{epi_id}/{view_mode}', 'Api\WebViewController@episode_resources_details');

# Route::get('/paypal/execute-feature-success', 'Api\V3_2_1\PaypalController@execute_feature_success');
# Route::get('/paypal/execute-feature-failed', 'Api\V3_2_1\PaypalController@execute_feature_failed');

Route::any('/api/show/{show_id}', 'Api\WebViewController@show_details');
Route::any('/api/show/{show_id}/{view_mode}', 'Api\WebViewController@show_details');

Route::get('/user/verify/{id}/{token}', 'Auth\VerifyController@index');
Route::get('/user/reset-password/{email}/{token}', 'Auth\VerifyController@verifypasswordlink');

